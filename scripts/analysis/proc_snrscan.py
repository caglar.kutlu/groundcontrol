#!/usr/bin/python3
from pathlib import Path
from collections import defaultdict
import sys

from matplotlib.cm import cmap_d
from cycler import cycler
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
import attr
import argparse
import parse
import typing as t
import xarray as xr
import scipy.constants as cnst

from groundcontrol.measurement import MeasurementModel, read_measurement_csv, read_csv
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import quantity, parameter, declarative
from groundcontrol.analysis.plotting import plot2d
import groundcontrol.units as un
from groundcontrol.analysis.nt import noise_psd


class SNRSpectrum(MeasurementModel):
    """Standard data model for Spectrum Analyzer measurements.
    """
    frequencies: Array[float] = quantity(un.hertz, 'Frequency')
    snrs: Array[float] = quantity(un.ratio_ww, 'SNR')
    noisepows: Array[float] = quantity(un.dBm, 'NoisePower')
    rbw: float = parameter(un.hertz, "RBW")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


@declarative
class DataSet:
    snrons: t.List[np.ndarray]
    snroffs: t.List[np.ndarray]
    tups: t.List[TuningPoint]
    minds: t.List[int]

    @classmethod
    def from_path(cls, path: Path):
        dataset = defaultdict(dict)
        for fl in path.glob("SNRS/*.csv"):
            pd = parse.parse("{root}/SNRS_{onoff}-i{mind}_{index}.csv", str(fl))
            mind = int(pd['mind'])
            snrspect = read_measurement_csv(fl)
            if pd['onoff'] == "ON":
                dataset[mind]['snron'] = snrspect
            else:
                dataset[mind]['snroff'] = snrspect

        for fl in path.glob("TP/*.csv"):
            pd = parse.parse("{root}/TP_i{mind}_{index}.csv", str(fl))
            mind = int(pd['mind'])
            tp = read_measurement_csv(fl)
            dataset[mind]['tup'] = tp

        sds = dict(sorted(dataset.items(), key=lambda tp: tp[0]))

        snrons = []
        snroffs = []
        tups = []
        minds = []

        for mind, v in sds.items():
            try:
                snrons.append(v['snron'])
                snroffs.append(v['snroff'])
                tups.append(v['tup'])
                minds.append(mind)
            except KeyError:
                continue
        return cls(snrons, snroffs, tups, minds)

    def iplot(self, mind: int):
        snron = self.snrons[mind]
        snroff = self.snroffs[mind]
        tup = self.tups[mind]

        fig, ax = plt.subplots()
        ax.plot(snron.frequencies, snron.snrs, '*', label='ON')
        ax.plot(snroff.frequencies, snroff.snrs, '^', label='OFF')

        ax.grid()
        ax.legend()


def plotall(path):
    spect: SNRSpectrum
    ds = DataSet.from_path(path)
    # ds.iplot(0)
    # plt.show()

    # THE NT MEASUREMENT WITH JPA OFF
    data, meta = read_csv('JPAOFFESTIMATIONS/nt_fridgens.csv')
    f, g, gerr, tn, tnerr = data.T

    tnoff = xr.DataArray(tn, coords=[('f', f)])

    mind = 4
    fig, ax = plt.subplots()
    cmap = cmap_d['tab20']
    ax.set_prop_cycle(cycler(color=cmap.colors))
    for mind in ds.minds:
        tup = ds.tups[mind]
        print(tup.pretty())
        # quick analysis to show
        snron = ds.snrons[mind]
        snron = xr.DataArray(snron.snrs, [('f', snron.frequencies)])
        snroff = ds.snroffs[mind]
        snroff = xr.DataArray(snroff.snrs, [('f', snroff.frequencies)])
        rpovr = snroff/snron

        _tnoff = tnoff.interp_like(rpovr)

        tnf = noise_psd(0.04, rpovr.f.values)/cnst.Boltzmann
        tnf = xr.DataArray(tnf, [('f', rpovr.f.values)])

        tn = rpovr*(tnf + _tnoff) - tnf
        ax.plot(tn.f, tn.values, label=f'i{mind}')
        ax.plot(tn.f, _tnoff)

    ylim = ax.get_ylim()
    # ylim = (0.1, ylim[1])
    ylim = (0.1, 2)
    ax.set_ylim(ylim)
    ax.legend(loc='upper right')
    ax.set_xlabel("Frequency [Hz]")
    ax.set_ylabel("$T_n$ [K]")
    ax.set_title("$f_r$~5.5-5.7 GHz | G~10-19dB")
    ax.grid()

    plt.show()


# def plot_iloc(path):
#     spect: SNRSpectrum
#     ds = DataSet.from_path(path)

#     # THE NT MEASUREMENT WITH JPA OFF
#     data, meta = read_csv('JPAOFFESTIMATIONS/nt_fridgens.csv')
#     f, g, gerr, tn, tnerr = data.T

#     tnoff = xr.DataArray(tn, coords=[('f', f)])

#     fig, ax = plt.subplots()
#     for mind in ds.minds:
#         tup = ds.tups[mind]
#         print(tup.pretty())
#         # quick analysis to show
#         snron = ds.snrons[mind]
#         snron = xr.DataArray(snron.snrs, [('f', snron.frequencies)])
#         snroff = ds.snroffs[mind]
#         snroff = xr.DataArray(snroff.snrs, [('f', snroff.frequencies)])
#         rpovr = snroff/snron

#         _tnoff = tnoff.interp_like(rpovr)

#         tnf = noise_psd(0.04, rpovr.f.values)/cnst.Boltzmann
#         tnf = xr.DataArray(tnf, [('f', rpovr.f.values)])

#         tn = rpovr*(tnf + _tnoff) - tnf
#         ax.plot(tn.f, tn.values, label=f'i{mind}')
#         ax.plot(tn.f, _tnoff)

#     ylim = ax.get_ylim()
#     # ylim = (0.1, ylim[1])
#     ylim = (0.1, 2)
#     ax.set_ylim(ylim)
#     ax.legend(loc='upper right')
#     ax.set_xlabel("Frequency [Hz]")
#     ax.set_ylabel("$T_n$ [K]")
#     ax.set_title("$f_r$~5.5-5.7 GHz | G~10-19dB")
#     ax.grid()

#     plt.show()


def main(path):
    plotall(path)


if __name__ == "__main__":
    try:
        path = Path(sys.argv[1])
    except IndexError:
        path = Path("./SNRSCANS/200619_SNRSCAN_5.5-5.7G_P-26dBm/")
    main(path)
    
