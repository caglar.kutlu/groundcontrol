"""This script reads and converts the dataset into xarray compatible netCDF
format and saves it."""

from pathlib import Path
import argparse

import xarray as xr
import matplotlib.pyplot as plt

from groundcontrol.analysis.paramap import RawDataSet
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.util import fullrange, resize

__description__ = __doc__

parser = argparse.ArgumentParser(description=__description__)
parser.add_argument(
        'path',
        help='Path to paramap measurement folder.',
        type=Path)

parsed = parser.parse_args()

fpath = parsed.path

print(f"Reading {fpath!s}")
ds = RawDataSet.from_path(fpath).to_xarray()

ds.to_netcdf(f"{fpath.name}.nc")
