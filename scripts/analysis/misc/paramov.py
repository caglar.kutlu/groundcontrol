# coding: utf-8
import xarray as xr
import matplotlib.pyplot as plt
from datargs import arg, argsclass, parse
from pathlib import Path
from xmovie import Movie


@argsclass
class Opts:
    path: Path = arg(True, help="Path to paramap netCDF file.")
    out: Path = None


opts = parse(Opts)


ds = xr.open_dataset(opts.path)
ds.ppump.attrs["long_name"] = "$ P_p $"
ds.ppump.attrs["units"] = "dBm"
ds.detuning.attrs["long_name"] = "Normalized $ f_p/2 - f_{r0} $"
ds = ds.interpolate_na(dim="ppump", max_gap=1)
ds.isel(ib=0).g1.plot.pcolormesh(vmin=0, vmax=35)


G = 'g1'


# chunking necessary for parallel processing
mov = Movie(getattr(ds, G).chunk({"ib": 1}), framedim="ib", vmin=0, vmax=40)

outname = f"{opts.path.stem}_{G}_PARAMOV" if opts.out is None else opts.out.as_posix()


print(f"{outname}.gif")

mov.save(
    f"{outname}.gif",
    remove_movie=False,
    progress=True,
    framerate=5,
    overwrite_existing=True,
)

mov.save(
    f"{outname}_FAST.gif",
    remove_movie=False,
    progress=True,
    framerate=10,
    overwrite_existing=True,
)
