from pathlib import Path
from typing import Sequence

import attr
import pandas as pd
from datargs import arg, argsclass, parse


def _intervalparse(s: str):
    # Parses stuff like "(5.4e9, 5.5e9]"
    return tuple(map(float, s[1:-1].split(", ")))


def read_lut_df(fname: Path):
    df = pd.read_csv(
        fname, converters={"faccat": _intervalparse, "g1cat": _intervalparse}
    )

    df.faccat = pd.IntervalIndex.from_tuples(df.faccat)
    df.g1cat = pd.IntervalIndex.from_tuples(df.g1cat)

    df = df.set_index(["faccat", "g1cat"]).rename(
        columns={"detuning": "det", "ppump": "pp"}
    )
    return df


@argsclass(
    description=(
        "Script for converting LUT to working point set csv file in the format expected"
        " by other scripts like jpasnrscan.py"
    )
)
class ProgOpts:
    lut: Path = arg(True, help="Path to LUT csv.")
    out: Path = arg(True, help="Output working point set csv filename.")
    freq: Sequence[float] = arg(
        aliases=["-f"], default=(), help="Set of frequencies to select from the LUT."
    )
    gain: Sequence[float] = arg(
        aliases=["-g"], default=(), help="Set of gains to select from the LUT."
    )
    gain_as_setpoint: bool = arg(
        default=True,
        help=(
            "Include the given gain selection information in a field named 'g1sp'. "
            " This is to be used for online tuning algorithms."
        ),
    )


def main(opts: ProgOpts):
    lut = read_lut_df(opts.lut)

    entries = []
    assumegaincut = False

    if opts.freq == ():
        opts.freq = lut.index.get_level_values("faccat").drop_duplicates()
    if opts.gain == ():
        assumegaincut = True

    for f in opts.freq:
        flut = lut.loc[f]
        _dif = flut[["pp", "g1"]].diff().dropna()
        dpdg = (_dif.pp / _dif.g1).min()
        # maybe not use min() and extrapolate for the first entry?
        flut["dpdg"] = dpdg

        if assumegaincut == True:
            opts.gain = flut.index.get_level_values("g1cat").drop_duplicates()

        for g in opts.gain:
            entry = flut.loc[g]
            if opts.gain_as_setpoint:
                if isinstance(g, pd.Interval):
                    entry["g1sp"] = g.left
                else:
                    entry["g1sp"] = g
            entries.append(entry)

    seldf = (
        pd.DataFrame(entries)
        .reset_index(drop=True)
        .drop_duplicates()
        # .drop_duplicates(subset=["ib", "fp", "pp"])
    )

    # order for backward consistency with jpasnrscan.py
    order = ["ib", "det", "pp", "fr0", "bw0", "g1", "g2", "g1m", "g2m", "fp", "dpdg"]

    if opts.gain_as_setpoint:
        order.append("g1sp")

    seldf[order].to_csv(opts.out.with_suffix(".csv"), index=False)


if __name__ == "__main__":
    main(parse(ProgOpts))
