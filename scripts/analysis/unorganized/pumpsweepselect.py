"""Selects the desired wp for today's measurement"""
import argparse
from pathlib import Path
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np


def setup_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
            'path',
            help='Path to paramap netcdf file.',
            type=Path)

    parser.add_argument(
            '-c2',
            help=("Perform a cut by defining a region with "
                  "'G1MIN <= g1 <= G1MAX, g1-g2 >= G2DIF' where "
                  "g1 is the gain at 1st frequency offset and g2 "
                  "is the gain at the 2nd frequency offset."),
            metavar=('G1MIN', 'G1MAX', 'G2DIF'),
            dest='dif',
            nargs=3,
            default=None,
            type=float)

    parser.add_argument(
            '-d',
            help=("Select detuning."),
            dest='det',
            default=0,
            type=float)

    parser.add_argument(
            '-p',
            help=("Perform a cut by defining a pump power region with "
                  "'PMIN <= ppump <= PMAX'"),
            metavar=('PMIN', 'PMAX'),
            dest='pump',
            nargs=2,
            default=(-200, 50),
            type=float)

    parser.add_argument(
            '-f',
            dest='fr0',
            help='fr0 selection, nearest value is used.',
            type=float,
            default=0)

    parser.add_argument(
            '--ylim',
            help="Pump limits used in plotting only",
            nargs=2,
            type=float,
            default=None)
    return parser


parser = setup_parser()

parsed = parser.parse_args()
if parsed.dif is None:
    print("Supply -c2 argument")


# selected frequency
FR = parsed.fr0
DETUNING = parsed.det
G1G2DIFF = parsed.dif[2]
G1MIN = parsed.dif[0]
G1MAX = parsed.dif[1]
G2MIN = G1MIN

PPMIN = parsed.pump[0]
PPMAX = parsed.pump[1]
YLIM = parsed.ylim  # used for plotting


path = parsed.path
outdir = Path(".")


ds = xr.open_dataset(path)
ds.ppump.attrs['long_name'] = "$ P_\mathrm{pump} $"
ds.ppump.attrs['units'] = "dBm"
bw0 = ds.bw0.mean().values
ds.detuning.attrs['long_name'] = f"$ \delta ({bw0/2e6:.1f} \mathrm{{MHz}})^{{-1}} (f_p/2 - f_r)$"
nu1 = ds.attrs['fo1']/1e3
nu2 = ds.attrs['fo2']/1e3
ds.g1.attrs['long_name'] = f"$ G({nu1:.0f} \mathrm{{kHz}}) $"
ds.g2.attrs['long_name'] = f"$ G({nu2:.0f} \mathrm{{kHz}}) $"
ds.g1.attrs['units'] = 'dB'
ds.g2.attrs['units'] = 'dB'


# Set the fr0 as coordinate instead of flux bias
ds = ds.swap_dims({'ib': 'fr0'})


# Select the bias point with the resonance closest to the picked frequency FR.
ds0 = ds.sel(fr0=FR, method='nearest')


# Point Selection

# Selection based on g2 and g1 difference
g2diff = ds0.g1 - G1G2DIFF
ds1 = ds0.where(ds0.g2 > g2diff)

# Selection based on minimum and maximum g1 and g2
ds2 = ds1.where(ds1.g1 >= G1MIN).where(ds1.g1 <= G1MAX)


# Selection based on detuning
dssel = ds2.sel(detuning=[DETUNING], method='nearest')

# below is computed just for plotting
detdelta = ds.detuning.diff(dim='detuning')[0]
ds3 = ds2.where(
        ds2.detuning > DETUNING - detdelta/2).where(
                ds2.detuning < DETUNING + detdelta/2)


# Interpolation
dsselfin = dssel.interpolate_na(dim='ppump', max_gap=0.9)
ds4 = ds3.interpolate_na(dim='ppump', max_gap=0.9)
# (gap is defined in terms of the dim parameter's values)


# Selection based on minimum maximum pump powers
ds4 = ds4.where(ds4.ppump >= PPMIN).where(ds4.ppump <= PPMAX)
dsselfin = dsselfin.where(dsselfin.ppump >= PPMIN).where(dsselfin.ppump <= PPMAX)


# Plotting
for obj in [ds2, ds4, dsselfin]:
    obj.g1.attrs = ds.g1.attrs
    obj.g2.attrs = ds.g2.attrs

fig1, ax = plt.subplots()
ds2.g1.plot.pcolormesh(
                add_colorbar=False, cmap='gray', vmin=0, vmax=G1MAX)
ds4.g1.plot.pcolormesh(vmin=0, vmax=G1MAX)
ax.set_ylim(YLIM)

# for some reason dataarray fails when setting this
ax.set_xlabel(ds2.detuning.attrs['long_name'])

fig2, ax = plt.subplots()
dsselfin.g1.plot(label=dsselfin.g1.attrs['long_name'], marker='*')
dsselfin.g2.plot(label=dsselfin.g2.attrs['long_name'], marker='^')
ax.set_ylabel('G [dB]')
ax.legend()

plt.show()

# Saving
name = f'fr{FR/1e9:.3f}G-delta{DETUNING:.1f}'

dssqz = dsselfin.where(~dsselfin.g1.isnull(), drop=True)
df = dssqz.to_dataframe().drop(['g1m', 'g2m'], axis=1).reset_index(
        ['detuning', 'ppump'])
df['fp'] = (df.detuning*df.bw0/2 + df.fr0)*2

df = df.rename(columns={'ppump': 'pp', 'detuning': 'det'})
colorder = ['ib', 'fp', 'pp', 'det', 'g1', 'g2']
dfreordered = df[colorder]
dfreordered.to_csv(outdir/f'{name}.csv', index=False)

fig1.savefig(outdir/f'{name}_pumpswpsel_paramap.png', dpi=300)
fig2.savefig(outdir/f'{name}_pumpswpsel_gains.png', dpi=300)

