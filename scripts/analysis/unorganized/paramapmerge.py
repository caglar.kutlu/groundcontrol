from datargs import parse, argsclass, arg
from pathlib import Path

import xarray as xr


@argsclass
class Opts:
    file: Path = arg(positional=True, nargs='+')
    # it's actually t.List[Path]


opts = parse(Opts)

fnames = opts.file

print("Collecting datasets.")
ds_l = [xr.open_dataset(fname) for fname in fnames]


print("Merging datasets.")
ds = xr.concat(ds_l, dim='ib')


fname = 'mergedparamap.nc'
print(f"Writing to file {fname}")
ds.to_netcdf(fname)
