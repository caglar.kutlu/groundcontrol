import xarray as xr
from pathlib import Path
import matplotlib.pyplot as plt

from datargs import arg, argsclass, parse


@argsclass
class ProgOpts:
    path: Path = arg(
            positional=True,
            help='Path to paramap netcdf file.')


def main(opts):
    fname = opts.path

    ds = xr.open_dataset(fname)

    outg1 = Path('./g1')
    outg2 = Path('./g2')
    outg1.mkdir()
    outg2.mkdir()

    fig, ax = plt.subplots()
    fig: plt.Figure

    for i, ib in enumerate(ds.ib):
        ib = ib.item()
        dss = ds.isel(ib=i).dropna(dim='ppump', how='all')
        freq = dss.fr0.item()
        fname = f"{freq}.png"
        print(f"({i}) Plotting G1 of ib={ib:.3e} A (fr0={freq/1e9:.3f} GHz).")
        addcbar = i == 0
        dss.g1.plot.pcolormesh(
                vmin=10, vmax=40, levels=31, ax=ax,
                add_colorbar=addcbar)
        fig.savefig(outg1/fname, dpi=300)

        plt.close('all')

        print(f"Plotting G2 of ib={ib:.3e} A (fr0={freq/1e9:.3f} GHz).")
        dss.g2.plot.pcolormesh(
                vmin=10, vmax=40, levels=31, ax=ax,
                add_colorbar=False)
        fig.savefig(outg2/fname, dpi=300)
        plt.close('all')


if __name__ == "__main__":
    main(parse(ProgOpts))
