#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pathlib import Path
import argparse
import typing as t

import scipy.constants as cnst
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import numpy as np

from groundcontrol.analysis.nt import noise_psd


def setup_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('path', type=Path, help="Path to measurement.")
    parser.add_argument(
            'ntref', type=Path,
            default=None,
            help="Reference noise measurement")
    parser.add_argument(
            '-T', '--fridge-temperature', type=float,
            help=("Temperature of the fridge.  "
                  "More precisely, physical temperature "
                  "of the element that supply the noise to the JPA."))
    parser.add_argument(
            '-p', '--plot', type=str,
            choices=['tn', 'tnfactor', 'quanteff'],
            default='tn')
    return parser


def read_nt_file(csvfile):
    df = pd.read_csv(csvfile, dtype=np.float)
    f, g, gerr, tn, tnerr = df.values.T
    ds = xr.Dataset({
        'gain': (('f',), g),
        'gain_err': (('f',), gerr),
        'tn': (('f',), tn),
        'tnerr': (('f',), tnerr)},
        coords={'f': ('f', f)})
    return ds


def main(path: Path, nref: Path, t_fridge: t.Optional[float]):
    t_fridge = 0.04 if t_fridge is None else t_fridge
    ds = xr.open_dataset(path).transpose()

    dmin, dmax, dstep = -2, 2, 0.2
    delta_bins = np.arange(dmin, dmax + 2*dstep, dstep) - dstep/2
    delta_bin_centers = delta_bins[:-1] + dstep/2

    regrpd = ds.groupby_bins(
            'delta', delta_bins, labels=delta_bin_centers,
            restore_coord_dims=True)
    aggrd = regrpd.mean().transpose().rename({'delta_bins': 'delta'})
    interpd = aggrd.interpolate_na(dim='ppump')

    tnoffds = pd.read_csv(
            nref,
            names=['f', 'gain', 'gain_std', 'tn', 'tn_std'],
            comment='#').set_index('f').to_xarray().sel(
                    f=interpd.fcenter, method='nearest')

    dsrat = interpd.assign({'r': interpd.snr_on/interpd.snr_off})
    tnf = noise_psd(t_fridge, interpd.fcenter)/cnst.Boltzmann
    dsfin = dsrat.assign({'tn': (1/dsrat.r)*(tnf + tnoffds.tn) - tnf})
    tquantum = cnst.Planck*dsfin.f/(2*cnst.Boltzmann)
    dsfin = dsfin.assign(
            {'tnfactor': dsfin.tn/tquantum,
             'quanteff': tquantum/(dsfin.tn + tquantum)})

    dsfin.tn.attrs['long_name'] = "$ T_n $"
    dsfin.tnfactor.attrs['long_name'] = "$ T_n/T_Q $"
    dsfin.quanteff.attrs['long_name'] = "$ \eta_Q = T_Q/(T_n + T_Q) $"
    dsfin.tn.attrs['units'] = "K"
    dsfin.ppump.attrs['long_name'] = "$ P_\mathrm{pump} $"
    dsfin.ppump.attrs['units'] = "dBm"
    bw0 = dsfin.bw0.mean().values
    dsfin.delta.attrs['long_name'] = f"$ \delta ({bw0/2e6:.1f} \mathrm{{MHz}})^{{-1}} (f_p/2 - f_r)$"

    return ds, dsrat, tnf, tnoffds, dsfin


def plot_tn(ds, cmap='inferno_r'):
    ds.tn.plot.pcolormesh(vmin=0, vmax=2, levels=21, cmap=cmap)


def plot_tn_factor(ds, cmap='inferno_r'):
    # Added noise temperature q limit
    ds.tnfactor.plot.pcolormesh(vmin=1, vmax=31, levels=16, cmap=cmap)


def plot_quantum_efficiency(ds, cmap='inferno'):
    ds.quanteff.plot.pcolormesh(vmin=0, vmax=0.5, levels=21, cmap=cmap)


if __name__ == "__main__":
    parser = setup_parser()
    parsed = parser.parse_args()
    ds, dsrat, tnf, tnoffds, dsfin = main(
            parsed.path, parsed.ntref,
            parsed.fridge_temperature)

    if parsed.plot == 'tn':
        plot_tn(dsfin)
    elif parsed.plot == 'tnfactor':
        plot_tn_factor(dsfin)
    elif parsed.plot == 'quanteff':
        plot_quantum_efficiency(dsfin)

    fig = plt.gcf()
    ax = plt.gca()
    # for some reason xarray fails to print label properly
    ax.set_xlabel(dsfin.delta.attrs['long_name'])
    fig.savefig(f"{parsed.path.stem}_{parsed.plot}.png", dpi=300)

    plt.show()
