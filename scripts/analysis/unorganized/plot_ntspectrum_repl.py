import sys
import typing as t
from functools import partial

import xarray as xr
import matplotlib as mpl
import matplotlib.pyplot as plt


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


def plotdouble(
        idx: t.Iterable,
        ds: xr.Dataset,
        figsize=(19.20, 10.80)):
    fig, ax = plt.subplots(figsize=figsize)
    sliced = ds.isel(timestamp=idx)
    sliced.tn_est2.plot.line(x='frequencies', ax=ax)
    ax2 = ax.twinx()
    ax2.tick_params(axis='y', labelcolor='red')
    ax2.set_ylabel(ax2.get_ylabel(), color='red')
    sliced.g_j.plot.line(
            x='frequencies', ax=ax2, linestyle='-.', linewidth=2,
            add_legend=False)
    ax.grid(True, linestyle='-.')
    return fig, [ax, ax2]


fname = sys.argv[1]


ds = xr.open_dataset(fname)


plotdouble = partial(plotdouble, ds=ds)
