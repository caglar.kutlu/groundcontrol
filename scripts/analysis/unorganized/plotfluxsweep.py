from pathlib import Path

import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

from datargs import parse, arg, argsclass


@argsclass
class ProgOpt:
    path: Path = arg(positional=True)
    swapaxes: bool = arg(default=False, aliases=['-s'])


def makefunctions(ds):
    def flux2ib(flux):
        return np.interp(flux, ds.flux.values, ds.ib.values)

    def ib2flux(ib):
        return np.interp(ib, ds.ib.values, ds.flux.values)

    return flux2ib, ib2flux


def plot_unswapped(nds, ax):
    nds.swap_dims(ib='flux').fr.plot(
            ax=ax,
            marker='o', linewidth=1, markersize=5, color='k', linestyle='')
    ax.set_xlabel(r"$ \phi / \phi_0 $")
    ax.set_ylabel("$ f_r $ (GHz)")
    ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(0.5))
    xticks = ax.get_xticks()

    flux2ib, ib2flux = makefunctions(nds)

    secax = ax.secondary_xaxis('top', functions=(flux2ib, ib2flux))
    secax.set_xlabel(r"Coil current (mA)")
    secax.set_xticks(flux2ib(xticks))
    ax.set_xlim((-1, 1))


def plot_swapped(nds, ax):
    nds.fr.plot(
            ax=ax,
            marker='o', linewidth=1, markersize=5, color='k', linestyle='')
    ax.set_xlabel(r"Coil current (mA)")
    ax.set_ylabel("$ f_r $ (GHz)")
    xticks = ax.get_xticks()
    flux2ib, ib2flux = makefunctions(nds)

    secax = ax.secondary_xaxis('top', functions=(ib2flux, flux2ib))
    secax.set_xlabel(r"$ \phi / \phi_0 $")
    secax.set_xticks(ib2flux(xticks))
    secax.set_xlim((-1, 1))


def main(opts):
    ds = xr.open_dataset(opts.path)

    nds = ds
    nds['fr'] = nds.fr/1e9

    nds.coords['ib'] = nds.ib*1e3
    nds.ib.attrs['units'] = 'mA'

    fig, ax = plt.subplots()
    # nds.fr.plot(
    #         ax=ax,
    #         marker='o', linewidth=1, markersize=5, color='k', linestyle='')
    # ax.set_xlabel(r"$ \phi / \phi_0 $")
    # ax.set_ylabel("$ f_r $ (GHz)")
    # ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(0.5))
    # xticks = ax.get_xticks()

    # flux2ib, ib2flux = makefunctions(nds)

    # secax = ax.secondary_xaxis('top', functions=(flux2ib, ib2flux))
    # secax.set_xlabel(r"Coil current (mA)")
    # secax.set_xticks(flux2ib(xticks))
    # ax.set_xlim((-1, 1))
    if opts.swapaxes:
        plot_swapped(nds, ax)
    else:
        plot_unswapped(nds, ax)

    plt.tight_layout()
    plt.show()

    fig.savefig(Path(opts.path.name).with_suffix('.png'), dpi=300)


if __name__ == "__main__":
    opts = parse(ProgOpt)
    main(opts)
