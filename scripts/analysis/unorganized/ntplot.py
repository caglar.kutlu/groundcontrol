import typing as t
from pathlib import Path

from datargs import argsclass, arg
from datargs import parse as parseargs
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np

from groundcontrol.util import lin2db_pow


@argsclass
class Opts:
    path: Path = arg(positional=True)
    gain_ylim: float = arg(
            nargs=2, metavar=('MIN', 'MAX'), aliases=['-G'],
            default=None)
    tn_ylim: float = arg(
            nargs=2, metavar=('MIN', 'MAX'), aliases=['-T'],
            default=None)
    title: str = ""
    nrep: int = 0
    save: bool = False


def plot_tn(ds, ax=None):
    if ax is None:
        ax = plt.gca()

    ax.errorbar(
            ds.frequencies,
            ds.tn, ds.tn_err, errorevery=10, color='black', marker='o')
    xname = ds.frequencies.attrs['long_name']
    xunit = ds.frequencies.attrs['units']
    yname = ds.tn.attrs['long_name']
    yunit = ds.tn.attrs['units']
    ax.set_xlabel(f"{xname} ({xunit})")
    ax.set_ylabel(f"{yname} ({yunit})")

    return ax


def plot_gain(ds, ax=None, color='red'):
    if ax is None:
        ax = plt.gca()

    gjdb = lin2db_pow(ds.gain)
    gjdbmin = lin2db_pow(ds.gain-ds.gain_err)
    gjdbmax = lin2db_pow(ds.gain+ds.gain_err)
    gjdberr = [
            (gjdb-gjdbmin).values,
            (gjdbmax-gjdb).values]

    ax.errorbar(
            ds.frequencies,
            gjdb, yerr=gjdberr,
            errorevery=10, color=color, marker='x')
    xname = ds.frequencies.attrs['long_name']
    xunit = ds.frequencies.attrs['units']
    yname = ds.gain.attrs['long_name']
    yunit = 'dB'
    ax.set_xlabel(f"{xname} ({xunit})")
    ax.set_ylabel(f"{yname} ({yunit})")
    return ax


def main(opts: Opts):
    ds = xr.open_dataset(opts.path)
    ds = ds.sel(nrep=opts.nrep)

    fig, ax = plt.subplots()
    ax2 = ax.twinx()
    plot_tn(ds, ax)
    plot_gain(ds, ax2)
    ax2.tick_params(axis='y', labelcolor='red')
    ax2.set_ylabel(ax2.get_ylabel(), color='red')
    ax.grid(True, linestyle='-.')

    if opts.tn_ylim is not None:
        ax.set_ylim(opts.tn_ylim)

    if opts.gain_ylim is not None:
        ax2.set_ylim(opts.gain_ylim)

    ax.set_title(opts.title)

    if opts.save:
        suffixes = opts.path.suffixes
        name = Path(opts.path.stem).stem  # do a foldl instead
        fname = Path(''.join([f"{name}__{opts.nrep}"] + suffixes))
        fig.savefig(fname.with_suffix('.png'), dpi=300)

    plt.show()


if __name__ == "__main__":
    main(parseargs(Opts))
