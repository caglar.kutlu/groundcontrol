from pathlib import Path
from datetime import datetime
import argparse
import sys

import attr
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import scipy.constants as cnst
import pandas as pd

from groundcontrol.analysis.nt import noise_psd
from groundcontrol.util import makeodd, db2lin_pow, dbm2watt
from groundcontrol.declarators import quantity, parameter
import groundcontrol.units as un
from groundcontrol.measurement import read_measurement_csv, MeasurementModel
from groundcontrol.helper import Array


class SNRSpectrum(MeasurementModel):
    frequencies: Array[float] = quantity(un.hertz, 'Frequency')
    snrs: Array[float] = quantity(un.ratio_ww, 'SNR')
    noisepows: Array[float] = quantity(un.dBm, 'NoisePower')
    rbw: float = parameter(un.hertz, "RBW")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


def setup_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('path', type=Path, help="Path to measurement.")
    parser.add_argument(
            'ntref', type=Path,
            default=None,
            help="Reference noise measurement")
    parser.add_argument(
            '-T', '--fridge-temperature', type=float, required=True,
            help=("Temperature of the fridge.  "
                  "More precisely, physical temperature "
                  "of the element that supply the noise to the JPA."))
    parser.add_argument('--max-measurement', type=int, default=int(1e6))
    return parser


def read_baseline(datadir):
    # Transmission measurement for baseline
    bl = read_measurement_csv(
            datadir/f"SPPM/SPPM_BASELINE_0.csv").to_xarray(
                    coords='frequencies')
    return bl


def remove_baseline(trans, bl):
    # rbl = bl.reindex_like(trans).interpolate_na(
    #         dim='frequencies').dropna(dim='frequencies')
    rbl = bl.interp_like(trans)
    rbla, trga = xr.align(rbl, trans)
    return trga - rbla


def remove_degenerate(gj, fp, interpolate=True):
    fpeak = fp/2
    fdeg = gj.sel(
            frequencies=fpeak, method='nearest').frequencies.values

    dropped = gj.drop_sel(frequencies=fdeg)

    if interpolate:
        gjn = dropped.reindex_like(gj).interpolate_na(
                dim='frequencies')
    else:
        gjn = dropped
    return gjn


def smoothify(
        ds,
        band,
        coord,
        porder=3,
        drop_band_ratio=0):
    cvals = ds.coords[coord]
    df = cvals[1] - cvals[0]
    wl = makeodd(int(band // df))
    try:
        datmp = ds.to_array()
        sgarr = savgol_filter(
                datmp, window_length=wl,
                polyorder=porder, mode='mirror')
        datmp[:] = sgarr
        dssg = datmp.to_dataset('variable')
    except ValueError as e:
        if e.args[0].startswith('polyorder'):
            print(f"DF: {df}, WL: {wl}, PO: {porder}, BAND: {band}")
        raise e
    dropband = drop_band_ratio * band
    cvals_max = cvals.max() - dropband/2
    cvals_min = cvals.min() + dropband/2
    return dssg.where(
            (cvals >= cvals_min)*(cvals <= cvals_max))


def read_nt_dataset(csvfile):
    df = pd.read_csv(csvfile, dtype=np.float)
    f, g, gerr, tn, tnerr = df.values.T
    ds = xr.Dataset({
        'gain': (('frequencies',), g),
        'gain_err': (('frequencies',), gerr),
        'tn': (('frequencies',), tn),
        'tnerr': (('frequencies',), tnerr)},
        coords={'frequencies': ('frequencies', f)})
    return ds


def read_data(datadir, mindex):
    # Tuning Point Information
    tup = read_measurement_csv(
            datadir/f"TP/TP_i{mindex:d}_{5*mindex+1:d}.csv")
    # Transmission maeasurement for resonance estimation
    trres = read_measurement_csv(
            datadir/f"SPPM/SPPM_RESONANCE-i{mindex:d}_{5*mindex+2:d}.csv").to_xarray(
                    coords='frequencies')
    # Transmission maeasurement for gain when JPA is on
    trg = read_measurement_csv(
            datadir/f"SPPM/SPPM_GAIN-i{mindex:d}_{5*mindex+3:d}.csv").to_xarray(
                    coords='frequencies')
    # Scanned SNR spectrum when JPA is on
    snrson = read_measurement_csv(
            datadir/f"SNRS/SNRS_ON-i{mindex:d}_{5*mindex+4:d}.csv").to_xarray(
                    coords='frequencies')
    # Scanned SNR spectrum when JPA is off
    snrsoff = read_measurement_csv(
            datadir/f"SNRS/SNRS_OFF-i{mindex:d}_{5*mindex+5}.csv").to_xarray(
                    coords='frequencies')
    return tup, trres, trg, snrson, snrsoff


def read_dataset(datadir, mindex, baseline, nt_ref_ds, t_fridge):
    bl = baseline
    ntds = nt_ref_ds
    tup, trres, trg, snrson, snrsoff = read_data(datadir, mindex)

    gj = remove_baseline(trg, bl)
    gj.mags.attrs['long_name'] = "$ G_J $"
    gj.mags.attrs['units'] = un.dB

    gjn = remove_degenerate(gj, tup.pump_frequency)

    gjns = smoothify(gjn, 100e3, 'frequencies')

    # REINDEXING
    ntdsi = ntds.interp_like(snrson)
    gjnsi = gjns.interp_like(snrson)

    # COMPUTING VIA SNRCOMP METH #1
    rcomp = snrsoff.snrs / snrson.snrs
    # Fridge equiv noise temp
    tnf = noise_psd(t_fridge, rcomp.frequencies)/cnst.Boltzmann
    tnf = xr.DataArray(tnf, [('frequencies', rcomp.frequencies.values)])

    tn1 = rcomp*(tnf + ntdsi.tn) - tnf

    # COMPUTING VIA SNRCOMP METH #2
    zet = snrson.noisepows / snrsoff.noisepows
    rcomp = zet / db2lin_pow(gjnsi.mags)

    tn2 = rcomp*(tnf + ntdsi.tn) - tnf

    # Combining everything into a dataset
    noise_on = snrson.noisepows
    noise_off = snrsoff.noisepows
    snr_on = snrson.snrs
    snr_off = snrsoff.snrs
    g_j = gjnsi.mags
    g_j_raw = gj.mags.interp_like(snrson)
    tn_est1 = tn1
    tn_est2 = tn2
    tn_ref = ntdsi.tn

    ds = xr.Dataset({
        'noise_on': (noise_on/snrson.attrs['rbw']).drop('timestamp'),
        'noise_off': (noise_off/snrsoff.attrs['rbw']).drop('timestamp'),
        'snr_on': (snr_on).drop('timestamp'),
        'snr_off': (snr_off).drop('timestamp'),
        'g_j': g_j,
        'g_j_raw': g_j_raw,
        'tn_est1': tn_est1,
        'tn_est2': tn_est2,
        'tn_ref': tn_ref
        })  # This is done to mitigate timestamp discrepancy
    ds.noise_on.attrs['units'] = 'W/Hz'
    ds.noise_on.attrs['long_name'] = "$ N_{\mathrm{ON}} $"
    ds.noise_off.attrs['units'] = 'W/Hz'
    ds.noise_off.attrs['long_name'] = "$ N_{\mathrm{OFF}} $"
    ds.snr_on.attrs['units'] = 'W/W'
    ds.snr_on.attrs['long_name'] = "$ R_{\mathrm{ON}} $"
    ds.snr_off.attrs['units'] = 'W/W'
    ds.snr_off.attrs['long_name'] = "$ R_{\mathrm{OFF}} $"
    ds.g_j.attrs['units'] = 'W/W'
    ds.g_j.attrs['long_name'] = '$ G_J $'
    ds.g_j_raw.attrs['units'] = 'W/W'
    ds.g_j_raw.attrs['long_name'] = '$ G_J $'
    ds.tn_est1.attrs['units'] = 'K'
    ds.tn_est1.attrs['long_name'] = '$ T_n^{(1)} $'
    ds.tn_est2.attrs['units'] = 'K'
    ds.tn_est2.attrs['long_name'] = '$ T_n^{(2)} $'
    ds.tn_ref.attrs['units'] = 'K'
    ds.coords['timestamp'] = (
            'timestamp', [tup.timestamp],
            {
                'long_name': 'Time',
                'description': "Time when tuning point is recorded."
                })
    ds.coords['fr'] = (
            'timestamp', [tup.resonance],
            {'long_name': '$ f_r $',
                'description': "Resonance frequency",
                'units': 'Hz'})
    ds.coords['ib'] = (
            'timestamp', [tup.bias_current],
            {'long_name': '$ i_b $',
                'units': 'A'})
    ds.coords['fp'] = (
            'timestamp', [tup.pump_frequency],
            {'long_name': '$ f_p $', 'units': "dBm"})
    ds.coords['pp'] = (
            'timestamp', [tup.pump_power],
            {'long_name': '$ P_p $', 'units': "dBm"})
    return ds


def main(datapath, ntref, maxmeas, fridgetemp):
    baseline = read_baseline(datapath)
    ntds = read_nt_dataset(ntref)

    dss = []
    for i in range(maxmeas):
        try:
            print(f"Reading measurement: {i:d}.")
            ds = read_dataset(datapath, i, baseline, ntds, fridgetemp)
            dss.append(ds)
        except FileNotFoundError:
            print(f"Total number of measurements {i}.")
            break

    dsc = xr.concat(dss, dim='timestamp')
    dsc.coords['frequencies'].attrs['units'] = "Hz"
    dsc.to_netcdf(f"{datapath.name!s}.nc")
    return dsc


if __name__ == "__main__":
    parser = setup_parser()
    parsed = parser.parse_args()
    dsc = main(
            parsed.path, parsed.ntref,
            parsed.max_measurement, parsed.fridge_temperature)
