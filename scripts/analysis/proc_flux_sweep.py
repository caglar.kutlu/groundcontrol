"""Processes flux_sweep.py data.  Tries to estimate the parameters for flux
calibration relying on the periodicity of resonance frequency versus coil
current.

The general form of the expected relation is:
    phi = a*ib + b

where a is referred as the `ibflux` and b is referred as `ib0` within the
context of this script.  `ibflux` corresponds to the current required to add
one flux to the SQUID loop inside the JPA, whereas `ib0` roughly corresponds to
the trapped flux.
    """
import argparse
from pathlib import Path
import typing as t

import numpy as np

from groundcontrol.measurement import read_measurement_csv
from groundcontrol.estimation import make_resonance_map_dataarray

__version__ = '0.1'
__description__ = __doc__


def setup_parser():
    parser = argparse.ArgumentParser(description=__description__)

    parser.add_argument(
            'path',
            type=Path,
            help="Path to data.")

    parser.add_argument(
            '-o',
            '--out',
            type=Path,
            default=None,
            help="Output filename.")

    parser.add_argument(
            '-l',
            '--linspace',
            nargs=3,
            type=float,
            default=None,
            help="""Frequency values to be used when creating a current set.
            (MIN, MAX, NCOUNT)""")

    parser.add_argument(
            '--csv',
            type=Path,
            default=Path('ibset.csv'),
            help="""Filename for the csv produced based on the specified
            cut.""")

    parser.add_argument(
            '--ibflux',
            type=float,
            default=None,
            help="""Flux-current slope in units of 'ibunit'/phi0.
            Flux is calibrated using this if provided.""")

    parser.add_argument(
            '--refc',
            type=float,
            default=None,
            help="""Current closest to 0 phi0.  Estimated from the median of ib
            values if not provided.""")

    parser.add_argument(
            '--rval',
            type=float,
            default=0.8,
            help="""r value to be used when estimating the slope term.  Should
            have a value between 0.5 and 0.99.  Defaults to 0.8.""")

    return parser


def linspace(da, fmin, fmax, ncount):
    """Creates current mesh linearly spaced in frequency."""
    fluxcomputed = ~np.all(np.isnan(da.flux.values))
    if fluxcomputed:
        dahalf = da[(da.flux <= 0)*(da.flux >= -0.5)]
        da_ = (dahalf
                .swap_dims({'ib': 'fr'})
                .reset_coords('ib'))
    else:
        maxind = da.values.argmax()
        ibmax = da.ib.values[maxind]
        daa = da.where(da.ib <= ibmax).dropna(dim='ib')
        da_ = daa.swap_dims({'ib': 'fr'}).reset_coords('ib')
    # Create the space
    frspace = np.linspace(fmin, fmax, ncount)
    # Interpolate the dataarray at the created mesh points
    damesh = da_.interp(fr=frspace)
    return damesh


def main(
        path: Path, out: Path, opt_ls: t.Optional[t.Tuple],
        csvpath: Path, ibflux: t.Optional[float],
        refc: t.Optional[float], rval: float):
    remfile = next((path / 'ReM').glob('ReM_*.csv'))
    rm = read_measurement_csv(remfile)

    ibarr = np.ma.array(rm.current, mask=np.isnan(rm.frequency)).compressed()
    frarr = np.ma.array(rm.frequency, mask=np.isnan(rm.frequency)).compressed()

    da = make_resonance_map_dataarray(
            ibarr, frarr, ibflux=ibflux, refc=refc, r=rval)

    da.to_netcdf(out)

    if opt_ls is not None:
        # simplest implementation
        damesh = linspace(da, opt_ls[0], opt_ls[1], int(opt_ls[2]))
        damesh.drop('flux').to_dataframe().to_csv(
                csvpath.absolute().as_posix())


if __name__ == "__main__":
    parser = setup_parser()
    parsed = parser.parse_args()
    if parsed.out is None:
        out = f"{parsed.path.name}.rmap.nc"
    else:
        out = parsed.out
    main(parsed.path, out, parsed.linspace, parsed.csv, parsed.ibflux, parsed.refc, parsed.rval)
