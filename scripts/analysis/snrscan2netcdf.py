#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Convert jpasnrscan data to netcdf format and save."""
from pathlib import Path
import os

import xarray as xr
from datargs import argsclass, arg, parse

from groundcontrol.analysis.jpasnrscan import read_dataset
from groundcontrol.logging import DEBUG, set_stdout_loglevel


isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


@argsclass
class Opts:
    path: Path = arg(positional=True)
    out: Path = arg(
            aliases=['-o'],
            default=None)


def main(opts: Opts):
    rawds = read_dataset(opts.path)
    rawds.to_netcdf(opts.out)


if __name__ == "__main__":
    opts = parse(Opts)
    if opts.out is None:
        here = Path('.')
        opts.out = here / f"{opts.path.name}.nc"
    if opts.out.exists():
        raise ValueError(f"File exists. ({opts.out!s})")
    main(opts)
