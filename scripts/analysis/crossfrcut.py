"""A script to perform cuts on paramap datasets.  The dataset must be a .nc
file as produced by `paramap2netcdf.py` from a result of measurements performed
via `paramap.py`.

Example Usage:
    $ python3 scripts/analysis/crossfrcut.py myparamapdata.nc -c 15 25 2 \\
        --gain-edges $(seq -s ' ' 15 25) \\
        --frequency-edges $(seq -s ' ' 5.7e9 20e6 6.05e9) --plot

    This command will perform the following cuts on the dataset (in that
    order):
    1. Select data points obeying 15<g1<25 and g1 - g2 <= 2 (dB).  g1 is the
       first gain measurement offset, which usually represents the maximum
       gain.  g2 is the gain at the 2nd offset.  i usually take it to be 100
       kHz out of habit.  g1 - g2 <= 3 would have the meaning "points that have
       at least 200 kHz bandwidth" in my case for example.  Using 2 dB instead
       of 3 dB just makes that argument stronger.
    2. Group the datapoints in the bins defined by the g1 intervals {[15, 16),
       [16, 17), ... , [24, 25)}.  After grouping, select the point with
       minimum pump power for each group.  (seq -s ' '15 25 will generate the
       set {15, 16, 17,...,25}).
    3. Group the datapoints in the bins defined by the fpeak (fp/2) intervals
       {[5.7e9, 5.720e9), [5.720e9, 5.740e9), ... , [6.03e9, 6.05e9)}.  After
       grouping, select the point with minimum pump power for each group.
       (seq -s ' ' 5.7e9 20e6 6.05e9 will generate the set {5.7e9, 5.720e9,
       5.740e9,..., 6.05e9}).

    After performing the cuts, the multidimensional data will be flattened into
    a table and saved as a csv file 'out.csv' in this case.  This file then can
    be used in jpasnrscan.py or jpasatscan.py .  I made this coarse frequency
    cut to use in jpasatscan for example.  The --plot argument is, well for
    doing a simple plot of the resulting cut.

"""
from pathlib import Path
import argparse

import xarray as xr
import pandas as pd
import matplotlib.pyplot as plt


def setup_parser():
    parser = argparse.ArgumentParser(
            description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
            'path',
            help='Path to paramap netcdf file.',
            type=Path)

    parser.add_argument(
            '-c',
            help=("Perform a cut by defining a region with "
                  "'G1MIN <= g1 <= G1MAX, g1-g2 >= G2DIF' where "
                  "g1 is the gain at 1st frequency offset and g2 "
                  "is the gain at the 2nd frequency offset."),
            metavar=('G1MIN', 'G1MAX', 'G2DIF'),
            dest='dif',
            nargs=3,
            default=(10, 30, 3),
            type=float)

    parser.add_argument(
            '-d',
            help=("Perform a cut by defining a detuning region with "
                  "'DMIN <= detuning <= DMAX'"),
            metavar=('DMIN', 'DMAX'),
            dest='det',
            nargs=2,
            default=(None, None),
            type=float)

    parser.add_argument(
            '-p',
            help=("Perform a cut by defining a pump power region with "
                  "'PMIN <= ppump <= PMAX'"),
            metavar=('PMIN', 'PMAX'),
            dest='pump',
            nargs=2,
            default=(None, None),
            type=float)

    parser.add_argument(
            '--plot',
            action='store_const',
            const=True,
            default=False)

    parser.add_argument(
            '--out', '-o',
            type=Path,
            default=Path('./out.csv'),
            help="Output csv filename.  Defaults to 'out.csv'")

    parser.add_argument(
            '--gain-edges',
            help=("A set of monotone increasing gain values to be used as"
                  "intervals.  The intervals are used to group the dataset and"
                  "aggregate by selecting the minimum pump power data in each "
                  "group."),
            nargs='+',
            type=float,
            default=None)

    parser.add_argument(
            '--frequency-edges',
            help=("A set of monotone increasing frequency values to be used as"
                  "intervals.  The intervals are used to group the dataset and"
                  "aggregate by selecting the minimum pump power data in each "
                  "group."),
            nargs='+',
            type=float,
            default=None)

    return parser


def cut(
        ds0,
        g1min,
        g1max,
        gdelta,
        dmin,
        dmax,
        pmin,
        pmax):

    # detuning cut
    if dmin is not None:
        ds0 = ds0.where((ds0.detuning >= dmin)*(ds0.detuning <= dmax))

    if pmin is not None:
        ds0 = ds0.where((ds0.ppump >= pmin)*(ds0.ppump <= pmax))

    fp = ((ds0.detuning*ds0.bw0/2) + ds0.fr0)*2

    # Amplification center frequency
    fac = fp/2
    ds0.coords['fac'] = fac
    ds0.coords['fp'] = fp

    # Selection based on minimum and maximum g1 and g2
    ds1 = ds0.where(ds0.g1 >= g1min).where(ds0.g1 <= g1max)

    # Cut out where g2 is less than g1-gdelta
    g2diff = ds1.g1 - gdelta
    ds2 = ds1.where(ds1.g2 >= g2diff)

    return ds2


def minpumpdiscretize_deprecate(
        df,
        fbins,
        gbins):
    gcut = pd.cut(df.g1, gbins)

    cuts = []
    for _, grp in df.groupby(gcut):
        fcut = pd.cut(grp.fac, fbins)
        faccut = grp.groupby(fcut).min(key='ppump').set_index(
                'fac', drop=False)
        cuts.append(faccut)

    return pd.concat(cuts)


def main(path, gcut, dcut, pcut, doplot, out, gbe=None, fbe=None):
    ds0 = xr.open_dataset(path)

    print("Performing the cuts.")
    dsc = cut(ds0, *gcut, *dcut, *pcut)

    # flatten
    print("Flattening the dataset.")
    df = (dsc.to_dataframe()
             .reset_index().dropna(how='any', subset=['g1', 'g2']))

    if gbe is not None:
        print("Performing minimization based on provided intervals.")
        df = downsample(df, gbe, fbe)
        df = df.drop(columns=['glevels', 'faclevels'])
    else:
        print("No gain interval is provided, skipping minimization.")

    print("Sorting with respect to resonance frequencies.")
    df = df.sort_values('fr0')

    print(f"Number of points: {len(df):d}")

    if doplot:
        plot_g1(df)
        fig = plt.gcf()
        fig.savefig(out.with_suffix('.png').as_posix(), dpi=300)
        plt.show()

    df_to_csv(df, out)

    return dsc, df


def df_to_csv(df, fname: Path):
    """Saves the DataFrame in a format to be consumed by the snrscan script"""
    order = [
            'ib', 'detuning', 'ppump', 'fr0', 'bw0',
            'g1', 'g2', 'g1m', 'g2m', 'fp']
    mapper = {'detuning': 'det', 'ppump': 'pp'}
    # newnames = [
    #         mapper[name] if name in mapper else name
    #         for name in order]
    dfro = df[order]
    dfrnm = dfro.rename(columns=mapper)

    dfrnm.to_csv(f'{fname.with_suffix(".csv")}', index=False)


def downsample(df, gain_bin_edges, fac_bin_edges=None, head=1):
    """Downsamples the dataframe by selecting working points with the minimum
    power that fall into the groups given by the gain and fac bins.  For each
    bin, the entries are ordered by ppump in ascending order.  The first `head`
    amount of working points are selected starting from the minimum pump
    power."""
    gbe = gain_bin_edges
    fbe = fac_bin_edges

    # Create the mapping index-> gain groups
    glevels = pd.cut(df.g1, gbe, labels=gbe[:-1], include_lowest=True)
    if fbe is None:
        ddf = (df
               .assign(glevels=glevels, faclevels=df.fac)
               .sort_values('ppump', ascending=True)
               .groupby(['faclevels', 'glevels'])
               .head(head))
    else:
        faclevels = pd.cut(df.fac, fbe, labels=fbe[:-1])
        ddf = (df
               .assign(glevels=glevels, faclevels=faclevels)
               .sort_values('ppump', ascending=True)
               .groupby(['faclevels', 'glevels'])
               .head(head))
    return ddf


def plot_g1(df):
    try:
        plt.plot(df.fac, df.g1, 'o')
    except AttributeError:
        plt.plot(df.index, df.g1, 'o')


if __name__ == "__main__":
    parser = setup_parser()
    parsed = parser.parse_args()
    dsc, df = main(
            parsed.path,
            parsed.dif,
            parsed.det,
            parsed.pump,
            parsed.plot,
            parsed.out,
            gbe=parsed.gain_edges,
            fbe=parsed.frequency_edges)
