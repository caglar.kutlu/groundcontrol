import xarray as xr
import argparse
from pathlib import Path


parser = argparse.ArgumentParser()
parser.add_argument(
    'path', type=Path, help='Path to resonance map .nc file.')
parser.add_argument(
    'flux', type=float, help='Flux value.')

args = parser.parse_args()


ds = xr.open_dataset(args.path)

current = (ds
        .swap_dims({'ib': 'flux'})
        .interp(flux=args.flux).ib.values
        )

print(current)
