import typing as t
from pathlib import Path

from datargs import parse, argsclass, arg

from groundcontrol.analysis.ntmeas import RawDataSet, Analyzer, Plotter


@argsclass
class Opts:
    path: Path = arg(positional=True)
    out: Path = arg(help="Output filename.", default=None)
    nbwfactor: float = arg(default=1)
    rawonly: bool = arg(default=False)
    ignoretrans: bool = arg(default=False, help="Ignore transmission measurements")


def main(opts: Opts):
    """Main program. """
    if opts.out is None:
        if opts.rawonly:
            opts.out = Path(f"{opts.path.name}.nc")
        else:
            opts.out = Path(f"{opts.path.name}.proc.nc")

    rds = RawDataSet.from_path(opts.path, opts.nbwfactor, ignoretrans=opts.ignoretrans)

    if opts.rawonly:
        dsa = rds.to_xarray()
    else:
        ana = Analyzer.make(rds.to_xarray())
        dsa = ana.fit_all()

    dsa.to_netcdf(opts.out)

    return dsa


if __name__ == "__main__":
    ds = main(parse(Opts))
