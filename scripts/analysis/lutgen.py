"""LUT generation for JPA tuning.

This script will eventually replace `crossfrcut.py`.

A script to perform cuts on paramap datasets.  The dataset must be a .nc
file as produced by `paramap2netcdf.py` from a result of measurements performed
via `paramap.py`.

Example Usage:
    $ python3 lutgen.py myparamapdata.nc -w 2 \\
        --gain-edges $(seq -s ' ' 15 25) \\
        --frequency-edges $(seq -s ' ' 5.7e9 20e6 6.05e9) --plot

"""
from pathlib import Path

from datargs import parse, arg, argsclass
import xarray as xr

from groundcontrol.analysis.paramap import ParaSelector


@argsclass(description=__doc__)
class Opts:
    ncfile: Path = arg(
            True,
            help="Path to the paramap measurement netcdf file.")
    gain_edges: float = arg(
            nargs='+', aliases=['-g'])
    frequency_edges: float = arg(
            nargs='+', aliases=['-f'])
    nup_fr0: int = arg(
            help='Upsample `fr0` coordinate by this factor.',
            aliases=['-F'],
            default=1)
    nup_det: int = arg(
            help='Upsample `detuning` coordinate by this factor.',
            aliases=['-D'],
            default=1)
    nup_ppump: int = arg(
            help='Upsample `ppump` coordinate by this factor.',
            aliases=['-P'],
            default=1)
    gdelta_max: float = arg(
            help="Maximum for g1-g2.  Defaults to 3",
            aliases=['-G'],
            default=3)
    nhead: int = arg(
            aliases=['-N'],
            help="Number of parameters to keep per LUT bin ordered with "
                 "increasing pump power. "
                 "Defaults to 1.  It means only the parameter set with the "
                 "minimum pump power is kept.",
            default=1)
    csv: Path = arg(default=None)
    compat: bool = arg(
            default=False,
            help="This flag determines whether the csv output is compatible "
                 "with what `jpasnrscan.py` requires for measurement.")
    # hdf: Path = arg(nargs='?', const=True, default=None)


def df_to_compat_csv(df, fname: Path):
    """Saves the DataFrame in a format to be consumed by the `jpasnrscan.py`
    script."""
    order = [
            'ib', 'detuning', 'ppump', 'fr0', 'bw0',
            'g1', 'g2', 'g1m', 'g2m', 'fp']
    mapper = {'detuning': 'det', 'ppump': 'pp'}
    # newnames = [
    #         mapper[name] if name in mapper else name
    #         for name in order]
    dfro = df[order]
    dfrnm = dfro.rename(columns=mapper)

    dfrnm.to_csv(f'{fname.with_suffix(".csv")}', index=False)


def main(args: Opts):
    if len(args.frequency_edges) == 1:
        fedges = args.frequency_edges[0]
    else:
        fedges = args.frequency_edges
    ds = xr.load_dataset(args.ncfile)
    prs = ParaSelector.from_dataset(ds)

    print("Generating LUT.")
    df = prs.generate_lut(
            args.gain_edges,
            fedges,
            args.nup_fr0,
            args.nup_det,
            args.nup_ppump,
            args.gdelta_max,
            args.nhead)

    if args.compat:
        suffix = "LUTCOMPAT"
    else:
        suffix = "LUT"

    defaultcsvname = (
            args.ncfile
                .with_stem(f"{args.ncfile.stem}_{suffix}")
                .with_suffix(".csv"))

    csvname = defaultcsvname if args.csv is None else args.csv

    if args.compat:
        df_to_compat_csv(df, csvname)
    else:
        df.to_csv(csvname)


if __name__ == "__main__":
    args = parse(Opts)
    main(args)
