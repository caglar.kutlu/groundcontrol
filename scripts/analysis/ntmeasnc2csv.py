import xarray as xr
from datargs import argsclass, parse, arg
from pathlib import Path
from groundcontrol.analysis.ntmeas import fitds_to_csv


@argsclass
class Opts:
    path: Path = arg(True)


def main(opts):
    path = opts.path
    ds = xr.open_dataset(path)

    dsavg = ds.mean(dim='nrep')

    outstem = f"{path.stem.split('.')[0]}_avg"

    outpath = (path.parent/outstem).with_suffix('.csv')

    fitds_to_csv(dsavg, outpath)


if __name__ == "__main__":
    opts = parse(Opts)
    main(opts)
