from pathlib import Path
import typing as t
import argparse
import json

import scipy.constants as cnst
import numpy as np
import lmfit
import matplotlib as mpl
import attr
from matplotlib import pyplot as plt

from groundcontrol.analysis.nt import noise_psd  # this is P_n(T, f) above
from groundcontrol.util import dbm2watt, db2lin_pow, lin2db_pow
from groundcontrol.csv import read_csv


MIDFONTSIZE = 14


def setupmpl():
    mpl.rcParams['font.size'] = MIDFONTSIZE
    mpl.rcParams['font.family'] = 'serif'
    mpl.rcParams['xtick.labelsize'] = MIDFONTSIZE
    mpl.rcParams['ytick.labelsize'] = MIDFONTSIZE
    mpl.rcParams['axes.formatter.use_mathtext'] = True
    mpl.rcParams['axes.formatter.useoffset'] = False
    # mpl.rcParams['figure.dpi'] = 300


setupmpl()


@attr.s(auto_attribs=True)
class ModelParams:
    gl: float = db2lin_pow(-0)
    gc1: float = db2lin_pow(-0)
    gc2: float = db2lin_pow(-0)
    gj: float = db2lin_pow(-0)
    tf: float = 0.04

    @property
    def gtot(self):
        return self.gl*self.gc1*self.gc2**2*self.gj


@lmfit.Model
def regmodel(p_in, g, tn):
    return g*(p_in + cnst.Boltzmann*tn)


def read_settings(path):
    with open(path / 'settings.json') as fin:
        return json.load(fin)


@attr.s(auto_attribs=True)
class DataSet:
    temps: t.List  # K
    f_arrs: t.List  # Hz
    psd_arrs: t.List  # W/Hz
    metas: t.List

    @classmethod
    def from_dict(cls, ddict, nbwfactor=1):
        """
        Args:
            ddict: Dictionary of {temperature: datapath, ...}.
            nbwfactor:  Factor for converting the RBW used when measuring
                the data to the noise bandwidth.  This is important to
                have consistent PSD estimate values.  It depends on the
                RBW filter shape, may be provided by the manufacturer.
        """
        farrs = []
        psdarrs = []
        temps = []
        metas = []
        for temp, path in ddict.items():
            # data, meta = read_n9010a_csv(path)
            data, meta = read_csv(path)
            nbw = nbwfactor*float(meta['RBW'].split()[0])
            farr, powarr = data.T
            farrs.append(farr)
            psdestimate = dbm2watt(powarr)/nbw
            psdarrs.append(psdestimate)
            temps.append(temp)
            metas.append(meta)
        return cls(temps, farrs, psdarrs, metas)

    def plot(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
        else:
            fig = plt.gcf()
        for temp, farr, psdarr in zip(
                self.temps, self.f_arrs, self.psd_arrs):
            label = f"$T_s$={temp:.3f}K"
            ax.plot(farr, psdarr, label=label)

        ax.set_xlabel("Frequency [Hz]")
        ax.set_ylabel("PSD [W/Hz]")
        return fig, ax


def _passivetn(gcomp: float, temp: float, frequency: float):
    return (1/gcomp-1)*noise_psd(temp, frequency)/cnst.Boltzmann


def yestimate(pi1, pi2, po1, po2):
    # quick estimate
    y = po2/po1
    tnr = (pi2 - y*pi1)/(cnst.Boltzmann*(y-1))
    gr = (po1-po2)/(pi1-pi2)
    return tnr, gr


def fit(dataset: DataSet):
    ds = dataset

    # columns are temperatures, rows are frequencies
    pinmat = np.array([noise_psd(ts, f_arr)
                       for f_arr, ts
                       in zip(ds.f_arrs, ds.temps)]).T

    psdmat = np.array(ds.psd_arrs).T
    fmat = np.array(ds.f_arrs).T

    results = []
    for fset, psdset, pinset in zip(
            fmat, psdmat, pinmat):
        tnr0, gr0 = yestimate(
            pinset[0], pinset[-1],
            psdset[0], psdset[-1])
        params = regmodel.make_params(g=gr0, tn=tnr0)
        fitresult = regmodel.fit(p_in=pinset, data=psdset, params=params)
        results.append(fitresult)

    return results, fmat[:, 0]


@attr.s(auto_attribs=True)
class NTResult:
    results: lmfit.model.ModelResult
    freqs: np.ndarray

    @classmethod
    def from_results(cls, results, freqs):
        cls(results, freqs)

    @classmethod
    def from_dataset(cls, dataset):
        fr, freqs = fit(dataset)
        return cls(fr, freqs)

    def get_params(self, name: str):
        vals = np.empty_like(self.results, dtype=np.float64)
        stds = np.empty_like(self.results, dtype=np.float64)
        for i, res in enumerate(self.results):
            par = res.params[name]
            val, std = par.value, par.stderr
            vals[i] = val
            stds[i] = std
        return vals, stds

    def plot_params(
            self,
            name: str, ax=None,
            trans: t.Optional[t.Callable] = None, **kwargs):
        vals, stds = self.get_params(name)
        if ax is None:
            fig, ax = plt.subplots()
        else:
            fig = plt.gcf()

        if trans is not None:
            uperror = trans(vals + stds)
            downerror = trans(vals - stds)
            vals = trans(vals)
            yerr = np.vstack([vals-downerror, uperror-vals])
        else:
            yerr = stds

        ax.errorbar(self.freqs, vals, yerr=yerr, **kwargs)
        ax.set_xlabel('Frequency [Hz]')
        return fig, ax

    def save(self, fname: str):
        tn, tnerr = self.get_params('tn')
        g, gerr = self.get_params('g')
        header = ("Frequency[Hz], Gain[W/W], GainErr[W/W], "
                  "NoiseTemperature[K], NoiseTemperatureErr[K]")
        data = np.vstack([self.freqs, g, gerr, tn, tnerr]).T
        np.savetxt(fname, data, header=header, delimiter=',')


def setup_parser():
    parser = argparse.ArgumentParser(
            description="""Analyzer for ntmeas measurements.  This script only
            checks spectrum data, you have to supply ns temperatures.""")

    parser.add_argument(
        'path', type=Path,
        help="Path to measurement.")

    parser.add_argument(
        '-t', type=float,
        nargs='+', required=True, dest='tns',
        help="""Noise source temperatures used during measurement.  Must be
        ordered the same way as is in the experiment.""")

    parser.add_argument(
        '--ylim', type=float,
        nargs=2, default=None,
        help="""Vertical axis limits used in plotting NT.""")
    return parser


def main(path, temperatures, ylim):
    settings = read_settings(path)
    if settings['measuretrans'] is False:
        magic1 = 2
        magic2 = 1
    else:
        magic1 = 3
        magic2 = 2
    nstep = len(temperatures)
    ddicts = (
            {t: path/f"SASM/SASM_i{nstep*i+k}_{magic1*(nstep*i+k)+magic2}.csv"
                for k, t in enumerate(temperatures)}
            for i in range(10000))

    def gendatasets(ddicts):
        for d in ddicts:
            try:
                yield DataSet.from_dict(d)
            except FileNotFoundError:
                break

    datasets = list(gendatasets(ddicts))

    results = [NTResult(*fit(ds))
               for ds
               in datasets]

    figtn, axtn = plt.subplots()
    figg, axg = plt.subplots()
    for i, ntres in enumerate(results):
        ntres.plot_params(
                'g', trans=lin2db_pow, ax=axg, label=f"Trial {i}")
        axg.set_ylabel("G [dB]")
        ntres.plot_params(
                'tn', ax=axtn, label=f"Trial {i}")
        axtn.set_ylabel("$T_n$ [K]")

        if ylim is not None:
            axtn.set_ylim(ylim)

        ntres.save(f'nt_jpaoff{i}.csv')

    axg.legend()
    axtn.legend()
    figg.savefig(f'gain.png', dpi=300)
    figtn.savefig(f'nt.png', dpi=300)
    plt.show()


if __name__ == "__main__":
    parser = setup_parser()
    args = parser.parse_args()
    main(args.path, args.tns, args.ylim)
