import typing as t
from pathlib import Path
import sys

import numpy as np

import groundcontrol
groundcontrol.USE_UNICODE = False
from groundcontrol.instruments import Instrument
from groundcontrol.controllers.jpa import JPAController
from groundcontrol.controller import Controller
from groundcontrol.settings import VNASettings, setting
from groundcontrol.logging import logger
from groundcontrol.measurement import NetworkParams1PModel
from groundcontrol.measurementio import MIOFileCSV


def find_dip(meas: NetworkParams1PModel):
    """Returns the frequency where minimum occurs."""
    f = meas.frequencies
    p = meas.values
    dipi = np.argmin(p)
    return f[dipi]


class MainController(Controller):
    jpac: JPAController

    mio: MIOFileCSV
    current: t.Optional[float] = None

    generic_preset: VNASettings = setting(
            default=VNASettings(
                start_frequency=2.2e9,
                stop_frequency=2.50e9,
                if_bandwidth=0.5e3,
                sweep_step=100e3,
                power=6
                ))

    coarse_preset: VNASettings = setting(
             default=VNASettings(
                            span=20e6,
                            if_bandwidth=1e3,
                            sweep_step=100e3,
                            power=3
                            ))

    narrow_preset: VNASettings = setting(
            default=VNASettings(
                span=20e6,
                if_bandwidth=200,
                sweep_step=100e3,
                power=-20
                ))

    def __setup__(self):
        self.jpac.initiate()
        self.jpac.deactivate_pump()
        self.jpac.activate_signal()

    def find_resonance_coarse(self, mref=None) -> float:
        jpac = self.jpac
        jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.MLOGARITHMIC)

        logger.info("Turning on calibration.")
        jpac.turn_on_vna_calibration()
        jpac.trigger_network_analyzer()
        s21mag = jpac.read_network_mag()
        fres = find_dip(s21mag)

        s21mag.mref = mref
        self.mio.write(s21mag)

        logger.info(f"Found resonance at {fres/1e9:.6f} GHz.")
        return fres

    def measure_phase(self) -> NetworkParams1PModel:
        """Measures a phase spectrum."""
        jpac = self.jpac
        jpac.trigger_network_analyzer()
        return jpac.read_network_phase()

    def zoom_and_measure_phase(self, zoomf: float) -> NetworkParams1PModel:
        jpac = self.jpac
        pre = self.narrow_preset.evolve(center_frequency=zoomf)
        jpac.do_vna_preset(pre)
        jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.UPHASE)
        logger.info("Turning off calibration for phase measurement.")
        jpac.turn_off_vna_calibration()
        return self.measure_phase()

    def run(self):
        jpac = self.jpac
        if self.current is not None:
            jpac.ramp_current(self.current)
        while True:
            # Measured current and error
            cmeas = jpac.measure_current(10)
            logger.debug("Writing current measurement")
            self.mio.write(cmeas)

            logger.info("Performing a coarse resonance search with wide span.")
            jpac.do_vna_preset(self.generic_preset)
            f1 = self.find_resonance_coarse("COARSE-WIDE")

            logger.info("Performing a coarse resonance search "
                        "with narrow span.")
            jpac.do_vna_preset(self.coarse_preset.evolve(center_frequency=f1))
            f = self.find_resonance_coarse("COARSE-NARROW")

            phasemeas = self.zoom_and_measure_phase(f)
            phasemeas.mref = "FINE-NARROW-PHASE"
            logger.debug("Writing fine phase measurement")
            self.mio.write(phasemeas)

            # With same parameters measure the magnitude.
            jpac.turn_on_vna_calibration()
            s21mag = jpac.read_network_mag()
            logger.debug("Writing fine magnitude measurement")
            s21mag.mref = "FINE-NARROW-MAG"
            self.mio.write(s21mag)

    _ResourceType = t.Union[str, t.Tuple[str, t.Type[Instrument]]]
    @classmethod
    def make(
            cls,
            resources: t.Dict[str, _ResourceType],
            current: float,
            path: str):
        jpac = JPAController.make(resources=resources)
        im = jpac.instrument_manager
        mio = MIOFileCSV.open(path, mode='w')
        classes = {}

        for k, v in resources.items():
            try:  # if a mapping of str -> tuple
                rscaddr, inscls = v
            except ValueError:  # elif a mapping of  str -> str
                rscaddr = v
                inscls = None
            resources[k] = rscaddr
            if inscls is not None:
                classes[k] = inscls

        return cls(jpac=jpac,
                   instrument_manager=im, resources=resources,
                   resource_classes=classes,
                   current=current, mio=mio)


def main():
    uris = {
            'vna': 'TCPIP::10.10.0.37::hislip0::INSTR',
            'sg1': 'TCPIP0::10.10.0.202::inst0::INSTR',
            # 'sa': 'TCPIP::10.10.1.101::inst0::INSTR',
            # 'tc': 'TCPIP0::10.10.1.150::7777::SOCKET',
            'cs': 'TCPIP0::10.10.0.201::5025::SOCKET'
    }

    logger.info("RESONANCE TRACKING STARTED")

    # Parameters
    current = None

    path = Path('~/Measurements/190921/TRACK_RESONANCE_WARMUP_2_TEST').expanduser()
    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()
    mc = MainController.make(
        resources=uris,
        current=current,
        path=path)

    mc.initiate()
    try:
        mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    logger.info("RESONANCE TRACKING FINISHED")


if __name__ == "__main__":
    main()
