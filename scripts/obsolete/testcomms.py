"""Script to test comm.s with the devices on the rack."""

from groundcontrol.controller import Controller, instrument, Instrument
from groundcontrol.resources import get_uris


class TestController(Controller):
    sg1: Instrument = instrument()
    sa: Instrument = instrument()
    vna: Instrument = instrument()
    tc: Instrument = instrument()
    cs: Instrument = instrument()


def main():
    uris = get_uris()

    tcon = TestController.make(resources=uris)
    tcon.initiate()


if __name__ == "__main__":
    main()
