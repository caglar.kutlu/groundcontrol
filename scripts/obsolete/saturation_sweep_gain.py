"""Quick script to do measurement on BF4. """
import typing as t
from pathlib import Path
from shutil import copyfile
import sys
import argparse

import attr
import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol import JPATuner, WorkingPoint, TuningPoint
from groundcontrol.settings import InstrumentSettings, VNASettings
from groundcontrol.declarators import setting, declarative
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.pid import TunerSettings
from groundcontrol.logging import setup_logger, create_log, INFO
from groundcontrol.helper import count
import groundcontrol.units as un
from groundcontrol.resources import get_uris

print(f"groundcontrol version: {__version__}")

logger = setup_logger("noise_temp_vs_gain")


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class SweepParameters(InstrumentSettings):
    offset_gains: np.ndarray = setting(un.dB, "Offset Gains")
    signal_powers: np.ndarray = setting(un.dBm, "Signal Powers")

    @classmethod
    def make(cls, offset_gains, signal_powers):
        return cls(offset_gains, signal_powers)

    @classmethod
    def from_ranges(
            cls,
            gain_rng,
            pow_rng):
        """Creates sweep parameters from given ranges given in the format
        (start, stop, step), start and stop inclusive.  For numerical
        consistency, provide integer values.
        """
        pow_min, pow_max, pow_step = pow_rng
        gain_min, gain_max, gain_step = gain_rng

        sig_pows = fullrange(pow_min, pow_max, pow_step)
        gains = fullrange(gain_min, gain_max, gain_step)

        return cls.make(
                gains, sig_pows)

    def to_table(self):
        args = (self.offset_gains, self.signal_powers)

        # indexing ij ensures matrix style indexing
        cols = map(lambda ar: ar.flatten(), np.meshgrid(*args, indexing='ij'))
        return np.vstack(list(cols)).T

    def to_csv(self, fname: str):
        header = ("Gain [dB], Signal Power [dBm]")
        np.savetxt(fname, self.to_table(), delimiter=',', header=header)


@declarative
class MainController:
    jpac: JPAController
    tuner: JPATuner

    mio: MIOFileCSV

    sweep_params: SweepParameters = setting()

    target_resf: float = setting(
            un.hertz, "Target Resonance", default=2.300e9)

    target_peak_offset: float = setting(
            un.hertz, "Target Peak Offset", default=0)

    init_current: float = setting(
            un.ampere, "Initial Current", default=0,
            description="The DC flux is biased with this current when "
                        "estimating baseline.")
    res_maxerror: float = setting(
            un.hertz, "Maximum Resonance Error", default=50e3,
            description="Maximum acceptable resonance error when tuning.")

    skipbootstrap: bool = setting(
            un.nounit, "Skip Gain Bootstrap", default=False,
            description="Whether or not to skip gain bootstrapping.")

    maxtrial: int = setting(
            un.nounit, "Maximum Trial After Failed Tuning", default=10)

    _tups: TuningPoint = attr.ib(factory=dict)

    def _measure_baseline(self):
        c = self.jpac
        c.deactivate_pump()
        logger.info("Setting current for baseline measurement.")
        current = c.cs.query_level_i()
        c.ramp_current(self.init_current)
        bl = self.tuner.measure_baseline()
        logger.info("Ramping current back to initial value.")
        c.ramp_current(current)
        return bl

    def initiate(self):
        create_log(self.mio.path / "groundcontrol.log", INFO)
        self.sweep_params.to_csv(self.mio.path / "sweep_params.csv")

        # We are assuming current vna is centered around resonance frequency.
        cursettings = self.jpac.read_vna_settings()
        Tb = self._measure_baseline()
        self.tuner.Tb = Tb
        self.jpac.do_vna_preset(cursettings)
        self.tuner.bootstrap(assumeresonance=True)
        self.tuner.Tb.mref = "BASELINE"
        self.mio.write(self.tuner.Tb)

        logger.info(f"Tuning resonance to target: {self.target_resf}")
        self.tuner.tune_resonance(
                self.target_resf,
                maxerror=self.res_maxerror)

    def _tune(self, gain, skipbootstrap=False):
        counter = count()
        while next(counter) < self.maxtrial:
            if skipbootstrap:
                success = self.tuner.tune_gain(
                        gain,
                        fpeak=self.tuner.Fr + self.target_peak_offset)
            else:
                success = self.tuner.tune_absolute(
                        gain=gain,
                        fpeak_offset=self.target_peak_offset,
                        skipfinal=True)
            if success:
                return True
            else:
                logger.info("Tuning failed, trying again.")
                self.tuner.bootstrap()
        return False

    def do_power_sweep(self, i: int) -> bool:
        tuner = self.tuner
        for j, spow in enumerate(self.sweep_params.signal_powers):
            logger.info(f"SIGNAL POWER: {spow:.2f} dBm")
            gs = tuner.gain_settings
            tuner.gain_settings = gs.evolve(
                    power=spow)

            gainspect = tuner.measure_gain()
            mref = f"i{i:d}j{j:d}"
            gainspect.mref = mref
            self.mio.write(gainspect)

    def run(self) -> bool:
        for i, gain in enumerate(self.sweep_params.offset_gains):
            logger.info(f"TUNING TO: {gain:.2f} dB")
            if i == 0:
                tunesuccess = self._tune(gain, False)
            else:
                tunesuccess = self._tune(gain, self.skipbootstrap)
            if not tunesuccess:
                logger.info("Tuning CATASTROPHICALLY failed, aborting.")
                break
            tup = self.tuner.tup
            tup.mref = f"i{i:d}"
            self._tups[i] = tup
            self.mio.write(tup)
            logger.info(f"STARTING POWER SWEEP.")
            self.do_power_sweep(i)

    def finish(self):
        pass

    @classmethod
    def make(
            cls,
            controller: JPAController,
            tuner: JPATuner,
            sweep_params: SweepParameters,
            path: t.Union[Path, str]):
        mio = MIOFileCSV.open(path, mode='w')
        return cls(controller, tuner, mio, sweep_params)


def save_script(path: Path):
    """Saves this script to the given location."""
    thisfpath = Path(__file__)
    return copyfile(thisfpath, path / thisfpath.name)


def setup_parser():
    parser = argparse.ArgumentParser(
            description=
            "Measures the saturation at different gains."
            "The VNA must be centered at resonance before starting.")
    parser.add_argument(
            '-p',
            '--path',
            type=Path,
            help="Path to the folder to be used when saving data.")

    parser.add_argument(
            '-w',
            '--overwrite',
            action='store_const',
            const=True, default=False,
            help="Whether or not to overwrite the files in "
                 "the given path if they exist.")

    parser.add_argument(
            '-f',
            '--frequency',
            type=float,
            help="Center frequency to tune to.")
    return parser


parser = setup_parser()


def main(
        path: Path,
        frequency: float = 2.305e9,
        overwrite: bool = False):
    try:
        path.mkdir(parents=True, exist_ok=overwrite)
    except AttributeError:
        logger.error("Need to supply a path.")
        logger.info("Aborting.")
        sys.exit(42)
    except FileExistsError:
        logger.error("A folder at the given path exists.")
        logger.info("Aborting.")

    __measurement_name__ = "Power Saturation Measurement"
    uris = get_uris()

    # Measurement Parameters ##
    gain_min = 16
    gain_max = 20
    gain_step = 1

    pow_min = -30
    pow_max = -5
    pow_step = 1

    # Sweep parameters
    sweep_params = SweepParameters.from_ranges(
            (gain_min, gain_max, gain_step),
            (pow_min, pow_max, pow_step))

    # Script parameters
    skipbootstrap = True
    target_resf = frequency
    init_current = 60e-6
    res_maxerror = 10e3
    target_peak_offset = 50e3

    pump_ramp_rate = 10  # dBm/s  we don't need to be so careful
    current_ramp_rate = 10e-6  # A/s
    dfres_sign = 1  # sign of the slope of the resonance-current curve

    tuning_window = (1.75e9, 1.85e9)
    dfo = -100  # This is the gain measurement offset

    boot_step = 0.5  # dB
    boot_threshold = 12  # dB
    boot_ifbw = 100  # Hz

    zet_max = WorkingPoint(100e-6, tuning_window[1]*2, 25)
    zet_min = WorkingPoint(-100e-6, tuning_window[0]*2, -20)

    res_tuner_set = TunerSettings(
                kp=0.2e-12,
                ki=8,
                kd=1e-5,
                ramp_step=200e3)

    gain_tuner_set = TunerSettings(
                kp=0.01,
                ki=10,
                kd=0,
                ramp_step=1)

    # Presets
    res_preset = VNASettings(
                span=3e6, sweep_step=20e3, if_bandwidth=200, power=-15)

    baseline_preset = VNASettings(
                start_frequency=tuning_window[0],
                stop_frequency=tuning_window[1],
                if_bandwidth=100, power=10, sweep_step=100e3)

    offset_preset = VNASettings(
                span=10, sweep_step=1, power=-30, if_bandwidth=2)

    gain_preset = VNASettings(
                span=1e3, sweep_step=100, power=-30, if_bandwidth=2)

    # Initializing
    c = JPAController.make(resources=uris)

    tuner = JPATuner.make(
            controller=c,
            Df_sign=dfres_sign,
            wt=tuning_window,
            dfo=dfo,
            gain_bootstrap_step=boot_step,
            gain_bootstrap_threshold=boot_threshold,
            gain_bootstrap_ifbw=boot_ifbw,
            zet_max=zet_max,
            zet_min=zet_min,
            restun=res_tuner_set,
            gaintun=gain_tuner_set,
            res_settings=res_preset,
            baseline_settings=baseline_preset,
            offset_settings=offset_preset,
            gain_settings=gain_preset
            )

    mc = MainController.make(
            c,
            tuner,
            sweep_params=sweep_params,
            path=path)

    c.pump_ramp_rate = pump_ramp_rate
    c.current_ramp_rate = current_ramp_rate
    mc.target_resf = target_resf
    mc.init_current = init_current
    mc.target_peak_offset = target_peak_offset
    mc.res_maxerror = res_maxerror
    mc.skipbootstrap = skipbootstrap

    logger.info(f"{__measurement_name__} MEASUREMENT")
    try:
        # ordering is important
        c.initiate()
        tuner.initiate()
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    finally:
        mc.finish()
        logger.info(f"Saving script to {path}")
        save_script(path)
    logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")


if __name__ == "__main__":
    args = parser.parse_args()
    if args.frequency is not None:
        main(args.path, args.frequency, overwrite=args.overwrite)
    else:
        main(args.path, overwrite=args.overwrite)
