"""Extract working points from paramap based on given conditions.
If '-d' option is given it takes precedence over '-c'.
"""
import argparse
import sys
from pathlib import Path
import typing as t

import matplotlib as mpl
import matplotlib.pyplot as plt
import xarray as xr
import pandas as pd
import numpy as np


mpl.rcParams['font.size'] = 12
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


def setup_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
            'path',
            help='Path to paramap netcdf file.',
            type=Path)

    parser.add_argument(
            '-c1',
            help=("Perform a cut by defining a region with "
                  "'G1MIN <= g1 <= G1MAX, g2 >= G2MIN' where "
                  "g1 is the gain at 1st frequency offset and g2 "
                  "is the gain at the 2nd frequency offset."),
            metavar=('G1MIN', 'G1MAX', 'G2MIN'),
            dest='cut',
            nargs=3,
            default=(10, 40, 8),
            type=float)

    parser.add_argument(
            '-c2',
            help=("Perform a cut by defining a region with "
                  "'G1MIN <= g1 <= G1MAX, g1-g2 >= G2DIF' where "
                  "g1 is the gain at 1st frequency offset and g2 "
                  "is the gain at the 2nd frequency offset."),
            metavar=('G1MIN', 'G1MAX', 'G2DIF'),
            dest='dif',
            nargs=3,
            default=None,
            type=float)

    parser.add_argument(
            '-d',
            help=("Perform a cut by defining a detuning region with "
                  "'DMIN <= detuning <= DMAX'"),
            metavar=('DMIN', 'DMAX'),
            dest='det',
            nargs=2,
            default=None,
            type=float)

    parser.add_argument(
            '-p',
            help=("Perform a cut by defining a pump power region with "
                  "'PMIN <= ppump <= PMAX'"),
            metavar=('PMIN', 'PMAX'),
            dest='pump',
            nargs=2,
            default=None,
            type=float)

    parser.add_argument(
            '-f',
            dest='fr0',
            help='fr0 selection, nearest value is used.',
            type=float,
            default=0)
    return parser


def abscut(ds, g1min, g1max, g2min):
    bw0_MHz = ds.bw0.values/1e6
    fr0 = ds.fr0.values

    dscut = (ds.g1 > g1min)*(ds.g2 > g2min)*(ds.g1 < g1max)

    name = f'cut{fr0/1e9:.6f}G'
    save_cut(ds, dscut, name)
    plot(ds.g1.where(dscut), fr0, bw0_MHz, name)


def difcut(ds, g1min, g1max, g2dif):
    bw0_MHz = ds.bw0.values/1e6
    fr0 = ds.fr0.values

    g1cut = (ds.g1 > g1min)*(ds.g1 < g1max)
    dscut = g1cut*(ds.g2 >= (ds.g1 - g2dif))

    name = f'cut{fr0/1e9:.6f}G'
    save_cut(ds, dscut, name)
    plot(ds.g1.where(dscut), fr0, bw0_MHz, name)


def save_cut(ds: xr.Dataset, dscut, fname: str):
    """Prepares and saves the ds in csv format appropriate for consuming in
    snrscan script"""
    cut = ds.where(dscut)

    # Usually pump frequencies are not supplied.
    # We assume 3WM pumping and calculate them from detuning
    fpumps = 2*((cut.bw0/2)*cut.detuning + cut.fr0)
    cut_w_fp = cut.assign_coords(fp=fpumps)

    # Lifting the ib, fr0 and bw0 to dimension status
    lifted = cut_w_fp.expand_dims(['ib', 'fr0', 'bw0'])

    # We can't add fp to this because it's a coordinate for detuning dimension.

    # Flattening the matrix
    # First create an auxiliary multi-index
    order = ['ib', 'detuning', 'ppump', 'fr0', 'bw0']
    cut_stacked = lifted.stack(wp=order)

    # This effectively reduced the dimension into one, stacking one
    # in front of the other.  The stacking order is defined by the order
    # of parameters we give to the auxiliary variable.

    # Since we have a single dimension, we can drop the NA values.
    # Note that if we did this without stacking, we can only get rid of na
    # values along single dimension at a time.  If we have n-dimensions, this
    # means we can only drop stuff that is outside a hypercube of n-dimensions.

    ds = cut_stacked.dropna('wp', how='all')

    # Lastly, we convert to a pandas dataframe to be saved.
    df = ds.to_dataframe()

    # This dataframe is multi-index but it will automatically be converted to a
    # table with hidden index upon saving.

    # Backwards compatibility
    mapper = {'detuning': 'det', 'ppump': 'pp'}
    newnames = [
            mapper[name] if name in mapper else name
            for name in order]
    df.index.rename(newnames, inplace=True)

    df.to_csv(f'{fname}.csv')


def get_dataset(path, fr0p):
    ds = xr.open_dataset(path)
    dspick = ds.swap_dims({'ib': 'fr0'}).sel(fr0=fr0p, method='nearest')
    return dspick


def plot(da, fr0, bw0_MHz, fname=None):
    fig, ax = plt.subplots(figsize=(5.5, 4.8))
    plotfun = da.plot.pcolormesh
    # plotfun = da.plot.contourf
    plotfun(
            vmin=8, vmax=40, levels=18, add_colorbar=True)
    ax.set_title(f"$ f_r = {fr0:.3f}\, \\mathrm{{GHz}}$")
    ax.set_xlabel(f"$ \\delta = (f_p/2 - f_r)/({bw0_MHz/2:.1f} \mathrm{{MHz}})$")
    ax.set_ylabel("$ \\mathrm{P}_\mathrm{pump} $ [dBm]")

    plt.tight_layout()

    if fname is not None:
        fig.savefig(f'{fname}.png', dpi=300)
    plt.show()


def parse_and_dispatch():
    parser = setup_parser()
    parsed = parser.parse_args()

    fpath = parsed.path
    fr0p = parsed.fr0
    dmin, dmax = (None, None) if parsed.det is None else parsed.det
    pmin, pmax = (None, None) if parsed.pump is None else parsed.pump

    # Read the .nc file and cut select fr slice
    ds = get_dataset(fpath, fr0p)

    # detuning cut
    if dmin is not None:
        ds = ds.where((ds.detuning >= dmin)*(ds.detuning <= dmax))

    if pmin is not None:
        ds = ds.where((ds.ppump >= pmin)*(ds.ppump <= pmax))

    if parsed.dif is None:
        g1min, g1max, g2min = parsed.cut
        return abscut(ds, g1min, g1max, g2min)
    else:
        g1min, g1max, g2dif = parsed.dif
        return difcut(ds, g1min, g1max, g2dif)


def main():
    parse_and_dispatch()


if __name__ == "__main__":
    main()
