# coding: utf-8
"""This is a script used to analyse resonance map data taken prior to December 2019.
"""
import argparse
from pathlib import Path
import os

import numpy as np
import matplotlib.pyplot as plt

from groundcontrol.measurement import read_measurement_csv
from groundcontrol.measurement import ResonanceMap
from groundcontrol.estimation import resonance_from_phase2_nppm
from groundcontrol.logging import set_stdout_loglevel, logger


try:
    _ = os.environ['DEBUG']
    set_stdout_loglevel('DEBUG')
except KeyError:
    pass


parser = argparse.ArgumentParser(description="Process resonance map data.")
parser.add_argument('path', help="Path to the folder containing data.")
parser.add_argument('bw', type=float, help="Expected passive bandwidth.")
parser.add_argument(
        '-o', '--out', type=Path, help="Output filename.")
parser.add_argument(
        '-p', '--plot', action='store_const', const=True, default=False,
        help="Plot or not plot.")
parsed = parser.parse_args()

folder = Path(parsed.path)
bw = parsed.bw
outfname = parsed.out
toplot = parsed.plot

mref = "PHASE"


def numberkey(fname: str):
    """Sorter based on the numbers inside filename."""
    nums = "".join(c for c in str(fname) if c.isdigit())
    return int(nums)


# Sorting by their ordering..
phasefiles = sorted(folder.glob(f"NPPM/*{mref}*.csv"), key=numberkey)
currentfiles = sorted(folder.glob("SMM/*"), key=numberkey)

resfs = []
phased = []
currentd = []
ts0 = None
for i, (pf, cf) in enumerate(zip(phasefiles, currentfiles)):
    current = np.genfromtxt(cf, delimiter=',', comments='#')
    nppm = read_measurement_csv(pf)
    ts = nppm.timestamp
    if i == 0:
        tsinit = ts
    fr = resonance_from_phase2_nppm(nppm, bw_expected=bw)
    if fr is None:
        fr = np.nan

    resfs.append(fr)
    currentd.append(current)

tsend = ts
compltime_s = (tsend - tsinit).seconds

resfs = np.array(resfs)
cvals, cerrs = np.array(currentd).T

dc = cvals[1] - cvals[0]

rmap = ResonanceMap(cvals, resfs, stepsize=dc, completion=compltime_s)

if outfname is not None:
    logger.info(f"Writing to: {outfname}")
    rmap.to_csv(outfname)

if toplot:
    fig, ax = plt.subplots()
    ax.plot(cvals, resfs)
    ax.set_xlabel("Current [A]")
    ax.set_ylabel("Resonance Frequency [Hz]")
    plt.show()
