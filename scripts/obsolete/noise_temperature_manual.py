"""THIS SCRIPT WILL BE DEPRECATED."""
import typing as t
import logging
import time
from datetime import datetime
import os
from pathlib import Path
import sys
from io import IOBase
import json
from numbers import Number

import attr
import numpy as np

from groundcontrol import InstrumentManager
from groundcontrol.instruments import SCPIVectorNetworkAnalyzer
from groundcontrol.instruments import KeysightN5232A
from groundcontrol.instruments import RohdeSchwarzFSV
from groundcontrol.instruments import LS372TemperatureController


logger = logging.getLogger("jpa_noise_temperature_manual")
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('debug.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
fileformatter = logging.Formatter('%(asctime)s - %(levelname)s -s%(name)s - %(message)s')  # noqa: E501
streamformatter = logging.Formatter('%(levelname)s %(message)s')
fh.setFormatter(fileformatter)
ch.setFormatter(streamformatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


def within(x: Number, a: Number, b: Number):
    return (x > a) and (x <= b)


@attr.s(auto_attribs=True)
class MeasurementParameters:
    vna_center_frequency: float
    vna_span: float
    vna_ifbw: float
    vna_sweep_step: float
    vna_power: float
    measurement_offset: float
    sa_center: float
    sa_span: float
    sa_rbw: float
    sa_navg: int
    baseline_db: float
    resonance_frequency: float
    pump_frequency: float
    temperature_mxc: float
    temperature_fluctuation_threshold: float
    temperature_settle_time: float  # seconds after rampup complete
    temperature_timeout: float  # seconds after setpoint set till settling
    temperature_sweep: t.List[float]  # list of temperature values to sweep
    buffersize: int
    outprefix: str

    def save(self):
        fpath = f"{self.outprefix}_params.json"
        if os.path.exists(fpath):
            logger.error(f"File {fpath} exists.  Aborting.")
            sys.exit(1)
        with open(fpath, 'w') as fp:
            d = attr.asdict(self)
            ts = d['temperature_sweep']
            # THIS MAY BE WRONG!
            ti = ts[0]
            tf = ts[-1]
            try:
                tdt = ts[1] - ts[0]
            except IndexError:
                tdt = 0
            d['temperature_sweep'] = [ti, tf, tdt]
            json.dump(d, fp)


@attr.s(auto_attribs=True)
class Spectrum:
    x: np.ndarray
    y: np.ndarray
    xlabel: str
    ylabel: str
    header: str

    def save(self, fname: str):
        data = np.vstack([self.x, self.y]).T
        np.savetxt(fname, X=data, header=self.header, delimiter=',')

    @classmethod
    def create(cls, x, y, xlabel, ylabel, header=None):
        if header is None:
            header = f"{xlabel},{ylabel}"
        return cls(x, y, xlabel, ylabel, header)


@attr.s(auto_attribs=True)
class DataModel:
    buffersize: int
    filehandle: IOBase
    vna_spectrum: Spectrum  # holds the last saved spectrum
    sa_spectrum: Spectrum  # holds the last saved spectrum
    ts_vna_start: np.ndarray
    ts_vna_end: np.ndarray
    ts_sa_start: np.ndarray
    ts_sa_end: np.ndarray
    temperature: np.ndarray
    fluctuation: np.ndarray
    fname_prefix: str = "NT"
    vna_fname_prefix: str = "VNA"
    sa_fname_prefix: str = "SA"
    bufcursor: int = 0
    measurement_counter: int = 0

    def append(
            self, vna_spectrum, sa_spectrum,
            ts_vna_start, ts_vna_end,
            ts_sa_start, ts_sa_end, temperature, fluctuation):
        """Spectrum data are saved into csv files and scalar data is
        buffered."""
        for src, dest in zip(
                [ts_vna_start,
                    ts_vna_end,
                    ts_sa_start,
                    ts_sa_end,
                    temperature,
                    fluctuation],
                [self.ts_vna_start,
                    self.ts_vna_end,
                    self.ts_sa_start,
                    self.ts_sa_end,
                    self.temperature,
                    self.fluctuation]):
            dest[self.bufcursor] = src
        self.vna_spectrum = vna_spectrum
        self.sa_spectrum = sa_spectrum
        self.save_spectra()
        self.bufcursor += 1  # this stays right before the following
        self.measurement_counter += 1

        if self.bufcursor >= self.buffersize:
            logger.info("Appending data to the CSV file.")
            self.flush()
            self.empty_buffer()

    def save_spectra(self):
        fpre = self.fname_prefix
        vnafpre = self.vna_fname_prefix
        safpre = self.sa_fname_prefix

        mno = self.measurement_counter
        fname_vna = f"{fpre}_{vnafpre}_M{mno}.csv"
        fname_sa = f"{fpre}_{safpre}_M{mno}.csv"
        self.sa_spectrum.save(fname_sa)
        self.vna_spectrum.save(fname_vna)

    def empty_buffer(self):
        self.bufcursor = 0

    def flush(self):
        """Flushes the data into csv file."""
        lst = [d[:self.bufcursor+1]
               for d
               in [self.ts_vna_start,
                   self.ts_vna_end,
                   self.ts_sa_start,
                   self.ts_sa_end,
                   self.temperature,
                   self.fluctuation]]
        names = [
                "VNA Start",
                "VNA End",
                "SA Start",
                "SA End",
                "Noise Source Temperature [K]",
                "NS Temperature Fluctuation [K]"]
        data = np.core.records.fromarrays(
                lst, names=names,
                dtype=[
                    ("VNA Start", '<U64'),
                    ("VNA End", '<U64'),
                    ("SA Start", '<U64'),
                    ("SA End", '<U64'),
                    ("Noise Source Temperature [K]", float),
                    ("NS Temperature Fluctuation [K]", float)])
        fmt = ('%s', '%s', '%s', '%s', '%.18e', '%.18e')

        np.savetxt(self.filehandle, X=data, fmt=fmt, delimiter=',')
        self.filehandle.flush()

    def __del__(self):
        self.flush()
        if self.filehandle is not None:
            self.filehandle.close()

    @classmethod
    def create(
            cls,
            buffersize,
            fname_prefix: str = "NT",
            vna_fname_prefix: str = "VNA",
            sa_fname_prefix: str = "SA",
            header=None):
        if header is None:
            header = "VNAStart,VNAEnd,SAStart,SAEnd,Temperature,TempFluct"
        ts_vna_start = np.zeros(buffersize, dtype='<U64')
        ts_vna_end = np.zeros(buffersize, dtype='<U64')
        ts_sa_start = np.zeros(buffersize, dtype='<U64')
        ts_sa_end = np.zeros(buffersize, dtype='<U64')
        temperature = np.zeros(buffersize)
        fluctuation = np.zeros(buffersize)
        x = np.zeros(0)
        y = np.zeros(0)
        nilspectrum = Spectrum.create(x, y, "", "")
        fname = f"{fname_prefix}.csv"
        if os.path.exists(fname):
            raise ValueError(f"{fname} exists.")

        filehandle = open(fname, 'w+')
        filehandle.write(f"{header}\n")
        return cls(buffersize, filehandle, nilspectrum, nilspectrum,
                   ts_vna_start, ts_vna_end, ts_sa_start, ts_sa_end,
                   temperature, fluctuation, fname_prefix,
                   vna_fname_prefix, sa_fname_prefix)


@attr.s(auto_attribs=True)
class Controller:
    im: InstrumentManager
    sa: RohdeSchwarzFSV
    vna: KeysightN5232A
    tc: LS372TemperatureController
    params: MeasurementParameters
    datamodel: DataModel
    MAX_TEMPERATURE_SETTING = 0.65  # don't set above 600 mK !

    SABYTEORDER: t.ClassVar = RohdeSchwarzFSV.ByteOrderFormat.LITTLE_ENDIAN
    SADATAFORMAT: t.ClassVar = RohdeSchwarzFSV.FormatDataType.REAL32

    VNABYTEORDER: t.ClassVar = KeysightN5232A.ByteOrderFormat.LITTLE_ENDIAN
    VNADATAFORMAT: t.ClassVar = KeysightN5232A.FormatDataType.REAL32

    @property
    def sa_center(self):
        return self.params.vna_center_frequency\
                + self.params.measurement_offset

    def initiate(self):
        sa = self.sa

        sa.set_format_byte_order(self.SABYTEORDER)
        sa.set_format_data(self.SADATAFORMAT)

        sa.set_initiate_continuous(sa.State.OFF)
        
        vna = self.vna

        vna.set_format_byte_order(self.VNABYTEORDER)
        vna.set_format_data(self.VNADATAFORMAT)

        # Assuming first trace is set up properly in the VNA prior to starting.
        vna.set_calculate_parameter_mnumber_select(1)  # selects 1st trace 
        vna.set_calculate_format(vna.DisplayFormat.MLOGARITHMIC)

        # Single trigger mode
        vna.set_initiate_continuous(vna.State.OFF)

        # Setting measurement parameters
        pars = self.params
        sa.set_sense_frequency_center(pars.sa_center)
        sa.set_sense_frequency_span(pars.sa_span)
        sa.set_sense_average_state(sa.State.ON)
        sa.set_trace_type(sa.TraceType.AVERAGE)
        sa.set_sense_average_count(pars.sa_navg)
        npoints = int(np.floor(pars.sa_span/pars.sa_rbw))
        sa.set_sense_sweep_points(npoints)
        sa.set_sense_bandwidth_resolution(pars.sa_rbw)

        vna.set_sense_frequency_center(pars.vna_center_frequency)
        vna.set_sense_frequency_span(pars.vna_span)
        vna.set_sense_sweep_step(pars.vna_sweep_step)
        vna.set_sense_bandwidth(pars.vna_ifbw)
        vna.set_source_power_level(pars.vna_power)
        

    def adjust_excitation(self, setpoint):
        tc = self.tc
        ch = tc.Channel.C1
        mode, excitation, autorange, resrange, csshunt, units = tc.query_input_setup(ch)
        exc_old = excitation
        if mode == tc.CurrentExcitation:
            return  # dont do anything if not set to voltage excitation
        if within(setpoint, 0.01, 0.05):
            excitation = tc.VoltageExcitation.V6u32
        elif within(setpoint, 0.05, 0.2):
            excitation = tc.VoltageExcitation.V20u0
        elif within(setpoint, 0.2, 0.7):
            excitation = tc.VoltageExcitation.V63u2
        elif within(setpoint, 0.7, 1):
            excitation = tc.VoltageExcitation.V200u
        E = tc.VoltageExcitation

        # issue a change only if new excitation is to be different from current one.
        if exc_old != excitation:
            logger.info(f"Changing excitation from {E(exc_old).name} to {E(excitation).name}.")
            tc.set_input_setup(ch, mode, excitation, autorange, resrange, csshunt, units)

    def trigger_temperature_set(self, setpoint) -> (float, float):
        tc = self.tc
        self.adjust_excitation(setpoint)

        if setpoint > self.MAX_TEMPERATURE_SETTING:
            logger.info(
                "Temperature setting exceeded absolute maximum "
                f"permitted {self.MAX_TEMPERATURE_SETTING}.  Aborting.")
            self.abort(42)
        tc.set_temperature_setpoint(setpoint)
        temps, fluctuation = self.wait_temperature_settling(setpoint)
        return temps.mean(), fluctuation

    def wait_temperature_settling(self, setpoint: float):
        tc = self.tc
        PERIOD = 1  # make this a parameter
        LASTN = 50  # use last N samples to check fluctuation
        TEMPERROR = 0.001  # make this a parameter, std error
        params = self.params
        timeout = params.temperature_timeout
        settle = params.temperature_settle_time
        threshold = params.temperature_fluctuation_threshold

        tempbuff = np.zeros(int(timeout/PERIOD)*2)

        start = time.time()
        timepassed = time.time() - start

        settled = False
        rampingdone = False
        cursor = 0
        while timepassed < timeout:
            tempbuff[cursor] = tc.query_kelvin_reading(tc.Channel.C1)
            logger.info(f"Temperature reading: {tempbuff[cursor]}")
            cursor += 1
            if (not rampingdone) and (tc.query_ramp_status() == tc.RampStatus.NORAMPING): 
                logger.info(f"Ramping finished, waiting for {settle} seconds to settle.")
                rampingdone = True
            
            if rampingdone and timepassed > settle:
                begin = cursor - LASTN
                if begin < 0:
                    # wait till you have enough samples
                    continue
                temp = tempbuff[begin:cursor]
                fluctuation = temp.std()/setpoint
                error = np.abs(temp.mean() - setpoint)/setpoint
                if fluctuation <= threshold and error <= TEMPERROR:
                    logger.info(f"Temperature fluctuation is {fluctuation}<={threshold}.")
                    logger.info(f"Temperature error is {error}<={TEMPERROR}.  Equilibrium reached.")
                    settled = True
                    break
            timepassed = time.time() - start
            time.sleep(PERIOD)

        if not settled:
            logger.info("Equilibrium was not reached, aborting the script for safety.")
            self.abort(41)

        return tempbuff[begin:cursor], fluctuation

    def trigger_sa(self):
        sa = self.sa
        swptime_s = sa.query_sense_sweep_time()  # in ms
        swptime_ms = swptime_s * 1000
        navg = sa.query_sense_average_count()
        efficiency = 0.1  # exaggerated
        newtimeout = navg*swptime_ms/efficiency
        sa.timeout = newtimeout
        sa.do_initiate_immediate()
        sa.query_opc()

    def trigger_vna(self):
        vna: SCPIVectorNetworkAnalyzer = self.vna
        # turn vna power on
        vna.set_output(vna.State.ON)
        swptime_ms = vna.query_sense_sweep_time()*1000
        efficiency = 0.2
        vna.timeout = swptime_ms/efficiency
        vna.do_initiate()
        vna.query_opc()
        vna.do_display_window_trace_y_scale_auto()
        vna.set_output(vna.State.OFF)

    def read_sa_spectrum(self):
        sa = self.sa
        s = sa.query_trace_data(self.SADATAFORMAT, self.SABYTEORDER, 1)
        n = len(s)
        fmin, fmax = sa.query_sense_frequency_start(), sa.query_sense_frequency_stop()
        f = np.linspace(fmin, fmax, n)
        spectrum = Spectrum.create(f, s, "Frequency[Hz]", "Power[dBm]")
        return spectrum

    def read_vna_spectrum(self):
        vna = self.vna
        dformat = vna.FormatDataType.REAL64
        vna.set_format_data(vna.FormatDataType.REAL64)
        x_arr = vna.query_calculate_x_values(format=dformat)
        dformat = vna.FormatDataType.REAL32
        vna.set_format_data(dformat)
        y_arr = vna.query_calculate_data(vna.OutDataType.FDATA, format=dformat)
        spectrum = Spectrum.create(x_arr, y_arr, "Frequency[Hz]", "S21[dB]")
        return spectrum

    def run(self):
        params = self.params

        for temperature in params.temperature_sweep:
            logger.info(f"Setting temperature to {temperature}")
            tempmean, fluctuation = self.trigger_temperature_set(temperature)

            vnastart = datetime.now()
            logger.info("Measuring S21.")
            self.trigger_vna()
            vnaend = datetime.now()
            sastart = datetime.now()
            logger.info("Measuring power spectrum.")
            self.trigger_sa()
            saend = datetime.now()

            logger.debug("Reading SA data.")
            sa_spectrum = self.read_sa_spectrum()
            logger.debug("Reading VNA data.")
            vna_spectrum = self.read_vna_spectrum()
            logger.info("Saving data.")
            self.datamodel.append(vna_spectrum, sa_spectrum,
                                  str(vnastart), str(vnaend), str(sastart),
                                  str(saend), 
                                  tempmean, fluctuation)

    def abort(self, errcode: int):
        self.im.delete_all()
        self.exit(errcode)

    def recreate_datamodel(self):
        self.datamodel = DataModel.create(
                self.params.buffersize, self.params.outprefix)

    @classmethod
    def create(cls, params: MeasurementParameters,
               resources: t.Dict[str, str]):
        im = InstrumentManager.make()
        sa = im.create_visa_instrument(
                'SA',
                resources['SA'],
                RohdeSchwarzFSV)
        vna = im.create_visa_instrument(
                'VNA',
                resources['VNA'],
                KeysightN5232A)
        tc = im.create_visa_instrument(
                'TC',
                resources['TC'],
                LS372TemperatureController)

        datamodel = DataModel.create(
                params.buffersize, fname_prefix=params.outprefix)
        return cls(im, sa, vna, tc, params, datamodel)


def _get_measurement_folder(basedir: str, num: int):
    return os.path.join(basedir, f"{num:d}")


def main():
    resources = {
        'VNA': 'TCPIP::10.10.0.37::hislip0::INSTR',
        'SA': ('TCPIP0::10.10.0.74::hislip0::INSTR', RohdeSchwarzFSV),
        'TC': 'TCPIP0::10.10.0.150::7777::SOCKET'
    }

    NTOTAL = 2
    BASEDIR = Path("~/Measurements/190920/NT_B0T_2").expanduser()
    TEMPERATURES = np.arange(0.06, 0.27, 0.02) 
    # [0.06, 0.1, 0.15, 0.2, 0.25]

    if BASEDIR.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)

    BASEDIR.mkdir()
    for i in range(1, NTOTAL+1):
        subdir = BASEDIR / str(i)
        subdir.mkdir()
        for subsubdirname in ('going_up', 'going_down'):
            mydir = subdir/subsubdirname
            mydir.mkdir()

    params = MeasurementParameters(
        # vna_center_frequency=2.30110e9,
        vna_center_frequency=2.3011e9,
        vna_span=2e6,
        vna_ifbw=10,
        vna_sweep_step=10e3,
        vna_power=0,
        measurement_offset=0,
        sa_center=2.3011e9,
        sa_span=200e3,
        sa_rbw=100,
        sa_navg=100,
        baseline_db=-3.05,   # CRUDE!
        resonance_frequency=2.30110e9,
        pump_frequency=4.60220e9*2,
        temperature_mxc=45,
        temperature_fluctuation_threshold=0.02,
        temperature_settle_time=300,  # seconds after rampup complete
        temperature_timeout=1800,  # seconds after settle time, abort if not settled
        temperature_sweep=TEMPERATURES,
        outprefix=os.path.join(_get_measurement_folder(BASEDIR, 1), 'going_up/NT'),
        buffersize=1)

    mc = Controller.create(params, resources)
    mc.initiate()

    i0 = 1
    for i in range(i0, NTOTAL+1):
        # GOING UP
        outprefix = os.path.join(_get_measurement_folder(BASEDIR, i), 'going_up/NT')
        temps = TEMPERATURES
        mc.params = attr.evolve(mc.params, temperature_sweep=temps, outprefix=outprefix)
        if i != i0:
            mc.recreate_datamodel()
        mc.params.save()
        mc.run()

        # GOING DOWN
        outprefix = os.path.join(_get_measurement_folder(BASEDIR, i), 'going_down/NT')
        temps = np.flip(TEMPERATURES)
        mc.params = attr.evolve(mc.params, temperature_sweep=temps, outprefix=outprefix)
        mc.recreate_datamodel()
        mc.params.save()
        mc.run()


if __name__ == "__main__":
    main()
