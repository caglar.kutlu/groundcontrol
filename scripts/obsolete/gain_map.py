import typing as t
from pathlib import Path

import groundcontrol
groundcontrol.USE_UNICODE = False

import numpy as np
from scipy.signal import savgol_filter, find_peaks

from groundcontrol.controllers.jpa import JPAController
from groundcontrol.controller import Controller
from groundcontrol.settings import VNASettings, setting
from groundcontrol.logging import logger
from groundcontrol.measurement import NetworkParams1PModel, MeasurementModel
from groundcontrol.measurement import parameters, quantity, parameter
from groundcontrol.measurementio import MIOFileCSV
import groundcontrol.units as un
from groundcontrol.helper import Array


def find_dip(meas: NetworkParams1PModel):
    """Returns the frequency where minimum occurs."""
    f = meas.frequencies
    p = meas.values
    dipi = np.argmin(p)
    return f[dipi]


class ResonanceMeasurement(MeasurementModel):
    fres: float
    current: float
    description: str = (
            "Measurement MREFs correspond to i'th current "
            "measurement")


class SParamMeasurement(MeasurementModel):
    frequencies: Array[float] = quantity(un.hertz, 'Frequency')
    mags: Array[float] = quantity(un.dB, 'Magnitude')
    phases: Array[float] = quantity(un.degree, "Phase")
    ifbw: float = parameter(un.hertz, 'IF Bandwidth')
    navg: int = parameter(un.nounit, 'Averages')
    power: float = parameter(un.dBm, 'Output Power')
    nparam: str = parameter(un.nounit, "S21")

    @classmethod
    def from_mag_phase(
            cls,
            mag: NetworkParams1PModel,
            phase: NetworkParams1PModel):
        """Combine two measurements to create this measurement.
        To be able to combine successfully, the two measurements must be fully
        compatible, in terms of units and all other stuff.

        TODO:
            - This does not check for unit compatibility!!!

        Returns:
            obj: Instance of this class.

        Raises:
            ValueError:  If one of the measurements contain an incompatible
            parameter with the other one.
        """

        magparams = parameters(mag)
        phaseparams = parameters(phase)
        magparamset = set(magparams.items())
        phaseparamset = set(phaseparams.items())

        # Check the set difference
        difference = magparamset ^ phaseparamset

        parfields = cls.parameter_fields
        parfieldset = set(pf['name'] for pf in parfields)
        for key, param in difference:
            # If their differing parameter is not contained within this class
            # we may ignore it.
            if key in parfieldset:
                msg = ("The two measurements are incompatible. "
                       f"They at least differ in {key} "
                       f"({magparams[key]}, {phaseparams[key]})")
                raise ValueError(msg)

        magparamkeyset = set(magparams.keys())
        kwargs = {}
        for key in parfieldset.intersection(magparamkeyset):
            kwargs[key] = magparams[key].value

        kwargs['frequencies'] = mag.frequencies
        kwargs['mags'] = mag.values
        kwargs['phases'] = phase.values
        return cls(**kwargs)


class SweepTable(MeasurementModel):
    """An index for measurements."""
    cur_index: Array[float] = quantity(un.nounit)  # index along which current changes
    pow_index: Array[float] = quantity(un.nounit)  # index along which pump power changes
    currents: Array[float] = quantity(un.ampere, "Coil Current")
    powers: Array[float] = quantity(un.dBm, "Pump Power")

    @classmethod
    def make(cls, csweep, psweep):
        # Sweeping currents first and then powers.
        # If currents correspond to rows, then it is row first, column second
        # sweep.
        nc = len(csweep)
        npow = len(psweep)

        # flattened iterable of indexes:  [(0,0), (0,1), (0,2), (1,0), ...]
        cindex, pindex = np.unravel_index(np.arange(nc*npow), (nc, npow))

        # indexing ij ensures matrix style indexing
        currents, powers = map(
                lambda ar: ar.flatten(),
                np.meshgrid(csweep, psweep, indexing='ij'))
        return cls(cindex, pindex, currents, powers)


class MainController(Controller):
    jpac: JPAController

    current_sweep: np.ndarray
    power_sweep: np.ndarray
    mio: MIOFileCSV

    generic_preset: VNASettings = setting(
            default=VNASettings(
                start_frequency=1.9e9,
                stop_frequency=2.35e9,
                if_bandwidth=2e3,
                sweep_step=100e3,
                power=13
                ))

    coarse_preset: VNASettings = setting(
             default=VNASettings(
                            span=20e6,
                            if_bandwidth=1e3,
                            sweep_step=100e3,
                            power=9
                            ))

    narrow_preset: VNASettings = setting(
            default=VNASettings(
                span=20e6,
                if_bandwidth=200,
                sweep_step=100e3,
                power=-20
                ))

    gain_preset: VNASettings = setting(
            default=VNASettings(
                span=2e6,
                if_bandwidth=10,
                sweep_step=10e3,
                power=-30))

    current_step: float = 1e-7
    current_ramp_rate: float = 1e-6

    def __setup__(self):
        self.jpac.initiate()
        self.jpac.current_step = self.current_step
        self.jpac.current_ramp_rate = self.current_ramp_rate
        self.jpac.deactivate_pump()

    def find_resonance_coarse(self, debug: str = None) -> float:
        jpac = self.jpac
        jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.MLOGARITHMIC)

        logger.info("Turning on calibration.")
        jpac.turn_on_vna_calibration()
        jpac.trigger_network_analyzer()
        s21mag = jpac.read_network_mag()
        fres = find_dip(s21mag)

        logger.info(f"Found COARSE resonance at {fres/1e9:.6f} GHz.")

        # Saving for debugging.
        if debug is not None:
            s21mag.mref = debug
            self.mio.write(s21mag)
        return fres

    def _get_resonance(
            self, sparam: NetworkParams1PModel) -> t.Tuple[float, int]:
        """Returns (frequency of the first resonance, amount of resonances)."""
        f = sparam.frequencies
        ph = sparam.values

        # Some rough way of finding the resonance from the phase shift.
        window_length = 21
        order = 3
        filtered = savgol_filter(ph, window_length, order)

        df = f[1] - f[0]

        # Derivative estimation
        delta_ph = -np.diff(filtered)/df
        peaks, props = find_peaks(delta_ph, prominence=1e-4)

        npk = len(peaks)
        return f[peaks[0]], npk

    def find_resonance_fine(
            self, f0: float, debug: str = None) -> t.Optional[float]:
        mphase = self.zoom_and_measure_phase(f0)

        foundf, npk = self._get_resonance(mphase)

        if npk == 0:
            logger.debug(f"Resonance analyzer found NO peaks.")
            return None
        elif npk >= 1:
            logger.debug(f"Resonance analyzer found no {npk} peaks.")

        logger.info(f"Found FINE resonance at {foundf/1e9:.6f} GHz.")
        # ignoring false positives etc.

        # Saving for debugging.
        if debug is not None:
            mphase.mref = debug
            self.mio.write(mphase)

        return foundf

    def measure_phase(self) -> NetworkParams1PModel:
        """Measures a phase spectrum."""
        jpac = self.jpac
        jpac.trigger_network_analyzer()
        return jpac.read_network_phase()

    def zoom_and_measure_phase(self, zoomf: float) -> NetworkParams1PModel:
        jpac = self.jpac
        pre = self.narrow_preset.evolve(center_frequency=zoomf)
        jpac.do_vna_preset(pre)
        jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.UPHASE)
        logger.info("Turning off calibration for phase measurement.")
        jpac.turn_off_vna_calibration()
        return self.measure_phase()

    def run(self):
        jpac = self.jpac
        jpac.deactivate_pump()
        jpac.set_pump_power(self.power_sweep[0])
        # jpac.set_pump_power(-20)

        for i, current in enumerate(self.current_sweep):
            jpac.ramp_current(current)
            # Measured current and error
            cmeas = jpac.measure_current(10)
            cmeas.mref = "DEBUG-CURRENT"
            logger.debug("Writing current measurement")
            self.mio.write(cmeas)

            logger.info("Performing a coarse resonance search with wide span.")
            jpac.do_vna_preset(self.generic_preset)
            f1 = self.find_resonance_coarse(debug="DEBUG-RES-COARSE")
            jpac.do_vna_preset(self.coarse_preset.evolve(center_frequency=f1))
            jpac.do_vna_autoscale()

            logger.info("Performing a coarse resonance search "
                        "with narrow span.")
            fcoarse = self.find_resonance_coarse(debug="DEBUG-RES-COARSE2")
            jpac.do_vna_preset(self.narrow_preset.evolve(center_frequency=fcoarse))
            jpac.do_vna_autoscale()

            logger.info("Performing a fine resonance search with narrow span.")
            ffine = self.find_resonance_fine(fcoarse, debug="DEBUG-RES-FINE")
            jpac.do_vna_preset(self.gain_preset.evolve(center_frequency=ffine))
            jpac.do_vna_autoscale()

            resmeas = ResonanceMeasurement(fres=ffine, current=cmeas.value)
            resmeas.mref = str(i)

            fpump = 2*ffine

            logger.info(f"Setting pump frequency {fpump}.")
            jpac.set_pump_frequency(fpump)

            logger.info(f"Activating pump.")
            jpac.activate_pump()

            jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.MLOGARITHMIC)
            jpac.turn_on_vna_calibration()
            jpac.do_vna_autoscale()
            for j, power in enumerate(self.power_sweep):
                jpac.ramp_pump_power(power)
                jpac.trigger_network_analyzer()

                phasemeas = jpac.read_network_phase()
                magmeas = jpac.read_network_mag()

                jpac.do_vna_autoscale()

                sparam = SParamMeasurement.from_mag_phase(magmeas, phasemeas)
                sparam.mref = 'c'.join(map(str, (i, j)))
                logger.debug("Writing the S21 measurement.")
                self.mio.write(sparam)

            jpac.deactivate_pump()
            jpac.set_pump_power(self.power_sweep[0])

    @classmethod
    def make(
            cls,
            resources: t.Dict[str, str],
            current_sweep: np.ndarray,
            power_sweep: np.ndarray,
            path: str):
        jpac = JPAController.make(resources=resources)
        im = jpac.instrument_manager
        mio = MIOFileCSV.open(path, mode='w')
        swptable = SweepTable.make(
                csweep=current_sweep,
                psweep=power_sweep)
        mio.write(swptable)

        return cls(
                jpac=jpac,
                instrument_manager=im, resources=resources,
                current_sweep=current_sweep,
                power_sweep=power_sweep,
                mio=mio)


def main():
    __measurement_name__ = "GAIN MAP"
    uris = {
            'vna': 'TCPIP::10.10.0.37::hislip0::INSTR',
            'sg1': 'TCPIP0::10.10.0.202::inst0::INSTR',
            # 'sa': ('TCPIP::10.10.0.74::inst0::INSTR', RohdeSchwarzFSV),
            # 'tc': 'TCPIP0::10.10.1.150::7777::SOCKET',
            'cs': 'TCPIP0::10.10.0.201::5025::SOCKET'
    }
    current_step: float = 5e-7
    current_ramp_rate: float = 5e-6

    # Reverses the order of sweeping
    reverse = False

    # Current
    curr_min, curr_max, npoints = -10e-6, 80e-6, 91
    curr_step = (curr_max - curr_min)/(npoints-1)

    # SG powers
    pow_min, pow_max, pow_step = (-7, 19.5, 0.5)
    powers = np.arange(pow_min, pow_max, pow_step)

    # VNA Power when measuring GAIN
    vna_gain_power = -30
    vna_if_bw = 10

    currents = np.linspace(curr_min, curr_max, npoints)
    if reverse:
        currents = np.flip(currents)

    path = Path('~/Measurements/190817_GAINMAP').expanduser()
    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    mc = MainController.make(
        resources=uris,
        current_sweep=currents,
        power_sweep=powers,
        path=path)

    mc.gain_preset = mc.gain_preset.evolve(
            power=vna_gain_power,
            if_bandwidth=vna_if_bw)

    mc.current_ramp_rate = current_ramp_rate
    mc.current_step = current_step

    logger.info(f"{__measurement_name__} MEASUREMENT")
    logger.info("Current sweep (min, max, step) = "
                f"({curr_min*1e6:.3f},"
                f" {curr_max*1e6:.3f},"
                f" {curr_step*1e6:.3f}) μA")
    logger.info("Power sweep (min, max, step) = "
                f"({pow_min*1e6:.3f},"
                f" {pow_max*1e6:.3f},"
                f" {pow_step*1e6:.3f}) μA")

    mc.initiate()
    mc.run()
    logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")


if __name__ == "__main__":
    main()
