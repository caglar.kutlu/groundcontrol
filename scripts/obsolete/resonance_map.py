import typing as t
from pathlib import Path
import sys

import numpy as np

import groundcontrol
groundcontrol.USE_UNICODE = False

from groundcontrol.instruments import KeysightN5232A, Instrument
from groundcontrol.instruments import RohdeSchwarzFSV
from groundcontrol.controllers.jpa import JPAController
from groundcontrol.controller import Controller
from groundcontrol.settings import VNASettings, setting
from groundcontrol.logging import logger
from groundcontrol.measurement import NetworkParams1PModel
from groundcontrol.measurementio import MIOFileCSV


def find_dip(meas: NetworkParams1PModel):
    """Returns the frequency where minimum occurs."""
    f = meas.frequencies
    p = meas.values
    dipi = np.argmin(p)
    return f[dipi]


class MainController(Controller):
    jpac: JPAController

    current_sweep: np.ndarray
    mio: MIOFileCSV

    save_mag: bool = False
    save_coarse: bool = False
    save_generic: bool = False

    generic_preset: VNASettings = setting(
            default=VNASettings(
                start_frequency=2e9,
                stop_frequency=2.35e9,
                if_bandwidth=2e3,
                sweep_step=100e3,
                power=3
                ))

    coarse_preset: VNASettings = setting(
             default=VNASettings(
                            span=40e6,
                            if_bandwidth=1e3,
                            sweep_step=100e3,
                            power=-6
                            ))

    narrow_preset: VNASettings = setting(
            default=VNASettings(
                span=10e6,
                if_bandwidth=100,
                sweep_step=100e3,
                power=-20
                ))

    current_step: float = 1e-7
    current_ramp_rate: float = 1e-6

    def __setup__(self):
        self.jpac.initiate()
        self.jpac.current_step = self.current_step
        self.jpac.current_ramp_rate = self.current_ramp_rate
        self.jpac.deactivate_pump()

    def find_resonance_coarse(
            self,
            save: bool = False,
            mref: t.Optional[str] = None) -> float:
        jpac = self.jpac
        jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.MLOGARITHMIC)

        logger.info("Turning on calibration.")
        jpac.turn_on_vna_calibration()
        jpac.trigger_network_analyzer()
        s21mag = jpac.read_network_mag()
        fres = find_dip(s21mag)

        if save:
            s21mag.mref = mref
            self.mio.write(s21mag)

        logger.info(f"Found resonance at {fres/1e9:.6f} GHz.")
        return fres

    def measure_phase(self) -> NetworkParams1PModel:
        """Measures a phase spectrum."""
        jpac = self.jpac
        jpac.trigger_network_analyzer()
        return jpac.read_network_phase()

    def zoom_and_measure_phase(self, zoomf: float) -> NetworkParams1PModel:
        jpac = self.jpac
        pre = self.narrow_preset.evolve(center_frequency=zoomf)
        jpac.do_vna_preset(pre)
        jpac.vna.set_calculate_format(jpac.vna.DisplayFormat.UPHASE)
        logger.info("Turning off calibration for phase measurement.")
        jpac.turn_off_vna_calibration()
        return self.measure_phase()

    def run(self):
        jpac: JPAController = self.jpac
        for current in self.current_sweep:
            absc = np.abs(current)
            rangenow = jpac.cs.query_range_i()
            # ranges should be ordered in ascending order
            ranges = (1e-4, 1e-3, 1e-2)
            for rng in ranges:
                if absc < rng:
                    newrange = rng
                    break
            if newrange != rangenow:
                logger.info(f"Setting range to {newrange*1e3:.3f} mA.")
                jpac.cs.set_range_i(newrange)

            jpac.ramp_current(current)
            # Measured current and error
            cmeas = jpac.measure_current(10)

            logger.info("Performing a coarse resonance search with wide span.")
            jpac.do_vna_preset(self.generic_preset)
            if self.save_generic:
                f1 = self.find_resonance_coarse(save=True, mref="GENERIC")
            else:
                f1 = self.find_resonance_coarse()

            logger.info("Performing a coarse resonance search with narrow span.")
            jpac.do_vna_preset(self.coarse_preset.evolve(center_frequency=f1))
            if self.save_coarse:
                f= self.find_resonance_coarse(save=True, mref="COARSE")
            else:
                f = self.find_resonance_coarse()

            phasemeas = self.zoom_and_measure_phase(f)
            phasemeas.mref = "PHASE"
            logger.debug("Writing current measurement")
            self.mio.write(cmeas)
            logger.debug("Writing phase measurement")
            self.mio.write(phasemeas)
            self.jpac.vna.do_display_window_trace_y_scale_auto()

            if self.save_mag:
                mag = self.jpac.read_network_mag()
                self.jpac.vna.do_display_window_trace_y_scale_auto()
                mag.mref = "MAG"
                logger.debug("Writing mag measurement")
                self.mio.write(mag)

    _ResourceType = t.Union[str, t.Tuple[str, t.Type[Instrument]]]
    @classmethod
    def make(
            cls,
            resources: t.Dict[str, _ResourceType],
            current_sweep: np.ndarray,
            path: str):
        """Creates the Controller object.

        Args:
            resources:  A dictionary mapping the instrument names to
                the addresses of their resources.  Optionally can be a
                dictionary mapping to tuples where the tuples first element is
                the resource address and the second element is the particular
                `Instrument` class to be used when constructing the
                instrument.

            current_sweep:  1-d array of values to be swept.
            path:  Location where to save the files.

        """

        jpac = JPAController.make(resources=resources)
        im = jpac.instrument_manager
        mio = MIOFileCSV.open(path, mode='w')

        classes = {}

        for k, v in resources.items():
            try:  # if a mapping of str -> tuple
                rscaddr, inscls = v
            except ValueError:  # elif a mapping of  str -> str
                rscaddr = v
                inscls = None
            resources[k] = rscaddr
            if inscls is not None:
                classes[k] = inscls

        return cls(jpac=jpac,
                instrument_manager=im,
                resources=resources,
                resource_classes=classes,
                current_sweep=current_sweep, mio=mio)


def main():
    uris_bf5 = {
            'vna': 'TCPIP::10.10.0.37::hislip0::INSTR',
            'sg1': 'TCPIP0::10.10.0.202::inst0::INSTR',
            'cs': 'TCPIP0::10.10.0.201::5025::SOCKET'
    }

    uris_bf4 = {
            'vna': 'TCPIP::10.10.1.100::hislip0::INSTR',
            'sg1': 'TCPIP0::10.10.1.103::inst0::INSTR',
            'cs': 'TCPIP0::10.10.1.200::5025::SOCKET',
    }

    uris = uris_bf5

    current_step: float = 5e-7
    current_ramp_rate: float = 5e-6
    save_mag = True
    save_generic = True
    save_coarse = True

    # Reverses the order of sweeping
    reverse = False

    curr_min, curr_max, npoints = -400e-6, 400e-6, 201
    currents = np.linspace(curr_min, curr_max, npoints)
    if reverse:
        currents = np.flip(currents)

    path = Path('~/Measurements/190914/RESONANCE_MAP_B8T').expanduser()
    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    mc = MainController.make(
        resources=uris,
        current_sweep=currents,
        path=path)

    mc.current_ramp_rate = current_ramp_rate
    mc.current_step = current_step
    mc.save_mag = save_mag
    mc.save_generic = save_generic
    mc.save_coarse = save_coarse

    logger.info("RESONANCE MAP MEASUREMENT")
    logger.info("Current (min, max, step) = "
                f"({curr_min*1e6:.3f},"
                f" {curr_max*1e6:.3f},"
                f" {curr_max*1e6:.3f}) microA")
    mc.initiate()
    mc.run()
    logger.info("RESONANCE MAP MEASUREMENT FINISHED")


if __name__ == "__main__":
    main()
