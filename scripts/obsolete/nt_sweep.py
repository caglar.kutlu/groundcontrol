"""This script is to take NT data at different resonance frequencies and half
pump offsets relative to the resonance frequencies.  It can be considered as
noise temperature vs absolute tuning frequency.

Note that the sweep follows the pseudocode:

    gain = some-value
    peak_frequency_offset = some-value
    resonance_frequencies = {list-of-values}

    # peak_frequencies = resonance_frequencies + peak_frequency_offset

    for frequency in resonance_frequencies:
        tune_jpa(frequency, gain, peak_frequency_offset)
        for temperature in noise_source_temperatures:
            set_noise_source_temperature(temperature)
            wait_until_temperature_stabilizes()
            measure_gain_spectrum()
            measure_noise_spectrum()

Because the temperature change is in the inner loop and since temperature
change has a higher time-cost, this is not the most efficient way of performing
this sweep.  However, this ensures that each tuning step will have it's
measurement completed in the shortest amount of time possible.
"""
import typing as t
from pathlib import Path
from datetime import datetime
from shutil import copyfile
import argparse
import sys

import attr
import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol import JPATuner, WorkingPoint, TuningPoint
from groundcontrol.settings import InstrumentSettings, VNASettings, SASettings
from groundcontrol.declarators import setting, declarative, quantity, \
    parameter
from groundcontrol.measurement import MeasurementModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.pid import TunerSettings
from groundcontrol.logging import setup_logger, create_log, INFO
import groundcontrol.units as un
from groundcontrol.helper import count
from groundcontrol.resources import get_uris


__script_version__ = "0.2"

logger = setup_logger("nt_frequency_sweep")
logger.info(f"groundcontrol version: {__version__}")


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class SweepParameters(InstrumentSettings):
    resonance_freqs: np.ndarray = setting(un.hertz, "Resonance Frequency")
    source_temps: np.ndarray = setting(un.kelvin, "Noise Source Temperatures")

    @classmethod
    def make(cls, resonance_freqs, source_temps):
        return cls(resonance_freqs, source_temps)

    @classmethod
    def from_ranges(
            cls,
            resf_rng,
            temp_rng,
            repeated: t.Optional[int] = None):
        """Creates sweep parameters from given ranges given in the format
        (start, stop, step), start and stop inclusive.  For numerical
        consistency, provide integer values.
        
        Args:
            resf_rng:  Resonance frequency range tuple (min, max, step).
            temp_rng:  NS temperature range tuple (min, max, step).
            repeated:  Repeats resonance frequency setpoints for given amount.
                If `None`, no repeating is done.
        """
        t_min, t_max, t_step = temp_rng
        fr_min, fr_max, fr_step = resf_rng

        resonance_frequencies = fullrange(fr_min, fr_max, fr_step)
        if repeated is not None:
            resonance_frequencies = np.repeat(resonance_frequencies, repeated)
             
        source_temperatures = fullrange(t_min, t_max, t_step)

        return cls.make(
                resonance_frequencies,
                source_temperatures)

    def to_table(self):
        args = (self.resonance_freqs, self.source_temps)

        # indexing ij ensures matrix style indexing
        cols = map(lambda ar: ar.flatten(), np.meshgrid(*args, indexing='ij'))
        return np.vstack(list(cols)).T

    def to_csv(self, fname: str):
        header = ("Resonance Frequency, "
                  "Source Temperature")
        np.savetxt(fname, self.to_table(), delimiter=',', header=header)


class TemperatureMeasurement(MeasurementModel):
    temperature: float = quantity(un.kelvin, "Temperature")
    stdev: float = quantity(un.kelvin, "Temperature Stdev")
    settle_delay: float = quantity(un.second, "Settle Delay")
    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))


class TuneFail(Exception):
    pass


@declarative
class MainController:
    jpac: JPAController
    tuner: JPATuner

    mio: MIOFileCSV

    sweep_params: SweepParameters = setting()

    # Tuning Parameters
    temperature_settle_delay: float = setting(un.second, default=300)
    temperature_settle_timeout: float = setting(un.second, default=600)

    sa_preset: SASettings = setting(
            un.nounit, "SA Preset",
            default=SASettings(
                span=500e3,
                rbw=1e3,
                vbw=1,
                npoints=500,
                naverage=100))

    target_gain: float = setting(
            un.dBm, "Target Gain", default=20)

    fpeak_offset: float = setting(
            un.hertz, "Frequency Peak Offset", default=0)

    res_maxerror: float = setting(
            un.hertz, "Maximum Resonance Error", default=50e3,
            description="Maximum acceptable resonance error when tuning.")

    gain_maxerror: float = setting(
            un.dB, "Maximum Gain Error", default=0.1,
            description="Maximum acceptable gain error when tuning.")

    rollback: bool = setting(
            un.nounit, "Rollback", default='False',
            description=(
                "Whether to return to original parameters before the "
                "script has started or not."))

    tuning_policy: str = setting(
            un.nounit, "Tuning Policy", default='resonance',
            description=(
                "If set to `resonance` the pump frequency is set so that the "
                "gain peak frequency is at the measured resonance frequency.  "
                "If set to `fpeak` the gain peak frequency is set at the "
                "target resonance frequency rather than the measured one."))

    skip_initial_temp_settle: bool = setting(
            un.nounit, "Skip Initial Noise Source Temperature Settle Time",
            default=False)

    notune: bool = setting(
            un.nounit,
            "If `True`, the tuning state will not be changed. "
            "Otherwise, the device will be tuned to given gain and frequency.",
            default=False)

    maxtrial: int = setting(
            un.nounit, "Maximum Trial After Failed Tuning", default=10)

    baseline_current: float = setting(
            un.ampere, "Bias Current when Measuring Baseline")

    _tups: t.Dict[t.Tuple[int, int], TuningPoint] = attr.ib(factory=dict)

    def _tune(self, fres):
        counter = count()
        while next(counter) < self.maxtrial:
            success = self.tuner.tune_absolute(
                        self.target_gain,
                        fres, self.fpeak_offset,
                        res_maxerr=self.res_maxerror,
                        gain_maxerr=self.gain_maxerror,
                        policy=self.tuning_policy)
            if success:
                return True
            else:
                logger.info("Tuning failed, trying again.")
                self.tuner.bootstrap()
        return False

    def _measure_baseline(self):
        c = self.jpac
        c.deactivate_pump()
        logger.info("Setting current for baseline measurement.")
        current = c.query_current_setpoint()
        self.finish()
        c.ramp_current(self.baseline_current)
        bl = self.tuner.measure_baseline()
        logger.info("Ramping current back to initial value.")
        c.ramp_current(current)
        return bl

    def initiate(self):
        self.jpac.sa_preset = self.sa_preset
        self.jpac.do_sa_preset(self.sa_preset)
        self.sweep_params.to_csv(self.mio.path / "sweep_params.csv")

        vna = self.jpac.vna
        vna.set_calculate_smoothing(vna.State.OFF)

        # We are assuming current vna is centered around resonance frequency.
        cursettings = self.jpac.read_vna_settings()
        if self.rollback:
            self._wp = self.tuner.query_working_point()
            logger.info(f"Rollback ON: {self._wp}")
        Tb = self._measure_baseline()
        self.tuner.Tb = Tb
        self.tuner.Tb.mref = "BASELINE"
        self.mio.write(self.tuner.Tb)
        self.jpac.do_vna_preset(cursettings)

        if self.notune:
            success = self.tuner.bootstrap_resonance(assumeresonance=True)
            self.tuner.update_tuning_point()
        else:
            success = self.tuner.bootstrap(assumeresonance=True)

        if not success:
            logger.error("Bootstrapping failed.")
            # this is not nice but anyways.
            raise TuneFail

    def run(self) -> bool:
        c = self.jpac
        tuner = self.tuner
        swpars = self.sweep_params
        fres_old = None
        for i, fres in enumerate(swpars.resonance_freqs):
            # SET TO MINIMUM TEMPERATURE FIRST
            temp0 = swpars.source_temps[0]
            logger.info(f"Initial setting of source "
                        f"temperature to {temp0*1e3} mK")
            if (i == 0) and self.skip_initial_temp_settle:
                settle_delay = 0
            else:
                settle_delay = self.temperature_settle_delay

            window, success = c.ramp_noise_source_temperature(
                    temp0,
                    settle_delay,
                    self.temperature_settle_timeout)

            if not success:
                logger.info("Temperature settling failed.  Aborting.")
                return False

            tempmeas = TemperatureMeasurement(
                    np.mean(window), np.std(window),
                    self.temperature_settle_delay)

            tpstr = f"f_r={fres/1e9} GHz, f_pko={self.fpeak_offset/1e3} kHz"
            tup_mref = f"i{i:d}"

            logger.info("(Re)measuring baseline for reference.")
            tuner.Tb = self._measure_baseline()
            tuner.Tb.mref = f"BASELINE-{tup_mref}"
            self.mio.write(self.tuner.Tb)

            if self.notune:
                logger.info(f"Skipping tuning:  NOTUNE option.")
                tup = self.tuner.tup
                self._tups[i] = tup
            elif (fres == fres_old):
                logger.info(f"Skipping tuning:  Same resonance frequency.")
                tup = self.tuner.tup
                self._tups[i] = tup
            else:
                logger.info(f"TUNING to tuning point: {tpstr}")
                # Tuning
                success = self._tune(fres)
                if not success:
                    logger.warning(f"Tuning to {tpstr} failed. "
                                   "Continuing with next point.")
                    continue
                else:
                    tup = self.tuner.tup
                    self._tups[i] = tup

            # Recording tuning point

            tup.mref = tup_mref
            self.mio.write(tup)

            for j, temp in enumerate(self.sweep_params.source_temps):
                if j != 0:
                    # For the first temperature we already settled
                    logger.info(f"Setting source temperature to {temp*1e3} mK")
                    window, success = c.ramp_noise_source_temperature(
                            temp,
                            self.temperature_settle_delay,
                            self.temperature_settle_timeout)

                    if not success:
                        logger.info("Temperature settling failed.  Aborting.")
                        return False

                    tempmeas = TemperatureMeasurement(
                            np.mean(window), np.std(window),
                            self.temperature_settle_delay)
                mref = f"i{i:d}j{j:d}"
                tempmeas.mref = mref
                self.mio.write(tempmeas)

                logger.info("Measuring resonance again for reference.")
                self.jpac.deactivate_pump()
                rspect = self.tuner.measure_resonance_spectrum()
                rspect.mref = f"RESONANCE-{mref}"
                self.mio.write(rspect)
                self.jpac.activate_pump()

                logger.info("Measuring gain spectrum again for reference.")
                gspect = self.tuner.measure_gain()
                gspect.mref = f"GAIN-{mref}"
                self.mio.write(gspect)

                logger.info("Measuring noise spectrum.")
                noise_spectrum = self.measure_spectrum(tup.peak_frequency)
                noise_spectrum.mref = mref
                self.mio.write(noise_spectrum)

            # for checking last frequency later
            fres_old = fres
        return True

    def measure_spectrum(self, fpeak: float) -> MeasurementModel:
        c = self.jpac
        # Important to turn off vna power
        c.deactivate_signal()
        c.do_sa_preset(self.sa_preset.evolve(
            center_frequency=fpeak))
        spectrum = c.measure_spectrum()
        c.activate_signal()
        return spectrum

    def finish(self):
        c = self.jpac
        logger.info("Setting noise source temperature to initial temperature.")
        c.set_noise_source_temperature(
                self.sweep_params.source_temps[0])
        if self.rollback:
            logger.info(f"Rolling back to initial WP: {self._wp}")
            self.tuner.set_working_point(self._wp)

    @classmethod
    def make(
            cls,
            controller: JPAController,
            tuner: JPATuner,
            sweep_params: SweepParameters,
            path: t.Union[Path, str]):
        mio = MIOFileCSV.open(path, mode='w')
        return cls(controller, tuner, mio, sweep_params)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def setup_parser():
    parser = argparse.ArgumentParser(
            description=(
                "Performs noise temperature measurement sweep at given "
                "frequency range.  The VNA must be centered at resonance "
                "before starting the script."))

    parser.add_argument(
            '-p',
            '--path',
            type=Path,
            help="Path to the folder to be used when saving data.")

    parser.add_argument(
            '--rollback',
            action='store_const',
            const=True, default=False,
            help="Rollback to the initial JPA parameters after measurement is "
                 "done or something fails.")

    parser.add_argument(
            '-n',
            '--notune',
            action='store_const',
            const=True, default=False,
            help="Measure the noise temperature without changing "
                 "any bias conditions.  Note that bias conditions may "
                 "be temporarily changed for auxiliary measurements, but "
                 "they will be restored to their initial values during "
                 "relevant measurements.")

    parser.add_argument(
            '-r',
            '--repeat',
            type=int,
            default=1,
            help=("Number of times to repeat measurements at set frequencies."
                  "Defaults to 1."))

    return parser


parser = setup_parser()


def main(
        path: Path,
        isnotune: bool = False,
        repeated: t.Optional[int] = None,
        rollback: bool = False):
    """Must be started when the VNA is centered on the current resonance
    frequency.
    """
    __measurement_name__ = "NT Frequency Sweep"
    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    uris = get_uris()

    # Measurement Parameters ##
    # Resonance Frequency (Hz)
    fr_min = 2.28e9
    fr_step = 16e3
    fr_max = fr_min + 0*fr_step

    # Noise Source Temperatures (K)
    t_min = 0.07
    t_max = 0.28
    t_step = 0.035/2

    # Sweep parameters (start stop inclusive)
    sweep_params = SweepParameters.from_ranges(
            (fr_min, fr_max, fr_step),
            (t_min, t_max, t_step),
            repeated)

    logger.info(sweep_params)

    # Script parameters
    tuning_policy = 'resonance'  # 'fpeak'  # ATTENTION!
    peakfinder = 'argmax'   # peak finder during res. est.
    res_maxerror = 20e3
    gain_maxerror = 0.05
    noise_source_control_channel = "02"
    skip_initial_temp_settle = True
    temperature_settle_delay = 1*60  # seconds
    temperature_settle_timeout = 30*60  # seconds
    pump_ramp_rate = 10  # dB/s  we don't need to be so careful
    pump_step = 1  # minimum pump step when ramping up
    current_ramp_rate = 10e-6  # A/s  we don't need to be so careful
    current_step = 1e-6  # A/s  we don't need to be so careful (???)
    dfres_sign = -1  # sign of the slope of the resonance-current curve

    tuning_window = (5e9, 6.1e9)
    dfo = -1e3  # This is the gain measurement offset

    boot_step = 0.5  # dB
    boot_threshold = 14  # dB
    boot_ifbw = 100  # Hz

    zet_max = WorkingPoint(100e-6, tuning_window[1]*2, 25)
    zet_min = WorkingPoint(-100e-6, tuning_window[0]*2, -20)

    res_tuner_set = TunerSettings(
                kp=0.5e-12,
                ki=8,
                kd=1e-5,
                ramp_step=200e3)

    gain_tuner_set = TunerSettings(
                kp=0.01,
                ki=10,
                kd=0,
                ramp_step=1)

    # Presets for VNA
    res_preset = VNASettings(
                span=100e6, sweep_step=100e3, if_bandwidth=100, power=-20)

    baseline_current = -20e-6  # current to move resonance far
    baseline_preset = VNASettings(
                start_frequency=tuning_window[0],
                stop_frequency=tuning_window[1],
                if_bandwidth=100, power=10, sweep_step=1e6)

    offset_preset = VNASettings(
                span=10, sweep_step=1, power=-30, if_bandwidth=2)

    gain_preset = VNASettings(
                span=10e6, sweep_step=50e3, power=-30, if_bandwidth=5)

    sa_preset = SASettings(
                span=10e6,
                rbw=10e3,
                vbw=1,
                npoints=1000,
                naverage=20)

    target_gain = 21  # dB, offset gain
    fpeak_offset = 0

    # Initializing
    c = JPAController.make(resources=uris)

    tuner = JPATuner.make(
            controller=c,
            Df_sign=dfres_sign,
            wt=tuning_window,
            dfo=dfo,
            gain_bootstrap_step=boot_step,
            gain_bootstrap_threshold=boot_threshold,
            gain_bootstrap_ifbw=boot_ifbw,
            zet_max=zet_max,
            zet_min=zet_min,
            restun=res_tuner_set,
            gaintun=gain_tuner_set,
            res_settings=res_preset,
            baseline_settings=baseline_preset,
            offset_settings=offset_preset,
            gain_settings=gain_preset,
            peakfinder=peakfinder
            )

    mc = MainController.make(
            c,
            tuner,
            sweep_params=sweep_params,
            path=path)

    c.verbose = False
    c.noise_source_control_channel = noise_source_control_channel
    c.pump_ramp_rate = pump_ramp_rate
    c.pump_step = pump_step
    c.current_ramp_rate = current_ramp_rate
    c.current_step = current_step

    mc.rollback = rollback
    mc.notune = isnotune
    mc.tuning_policy = tuning_policy
    mc.skip_initial_temp_settle = skip_initial_temp_settle
    mc.res_maxerror = res_maxerror
    mc.gain_maxerror = gain_maxerror
    mc.sa_preset = sa_preset
    mc.temperature_settle_delay = temperature_settle_delay
    mc.temperature_settle_timeout = temperature_settle_timeout
    mc.target_gain = target_gain
    mc.fpeak_offset = fpeak_offset
    mc.baseline_current = baseline_current

    try:
        logger.info(f"{__measurement_name__} STARTING")
        c.initiate()
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    except TuneFail:
        logger.info("Tuning failed.  Aborting.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{path}'")
        save_script(path)
    logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")
    return True


if __name__ == "__main__":
    args = parser.parse_args()
    if args.path.exists():
        print(f"'{args.path}' exists! Aborting.")
        sys.exit(42)
    else:
        args.path.mkdir()

    main(args.path, args.notune, args.repeat, args.rollback)
