"""Quick script to do measurement on BF4. """
import typing as t
from pathlib import Path
from datetime import datetime
from shutil import copyfile
import sys

import attr
import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol import JPATuner, WorkingPoint, TuningPoint
from groundcontrol.settings import InstrumentSettings, VNASettings, SASettings
from groundcontrol.declarators import setting, declarative, quantity, parameter
from groundcontrol.measurement import MeasurementModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.pid import TunerSettings
from groundcontrol.logging import setup_logger, create_log, INFO
import groundcontrol.units as un
from groundcontrol.helper import count

print(f"groundcontrol version: {__version__}")

logger = setup_logger("noise_temp_vs_gain")


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class SweepParameters(InstrumentSettings):
    source_temps: np.ndarray = setting(un.kelvin, "Noise Source Temperatures")
    offset_gains: np.ndarray = setting(un.dB, "Offset Gains")

    @classmethod
    def make(cls, source_temps, offset_gains):
        return cls(source_temps, offset_gains)

    @classmethod
    def from_ranges(
            cls,
            temp_rng,
            gain_rng):
        """Creates sweep parameters from given ranges given in the format
        (start, stop, step), start and stop inclusive.  For numerical
        consistency, provide integer values.
        """
        t_min, t_max, t_step = temp_rng
        gain_min, gain_max, gain_step = gain_rng

        gains = fullrange(gain_min, gain_max, gain_step)
        source_temperatures = fullrange(t_min, t_max, t_step)

        return cls.make(
                source_temperatures,
                gains)

    def to_table(self):
        args = (self.source_temps, self.offset_gains)

        # indexing ij ensures matrix style indexing
        cols = map(lambda ar: ar.flatten(), np.meshgrid(*args, indexing='ij'))
        return np.vstack(list(cols)).T

    def to_csv(self, fname: str):
        header = ("Source Temperature [K], "
                  "Offset Gain [dB]")
        np.savetxt(fname, self.to_table(), delimiter=',', header=header)


class TemperatureMeasurement(MeasurementModel):
    temperature: float = quantity(un.kelvin, "Temperature")
    stdev: float = quantity(un.kelvin, "Temperature Stdev")
    settle_delay: float = quantity(un.second, "Settle Delay")
    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))


@declarative
class MainController:
    jpac: JPAController
    tuner: JPATuner

    mio: MIOFileCSV

    sweep_params: SweepParameters = setting()

    # Tuning Parameters
    temperature_settle_delay: float = setting(un.second, default=300)
    temperature_settle_timeout: float = setting(un.second, default=600)

    sa_preset: SASettings = setting(
            un.nounit, "SA Preset",
            default=SASettings(
                span=500e3,
                rbw=1e3,
                vbw=10,
                npoints=500,
                naverage=100))

    target_resf: float = setting(
            un.hertz, "Target Resonance", default=2.300e9)

    target_peak_offset: float = setting(
            un.hertz, "Target Peak Offset", default=0)
    init_current: float = setting(
            un.hertz, "Initial Current", default=0,
            description="The DC flux is biased with this current when "
                        "estimating baseline.")
    res_maxerror = 50e3

    maxtrial: int = setting(
            un.nounit, "Maximum Trial After Failed Tuning", default=10)

    _tups: t.Dict[int, TuningPoint] = attr.ib(factory=dict)

    def _tune(self, j, gain):
        counter = count()
        while next(counter) < self.maxtrial:
            success = self.tuner.tune_absolute(
                    gain=gain,
                    fpeak_offset=self.target_peak_offset)

            if success:
                return True
            else:
                logger.info("Tuning failed, trying again.")
                self.tuner.bootstrap()
        return False

    def initiate(self):
        create_log(self.mio.path / "groundcontrol.log", INFO)
        self.jpac.sa_preset = self.sa_preset
        self.jpac.do_sa_preset(self.sa_preset)
        self.sweep_params.to_csv(self.mio.path / "sweep_params.csv")

        logger.info("Setting bias to the baseline "
                    "estimation point.")
        ib0 = self.tuner.working_point.ib

        wpinit = WorkingPoint(ib=self.init_current)
        self.tuner.set_wp(wpinit)

        logger.info("Baseline estimation.")
        self.tuner.bootstrap_resonance()
        self.tuner.Tb.mref = "BASELINE"
        self.mio.write(self.tuner.Tb)
        Tb = self.tuner.Tb

        logger.info("Setting bias to it's original value.")
        self.tuner.set_wp(wpinit.evolve(ib=ib0))

        logger.info("Bootstrapping.")
        self.tuner.bootstrap()
        # This function also sets the baseline, but we want to use the previous
        # measurement this is a workaround for now
        self.tuner.Tb = Tb

        logger.info(f"Tuning resonance to target: {self.target_resf}")
        self.tuner.tune_resonance(
                self.target_resf,
                maxerror=self.res_maxerror)

    def do_sweep(self, i: int) -> bool:
        swpars = self.sweep_params
        for j, gain in enumerate(swpars.offset_gains):
            tpstr = f"Target Gain: {gain:.2f} dB"
            if i > 0:
                logger.info(f"MOVING to tuning point {tpstr}")
                # We should have already measured this tuning point.
                tup0 = self._tups[j]
                self.tuner.move_to(tup0)
                tup = self.tuner.tup
            else:
                logger.info(f"TUNING to tuning point {tpstr}")
                success = self._tune(j, gain)
                if not success:
                    return False
                else:
                    tup = self.tuner.tup
                    self._tups[j] = tup
            mref = f"i{i:d}j{j:d}"
            tup.mref = mref
            self.mio.write(tup)

            logger.info("Measuring resonance again for reference.")
            self.jpac.deactivate_pump()
            rspect = self.tuner.measure_resonance_spectrum()
            rspect.mref = f"RESONANCE-{mref}"
            self.mio.write(rspect)
            self.jpac.activate_pump()

            logger.info("Measuring noise spectrum.")
            noise_spectrum = self.measure_spectrum(tup.peak_frequency)
            noise_spectrum.mref = mref
            self.mio.write(noise_spectrum)
        return True

    def measure_spectrum(self, fpeak: float) -> MeasurementModel:
        c = self.jpac
        c.do_sa_preset(self.sa_preset.evolve(
            center_frequency=fpeak))
        spectrum = c.measure_spectrum()
        return spectrum

    def run(self) -> bool:
        c = self.jpac
        for i, temp in enumerate(self.sweep_params.source_temps):
            logger.info(f"Setting noise source temperature to: {temp*1e3} mK")
            window, success = c.ramp_noise_source_temperature(
                    temp,
                    self.temperature_settle_delay,
                    self.temperature_settle_timeout)

            if not success:
                logger.info("Temperature settling failed.  Aborting.")
                return False

            tempmeas = TemperatureMeasurement(
                    np.mean(window), np.std(window),
                    self.temperature_settle_delay)
            tempmeas.mref = f"i{i:d}"
            self.mio.write(tempmeas)

            sweepsuccess = self.do_sweep(i)
            if not sweepsuccess:
                logger.info("Tuning CATASTROPHICALLY failed, aborting.")
                break

    def finish(self):
        c = self.jpac
        c.set_noise_source_temperature(
                self.sweep_params.source_temps[0])

    @classmethod
    def make(
            cls,
            controller: JPAController,
            tuner: JPATuner,
            sweep_params: SweepParameters,
            path: t.Union[Path, str]):
        mio = MIOFileCSV.open(path, mode='w')
        return cls(controller, tuner, mio, sweep_params)


def save_script(path: Path):
    """Saves this script to the given location."""
    thisfpath = Path(__file__)
    return copyfile(thisfpath, path / thisfpath.name)


def main():
    __measurement_name__ = "Noise Temperature Map"
    uris = {
            'vna': 'TCPIP::10.10.1.100::hislip0::INSTR',
            'sg1': 'TCPIP0::10.10.1.103::inst0::INSTR',
            'sa': 'TCPIP::10.10.1.101::inst0::INSTR',
            'tc': 'TCPIP0::10.10.1.150::7777::SOCKET',
            'cs': 'TCPIP0::10.10.1.200::5025::SOCKET'
    }

    # Measurement Parameters ##
    # Resonance Frequency (Hz)
    gain_min = 16
    gain_max = 25
    gain_step = 0.5

    # Noise Source Temperatures (K)
    t_min = 0.08
    t_max = 0.4
    t_step = 0.02

    # Sweep parameters
    sweep_params = SweepParameters.from_ranges(
            (t_min, t_max, t_step),
            (gain_min, gain_max, gain_step))

    # Script parameters
    target_resf = 2.3e9
    init_current = -16.312e-6
    res_maxerror = 50e3
    target_peak_offset = 0

    temperature_settle_delay = 3*60  # seconds
    temperature_settle_timeout = 10*60  # seconds
    pump_ramp_rate = 10  # dBm/s  we don't need to be so careful
    dfres_sign = -1  # sign of the slope of the resonance-current curve

    tuning_window = (2.28e9, 2.31e9)
    dfo = -1e3  # This is the gain measurement offset

    boot_step = 1  # dB
    boot_threshold = 10  # dB
    boot_ifbw = 100  # Hz

    zet_max = WorkingPoint(100e-6, 2.32e9*2, 25)
    zet_min = WorkingPoint(-100e-6, 2.28e9*2, -10)

    res_tuner_set = TunerSettings(
                kp=0.4e-12,
                ki=4,
                kd=0.001,
                ramp_step=100e3)

    gain_tuner_set = TunerSettings(
                kp=0.04,
                ki=4,
                kd=0.01,
                ramp_step=1)

    # Presets
    res_preset = VNASettings(
                # span=10e6, sweep_step=50e3, if_bandwidth=500, power=-20)
                span=10e6, sweep_step=20e3, if_bandwidth=200, power=-20)

    baseline_preset = VNASettings(
                start_frequency=2.2e9, stop_frequency=2.31e9, if_bandwidth=100,
                power=-10, sweep_step=100e3)

    offset_preset = VNASettings(
                span=10, sweep_step=1, power=-30, if_bandwidth=10)

    gain_preset = VNASettings(
                span=500e3, sweep_step=10e3, power=-30, if_bandwidth=1)

    sa_preset = SASettings(
                span=500e3,
                rbw=1e3,
                vbw=10,
                npoints=500,
                naverage=100)

    # Saving folder
    path = Path('~/Measurements/191025/NT_VS_GAIN').expanduser()

    # Initializing
    c = JPAController.make(resources=uris)

    tuner = JPATuner.make(
            controller=c,
            Df_sign=dfres_sign,
            wt=tuning_window,
            dfo=dfo,
            gain_bootstrap_step=boot_step,
            gain_bootstrap_threshold=boot_threshold,
            gain_bootstrap_ifbw=boot_ifbw,
            zet_max=zet_max,
            zet_min=zet_min,
            restun=res_tuner_set,
            gaintun=gain_tuner_set,
            res_settings=res_preset,
            baseline_settings=baseline_preset,
            offset_settings=offset_preset,
            gain_settings=gain_preset
            )

    mc = MainController.make(
            c,
            tuner,
            sweep_params=sweep_params,
            path=path)

    c.pump_ramp_rate = pump_ramp_rate
    mc.sa_preset = sa_preset
    mc.temperature_settle_delay = temperature_settle_delay
    mc.temperature_settle_timeout = temperature_settle_timeout
    mc.target_resf = target_resf
    mc.init_current = init_current
    mc.target_peak_offset = target_peak_offset
    mc.res_maxerror = res_maxerror

    logger.info(f"{__measurement_name__} MEASUREMENT")
    try:
        c.initiate()
        tuner.initiate()
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{path}'")
        save_script(path)
    logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")

    return mc


if __name__ == "__main__":
    mc = main()
