"""This script measures NT at given setpoints.

Currently this is mainly copied from NT sweep.  Should be rewritten.
"""
import typing as t
from pathlib import Path
from datetime import datetime
from shutil import copyfile
import argparse
import sys, os
from itertools import chain, repeat

import attr
import numpy as np

from groundcontrol import __version__
from groundcontrol.analysis.calibrate import remove_baseline
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol import JPATuner, WorkingPoint, TuningPoint
from groundcontrol.settings import InstrumentSettings, VNASettings, SASettings
from groundcontrol.declarators import setting, declarative, quantity, \
    parameter
from groundcontrol.measurement import MeasurementModel, SParam1PModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.pid import TunerSettings
from groundcontrol.logging import setup_logger, create_log, INFO, \
        set_stdout_loglevel, DEBUG
import groundcontrol.units as un
from groundcontrol.helper import count
from groundcontrol.resources import get_uris
from groundcontrol.estimation import resonance_from_phase2, phasemodel
estimate_resonance = resonance_from_phase2


__script_version__ = "0.2"

logger = setup_logger("jpantscan")
logger.info(f"groundcontrol version: {__version__}")
isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class TemperatureMeasurement(MeasurementModel):
    temperature: float = quantity(un.kelvin, "Temperature")
    stdev: float = quantity(un.kelvin, "Temperature Stdev")
    settle_delay: float = quantity(un.second, "Settle Delay")
    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))


class ExperimentError(Exception):
    pass


@declarative
class MainController:
    jpac: JPAController
    tuner: JPATuner

    mio: MIOFileCSV

    wp_setpoints: t.List[WorkingPoint]
    tns_setpoints: t.List[float]  # K

    # Measurement settings
    temperature_settle_delay: float
    temperature_settle_timeout: float
    sa_preset: SASettings
    sa_preset_base: t.Optional[SASettings]
    vnaset_baseline: VNASettings
    vnaset_resonance: VNASettings

    # Current to use when measuring baseline
    baseline_current: float

    # Control options
    rollback: bool
    notune: bool
    measurebase: bool

    # Some defaults
    skip_initial_temp_settle: bool = True
    bootstrap_resonance_maxtrial: int = 10
    q_expected: float = 300
    _frfit_max_fstderr: float = 0.5e-3
    _tc_meas_window: float = 30
    _tc_meas_period: float = 0.2

    # Uninitialized privates
    _wp: t.Optional[WorkingPoint] = None
    _initial_vnaset: t.Optional[VNASettings] = None
    _bl: t.Optional[SParam1PModel] = None

    @property
    def wp(self):
        return self._wp

    def _measure_base_spectrum(self, fpeak: float):
        c = self.jpac
        logger.info("Setting current for base spectrum measurement.")
        c.deactivate_pump()
        current = c.query_current_setpoint()
        c.ramp_current(self.baseline_current)
        spect = self.measure_spectrum(fpeak, self.sa_preset_base)
        logger.info("Ramping current back to initial value.")
        c.ramp_current(current)
        return spect

    def _measure_baseline(self):
        c = self.jpac
        c.deactivate_pump()
        logger.info("Setting current for baseline measurement.")
        current = c.query_current_setpoint()
        c.ramp_current(self.baseline_current)
        bl = self.tuner.measure_baseline()
        logger.info("Ramping current back to initial value.")
        c.ramp_current(current)
        self._bl = bl
        return bl

    def bootstrap_resonance(
            self,
            assumeresonance: bool = False, saveres: bool = False,
            mref: str = "RES"):
        logger.info("Starting resonance bootstrap.")
        ntrial = self.bootstrap_resonance_maxtrial
        c = self.jpac
        c.deactivate_pump()

        if assumeresonance:
            fhint = self._initial_vnaset.center_frequency
        else:
            start_freq = self.vnaset_baseline.get_start_frequency()
            stop_freq = self.vnaset_baseline.get_stop_frequency()
            sweep_step = self.vnaset_baseline.sweep_step
            logger.debug(f"Coarse resonance Start/Stop: "
                         f"{start_freq/1e9:.6f}, {stop_freq/1e9:.6f} GHz")

            c.do_vna_preset(self.vnaset_resonance.evolve(
                start_frequency=start_freq,
                stop_frequency=stop_freq,
                center_frequency=None, span=None, sweep_step=sweep_step))

            for i in range(1, ntrial+1):
                tr = c.measure_transmission()
                c.do_vna_autoscale()
                bluphases = self._bl.uphases
                blfreqs = self._bl.frequencies
                freqs = tr.frequencies
                uphases = tr.uphases
                # interpolates and removes the baseline
                uphasecal = remove_baseline(
                        freqs, uphases,
                        blfreqs, bluphases, scale='log')
                # assume only uphase is used in estimation.
                tr.uphases = uphasecal

                bw_exp = np.mean((start_freq, stop_freq))/self.q_expected
                fhint = estimate_resonance(tr, bw_exp)
                if fhint is None:
                    logger.info(f"Resonance was not found ({i}/{ntrial}).")
                else:
                    logger.info(f"Zooming to resonance at {fhint/1e9:.6f} "
                                "GHz.")
                    break

        if fhint is None:
            logger.info("Resonance bootstrap failed.")
            return None

        # We got the hint now do narrow search
        c.do_vna_preset(self.vnaset_resonance.evolve(
            center_frequency=fhint))

        fr, bw = None, None
        for i in range(1, ntrial+1):
            tr = c.measure_transmission()
            bluphases = self._bl.uphases
            blfreqs = self._bl.frequencies
            freqs = tr.frequencies
            uphases = tr.uphases
            # interpolates and removes the baseline
            uphasecal = remove_baseline(
                    freqs, uphases,
                    blfreqs, bluphases, scale='log')
            # assume only uphase is used in estimation.
            c.do_vna_autoscale()
            pars = phasemodel.guess(tr.uphases, tr.frequencies)
            fit = phasemodel.fit(tr.uphases, f=tr.frequencies, params=pars)
            logger.debug(fit.fit_report())
            bp = fit.params
            if (bp['fr'].stderr is None) or (
                    bp['fr'].stderr/bp['fr'].value >= self._frfit_max_fstderr):
                # Resonance not found
                fstderr = bp['fr'].stderr/bp['fr'].value
                logger.debug(f"Fractional error: {fstderr:.3f}//Limit: "
                             f"{self._frfit_max_fstderr:.3f}")
                fr = None
            else:
                # Resonance found
                fr, bw = bp['fr'].value, bp['bw'].value

            if fr is None:
                logger.info(f"Resonance was not found ({i}/{ntrial}).")
            else:
                logger.info(f"Resonance found at {fr/1e9:.6f} "
                            "GHz.")
                if saveres:
                    tr.mref = mref
                    self.mio.write(tr)
                break
        self.tuner.Fr = fr
        if fr is None:
            msg = "Bootstrapping failed."
            logger.error(msg)
            # this is not nice but anyways.
            raise ExperimentError(msg)
        return fr, bw

    def initiate(self):
        self.jpac.sa_preset = self.sa_preset
        self.jpac.do_sa_preset(self.sa_preset)

        vna = self.jpac.vna
        vna.set_calculate_smoothing(vna.State.OFF)

        # We are assuming current vna is centered around resonance frequency.
        cursettings = self.jpac.read_vna_settings()
        self._initial_vnaset = cursettings
        if self.rollback:
            self._wp = self.tuner.query_working_point()
            logger.info(f"Rollback ON: {self._wp}")

        if self.notune:
            self._wp = self.tuner.query_working_point()
            logger.info(f"NOTUNE ON")
            self.wp_setpoints = [self._wp]
        Tb = self._measure_baseline()
        self.tuner.Tb = Tb
        self.tuner.Tb.mref = "BASELINE"
        self.mio.write(self.tuner.Tb)
        self.jpac.do_vna_preset(cursettings)

    def set_working_point(self, wp: WorkingPoint):
        self.tuner.set_working_point(wp)
        # this is an oxymoron workaround for some inconsistency in
        # jpa_tuner
        self.tuner.fpeak = wp.fp/2

    def update_tuning_point(self):
        # This forces tuner to measure gain again.
        self.tuner.Go = None
        self.tuner.update_tuning_point()

    def activate_calibration(self):
        self.jpac.turn_on_vna_calibration()

    def run(self) -> bool:
        c = self.jpac
        tuner = self.tuner

        wp_s = self.wp_setpoints
        tns_s = self.tns_setpoints

        for i, wp in enumerate(wp_s):
            # SET TO MINIMUM TEMPERATURE FIRST
            temp0 = tns_s[0]
            logger.info(f"Initial setting of source "
                        f"temperature to {temp0*1e3} mK")
            if (i == 0) and self.skip_initial_temp_settle:
                settle_delay = 0
            else:
                settle_delay = self.temperature_settle_delay

            window, success = c.ramp_noise_source_temperature(
                    temp0,
                    settle_delay,
                    self.temperature_settle_timeout,
                    measurement_window=self._tc_meas_window,
                    measurement_period=self._tc_meas_period)

            if not success:
                logger.info("Temperature settling failed.  Aborting.")
                return False

            tempmeas = TemperatureMeasurement(
                    np.mean(window), np.std(window),
                    self.temperature_settle_delay)

            # mref for outer loop
            tup_mref = f"i{i:d}"
            logger.info("(Re)measuring baseline for reference.")
            tuner.Tb = self._measure_baseline()
            tuner.Tb.mref = f"BASELINE-{tup_mref}"
            self.mio.write(self.tuner.Tb)

            logger.info(f"Setting {wp}")
            self.set_working_point(wp)

            logger.info(f"(Re)activating calibration.")
            self.activate_calibration()

            fr, bw = self.bootstrap_resonance()
            self.tuner.Fr = fr

            logger.info("Updating tuning point info.")
            self.update_tuning_point()
            tup = self.tuner.tup
            logger.info(tup.pretty())
            tup.mref = tup_mref

            self.mio.write(tup)
            for j, temp in enumerate(tns_s):
                if j != 0:
                    # For the first temperature we already settled
                    logger.info(f"Setting source temperature to {temp*1e3} mK")
                    window, success = c.ramp_noise_source_temperature(
                            temp,
                            self.temperature_settle_delay,
                            self.temperature_settle_timeout,
                            measurement_window=self._tc_meas_window,
                            measurement_period=self._tc_meas_period)

                    if not success:
                        logger.info("Temperature settling failed.  Aborting.")
                        return False

                    tempmeas = TemperatureMeasurement(
                            np.mean(window), np.std(window),
                            self.temperature_settle_delay)
                mref = f"i{i:d}j{j:d}"
                tempmeas.mref = mref
                self.mio.write(tempmeas)

                logger.info("Measuring resonance again for reference.")
                self.jpac.deactivate_pump()
                rspect = self.tuner.measure_resonance_spectrum()
                rspect.mref = f"RESONANCE-{mref}"
                self.mio.write(rspect)

                logger.info("Measuring gain spectrum again for reference.")
                self.jpac.activate_pump()
                gspect = self.tuner.measure_gain()
                gspect.mref = f"GAIN-{mref}"
                self.mio.write(gspect)

                logger.info("Measuring noise spectrum.")
                noise_spectrum = self.measure_spectrum(tup.peak_frequency)
                noise_spectrum.mref = mref
                self.mio.write(noise_spectrum)

                if self.measurebase:
                    logger.info("Measuring base spectrum.")
                    basespect = self._measure_base_spectrum(tup.peak_frequency)
                    basespect.mref = f"BASE-{mref}"
                    self.mio.write(basespect)

        return True

    def measure_spectrum(
            self, fpeak: float,
            sapreset: t.Optional[SASettings] = None) -> MeasurementModel:
        if sapreset is None:
            sapreset = self.sa_preset
        c = self.jpac
        # Important to turn off vna power
        c.deactivate_signal()
        c.do_sa_preset(sapreset.evolve(
            center_frequency=fpeak))
        spectrum = c.measure_spectrum()
        c.activate_signal()
        return spectrum

    def finish(self):
        logger.info("Finishing measurement.")
        c = self.jpac
        logger.info("Setting noise source temperature to initial temperature.")
        c.set_noise_source_temperature(
                self.tns_setpoints[0])
        if self.rollback:
            logger.info(f"Rolling back to initial WP: {self._wp}")
            self.tuner.set_working_point(self._wp)

    @classmethod
    def make(
            cls,
            path: t.Union[Path, str],
            controller: JPAController,
            tuner: JPATuner,
            wp_setpoints: t.List[WorkingPoint],
            tns_setpoints: t.List[float],
            sa_preset: SASettings,
            sa_preset_base: SASettings,
            baseline_current,
            rollback: bool,
            notune: bool,
            measurebase: bool,
            temperature_settle_delay: float = 1*60,
            temperature_settle_timeout: float = 30*60,
            ):
        vnasetbl = tuner.baseline_settings
        vnasetres = tuner.res_settings
        mio = MIOFileCSV.open(path, mode='w')
        return cls(
                controller, tuner, mio,
                wp_setpoints, tns_setpoints,
                temperature_settle_delay=temperature_settle_delay,
                temperature_settle_timeout=temperature_settle_timeout,
                sa_preset=sa_preset,
                sa_preset_base=sa_preset_base,
                baseline_current=baseline_current,
                rollback=rollback,
                notune=notune,
                measurebase=measurebase,
                vnaset_baseline=vnasetbl,
                vnaset_resonance=vnasetres)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def setup_parser():
    parser = argparse.ArgumentParser(
            description=(
                "Performs noise temperature measurement sweep at given "
                "frequency range.  The VNA must be centered at resonance "
                "before starting the script."))

    parser.add_argument(
            '-p',
            '--path',
            type=Path,
            help="Path to the folder to be used when saving data.")

    parser.add_argument(
            '--rollback',
            action='store_const',
            const=True, default=False,
            help="Rollback to the initial JPA parameters after measurement is "
                 "done or something fails.")

    parser.add_argument(
            '-n',
            '--notune',
            action='store_const',
            const=True, default=False,
            help="Measure the noise temperature without changing "
                 "any bias conditions.  Note that bias conditions may "
                 "be temporarily changed for auxiliary measurements, but "
                 "they will be restored to their initial values during "
                 "relevant measurements.")

    parser.add_argument(
            '-r',
            '--repeat',
            type=int,
            default=1,
            help=("Number of times to repeat measurements at set frequencies."
                  "Defaults to 1."))

    parser.add_argument(
            '--measure-base-spectrum',
            action='store_const',
            const=True, default=False,
            help="Measure the power spectrum when JPA is turned off and moved "
                 "outside the range at each iteration step.")

    return parser


parser = setup_parser()


def main(
        path: Path,
        isnotune: bool = False,
        repeated: t.Optional[int] = None,
        rollback: bool = False,
        measurebase: bool = False):
    """Must be started when the VNA is centered on the current resonance
    frequency.
    """
    __measurement_name__ = "NTScan"
    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    uris = get_uris()

    # Noise Source Temperatures (K)
    # t_min = 0.07
    # t_max = 0.21
    # t_step = 0.03

    tns_setpoints = [0.05, 0.075, 0.100, 0.125]

    # Selected working points
    _W = WorkingPoint
    wp_setpoints = [
            _W(ib=9.2e-6, fp=11.618e9, pp=-10),
            # _W(-79e-6, 11.621243e9, -10),
            # _W(-80e-6, 11.560450e9, -3),
            # _W(-82e-6, 11.484932e9, -2),
            # _W(-82.5e-6, 11.458000e9, -1),
            # _W(-83.5e-6, 11.400982e9, -1),
            ]

    if repeated is not None:
        wp_setpoints = list(
                chain(*zip(
                    *repeat(wp_setpoints, repeated))))

    # Script parameters
    peakfinder = 'argmax'   # peak finder during res. est.
    noise_source_control_channel = "02"
    temperature_settle_delay = 1*60  # seconds
    temperature_settle_timeout = 30*60  # seconds
    pump_ramp_rate = 10  # dB/s  we don't need to be so careful
    pump_step = 1  # minimum pump step when ramping up
    current_ramp_rate = 10e-6  # A/s  we don't need to be so careful
    current_step = 1e-6  # A/s  we don't need to be so careful (???)
    dfres_sign = -1  # sign of the slope of the resonance-current curve

    tuning_window = (4.9e9, 6.1e9)
    dfo = -1e3  # This is the gain measurement offset

    boot_step = 0.5  # dB
    boot_threshold = 14  # dB
    boot_ifbw = 100  # Hz

    zet_max = WorkingPoint(100e-6, tuning_window[1]*2, 25)
    zet_min = WorkingPoint(-100e-6, tuning_window[0]*2, -20)

    res_tuner_set = TunerSettings(
                kp=0.5e-12,
                ki=8,
                kd=1e-5,
                ramp_step=200e3)

    gain_tuner_set = TunerSettings(
                kp=0.01,
                ki=10,
                kd=0,
                ramp_step=1)

    # Presets for VNA
    res_preset = VNASettings(
                span=100e6, sweep_step=100e3, if_bandwidth=100, power=-20)

    baseline_current = 27.5e-6  # current to move resonance far
    baseline_preset = VNASettings(
                start_frequency=tuning_window[0],
                stop_frequency=tuning_window[1],
                if_bandwidth=100, power=10, sweep_step=1e6)

    offset_preset = VNASettings(
                span=10, sweep_step=1, power=-26, if_bandwidth=2)

    gain_preset = VNASettings(
                span=10e6, sweep_step=10e3, power=-26, if_bandwidth=5)

    sa_preset = SASettings(
                span=10e6,
                rbw=10e3,
                vbw=1,
                npoints=1000,
                naverage=50)

    sa_preset_base = sa_preset.evolve(
                rbw=100e3,
                vbw=1,
                npoints=100,
                naverage=20)

    # Initializing
    c = JPAController.make(resources=uris)

    tuner = JPATuner.make(
            controller=c,
            Df_sign=dfres_sign,
            wt=tuning_window,
            dfo=dfo,
            gain_bootstrap_step=boot_step,
            gain_bootstrap_threshold=boot_threshold,
            gain_bootstrap_ifbw=boot_ifbw,
            zet_max=zet_max,
            zet_min=zet_min,
            restun=res_tuner_set,
            gaintun=gain_tuner_set,
            res_settings=res_preset,
            baseline_settings=baseline_preset,
            offset_settings=offset_preset,
            gain_settings=gain_preset,
            peakfinder=peakfinder
            )

    mc = MainController.make(
            path=path,
            controller=c,
            tuner=tuner,
            wp_setpoints=wp_setpoints,
            tns_setpoints=tns_setpoints,
            sa_preset=sa_preset,
            sa_preset_base=sa_preset_base,
            baseline_current=baseline_current,
            rollback=rollback,
            notune=isnotune,
            measurebase=measurebase,
            temperature_settle_delay=temperature_settle_delay,
            temperature_settle_timeout=temperature_settle_timeout
            )

    c.verbose = False
    c.noise_source_control_channel = noise_source_control_channel
    c.pump_ramp_rate = pump_ramp_rate
    c.pump_step = pump_step
    c.current_ramp_rate = current_ramp_rate
    c.current_step = current_step

    try:
        logger.info(f"{__measurement_name__} STARTING")
        c.initiate()
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    except ExperimentError:
        logger.info("Experiment error. Aborting.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{path}'")
        save_script(path)
    logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")
    return True


if __name__ == "__main__":
    args = parser.parse_args()
    if args.path.exists():
        print(f"'{args.path}' exists! Aborting.")
        sys.exit(42)
    else:
        args.path.mkdir()

    main(args.path, args.notune, args.repeat, args.rollback,
            args.measure_base_spectrum)
