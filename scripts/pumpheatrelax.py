"""This script is used to evaluate the pump heating effect on resonance. By
default VNA is assumed to be centered on f_r.
"""

import typing as t
import os
import json
from shutil import copyfile
from pathlib import Path
import sys
from datetime import datetime
import time

from datargs import arg, parse, argsclass
from dataclasses import asdict
import attr
import numpy as np

import groundcontrol
from groundcontrol import __version__
from groundcontrol.analysis.calibrate import remove_baseline_sppm
from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.declarators import declarative
from groundcontrol.settings import VNASettings
from groundcontrol.logging import (
        setup_logger, create_log, INFO,
        DEBUG, set_stdout_loglevel)
from groundcontrol.measurement import (
        SParam1PModel, MeasurementModel, quantity, parameter)
import groundcontrol.units as un
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.resources import get_uris
from groundcontrol.estimation import phasemodel, resonance_from_phase2
from groundcontrol.helper import default_serializer

groundcontrol.USE_UNICODE = False
__script_version__ = "0.1"
__measurement_name__ = "PumpHeatRelaxation"

logger = setup_logger("pumpheatrelax")

isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


class ResonanceParamModel(MeasurementModel):
    frequency: float = quantity(un.hertz, "ResonanceFrequency")
    linewidth: float = quantity(un.hertz, "ResonanceLinewidth")
    frequency_err: float = quantity(un.hertz, "ResonanceFrequencyError")
    linewidth_err: float = quantity(un.hertz, "LinewidthError")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


@argsclass(description=__doc__)
class ProgramSettings:
    path: Path = arg(positional=True)
    pump_duration: float = arg(
            help="Pump signal is applied for this amount of time in seconds.")
    period: float = arg(
            help="Resonance frequency (f_r) measurement period in seconds.")
    timelimit: float = arg(
            help="Total time to measure f_r after turning off pump.")

    period_on_pump: float = arg(
            help="The period to measure f_r DURING pumping, in seconds.  The"
                 "pump is temporarily turned off for measurement.  "
                 "By default no measurement is performed during pumping.",
            default=None
            )

    pump_frequency: float = arg(
            help=("Pump frequency in Hz. If not provided, nstrument's "
                  "current setting used."),
            aliases=['-f'],
            default=None)
    pump_power: float = arg(
            help=("Pump power in dBm. If not provided, nstrument's "
                  "current setting used."),
            aliases=['-p'],
            default=None)
    fr0: float = arg(
            default=None,
            help=("Hint for the initial f_r.  VNA center frequency is used "
                  "if not provided."))
    step: float = arg(
            default=None,
            help=("Step to use during f_r measurement in Hz.  Defaults to"
                  "the current setting on VNA."))
    ifbw: float = arg(
            default=None,
            help=("IF bandwidth to use during f_r measurement.  Defaults to"
                  "the current setting on VNA."))
    fspan: float = arg(
            default=None,
            help=("Frequency span used when measuring f_r periodically."
                  " Defaults to the current setting on VNA."))
    power: float = arg(
            default=None,
            help=("Signal power during f_r measurement.  Defaults to the"
                  " current setting on VNA."))
    fr_range: float = arg(
            default=None,
            metavar=('START', 'STOP'),
            nargs=2,
            help=("If supplied, initial f_r will be searched "
                  "within the region (START, STOP)."))
    q_expected: float = arg(
            default=None,
            help=("Expected q-factor for the passive resonance. "
                  " Guessed from VNA parameters if not provided."))
    baseline_current: float = arg(
            default=None,
            help="Current to apply when measuring baseline.")
    baseline_power: float = arg(
            default=0,
            help="Signal power to apply when measuring baseline.  "
            "Default: 0 dBm")
    baseline_step: float = arg(
            default=1e6,
            help="Sweep step in Hz during baseline measurement. "
                 "Default: 1 MHz.")
    baseline_ifbw: float = arg(
            default=1e3,
            help="IF bandwidth in Hz during baseline measurement. "
                 "Default: 1 kHz.")

    def save(self, fp):
        dc = {k: default_serializer(v, False)
              for k, v in asdict(self).items()}
        if isinstance(fp, Path) or isinstance(fp, str):
            with open(fp, 'w') as fl:
                json.dump(dc, fl, indent=4)
        else:
            json.dump(dc, fp, indent=4)

    @classmethod
    def parse(cls):
        """Parses the commandline arguments and returns an object of this
        class"""
        obj = parse(cls)
        return obj


class ExperimentError(Exception):
    pass


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


@declarative
class MainController:
    rc: RackController
    mio: MIOFileCSV

    settings: ProgramSettings
    last_sppm: t.Optional[SParam1PModel] = None
    _baseline: t.Optional[SParam1PModel] = None
    _vnaset: VNASettings = None
    _bw_exp: t.Optional[float] = None
    _bootstrap_wide: bool = False
    _measurebaseline: bool = False
    _fr0: t.Optional[float] = None
    _bootstrap_maxtrial: int = 10
    _bw_guess_span_divisor = 10
    _frfit_max_fstderr: float = 0.5e-3

    def initiate(self):
        st = self.settings

        # VNA stuff
        self.rc.initiate()
        _vs = self.rc.query_vna_settings()
        ifbw = _vs.if_bandwidth if st.ifbw is None else st.ifbw
        power = _vs.power if st.power is None else st.power
        step = _vs.sweep_step if st.step is None else st.step
        span = _vs.span if st.fspan is None else st.fspan

        vnaset = VNASettings(
                if_bandwidth=ifbw,
                power=power,
                sweep_step=step,
                span=span)
        self._vnaset = vnaset

        fr_hint = st.fr0
        fr0 = _vs.center_frequency if fr_hint is None else fr_hint
        qexp = st.q_expected
        self._fr0 = fr0

        if qexp is None:
            self._bw_exp = span/self._bw_guess_span_divisor
        else:
            self._bw_exp = fr0/qexp

        blset = self.rc.baseline_settings
        blset.power = st.baseline_power
        blset.if_bandwidth = st.baseline_ifbw
        blset.sweep_step = st.baseline_step

        # PUMP stuff
        self.rc.deactivate_pump()
        if st.pump_power is not None:
            self.rc.set_pump_power(st.pump_power)
        if st.pump_frequency is not None:
            self.rc.set_pump_frequency(st.pump_frequency)

        # Other
        if ((st.baseline_current is not None) and
                (st.fr_range is not None)):
            self._measurebaseline = True

        if (st.fr_range is not None) and (st.fr0 is None):
            self._bootstrap_wide = True

    def measure_transmission(
            self,
            center: t.Optional[float] = None,
            start: t.Optional[float] = None,
            stop: t.Optional[float] = None,
            calibrated: bool = True):

        if all((start is not None, stop is not None)):
            vset = self._vnaset.evolve(
                    start_frequency=start,
                    stop_frequency=stop)
        elif center is not None:
            vset = self._vnaset.evolve(
                    center_frequency=center)
        else:
            vset = self._vnaset

        rc = self.rc
        rc.do_vna_preset(vset)

        tr = rc.measure_transmission()
        self.last_sppm = tr
        rc.do_vna_autoscale()

        if calibrated and (self._baseline is not None):
            tr = remove_baseline_sppm(tr, self._baseline)

        return tr

    def measure_resonance(
            self,
            center: float,
            wide: bool = False,
            roughest: bool = False
            ) -> t.Union[t.Optional[float], t.Tuple[t.Optional[float]]]:
        if wide:
            start, stop = self.settings.fr_range
            tr = self.measure_transmission(start=start, stop=stop)
        else:
            tr = self.measure_transmission(center=center)

        if roughest:
            return self.estimate_resonance_rough(tr)
        else:
            return self.estimate_resonance(tr)

    def measure_resonance_repeated(
            self,
            center: float,
            wide: bool = False,
            roughest: bool = False, ntrial: int = 1
            ) -> t.Optional[t.Union[float, t.Tuple[float]]]:

        for i in range(1, ntrial+1):
            results = self.measure_resonance(center, wide, roughest)

            if roughest:
                fr = results
            else:
                fr, bw, frerr, bwerr = results

            if fr is None:
                logger.info("Resonance not found ({i}/{ntrial}).")
                continue
            else:
                return results
        return None

    def estimate_resonance(
            self,
            sppm: SParam1PModel
            ) -> t.Tuple[t.Optional[float]]:
        """Estimates the resonance by performing a fit on the supplied
        SParam1PModel data phase.

        Args:
            sppm: The resonance spectrum data.

        Returns:
            fr:  Estimated resonance frequency.
            bw:  Estimated bandwidth.
            frerr:  Fitting error for fr.
            bwerr:  Fitting error for bwerr.

        """
        pars = phasemodel.guess(
                sppm.uphases, sppm.frequencies, bw=self._bw_exp)
        fit = phasemodel.fit(sppm.uphases, f=sppm.frequencies, params=pars)
        logger.debug(fit.fit_report())
        bp = fit.params
        fr = bp['fr'].value
        bw = bp['bw'].value
        frerr = bp['fr'].stderr
        bwerr = bp['bw'].stderr
        if (frerr is None) or (frerr/fr >= self._frfit_max_fstderr):
            # Resonance not found
            if frerr is not None:
                fstderr = frerr/fr
                logger.debug(f"Fractional error: {fstderr:.3f}//Limit: "
                             f"{self._frfit_max_fstderr:.3f}")
            else:
                logger.debug("Fit fail.")
            fr, bw = None, None  # Is this necessary?
        return fr, bw, frerr, bwerr

    def estimate_resonance_rough(
            self,
            sppm: SParam1PModel):
        """Estimates the resonance by taking a derivative of the phase and
        finding peaks SParam1PModel data phase.

        Args:
            sppm: The resonance spectrum data.

        Returns:
            fhint:  Estimated resonance frequency.

        """
        bw_exp = self._bw_exp
        fhint = resonance_from_phase2(sppm, bw_exp)
        return fhint

    def bootstrap_resonance(
            self) -> t.Optional[t.Tuple[t.Optional[float]]]:
        """Measures the initial resonance frequency.

        Returns:
            None if bootstrap fails at coarse search.
            (fr, bw, frerr, bwerr) else.  fr,... can be
                None if narrow search failed.
        """
        logger.info("Starting resonance bootstrap.")

        if self._bootstrap_wide:
            logger.info("Coarse resonance search in wide range.")
            fhint: t.Optional[float]
            fhint = self.measure_resonance(wide=True, roughest=True)
        else:
            fhint = self._fr0

        if fhint is None:
            return None

        logger.info(f"Zooming to resonance at {fhint/1e9:.6f} "
                    "GHz.")
        fr, bw, frerr, bwerr = self.measure_resonance(center=fhint)

        return fr, bw, frerr, bwerr

    def bootstrap_resonance_repeated(self, ntrial):
        for i in range(1, self._bootstrap_maxtrial+1):
            try:
                fr, bw, frerr, bwerr = self.bootstrap_resonance()
                if fr is None:
                    raise ValueError
            except TypeError:  # returning None causes unpack related typerror
                logger.info("Bootstrap failed at coarse search. "
                            f"Retrying ({i}/{ntrial}).")
                continue
            except ValueError:
                logger.info("Bootstrap failed at narrow search. "
                            f"Retrying ({i}/{ntrial}).")
                continue
            else:
                break

        if fr is None:
            return None
        else:
            return fr, bw, frerr, bwerr

    def sleep(self, time_s):
        logger.debug(f"Sleeping for {time_s} s.")
        time.sleep(time_s)

    def activate_pump(self):
        self.rc.activate_pump()

    def deactivate_pump(self):
        self.rc.deactivate_pump()

    def measure_baseline(self):
        bl = self.rc.measure_baseline(
                bias_current=self.settings.baseline_current,
                preservesettings=True)
        return bl

    def run(self):
        logger.info(f"Starting {__measurement_name__}.")
        mio = self.mio
        settings = self.settings
        ntrial = self._bootstrap_maxtrial
        self.rc.activate_signal()

        if self._measurebaseline:
            logger.info("Performing baseline measurement.")
            self._baseline = self.measure_baseline()
            self._baseline.mref = 'BASELINE'
            mio.write(self._baseline)
        try:
            fr, bw, frerr, bwerr = self.bootstrap_resonance_repeated(ntrial)
        except TypeError:  # returning None causes unpack related typerror
            logger.error("Bootstrap failed.  Aborting experiment.")
            raise ExperimentError("Bootstrap failed.")
        else:
            logger.info(f"Initial resonance found at {fr/1e9:.6f} GHz.")

        sppminit = self.last_sppm
        sppminit.mref = 'INIT'
        rpminit = ResonanceParamModel(fr, bw, frerr, bwerr)
        rpminit.mref = 'INIT'
        cminit = self.rc.measure_current()
        cminit.mref = 'INIT'
        mio.write(sppminit)
        mio.write(rpminit)
        mio.write(cminit)

        logger.info(f"Activating pump for {settings.pump_duration} s.")
        self.activate_pump()
        logger.info("Pump activated.")

        i = 0
        if settings.period_on_pump is None:
            self.sleep(settings.pump_duration)
        else:
            logger.info("Resonance will be measured during pumping.")
            measgen = self.periodic_resonance_gen(
                    fr, 0, settings.pump_duration)
            start = time.time()
            self.deactivate_pump()
            logger.info("Pump deactivated.")
            for i, (tr, cm, rpm) in enumerate(measgen):
                self.activate_pump()
                logger.info("Pump activated.")
                rpm.mref = 'PUMP'
                cm.mref = 'PUMPC'
                tr.mref = f'i{i}'
                mio.write(tr)
                mio.append(cm)
                mio.append(rpm)
                mdur = time.time() - start  # measurement duration
                self.sleep(settings.period_on_pump - mdur)
                start = time.time()
                self.deactivate_pump()
                logger.info("Pump deactivated.")
            # to make sure next batch has new mref index
            i += 1

        self.deactivate_pump()
        logger.info("Pump deactivated.")

        logger.info("Starting periodic resonance measurement, pump off.")
        timelimit = settings.timelimit
        period = settings.period
        measgen = self.periodic_resonance_gen(fr, period, timelimit)
        for i, (tr, cm, rpm) in enumerate(measgen, start=i):
            rpm.mref = 'PUMP'
            cm.mref = 'PUMPC'
            tr.mref = f'i{i}'
            mio.write(tr)
            mio.append(cm)
            mio.append(rpm)
        return True

    def periodic_resonance_gen(self, fr, period, timelimit):
        start = time.time()
        now = time.time()
        while (now - start < timelimit):
            minit = time.time()
            logger.info("Measuring resonance.")
            fr, bw, frerr, bwerr = self.measure_resonance(center=fr)
            tr = self.last_sppm
            logger.info("Measuring current.")
            cm = self.rc.measure_current()
            if fr is not None:
                logger.info(f"Resonance found at {fr/1e9:.6f} GHz.")
            else:
                fr, bw, frerr, bwerr = [np.nan, np.nan, np.nan, np.nan]
                logger.info("Resonance not found.")
            rpm = ResonanceParamModel(fr, bw, frerr, bwerr)
            mdur = time.time() - minit
            if period != 0:
                self.sleep(period - mdur)
            yield tr, cm, rpm

            now = time.time()

    def finish(self):
        logger.info(f"Finishing {__measurement_name__}.")
        # just to make sure
        self.deactivate_pump()
        self.rc.deactivate_signal()

    @classmethod
    def make(
            cls,
            settings: ProgramSettings,
            uris=None):
        if uris is None:
            uris = get_uris()
        rc = RackController.make(resources=uris)
        mio = MIOFileCSV.open(settings.path, mode='w')

        return cls(
                rc=rc, mio=mio, settings=settings)


# def _bootstrap_resonance(
#         self,
#         fhint: t.Optional[float] = None):
#     """Finds the current resonance frequency by first doing a coarse search
#     followed by a narrow one. """
#     settings = self.settings
#     logger.info("Starting resonance bootstrap.")
#     ntrial = self._bootstrap_maxtrial
#     rc = self.rc
#     rc.deactivate_pump()

#     if fhint is None:
#         start_freq, stop_freq = settings.bootrange
#         sweep_step = self.vnaset_baseline.sweep_step
#         logger.debug(f"Coarse resonance Start/Stop: "
#                      f"{start_freq/1e9:.6f}, {stop_freq/1e9:.6f} GHz")

#         rc.do_vna_preset(self.vnaset_resonance.evolve(
#             start_frequency=start_freq,
#             stop_frequency=stop_freq,
#             center_frequency=None, span=None, sweep_step=sweep_step))

#         for i in range(1, ntrial+1):
#             tr = rc.measure_transmission()
#             rc.do_vna_autoscale()
#             bluphases = self._bl.uphases
#             blfreqs = self._bl.frequencies
#             freqs = tr.frequencies
#             uphases = tr.uphases
#             # interpolates and removes the baseline
#             uphasecal = remove_baseline(
#                     freqs, uphases,
#                     blfreqs, bluphases, scale='log')
#             # assume only uphase is used in estimation.
#             tr.uphases = uphasecal

#             bw_exp = np.mean((start_freq, stop_freq))/self.q_expected
#             fhint = estimate_resonance(tr, bw_exp)

#             if fhint is not None:
#                 break

#             logger.info(f"Resonance was not found ({i}/{ntrial}).")
#             if i == ntrial:
#                 return None

#     # We got the hint now do narrow search
#     logger.info(f"Zooming to resonance at {fhint/1e9:.6f} "
#                 "GHz.")
#     rc.do_vna_preset(self.vnaset_resonance.evolve(
#         center_frequency=fhint))

#     fr, bw = None, None
#     for i in range(1, ntrial+1):
#         tr = rc.measure_transmission()
#         bluphases = self._bl.uphases
#         blfreqs = self._bl.frequencies
#         freqs = tr.frequencies
#         uphases = tr.uphases
#         # interpolates and removes the baseline
#         uphasecal = remove_baseline(
#                 freqs, uphases,
#                 blfreqs, bluphases, scale='log')
#         # assume only uphase is used in estimation.
#         rc.do_vna_autoscale()
#         pars = phasemodel.guess(tr.uphases, tr.frequencies)
#         fit = phasemodel.fit(tr.uphases, f=tr.frequencies, params=pars)
#         logger.debug(fit.fit_report())
#         bp = fit.params
#         frerr = bp['fr'].stderr
#         if (frerr is None) or (
#                 frerr/bp['fr'].value >= self._frfit_max_fstderr):
#             # Resonance not found
#             if frerr is not None:
#                 fstderr = bp['fr'].stderr/bp['fr'].value
#                 logger.debug(f"Fractional error: {fstderr:.3f}//Limit: "
#                              f"{self._frfit_max_fstderr:.3f}")
#             else:
#                 logger.debug(f"Fit fail.")
#             fr = None
#         else:
#             # Resonance found
#             fr, bw = bp['fr'].value, bp['bw'].value

#         if fr is None:
#             logger.info(f"Resonance was not found ({i}/{ntrial}).")
#         else:
#             logger.info(f"Resonance found at {fr/1e9:.6f} "
#                         "GHz.")
#             if mref is not None:
#                 tr.mref = mref
#                 self.mio.write(tr)
#             break
#     self.tuner.Fr = fr
#     if fr is None:
#         msg = "Bootstrapping failed."
#         logger.error(msg)
#         # this is not nice but anyways.
#         raise ExperimentError(msg)
#     return fr, bw


def main():
    uris = get_uris()

    settings = ProgramSettings.parse()
    path = settings.path

    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"groundcontrol version: {__version__}")
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    mc = MainController.make(
        uris=uris,
        settings=settings)

    mc.initiate()
    try:
        settings.save(path/'settings.json')
        mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    except Exception as e:
        logger.info("Unknown exception. Aborting.")
        logger.error(e)
        # LOG THIS TOO
        raise e
    finally:
        mc.finish()
        logger.info(f"Saving script to '{settings.path}'")
        save_script(settings.path)


if __name__ == "__main__":
    main()
