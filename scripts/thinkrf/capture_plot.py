import sys
import typing as t
import time
from enum import Enum

import attr
from datargs import arg, argsclass, parse
import matplotlib.pyplot as plt
from pyrf.devices.thinkrf import WSA
from pyrf.util import capture_spectrum
import numpy as np
from groundcontrol.measurement import SASpectrumModel
from groundcontrol.analysis.plotting import plot2d


TRIGGER_SETTING = {'type': 'NONE', 'amplitude': -130}
ATTENUATOR = 20


@argsclass
class Opts:
    fcenter: float = arg(default=100e6)
    decimation: int = arg(default=1)
    fshift: float = arg(default=0)
    rbw: float = arg(default=100)
    navg: int = arg(default=1)
    useref: bool = arg(default=True)
    rfe: str = arg(
            default='ZIF',
            choices=[
                'ZIF', 'SH', 'SHN', 'HDR', 'DD'])
    ip: str = arg(
            default='192.168.0.15')
    mode: str = arg(choices=['CAPSPEC', 'TDCAPFFT'], default='CAPSPEC')
    show: bool = arg(default=False, aliases=['-s'])


class ProgMode(Enum):
    CAPSPEC = 'CAPSPEC'
    TDCAPFFT = 'TDCAPFFT'


@attr.s(auto_attribs=True)
class ProgramSettings:
    rfe: str
    fcenter: float
    decimation: int
    fshift: float
    rbw: float
    useref: bool
    navg: int
    progmode: ProgMode
    show: bool

    @classmethod
    def from_opts(cls, opts: Opts):
        return cls(
                opts.rfe,
                opts.fcenter,
                opts.decimation,
                opts.fshift,
                opts.rbw,
                opts.useref,
                opts.navg,
                ProgMode(opts.mode),
                opts.show)


@attr.s(auto_attribs=True)
class Program:
    """Simple program to do spectrum measurement using ThinkRF 5550."""
    settings: ProgramSettings
    _ip: str
    dut: WSA = attr.Factory(WSA)

    def initiate(self):
        dut = self.dut
        dut.connect(self._ip)
        dut.reset()
        dut.rfe_mode(self.settings.rfe)
        dut.freq(self.settings.fcenter)
        dut.attenuator(ATTENUATOR)
        dut.trigger(TRIGGER_SETTING)
        if self.settings.useref:
            dut.pll_reference('EXT')
        else:
            dut.pll_reference('INT')

    def capture_spectrum(self) -> SASpectrumModel:
        self.dut.request_read_perm()
        _start = time.time()
        fstart, fstop, pow_data = capture_spectrum(
                self.dut,
                average=self.settings.navg,
                rbw=self.settings.rbw,
                dec=self.settings.decimation,
                fshift=self.settings.fshift,
                apply_window=False,
                average_mode='pow',
                correct_phase=False,
                iq_correction_wideband=False,
                hide_dc_offset=False
                )
        _end = time.time()
        print(f"Capture spectrum took {_end - _start:.3f} s to return.")

        power = np.array(pow_data)
        freqs = np.linspace(fstart, fstop, len(pow_data))
        spectrum = SASpectrumModel(
                frequencies=freqs,
                values=power,
                rbw=self.settings.rbw,
                vbw=self.settings.rbw,
                navg=self.settings.navg,
                detector='RMS',
                filter_type='Rectangular')
        return spectrum

    def capture_samples(self) -> np.ndarray:
        pass

    def run(self):
        spectrum = self.capture_spectrum()
        plot2d(spectrum)
        if self.settings.show:
            plt.show()

    def finish(self):
        self.dut.disconnect()
        self._finished = True

    def __del__(self):
        try:
            self._finished
        except AttributeError:
            self.finish()


def main(args):
    settings = ProgramSettings.from_opts(args)
    prog = Program(ip=args.ip, settings=settings)
    prog.initiate()
    prog.run()
    return prog


if __name__ == "__main__":
    args = parse(Opts)
    # try:
    prog = main(args)
    dut = prog.dut
    # finally:
    #     prog.finish()
