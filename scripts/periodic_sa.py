from pathlib import Path
import sys
import os
from shutil import copyfile

from datargs import arg, argsclass, parse
import json
import attr
import cattr

from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.settings import SASettings
from groundcontrol.logging import create_log, setup_logger,\
        set_stdout_loglevel, DEBUG, INFO
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.declarators import declarative
from groundcontrol.resources import get_uris
from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.exceptions import ExperimentError
from groundcontrol.helper import default_serializer


def to_json(obj, path):
    with open(path, 'w') as fl:
        json.dump(
                cattr.unstructure(obj), fl, indent=4,
                default=default_serializer)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


__script_version__ = "0.1"
__measurement_name__ = "persa"


logger = setup_logger(__measurement_name__)
logger.info(f"groundcontrol version: {__version__}")

isdebug = True
try:
    _ = os.environ['DEBUG']
    # if debug
    set_stdout_loglevel(DEBUG)
except KeyError:
    isdebug = False


@attr.s(auto_attribs=True)
@argsclass
class ProgOpts:
    out: Path = arg(
            aliases=['-o'],
            default=Path('./persa.out'))
    period: float = arg(default=0)
    rbw: float = arg(default=None)
    vbw: float = arg(default=None)
    navg: int = arg(default=None)
    det: str = arg(
            default=None,
            choices=list(map(lambda c: c.value, SASettings.DetectorType)))
    start: float = arg(
            default=None,
            help="Start frequency in Hz.")
    stop: float = arg(
            default=None,
            help="Stop frequency in Hz.")
    center: float = arg(
            default=None,
            help="Center frequency in Hz.")
    span: float = arg(
            default=None,
            help="Span in Hz.")
    npoints: int = arg(
            default=None,
            help="Number of spectrum bins.")


@declarative
class MainController:
    rc: RackController

    mio: MIOFileCSV
    sa_preset: SASettings
    period: float

    def initiate(self):
        self.rc.sa_preset = self.sa_preset
        self.rc.initiate()

    def report_settings(self):
        saset = self.rc.query_sa_settings()
        period = self.period
        logger.info(
                "Active Settings:\n"
                f"{saset!s})\n"
                f"Period: {period}")

    def run(self):
        rc = self.rc
        self.report_settings()

        while True:
            rc.trigger_spectrum_analyzer()
            spectrum = rc.read_spectrum()
            logger.debug("Writing power spectrum.")
            self.mio.write(spectrum)

            if self.period != 0:
                logger.info(f"Sleeping for {self.period:.3f} seconds.")
                self._sleep(self.period)

    def finish(self):
        pass

    @classmethod
    def make(cls, controller, mio, sapreset, period):
        # this should be done by mio creator actually.
        return cls(controller, mio, sapreset, period)

    @classmethod
    def from_progopts(cls, progopts: ProgOpts):
        uris = dict(sa=get_uris().pop('sa'))
        rc = RackController.make(
                resources=uris, allow_not_found_instrument=True)
        path = progopts.out

        mio = MIOFileCSV.open(path, mode='w')
        sapreset = SASettings(
                rbw=progopts.rbw,
                vbw=progopts.vbw,
                start_frequency=progopts.start,
                stop_frequency=progopts.stop,
                center_frequency=progopts.center,
                span=progopts.span,
                detector=progopts.det,
                naverage=progopts.navg,
                npoints=progopts.npoints)
        period = progopts.period

        return cls.make(
                rc, mio, sapreset, period)


def main(args: ProgOpts):
    if args.out.exists():
        logger.error(f"'{args.out!s}' exists! Aborting.")
        sys.exit(42)
    else:
        args.out.mkdir()

    create_log(args.out / "groundcontrol.log", INFO)
    logger.info(f"Script: {__file__} | Version: {__script_version__}")
    to_json(args, args.out / 'settings.json')

    mc = MainController.from_progopts(args)
    mc.rc.verbose = False

    try:
        logger.info(f"Starting measurement: '{__measurement_name__}'")
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    except ExperimentError:
        logger.info("Experiment error. Aborting.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{args.out}'")
        save_script(args.out)
    logger.info(f"Measurement '{__measurement_name__}' finished.")
    return True


if __name__ == "__main__":
    main(parse(ProgOpts))
