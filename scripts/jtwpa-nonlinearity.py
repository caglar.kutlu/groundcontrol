"""A script to measure the nonlinear characteristic of the JTWPA.

A CW signal is applied at a given frequency (f_stim) and power spectrum is
measured at the 2nd and 3rd multiples of this frequency.  This procedure is
repeated for varying bias currents and the individual spectra are recorded
along with the peak values.
"""
import typing as t
from argparse import ArgumentParser
from pathlib import Path
import sys
from shutil import copyfile
from datetime import datetime

import numpy as np
import attr

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol.declarators import declarative, quantity, parameter,\
    quantities, parameters
from groundcontrol.settings import SASettings
from groundcontrol.measurement import MeasurementModel, SASpectrumModel
from groundcontrol.measurementio import MIOFileCSV
import groundcontrol.units as un
from groundcontrol.logging import setup_logger, create_log, INFO
from groundcontrol.friendly import mprefix_str
from groundcontrol.resources import get_uris


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


__script_version__ = "0.1"


logger = setup_logger("jtwpa_nonlinearity")


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class HarmonicPower(MeasurementModel):
    p_2: float = quantity(un.dBm, "p_2")
    f_2: float = quantity(un.dBm, "f_2")
    p_3: float = quantity(un.dBm, "p_3")
    f_3: float = quantity(un.dBm, "f_2")
    i_b: float = quantity(un.ampere, "i_b")
    timestamp: datetime = quantity(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))
    p_s: float = parameter(un.dBm, "SignalPower")
    f_s: float = parameter(un.hertz, "SignalFrequency")
    rbw: float = parameter(un.hertz, "ResolutionBandwidth")


class HarmonicPowerSet(MeasurementModel):
    p_2: Array[float] = quantity(un.dBm, "p_2")
    f_2: Array[float] = quantity(un.dBm, "f_2")
    p_3: Array[float] = quantity(un.dBm, "p_3")
    f_3: Array[float] = quantity(un.dBm, "f_3")
    i_b: Array[float] = quantity(un.ampere, "i_b")
    timestamp: datetime = quantity(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))
    p_s: np.ndarray = parameter(un.dBm, "SignalPower")
    f_s: float = parameter(un.hertz, "SignalFrequency")
    rbw: np.ndarray = parameter(un.hertz, "ResolutionBandwidth")


def combine_measurements(
        measurements: t.List[MeasurementModel]) -> HarmonicPowerSet:
    """Will fail horribly is measurements contain different model types."""
    for i, m in enumerate(measurements):
        qs = quantities(m)
        ps = parameters(m)
        if i == 0:
            out_tmp = dict(
                    **{q.name: [q.value] for name, q in qs.items()},
                    **{p.name: p.value for name, p in ps.items()})
        else:
            for name, q in qs.items():
                out_tmp[name].append(q.value)

    out = HarmonicPowerSet(**{k: np.array(v) for k, v in out_tmp.items()})

    return out


@declarative
class MainController:
    jpac: JPAController
    mio: MIOFileCSV

    f_s: float
    p_s: float
    i_b_setpoints: np.ndarray

    sa_preset: SASettings = SASettings(
                span=1e6,
                rbw=1e3,
                vbw=1,
                npoints=1000,
                naverage=1)

    save_all: bool = False

    measured: t.List[HarmonicPower] = attr.Factory(list)

    def initiate(self):
        self.jpac.initiate()
        sg = self.jpac.sg1
        sg.set_frequency(self.f_s)
        sg.set_power(self.p_s)
        sg.set_output_state(sg.State.ON)
        self.jpac.verbose = False
        self.jpac.current_ramp_rate = 10e-6

    def measure_spectrum_at(self, f: float):
        """Centers the SA to the given frequency and measures the spectrum."""
        self.jpac.do_sa_preset(self.sa_preset.evolve(center_frequency=f))
        spectrum = self.jpac.measure_spectrum()
        return spectrum

    def estimate_peak(self, spectrum: SASpectrumModel):
        indmax = np.argmax(spectrum.values)
        return spectrum.frequencies[indmax], spectrum.values[indmax]

    def measure_2nd_harmonic(self):
        spectrum = self.measure_spectrum_at(2*self.f_s)
        f, p = self.estimate_peak(spectrum)
        return spectrum, f, p

    def measure_3rd_harmonic(self):
        spectrum = self.measure_spectrum_at(3*self.f_s)
        f, p = self.estimate_peak(spectrum)
        return spectrum, f, p

    def measure_harmonics(self) -> HarmonicPower:
        sp2, f2, p2 = self.measure_2nd_harmonic()
        sp3, f3, p3 = self.measure_3rd_harmonic()
        cm = self.jpac.measure_current()
        rbw = self.jpac.sa.query_sense_bandwidth_resolution()
        return sp2, sp3, cm, HarmonicPower(
                p2, f2, p3, f3, cm.value, cm.timestamp, self.p_s, self.f_s, rbw)

    def set_current(self, ib: float):
        self.jpac.ramp_current(ib)

    def run(self):
        for i, ib in enumerate(self.i_b_setpoints):
            self.set_current(ib)
            sp2, sp3, cm, hrm = self.measure_harmonics()
            sp2.mref = f'P2-i{i:d}'
            sp3.mref = f'P3-i{i:d}'
            cm.mref = f'I_B-i{i:d}'
            self.measured.append(hrm)
            logger.info(hrm)

            if self.save_all:
                self.mio.write(sp2)
                self.mio.write(sp3)
                self.mio.write(cm)

    def finish(self):
        logger.info("Writing the compiled data table.")
        combined = combine_measurements(self.measured)
        self.mio.write(combined)


@declarative
class MainProgram:
    name: str = "JTWPA Harmonic Current Sweep"
    description: str = (
            "Perform harmonic analysis while sweeping bias current.")
    parser: ArgumentParser = attr.ib()

    ib_min: float = -200e-6
    ib_max: float = 200e-6
    f_min: float = 10e6
    f_max: float = 5e9

    _mc: MainController = None
    _path: Path = None

    _abortnoop: bool = False

    @parser.default
    def _setup_parser(self):
        parser = ArgumentParser(description=self.description)

        parser.add_argument(
            'path',
            type=Path,
            help="Path to the folder to be used when saving data.")

        parser.add_argument(
               '-b', '--biasrange', nargs=3, type=float,
               default=(0e-6, 10e-6, 1e-6),
               metavar=('START', 'STOP', 'STEP'),
               help=(
                   "Define the current sweep domain in Ampere."
               ))

        parser.add_argument(
            '-s',
            '--save-all',
            action='store_const',
            const=True, default=False,
            help="Whether or not to save detailed measurements (spectra etc.)."
                 )

        parser.add_argument(
            '-f',
            '--signal-frequency',
            type=float,
            help="Signal frequency in Hz.")

        parser.add_argument(
            '-p',
            '--signal-power',
            type=float,
            help="Signal power in dBm.")

        parser.add_argument(
            '--rbw',
            type=float,
            default=1e3,
            help="Resolution bandwidth setting for spectrum analyzer.")

        parser.add_argument(
            '--vbw',
            type=float,
            default=None,
            help=("Video bandwidth setting for spectrum analyzer."
                  "If not given, set to be equal to rbw."))

        parser.add_argument(
            '--span',
            type=float,
            default=1e6,
            help="Span setting for spectrum analyzer.")

        parser.add_argument(
            '--naverage',
            type=int,
            default=1,
            help="Averaging for spectrum analyzer.")

        return parser

    def setup(self):
        try:
            args = self.parser.parse_args()
        except SystemExit:
            self.abort_noop()

        ibs = np.arange(*args.biasrange)
        f = args.signal_frequency
        ps = args.signal_power
        path: Path = args.path
        span = args.span
        rbw = args.rbw
        vbw = args.vbw
        save_all = args.save_all
        naverage = args.naverage

        if np.any(ibs > self.ib_max) or np.any(ibs < self.ib_min):
            logger.error(f"Min. bias: {mprefix_str(self.ib_min, un.ampere)}\n"
                         f"Max. bias: {mprefix_str(self.ib_max, un.ampere)}")
            self.abort_noop()

        if np.any(f > self.f_max) or np.any(f < self.f_min):
            logger.error(f"Min. freq.: {mprefix_str(self.f_min, un.hertz)}\n"
                         f"Max. freq.: {mprefix_str(self.f_max, un.hertz)}")
            self.abort_noop()

        #if path.exists:
        #    logger.error("Given path is not empty.")
        #    self.abort_noop()
        path.mkdir()
        self._path = path
        create_log(path / "groundcontrol.log", INFO)
        logger.info(f"groundcontrol version: {__version__}")

        rackcontroller = JPAController.make(
                resources=get_uris(), allow_not_found_instrument=True)

        mio = MIOFileCSV.open(path, mode='w')

        if vbw is None:
            vbw = rbw

        sapreset = SASettings(
                span=span, rbw=rbw, vbw=vbw,
                npoints=int(span/rbw), naverage=naverage)

        mc = MainController(
                rackcontroller, mio,
                i_b_setpoints=ibs, f_s=f, p_s=ps,
                sa_preset=sapreset,
                save_all=save_all)
        self._mc = mc

        mc.initiate()

    def main(self):
        try:
            logger.info(f"{self.name} STARTING")
            self.setup()
            self._mc.run()
        except CommunicationError as e:
            logger.error(e)
        except KeyboardInterrupt:
            logger.info("User abort.")
        finally:
            if not self._abortnoop:
                self._mc.finish()
                logger.info(f"Saving script to '{self._path}'")
                save_script(self._path)
                logger.info(f"{self.name} FINISHED")

    def abort_noop(self, code=42):
        self._abortnoop = True
        sys.exit(code)


if __name__ == "__main__":
    mp = MainProgram()
    mp.main()
