"""Scanning parameters for amplification."""
import argparse
import typing as t
import os
import sys
from pathlib import Path
from shutil import copyfile
import json

import attr
import numpy as np

from groundcontrol import __version__
from groundcontrol.settings import VNASettings, SASettings, CSSettings
from groundcontrol.logging import setup_logger, create_log, INFO, \
        set_stdout_loglevel, DEBUG
from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.declarators import setting, declarative
import groundcontrol.units as un
from groundcontrol.measurement import SParam1PModel, quantity, parameter
from groundcontrol.measurement import MeasurementModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.friendly import mprefix_str
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.exceptions import ExperimentError
from groundcontrol.resources import get_uris


def to_json(obj, path):
    with open(path, 'w') as fl:
        json.dump(attr.asdict(obj), fl, indent=4)


__script_version__ = "0.1"
__measurement_name__ = "jtwpa-ampscan"

logger = setup_logger(__measurement_name__)

isdebug = True
try:
    _ = os.environ['DEBUG']
    # if debug
    set_stdout_loglevel(DEBUG)
except KeyError:
    isdebug = False


@declarative
class MeasurementSettings:
    fp_start: float = setting()
    fp_stop: float = setting()
    fp_step: float = setting()
    pp_start: float = setting()
    pp_stop: float = setting()
    pp_step: float = setting()
    ib_start: float = setting()
    ib_stop: float = setting()
    ib_step: float = setting()
    vna_start: float = setting()
    vna_stop: float = setting()
    vna_step: float = setting()
    vna_ifbw: float = setting()
    vna_power: float = setting()
    vna_bl_ifbw: float = setting()
    vna_bl_power: float = setting()
    _CURRENTLIMIT: float = 1e-3

    @vna_start.validator
    @vna_stop.validator
    @vna_ifbw.validator
    @fp_start.validator
    @fp_stop.validator
    def _positive_definite(self, attrib, value):
        if value < 0:
            msg = f"{attrib.name} is positive definite."
            raise ValueError(msg)

    @ib_start.validator
    @ib_stop.validator
    def _current_lim(self, attrib, value):
        limit = self._CURRENTLIMIT
        unit = "A"
        if abs(value) > self._CURRENTLIMIT:
            limstr = mprefix_str(limit, unit)
            msg = f"{attrib.name} must be less than {limstr}"
            raise ValueError(msg)

    @property
    def pump_frequencies(self):
        return np.arange(self.fp_start, self.fp_stop, self.fp_step)

    @property
    def pump_powers(self):
        return np.arange(self.pp_start, self.pp_stop, self.pp_step)

    @property
    def bias_currents(self):
        return np.arange(self.ib_start, self.ib_stop, self.ib_step)

    @classmethod
    def from_cmdline_args(cls, parsed=None):
        pd = parsed
        if pd is None:
            parser = cls.make_parser()
            pd = parser.parse_args()

        args = []
        args.extend([
            pd.fprange[0],
            pd.fprange[1],
            pd.fprange[2],
            pd.pprange[0],
            pd.pprange[1],
            pd.pprange[2],
            pd.ibrange[0],
            pd.ibrange[1],
            pd.ibrange[2],
            pd.vnarange[0],
            pd.vnarange[1],
            pd.vnarange[2],
            pd.vna_ifbw,
            pd.vna_power,
            pd.vna_ifbw if pd.vna_bl_ifbw is None else pd.vna_bl_ifbw,
            pd.vna_power if pd.vna_bl_power is None else pd.vna_bl_power,
            ])

        return cls(*args)

    @staticmethod
    def make_parser(parser: argparse.ArgumentParser = None):
        if parser is None:
            parser = argparse.ArgumentParser()

        parser.add_argument(
                '-f',
                '--pump-frequency-range',
                dest='fprange',
                type=float,
                nargs=3,
                metavar=('START', 'STOP', 'STEP'),
                help="Start, stop and stepping frequencies for pump in Hz.",
                required=True
                )

        parser.add_argument(
                '-p',
                '--pump-power-range',
                dest='pprange',
                type=float,
                nargs=3,
                metavar=('START', 'STOP', 'STEP'),
                help="Start, stop and stepping powers for pump in dBm.",
                required=True
                )

        parser.add_argument(
                '-b',
                '--bias-current-range',
                dest='ibrange',
                type=float,
                nargs=3,
                metavar=('START', 'STOP', 'STEP'),
                help="Start, stop and stepping currents for biasing in A.",
                required=True
                )

        parser.add_argument(
                '-r',
                '--vna-range',
                dest='vnarange',
                type=float,
                nargs=3,
                metavar=('START', 'STOP', 'STEP'),
                help="Start, stop and stepping frequencies for VNA.",
                required=True
                )

        parser.add_argument(
                '--vna-ifbw',
                default=10e3,
                type=float,
                help="IFBW for VNA in Hz.  Defaults to 10 kHz.",
                )

        parser.add_argument(
                '--vna-power',
                default=-20,
                type=float,
                help="Signal power for VNA in dBm.  Defaults to -20 dBm.",
                )

        parser.add_argument(
                '--vna-bl-ifbw',
                type=float,
                help="Baseline measurement IFBW for VNA in Hz. "
                     "Defaults to measurement IFBW.",
                )

        parser.add_argument(
                '--vna-bl-power',
                type=float,
                help="Baseline measurement signal power for VNA in dBm. "
                     "Defaults to -20 dBm.",
                )

        return parser


@declarative
class ProgramSettings:
    path: Path = setting()
    isdry: bool = setting()

    @classmethod
    def from_cmdline_args(cls, parsed=None):
        if parsed is None:
            parser = cls.make_parser()
            parsed = parser.parse_args()

        args = []
        args.extend([
            parsed.path,
            parsed.isdry
            ])

        return cls(*args)

    @staticmethod
    def make_parser(parser: argparse.ArgumentParser = None):
        if parser is None:
            parser = argparse.ArgumentParser()

        parser.add_argument(
                'path',
                type=Path,
                help="Output path."
                )
        parser.add_argument(
                '--dry',
                action='store_true',
                help="Print measurement info and exit.",
                dest='isdry'
                )
        return parser


class WorkingPoint(MeasurementModel):
    fp: float = quantity(un.hertz, 'Pump Frequency')
    pp: float = quantity(un.dBm, 'Pump Power')
    ib: float = quantity(un.ampere, 'Bias Current')
    fpix: int = quantity(un.nounit, 'Pump Frequency Index')
    ppix: int = quantity(un.nounit, 'Pump Power Index')
    ibix: int = quantity(un.nounit, 'Current Bias Index')


@declarative
class MeasurementController:
    controller: RackController
    mio: MIOFileCSV

    # Settings
    settings: MeasurementSettings
    vnapreset: VNASettings = None

    _mind_fmt: str = "i{fpix:d}j{ppix:d}k{ibix:d}"
    _mind_bl_fmt: str = "BASELINE-k{ibix:d}"
    _saved_bl_ibix: t.List = attr.Factory(list)
    _max_comm_trial: int = 50
    _comm_trial: int = 0

    def initiate(self):
        c = self.controller
        stngs = self.settings
        cs_range_i = np.abs([stngs.ib_start, stngs.ib_stop]).max()

        # This may not have effect if c is already initiated
        c.cs_preset.range_i = cs_range_i

        if not c.initiated:
            c.clean_vna = True
            c.initiate()

        self._initial_vnaset = c.query_vna_settings()

        # Do settings
        vnapreset = VNASettings(
                start_frequency=stngs.vna_start,
                stop_frequency=stngs.vna_stop,
                sweep_step=stngs.vna_step,
                if_bandwidth=stngs.vna_ifbw,
                power=stngs.vna_power)
        self.vnapreset = vnapreset

        # Setup Instruments
        c.do_vna_preset(vnapreset)
        c.activate_signal()

    def apply_baseline_settings(self):
        c = self.controller
        c.do_vna_preset(self.vnapreset.evolve(
                if_bandwidth=self.settings.vna_bl_ifbw,
                power=self.settings.vna_bl_power))

    def apply_amplification_settings(self):
        c = self.controller
        c.do_vna_preset(self.vnapreset.evolve(
                if_bandwidth=self.settings.vna_ifbw,
                power=self.settings.vna_power))

    def measure_transmission(self):
        c = self.controller
        logger.info("Measuring transmission.")
        
        success = False
        for trial in range(self._max_comm_trial + 1):
            try:
                tr = c.measure_transmission()
                success = True
                break
            except CommunicationError as e:
                logger.info("Communication problem"
                        f"({trial:d}/{self._max_comm_trial:d}).")
                continue
        
        if not success:
            raise CommunicationError("Problem communicating with VNA.")
                
        c.do_vna_autoscale()
        return tr

    def calculate_vna_time(self):
        c = self.controller
        swtime = c.vna.query_sense_sweep_time()
        return swtime

    def make_wp_iterator(self):
        """Returns an iterator for working points"""
        fps = self.settings.pump_frequencies
        pps = self.settings.pump_powers
        ibs = self.settings.bias_currents

        for i, fp in enumerate(fps):
            for j, pp in enumerate(pps):
                for k, ib in enumerate(ibs):
                    yield WorkingPoint(
                            fp, pp, ib, i, j, k)

    def measure_baseline(self):
        sets = self.settings
        c = self.controller
        vnaset = self.vnapreset

        c.do_vna_preset(
                vnaset.evolve(
                    if_bandwidth=sets.vna_bl_ifbw,
                    power=sets.vna_bl_power))

        tr = self.measure_transmission()
        # restore settings
        c.do_vna_preset(vnaset)
        return tr

    def set_wp(self, wp: WorkingPoint):
        c = self.controller
        c.set_pump_frequency(wp.fp)
        c.set_pump_power(wp.pp)
        c.ramp_current(wp.ib)

    def run(self, wps=None):
        c = self.controller
        logger.info(f"Starting measurement '{__measurement_name__}'.")
        if wps is None:
            wps = self.make_wp_iterator()

        logger.info("Activating pump output.")
        c.activate_pump()

        for wp in wps:
            mref = self._mind_fmt.format(
                    fpix=wp.fpix,
                    ppix=wp.ppix,
                    ibix=wp.ibix)

            self.set_wp(wp)
            logger.info(wp.pretty())

            if wp.ibix not in self._saved_bl_ibix:
                # logger.info("Deactivating pump for baseline measurement.")
                txt = f"Measuring baseline for {mprefix_str(wp.ib, un.ampere)}"
                logger.info(txt)

                c.deactivate_pump()

                bl = self.measure_baseline()
                self._saved_bl_ibix.append(wp.ibix)
                bl.mref = self._mind_bl_fmt.format(ibix=wp.ibix)
                self.mio.write(bl)
                # logger.info("Reactivating pump for baseline measurement.")
                c.activate_pump()

            tr = self.measure_transmission()
            tr.mref = mref
            self.mio.append(wp)
            self.mio.write(tr)

        return True

    def finish(self):
        c = self.controller
        c.do_vna_preset(self._initial_vnaset)
        logger.info(f"Measurement '{__measurement_name__}' finished.")


@declarative
class MainProgram:
    meascontrol: MeasurementController
    progsettings: ProgramSettings

    @staticmethod
    def prepare_path(path: Path):
        if path.exists():
            logger.error(f"'{path!s}' exists! Aborting.")
            sys.exit(42)
        else:
            path.mkdir()

    def do_dry(self):
        logger.info(self.program_info())

    def program_info(self):
        mc = self.meascontrol
        mc.initiate()
        mc.apply_baseline_settings()
        dt_bl = mc.calculate_vna_time()
        mc.apply_amplification_settings()
        dt = mc.calculate_vna_time()
        ibs = mc.settings.bias_currents
        fps = mc.settings.pump_frequencies
        pps = mc.settings.pump_powers

        lines = ["Program Info\n========================="]
        cnt = np.prod(list(map(len, (ibs, fps, pps))))
        ttot = cnt*dt
        # extra baseline measurements done at each ib setting
        ttot_bl = dt_bl*len(ibs)
        for name, arr, unit in zip(
                (
                    "Pump frequency",
                    "Pump power",
                    "Bias current"),
                (fps, pps, ibs),
                (un.hertz, un.dBm, un.ampere)):
            itfst = mprefix_str(arr[0], unit)
            itlst = mprefix_str(arr[-1], unit)
            nitems = len(arr)
            txt = f"{name} sweep from {itfst} to {itlst} with {nitems} points."
            lines.append(txt)
        dttxt = ("Expected time for single measurement: "
                 f"{mprefix_str(dt, un.second)}")
        dttxt_bl = ("Expected time for single baseline measurement: "
                 f"{mprefix_str(dt_bl, un.second)}")
        ttottxt = ("Expected time for the complete sweep: "
                   f"{mprefix_str(ttot+ttot_bl, un.second)}")
        lines.append(dttxt)
        lines.append(dttxt_bl)
        lines.append(ttottxt)
        return "\n\t".join(lines)

    def do_measurement(self):
        logger.info(self.program_info())
        logger.info("Measurement starting.")
        return self.meascontrol.run()

    def _dispatch(self):
        progset = self.progsettings

        if progset.isdry:
            self.do_dry()
        else:
            self.do_measurement()

    def run(self):
        self.meascontrol.initiate()

        try:
            self._dispatch()
        except CommunicationError as e:
            logger.error(e)
        except KeyboardInterrupt:
            logger.info("User abort.")
        except ExperimentError:
            logger.info("Experiment error. Aborting.")
        finally:
            self.meascontrol.finish()
            if not self.progsettings.isdry:
                logger.info(f"Saving script to '{self.progsettings.path!s}'")
                self.save_script(Path(self.progsettings.path))

    def save_script(self, path: Path):
        """Saves this script to the given location."""
        fpath = Path(__file__)
        fname = fpath.name  # only the name of the file
        return copyfile(fpath, path / fname)

    @classmethod
    def make(cls):
        _parser = ProgramSettings.make_parser()
        parser = MeasurementSettings.make_parser(_parser)

        parsed = parser.parse_args()
        progset = ProgramSettings.from_cmdline_args(parsed)
        measset = MeasurementSettings.from_cmdline_args(parsed)

        # hackish way to do this
        if not progset.isdry:
            cls.prepare_path(progset.path)
            create_log(progset.path / "groundcontrol.log", INFO)
            logger.info(f"groundcontrol version: {__version__}")
            logger.info(f"Script: {__file__} | Version: {__script_version__}")
            # quick hack
            outpath = progset.path
            progset.path = f"{outpath!s}"

            to_json(progset, outpath / 'progset.json')
            to_json(measset, outpath / 'measset.json')
        else:
            progset.path = '/tmp/gndtmp'

        uris = get_uris()
        c = RackController.make(resources=uris)
        c.verbose = False
        c.noise_source_control_channel = "02"
        c.temperature_setpoint_max = 1.2
        c.current_ramp_rate = 400e-6
        c.current_step = 1e-6

        mio = MIOFileCSV.open(progset.path, mode='w')

        mc = MeasurementController(
                 c, mio, measset)

        return cls(mc, progset)


def main():
    mp = MainProgram.make()
    mp.run()


if __name__ == "__main__":
    main()
