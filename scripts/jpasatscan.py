"""This script collects SNR comparison NT estimation

"""
import typing as t
from pathlib import Path
from datetime import datetime
import time
from shutil import copyfile
import argparse
import sys
import os
from itertools import chain, repeat

import attr
import numpy as np
import pandas as pd

from groundcontrol import __version__
from groundcontrol.analysis.calibrate import remove_baseline
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol import JPATuner
from groundcontrol.settings import VNASettings, SASettings
from groundcontrol.declarators import declarative, quantity, \
    parameter, to_json
from groundcontrol.measurement import MeasurementModel, SParam1PModel, \
        combine_measurements
from groundcontrol.measurement import SASpectrumModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.jpa_tuner import WorkingPoint
from groundcontrol.logging import setup_logger, create_log, INFO, \
        set_stdout_loglevel, DEBUG
import groundcontrol.units as un
from groundcontrol.util import find_nearest_idx, dbm2watt
from groundcontrol.resources import get_uris
from groundcontrol.estimation import resonance_from_phase2, phasemodel
from groundcontrol.helper import pairwise, Array
estimate_resonance = resonance_from_phase2


__script_version__ = "0.2"

logger = setup_logger("jpasatscan")
logger.info(f"groundcontrol version: {__version__}")
isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


class WorkingPointExt(WorkingPoint, MeasurementModel):
    """Extended working point to include detuning fr0 etc."""
    det: float = quantity(un.nounit, "Detuning", True)
    fr0: float = quantity(
            un.hertz, "Passive Resonance Frequency", True)
    bw0: float = quantity(un.hertz, "Passive Bandwidth", True)


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class ExperimentError(Exception):
    pass


class ResonanceNotFoundError(ExperimentError):
    pass


class SNRSpectrum(MeasurementModel):
    """Standard data model for Spectrum Analyzer measurements.
    """
    frequencies: Array[float] = quantity(un.hertz, 'Frequency')
    snrs: Array[float] = quantity(un.ratio_ww, 'SNR')
    noisepows: Array[float] = quantity(un.dBm, 'NoisePower')
    rbw: float = parameter(un.hertz, "RBW")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


def fp2det(fp, fr, bw):
    """Normalized detuning value."""
    gamma = bw/2
    return (fp/2 - fr)/gamma


def det2fp(det, fr, bw):
    """Return detuning given fpump and other params"""
    gamma = bw/2
    return (gamma*det + fr)*2

class SaturationSweep(MeasurementModel):
    sigpows: np.ndarray = quantity(
            un.dB, 'SignalPower',
            description='Signal power at the instrument output.')
    gains: np.ndarray = quantity(un.dB, 'Gain')
    gain_offset: float = parameter(un.hertz, 'GainOffsetFrequency')
    nbw: float = parameter(un.hertz, 'NoiseBandwidth')
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


@declarative
class MainController:
    jpac: JPAController
    tuner: JPATuner

    mio: MIOFileCSV

    wp_setpoints: t.List[WorkingPoint]

    # Measurement settings
    sigpows: np.ndarray
    vnaset_baseline: VNASettings
    vnaset_resonance: VNASettings
    gain_frequency_offset: float
    # offset to apply all current settings, except baseline current
    offset_current: float

    # Current to use when measuring baseline
    baseline_current: float

    # Control options
    rollback: bool
    notune: bool
    usedetuning: bool
    noestbw0: bool
    skipconsres: bool
    # skip consecutive resonance measurements if ib is the same

    # Some defaults
    signal_settle_delay_s: float = 0
    pump_settle_delay_s: float = 0

    bootstrap_resonance_maxtrial: int = 10
    q_expected: float = 300
    _frfit_max_fstderr: float = 0.5e-3

    # Uninitialized privates
    _wp: t.Optional[WorkingPoint] = None
    _initial_vnaset: t.Optional[VNASettings] = None
    _bl: t.Optional[SParam1PModel] = None

    def _sleep(self, time_s):
        time.sleep(time_s)

    @property
    def wp(self):
        return self._wp

    def _measure_baseline(self):
        c = self.jpac
        c.deactivate_pump()
        logger.info("Setting current for baseline measurement.")
        current = c.query_current_setpoint()
        c.ramp_current(self.baseline_current)
        bl = self.tuner.measure_baseline()
        logger.info("Ramping current back to initial value.")
        c.ramp_current(current)
        self._bl = bl
        return bl

    def bootstrap_resonance(
            self,
            fhint: t.Optional[float] = None,
            mref: t.Optional[str] = None):
        """Finds the current resonance frequency by first doing a coarse search
        followed by a narrow one.  If `assumelast` is given, last resonance
        frequency."""
        logger.info("Starting resonance bootstrap.")
        ntrial = self.bootstrap_resonance_maxtrial
        c = self.jpac
        c.deactivate_pump()

        if fhint is None:
            start_freq = self.vnaset_baseline.get_start_frequency()
            stop_freq = self.vnaset_baseline.get_stop_frequency()
            sweep_step = self.vnaset_baseline.sweep_step
            logger.debug(f"Coarse resonance Start/Stop: "
                         f"{start_freq/1e9:.6f}, {stop_freq/1e9:.6f} GHz")

            c.do_vna_preset(self.vnaset_resonance.evolve(
                start_frequency=start_freq,
                stop_frequency=stop_freq,
                center_frequency=None, span=None, sweep_step=sweep_step))

            for i in range(1, ntrial+1):
                tr = c.measure_transmission()
                c.do_vna_autoscale()
                bluphases = self._bl.uphases
                blfreqs = self._bl.frequencies
                freqs = tr.frequencies
                uphases = tr.uphases
                # interpolates and removes the baseline
                uphasecal = remove_baseline(
                        freqs, uphases,
                        blfreqs, bluphases, scale='log')
                # assume only uphase is used in estimation.
                tr.uphases = uphasecal

                bw_exp = np.mean((start_freq, stop_freq))/self.q_expected
                fhint = estimate_resonance(tr, bw_exp)

                if fhint is not None:
                    break

                logger.info(f"Resonance was not found ({i}/{ntrial}).")
                if i == ntrial:
                    return None

        # We got the hint now do narrow search
        logger.info(f"Zooming to resonance at {fhint/1e9:.6f} "
                    "GHz.")
        c.do_vna_preset(self.vnaset_resonance.evolve(
            center_frequency=fhint))

        fr, bw = None, None
        for i in range(1, ntrial+1):
            tr = c.measure_transmission()
            bluphases = self._bl.uphases
            blfreqs = self._bl.frequencies
            freqs = tr.frequencies
            uphases = tr.uphases
            # interpolates and removes the baseline
            uphasecal = remove_baseline(
                    freqs, uphases,
                    blfreqs, bluphases, scale='log')
            # assume only uphase is used in estimation.
            c.do_vna_autoscale()
            pars = phasemodel.guess(tr.uphases, tr.frequencies)
            fit = phasemodel.fit(tr.uphases, f=tr.frequencies, params=pars)
            logger.debug(fit.fit_report())
            bp = fit.params
            frerr = bp['fr'].stderr
            if (frerr is None) or (
                    frerr/bp['fr'].value >= self._frfit_max_fstderr):
                # Resonance not found
                if frerr is not None:
                    fstderr = bp['fr'].stderr/bp['fr'].value
                    logger.debug(f"Fractional error: {fstderr:.3f}//Limit: "
                                 f"{self._frfit_max_fstderr:.3f}")
                else:
                    logger.debug(f"Fit fail.")
                fr = None
            else:
                # Resonance found
                fr, bw = bp['fr'].value, bp['bw'].value

            if fr is None:
                logger.info(f"Resonance was not found ({i}/{ntrial}).")
            else:
                logger.info(f"Resonance found at {fr/1e9:.6f} "
                            "GHz.")
                if mref is not None:
                    tr.mref = mref
                    self.mio.write(tr)
                break
        self.tuner.Fr = fr
        if fr is None:
            msg = "Bootstrapping failed."
            logger.error(msg)
            # this is not nice but anyways.
            raise ExperimentError(msg)
        return fr, bw

    def initiate(self):
        self.tuner.dfo = self.gain_frequency_offset
        # We are assuming current vna is centered around resonance frequency.
        cursettings = self.jpac.read_vna_settings()
        self._initial_vnaset = cursettings

        vna = self.jpac.vna
        vna.set_calculate_smoothing(vna.State.OFF)

        if self.rollback:
            self._wp = self.tuner.query_working_point()
            logger.info(f"Rollback ON: {self._wp}")

        if self.notune:
            self._wp = self.tuner.query_working_point()
            logger.info(f"NOTUNE ON")
            self.wp_setpoints = [self._wp]

        Tb = self._measure_baseline()
        self.tuner.Tb = Tb
        self.tuner.Tb.mref = "BASELINE"
        self.mio.write(self.tuner.Tb)
        self.jpac.do_vna_preset(cursettings)

    def set_output_signal(self, frequency, powerdbm):
        vna = self.jpac.vna
        vna.set_sense_frequency_center(frequency)
        self.jpac.set_signal_power(powerdbm)
        vna.set_sense_sweep_mode(vna.SweepMode.CONTINUOUS)
        vna.set_trigger_source(vna.TriggerSource.IMMEDIATE)
        vna.set_initiate_continuous(vna.State.ON)
        # 0 span measurement
        vna.set_sense_frequency_span(0)
        self._sleep(self.signal_settle_delay_s)

    def set_working_point(self, wp: WorkingPoint):
        self.tuner.set_working_point(wp)
        # this is an oxymoron workaround for some inconsistency in
        # jpa_tuner
        self.tuner.fpeak = wp.fp/2

    def update_tuning_point(self):
        # This forces tuner to measure gain again.
        self.tuner.Go = None
        self.tuner.update_tuning_point()

    def activate_calibration(self):
        self.jpac.turn_on_vna_calibration()

    def do_power_sweep(self) -> SaturationSweep:
        """Performs a sweep of signal powers and measures offset gain.
        """
        tuner = self.tuner
        gs = tuner.offset_settings
        psig_init = gs.power
        logger.info(" PS[dBm] | GO[dB] ")
        logger.info("==================")
        gos = []
        for ps in self.sigpows:
            # Set the power to be used in measurement
            gs.power = ps
            go = tuner.measure_offset_gain()
            gos.append(go)
            logger.info(f"{ps:^ 9.2f}|{go:^ 9.2f}")

        meas = SaturationSweep(
                self.sigpows,
                np.array(gos),
                gain_offset=self.tuner.dfo)

        # recover original value
        gs.power = psig_init

        return meas

    def run(self) -> bool:
        wp_s = self.wp_setpoints

        for i, (wp_prev, wp) in enumerate(pairwise(wp_s, None)):
            # applying the offset
            wp.ib = wp.ib + self.offset_current

            # mref for outer loop
            mref = f"i{i:d}"

            logger.info(f"Setting {wp}")
            self.set_working_point(wp)

            logger.info(f"(Re)activating calibration.")
            self.activate_calibration()

            if (self.skipconsres and (wp_prev is not None) and
                    wp.ib == wp_prev.ib):
                logger.info("Skipping resonance measurement.")
                fr, bw = wp_prev.fr0, wp_prev.bw0
            else:
                logger.info("Measuring passive resonance.")
                frbw = self.bootstrap_resonance(
                        mref=f"RESONANCE-{mref}")
                if frbw is None:
                    logger.error("Resonance bootstrap failed.")
                    raise ResonanceNotFoundError
                else:
                    fr, bw = frbw
            self.tuner.Fr = fr

            wp.fr0 = fr
            if not self.noestbw0:
                wp.bw0 = bw

            if self.usedetuning:
                wp.fp = det2fp(wp.det, wp.fr0, wp.bw0)
                self.set_working_point(wp)

            logger.info("Turning pump power on.")
            self.jpac.activate_pump()
            if self.pump_settle_delay_s != 0:
                logger.info(
                        "Waiting {self.pump_settle_delay_s:.2f}s to settle.")
                self._sleep(self.pump_settle_delay_s)
            logger.info("Updating tuning point info.")
            self.update_tuning_point()
            tup = self.tuner.tup
            logger.info(tup.pretty())
            tup.mref = mref
            self.mio.write(tup)

            logger.info("Measuring gain spectrum.")
            gspect = self.tuner.measure_gain()
            gspect.mref = f"GAIN-{mref}"
            self.mio.write(gspect)

            logger.info("Doing signal power sweep.")
            satswp = self.do_power_sweep()
            satswp.mref = f"{mref}"
            self.mio.write(satswp)

        return True

    def finish(self):
        logger.info("Finishing measurement.")
        self.jpac.deactivate_pump()
        if self.rollback:
            logger.info(f"Rolling back to initial WP: {self._wp}")
            self.tuner.set_working_point(self._wp)

    @classmethod
    def make(
            cls,
            path: t.Union[Path, str],
            controller: JPAController,
            tuner: JPATuner,
            wp_setpoints: t.List[WorkingPoint],
            baseline_current,
            rollback: bool,
            notune: bool,
            signal_powers: np.ndarray,
            pump_settle_delay_s: float,
            gainfreqoffset: float,
            offset_current: float = 0,
            usedetuning: bool = False,
            dont_estimate_bw0: bool = False,
            skip_consecutive_resonance: bool = True,
            ):
        vnasetbl = tuner.baseline_settings
        vnasetres = tuner.res_settings
        mio = MIOFileCSV.open(path, mode='w')
        return cls(
                controller, tuner, mio,
                wp_setpoints,
                baseline_current=baseline_current,
                rollback=rollback,
                notune=notune,
                vnaset_baseline=vnasetbl,
                vnaset_resonance=vnasetres,
                sigpows=signal_powers,
                pump_settle_delay_s=pump_settle_delay_s,
                gain_frequency_offset=gainfreqoffset,
                offset_current=offset_current,
                usedetuning=usedetuning,
                noestbw0=dont_estimate_bw0,
                skipconsres=skip_consecutive_resonance
                )


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def save_wp_setpoints(path: Path, wps: t.List[WorkingPoint]):
    fname = "wp.csv"

    wpcomb = combine_measurements(wps)
    wpcomb.to_csv(path / fname, nicenames=False)


@declarative
class ProgramSettings:
    path: Path
    isnotune: bool
    repeated: t.Optional[int]
    rollback: bool
    wp_fpath: t.Optional[Path]
    use_detuning: bool
    use_detuning_noest_bw0: bool
    noskip_consecutive_resonance: bool
    offset_current: float
    baseline_current: float
    pump_settle: float
    version: str = __script_version__

    def save(self, fp):
        return to_json(self, fp)

    @staticmethod
    def create_parser():
        parser = argparse.ArgumentParser(
                description=(
                    "Performs SNR measurements necessary to estimate noise "
                    "temperature of a JPA given a reference noise temperature "
                    "measurement when JPA is off."))

        parser.add_argument(
                '-p',
                '--path',
                type=Path,
                help="Path to the folder to be used when saving data.")

        parser.add_argument(
                '--rollback',
                action='store_const',
                const=True, default=False,
                help="""Rollback to the initial JPA parameters after
                measurement is done or something fails.""")

        parser.add_argument(
                '-n',
                '--notune',
                action='store_const',
                const=True, default=False,
                help="Measure the noise temperature without changing "
                     "any bias conditions.  Note that bias conditions may "
                     "be temporarily changed for auxiliary measurements, but "
                     "they will be restored to their initial values during "
                     "relevant measurements.",
                dest='isnotune')

        parser.add_argument(
                '-r',
                '--repeat',
                type=int,
                default=1,
                help="""Number of times to repeat measurements at set
                frequencies.  Defaults to 1.""",
                dest='repeated')

        parser.add_argument(
                '-f',
                '--wp-fpath',
                type=Path,
                help=("Working points to measure at in the form of csv file."
                      "Columns must be ordered as: {ib, fp, pp}"))

        parser.add_argument(
                '-d',
                '--use-detuning',
                action='store_const',
                const=True, default=False,
                help="""Use the detuning(det) values instead of the absolute
                pump frequencies(fp).  The fp will be estimated from in-situ
                passive resonance frequency (fr0) via: fp = bw0*det + 2*fr By
                default bw0 is also estimated in-situ.""")

        parser.add_argument(
                '-D',
                '--use-detuning-noest-bw0',
                action='store_const',
                const=True, default=False,
                help="""Use the detuning(det) values instead of the absolute
                pump frequencies(fp).  The fp will be estimated from in-situ
                passive resonance frequency (fr0) via: fp = bw0*det + 2*fr bw0
                from the supplied working point is used.""")

        parser.add_argument(
                '--noskip-consecutive-resonance',
                action='store_const',
                const=True, default=False,
                help="""If given, the consecutive resonance measurements belonging
                to the working points with the same ib are not skipped.""")

        parser.add_argument(
                '-c', '--offset-current',
                type=float, default=0,
                help="""The working point currents will be calculated with the
                relation: ib'=ib+offset.  Defaults to 0.""")

        parser.add_argument(
                '-b', '--baseline_current', type=float,
                help="""Current to apply when measuring the baseline.""")

        parser.add_argument(
                '--pump-settle',
                type=float,
                default=0,
                help=("Time in seconds to wait for the pump to settle after "
                      "activation.  Defaults to 0 s."))

        return parser

    @classmethod
    def make(cls, **kwargs):
        return cls(**kwargs)

    @classmethod
    def from_cmdline(cls):
        parser = cls.create_parser()
        args = parser.parse_args()
        return cls.make(**vars(args))


def main(progset: ProgramSettings):
    """Must be started when the VNA is centered on the current resonance
    frequency.
    """
    wp_df = parse_wp_csv(progset.wp_fpath)
    __measurement_name__ = "JPASATSCAN"
    create_log(progset.path / "groundcontrol.log", INFO)
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    uris = get_uris()

    _W = WorkingPointExt
    wp_setpoints = []
    if wp_df is None:
        # Selected working points
        pps = [3.3]  # np.arange(10, 17.1, 0.1)
        ib = -29.9e-6
        fp = 5.8993*2e9

        for pp in pps:
            wp = _W(ib=ib, fp=fp, pp=pp)
            wp_setpoints.append(wp)
    else:
        for index, row in wp_df.iterrows():
            wp_setpoints.append(
                    _W(
                        ib=row.ib, fp=row.fp, pp=row.pp,
                        fr0=row.fr0, bw0=row.bw0, det=row.det))

    signal_powers = np.linspace(-30, 15, 46)
    ps_gain = signal_powers[0]

    if progset.repeated is not None:
        wp_setpoints = list(
                chain(*zip(
                    *repeat(wp_setpoints, progset.repeated))))

    # Script parameters
    noise_source_control_channel = "02"
    pump_ramp_rate = 10  # dB/s  we don't need to be so careful
    pump_step = 1  # minimum pump step when ramping up
    current_ramp_rate = 10e-6  # A/s  we don't need to be so careful
    current_step = 1e-6  # A/s  we don't need to be so careful (???)

    tuning_window = (1.1e9, 1.17e9)
    dfo = -1e3  # This is the gain measurement offset

    # Presets for VNA
    res_preset = VNASettings(
                span=15e6, sweep_step=100e3, if_bandwidth=100, power=-20)

    baseline_preset = VNASettings(
                start_frequency=tuning_window[0],
                stop_frequency=tuning_window[1],
                if_bandwidth=200, power=0, sweep_step=100e3)

    offset_preset = VNASettings(
                span=10, sweep_step=1, power=ps_gain, if_bandwidth=10)

    gain_preset = VNASettings(
                span=2e6, sweep_step=25e3, power=ps_gain, if_bandwidth=10)

    # Initializing
    c = JPAController.make(resources=uris)

    tuner = JPATuner.make(
            controller=c,
            wt=tuning_window,
            dfo=dfo,
            res_settings=res_preset,
            baseline_settings=baseline_preset,
            offset_settings=offset_preset,
            gain_settings=gain_preset,
            )

    mc = MainController.make(
            path=progset.path,
            controller=c,
            tuner=tuner,
            wp_setpoints=wp_setpoints,
            baseline_current=progset.baseline_current,
            rollback=progset.rollback,
            notune=progset.isnotune,
            signal_powers=signal_powers,
            pump_settle_delay_s=progset.pump_settle,
            gainfreqoffset=dfo,
            offset_current=progset.offset_current,
            usedetuning=progset.use_detuning,
            dont_estimate_bw0=False,
            skip_consecutive_resonance=~progset.noskip_consecutive_resonance,
            )

    c.verbose = False
    c.noise_source_control_channel = noise_source_control_channel
    c.pump_ramp_rate = pump_ramp_rate
    c.pump_step = pump_step
    c.current_ramp_rate = current_ramp_rate
    c.current_step = current_step

    try:
        logger.info(f"{__measurement_name__} STARTING")
        # The wp set will be modified by controller, we need to save now
        # original.
        save_wp_setpoints(progset.path, wp_setpoints)
        progset.save(progset.path/'settings.json')
        c.initiate()
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    except ExperimentError:
        logger.info("Experiment error. Aborting.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{progset.path}'")
        save_script(progset.path)
    logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")
    return True


def parse_wp_csv(fname):
    """Parses the csv file containing working points into a pandas Dataframe.
    """
    # parses the csv file for working points
    df = pd.read_csv(fname)
    # the header column is usually commented out
    col0 = df.columns[0]
    col0clean = col0.replace('#', '').lstrip()
    return df.rename(columns={col0: col0clean})


if __name__ == "__main__":
    progset = ProgramSettings.from_cmdline()
    if progset.path.exists():
        print(f"'{progset.path}' exists! Aborting.")
        sys.exit(42)
    else:
        progset.path.mkdir()
    main(progset)
