"""Script for cavity characterizations."""
import typing as t
import os
import sys
from pathlib import Path
import json
from shutil import copyfile
import time

from datargs import argsclass, arg, parse
from dataclasses import asdict

import groundcontrol
from groundcontrol import __version__
from groundcontrol.controllers.haloscope import HaloscopeController
from groundcontrol.instruments import AttocubeANC350
from groundcontrol.helper import default_serializer
from groundcontrol.logging import set_stdout_loglevel, setup_logger,\
        DEBUG, INFO, create_log
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.resources import get_uris
from groundcontrol.settings import VNASettings
from groundcontrol.helper import pairwise
from groundcontrol.declarators import declarative
from groundcontrol.measurement import SParam1PModel, combine_measurements


groundcontrol.USE_UNICODE = False
__script_version__ = "0.1"
__measurement_name__ = "CAVityCHARacterization"

logger = setup_logger("cavchar")

isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


@argsclass
class ProgOpts:
    path: Path = arg(
            positional=True, help="Data save directory.")
    segends: float = arg(
            nargs='+', aliases=['-S'],
            help="Frequency segment endpoints for wide searches.")
    ifbw: float = arg(help="IF bandwidth in Hz.")
    power: float = arg(help="Power in dBm.")
    step: float = arg(help="VNA sweep step in Hz.")
    rotstep: int = arg(
            help="Number of steps for rotational piezo actuation.")
    rotdir: str = arg(
            choices=['FW', 'BW'], help="Rotational piezo actuation direction.")
    rotdelay: float = arg(
            help="Delay before measurement after a rotational step, in sec.")
    count: int = arg(
            help="Total number of samples.")

    def save(self, fp):
        dc = {k: default_serializer(v, False)
              for k, v in asdict(self).items()}
        if isinstance(fp, Path) or isinstance(fp, str):
            with open(fp, 'w') as fl:
                json.dump(dc, fl, indent=4)
        else:
            json.dump(dc, fp, indent=4)


@declarative
class CavCharController:
    hc: HaloscopeController
    mio: MIOFileCSV

    segments: t.List[t.Tuple[float, float]]
    vnaset: VNASettings
    rotstep: int
    rotdir: str
    rotdelay: float
    count: int

    def initiate(self):
        hc = self.hc
        hc.initiate()
        hc.set_vna_settings(self.vnaset)
        hc.activate_vna_output()

    def run(self):
        for i in range(self.count):
            mref = f"i{i}"
            sppm = self.measure_segments(self.segments)
            sppm.mref = f"WIDE-{mref}"
            logger.info(f"Recording data for MREF={mref}.")
            self.mio.write(sppm)

            self.move_rot(self.rotstep)
            self.sleep(self.rotdelay)


    def move_rot(self, steps):
        hc = self.hc
        logger.info(f"Moving rotational piezo {steps} steps.")
        for i in range(steps):
            hc.pc.do_move_single_step(hc.pc.AxisNo.A1, direction=self.rotdir)

    def measure_segments(self, segments) -> SParam1PModel:
        sppms = []
        for i, seg in enumerate(segments):
            # last = i == len(segments)-1
            last = True  # HACK
            sppms.append(self.measure_segment(seg, last))
        return combine_measurements(sppms)

    def measure_segment(self, segment, last=False):
        logger.info(f"Measuring frequency segment {segment}.")
        hc = self.hc

        if last:
            stop = segment[1]
        else:
            stop = segment[1] - self.vnaset.sweep_step

        hc.set_vna_settings(
                self.vnaset.evolve(
                    start_frequency=segment[0],
                    stop_frequency=stop))
        return hc.measure_s21()

    def sleep(self, sec):
        time.sleep(sec)

    def finish(self):
        self.hc.pc.disconnect()

    @classmethod
    def make(
            cls,
            settings: ProgOpts,
            uris=None):
        if uris is None:
            uris = get_uris()
        hc = HaloscopeController.make(resources=uris)
        mio = MIOFileCSV.open(settings.path, mode='w')

        vnaset = VNASettings(
                if_bandwidth=settings.ifbw,
                sweep_step=settings.step,
                power=settings.power)

        rotdir = {
                'FW': AttocubeANC350.Direction.FORWARD,
                'BW': AttocubeANC350.Direction.BACKWARD}[settings.rotdir]

        segments = list(map(tuple, pairwise(settings.segends)))

        return cls(
                hc=hc, mio=mio, segments=segments,
                vnaset=vnaset, rotstep=settings.rotstep,
                rotdir=rotdir,
                rotdelay=settings.rotdelay, count=settings.count)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def main(progset):
    path = progset.path

    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"groundcontrol version: {__version__}")
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    mc = CavCharController.make(
        settings=progset)

    mc.initiate()
    try:
        logger.info(progset)
        progset.save(path/'progset.json')
        mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    except Exception as e:
        logger.info("Unknown exception. Aborting.")
        logger.error(e)
        # LOG THIS TOO
        raise e
    finally:
        mc.finish()
        logger.info(f"Saving script to '{progset.path}'")
        save_script(progset.path)

    return mc


if __name__ == "__main__":
    progset = parse(ProgOpts)
    mc = main(progset)
