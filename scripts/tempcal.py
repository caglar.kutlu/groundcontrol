"""Temperature calibration measurement script."""
import typing as t
from pathlib import Path
import os
import sys
import time
from shutil import copyfile
import json
from datetime import datetime
from functools import wraps

from datargs import argsclass, arg
from dataclasses import asdict
import attr
from datargs import parse as parsearg
import numpy as np

import groundcontrol
from groundcontrol import __version__
from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.instruments import LS372TemperatureController
from groundcontrol.controller import Controller, instrument
from groundcontrol.declarators import declarative, to_json
from groundcontrol.settings import SASettings
from groundcontrol.logging import (
        setup_logger, create_log, INFO,
        DEBUG, set_stdout_loglevel)
from groundcontrol.measurement import (
        MeasurementModel, quantity, parameter)
import groundcontrol.units as un
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.resources import get_uris
from groundcontrol.friendly import mprefix_str as _mstr
from groundcontrol.helper import default_serializer
from groundcontrol.settings import TCSettings


groundcontrol.USE_UNICODE = False
__script_version__ = "0.1"
__measurement_name__ = "TempSensCal"

logger = setup_logger("tempcal")

isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


class SensorResistanceRecord(MeasurementModel):
    timestamp: str = quantity(un.nounit, "Timestamp")
    channel: str = quantity(un.nounit, 'MeasurementChannel')
    resistance: float = quantity(un.ohm, "Resistance")
    resistance_min: float = quantity(un.ohm, "ResistanceMin")
    resistance_max: float = quantity(un.ohm, "ResistanceMax")
    reactance: float = quantity(un.ohm, "Reactance")
    exc_pow: float = quantity(un.watt, "ExcitationPower")
    exc_volt: float = quantity(un.volt, "ExcitationVoltage")
    exc_curr: float = quantity(un.ampere, "ExcitationCurrent")
    read_stat: str = quantity(un.nounit, "ReadingStatus")
    exc_frequency: float = quantity(un.hertz, "ExcitationFrequency")
    dwell: float = parameter(un.second, "DwellPeriod")
    sampling_rate: float = parameter(un.hertz, 'SamplingRate')


@argsclass
class ProgramSettings:
    path: Path = arg(True)
    channels: t.Tuple[str] = arg(
            nargs='+',
            help="Channels to scan.",
            aliases=['-C'])
    sampling_rate: float = arg(
            default=4,
            help="Sampling rate given in units of sample per second.",
            aliases=['-r'])
    dwell_period: float = arg(
            default=1,
            help="Dwell period in seconds per channel.",
            aliases=['-d']
            )

    def save(self, fp):
        dc = {k: default_serializer(v, False)
              for k, v in asdict(self).items()}
        if isinstance(fp, Path) or isinstance(fp, str):
            with open(fp, 'w') as fl:
                json.dump(dc, fl, indent=4)
        else:
            json.dump(dc, fp, indent=4)


def _autodec(fun):
    @wraps(fun)
    def method(self, *args, **kwargs):
        if self._isauto:
            self._channel, self._isauto = self._query_scanner()
        return fun(self, *args, **kwargs)
    return method


class TCController(Controller):
    tc: LS372TemperatureController = instrument()
    _channel: t.Optional[str] = None
    _isauto: bool = False

    @property
    def last_active_channel(self):
        return self._channel

    def __setup__(self):
        self._channel, self._isauto = self._query_scanner()

    def _query_scanner(self):
        tc = self.tc
        ch, auto = tc.query_scanner_parameter()
        ch = ch.value
        auto = auto.asbool()
        return ch, auto

    def query_active_channel(self):
        ch, _ = self._query_scanner()
        return ch

    def query_autoscan(self):
        _, auto = self._query_scanner()
        return auto

    @_autodec
    def query_channel_settings(
            self, ch: t.Optional[str] = None):
        if ch is None:
            ch = self._channel
        ChSet = TCSettings.ChannelSettings
        tc = self.tc
        chst, dwell, pause, cno, tco = tc.query_input_parameter(ch)
        mode, exc, ar, rng, cssh, units = tc.query_input_setup(ch)
        fst, fsettle, fwindow = tc.query_filter(ch)
        ef = tc.query_excitation_frequency(ch)
        return ChSet(
            mode, exc, ar, rng, cssh, units,
            chst.asbool(), dwell, pause, cno, tco, fst.asbool(),
            fsettle, fwindow, ef)

    def query_settings(
            self,
            channels: t.Iterable[str] = tuple(
                map(lambda c: c.value,
                    LS372TemperatureController.Channel))):
        chd = {}
        for ch in channels:
            chd[ch] = self.query_channel_settings(ch)

        return TCSettings(chd)

    @_autodec
    def query_resistance(self):
        return self.tc.query_resistance_reading(self._channel)

    @_autodec
    def query_min_max(self, reset: bool = True) -> t.Tuple[float, float]:
        min, max = self.tc.query_min_max(self._channel)
        if reset:
            self.tc.do_reset_min_max()
        return min, max

    @_autodec
    def query_reactance(self):
        tc = self.tc
        return tc.query_quadrature_reading(self._channel)

    @_autodec
    def query_excitation_power(self):
        return self.tc.query_excitation_power(self._channel)

    def set_channel(self, ch: str):
        tc = self.tc
        retval = self.tc.set_scanner_parameter(
            ch, tc.State.from_bool(self._isauto))

        # device responds to next query before processing this one
        # tried OPC, didnt work.  Maybe WAI?
        # Doing a quick hack for now, by setting a small amount of sleep time.
        # minimum settling time is 3s for LS372, so this will have no
        # measurable effect
        time.sleep(0.2)
        self.tc.query_opc()
        self._channel = ch
        return retval

    @_autodec
    def set_autoscan(self, enable: bool):
        tc = self.tc
        return tc.set_scanner_parameter(
                self._channel,
                tc.State.from_bool(enable))

    def is_settled(self) -> bool:
        _, istat = self.tc.query_reading_settle()
        return istat == istat.VALID

    def is_control_settled(self) -> bool:
        """Control input runs parallel to the other inputs."""
        istat, _ = self.tc.query_reading_settle()
        return istat == istat.VALID

    def block_until_settled(
            self,
            tick_s: float = 0.05,
            timeout_s: float = 201) -> bool:
        """Blocks until the active channel settles.
        Args:
            tick_s:  Time period for polling.
        """

        start = time.time()
        while (time.time() - start) < timeout_s:
            if self.is_settled():
                return True
            time.sleep(tick_s)

        return False

    @_autodec
    def query_reading_status(self):
        return self.tc.query_input_reading_status(self._channel)


@declarative
class MainController:
    tcc: TCController
    mio: MIOFileCSV
    progset: ProgramSettings
    _tcsettings: TCSettings = None
    _chsets: t.Dict[str, TCSettings.ChannelSettings] = None

    def initiate(self):
        self.tcc.initiate()
        logger.info("Querying current settings.")
        self._tcsettings = self.tcc.query_settings()

        chs = progset.channels
        self._chsets = {}
        for ch in chs:
            self._chsets[ch] = self._tcsettings.channels[ch]

        to_json(self._tcsettings, self.progset.path / 'tcsettings.json')

    def query_record(self):
        tcc = self.tcc

        tstamp = datetime.now().isoformat()
        rstat = tcc.query_reading_status().name
        res = tcc.query_resistance()
        rmin, rmax = tcc.query_min_max()
        react = tcc.query_reactance()

        ch = tcc.query_active_channel()

        _CHSet = TCSettings.ChannelSettings
        chset: _CHSet = self._chsets[ch]

        excitmode = chset.excitation_mode
        if excitmode == chset.ExcitationMode.VOLTAGE:
            excitvolt = chset.excitation.asfloat()
            excitcurr = np.nan
        else:
            excitcurr = chset.excitation.asfloat()
            excitvolt = np.nan

        excitfreq = chset.excitation_frequency.asfloat()
        excitpow = tcc.query_excitation_power()

        record = SensorResistanceRecord(
                    tstamp,
                    ch,
                    res,
                    rmin,
                    rmax,
                    react,
                    excitpow,
                    excitvolt,
                    excitcurr,
                    rstat,
                    excitfreq,
                    self.progset.dwell_period,
                    self.progset.sampling_rate)
        return record

    def scan(self) -> t.Generator:
        progset = self.progset
        tcc = self.tcc
        sampperiod = 1/self.progset.sampling_rate
        for ch in progset.channels:
            if tcc.last_active_channel != ch:
                logger.info(f"Setting channel {ch} and waiting for settling.")
                tcc.set_channel(ch)
            tcc.block_until_settled()
            logger.info(f"Channel settled ({ch}).")
            start = time.time()
            before = start
            while (time.time() - start) < progset.dwell_period:
                record = self.query_record()
                minmax = '/'.join((f"{record.resistance_min:.2f}",
                                   f"{record.resistance_max:.2f}"))
                logger.info(f'R{ch}(MIN/MAX): '
                            f'{record.resistance:.2f}({minmax})')
                yield record
                dif = time.time() - before

                if dif < sampperiod:
                    self.sleep(sampperiod - dif)
                    before = time.time()
                else:
                    before = time.time() - dif + sampperiod

    def sleep(self, sec):
        time.sleep(sec)

    def run(self):
        logger.info("Starting measurements.")
        while True:
            for record in self.scan():
                record.mref = 'EXP'
                self.mio.append(record)

    def finish(self):
        logger.info("Finishing measurements.")

    @classmethod
    def make(cls, progset):
        resources = get_uris()
        tcc = TCController.make(resources=resources)
        mio = MIOFileCSV.open(progset.path, mode='w')
        return cls(tcc, mio, progset)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def main(progset):
    path = progset.path

    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"groundcontrol version: {__version__}")
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    mc = MainController.make(
        progset=progset)

    mc.initiate()
    try:
        logger.info(progset)
        progset.save(path/'progset.json')
        mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    except Exception as e:
        logger.info("Unknown exception. Aborting.")
        logger.error(e)
        # LOG THIS TOO
        raise e
    finally:
        mc.finish()
        logger.info(f"Saving script to '{progset.path}'")
        save_script(progset.path)

    return mc


if __name__ == "__main__":
    progset = parsearg(ProgramSettings)
    mc = main(progset)
