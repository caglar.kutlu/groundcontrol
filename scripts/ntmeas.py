"""A straightforward NT measurement script.

It sweeps the provided noise source temperature set points. At each set point:
- S21 is measured using the VNA.
- Power spectrum is measured using SA.

"""
import os
import sys
from shutil import copyfile
import typing as t
import argparse
from pathlib import Path
from datetime import datetime
import itertools as it
from functools import partial
import time
import abc
from decimal import Decimal

import attr
import json
import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.instruments.spectrumanalyzer import (
        SCPISpectrumAnalyzer, RohdeSchwarzFSV)
from groundcontrol.instruments.signalgenerator import (
        SCPISignalGenerator)
from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.controller import InstrumentManager
from groundcontrol.instruments.misc.downconverter import\
        ThinkRFAsDownconverterController, SGDownconverterController
from groundcontrol.instruments.misc.capp_digitizer import\
        CAPPDigitizer
from groundcontrol.settings import VNASettings, SASettings
from groundcontrol.logging import setup_logger, create_log, INFO, \
        set_stdout_loglevel, DEBUG
from groundcontrol.declarators import declarative, setting
from groundcontrol.measurement import \
        MeasurementModel, quantity, parameter, SASpectrumModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.resources import get_uris
import groundcontrol.units as un
from groundcontrol.exceptions import ExperimentError
from groundcontrol import friendly
from groundcontrol.util import watt2dbm

try:
    from groundcontrol.instruments.spectrumanalyzer.speqan import SpeqanClient
except ImportError as e:
    __e1 = e
    SpeqanClient = None

try:
    from speqan.types import PSDEstimate
except ImportError as e:
    __e2 = e
    PSDEstimate = None


def to_json(obj, path):
    with open(path, 'w') as fl:
        json.dump(attr.asdict(obj), fl, indent=4)


__script_version__ = "0.4"
__measurement_name__ = "ntmeas"

logger = setup_logger(__measurement_name__)
logger.info(f"groundcontrol version: {__version__}")

isdebug = True
try:
    _ = os.environ['DEBUG']
    # if debug
    set_stdout_loglevel(DEBUG)
except KeyError:
    isdebug = False


class TemperatureMeasurement(MeasurementModel):
    temperature: float = quantity(un.kelvin, "Temperature")
    stdev: float = quantity(un.kelvin, "Temperature Stdev")
    settle_delay: float = quantity(un.second, "Settle Delay")
    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))


def setup_parser():
    parser = argparse.ArgumentParser(
            description=(__doc__))

    parser.add_argument(
            '--repeat',
            type=int,
            default=1,
            help=("The measurements are repeated one after the other "
                  "given amount of times. If setpoints are given as "
                  "[0.1, 0.2, 0.3]  and repeat is set to 2, the measurements "
                  "will be performed as if temperatures are [0.1, 0.2, 0.3, "
                  "0.1, 0.2, 0.3]."))

    parser.add_argument(
            '--repeat-measurements',
            action='store_const',
            const=True,
            default=False,
            help="If this flag is provided --repeat option determines the "
                 "number of measurements performed per temperature setpoint.")

    parser.add_argument(
            '--no-measure-transmission',
            action='store_const',
            const=True, default=False,
            help="Bypass s21 measurements.")

    parser.add_argument(
            '--dry',
            action='store_const',
            const=True,
            default=False,
            help="Print settings and quit without initiating measurement.")

    parser.add_argument(
            '--step',
            type=float,
            default=None,
            help="Frequency step(bin width) in Hz."
                 "Used both for VNA and SA."
                 "If not given, the current setting on "
                 "the instrument will be used."
            )

    parser.add_argument(
            '--rbw',
            type=float,
            default=None,
            help="Resolution bandwidth setting for SA in Hz. "
                 "If not given, the current setting on "
                 "the instrument will be used."
            )

    parser.add_argument(
            '--vbw',
            type=float,
            default=None,
            help="Video bandwidth setting for SA in Hz. "
                 "If not given, the current setting on "
                 "the instrument will be used."
            )

    parser.add_argument(
            '--sa-navg',
            type=int,
            default=1,
            help="Number of averages for SA."
                 "If not given, NO averaging is done."
            )

    parser.add_argument(
            '--vna-navg',
            type=int,
            default=None,
            help="Number of averages for VNA."
                 "If not given, the current setting on "
                 "the instrument will be used."
            )

    parser.add_argument(
            '--vna-step',
            type=float,
            default=None,
            help="Overrides Frequency step for VNA only.  Given in Hz."
                 "If not given, --step option is applied."
            )

    parser.add_argument(
            '--vna-ifbw',
            type=float,
            default=None,
            help="IF bandwidth for VNA given in Hz. "
                 "If not given, the current setting on "
                 "the instrument will be used."
            )

    parser.add_argument(
            '--vna-power',
            type=float,
            default=None,
            help="Signal power for the transmission measurement. If not given "
                 "the setting present on the VNA is used."
            )

    parser.add_argument(
            '--settle-delay',
            type=float,
            default=30,
            help="Temperature settling delay in seconds.  Defaults to 30.")

    parser.add_argument(
            '--settle-timeout',
            type=float,
            default=30*60,
            help="Timeout for settling in seconds.  Defaults to 30*60.")

    parser.add_argument(
            '-p',
            '--path',
            type=Path,
            help="Output filename.  Defaults to 'ntmeas.out' if not given.",
            default='ntmeas.out'
            )

    parser.add_argument(
            '--noise-channel',
            type=int,
            help="Noise source channel.",
            default=2)

    parser.add_argument(
            '--with-digitizer-exclusive',
            action='store_const',
            const=True,
            default=False,
            help="The spectrum is measured only with CAPP digitizer.")

    parser.add_argument(
            '--with-speqan-exclusive',
            action='store_const',
            const=True,
            default=False,
            help="The spectrum is measured using a `speqan` client "
                 "connecting to a `speqan` service running on "
                 "tcp://127.0.0.1 port 5566.")

    parser.add_argument(
            '--external-mixer',
            action='store_const',
            const=True,
            default=False,
            help="An external mixer is used during measurement.")

    parser.add_argument(
            '--extmix-if-freq-nbins',
            nargs=2,
            type=float,
            default=None,
            help="Frequency and span to be used for the IF spectrum.")

    parser.add_argument(
            '--sa-uri',
            type=str,
            default=None,
            help="Overrides the default uri for 'sa' from the rack.")

    parser.add_argument(
            '--sa-name',
            type=str,
            default=None,
            help=("Overrides the default key for the SA uri in the resource "
                  "dictionary."))

    reqgrp = parser.add_argument_group("Required arguments")

    reqgrp.add_argument(
            '-S',
            dest='temp_setpoints',
            type=float,
            nargs='+',
            help="Temperature setpoints in Kelvin.", required=True)

    reqgrp.add_argument(
            '-R',
            dest='range',
            type=float,
            nargs=2,
            metavar=('START', 'STOP'),
            help="Start and stop frequencies in Hz.  Start is the center "
                 "frequency of the first frequency bin, stop is the center "
                 "frequency of the last frequency bin of the measurement.",
            required=True
            )

    return parser


_nopsetting = partial(setting, isoptional=False)


@declarative
class SAController:
    """A controller for an SCPISpectrumAnalyzer.  This is implementation of the
    original interface for CAPP digitizer."""
    _sa: SCPISpectrumAnalyzer
    binwidth: float
    fcenter: float
    nbins: int

    navg: int
    filter_type: str  # windowing function name
    rbw: float  # 3dB BW
    timeout_guard_factor: float = 10
    timeout_guard_time_ms: float = 500
    _rohdeschwarz: bool = False

    @property
    def span(self):
        return (self.nbins-1)*self.binwidth

    def initiate(self):
        sa = self._sa
        isrohde = self._rohdeschwarz
        if self.fcenter is not None:
            sa.set_sense_frequency_center(self.fcenter)
        self.fcenter = sa.query_sense_frequency_center()

        if self.nbins is not None:
            sa.set_sense_sweep_points(self.nbins)
            nbins = sa.query_sense_sweep_points()
            assert nbins == self.nbins
        else:
            self.nbins = sa.query_sense_sweep_points()

        if self.binwidth is not None:
            span = self._calc_span(self.binwidth, self.nbins, isrohde)
            sa.set_sense_frequency_span(span)
        span = sa.query_sense_frequency_span()
        self.binwidth = self._calc_binwidth(span, self.nbins, isrohde)

        if self.filter_type is not None:
            sa.set_sense_bandwidth_shape(sa.FilterType.from_str(self.filter_type))
        self.filter_type = sa.query_sense_bandwidth_shape().name

        if self.rbw is not None:
            sa.set_sense_bandwidth_resolution(self.rbw)
        self.rbw = sa.query_sense_bandwidth_resolution()

    def query_vbw(self):
        return self._sa.query_sense_bandwidth_video()

    def get_averaged_spectrum(self, navg: int) -> SASpectrumModel:
        self.set_navg(navg)
        spect = self.measure_spectrum()
        return spect.values

    def set_center_frequency(self, fcenter: float):
        self._sa.set_sense_frequency_center(fcenter)
        fcenter = self._sa.query_sense_frequency_center()
        self.fcenter = fcenter

    def trigger_spectrum_analyzer(self):
        """Triggers and blocks the spectrum analyzer until the measurement is complete.
        """
        sa = self._sa
        last_swt = sa.query_sense_sweep_time()
        last_navg = sa.query_sense_average_count()

        # We need to set the timeout to larger than total measurement time
        swt_ms = last_swt*1000

        # The measurement is expected to complete around this time
        expected_completion_time_ms = last_navg*swt_ms

        # Saving last timeout setting
        oldtimeout = sa.timeout
        # Setting timeout greater than expected time
        # Measurement should be guaranteed to complete before this
        guard_factor = self.timeout_guard_factor
        sa.timeout = expected_completion_time_ms * guard_factor\
            + self.timeout_guard_time_ms

        logger.info("Triggering SA measurement.")
        sa.do_initiate_immediate()
        completion_str = friendly.timedelta(expected_completion_time_ms/1000)
        logger.info(f"Expected completion time: {completion_str}")
        # This blocks
        sa.query_opc()

        # Restoring timeout
        sa.timeout = oldtimeout

    def set_rbw(self, rbw: float):
        self._sa.set_sense_bandwidth_resolution(rbw)
        rbw = self._sa.query_sense_bandwidth_resolution()
        self.rbw = rbw

    def set_navg(self, navg: int):
        self._sa.set_sense_average_count(navg)
        if navg > 1:
            self._sa.set_sense_average_state(self._sa.State.ON)
        navg = self._sa.query_sense_average_count()
        self.navg = navg

    def query_spectrum_y(self) -> np.ndarray:
        """Queries the spectrum data as an array.

        Args:
            assume_preset:  Assumes preset values for data format and byte
            order parameters of the instrument.
        """
        sa = self._sa

        byteorder = sa.query_format_byte_order()
        datafmt = sa.query_format_data()

        data = sa.query_trace_data(datafmt, byteorder, traceno=1)
        return data

    def query_spectrum_x(self):
        """Generates an array for the frequency bins and returns it."""
        fstart = self.fcenter - self.span/2
        fstop = fstart + self.span
        npoints = self.nbins

        f = np.linspace(fstart, fstop, npoints)
        return f

    def read_spectrum(self) -> SASpectrumModel:
        """Reads the spectrum from the analyzer and returns it as a
        measurement.
        """
        sa = self._sa

        # ASSUMPTION: Measurement is on trace 1
        s = self.query_spectrum_y()
        f = self.query_spectrum_x()

        # ASSUMPTION:  SA has units set to dBm
        rbw = self.rbw
        vbw = sa.query_sense_bandwidth_video()
        navg = sa.query_sense_average_count()

        # Saving the enums with their names rather than values, since the names
        # are more descriptive.  This is because of the way the enums are used
        # within instrument classes, they actually encode the return strings
        # rather than solely providing enumeration/tokenization.
        detector = sa.query_sense_detector().name
        filter_type = self.filter_type

        measurement = SASpectrumModel(
                frequencies=f,
                values=s,
                rbw=rbw,
                vbw=vbw,
                navg=navg,
                detector=detector,
                filter_type=filter_type)

        return measurement

    def measure_spectrum(self) -> SASpectrumModel:
        """Triggers the spectrum analyzer and reads off the measurement."""
        self.trigger_spectrum_analyzer()
        return self.read_spectrum()

    def _set_span_w_binwidth(self, binwidth: float):
        """Rohde & Schwarz has a different definition for SPAN than Keysight
        instruments.  Regardless of the instrument, the input argument is
        treated as span = f_lastbin - f_firstbin.  The SA is configured to give
        consistent results within the context of `groundcontrol`.

        Note that this mutates `binwidth` variable.
        """
        if self._rohdeschwarz:
            span = self.nbins * binwidth
            self._sa.set_sense_frequency_span(span)
            _span = self._sa.query_sense_frequency_span()
            # to match our definition
            span = _span - binwidth
        else:
            span = (self.nbins - 1) * binwidth
            self._sa.set_sense_frequency_span(span)
            span = self._sa.query_sense_frequency_span()

    def set_nbins(self, nbins: int):
        self._sa.set_sense_sweep_points(nbins)
        nbins = self._sa.query_sense_sweep_points()
        self.nbins = nbins

    def set_binwidth(self, binwidth: float):
        self._set_span_w_binwidth(binwidth)
        self.binwidth = binwidth

    @staticmethod
    def _calc_span(binwidth, nbins, isrohde=False):
        if isrohde:
            span = nbins * binwidth
        else:
            span = (nbins - 1) * binwidth
        return span

    @staticmethod
    def _calc_binwidth(span, nbins, isrohde=False):
        """Span here is as responded by the instrument."""
        if isrohde:
            return span/nbins
        else:
            return span/(nbins-1)

    @classmethod
    def make(
            cls,
            sa: SCPISpectrumAnalyzer,
            binwidth: t.Optional[float] = None,
            fcenter: t.Optional[float] = None,
            nbins: t.Optional[int] = None,
            navg: t.Optional[int] = None,
            filter_type: t.Optional[str] = None,
            rbw: t.Optional[float] = None):
        isrohde = isinstance(sa, RohdeSchwarzFSV)

        return cls(
                sa, binwidth, fcenter, nbins, navg, filter_type, rbw,
                rohdeschwarz=isrohde)


class ISpectrumMeasurer(abc.ABC):
    @abc.abstractmethod
    def initiate(self):
        pass

    @property
    @abc.abstractmethod
    def fcenter(self):
        pass

    @property
    @abc.abstractmethod
    def binwidth(self) -> float:
        pass

    @property
    @abc.abstractmethod
    def nbins(self) -> int:
        pass

    @abc.abstractmethod
    def get_averaged_spectrum(self, navg: int) -> np.ndarray:
        pass


class IDownconverter(abc.ABC):
    @abc.abstractmethod
    def initiate(self):
        pass

    @abc.abstractmethod
    def set_center_frequency(self, fcenter: float):
        pass


@attr.define
class SpeqanAdapter(IDownconverter, ISpectrumMeasurer):
    uri: str
    _client: t.Optional[SpeqanClient] = None
    _nbins: t.Optional[int] = None
    _binwidth: t.Optional[float] = None

    def initiate(self):
        self._client = SpeqanClient.from_visa_resource_name(self.uri, 'speqan')

    def set_center_frequency(self, fcenter: float):
        success = self._client.set_center_frequency(Decimal(fcenter))
        return success

    def query_center_frequency(self) -> float:
        return float(self._client.query_center_frequency())

    def get_averaged_spectrum(self, navg: int) -> SASpectrumModel:
        psdest: PSDEstimate = self._client.get_averaged_spectrum(navg)
        return watt2dbm(psdest.spectrum)

    @property
    def nbins(self) -> int:
        if self._nbins is None:
            self._nbins = self._client.query_nbins()
        return self._nbins

    @property
    def fcenter(self):
        """IF center frequency."""
        return 0

    @property
    def binwidth(self) -> float:
        if self._binwidth is None:
            self._binwidth = float(self._client.query_binwidth())
        return self._binwidth


@declarative
class BasebandSAController:
    """This class is to control a downconverter+SA combo, or
    a thinkRF+CAPPDigitizer combo, or a `speqan` client.  It was originally written to
    accomodate only for the CAPP digitizer."""
    dcc: t.Union[
            ThinkRFAsDownconverterController,
            SGDownconverterController, IDownconverter]
    dig: t.Union[CAPPDigitizer, SAController, ISpectrumMeasurer]
    _dcc_settle_s: float = 0.1

    def __setup__(self):
        self.dcc.initiate()
        if isinstance(self.dig, SAController):
            self.dig.initiate()

    def initiate(self):
        self.__setup__()

    def center_digitizer_at(self, freq):
        """Tunes the downconverter so that the central bin is at the
        given frequency in multiples of 10 Hz.

        Returns the real centered frequency, which has a resolution of 10 Hz.
        """
        dcc = self.dcc
        dfcenter = self.dig.fcenter
        ftrunc = np.round((freq - dfcenter) / 10) * 10
        fcenter = ftrunc + dfcenter  # lower resolution center
        # we use upper sideband
        dcc.set_center_frequency(ftrunc)
        time.sleep(self._dcc_settle_s)
        logger.debug(f"f_DCC={ftrunc:.0f} Hz")
        return fcenter

    def measure_spectrum_at(
            self,
            fcenter: float,
            navg: int = 1):
        self.center_digitizer_at(fcenter)
        logger.info(f"Digitizer centered at {fcenter/1e9:.9f} GHz")

        logger.info(f"Measuring {navg} power spectra with digitizer.")
        dbm = self.dig.get_averaged_spectrum(navg)

        bwid = self.dig.binwidth  # BINWIDTH, NOT BANDWIDTH
        nbins = self.dig.nbins
        span = (nbins - 1) * bwid
        freq = np.linspace(fcenter - span / 2, fcenter + span / 2, nbins)

        if isinstance(self.dig, (CAPPDigitizer, SpeqanAdapter)):
            rbw = 0.8845 * bwid
            filter_type = 'Rectangular'
            vbw = rbw
        else:
            rbw = self.dig.rbw
            filter_type = self.dig.filter_type
            vbw = self.dig.query_vbw()

        sasm = SASpectrumModel(
                frequencies=freq, values=dbm,
                rbw=rbw, vbw=vbw, navg=navg,
                detector='RMS', filter_type=filter_type)

        return sasm

    def _calc_coverage_params_start_stop(
            self, start: float, stop: float) -> t.Tuple[float, float]:
        """Returns number of sweeps required to cover from start to stop.

        Sweeps are assumed to be left inclusive. and bins are overlapping.
        This may change.

        """
        dignbins = int(self.dig.nbins)
        span = stop - start
        nbins = int(np.ceil(span/self.dig.binwidth) + 1)

        # kinda not sure about this
        nsweeps, r = divmod(nbins-1, dignbins-1)
        if r != 0:
            nsweeps += 1
        return nsweeps, r

    def gen_measure_spectrum_coverage(
            self, start: float, stop: float, navg: int = 1) -> t.Generator:
        nswp, r = self._calc_coverage_params_start_stop(start, stop)
        logger.info(f"Preparing to perform {nswp} sweeps for coverage.")
        digspan = (self.dig.nbins - 1)*self.dig.binwidth
        logger.debug(f"Span of SA/DIG: {digspan}")
        f0 = start + digspan/2
        fcenters = np.arange(nswp)*digspan + f0
        logger.debug(f"Center frequencies for coverage: {fcenters}")

        for i, fc in enumerate(fcenters):
            spect = self.measure_spectrum_at(fc, navg)
            if i != 0:
                f = spect.frequencies
                v = spect.values
                if i == (nswp - 1):
                    # truncate last spectrum for the desired coverage
                    mr = None if r == 0 else -r
                    spect = spect.evolve(
                            frequencies=f[1:mr],
                            values=v[1:mr])
                else:
                    spect = spect.evolve(
                            frequencies=f[1:],
                            values=v[1:])
            yield spect

    def measure_spectrum_coverage(
            self, start: float, stop: float, navg: int = 1) -> SASpectrumModel:
        gen = self.gen_measure_spectrum_coverage(start, stop, navg)

        template = None
        freqs = []
        vals = []
        for i, spect in enumerate(gen):
            logger.info(f"Measured {i+1}th spectrum.")
            if i == 0:
                template = spect
            freqs.append(spect.frequencies)
            vals.append(spect.values)

        freqarr = np.hstack(freqs)
        valarr = np.hstack(vals)

        spectrum = template.evolve(frequencies=freqarr, values=valarr)
        return spectrum

    @classmethod
    def thinkrf_cappdig_combo(
            cls, im: InstrumentManager, uri: str):
        """Digitizer configuration is currently hardcoded, provide the uri for
        the downconverter.

        Resources must be dict with key 'dcc' and the URI.

        """
        dcc = ThinkRFAsDownconverterController.make(im, resources={'trf': uri})
        dig = CAPPDigitizer()
        return cls(dcc, dig)

    @classmethod
    def sgmixer_sa_combo(
            cls,
            sg: SCPISignalGenerator,
            sa: SCPISpectrumAnalyzer,
            fcenter: float,
            binwidth: float,
            nbins: int,
            rbw: float):
        dcc = SGDownconverterController(sg)
        sacon = SAController.make(
                sa, binwidth, fcenter, nbins, rbw=rbw)
        return cls(dcc, sacon)

    @classmethod
    def speqan(cls, uri: str):
        dig = SpeqanAdapter(uri)
        dcc = dig
        return cls(dcc, dig)


@declarative
class NTMeasSettings:
    temp_setpoints: t.List[float] = _nopsetting()
    repeat: bool = _nopsetting()
    measuretrans: bool = _nopsetting()
    vnapreset: VNASettings = _nopsetting()
    sapreset: SASettings = _nopsetting()
    temperature_settle_delay: float = _nopsetting()
    temperature_settle_timeout: float = _nopsetting()
    temperature_meas_window: float = setting(default=30)
    temperature_meas_period: float = setting(default=0.2)
    skip_initial_temp_settle: bool = setting(default=False)
    temperature_max_fluctuation: float = setting(default=1e-2)
    temperature_max_error: float = setting(default=1e-2)
    repeat_measurements: bool = setting(default=False)
    dig_excl: bool = setting(default=False)
    speqan_excl: bool = setting(default=False)
    ext_mix: bool = setting(default=False)
    ext_mix_if_freq_nbins: t.Optional[t.Tuple[float, int]] = None
    script_version: str = setting(default=__script_version__)

    _maxt: float = setting(default=1.2)

    @temp_setpoints.validator
    def _tnsvalidate(self, attribute, value):
        ls = value
        if max(ls) >= self._maxt:
            raise ValueError("Temperature must be less than "
                             f"{self._maxt:.2f} K")
        if min(ls) < 0:
            raise ValueError("Temperature setpoint must be positive.")

    @classmethod
    def from_parsed_args(cls, pargs):
        start, stop = pargs.range
        vnastep = pargs.step if pargs.vna_step is None else pargs.vna_step
        vnaset = VNASettings(
                start_frequency=start,
                stop_frequency=stop,
                if_bandwidth=pargs.vna_ifbw,
                sweep_step=vnastep,
                power=pargs.vna_power,
                naverage=pargs.vna_navg)

        if pargs.step is not None:
            sanpoints = int(np.ceil((stop-start)/pargs.step))
        else:
            sanpoints = None
        saset = SASettings(
                start_frequency=start,
                stop_frequency=stop,
                rbw=pargs.rbw,
                vbw=pargs.vbw,
                naverage=pargs.sa_navg,
                npoints=sanpoints,
                swt_auto=True)  # HARDCODED

        measuretrans = not pargs.no_measure_transmission

        if not pargs.repeat_measurements:
            setpoint = list(it.chain.from_iterable(
                it.repeat(pargs.temp_setpoints, pargs.repeat)))
        else:
            setpoint = list(np.repeat(pargs.temp_setpoints, pargs.repeat))

        if pargs.external_mixer:
            iff, ifnbins = pargs.extmix_if_freq_nbins
            ifnbins = int(ifnbins)
            extmixbla = (iff, ifnbins)
        else:
            extmixbla = None

        obj = cls(
                setpoint, pargs.repeat, measuretrans,
                vnaset, saset,
                pargs.settle_delay,
                pargs.settle_timeout,
                repeat_measurements=pargs.repeat_measurements,
                script_version=__version__,
                dig_excl=pargs.with_digitizer_exclusive,
                speqan_excl=pargs.with_speqan_exclusive,
                ext_mix=pargs.external_mixer,
                ext_mix_if_freq_nbins=extmixbla
                )
        return obj


@declarative
class NTMeasController:
    controller: RackController
    mio: MIOFileCSV

    # Settings
    settings: NTMeasSettings

    bsacon: t.Optional[BasebandSAController] = None

    _forcealign: bool = False
    _mref_fmt: str = "i{i:d}"
    _initial_vnaset: t.Optional[VNASettings] = None

    def initiate(self):
        c = self.controller

        if not c.initiated:
            c.clean_vna = True
            c.initiate()

        if self.settings.measuretrans:
            self._initial_vnaset = c.query_vna_settings()
            c.do_vna_preset(self.settings.vnapreset)

        # Setup Instruments
        c.do_sa_preset(self.settings.sapreset)

        if ((self.settings.dig_excl or self.settings.ext_mix or self.settings.speqan_excl)
                and self.bsacon is None):
            raise ValueError("Baseband SA controller not provided.")

        if self.settings.ext_mix or self.settings.speqan_excl:
            self.bsacon.initiate()

    def finish(self):
        sets = self.settings
        c = self.controller
        if sets.measuretrans:
            c.do_vna_preset(self._initial_vnaset)
        logger.info("Setting noise source temperature to initial temperature.")
        c.set_noise_source_temperature(sets.temp_setpoints[0])

    def _ramp_temperature(self, temp: float, settle_delay: float):
        c = self.controller
        sets = self.settings
        window, success = c.ramp_noise_source_temperature(
                temp,
                settle_delay,
                sets.temperature_settle_timeout,
                measurement_window=sets.temperature_meas_window,
                measurement_period=sets.temperature_meas_period,
                max_fluctuation=sets.temperature_max_fluctuation,
                max_error=sets.temperature_max_error)
        tempmeas = TemperatureMeasurement(
                np.mean(window), np.std(window),
                settle_delay)
        return tempmeas

    def _tempiter(self) -> t.Iterator:
        sets = self.settings
        lasttns = None
        for i, tns in enumerate(self.settings.temp_setpoints):
            if ((i == 0) and sets.skip_initial_temp_settle) or tns == lasttns:
                settle_delay = 0
            else:
                settle_delay = sets.temperature_settle_delay

            tempmeas = self._ramp_temperature(tns, settle_delay)
            lasttns = tns
            yield tempmeas, tns

    def _measure_transmission(self):
        c = self.controller
        sets = self.settings
        c.do_vna_preset(sets.vnapreset)
        c.activate_signal()
        logger.info("Measuring transmission.")
        tr = c.measure_transmission()
        c.do_vna_autoscale()
        c.deactivate_signal()
        return tr

    def _measure_spectrum_sa(self) -> SASpectrumModel:
        c = self.controller
        sets = self.settings
        c.do_sa_preset(sets.sapreset)
        if c.vna is not None:
            c.deactivate_signal()
        logger.info("Measuring power spectrum via SA.")
        spect = c.measure_spectrum()
        return spect

    def _measure_spectrum(self) -> SASpectrumModel:
        if self.settings.dig_excl or self.settings.ext_mix or self.settings.speqan_excl:
            return self._measure_spectrum_dig()
        else:
            return self._measure_spectrum_sa()

    def _measure_spectrum_dig(self) -> SASpectrumModel:
        saset = self.settings.sapreset
        start = saset.start_frequency
        stop = saset.stop_frequency
        logger.info("Measuring power spectrum via DIG.")
        spect = self.bsacon.measure_spectrum_coverage(
                start, stop, saset.naverage)
        return spect

    def run(self):
        sets = self.settings
        tnslast = None
        for i, (tm, tns) in enumerate(self._tempiter()):
            mref = self._mref_fmt.format(i=i)
            tm.mref = mref
            self.mio.write(tm)

            if sets.measuretrans:
                tr = self._measure_transmission()
                tr.mref = mref
                self.mio.write(tr)

            if self._forcealign and (tns != tnslast):
                logger.warning('ALIGNMENT FORCED.  THIS IS EXPERIMENTAL.')
                sa = self.controller.sa
                oldtmout = sa.timeout
                sa.timeout = 20000  # 20s is more than enough
                logger.info("Aligning SA (ALL).")
                calres = sa.query('CAL:ALL?')
                logger.info(f"Alignment result: {calres}")
                sa.timeout = oldtmout

            sp = self._measure_spectrum()
            sp.mref = mref
            self.mio.write(sp)
            tnslast = tns

    @classmethod
    def make(cls, controller, mio, settings):
        # this should be done by mio creator actually.
        return cls(controller, mio, settings)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def argsvalidate(args):
    flag = True
    if ((args.extmix_if_freq_nbins is not None) and (not args.external_mixer)):
        msg = ("The --external-mixer option must be provided with "
               "--extmix-if-freq-nbins.")
        print(msg)
        flag = False

    if ((args.external_mixer) and (args.with_digitizer_exclusive)):
        print("CAPP Digitizer measurement implies external mixer. "
              "Do not provide --external-mixer and --with-digitizer-exclusive "
              "options together.")
        flag = False

    return flag


def main(args):
    valid = argsvalidate(args)
    if not valid:
        print("Argument check failed.")
        sys.exit(42)
    settings = NTMeasSettings.from_parsed_args(args)

    if args.dry:
        logger.info(json.dumps(attr.asdict(settings), indent=4))
        sys.exit(0)

    if args.path.exists():
        logger.error(f"'{args.path!s}' exists! Aborting.")
        sys.exit(42)
    else:
        args.path.mkdir()

    mio = MIOFileCSV.open(args.path, mode='w')

    create_log(args.path / "groundcontrol.log", INFO)
    logger.info(f"Script: {__file__} | Version: {__script_version__}")
    to_json(settings, args.path / 'settings.json')

    # Creating the rack controller
    uris = get_uris()
    # no need the current source
    uris.pop('cs', None)
    if args.no_measure_transmission:
        uris.pop('vna', None)

    if args.sa_name is not None:
        uris['sa'] = uris[args.sa_name]

    if args.sa_uri is not None:
        uris['sa'] = args.sa_uri

    c = RackController.make(
            resources=uris, allow_not_found_instrument=True)
    c.verbose = False
    c.noise_source_control_channel = f"{args.noise_channel:02d}"
    c.temperature_setpoint_max = 1.2
    c._bf6_compat = True

    # Maincontroller
    mc = NTMeasController.make(
             c, mio, settings)
    # mc._forcealign = True  # EXPERIMENTAL

    if settings.dig_excl:
        im = c.instrument_manager
        digcon = BasebandSAController.thinkrf_cappdig_combo(im, uris['trf'])
        mc.bsacon = digcon
    elif settings.speqan_excl:
        if SpeqanClient is None:
            logger.error("Can not import SpeqanClient.")
            raise __e1
        if PSDEstimate is None:
            logger.error("Can not import PSDEstimate.")
            raise __e2
        digcon = BasebandSAController.speqan('tcp://127.0.0.1:5566')
        mc.bsacon = digcon

    try:
        logger.info(f"Starting measurement: '{__measurement_name__}'")
        c.initiate()

        if settings.ext_mix:
            im = c.instrument_manager
            sg2uri = uris.pop('sg2')
            sg = im.create_instrument('sg2', sg2uri, SCPISignalGenerator)
            sa = c.sa
            fcenter, nbins = settings.ext_mix_if_freq_nbins
            rbw = settings.sapreset.rbw
            binwidth = args.step
            bsacon = BasebandSAController.sgmixer_sa_combo(
                    sg, sa, fcenter, binwidth, nbins, rbw)
            mc.bsacon = bsacon

        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    except ExperimentError:
        logger.info("Experiment error. Aborting.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{args.path}'")
        save_script(args.path)
    logger.info(f"Measurement '{__measurement_name__}' finished.")
    return True


if __name__ == "__main__":
    parser = setup_parser()
    args = parser.parse_args()

    main(args)
