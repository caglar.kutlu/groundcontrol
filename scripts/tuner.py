"""An example for using JPATuner class"""
import os
import sys

from groundcontrol.controllers import JPAController
from groundcontrol import JPATuner, WorkingPoint
from groundcontrol.instruments import RohdeSchwarzFSV, AgilentN5232A
from groundcontrol import __version__
from groundcontrol.settings import VNASettings
from groundcontrol.logging import set_stdout_loglevel, DEBUG, INFO
from groundcontrol.resources import get_uris
from groundcontrol.pid import TunerSettings

print(f"groundcontrol version: {__version__}")

try:
    rack_instance = os.environ['RACK']
except KeyError:
    sys.exit("You need to set 'RACK' environment variable to one of "
             "'BF4' or 'BF5' before running this.")

isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


uris = get_uris()


c = JPAController.make(resources=uris, allow_not_found_instrument=True)

pump_ramp_rate = 10
tuning_window = 1.75e9, 1.84e9
power_low = -30
power_resonance = -20  # Power used for resonance measurements
power_high = 0

res_preset = VNASettings(
        span=2e6,
        sweep_step=10e3,
        if_bandwidth=100, # 500
        power=power_resonance
        )

baseline_preset = VNASettings(
        start_frequency=tuning_window[0],
        stop_frequency=tuning_window[1],
        if_bandwidth=100,
        power=0,
        sweep_step=100e3)

offset_settings = VNASettings(
        span=10, sweep_step=1, power=power_low, if_bandwidth=10)

gain_settings = VNASettings(
        span=500e3, sweep_step=10e3, power=power_low, if_bandwidth=1)

res_tuner_set = TunerSettings(
            kp=0.5e-12,
            ki=8,
            kd=1e-5,
            ramp_step=200e3)

gain_tuner_set = TunerSettings(
            kp=0.01,
            ki=10,
            kd=0,
            ramp_step=1)

def initiate():
    zet_max = WorkingPoint(100e-6, tuning_window[1]*2, 20)
    zet_min = WorkingPoint(-100e-6, tuning_window[0]*2, -20)

    baseline_preset.start_frequency = tuning_window[0]
    baseline_preset.stop_frequency = tuning_window[1]

    tuner = JPATuner.make(
            controller=c,
            res_settings=res_preset,
            baseline_settings=baseline_preset,
            offset_settings=offset_settings,
            gain_settings=gain_settings,
            restun=res_tuner_set,
            gaintun=gain_tuner_set
            )

    c.initiate()
    c.pump_ramp_rate = pump_ramp_rate

    # SIGN IS IMPORTANT
    tuner.Df_sign = -1
    tuner.gain_bootstrap_threshold = 14

    tuner.zet_min = zet_min
    tuner.zet_max = zet_max
    tuner.wt = tuning_window

    return tuner


initiate_tuner = initiate
