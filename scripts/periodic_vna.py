import typing as t
from pathlib import Path
import sys
from itertools import count
import time
import argparse

import numpy as np

import groundcontrol
groundcontrol.USE_UNICODE = False
from groundcontrol.instruments import Instrument
from groundcontrol.controllers.jpa import JPAController
from groundcontrol.controller import Controller
from groundcontrol.declarators import declarative
from groundcontrol.settings import VNASettings, setting
from groundcontrol.logging import logger
from groundcontrol.measurement import NetworkParams1PModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.resources import get_uris


@declarative
class MainController:
    jpac: JPAController

    mio: MIOFileCSV
    period: float = 0
    timelimit_s: float = 0

    def initiate(self):
        self.jpac.initiate()

    def run(self):
        jpac = self.jpac
        jpac.activate_signal()

        t_start = time.time()
        for i in count():
            t_now = time.time()
            if ((self.timelimit_s != 0) and
                ((t_now - t_start) > self.timelimit_s)):
                logger.info("Time limit exceeded.")
                break

            mref = f"i{i}"
            # Measured current and error
            cmeas = jpac.measure_current(1)
            logger.debug("Writing current measurement")
            cmeas.mref = mref
            self.mio.write(cmeas)

            sparam = jpac.measure_transmission()
            logger.debug("Writing S-parameter measurement")
            sparam.mref = mref
            self.mio.write(sparam)

            if self.period != 0:
                logger.info(f"Sleeping for {self.period:.3f} seconds.")
                self._sleep(self.period)

    _ResourceType = t.Union[str, t.Tuple[str, t.Type[Instrument]]]

    def _sleep(self, time_s):
        time.sleep(time_s)

    @classmethod
    def make(
            cls,
            resources: t.Dict[str, _ResourceType],
            path: str):
        jpac = JPAController.make(resources=resources)
        im = jpac.instrument_manager
        mio = MIOFileCSV.open(path, mode='w')
        classes = {}

        for k, v in resources.items():
            try:  # if a mapping of str -> tuple
                rscaddr, inscls = v
            except ValueError:  # elif a mapping of  str -> str
                rscaddr = v
                inscls = None
            resources[k] = rscaddr
            if inscls is not None:
                classes[k] = inscls

        return cls(jpac=jpac,
                   mio=mio)


def setup_parser():
    parser = argparse.ArgumentParser(
            description=("""Continuously measures S21 and current."""
                ))

    parser.add_argument(
            '--period',
            help="Measurement period in seconds.",
            default=0,
            type=float)

    parser.add_argument(
            '--time-limit',
            help="Measurement time limit in seconds.",
            default=0,
            type=float)

    parser.add_argument(
            '-p',
            '--path',
            type=Path,
            help="Path to the folder to be used when saving data.")
    return parser


def main():
    uris = get_uris()

    parser = setup_parser()
    args = parser.parse_args()

    period = args.period
    path = args.path

    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()
    mc = MainController.make(
        resources=uris,
        path=path)

    mc.timelimit_s = args.time_limit

    mc.period = period

    logger.info("VNA S21 RECORDING STARTED")
    mc.initiate()
    try:
        mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    finally:
        logger.info("VNA S21 RECORDING FINISHED")


if __name__ == "__main__":
    main()
