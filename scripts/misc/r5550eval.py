"""Script to do an evaluation measurement for ThinkRF 5550 device.

Currently, only used mode is SHN and it is hardcoded into the script.

"""
import attr
import typing as t
import sys
import time
from shutil import copyfile
from pathlib import Path
import json
from dataclasses import asdict
import math
import os
from decimal import Decimal

from pyrf.devices.thinkrf import WSA
from pyrf.util import compute_spp_ppb
from pyrf.vrt import ContextPacket
import numpy as np
from scipy.signal import welch
from scipy.fft import fftshift, fft, fftfreq
from datargs import argsclass, arg, parse
from argparse import RawTextHelpFormatter

from groundcontrol.controller import Controller, instrument
from groundcontrol.controllers import SAController, SGController
from groundcontrol.controllers.sacon import PowerSpectrum
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.measurement import MeasurementModel, quantity
from groundcontrol.logging import logger, create_log, INFO, DEBUG, set_stdout_loglevel
from groundcontrol.util import watt2dbm, find_nearest_idx, dbm2watt, db2lin_pow
from groundcontrol.helper import default_serializer, StrEnum
from groundcontrol import __version__
from groundcontrol.resources import get_uris
from groundcontrol.declarators import declarative
import groundcontrol.units as _un
from groundcontrol.analysis.plotting import plot2d

desc_stability = """This program acquires data from a spectrum analyzer and a
 ThinkRF R5550 for the desired number of repetitions.  A signal generator is
 used to provide a reference signal.

Caveats:
    - SA RBW is hardcoded to 120 Hz.
    - SA span is hardcoded to 1.2 MHz.
    - R5550 RBW is hardcoded to 125 Hz.
    - R5550 span is hardcoded to 125/64 MHz = 1.953125 MHz.
    - IF USING CW SIGNAL:
        - Signal generator frequency offset should be chosen to be a common
          multiple of 120 and 125 Hz so that the signal is centered in each
          device's frequency bins.
    - IF USING NOISE MODULATED SIGNAL:
        - Use 1.9 MHz lowpass at the I input of SG.
        - Noise power is proportional to signal generator's output power.
        - Put -1 MHz sigoffset so that the carrier is hidden and we only see a
          single sideband of the noise in measurement windows.
"""

__script_version__ = "0.1"
BITSIZE = 14
NFACTOR = 2 ** (BITSIZE - 1)


vecmax = np.vectorize(max)
_a = [1, 0, 5, 1]
_b = [1, 0, 2, 4]
_c = [1, 0, 5, 4]
assert np.allclose(vecmax(_a, _b), _c)


isdebug = True
try:
    _ = os.environ["DEBUG"]
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


class Window(StrEnum):
    RECTANGULAR = "rect"
    BH92 = "blackmanharris"


def compwelch(
    iqdata,
    context,
    nfft: int,
    reflevel_error: float,
    detrend: bool = False,
    window: Window = Window.RECTANGULAR,
):
    ref = context["reflevel"] - reflevel_error
    iq = iqdata / NFACTOR

    if detrend:
        detrend = "constant"

    f, pxx = welch(
        iq,
        fs=context["bandwidth"],
        window=window,
        nperseg=nfft,
        noverlap=0,
        average="mean",
        scaling="spectrum",
        detrend=detrend,
        return_onesided=False,
    )

    f = f + context["rffreq"]

    dbm_uncal = watt2dbm(pxx)
    pxxdbm = dbm_uncal + ref
    return fftshift(f), fftshift(pxxdbm)


def compwelch2(
    iqdata,
    context,
    nfft: int,
    reflevel_error: float,
    detrend: bool = False,
    window: Window = Window.RECTANGULAR,
):
    # THIS DOES THE SAME THING AS COMPWELCH, BUT INTERMEDIATE STEPS ARE SHOWN
    ref = context["reflevel"] - reflevel_error
    iq = iqdata / NFACTOR

    niq = len(iqdata)
    q, r = divmod(niq, nfft)
    iq = iq[: q * nfft].reshape((-1, nfft))

    y = fft(iq, axis=1) / nfft
    f = fftfreq(nfft, 1 / context["bandwidth"])
    f = f + context["rffreq"]

    pxx = np.mean((y * np.conjugate(y)).real, axis=0)

    refw = db2lin_pow(ref)
    pxxdbm = watt2dbm(pxx * refw)
    # pxxdbm = dbm_uncal + ref
    return fftshift(f), fftshift(pxxdbm)


# compwelch = compwelch2


@declarative
class ThinkRFR5550:
    resource: WSA
    name: str
    last_context: ContextPacket = None

    _maxblocks: int = 1000
    _maxiter: int = 1000000

    def compute_acquisition_params(
        self, samples: int
    ) -> t.Tuple[int, int, t.Tuple[int, int, int]]:
        """Computes the sample distribution parameters for acquisition.

        Returns:
            spp:  Sample per packet.
            ppb:  Packets per block.
            (bpa, lastspp, lastppb):  Blocks per acquisition, spp in last
                block, ppb in last block.
        """
        properties = self.resource.properties
        decimation = self.resource.decimation()
        fshift = self.resource.fshift()

        # TODO: maxppb should be a SCPI query as HDR should not have decimation
        if (decimation == 1) and (fshift == 0):
            maxppb = properties.MAX_PPB
        else:
            maxppb = properties.MAX_PPB_DECIMATED_OR_FSHIFT
        maxspp = properties.MAX_SPP
        # spb: samples per block
        maxspb = int(maxspp * maxppb)

        logger.debug(f"MAXSPP: {maxspp}, MAXPPB: {maxppb}, MAXSPB: {maxspb}")

        # Round up to the next multiple of 32
        if samples % 32:
            samples = int(32 * math.ceil(samples / 32))

        bpa = 1
        # Calculate the required number of packets
        if samples <= maxspp:
            spp = int(max(samples, properties.MIN_SPP))
            ppb = int(1)
            lastspp = spp
            lastppb = ppb
        elif samples > maxspb:
            spp = int(properties.MAX_SPP)
            ppb = int(maxppb)
            bpam1, r = divmod(samples, maxspb)
            logger.debug(f"divmod(samples, maxspb)={bpam1, r}")
            bpa = bpam1 + 1
            lastspp, lastppb, _ = self.compute_acquisition_params(r)
        else:
            spp = int(properties.MAX_SPP)
            ppb = (samples / spp + 1) if (samples % spp) else (samples / spp)
            ppb = int(ppb)
            lastspp = spp
            lastppb = ppb

        if bpa > self._maxblocks:
            raise ValueError(
                f"Maximum number of blocks exceeds the limit {self._maxblock} ({bpa})."
            )

        return (spp, ppb, (bpa, lastspp, lastppb))

    def capture_iq(self, spp, ppb) -> np.ndarray:
        """Captures the spp*ppb samples and returns a complex array of `i +
        j*q`."""
        dut = self.resource
        dut.flush()
        dut.flush_captures()
        _start = time.time()
        conpkts, datapkts = self.capture_packets(spp, ppb)
        _end = time.time()
        elapsed = _end - _start
        idata, qdata = np.vstack([pkt.data.np_array for pkt in datapkts]).T
        logger.info(f"It took {elapsed:.6f} s to acquire {spp*ppb} samples.")

        context = {}
        for pk in conpkts:
            context.update(pk.fields)

        self.last_context = context

        return idata + 1j * qdata

    def capture_packets(self, spp, ppb):
        dut = self.resource
        dut.request_read_perm()
        conpkts = []
        datapkts = []
        dut.capture(spp, ppb)
        for i in range(self._maxiter):
            packet = dut.read()

            if packet.is_context_packet():
                conpkts.append(packet)

            if packet.is_data_packet():
                datapkts.append(packet)

            if len(datapkts) == ppb:
                break
        return conpkts, datapkts

    def close(self):
        self.resource.disconnect()

    @classmethod
    def from_visa_resource_name(cls, rscname, name):
        ip = rscname.split("::")[1]
        resource = WSA()
        resource.connect(ip)
        return cls(resource, name)


class SignalGeneratorState(MeasurementModel):
    frequency: float = quantity(_un.hertz, "Frequency")
    power: float = quantity(_un.dBm, "Power")


class ExpLayout(StrEnum):
    """Enumerates the experimental layout.

    Description:
        COMP1:  Corresponds to the configuration where an R5550 and a spectrum
            analyzer are connected to the two outputs of a power splitter with a
            signal generator connected to the input of the splitter.
        SGONLY:  A signal generator is connected directly to the R5550's RF input.

    """

    COMP1 = "COMP1"
    SGONLY = "SGONLY"
    NONE = "NONE"


class MainController(Controller):
    sac: SAController = instrument(required=False)
    sgc: SGController = instrument(required=False, bindmap={"sg": "sgtest"})
    trf: ThinkRFR5550 = instrument()

    niter: int = 10
    freq: float = attr.ib(default=5.9e9)
    sigoff: float = attr.ib(default=30e3)
    sigpow: float = attr.ib(default=-50)
    decimation: float = attr.ib(default=64)
    attenuator: float = attr.ib(default=0)
    nfft: int = attr.ib(default=15625)
    trfnavg: int = 100
    sanavg: int = 40
    sarbw: float = 120
    saspan: float = 1.20e6
    sanpoints: int = 10001
    alignsa: bool = True
    sgpowsweep: t.Optional[t.Tuple[float, float, float]] = None
    sgfreqsweep: t.Optional[t.Tuple[float, float, float]] = None
    onlysig: bool = False
    detrend: bool = False
    explayout: ExpLayout = ExpLayout.COMP1
    _spp: t.Optional[int] = None
    _ppb: t.Optional[int] = None
    _bpa: int = 1
    _spplastblk: int = 0
    _ppblastblk: int = 0
    _mio: t.Optional[MIOFileCSV] = None

    @property
    def nsamples(self):
        return self.nfft * self.trfnavg

    @property
    def acqsamples(self):
        """Returns real number of samples to acquire."""
        return (
            self._spp * self._ppb * (self._bpa - 1)
            + self._ppblastblk * self._spplastblk
        )

    @attenuator.validator
    def _attenvalidate(self, attrib, val):
        validatt = [0, 10, 20, 30]
        if val not in validatt:
            raise ValueError("Attenuator must be one of {validatt}.")

    def __setup__(self):
        self._setup_trf()
        if (self.explayout is ExpLayout.NONE):
            return

        if self.explayout is ExpLayout.COMP1:
            self._setup_sa()
        self._setup_sg()

    def _setup_sg(self):
        f = self.freq + self.sigoff
        self.sgc.set_frequency(f)
        sigpow = self.sigpow if self.sgpowsweep is None else self.sgpowsweep[0]
        self.sgc.set_power(sigpow)
        self.sgc.activate()

    def _setup_sa(self):
        sac = self.sac
        sac.set_center_frequency(self.freq)
        sac.set_span(self.saspan)
        sac.set_npoints(self.sanpoints)
        sac.set_naverage(self.sanavg)

    def _setup_trf(self):
        dut = self.trf.resource
        dut.reset()
        dut.pll_reference("EXT")
        time.sleep(0.5)
        vcolock = dut.locked("VCO")
        clklock = dut.locked("CLKREF")

        if any([not vcolock, not clklock]):
            print("R5550 reference locking error.")
            sys.exit()

        dut.iq_output_path("DIGITIZER")
        dut.rfe_mode("SHN")

        pass_band_center = dut.properties.PASS_BAND_CENTER["SHN"]
        full_bw = dut.properties.FULL_BW["DEC_SHN"]
        usable_bw = dut.properties.USABLE_BW["DEC_SHN"]
        self._fsample = full_bw / self.decimation

        dut.attenuator(self.attenuator)
        dut.decimation(self.decimation)
        dut.fshift(0)
        dut.freq(self.freq)

        trigset = {
            "type": "NONE",
            "fstart": self.freq - 10e6,
            "fstop": self.freq + 10e6,
            "amplitude": -140,
        }

        dut.trigger(trigset)

        nsamples = self.nsamples

        spp, ppb, blkp = self.trf.compute_acquisition_params(nsamples)
        bpa, lastspp, lastppb = blkp
        self._spp = spp
        self._ppb = ppb
        self._bpa = bpa
        self._spplastblk = lastspp
        self._ppblastblk = lastppb

        # dut.configure_flattening()  # not tested
        # dut.flattening_enabled(True)
        time.sleep(0.1)

    def measure_thinkrf_spectrum(self) -> PowerSpectrum:
        logger.info("Triggering R5550.")
        dut = self.trf.resource
        referr = dut.properties.REFLEVEL_ERROR

        asamples = self.acqsamples
        nsamples = self.nsamples
        capture_time = asamples / self._fsample
        bpa = self._bpa
        ppb = self._ppb
        spplast = self._spplastblk
        ppblast = self._ppblastblk
        msg = (
            f"Capturing total of {asamples} samples "
            f"distributed to {ppb*(bpa-1)+ppblast} packets in "
            f"{bpa} continuous sample block(s). "
        )
        if bpa > 1:
            _sg = f"Last block has {spplast} sample per packet with {ppblast} packets. "
            msg = f"{msg} {_sg}"

        _sg = (
            f"Using sample rate {self._fsample} Hz, capture time is about "
            f"{capture_time:.6f} s."
        )
        msg = f"{msg} {_sg}"
        logger.info(msg)

        iqdata_l = []
        _start = time.time()
        for iblk in range(1, bpa + 1):
            if iblk == bpa:
                spp, ppb = spplast, ppblast
            else:
                spp, ppb = self._spp, self._ppb
            iqdata = self.trf.capture_iq(spp, ppb)
            context = self.trf.last_context
            iqdata_l.append(iqdata)
        iqdata = np.concatenate(iqdata_l)
        _end = time.time()
        elapsed = _end - _start
        logger.info(f"It took {elapsed:.6f} s to acquire the total {asamples} samples.")
        logger.info(f"Truncating acquired {asamples} samples to {nsamples} samples.")
        iqdata = iqdata[:nsamples]

        _start = time.time()
        f, pxxdbm = compwelch(
            iqdata, context, self.nfft, referr, detrend=self.detrend, window="rect"
        )  # CAREFUL IF YOU CHANGE THIS!
        # window='nuttall')
        _end = time.time()
        elapsed = _end - _start
        logger.info(
            f"It took {elapsed:.6f} s to compute a PSD using Welch's "
            f"with nperseg={self.nfft} for a total of {nsamples} "
            "samples."
        )

        # flatten  # this gives garbage result
        # pxxdbm = dut.flatten(self.freq, pxxdbm, 125, 0)

        ps = PowerSpectrum(frequency=f, power=pxxdbm, rbw=0.8845 * 125, nbw=1 * 125)

        logger.info("Acquired R5550 spectrum.")
        return ps

    def run(self):
        for step in self.rungen():
            pass

    def rungen_none(self):
        for i in range(self.niter):
            logger.info(f"Iteration: {i}")
            mref = f"i{i}"
            trfps = self.measure_thinkrf_spectrum()
            logger.info("Acquired SA spectrum.")
            trfps.mref = f"TRF-{mref}"
            self._mio.write(trfps)
            yield trfps

    def rungen(self):
        if self.explayout is ExpLayout.NONE:
            yield from self.rungen_none()
            return

        if (self.explayout is ExpLayout.COMP1) and self.alignsa:
            self.sac.align_all_blocking()

        ispowswp = self.sgpowsweep is not None
        isfreqswp = self.sgfreqsweep is not None
        if ispowswp:
            start = self.sgpowsweep[0]
            stop = self.sgpowsweep[1]
            npoints_pow = self.sgpowsweep[2]
            powarr = np.linspace(start, stop, npoints_pow)
            logger.info("Using SG power sweep.")
        else:
            npoints_pow = 1
            powarr = [np.nan]

        if isfreqswp:
            start = self.sgfreqsweep[0]
            stop = self.sgfreqsweep[1]
            npoints_freq = self.sgfreqsweep[2]
            freqarr = np.linspace(start, stop, npoints_freq)
            logger.info("Using SG frequency sweep.")
        else:
            npoints_freq = 1
            freqarr = [np.nan]

        freqmesh, powmesh = np.meshgrid(freqarr, powarr, indexing="xy")
        freqXpow = np.vstack(list(map(np.ravel, (freqmesh, powmesh)))).T
        # This generates a k-by-2 matrix with each row having
        # (frequency, power)

        trfps_buf = {}
        saps_buf = {}
        for i in range(npoints_freq * npoints_pow * self.niter):
            if (ispowswp or isfreqswp) and (i % self.niter == 0):
                idx = i // self.niter
                sigfreq, sigpow = freqXpow[idx]
                if not np.isnan(sigpow):
                    logger.info(f"Setting power: {sigpow:.2f} dBm.")
                    self.sgc.set_power(sigpow)
                if not np.isnan(sigfreq):
                    logger.info(f"Setting frequency: {sigfreq:.2f} Hz.")
                    self.sgc.set_frequency(sigfreq)
                freq = self.sgc.query_frequency()
                power = self.sgc.query_power()
                self._mio.append(SignalGeneratorState(freq, power))
                time.sleep(0.1)

            logger.info(f"Iteration: {i}")
            mref = f"i{i}"

            if self.explayout is ExpLayout.COMP1:
                spiter = self.sac.measure_spectrum_gen()
                logger.info("Triggering SA measurement.")
                exptimems = next(spiter)
                logger.info(f"Expected completion time: {exptimems/1e3} s.")

                trfps = self.measure_thinkrf_spectrum()
                saps = next(spiter)
                logger.info("Acquired SA spectrum.")
                trfps.mref = f"TRF-{mref}"
                saps.mref = f"SA-{mref}"

                if isfreqswp and self.onlysig:
                    trfidx = find_nearest_idx(trfps.frequency, sigfreq)
                    ptrf = trfps.power[trfidx]
                    ftrf = trfps.frequency[trfidx]
                    trfps_buf[ftrf] = ptrf
                    self._savepsifready(trfps_buf, npoints_freq, trfps)

                    saidx = find_nearest_idx(saps.frequency, sigfreq)
                    psa = saps.power[saidx]
                    fsa = saps.frequency[saidx]
                    saps_buf[fsa] = psa
                    self._savepsifready(saps_buf, npoints_freq, saps)
                else:
                    self._mio.write(trfps)
                    self._mio.write(saps)
                yield trfps, saps
            else:
                trfps = self.measure_thinkrf_spectrum()
                trfps.mref = f"TRF-{mref}"
                if isfreqswp and self.onlysig:
                    trfidx = find_nearest_idx(trfps.frequency, sigfreq)
                    ptrf = trfps.power[trfidx]
                    ftrf = trfps.frequency[trfidx]
                    trfps_buf[ftrf] = ptrf
                    self._savepsifready(trfps_buf, npoints_freq, trfps)
                else:
                    self._mio.write(trfps)
                yield trfps

    def _savepsifready(self, buf, nfreq, template):
        if len(buf.keys()) == nfreq:
            ps = template.evolve(frequency=list(buf.keys()), power=list(buf.values()))
            self._mio.write(ps)
            buf.clear()
            return True
        else:
            return False

    def finish(self):
        logger.info("Finishing measurement.")
        if self.explayout is ExpLayout.COMP1:
            self.sac.sa.close()
        if not (self.explayout is ExpLayout.NONE):
            self.sgc.deactivate()
            self.sgc.sg.close()
        self.trf.close()

    @classmethod
    def from_settings(
        cls,
        path: Path,
        niter: int,
        freq: float,
        sigoff: float,
        navg: int,
        sanavg: int,
        alignsa: bool,
        rbw: float,
        sgpowsweep: t.Optional[t.Tuple[float, float, int]],
        sgfreqsweep: t.Optional[t.Tuple[float, float, int]],
        explayout: ExpLayout,
        detrend: bool,
        onlysig: bool,
    ):
        rbw = Decimal(rbw)
        R125 = Decimal(125)
        R62H5 = Decimal(62.5)
        if rbw not in [Decimal(125), Decimal(62.5)]:
            raise ValueError("RBW must be 125 or 62.5")

        nfft = {R125: 15625, R62H5: 15625 * 2}[rbw]

        mio = MIOFileCSV.open(path, mode="w")
        uris = get_uris()
        obj = cls.make(resources=uris)
        obj.nfft = nfft
        obj.niter = niter
        obj.freq = freq
        obj.sigoff = sigoff
        obj._mio = mio
        obj.trfnavg = navg
        obj.sanavg = sanavg
        obj.alignsa = alignsa
        obj.sgpowsweep = sgpowsweep
        obj.sgfreqsweep = sgfreqsweep
        obj.explayout = explayout
        obj.detrend = detrend
        obj.onlysig = onlysig
        return obj


@argsclass(
    description=desc_stability, parser_params=dict(formatter_class=RawTextHelpFormatter)
)
class ProgramSettings:
    path: Path = arg()
    freq: float = arg(help="Center frequency for SA and R5550")
    niter: int = arg(help="Number of repetitions. Defaults to 1.", default=1)
    sigoff: float = arg(
        help=(
            "Signal frequency as offset from 'FREQ'.Defaults to 30 kHz if not provided."
        ),
        default=30e3,
    )
    navg: int = arg(
        help="Number of spectra to average at each iteration. Defaults to 1.", default=1
    )
    onlysig: bool = arg(
        help=(
            "The saved spectra contains only the signal at the "
            "frequency bin closest to the applied signal frequency. "
            "This option has no effect unless used together "
            "with 'sgfreqsweep'."
        )
    )
    sanavg: int = arg(
        default=None,
        help=(
            "Number of spectra to average from SA each "
            "iter.  If not given, equals to 'navg'."
        ),
    )
    alignsa: bool = False
    sgfreqsweep: t.Tuple[float, float, float] = arg(
        nargs=3,
        default=None,
        help=(
            "Do a signal generator frequency sweep from START to STOP "
            "with NPOINTS.  If combined with --sgpowsweep, frequency "
            "sweep is always done first. "
            "During sweep each point is repeated niter times."
            "This mode ignores 'sigoff' options."
        ),
        metavar=("START", "STOP", "NUM"),
    )
    sgpowsweep: t.Tuple[float, float, float] = arg(
        nargs=3,
        default=None,
        help=(
            "Do a power sweep from START to STOP with NPOINTS."
            "During sweep each point is repeated niter times."
            "This mode ignores 'sigoff' options."
        ),
        metavar=("START", "STOP", "NUM"),
    )
    plot: bool = False
    maxhold: bool = False
    detrend: bool = False
    explayout: ExpLayout = arg(
            default=ExpLayout.NONE, choices=[a.value for a in ExpLayout],
            help="""Layout mnemonics have the following meanings:
                COMP: A signal generator(SG) connected to a spectrum analyzer(SA) and a
                    ThinkRF R5550(TRF).
                SGONLY:  An SG directly connected to the TRF.
                NONE:  Just measure spectrum with the TRF.""")
    rbw: float = arg(
        choices=[125, 62.5], default=125, help="Defaults to 125 Hz."
    )

    def save(self, fp):
        dc = {k: default_serializer(v, False) for k, v in asdict(self).items()}
        if isinstance(fp, Path) or isinstance(fp, str):
            with open(fp, "w") as fl:
                json.dump(dc, fl, indent=4)
        else:
            json.dump(dc, fp, indent=4)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def _plot(seqiter, maxhold=True):
    import matplotlib.pyplot as plt

    # only works for SGONLY right now
    plt.ion()
    fig, ax = plt.subplots()
    line = None
    for i, trfps in enumerate(seqiter):
        trfps: PowerSpectrum
        if i == 0:
            (line,) = ax.plot(trfps.frequency, trfps.power)
            ax.set_xlabel("Frequency (Hz)")
            ax.set_ylabel("Power (dBm)")
            lastpow = trfps.power
        else:
            if maxhold:
                power = vecmax(trfps.power, lastpow)
            else:
                power = trfps.power
            line.set_ydata(power)
            fig.canvas.draw()
            fig.canvas.flush_events()
            lastpow = power


def main(progset):
    if progset.sanavg is None:
        progset.sanavg = progset.navg

    path = progset.path

    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"groundcontrol version: {__version__}")
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    mc = MainController.from_settings(
        path,
        niter=progset.niter,
        freq=progset.freq,
        sigoff=progset.sigoff,
        navg=progset.navg,
        sanavg=progset.sanavg,
        alignsa=progset.alignsa,
        sgpowsweep=progset.sgpowsweep,
        sgfreqsweep=progset.sgfreqsweep,
        detrend=progset.detrend,
        explayout=progset.explayout,
        onlysig=progset.onlysig,
        rbw=progset.rbw,
    )

    mc.initiate()
    try:
        logger.info(progset)
        progset.save(path / "progset.json")
        if progset.plot:
            _plot(mc.rungen(), progset.maxhold)
        else:
            mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    except Exception as e:
        logger.info("Unknown exception. Aborting.")
        logger.error(e)
        # LOG THIS TOO
        raise e
    finally:
        mc.finish()
        logger.info(f"Saving script to '{progset.path}'")
        save_script(progset.path)

    return mc


if __name__ == "__main__":
    progset = parse(ProgramSettings)
    if progset.sgpowsweep is not None:
        start, stop, num = progset.sgpowsweep
        num = int(num)
        progset.sgpowsweep = (start, stop, num)
    if progset.sgfreqsweep is not None:
        start, stop, num = progset.sgfreqsweep
        num = int(num)
        progset.sgfreqsweep = (start, stop, num)
    mc = main(progset)
