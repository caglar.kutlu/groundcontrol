import os

from groundcontrol.controllers.jpa import JPAController
from groundcontrol import __version__
from groundcontrol.resources import get_uris
print(f"groundcontrol version: {__version__}")


isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False


uris = get_uris()


c = JPAController.make(resources=uris, allow_not_found_instrument=True)
