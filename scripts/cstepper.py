"""This is a script that steps current and waits for a prespecified amount of
time.

"""
import typing as t
import time

import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.logging import setup_logger
from groundcontrol.controllers import JPAController
from groundcontrol.settings import VNASettings
from groundcontrol.declarators import setting, settings, declarative
from groundcontrol.measurement import SParam1PModel, ScalarMeasurementModel
import groundcontrol.units as un
from groundcontrol.resources import get_uris
from groundcontrol.util import unwrap_deg, dbm2watt, watt2dbm


__script_name__ = __file__
__script_version__ = "0.1"
logger = setup_logger("cstepper")
logger.info(f"groundcontrol version: {__version__}")


@declarative
class CStepper:
    """For each current, for each pump power, pump frequency is swept to cover
    the given "gain frequency" range.

    Pump power is the outer loop since certain points may have no gain for any
    pump power, but it is very likely that there is a current bias that has
    gain for a given pump.  Given that one chooses a pump power that is not too
    far off.
    """
    jpac: JPAController

    currents: np.ndarray

    _current_i: int = 0
    _started: bool = False
    _steps_completed: int = 0

    @property
    def state_text(self):
        cur = self.currents[self._current_i]

        ctxt = f"Current: {cur*1e6:.3f} uA"
        return f"{ctxt}"

    @property
    def last_current(self):
        return self.currents[self._current_i]

    @property
    def nsteps(self):
        ncur = len(self.currents)
        return ncur

    @property
    def progress(self) -> float:
        """Progress as a value between 0 and 1."""
        return self._steps_completed/self.nsteps

    def initiate(self):
        logger.info("Initiating CStepper")
        jpac = self.jpac

    def stepiter(self):
        jpac = self.jpac
        self._started = True
        for i, cur in enumerate(self.currents):
            jpac.ramp_current(cur)
            self._current_i = i
            self._steps_completed += 1
            yield cur

    def cleanup(self):
        self._started = False
        self._current_i = 0
        self._steps_completed = 0


@declarative
class MainProgram:
    current_range: t.Tuple[float, float] = setting(
            un.ampere, "Coil Current Range",
            "Start and stop values for current, both inclusive.(START, STOP)")

    current_points: int = setting(
            un.nounit, "Current Points",
            "Number of points to use when sweeping current.")
    
    period: float = setting(
            un.second, "Period",
            "Time to wait between sweep points.")

    # NO NEED TO TOUCH THESE
    pump_ramp_rate: float = setting(un.dB_d_s, "Pump Ramp Rate", default=10)
    pump_step: float = setting(un.dB, "Pump Step", default=1)
    current_ramp_rate: float = setting(
            un.ampere_d_s, "Current Ramp Rate", default=5e-6)
    current_step: float = setting(un.ampere, "Current Step", default=0.2e-6)

    _cstepper: t.Optional[CStepper] = None
    _success: bool = False

    @property
    def cstepper(self):
        return self._cstepper

    def initiate(self, resources: t.Optional[t.Dict] = None):
        if resources is None:
            resources = get_uris()

        resources = {'cs': resources['cs']}

        self._resources = resources
        jpac = JPAController.make(
                resources=resources,
                allow_not_found_instrument=True)

        jpac.pump_ramp_rate = self.pump_ramp_rate
        jpac.pump_step = self.pump_step
        jpac.current_ramp_rate = self.current_ramp_rate
        jpac.current_step = self.current_step
        # Shut the ramping up
        jpac.verbose = False

        jpac.initiate()

        c_min, c_max = self.current_range
        c_npoints = self.current_points
        currents = np.mgrid[c_min:c_max:c_npoints*1j]

        cstepper = CStepper(
                jpac,
                currents)

        self._cstepper = cstepper
        cstepper.initiate()

    def run(self):
        gf = self.cstepper
        logger.info("STARTING CURRENT STEPPING")
        for istep, gmeas in enumerate(gf.stepiter()):
            #mod = (istep % (gf.nsteps//10000))
            if True: #mod == 0:
                logger.info(f"PROGRESS: {gf.progress*100:.1f}%")
                logger.info(f"\t{gf.state_text}")
                self._sleep(self.period)

    def _sleep(self, period: float):
        logger.info(f"Sleeping for {period:f}s")
        time.sleep(period)

    def finish(self):
        self.cstepper.cleanup()
        logger.info("STEPPING TERMINATED")

def main():
    mp = MainProgram(
            current_range=(-50e-6, 50e-6),
            current_points=100,
            period=15)
    try:
        mp.initiate()
        mp.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    finally:
        mp.finish()


if __name__ == "__main__":
    main()
