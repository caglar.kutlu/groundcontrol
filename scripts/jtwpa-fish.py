"""This is a program to investigate gain performance of a JTWPA for various
pump frequency, pump power and bias current configurations.  Since some devices
can have very sparse response in the space spanned by these three variables
a fully automatic characterization is difficult.  This is especially so for
JTWPA's that are not functioning properly.  The aim of this program/script is
to facilitate such operation and reduce the overall investigation time.

The Idea  (WORK IN PROGRESS)

There will be three modes of operation:

1. Wideband Response Sweep

for fp in pump_frequencies:
    for pp in pump_powers:
        set_working_point(fp=fp, pp=pp)
        s21 = measure_wide_transmission()

    userinput = input()
    if userinput is <narrowband gain sweep>:
        narrowband_gain_sweep(fp)


2. Narrowband Gain Sweep
fp <- arg

for pp in narrow_pump_powers:
    set_working_point(fp=fp, pp=pp)
    s21 = measure_narrow_transmission()

    userinput = input()
    if userinput is <stop>:
        break
    else:
        continue


"""

