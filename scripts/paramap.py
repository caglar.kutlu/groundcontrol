"""This is to investigate the parametric amplification regime at the given
resonance frequency.  In order to do this a 2-dimensional sweep is performed in
the pump frequency and pump power domain.

TODO:
    - Refactor parsing to using `datargs`

"""
import typing as t
from pathlib import Path
from shutil import copyfile
import argparse
import sys
import os
from datetime import datetime
import time

import pandas as pd
import numpy as np
import attr

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol import WorkingPoint
from groundcontrol.analysis.calibrate import remove_baseline,\
        remove_baseline_sppm
from groundcontrol.settings import VNASettings
from groundcontrol.declarators import declarative, quantity, \
    parameter, setting, to_json
from groundcontrol.measurement import MeasurementModel, SParam1PModel
from groundcontrol.measurementio import MIOFileCSV, MIOBase
from groundcontrol.logging import setup_logger, create_log, INFO,\
    set_stdout_loglevel, DEBUG
import groundcontrol.units as un
from groundcontrol.resources import get_uris
from groundcontrol.estimation import resonance_from_phase2, phasemodel
from groundcontrol.friendly import mprefix_str as _mp
from groundcontrol.helper import Array


isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


class ResonanceNotFoundError(Exception):
    pass


__measurement_name__ = "ParaMap"
__script_version__ = "0.3"

logger = setup_logger("paramap")
logger.info(f"groundcontrol version: {__version__}")
logger.info(f"{__measurement_name__} MEASUREMENT SCRIPT STARTED")


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


@declarative
class ParaMapSweepParameters:
    fpumps: Array[float] = quantity(un.hertz, "PumpFrequency")
    ppumps: Array[float] = quantity(un.dBm, "PumpPower")
    detunings: Array[float] = quantity(un.nounit, "Detuning")
    fr0: float = parameter(un.hertz, "PassiveResonanceFrequency")
    bw0: float = parameter(un.hertz, "PassiveBandwidth")

    @classmethod
    def make(cls, fpumps, ppumps, detunings, fr0, bw0):
        return cls(fpumps, ppumps, detunings, fr0, bw0)

    @classmethod
    def from_ranges(
            cls,
            fpump_rng,
            ppump_rng,
            det_rng, fr0=None, bw0=None
            ):
        """Creates sweep parameters from given ranges given in the format
        (start, stop, step), start and stop inclusive.  For numerical
        consistency, provide integer values.

        Args:
            fpump_rng: Pump frequency range tuple (min, max, step).
            ppump_rng: Pump power range tuple (min, max, step).
                Units will be (dBm, dBm, dB).
            det_rng:  Normalized detuning range (min, max, step)
        """
        fp_min, fp_max, fp_step = fpump_rng
        pp_min, pp_max, pp_step = ppump_rng
        det_min, det_max, det_step = det_rng

        fpumps = fullrange(fp_min, fp_max, fp_step)
        ppumps = fullrange(pp_min, pp_max, pp_step)
        dets = fullrange(det_min, det_max, det_step)

        if (fr0 is None) or (bw0 is None):
            # we can calculate the used fr0 and bw0 from the given fp and det
            # values
            fp1, fp2 = fp_min, fp_max
            d1, d2 = det_min, det_max

            A = (d1 - d2)/(fp1 - fp2)
            B = (d1*fp2 - d2*fp1)/(fp2 - fp1)
            bw0 = 1/A
            fr0 = B/(-2*A)

        return cls.make(
                fpumps,
                ppumps,
                dets, fr0, bw0)

    @classmethod
    def from_ranges_detuning(
            cls,
            detuning_rng,
            ppump_rng,
            fr0, bw0):
        """Calculates the sweep parameters from the normalized offset `detuning`.

        Args:
            detuning_rng:  Detuning range (min, max, step).  Detuning is
                defined by ``d = (2/bw)*(fp/2 - fr)``
            ppump_rng:  Pump ranges (min, max, step)
            fr0: Passive resonance frequency.
            bw0: Passive bandwidth.

        """
        dr = detuning_rng
        fpstart = cls.det2fp(dr[0], fr0, bw0)
        fpstop = cls.det2fp(dr[1], fr0, bw0)

        fpfirst = cls.det2fp(dr[0] + dr[2], fr0, bw0)
        fpstep = fpfirst - fpstart
        fprng = (fpstart, fpstop, fpstep)
        return cls.from_ranges(fprng, ppump_rng, dr, fr0, bw0)

    @staticmethod
    def detuning(fp, fr, bw):
        """Normalized detuning value."""
        gamma = bw/2
        return (fp/2 - fr)/gamma

    @staticmethod
    def det2fp(det, fr, bw):
        """Return detuning given fpump and other params"""
        gamma = bw/2
        return (gamma*det + fr)*2


class ParaMapRecord(MeasurementModel):
    det: float = quantity(un.nounit, "Detuning")
    fp: float = quantity(un.hertz, "PumpFrequency")
    pp: float = quantity(un.dBm, "PumpPower")
    ib: float = quantity(un.ampere, "BiasCurrent")
    fr: float = quantity(un.hertz, "ResonanceFrequency")
    bw: float = quantity(un.hertz, "ResonanceBandwidth")
    fo1: float = quantity(un.hertz, "FrequencyOffset1")
    fo2: float = quantity(un.hertz, "FrequencyOffset2")
    g1: float = quantity(un.dB, "Gain@F1")
    g2: float = quantity(un.dB, "Gain@F2")
    g1m: float = quantity(
            un.dB, "Gain@F1m", description="Gain at mirror frequency of f1")
    g2m: float = quantity(
            un.dB, "Gain@F2m", description="Gain at mirror frequency of f2")
    ps: float = parameter(un.dBm, "SignalPower")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


@declarative
class ParaMapSettings:
    version: str = setting(
            nicename="Script Version",
            description="Version of the script used during measurement.")
    resonance_frequency: float = setting(
            nicename="Resonance Frequency",
            description="Resonance frequency (Pump off).")
    ib_set: t.Optional[t.Iterable] = setting(
            nicename="Current Setpoints",
            description="The bias current in ampere.")
    detuning_rng: t.Tuple[float, float, float] = setting(
            nicename="Detuning Range",
            description="The frequency detunings.")
    ppump_rng: t.Tuple[float, float, float] = setting(
            nicename="Pump Power Range",
            description="Pump powers.")
    frequency_offset_1: float = setting(
            nicename="Frequency Offset 1",
            description="First frequency offset to measure gain at. "
                        "Offset is defined as `delta=fp/2-fs`.")
    frequency_offset_2: float = setting(
            nicename="Frequency Offset 2",
            description="Second frequency offset to measure gain at.")
    sample_count: int = setting(
            nicename="Sample Count",
            description="Number of samples to collect statistics from.")
    vna_power: float = setting(
            nicename="VNA Power",
            description="The output power for the VNA.")
    vna_ifbw: float = setting(
            nicename="VNA IF Bandwidth",
            description="The IF bandwidth setting for the VNA.")
    vna_navg: int = setting(
            nicename="VNA Average Count",
            description="Average count setting for the VNA.",
            default=1)
    vna_res_power: float = setting(
            nicename="Resonance VNA Power",
            description="VNA power to use when doing resonance measurement.")
    vna_res_ifbw: float = setting(
            nicename="Resonance VNA IF Bandwidth",
            description="VNA ifbw to use when doing resonance measurement.")
    vna_res_span: float = setting(
            nicename="Resonance VNA Span",
            description="VNA span to use when doing resonance measurement.")
    vna_res_sweep_step: float = setting(
            nicename="Resonance VNA Sweep Step",
            description="VNA sweep step to use when doing resonance "
                        "measurement.")
    do_cal: bool = setting(
            nicename="Perform Calibration",
            description="If enabled, the JPA is tuned to the provided "
                        "calibration bias and a transmission measurement is "
                        "performed to be used as baseline.")
    ib_cal: t.Optional[float] = setting(
            nicename="Calibration Current",
            description="Bias current to be used when calibration "
                        "is performed.  If not given, and if calibration is "
                        "enabled, bias current is set to 10 uA higher than "
                        "it's current value.")
    vna_bl_power: t.Optional[float] = setting(
            nicename="Baseline VNA Power",
            description="VNA power to be used when doing baseline "
                        "measurement.")
    vna_bl_fstart: t.Optional[float] = setting(
            nicename="Baseline VNA Start Frequency",
            description="Start frequency setting for the baseline measurement "
                        "using VNA.")
    vna_bl_fstop: t.Optional[float] = setting(
            nicename="Baseline VNA Stop Frequency",
            description="Stop frequency setting for the baseline measurement "
                        "using VNA.")
    skip_mirror: bool = setting(
            nicename="Skip Mirror",
            description="Whether or not to skip the measurements at the "
                        "mirror frequencies.")
    sweep_order: str = setting(
            nicename="Sweep Order",
            default='PUMPFIRST',
            description="Order of sweep.  One of ['PUMPFIRST', 'DETFIRST']")
    delay_ppump: float = setting(
            un.second,
            default=None,
            nicename="PumpPowerChangeDelay",
            description="Delay after changing pump power in seconds.")
    delay_fpump: float = setting(
            un.second,
            default=None,
            nicename="PumpFreqChangeDelay",
            description="Delay after changing pump frequency in seconds.")
    delay_ib: float = setting(
            un.second,
            default=None,
            nicename="CoilCurrentChangeDelay",
            description="Delay after changing coil current in seconds.")


@declarative
class ParaMapController:
    """Controller object for the paramap measurement.

    Important measurement parameters are:
        fo1:  The first offset frequency to measure the gain at.
        fo2:  The second offset frequency to measure the gain at.
        nsample:  Number of independent measurements performed at each
            frequency.
        vnaset:  The VNASettings to use when doing gain measurements.  The
            ifbw, navg and power settings are crucial.
        do_cal:  Whether or not to do baseline measurement for calibration.
        ib_cal:  If `do_cal`, sets the bias current to this value for measuring
        baseline.
        vnaset_baseline:  The VNASettings to use when measuring baseline.

    """
    c: JPAController
    mio: MIOBase

    fo1: float
    fo2: float

    nsample: int

    # power, ifbw and navg must be filled
    vnaset: VNASettings
    vnaset_resonance: VNASettings

    # Sweeping
    detuning_rng: t.Tuple[float, float, float]
    ppump_rng: t.Tuple[float, float, float]
    ib_set: t.Optional[t.Iterable] = None

    skip_mirror: bool = False
    do_cal: bool = False
    ib_cal: t.Optional[float] = None
    vnaset_baseline: t.Optional[VNASettings] = None
    pumpfirst: bool = True

    # Delays
    delay_ppump: t.Optional[float] = None
    delay_fpump: t.Optional[float] = None
    delay_ib: t.Optional[float] = None

    _suffix: t.Optional[str] = None

    _ib_cal_rel: float = -40e-6
    _wp0: WorkingPoint = None
    _bl: t.Optional[SParam1PModel] = None

    _initial_vnaset: t.Optional[VNASettings] = None

    _bootstrap_resonance_maxtrial: int = 10
    _ibnow: t.Optional[float] = None

    # maximum allowed fractional stderr (stderr/fr)
    _frfit_max_fstderr: float = 0.5e-3

    # expected q-factor, rough for resonance est.
    _q_exp: float = 300

    @property
    def coil_current(self):
        return self._ibnow

    def get_fpeak(self):
        return self.c.query_pump_frequency()/2

    def initiate(self):
        logger.info("ParaMap initiated")
        c = self.c
        if not c.initiated:
            c.clean_vna = True
            c.initiate()

        self._initial_vnaset = c.query_vna_settings()

        if self.vnaset.power is None:
            spow = c.vna.query_source_power_level()
            self.vnaset.power = spow
        # c.turn_on_vna_calibration("CH1_CALREG")
        c.turn_off_vna_calibration()

        wp0 = self.get_working_point()
        self._wp0 = wp0
        self._ibnow = wp0.ib

        if self.do_cal:
            self.measure_baseline()

    def measure_baseline(self):
        c = self.c
        ib_cal = self.ib_cal
        if ib_cal is None:
            ib_cal = self._wp0.ib + self._ib_cal_rel

        c.deactivate_pump()
        c.activate_signal()
        logger.info("Setting bias for baseline measurement.")
        c.ramp_current(ib_cal)
        c.do_vna_preset(self.vnaset_baseline)
        logger.info("Performing baseline measurement.")
        tr = c.measure_transmission()
        c.do_vna_autoscale()
        logger.info("Setting bias back to initial value.")
        c.ramp_current(self._wp0.ib)

        self._bl = tr
        return tr

    def measure_gain_at(self, f):
        vnaset = self.vnaset.evolve(
                center_frequency=f)

        c = self.c

        vnaset.span = 0
        vnaset.npoints = self.nsample
        c.do_vna_preset(vnaset)
        tr = c.measure_transmission()
        c.do_vna_autoscale()

        val = np.mean(tr.mags)
        val_std = np.std(tr.mags)

        if self._bl is not None:
            bl = self._bl
            val = remove_baseline(f, val, bl.frequencies, bl.mags)

        return val, val_std

    def get_working_point(self):
        c = self.c
        ib = c.query_current_setpoint()
        fp = c.query_pump_frequency()
        pp = c.query_pump_power()
        return WorkingPoint(fp=fp, ib=ib, pp=pp)

    def _state_txt(self, det, fp, pp):
        deltatxt = f"det={det:.2f}"
        fptxt = f"fp={fp/1e9:.6f} GHz"
        pptxt = f"pp={pp:.2f} dBm"
        return "|".join([deltatxt, fptxt, pptxt])

    def get_mref(self, mref: str):
        return f"{mref}-{self._suffix}"

    def estimate_resonance(self, sppm: SParam1PModel, fhint=None):
        kwargs = {}
        if self._q_exp is not None:
            # rough estimate bandwidth using first measurement frequency.
            bw0 = sppm.frequencies[0]/self._q_exp
            kwargs['bw'] = bw0

        if fhint is not None:
            kwargs['fr'] = fhint

        pars = phasemodel.guess(sppm.uphases, sppm.frequencies, **kwargs)
        fit = phasemodel.fit(sppm.uphases, f=sppm.frequencies, params=pars)
        logger.debug(fit.fit_report())
        bp = fit.params
        # If fit fails stderr is None.
        fr, frerr = bp['fr'].value, bp['fr'].stderr
        return fr, frerr

    def _bootstrap_resonance(self, assumeresonance: bool = True):
        logger.info("Starting resonance bootstrap.")
        ntrial = self._bootstrap_resonance_maxtrial
        c = self.c
        c.deactivate_pump()

        if assumeresonance:
            fhint = self._initial_vnaset.center_frequency
        else:
            start_freq = self.vnaset_baseline.get_start_frequency()
            stop_freq = self.vnaset_baseline.get_stop_frequency()
            logger.debug(f"Coarse resonance Start/Stop: "
                         f"{start_freq/1e9:.6f}, {stop_freq/1e9:.6f} GHz")

            c.do_vna_preset(self.vnaset_resonance.evolve(
                start_frequency=start_freq,
                stop_frequency=stop_freq,
                center_frequency=None, span=None,
                if_bandwidth=2e3, power=-5, sweep_step=0.1e6))

            for i in range(1, ntrial+1):
                tr = c.measure_transmission()
                c.do_vna_autoscale()

                trcal = remove_baseline_sppm(tr, self._bl)

                bw_exp = np.mean((start_freq, stop_freq))/self._q_exp
                fhint = resonance_from_phase2(trcal, bw_exp)

                fhint, fhinterr = self.estimate_resonance(trcal, fhint)
                if fhint is None:
                    logger.info(f"Resonance was not found ({i}/{ntrial}).")
                else:
                    logger.info(f"Zooming to resonance at {fhint/1e9:.6f} "
                                "GHz.")
                    break

        if (fhint is None) or (fhinterr/fhint >= self._frfit_max_fstderr):
            logger.info("Resonance bootstrap failed.")
            return None

        # We got the hint now do narrow search
        c.do_vna_preset(self.vnaset_resonance.evolve(
            center_frequency=fhint))

        fr, bw = None, None
        for i in range(1, ntrial+1):
            tr = c.measure_transmission()
            c.do_vna_autoscale()
            trcal = remove_baseline_sppm(tr, self._bl)
            pars = phasemodel.guess(trcal.uphases, trcal.frequencies)
            fit = phasemodel.fit(
                    trcal.uphases, f=trcal.frequencies, params=pars)
            logger.debug(fit.fit_report())
            bp = fit.params
            if bp['fr'].stderr is None:
                logger.debug("Resonance not found.  stderr is None.")
                fr = None
            elif (bp['fr'].stderr/bp['fr'].value >= self._frfit_max_fstderr):
                # Resonance not found
                if bp['fr'].stderr is not None:
                    fstderr = bp['fr'].stderr/bp['fr'].value
                    logger.debug(f"Fractional error: {fstderr:.3f}//Limit: "
                                 f"{self._frfit_max_fstderr:.3f}")
                fr = None
            else:
                # Resonance found
                fr, bw = bp['fr'].value, bp['bw'].value

            if fr is None:
                logger.info(f"Resonance was not found ({i}/{ntrial}).")
            else:
                logger.info(f"Resonance found at {fr/1e9:.6f} "
                            "GHz.")
                tr.mref = self.get_mref("RES")
                self.mio.write(tr)
                break
        return fr, bw

    def bootstrap_resonance(self, assumeresonance):
        c = self.c
        try:
            bres = self._bootstrap_resonance(assumeresonance)
        except ZeroDivisionError as e:
            logger.debug(f"ZeroDivisionError({e!s})")
            c.vna.write("*RST")
            c.setup_vna(clean=True)
            bres = self._bootstrap_resonance(assumeresonance)
        except IndexError as e:
            logger.debug(f"IndexError({e!s})")
            c.vna.write("*RST")
            c.setup_vna(clean=True)
            bres = self._bootstrap_resonance(assumeresonance)

        if (bres is None) or (any(bres) is None):
            raise ResonanceNotFoundError("Resonance bootstrap failed.")
        fr, bw = bres[0], bres[1]

        return fr, bw

    def do_measurement(self, fpeak):
        """Does measurements calculating to offsets referenced by the frequency
        given by fpeak."""
        fo1 = self.fo1
        fo2 = self.fo2

        f1 = fpeak + fo1
        f2 = fpeak + fo2
        f1m = fpeak - fo1
        f2m = fpeak - fo2
        g1, g1std = self.measure_gain_at(f1)
        g2, g2std = self.measure_gain_at(f2)
        if self.skip_mirror:
            g1m, g1mstd = np.nan, np.nan
            g2m, g2mstd = np.nan, np.nan
        else:
            g1m, g1mstd = self.measure_gain_at(f1m)
            g2m, g2mstd = self.measure_gain_at(f2m)

        return g1, g2, g1m, g2m

    def do_sweep_pumpfirst(self, ib: t.Optional[float] = None):
        c = self.c
        fo1 = self.fo1
        fo2 = self.fo2

        psig = self.vnaset.power

        if ib is not None:
            c.ramp_current(ib)
            self.sleep(self.delay_ib)
            assumeresonance = False
        else:
            assumeresonance = True

        fr0, bw0 = self.bootstrap_resonance(assumeresonance)

        sp = ParaMapSweepParameters.from_ranges_detuning(
                self.detuning_rng, self.ppump_rng, fr0=fr0, bw0=bw0)

        c.activate_pump()

        logger.info("Starting 'PUMPFIRST' sweep.")
        for det, fp in zip(sp.detunings, sp.fpumps):
            c.set_pump_frequency(fp)
            self.sleep(self.delay_fpump)
            for pp in sp.ppumps:
                c.set_pump_power(pp)
                self.sleep(self.delay_ppump)

                logger.info(self._state_txt(det, fp, pp))

                # Assume 3WM
                fpeak = fp/2

                g1, g2, g1m, g2m = self.do_measurement(fpeak)
                timestamp = datetime.now()

                record = ParaMapRecord(
                        det, fp, pp, ib, fr0, bw0, fo1, fo2, g1, g2, g1m, g2m,
                        psig, timestamp=timestamp)
                logger.info(record.pretty())

                record.mref = self.get_mref("EXP")
                self.mio.append(record)

    def do_sweep_detfirst(self, ib: t.Optional[float] = None):
        c = self.c
        fo1 = self.fo1
        fo2 = self.fo2

        psig = self.vnaset.power

        if ib is not None:
            c.ramp_current(ib)
            self.sleep(self.delay_ib)
            assumeresonance = False
        else:
            assumeresonance = True

        fr0, bw0 = self.bootstrap_resonance(assumeresonance)

        sp = ParaMapSweepParameters.from_ranges_detuning(
                self.detuning_rng, self.ppump_rng, fr0=fr0, bw0=bw0)

        c.activate_pump()

        logger.info("Starting 'DETFIRST' sweep.")
        for pp in sp.ppumps:
            c.set_pump_power(pp)
            self.sleep(self.delay_ppump)
            for det, fp in zip(sp.detunings, sp.fpumps):
                c.set_pump_frequency(fp)
                self.sleep(self.delay_fpump)
                logger.info(self._state_txt(det, fp, pp))

                # Assume 3WM
                fpeak = fp/2

                g1, g2, g1m, g2m = self.do_measurement(fpeak)
                timestamp = datetime.now()

                record = ParaMapRecord(
                        det, fp, pp, ib, fr0, bw0, fo1, fo2, g1, g2, g1m, g2m,
                        psig, timestamp=timestamp)
                logger.info(record.pretty())

                record.mref = self.get_mref("EXP")
                self.mio.append(record)

    def do_sweep(
            self, ib: t.Optional[float] = None):
        if self.pumpfirst:
            return self.do_sweep_pumpfirst(ib)
        else:
            return self.do_sweep_detfirst(ib)

    def run(self):
        self._bootstrap_resonance
        if self.ib_set is None:
            self.do_sweep()
        else:
            for i, ib in enumerate(self.ib_set):
                self._suffix = f'i{i:d}'
                try:
                    self.do_sweep(ib)
                except ResonanceNotFoundError:
                    logger.info(f"Skipping ib={_mp(ib,'A')}")
                    continue

    def sleep(self, seconds: t.Optional[float]):
        if seconds is not None:
            logger.debug(f"Sleeping for {seconds:.3f} s.")
            time.sleep(seconds)

    def finish(self):
        self.mio.close()
        self.c.deactivate_pump()
        logger.info("ParaMap finished.")

    @classmethod
    def make(cls, *args, **kwargs):
        return cls(*args, **kwargs)

    @classmethod
    def from_settings(
            cls,
            jpacontroller,
            mio,
            settings: ParaMapSettings
            ):
        s = settings
        return cls.make(
                jpacontroller, mio,
                fo1=s.frequency_offset_1,
                fo2=s.frequency_offset_2,
                nsample=s.sample_count,
                vnaset=VNASettings(
                    power=s.vna_power,
                    if_bandwidth=s.vna_ifbw,
                    naverage=s.vna_navg
                    ),
                vnaset_resonance=VNASettings(
                    power=s.vna_res_power,
                    if_bandwidth=s.vna_res_ifbw,
                    span=s.vna_res_span,
                    sweep_step=s.vna_res_sweep_step
                    ),
                detuning_rng=s.detuning_rng,
                ppump_rng=s.ppump_rng,
                ib_set=s.ib_set,
                skip_mirror=s.skip_mirror,
                do_cal=s.do_cal,
                ib_cal=s.ib_cal,
                vnaset_baseline=VNASettings(
                    power=s.vna_bl_power,
                    start_frequency=s.vna_bl_fstart,
                    stop_frequency=s.vna_bl_fstop,
                    if_bandwidth=1e3,
                    sweep_step=1e6),
                pumpfirst=s.sweep_order == 'PUMPFIRST',
                delay_ppump=s.delay_ppump,
                delay_fpump=s.delay_fpump,
                delay_ib=s.delay_ib
                )


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def setup_parser():
    def slicearg(s: str) -> t.Optional[int]:
        if s.upper().startswith('N'):
            return None
        else:
            return int(s)

    parser = argparse.ArgumentParser(
            description=(
                "This script performs measurements to reveal the parametric "
                "amplification parameters for a specific flux bias."
                ))

    parser.add_argument(
            'path',
            type=Path,
            help="Measurement folder.")

    parser.add_argument(
            '--ib-csv', '-i',
            type=Path,
            default=None,
            help="A csv file with a column named ib")

    parser.add_argument(
            '--dry',
            action='store_const',
            const=True,
            default=False,
            help="Perform a dry run: print settings and sweep parameters and "
                 "quit.")

    parser.add_argument(
            '-d', '--detuning',
            type=float,
            nargs=3,
            default=(-2, 2, 0.1),
            help="""Range for detuning in the form MIN, MAX, STEP. Defaults to
            (-2, 2, 1).""")

    parser.add_argument(
            '-p', '--pump-power',
            type=float,
            nargs=3,
            default=(-20, 19, 0.2),
            help="""Range for pump powers in the form MIN, MAX, STEP. Defaults
            to (-20, 19, 0.2).""")

    parser.add_argument(
            '-b', '--baseline_current', type=float,
            help="""Current to apply when measuring the baseline.""")

    parser.add_argument(
            '-O', '--sweep-order', type=str, default='PUMPFIRST',
            choices=['PUMPFIRST', 'DETFIRST'])
    parser.add_argument(
            '-DP', '--delay-ppump', type=float, default=None,
            help="Delay after changing pump power in seconds.")
    parser.add_argument(
            '-DF', '--delay-fpump', type=float, default=None,
            help="Delay after changing pump frequency in seconds.")
    parser.add_argument(
            '-DI', '--delay-ib', type=float, default=None,
            help="Delay after changing coil current in seconds.")
    return parser


def main(args):
    path = args.path
    ibcsv = args.ib_csv

    if ibcsv is not None:
        ibdf = pd.read_csv(ibcsv)
        ib_set = list(ibdf.ib.values)
    else:
        ib_set = None

    if ib_set is None:
        # manual override for quick testing
        ib_set = list(map(
                lambda l: l*1e-6,
                [-62.53737]))

    settings = ParaMapSettings(
            version=__script_version__,
            ib_set=ib_set,
            detuning_rng=args.detuning,
            ppump_rng=args.pump_power,
            skip_mirror=True,
            frequency_offset_1=1e3,
            frequency_offset_2=100e3,
            sample_count=2,
            vna_power=-20,
            vna_ifbw=50,
            vna_navg=1,
            vna_res_span=100e6,
            vna_res_ifbw=100,
            vna_res_power=-10,
            vna_res_sweep_step=1e6,
            do_cal=True,
            ib_cal=args.baseline_current,
            vna_bl_power=10,
            vna_bl_fstart=5e9,
            vna_bl_fstop=6.1e9,
            sweep_order=args.sweep_order,
            delay_fpump=args.delay_fpump,
            delay_ppump=args.delay_ppump,
            delay_ib=args.delay_ib,
            )

    if args.dry:
        logger.info(settings)
        to_json(settings, sys.stdout, True)
        sys.exit(0)

    if path.exists():
        logger.info(f"'{path}' exists! Aborting.")
        sys.exit(42)
    else:
        path.mkdir()

    mio = MIOFileCSV.open(path, mode='w')

    create_log(path / "groundcontrol.log", INFO)

    to_json(settings, path / 'settings.json', True)

    uris = get_uris()

    c = JPAController.make(resources=uris)
    c.verbose = False
    c.pump_ramp_rate = 10
    c.pump_step = 1
    c.current_ramp_rate = 10e-6
    c.current_step = 1e-6

    # IMPORTANT
    c.cs_preset.range_i = 1e-4

    expcontrol = ParaMapController.from_settings(
            c, mio, settings)

    # IMPORTANT, EXPECTED PASSIVE Q
    expcontrol._q_exp = 300

    try:
        expcontrol.initiate()
        expcontrol.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    except ResonanceNotFoundError:
        logger.info("Resonance was not found.  Aborting.")
    finally:
        expcontrol.finish()
        logger.info(f"Saving script to '{path}'")
        save_script(path)
    logger.info(f"{__measurement_name__} MEASUREMENT SCRIPT FINISHED")
    return True


if __name__ == "__main__":
    parser = setup_parser()
    args = parser.parse_args()

    main(args)

