"""Simple state controller for JPAs"""

import argparse
import typing as t
import os
import string

from groundcontrol import __version__
from groundcontrol.logging import setup_logger, \
        set_stdout_loglevel, DEBUG
from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.declarators import setting, declarative
import groundcontrol.units as un
from groundcontrol.friendly import mprefix_str
from groundcontrol.resources import get_uris


__script_version__ = "0.1"
__measurement_name__ = "jpastate"

logger = setup_logger(__measurement_name__)
logger.info(f"groundcontrol version: {__version__}")

isdebug = True
try:
    _ = os.environ['DEBUG']
    # if debug
    set_stdout_loglevel(DEBUG)
except KeyError:
    isdebug = False


@declarative
class ProgramSettings:
    ib: t.Optional[float] = setting(un.ampere, 'Bias Current')
    fp: t.Optional[float] = setting(un.hertz, 'Pump Frequency')
    pp: t.Optional[float] = setting(un.dBm, 'Pump Power')
    tns: t.Optional[float] = setting(un.kelvin, 'Noise Source Temperature')

    _CURRENTLIMIT: float = 1e-3
    _TNSLIMIT: float = 1.2

    @tns.validator
    @ib.validator
    def _current_lim(self, attrib, value):
        if attrib.name == 'ib':
            limit = self._CURRENTLIMIT
        elif attrib.name == 'tns':
            limit = self._TNSLIMIT
        unit = attrib.metadata['unit']
        if (value is not None) and (abs(value) > limit):
            limstr = mprefix_str(limit, unit)
            nname = string.capwords(attrib.metadata['nicename'], '. ')
            msg = f"{nname} must be less than {limstr}"
            raise ValueError(msg)

    @classmethod
    def from_cmdline_args(cls, parsed=None):
        if parsed is None:
            parser = cls.make_parser()
            parsed = parser.parse_args()

        args = []
        args.extend([
            parsed.ib,
            parsed.fp,
            parsed.pp,
            parsed.tns
            ])

        return cls(*args)

    @staticmethod
    def make_parser(parser: argparse.ArgumentParser = None):
        if parser is None:
            parser = argparse.ArgumentParser()

        parser.add_argument(
                '-i', '--bias-current',
                type=float,
                help="Current bias in Ampere.",
                dest='ib'
                )
        parser.add_argument(
                '-f', '--pump-frequency',
                type=float,
                help="Pump frequency in Hz.",
                dest='fp'
                )
        parser.add_argument(
                '-p', '--pump-power',
                type=float,
                help="Pump power in dBm.",
                dest='pp'
                )
        parser.add_argument(
                '-t', '--nsource-temperature',
                type=float,
                help="Noise source temperature in K.",
                dest='tns'
                )

        return parser


@declarative
class MainController:
    controller: RackController
    current_ramp_rate: float = 10e-6
    current_step: float = 1e-6

    def initiate(self):
        c = self.controller
        c.current_ramp_rate = self.current_ramp_rate
        c.current_step = self.current_step

        c.cs_preset.range_i = 1e-3
        if not c.initiated:
            c.initiate()

    def set_ib(self, value):
        c = self.controller
        c.ramp_current(value)
        c: RackController

    def query_ib(self):
        c = self.controller
        return c.query_current_setpoint()

    def set_fp(self, value):
        c = self.controller
        c.set_pump_frequency(value)

    def query_fp(self):
        c = self.controller
        return c.query_pump_frequency()

    def set_pp(self, value):
        c = self.controller
        c.set_pump_power(value)

    def query_pp(self):
        c = self.controller
        return c.query_pump_power()

    def set_tns(self, value):
        c = self.controller
        tc = c.tc
        htype = tc.HeaterType.SAMPLE
        hr = tc.query_heater_range(htype)
        if hr is tc.HeaterRange.OFF:
            # Set the smallest range to just "turn it on"
            tc.set_heater_range(htype, tc.HeaterRange.C31u6)
        c.ramp_noise_source_temperature(value, settle_delay=30,
                measurement_period=0.2, measurement_window=20)

    def query_tns(self):
        c = self.controller
        return c.query_noise_source_temperature()


@declarative
class MainProgram:
    mcontrol: MainController
    progsettings: ProgramSettings

    def run(self):
        ps = self.progsettings
        mc = self.mcontrol
        mc.initiate()

        if ps.ib is not None:
            _s = f"{mprefix_str(ps.ib, un.ampere)}"
            logger.info(f"Setting bias current to {_s}.")
            mc.set_ib(ps.ib)
            qib = mc.query_ib()
            logger.info(f"Queried ib: {mprefix_str(qib, un.ampere)}")

        if ps.fp is not None:
            _s = f"{mprefix_str(ps.fp, un.hertz)}"
            logger.info(f"Setting pump frequency to {_s}.")
            mc.set_fp(ps.fp)
            qfp = mc.query_fp()
            logger.info(f"Queried fp: {mprefix_str(qfp, un.hertz)}")

        if ps.pp is not None:
            logger.info(f"Setting pump power to {mprefix_str(ps.pp, un.dBm)}.")
            mc.set_pp(ps.pp)
            qpp = mc.query_pp()
            # THERE IS AN ISSUE QUERYING POWER
            qpp = ps.pp
            logger.info(f"Queried pp(NOT REAL): {mprefix_str(qpp, un.dBm)}")
        
        print(ps.tns)
        if ps.tns is not None:
            _s = f"{mprefix_str(ps.tns, un.kelvin)}"
            logger.info(f"Setting noise source temperature to {_s}")
            mc.set_tns(ps.tns)
            qtns = mc.query_tns()
            logger.info(f"Queried tns: {mprefix_str(qtns, un.kelvin)}")

    @classmethod
    def make(cls):
        parser = ProgramSettings.make_parser()

        parsed = parser.parse_args()
        progset = ProgramSettings.from_cmdline_args(parsed)

        uris = get_uris()
        c = RackController.make(resources=uris)
        c.verbose = False
        c.noise_source_control_channel = "02"
        c.temperature_setpoint_max = 1.2

        mc = MainController(c)

        return cls(mc, progset)


def main():
    mp = MainProgram.make()
    mp.run()


if __name__ == "__main__":
    main()
