"""Quantum ( :) ) calibration script."""
import typing as t
from pathlib import Path
import os
import sys
import time
from shutil import copyfile
import json
from datetime import datetime
from collections import deque

from datargs import argsclass, arg
from dataclasses import asdict
import attr
from datargs import parse as parsearg
import numpy as np

import groundcontrol
from groundcontrol import __version__
from groundcontrol.controllers.jpa import JPAController as RackController
from groundcontrol.declarators import declarative
from groundcontrol.settings import SASettings
from groundcontrol.logging import (
        setup_logger, create_log, INFO,
        DEBUG, set_stdout_loglevel)
from groundcontrol.measurement import (
        MeasurementModel, quantity, parameter)
import groundcontrol.units as un
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.resources import get_uris
from groundcontrol.friendly import mprefix_str as _mstr
from groundcontrol.helper import default_serializer
from groundcontrol.util import within

groundcontrol.USE_UNICODE = False
__script_version__ = "0.1"
__measurement_name__ = "QuantumCalibration"

logger = setup_logger("quantcal")

isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


@argsclass
class ProgramSettings:
    path: Path = arg(True)
    heat_range: float = arg(
            nargs=3, metavar=tuple("START STOP STEP".split()),
            aliases=['-H'])
    frequency: float
    channel: str
    rbw: float = arg(
            default=1e6,
            help='Resolution bandwidth in Hz.  Defaults to 1 MHz.')
    vbw: float = arg(
            default=None,
            help='Video bandwidth in Hz.  Defaults to 1/1000 of RBW.')
    navg: float = 1
    heat_period: float = arg(
            default=10,
            help='Period to wait while applying heat before the measurement.')
    measurement_period: float = arg(
            default=1,
            help="Measurement period in seconds.")
    heater_resistance: float = arg(
            default=100,
            help="Heater resistance in ohm.  Defaults to 100 ohm.")
    auto_excitation: bool = arg(
            default=False,
            help="Automatically adjusts the voltage excitation based on the "
                 "resistance values.  Note that the adjustment thresholds are "
                 "hardcoded to the script.")

    def save(self, fp):
        dc = {k: default_serializer(v, False)
              for k, v in asdict(self).items()}
        if isinstance(fp, Path) or isinstance(fp, str):
            with open(fp, 'w') as fl:
                json.dump(dc, fl, indent=4)
        else:
            json.dump(dc, fp, indent=4)


class QCalRecord(MeasurementModel):
    time: float = quantity(un.second)
    heat_power: float = quantity(un.watt)
    resistance: float = quantity(un.ohm)
    power: float = quantity(un.dBm)

    frequency: float = parameter(un.hertz)
    heat_period: float = parameter(un.second)
    measurement_period: float = parameter(un.second)


class ResistanceRecord(MeasurementModel):
    resistance: float = quantity(un.ohm)
    timestamp: datetime = parameter(
        un.nounit, "Timestamp",
        default=attr.Factory(datetime.now))


@declarative
class MainController:
    rc: RackController
    mio: MIOFileCSV

    heat_powers: t.List[float]

    measurement_period_s: float
    heat_period_s: float

    saset: SASettings

    htr_resistance: float
    auto_excitation: bool
    t_channel: t.Union[str, int]
    max_heat_w: float = 100e-6

    def initiate(self):
        rc = self.rc
        rc.initiate()
        tc = self.rc.tc
        tc.set_output_mode(
                tc.HeaterType.SAMPLE,
                tc.OutputMode.OPENLOOP,
                tc.Channel.parse(self.t_channel),
                tc.State.OFF,
                tc.OutputPolarity.UNIPOLAR,
                tc.State.OFF,
                0
                )

        tc.set_heater_setup(
                tc.HeaterType.SAMPLE,
                resistance=self.htr_resistance,
                out_display=tc.HeaterOutDisplayType.POWER)

        tc.set_heater_range(tc.HeaterType.SAMPLE, tc.HeaterRange.OFF)

        rc.deactivate_signal()
        rc.do_sa_preset(self.saset)

    def _tc_adjust_excitation(self, resistance):
        """Adjusts the voltage excitation based on the resistance value.

        Things here are currently hardcoded unfortunately.
        """
        tc = self.rc.tc
        ch = tc.Channel.parse(self.t_channel)
        mode, excitation, autorange, resrange, csshunt, units = tc.query_input_setup(ch)  # noqa: E501
        exc_old = excitation
        if mode == tc.CurrentExcitation:
            raise NotImplementedError("Current excitation is not defined yet.")
            return  # dont do anything if not set to voltage excitation
        if within(resistance, 3.2e3, 20e3):
            excitation = tc.VoltageExcitation.V6u32
        elif within(resistance, 3e3, 3.2e3):
            excitation = tc.VoltageExcitation.V20u0
        elif within(resistance, 2.2e3, 3e3):
            excitation = tc.VoltageExcitation.V63u2
        elif within(resistance, 0, 2.2e3):
            excitation = tc.VoltageExcitation.V200u

        # issue a change only if new excitation is to be different
        if exc_old != excitation:
            logger.info(f"Changing excitation from {exc_old.name} "
                        f"to {excitation.name}.")
            tc.set_input_setup(
                    ch, mode, excitation, autorange, resrange, csshunt, units)

    def query_resistance(self) -> float:
        tc = self.rc.tc
        channel = tc.Channel.parse(self.t_channel)
        res = tc.query_resistance_reading(channel)
        return res

    def set_heat_power(self, watt):
        if watt >= self.max_heat_w:
            raise ValueError(f"Maximum power is {self.max_heat} W ({watt})")
        tc = self.rc.tc
        bins = [rng for rng in tc.HeaterRange]
        res = self.htr_resistance
        binswatt = [res*currng.asfloat()**2 for currng in bins]
        rngind = np.digitize(watt, binswatt, right=True)

        # silently accept values bigger than max as max.
        if rngind == len(bins):
            rngind = rngind - 1

        rangesetting = bins[rngind]
        tc.set_heater_range(tc.HeaterType.SAMPLE, rangesetting)
        tc.set_manual_heater_out(tc.HeaterType.SAMPLE, watt)

    def query_heat_power(self) -> float:
        tc = self.rc.tc
        result = tc.query_manual_heater_out(tc.HeaterType.SAMPLE)
        rng = tc.query_heater_range(tc.HeaterType.SAMPLE).asfloat()
        return min(result, rng)  # if rng is 0, it will be returned.

    def sleep(self, seconds):
        return time.sleep(seconds)

    def measure_spectrum(self):
        rc = self.rc
        spectrum = rc.measure_spectrum()
        return spectrum

    def run(self):
        mio = self.mio
        heat_period = self.heat_period_s
        measurement_period = self.measurement_period_s

        resdeq = deque(maxlen=30)

        lasttime, timepassed = 0, 0
        for i, power in enumerate(self.heat_powers):
            logger.info(f"Applying {_mstr(power, un.watt)} power.")
            self.set_heat_power(power)
            logger.info(f"Waiting for {heat_period} s before "
                        "spectrum measurement.")

            timepassed = 0
            start = time.time()
            while (timepassed < heat_period):
                res = self.query_resistance()
                resdeq.append(res)
                resrec = ResistanceRecord(resistance=res)
                resrec.mref = 'DEBUG'
                mio.append(resrec)
                logger.info(f"Resistance: {res} ohm.")
                self.sleep(measurement_period)
                timepassed = time.time() - start

                if self.auto_excitation:
                    resmean = np.mean(resdeq)
                    self._tc_adjust_excitation(resmean)

            if i == 0:
                timepassed = 0
            else:
                timepassed = time.time() - lasttime
            lasttime = time.time()

            logger.info("Measuring spectrum.")
            spectrum = self.measure_spectrum()
            f = np.median(spectrum.frequencies)
            power = np.median(spectrum.values)
            logger.info("Measuring sensor resistance.")
            qcr = QCalRecord(
                    resistance=self.query_resistance(),
                    time=timepassed,
                    heat_power=self.query_heat_power(),
                    power=power,
                    frequency=f,
                    measurement_period=measurement_period,
                    heat_period=heat_period)
            logger.info(qcr.pretty())

            qcr.mref = "EXP"
            spectrum.mref = f'DEBUG-i{i}'
            mio.append(qcr)
            mio.write(spectrum)
            self.sleep(measurement_period)

    def finish(self):
        logger.info(f"Finishing {__measurement_name__}.")
        tc = self.rc.tc
        tc.set_heater_range(tc.HeaterType.SAMPLE, tc.HeaterRange.OFF)

    @classmethod
    def make(
            cls,
            settings: ProgramSettings,
            uris=None):
        if uris is None:
            uris = get_uris()
        rc = RackController.make(resources=uris)
        mio = MIOFileCSV.open(settings.path, mode='w')

        rbw = settings.rbw
        vbw = rbw/1000 if settings.vbw is None else settings.vbw

        saset = rc.sa_preset.evolve(
                rbw=settings.rbw,
                vbw=vbw,
                center_frequency=settings.frequency,
                span=0, npoints=1, naverage=settings.navg)
        heat_powers = np.arange(*settings.heat_range)

        return cls(
                rc=rc, mio=mio,
                heat_period_s=settings.heat_period,
                measurement_period_s=settings.measurement_period,
                saset=saset,
                htr_resistance=settings.heater_resistance,
                t_channel=settings.channel,
                heat_powers=heat_powers,
                auto_excitation=settings.auto_excitation)


def save_script(path: Path):
    """Saves this script to the given location."""
    fpath = Path(__file__)
    fname = fpath.name  # only the name of the file
    return copyfile(fpath, path / fname)


def main(settings: ProgramSettings):
    uris = get_uris()

    path = settings.path

    if path.exists():
        logger.error("There is a folder with the given name. Aborting.")
        sys.exit(42)
    path.mkdir()

    create_log(path / "groundcontrol.log", INFO)
    logger.info(f"groundcontrol version: {__version__}")
    logger.info(f"Script: {__file__} | Version: {__script_version__}")

    mc = MainController.make(
        uris=uris,
        settings=settings)

    mc.initiate()
    try:
        settings.save(path/'settings.json')
        mc.run()
    except KeyboardInterrupt:
        logger.info("Ctrl-c received. Aborting.")
    except Exception as e:
        logger.info("Unknown exception. Aborting.")
        logger.error(e)
        # LOG THIS TOO
        raise e
    finally:
        mc.finish()
        logger.info(f"Saving script to '{settings.path}'")
        save_script(settings.path)


if __name__ == "__main__":
    main(parsearg(ProgramSettings))
