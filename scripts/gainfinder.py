"""This is a script to find the resonance point of a JPA, given frequency
range.

It returns as soon as it finds a reasonable resonance frequency estimate.
"""
import typing as t

import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.logging import setup_logger
from groundcontrol.controllers import JPAController
from groundcontrol.settings import VNASettings
from groundcontrol.declarators import setting, settings, declarative
from groundcontrol.measurement import SParam1PModel, ScalarMeasurementModel
import groundcontrol.units as un
from groundcontrol.resources import get_uris
from groundcontrol.util import unwrap_deg, dbm2watt, watt2dbm, \
        db2lin_pow, lin2db_pow


__script_name__ = __file__
__script_version__ = "0.1"
logger = setup_logger("gainfinder")
logger.info(f"groundcontrol version: {__version__}")


def deembed(
        sppm: SParam1PModel,
        baseline: SParam1PModel):
    """Deembeds the baseline from gain measurement in sppm.
    Uses linear interpolation to approximate points.
    """
    # Measurement frequencies, baseline will be interpolated to these points
    fmeas = sppm.frequencies
    fb = baseline.frequencies
    bmags = baseline.mags
    bphases = baseline.phases

    mag_intrp = np.interp(fmeas, fb, bmags)
    phase_intrp = np.interp(fmeas, fb, bphases)

    magscal = sppm.mags - mag_intrp
    phasescal = sppm.phases - phase_intrp

    uphases = unwrap_deg(phasescal)
    return sppm.evolve(
            mags=magscal, phases=phasescal,
            uphases=uphases)


def _gainmeasurement(value, error) -> ScalarMeasurementModel:
    return ScalarMeasurementModel(
            value,
            error,
            un.dB,
            'Gain')


@declarative
class GainFinder:
    """For each current, for each pump power, pump frequency is swept to cover
    the given "gain frequency" range.

    Pump power is the outer loop since certain points may have no gain for any
    pump power, but it is very likely that there is a current bias that has
    gain for a given pump.  Given that one chooses a pump power that is not too
    far off.
    """
    jpac: JPAController

    currents: np.ndarray
    frequencies: np.ndarray
    ppumps: np.ndarray
    foffset: float
    threshold: float
    avgbaseline: bool = False

    _baseline: SParam1PModel = None
    _gmeas: ScalarMeasurementModel = None

    _current_i: int = 0
    _ppump_i: int = 0
    _freq_i: int = 0
    _steps_completed: int = 0
    _started: bool = False
    _tripped: bool = False

    @property
    def tripped(self):
        return self._tripped

    @property
    def state_text(self):
        cur = self.currents[self._current_i]
        ppump = self.ppumps[self._ppump_i]
        freq = self.frequencies[self._freq_i]

        ctxt = f"Current: {cur*1e6:.3f} uA"
        ptxt = f"Pump: {ppump:.2f} dBm"
        ftxt = f"Frequency: {freq/1e9:.6f} GHz"
        return f"{ctxt} | {ptxt} | {ftxt}"

    @property
    def last_gain(self):
        self._gmeas

    @property
    def last_current(self):
        return self.currents[self._current_i]

    @property
    def last_ppump(self):
        return self.ppumps[self._ppump_i]

    @property
    def last_freq(self):
        return self.frequencies[self._freq_i]

    @property
    def nsteps(self):
        ncur = len(self.currents)
        npow = len(self.ppumps)
        nfreqs = len(self.frequencies)
        ntot = ncur*npow*nfreqs
        return ntot

    @property
    def progress(self) -> float:
        """Progress as a value between 0 and 1."""
        return self._steps_completed/self.nsteps

    def initiate(self):
        logger.info("Initiating GainFinder")
        jpac = self.jpac
        jpac.deactivate_pump()
        jpac.activate_signal()
        baseline = self.measure_baseline()
        self._baseline = baseline
        jpac.activate_pump()

    def measure_baseline(self) -> SParam1PModel:
        jpac = self.jpac
        current = self.jpac.query_current_setpoint()
        meas = jpac.measure_baseline(preservesettings=True)
        if self.avgbaseline:
            # 10% percentile of the distance histogram for the 2nd current
            distances = np.abs(self.currents - current)
            dist = np.quantile(distances, 0.1)
            current2 = current + dist
            if current2 > self.currents.max():
                current2 -= 2*dist
            meas2 = jpac.measure_baseline(
                    bias_current=current2,
                    preservesettings=True)
            meas = meas.average(meas2)
        return meas

    def measure_gain(self, frequency) -> ScalarMeasurementModel:
        jpac = self.jpac
        f = frequency + self.foffset
        jpac.do_vna_preset(jpac.vna_preset.evolve(center_frequency=f))
        tr = jpac.measure_transmission()
        # gain spectrum
        gs = deembed(tr, self._baseline)

        gain, error = self.estimate_gain(gs)
        gmeas = _gainmeasurement(gain, error)
        return gmeas

    def estimate_gain(self, gspectrum: SParam1PModel) -> t.Tuple[float, float]:
        # NOTE assuming the window is very small
        mags = gspectrum.mags
        gain = np.mean(mags)
        # This is not the best estimate since mags is dB, but no need
        # to be very precise here.
        error = np.std(mags)
        return gain, error

    def stepiter(self):
        jpac = self.jpac
        self._started = True
        for i, cur in enumerate(self.currents):
            jpac.ramp_current(cur)
            self._current_i = i
            for j, pp in enumerate(self.ppumps):
                jpac.ramp_pump_power(pp)
                self._ppump_i = j
                for k, fr in enumerate(self.frequencies):
                    self._freq_i = k
                    jpac.set_pump_frequency(fr*2)
                    gmeas = self.measure_gain(fr)
                    self._gmeas = gmeas
                    trip = self.check_threshold(gmeas)
                    if trip:
                        self._tripped = True
                    self._steps_completed += 1
                    yield gmeas

    def check_threshold(self, gmeas) -> bool:
        # return ((gmeas.value-gmeas.error) > self.threshold)
        return (gmeas.value > self.threshold)

    def cleanup(self):
        self._started = False
        self._tripped = False
        self._current_i = 0
        self._ppump_i = 0
        self._freq_i = 0
        self._steps_completed = 0


@declarative
class MainProgram:
    current_range: t.Tuple[float, float] = setting(
            un.ampere, "Coil Current Range",
            "Start and stop values for current, both inclusive.(START, STOP)")

    current_points: int = setting(
            un.nounit, "Current Points",
            "Number of points to use when sweeping current.")

    frequency_window: t.Tuple[float, float] = setting(
            un.hertz, "Search Window",
            "Frequency boundaries to use when searching for resonance.  Right "
            "boundary is not inclusive.")

    frequency_step: float = setting(
            un.hertz, "Frequency Step",
            "Step size to use when doing frequency sweeps within the window.")

    pump_power: t.Union[float, t.Iterable[float]] = setting(
            un.dBm, "Pump Power",
            "A scalar or 1-d array of pump powers to be used when searching.")

    frequency_offset: float = setting(
            un.hertz, "Gain Measurement Frequency Offset",
            "Offset frequency from the peak point to measure the gain.")

    gain_preset: VNASettings = setting(
            un.nounit, "VNA Gain Preset",
            "Gain measurement Preset for VNA",
            default=VNASettings(
                power=-70,
                span=10, sweep_step=1, if_bandwidth=10))

    baseline_preset: VNASettings = setting(
            un.nounit, "VNA Baseline Preset",
            "Baseline measurement preset for VNA.",
            default=VNASettings(
                power=-30,
                sweep_step=1e6))

    gain_threshold: float = setting(
            un.dB, "Gain Threshold",
            "Gain threshold for stopping the search.")

    average_baseline: bool = setting(
            un.nounit, "Average Baseline Policy",
            "If `True`, the baseline will be measured for two current biases"
            "and the average of the two will be used.  If `False` baseline "
            "will be measured with the existing current setting on the "
            "device.",
            default=False)

    # NO NEED TO TOUCH THESE
    pump_ramp_rate: float = setting(un.dB_d_s, "Pump Ramp Rate", default=10)
    pump_step: float = setting(un.dB, "Pump Step", default=1)
    current_ramp_rate: float = setting(
            un.ampere_d_s, "Current Ramp Rate", default=5e-6)
    current_step: float = setting(un.ampere, "Current Step", default=0.2e-6)

    _gainfinder: t.Optional[GainFinder] = None
    _success: bool = False

    @property
    def gainfinder(self):
        return self._gainfinder

    def initiate(self, resources: t.Optional[t.Dict] = None):
        if resources is None:
            resources = get_uris()

        self._resources = resources
        jpac = JPAController.make(
                resources=resources,
                allow_not_found_instrument=True)

        jpac.pump_ramp_rate = self.pump_ramp_rate
        jpac.pump_step = self.pump_step
        jpac.current_ramp_rate = self.current_ramp_rate
        jpac.current_step = self.current_step
        # Shut the ramping up
        jpac.verbose = False

        jpac.baseline_settings = self.baseline_preset
        jpac.vna_preset = self.gain_preset
        jpac.initiate()

        c_min, c_max = self.current_range
        c_npoints = self.current_points
        currents = np.mgrid[c_min:c_max:c_npoints*1j]

        f_min, f_max = self.frequency_window
        f_step = self.frequency_step
        frequencies = np.mgrid[f_min:f_max:f_step]

        if isinstance(self.pump_power, float):
            ppump = [self.pump_power]
        else:
            ppump = self.pump_power

        foffset = self.frequency_offset
        avgbaseline = self.average_baseline

        gainfinder = GainFinder(
                jpac,
                currents,
                frequencies,
                ppump,
                foffset,
                self.gain_threshold,
                avgbaseline)

        self._gainfinder = gainfinder
        gainfinder.initiate()

    def run(self):
        gf = self.gainfinder
        logger.info("STARTING GAINFINDER")
        for istep, gmeas in enumerate(gf.stepiter()):
            #mod = (istep % (gf.nsteps//10000))
            if True: #mod == 0:
                logger.info(f"PROGRESS: {gf.progress*100:.1f}%")
                logger.info(f"\t{gf.state_text}")
                txt = f"\tG={gmeas.value:.2f} +-{gmeas.error:.2f} {gmeas.unit}"
                logger.info(txt)

            if gf.tripped:
                logger.info("GAIN OBSERVED")
                logger.info(f"\t{gf.state_text}")
                txt = f"\tG={gmeas.value:.2f} +-{gmeas.error:.2f} {gmeas.unit}"
                logger.info(txt)
                self._success = True
                break

    def finish(self):
        self.gainfinder.cleanup()
        if not self._success:
            logger.info("NO GAIN WAS OBSERVED :(")


def main():
    mp = MainProgram(
            current_range=(-70e-6, -10e-6),
            current_points=50,
            # frequency_window=(5.81735e9, 5.81735e9+0.1e6),
            frequency_window=(5.2e9, 5.8e9+0.1e6),
            frequency_step=0.1e6,
            # pump_power=np.arange(15, 19, 0.5),
            pump_power=np.arange(10, 13, 16),
            frequency_offset=-1e3,
            gain_preset=VNASettings(
                power=-20,
                span=10,
                sweep_step=1,
                if_bandwidth=400),
            baseline_preset=VNASettings(
                power=0,
                start_frequency=5.4e9,
                stop_frequency=6.1e9,
                sweep_step=1e6,
                if_bandwidth=1e3
                ),
            gain_threshold=4)
    try:
        mp.initiate()
        mp.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    finally:
        mp.finish()


if __name__ == "__main__":
    main()
