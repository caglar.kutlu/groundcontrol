"""Sweep flux bias current and measure resonance frequency.
"""
import typing as t
from pathlib import Path
from datetime import datetime
from shutil import copyfile
import argparse
import os

import attr
import numpy as np

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol.settings import InstrumentSettings, VNASettings
from groundcontrol.declarators import setting, declarative, quantity, parameter
from groundcontrol.measurement import MeasurementModel, SParam1PModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.logging import setup_logger, create_log, INFO,\
    set_stdout_loglevel, DEBUG
from groundcontrol.estimation import phasemodel, resonance_from_phase2
from groundcontrol.analysis.calibrate import remove_baseline_sppm
import groundcontrol.units as un
from groundcontrol.friendly import mprefix_str as _mstr
from groundcontrol.resources import get_uris
from groundcontrol.helper import Array


__measurement_name__ = "ResonanceMap"
__script_version__ = "0.3"


logger = setup_logger("resmap")
logger.info(f"groundcontrol version: {__version__}")
logger.info(f"{__measurement_name__}(v{__script_version__}) Started ")

isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


class ResonanceMap(MeasurementModel):
    current: Array[float] = quantity(un.ampere, "Coil Current")
    frequency: Array[float] = quantity(un.hertz, "Resonance Frequency")
    stepsize: float = parameter(un.ampere, "Current Step")
    completion: float = parameter(un.second, "Completion Time")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


class SweepParameters(InstrumentSettings):
    currents: np.ndarray = setting(un.ampere, "Flux Current")

    @classmethod
    def make(cls, currents):
        return cls(currents)

    @classmethod
    def from_ranges(
            cls,
            current_rng):
        """Creates sweep parameters from given ranges given in the format
        (start, stop, step), start and stop inclusive.  For numerical
        consistency, provide integer values.
        """
        current_min, current_max, current_step = current_rng

        currents = fullrange(current_min, current_max, current_step)

        return cls.make(
                currents)

    def to_table(self):
        args = (self.currents,)

        # indexing ij ensures matrix style indexing
        cols = map(lambda ar: ar.flatten(), np.meshgrid(*args, indexing='ij'))
        return np.vstack(list(cols)).T

    def to_csv(self, fname: str):
        header = ("Current [A]")
        np.savetxt(fname, self.to_table(), delimiter=',', header=header)


@declarative
class MainController:
    jpac: JPAController

    mio: MIOFileCSV

    sweep_params: SweepParameters = setting()

    wide_settings: VNASettings = setting(
            un.nounit,
            "Wide VNA Settings",
            description="VNA Settings to use when performing resonance search "
                        "for the whole window.",
            default=VNASettings(
                start_frequency=2.1e9,
                stop_frequency=2.35e9,
                sweep_step=100e3,
                power=-26,
                if_bandwidth=1e3))

    narrow_settings: VNASettings = setting(
            un.nounit,
            "Wide Settings",
            description="VNA Settings to use when performing gain "
                        "measurement at the offset",
            default=VNASettings(
                span=10e6, sweep_step=20e3, power=-30, if_bandwidth=100))

    record_wide: bool = setting(
            un.nounit, "Whether or not to record wide measurements",
            default=True)

    return_back: bool = setting(
            un.nounit, "Whether or not to return back to initial current.",
            default=True)
    assume_smooth: bool = setting(
            un.nounit, "Tries to skip wide measurement if last f_r is valid.",
            default=True)
    bl_current: t.Optional[float] = setting(
            un.ampere, "Current to measure baseline at.  If not provided, "
            "baseline calibration is skipped.", default=None)
    bl_power: t.Optional[float] = None

    last_current: t.Optional[float] = None
    q_expected: t.Optional[float] = setting(
            un.nounit, "Expected Q-factor of the passive resonance",
            default=None)

    baseline: t.Optional[SParam1PModel] = None

    _fr_fine: t.List = attr.ib(factory=list)
    _start_time: datetime = datetime.now()
    _end_time: datetime = datetime.now()
    _init_current: float = 0
    _frfit_max_fstderr: float = 0.5e-3  # df/f  when estimating resonance freq.

    def initiate(self):
        create_log(self.mio.path / "groundcontrol.log", INFO)
        self.sweep_params.to_csv(self.mio.path / "sweep_params.csv")
        self.jpac.activate_signal()
        self.jpac.deactivate_pump()
        self._init_current = self.jpac.cs.query_level_i()

        if max(self.sweep_params.currents) > 100e-6:
            logger.info("Setting current range to 1 mA")
            self.jpac.cs.set_range_i(1e-3)

    def measure_baseline(self) -> SParam1PModel:
        if self.bl_current is None:
            raise ValueError("Baseline current is not provided.")
        c = self.jpac
        self.last_current = c.query_current_setpoint()

        logger.info("Setting current for baseline.")
        c.ramp_current(self.bl_current)

        wideset = self.wide_settings
        narrowset = self.narrow_settings
        stngs = (self.wide_settings.evolve(
            start_frequency=wideset.start_frequency - narrowset.span,
            stop_frequency=wideset.stop_frequency + narrowset.span))

        if self.bl_power is not None:
            stngs = stngs.evolve(power=self.bl_power)

        sppm = self.measure_wide(stngs)
        return sppm

    def measure_wide(
            self,
            override_set: t.Optional[VNASettings] = None) -> SParam1PModel:
        if override_set is None:
            settings = self.wide_settings
        else:
            settings = override_set
        c = self.jpac
        c.do_vna_preset(settings)
        s21 = c.measure_transmission()
        return s21

    def measure_narrow(self, center: float) -> t.Optional[float]:
        c = self.jpac
        c.do_vna_preset(
                self.narrow_settings.evolve(center_frequency=center))
        s21 = c.measure_transmission()
        return s21

    def bootstrap(
            self, fhint=None, skipwide=False):
        bl = self.baseline
        doblcal = bl is not None

        if not skipwide:
            logger.info("PERFORMING WIDE SEARCH")
            wide = self.measure_wide()
            if doblcal:
                wide = remove_baseline_sppm(wide, bl)

            if fhint is None:
                start_freq = wide.frequencies[0]
                stop_freq = wide.frequencies[-1]
                bw_exp = np.mean((start_freq, stop_freq))/self.q_expected
                fhint = resonance_from_phase2(wide, bw_exp)

            fr, frerr = self.estimate_resonance(wide, fhint=fhint)

            if (frerr is None) or (frerr/fr >= self._frfit_max_fstderr):
                # Resonance not found
                if frerr is not None:
                    logger.debug(f"Fractional error: {frerr:.3f}//Limit: "
                                 f"{self._frfit_max_fstderr:.3f}")
                logger.info("NO RESONANCE FOUND (WIDE)")
                return wide
            else:
                logger.info(f"Resonance found (WIDE): {fr/1e9:.6f} "
                            "GHz.")
                fhint = fr
        else:
            wide = None

        logger.info("PERFORMING NARROW SEARCH")
        narrow = self.measure_narrow(fhint)
        if doblcal:
            narrow = remove_baseline_sppm(narrow, bl)
            # remove the offset
            narrow.uphases = narrow.uphases - narrow.uphases.mean()
            narrow.phases = narrow.phases - narrow.phases.mean()

        frfine, frfineerr = self.estimate_resonance(narrow)
        if (frfineerr is None) or (
                frfineerr/frfine >= self._frfit_max_fstderr):
            logger.info("NO RESONANCE FOUND (NARROW)")
            return wide, fhint, narrow
        else:
            logger.info(f"Resonance found: {frfine/1e9:.6f} GHz")
            return wide, narrow, frfine, frfineerr

    def run(self) -> bool:
        c = self.jpac
        mio = self.mio
        frlast = None
        self._start_time = datetime.now()
        self._end_time = datetime.now()

        if self.baseline is None:
            self.baseline = self.measure_baseline()
            self.baseline.mref = 'BASELINE'
            mio.write(self.baseline)

        for i, current in enumerate(self.sweep_params.currents):
            logger.info(f"CURRENT SET: {_mstr(current, un.ampere)}")
            c.ramp_current(current)
            try:
                cmeas = c.measure_current(1)
            except ValueError:
                # just try again, this shouldn't happen very often
                cmeas = c.measure_current(1)

            mref = f"i{i:d}"
            cmeas.mref = mref
            mio.write(cmeas)

            if frlast is None:
                result = self.bootstrap()
            elif (not self.assume_smooth):
                result = self.bootstrap(fhint=frlast)
            else:
                result = self.bootstrap(fhint=frlast, skipwide=True)
                try:
                    wide, narrow, fr, frerr = result
                except ValueError:
                    logger.info("No resonance nearby. "
                                "Bootstrapping with wide measurement.")
                    result = self.bootstrap(fhint=frlast)

            try:
                wide, narrow, fr, frerr = result
                frlast = fr
                narrow.mref = mref
                mio.write(narrow)
            except ValueError:
                # ValueError for unpacking less items
                wide, fr = result[:2]
                frlast = fr
            except TypeError:
                # TypeError for unpacking SParam1PModel
                wide = result
                frlast = None

            if self.record_wide and (wide is not None):
                wide.mref = f"DEBUG-{mref}"
                mio.write(wide)

            frfine = np.nan if frlast is None else frlast
            self._fr_fine.append(frfine)
            self._end_time = datetime.now()

    def estimate_resonance(self, sppm: SParam1PModel, fhint=None):
        kwargs = {}
        if self.q_expected is not None:
            # rough estimate bandwidth using first measurement frequency.
            bw0 = sppm.frequencies[0]/self.q_expected
            kwargs['bw'] = bw0

        if fhint is not None:
            kwargs['fr'] = fhint

        pars = phasemodel.guess(sppm.uphases, sppm.frequencies, **kwargs)
        fit = phasemodel.fit(sppm.uphases, f=sppm.frequencies, params=pars)
        logger.debug(fit.fit_report())
        bp = fit.params
        # If fit fails stderr is None.
        fr, frerr = bp['fr'].value, bp['fr'].stderr
        return fr, frerr

    def finish(self):
        currents = self.sweep_params.currents
        frs = np.array(self._fr_fine)

        # The algorithm may be cut in short, in which case
        # not all the currents are actually swept.
        setpoints = currents[:len(frs)]

        duration = self._end_time - self._start_time
        duration_s = duration.seconds + duration.microseconds*1e-6

        rmap = ResonanceMap(
                current=setpoints,
                frequency=frs,
                stepsize=currents[1]-currents[0],
                completion=duration_s)
        self.mio.write(rmap)

        if self.return_back:
            self.jpac.ramp_current(self._init_current)

    @classmethod
    def make(
            cls,
            controller: JPAController,
            sweep_params: SweepParameters,
            path: t.Union[Path, str],
            bl_current: t.Optional[float] = None):
        mio = MIOFileCSV.open(path, mode='w')
        return cls(controller, mio, sweep_params, bl_current=bl_current)


def save_script(path: Path):
    """Saves this script to the given location."""
    thisfpath = Path(__file__)
    return copyfile(thisfpath, path / thisfpath.name)


def setup_parser():
    def slicearg(s: str) -> t.Optional[int]:
        if s.upper().startswith('N'):
            return None
        else:
            return int(s)

    parser = argparse.ArgumentParser(
            description=(__doc__))

    parser.add_argument(
            '-p',
            '--path',
            type=Path,
            help="Path to the folder to be used when saving data.")

    parser.add_argument(
            '-b',
            '--baseline-current',
            type=float,
            default=None,
            help="Current to measure baseline at.  If not provided, "
                 "baseline calibration is skipped.")

    return parser


def main(args):
    path = args.path
    __measurement_name__ = "Resonance Map"
    uris = get_uris()

    # Measurement Parameters ##
    current_min = -1e-3
    current_max = 1e-3
    current_step = 5e-6

    q_expected = 300

    # Sweep parameters
    sweep_params = SweepParameters.from_ranges(
            (current_min, current_max, current_step))

    # Settings
    wide_settings = VNASettings(
                start_frequency=5.5e9,
                stop_frequency=6.1e9,
                sweep_step=500e3,  # device limitation
                power=-6,
                if_bandwidth=1e3,
                naverage=1)

    narrow_settings = VNASettings(
                span=100e6, sweep_step=100e3, power=-10,
                if_bandwidth=1000, naverage=1)

    record_wide = True

    # Initializing
    c = JPAController.make(resources=uris)
    c.current_ramp_rate = 50e-6  # A/s
    c.current_step = 5e-6  # A
    c.cs_preset.range_i = 10e-3
    c._max_flux_current = 2e-3  # CAREFUL

    c._bf6_compat = True

    mc = MainController.make(
            c,
            sweep_params=sweep_params,
            path=path,
            bl_current=args.baseline_current)

    mc.q_expected = q_expected
    mc.wide_settings = wide_settings
    mc.narrow_settings = narrow_settings
    mc.record_wide = record_wide
    mc.return_back = True
    # mc.bl_power = 3

    try:
        c.initiate()
        mc.initiate()
        mc.run()
    except CommunicationError as e:
        logger.error(e)
    except KeyboardInterrupt:
        logger.info("User abort.")
    finally:
        mc.finish()
        logger.info(f"Saving script to '{path}'")
        save_script(path)
        logger.info(f"{__measurement_name__} MEASUREMENT FINISHED")


if __name__ == "__main__":
    parser = setup_parser()
    parsed = parser.parse_args()
    main(parsed)
