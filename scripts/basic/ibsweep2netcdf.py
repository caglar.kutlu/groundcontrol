from pathlib import Path
from parse import parse, compile

import xarray as xr
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from groundcontrol.measurementio import MIOFileCSV


PATH = Path('.')
P1POWER = -30
P2POWER = 10


# Open the reader
mio = MIOFileCSV.open(PATH, 'r')


# The measurement index in a dataframe
mdf = mio.read_measurement_index_df()


# Try this in a terminal
# print(mdf)


# We know the format is:
sppm_pathfmt = "{mcls}/SPPM_{plabel}-i{sno:d}_{mindex:d}.csv"
smm_pathfmt = "{mcls}/SMM-AP_{plabel}_{mindex:d}.csv"
sppmparser = compile(sppm_pathfmt)
smmparser = compile(smm_pathfmt)


# Test
parsed = sppmparser.parse('SPPM/SPPM_P1-i0_0.csv')
assert parsed.named == {'mcls': 'SPPM', 'plabel': 'P1', 'sno': 0, 'mindex': 0}


def parse_path(path: Path):
    psx_str = path.as_posix()
    parsed = sppmparser.parse(psx_str)
    if parsed is None:
        parsed = smmparser.parse(psx_str)
    return parsed.named  # returns as dict


def parsepathseries(pathseries):
    _dcts = pathseries.apply(parse_path)

    plabels = _dcts.apply(lambda it: it.get('plabel'))
    snos = _dcts.apply(lambda it: it.get('sno'))
    return plabels, snos


plabels, snos = parsepathseries(mdf.datapath)


mdf['plabel'] = plabels
mdf['sno'] = snos


mdfp1 = mdf.plabel == 'P1'
mdfp2 = mdf.plabel == 'P2'
mdf_smm = mdf.mclass == 'ScalarMeasurementModel'
mdf_sppm = mdf.mclass == 'SParam1PModel'


# There are only two files for current measurements (since we are appending)

smms_p1 = mio.read(mdf[mdfp1 & mdf_smm].head(1).index[0]).to_xarray()
smms_p1.coords['sno'] = np.arange(len(smms_p1))
smms_p1.coords['sno'].attrs['unit'] = smms_p1.attrs['unit']
smms_p1.coords['sno'].attrs['long_name'] = smms_p1.attrs['name']

smms_p2 = mio.read(mdf[mdfp2 & mdf_smm].head(1).index[0]).to_xarray()
smms_p2.coords['sno'] = np.arange(len(smms_p2))
smms_p2.coords['sno'].attrs['unit'] = smms_p2.attrs['unit']
smms_p2.coords['sno'].attrs['long_name'] = smms_p2.attrs['name']


# SPPMS
sppms_p1 = []
for ind in mdf.where((mdfp1 & mdf_sppm)).dropna().index:
    print(f"Reading measurement index: {ind:d}")
    sppm = mio.read(ind)
    ds = sppm.to_xarray()
    ds.coords['sno'] = mdf.loc[ind].sno
    ds.coords['sigpow'] = P1POWER
    sppms_p1.append(ds)


sppms_p2 = []
for ind in mdf.where((mdfp2 & mdf_sppm)).dropna().index:
    print(f"Reading measurement index: {ind:d}")
    sppm = mio.read(ind)
    ds = sppm.to_xarray()
    ds.coords['sno'] = mdf.loc[ind].sno
    ds.coords['sigpow'] = P2POWER
    sppms_p2.append(ds)


def baseremove(dataset, ibcal=0e-6):
    dsdiff = dataset - dataset.sel(ib=ibcal, method='nearest')
    # Correction for undefined starting phase from instrument
    phase_offsets = dsdiff.uphases.mean(dim='frequencies')
    dsdiff['uphases'] = dsdiff.uphases - phase_offsets

    return (dataset
            .assign(uphases_cal=dsdiff.uphases)
            .assign(mags_cal=dsdiff.mags))


p1ds = xr.concat(sppms_p1, dim='sno')
p1ds.coords['ib'] = ('sno', smms_p1.value)
p1ds = baseremove(p1ds.swap_dims({'sno': 'ib'}))

p2ds = xr.concat(sppms_p2, dim='sno')
p2ds.coords['ib'] = ('sno', smms_p2.value)
p2ds = baseremove(p2ds.swap_dims({'sno': 'ib'}))


dataset = xr.concat([p1ds, p2ds], dim='sigpow')
dataset.coords['sigpow'].attrs['unit'] = 'dBm'
dataset.coords['sigpow'].attrs['long_name'] = 'Signal Power'


dataset.to_netcdf('dataset.nc')
