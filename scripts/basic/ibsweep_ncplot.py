import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
from functools import reduce

ds = xr.open_dataset('dataset.nc')

# PSIG = -135 dBm
p1ds = ds.sel(sigpow=-30).dropna(dim='ib')
# 2 measurement points had 'phase' jumps during measurement that
# I am 100% sure due to the VNA response and not the JPA itself
# so I am gonna remove these.
# sno is just an index of measurement
snocut = [105, 122]
p1ds = p1ds.where(
        reduce(
            lambda a, b: a & b,
            (p1ds.sno != sno for sno in snocut)
            ))


# Rolling mean over 50 bins
df = p1ds.frequencies.diff(dim='frequencies')[0]
bincnt = int(np.round(2.5e6/df))  # 2.5 MHz window
print(bincnt)
p1dsav = p1ds.rolling({'frequencies': bincnt}).mean()



# Plot a few select points
fig, axs = plt.subplots(nrows=2, sharex=True)

ibs = [1e-6, 60e-6, 65e-6, 66e-6, 70e-6]
ibs_m = np.array(ibs) - 85e-6
p1dsav.sel(ib=ibs, method='nearest').uphases_cal.plot.line(x='frequencies', ax=axs[0])
p1dsav.sel(ib=ibs_m, method='nearest').uphases_cal.plot.line(x='frequencies', ax=axs[1])

# no averaged
# p1ds.sel(ib=ibs, method='nearest').uphases_cal.plot.line(x='frequencies')


# Single frequency plot
plt.subplots()
f = 1.45e9
(p1dsav.uphases_cal
    .sel(frequencies=f, method='nearest')
    .plot.line(x='ib'))


# pcolormesh
p1dsav.uphases_cal.plot.pcolormesh(cmap='inferno', vmin=-50, vmax=50)


# PSIG = -95 dBm
p2ds = ds.sel(sigpow=10).dropna(dim='ib')

# No averaging needed 


# Plot a few select points
fig, axs = plt.subplots(nrows=2, sharex=True)

ibs = [1e-6, 60e-6, 65e-6, 66e-6, 70e-6]
ibs_m = np.array(ibs) - 85e-6
p2ds.sel(ib=ibs, method='nearest').uphases_cal.plot.line(x='frequencies', ax=axs[0])
p2ds.sel(ib=ibs_m, method='nearest').uphases_cal.plot.line(x='frequencies', ax=axs[1])

#
# Single frequency plot
plt.subplots()
f = 1.45e9
(p2ds.uphases_cal
    .sel(frequencies=f, method='nearest')
    .plot.line(x='ib'))


# pcolormesh
p2ds.uphases_cal.plot.pcolormesh(cmap='inferno')

plt.show()
