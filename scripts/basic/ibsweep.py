import sys

import numpy as np 

from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.controllers import JPAController as RackController
from groundcontrol.settings import VNASettings
from groundcontrol.resources import get_uris
from groundcontrol.logging import logger


CMIN, CMAX, CSTEP = -85e-6, 85e-6, 1e-6
P1 = -30
P2 = 10
PATH = "~/Measurements/201027/IBSWEEP"


vnapreset = VNASettings(
        start_frequency=0.8e9,
        stop_frequency=1.5e9,
        sweep_step=50e3,
        if_bandwidth=1e3,
        power=-30)

uris = get_uris()

cntrl = RackController.make(resources=uris)

cntrl.current_step = 1e-6
cntrl.current_ramp_rate = 40e-6

cntrl.vna_preset = vnapreset

cntrl.initiate()


mio = MIOFileCSV.open(PATH, 'w')


currents = np.arange(CMIN, CMAX+CSTEP/2, CSTEP)
powers = [('P1', P1), ('P2', P2)]


for plabel, power in powers:
    cntrl.do_vna_preset(vnapreset.evolve(power=power))
    for i, current in enumerate(currents):

        cntrl.ramp_current(current)

        # Measurements
        cntrl.trigger_network_analyzer()

        s21 = cntrl.read_s21()
        current = cntrl.measure_current()

        s21.mref = f"{plabel}-i{i:d}"
        current.mref = f"{plabel}"

        phaseavg = s21.uphases.mean()
        cval = current.value

        mio.write(s21)
        mio.append(current)
        logger.info(f"Average phase: {phaseavg:.3f} deg")
        logger.info(f"Current: {cval/1e-6:.3f} uA")


logger.info("Experiment finished.")
