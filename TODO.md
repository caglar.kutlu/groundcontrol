# TODO
This document serves as a reference to overall todo list for the project.

## General
    - [ ] Move TODOs that are dispersed around the files to here :)
    - [ ] `domaybe` function in helper should be renamed to `applymaybe`
    - [ ] {NONTRIVIAL} When blocking by querying `*OPC?` the program is
      completely unresponsive to any interruption.  This blocking may be
      accomplished by polling with timeout. Better to make a generic interface
      for long-running operations like this.


## MeasurementModel
    - [ ] `MeasurementModel`s should be able to use enums as types.
    

## JPAController
    - [ ] The reported completion time for the NA measurement may be wrong when
      doing `Point` style averaging.  Need to test this.
    - [ ] This controller should not hold state like `ramp_rate` etc. Maybe
      maximum values rather than state is better.
    - [ ] This class has too many functions which are mainly related to the
      instrument control rather than purely JPA.  Better to make a class to
      control instruments in the rack or the system and let JPAController
      access to it.


## JPATuner
    - [ ] This class has too much functionality that's better moved to
      JPAController.
    - [ ] Define a small set of functions and attributes that a JPATuner
      instance should have.  Let the subclasses implement these.


## API
    - [ ] The classes with declarative attributes should be able to generate
      docstrings.
    - [ ] Change `InstrumentSettings` api to a generic Settings declaration
      api.  Do it simple in the beginning.


## Instruments
    - [ ] The specific NA class N5232a is implemented in a way to emulate
      N5222a.  This is bad practice.  The instruments implementing just the SCPI
      commands should not be composing commands to emulate the behavior of
      other instruments.


## Exceptions
    - [ ] Get rid of unnecessary too specific exceptions.
    - [ ] Find a way to organize exceptions with enumerations.

