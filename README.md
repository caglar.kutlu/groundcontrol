This is a python3 library for controlling various instruments along with
abstractions to help declare experiments, controllers; and functions to
facilitate saving data.  

Coding style mainly follows google

Contact: `caglar.kutlu@gmail.com`

# Installation
You can install directly with pip via:

```
pip3 install --user .

```

For development, prefer:
```
pip3 install --user -e .
```

# Directory structure

## Changes on 21/10/19
Test folders are now divided into two:
    - testm: Manual test scripts that are written to "quickly check stuff on
      the go".
    - testa: Automated test scripts preferably using `unittest` module.
      Automated test means that the result of the test is a PASS or FAIL
      decided by the script itself rather than a human looking at a plot or a
      number.

```

.
├── assets:  Non-programming related assets, e.g. illustrations.
├── groundcontrol:  The python library.
├── groundcontrol.egg-info
├── IDEAS.md:  A place to dump ideas.
├── LICENSE
├── notebooks:  Some jupyter notebooks mainly for demonstrative purpose.
├── README.md:  This page.
├── requirements.txt:  Python requirements file.
├── scripts:  Experiment scripts using this library.
├── setup.py
├── snippets:  Some snippets of code that can be shared easily.
├── atest:  Place for unit tests or automated tests.
├── mtest:  Place for tests requiring human interaction (manual tests).
├── TODO.md:  Ideas for TODO.
└── tools:  A place to store useful binaries, scripts, etc.

```


# Codebase
Currently the codebase can be decomposed into 4 main components:

## 1. Instrument Interfaces
Mainly implemented under `groundcontrol/instruments`.  The instruments are only
thin wrappers around commands they document.  Enumerations are provided to ease
programming when using completions.

## 2. Measurement Abstraction
There is a simple abstraction used to define data structures in a declarative
form.  It's centered around the class `MeasurementModel` and IO related class
`MIOBase`.  Relevant modules are:
 - `groundcontrol/measurement.py`
 - `groundcontrol/measurementio.py`

There are some other tentative abstractions for `Controller` etc objects that
can be found under `controller.py`, `declarators.py`, `settings.py`,
`instrumentmanager.py`

## 3. Control
### 3.1 JPAController
There is currently one main module which contains most of the logic related to
instrument communications when doing JPA experiments.  It's implemented as
`groundcontrol/controllers/jpa.py`.  

### 3.2 Control Scripts
Rest of the logic for experiments are implemented in individual scripts that
can be found under `scripts`.  Almost all the scripts have a
simple CLI that at least implements `python3 scriptname.py -h` interaction.  As
of now, the up-to-date and relevant scripts for JPA measurements are:

1. `flux_sweep.py`:  Tunes bias current and measures resonance frequency.
2. `paramap.py`: Performs paramap measurements.
3. `jpasnrscan.py`: Does noise temperature measurements for the JPA using SNR
   comparison & spectral comparison methods.
4. `jpasatscan.py`: Does saturation measurements for the JPA.
5. `ntmeas.py`:  Does noise temperature measurement using the internal noise
   source.
6. `periodic_sa.py`: Periodic measurements of the spectrum analyzer.
7. `periodic_vna.py`:  Periodic measurements of the vector network analyzer.
8. `tempcal.py`:  Measure temperature sensor resistances using LS372
   controller.
9. `pumpheatrelax.py`: Perform series of resonance frequency relaxation time
   experiment for observing the effect of pump heating.

There are some analysis related scripts as well.  Usual way of implementing the
analysis for a particular measurement is to write a module within the library
under `groundcontrol/analysis` and writing a frontend script at
`scripts/analysis`.  The commonly analysis scripts are as follows:

1. `analysis/paramap2netcdf`:  Converts paramap measurement that was recorded
   using MIOFileCSV backend into a netCDF4 file.  The data then can further be
   manipulated within the convenience of the `xarray` module.
2. `analysis/snrscan2netcdf`:  Same as above but for snrscan measurements.
3. `proc_ntmeas.py`:  Processes the nt measurement.
4. `ntquick.py`:  Simple implementation of ntmeas analysis.  This produces csv
   files to be used in NT analysis of JPA.
5. `proc_flux_sweep.py`:  Processes the `flux_sweep.py` measurements.
6. `crossfrcut.py`:  Performs sophisticated cuts on paramap dataset.


The other scripts found are either obsolete, seldom-used or require
modifications to work with recent versions of `groundcontrol`.


### 3.3 Misc
Apart from these, there is a `jpa_tuner.py` module that was used to provide
tuning abstractions early on, but it's mostly obsolete now.  The `pid.py`
provides a simple implementation of PID algorithm to be used wherever an ad-hoc
tuning necessary.


# Example REPL Session
There is an example controller session file in `scripts/controller.py`.  You
may run it in an ipython session and you will have access to a Controller
instance.  The object initiates with the default visa resources of BF4 setup,
change if necessary.  To run it in an ipython session:

```
ipython3 -i scripts/controller.py
```

Then initiate the controller object via:

```
c.initiate()
```

Now you have access to SA, VNA, SG, CS(Current Source), TC (Temperature
controller) via members `c.vna`, `c.sg`, `c.cs`, `c.sa`, `c.tc`.


# Logging
By default a logger will output to a standard output.  You may record logs to a
file using `create_log` from `groundcontrol.logger`:

```
from groundcontrol.logger import create_log, DEBUG

create_log(fname='my.log', level=DEBUG)
```

This will create a file named `my.log` that will be filled with DEBUG level log
entries.


# Language & Style
The project's overall language is Python. For styling the code in the project
should follow the Google's python style and language guidelines that can be
found [here][1]. Also, try to use docstring compatible commenting if possible.
An example how to do that can be found [here][2].  Also, it is good practice to
use explicit typing wherever possible.  If using the typing style, no need to
provide types in docstrings.

For starters, at least make sure that you do the following:
* Always use 4 spaces for indentation. Make sure that you understand what your
  particular editor does when you press tab.
* No line should exceed 79 characters.
* Provide explanation for functions/methods unless the naming is very
  straightforward.
* No premature optimization.  The 'optimization' here means runtime
  performance, not 'amount of keystrokes saved while writing a piece of code'.

## `attrs`
`attrs` is a package that lets you write concise and self descriptive programs
with LESS typing and almost ZERO computational overhead.  I try to use it
whenever I need to use classes.  If you encounter some related code that you
want to change, you may get used to how it works a bit by looking at the 
examples provided [here][5]

## Doctests
Doctests are little snippets of code that you put in your source files, that are
supposed to be a working example of whatever your code is supposed to do.   They
are easy to do because you need to write something to test your code anyways, so
instead of doing that in a python shell, you just write it inside your source
and then run (for example):
```bash
python3 -m doctest chapp/sim/sources.py
```
This runs the example snippets contained in the file chapp/sim/sources.py, and
produces a simple result for you to see if any of the line in the snippet fails.

While doctests can't replace some rigorous testing covering corner cases, it's
fast and simple to write.  Usually, we don't have time to write tests for
everything we do, but we try them anyways.  If you write your `trials` as
doctests, then other people can try and learn from those examples this way.
See [this][3] for more information on how to use `doctest` module.

## Unittests
Unit tests are for more rigorous testing.  It's not required to write these for
whatever you are contributing, but you should conform to some standards if you
are choose to write your unit tests for your module.  Check [this][4] site for
examples of usage.

[1]: https://google.github.io/styleguide/pyguide.html
[2]: http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
[3]: https://pymotw.com/3/doctest/
[4]: https://pymotw.com/3/unittest/index.html#module-unittest
[5]: https://www.attrs.org/en/stable/ 
