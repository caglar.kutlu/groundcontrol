#!/usr/bin/env python

from distutils.core import setup
from groundcontrol import __version__

setup(name='groundcontrol',
      version=__version__,
      description="Instrument control",
      author='Caglar Kutlu',
      author_email='caglar.kutlu@gmail.com',
      url='',
      packages=[
          'groundcontrol', 'groundcontrol.instruments',
          'groundcontrol.instruments.spectrumanalyzer',
          'groundcontrol.instruments.dcsupply',
          'groundcontrol.instruments.misc',
          'groundcontrol.instruments.networkanalyzer',
          'groundcontrol.instruments.signalgenerator',
          'groundcontrol.instruments.sourcemeter',
          'groundcontrol.instruments.temperaturecontroller',
          'groundcontrol.controllers',
          'groundcontrol.analysis'],
      )
