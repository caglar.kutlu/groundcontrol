# Design Template
This file contains ideas and templates for designing API of this library, and
may contain misleading and/or incomplete information at different times.


# JPA Control
For our purposes JPA has two main modes:
1. Passive:  No pump signal is provided.
2. Active:  Pump signal is provided.

Also at any instant of time, the state of the JPA is determined by:
1. Resonance frequency:  This is the resonance measured in the passive mode.
2. Gain spectrum:  This is the gain measured in the active mode.
3. Bias current:  The current supplied to the dc biasing coil, setting the
   magnetic flux penetrating the SQUID.
4. Pump frequency:  Frequency of the pump signal.
5. Pump power:  Power of the pump signal.
6. Mode:  Active or passive.


## Resonance Frequency
There is a one-to-one correspondence of resonance with the flux threading
the SQUID inside JPA.  Thus, the resonance frequency is controlled via
controlling the current sent to the flux coils.


## Gain Spectrum
Gain spectrum depends on the resonance frequency, pump power, and pump
frequency.  In most cases, the center of the gain spectrum, i.e. the frequency
at which the gain is maximum, is determined by the pump frequency alone.  As
the JPA can get saturated at quite low power values, it is important to control
the stimulus power when measuring the gain.


## Bias current
The DC biasing is done through a coil providing the magnetic flux to the SQUID
inside JPA.  Since flux depends on the number of turns that the coil has, the
current to resonance frequency transfer function will also depend on the number 
of turns.  Practical tuning achieved for windings of 300 and 1000 is always
below 1 mA.   Even though the resonance frequency is periodic in applied flux,
there is deviation from exact periodicity and the deviation will show up as the
decrease in maximum resonance achievable.


# Framework Design
Designing something too generic is hard and it is easy to end-up designing
something very counter-intuitive and cumbersome to use.  For this reason,
instead of designing something for an arbitrary hierarchy of measurements,
nested measurements/experiments, I decided for going structures which allow
only 2-level hierarchy:  Experiment and Measurements.
- An Experiment can not contain another experiment,
- A measurement can not contain another measurement.
- An experiment can contain multiple measurements of the same kind
and/or of different types.
- An experiment can be transformed into a measurement via processing.
- A controller can and *maybe* should contain other controllers.  Composing
  controllers rather than inheriting or mixing may be better.


# Instrument Drivers
- A rethinking of the command implementation is necessary.
- Think of low level communication classes as collection of commands.  Define
  command types and response types.
- Implement instruments as collection of the individual command instances.
- Implement higher-level drivers as adapters for a common instrument class.


# Declarators
Declarator is a specific type of descriptor that describes the member variable
of a class.  They enable declarative and concise programming within python.

Example:
    <GIVE AN EXAMPLE HERE>

The library currently contain certain declarators to be used in conjunction
with `attrs` enabled classes.  Some of these are, from
`groundcontrol.declarators`:
- Quantity:  A quantity is generally a measurable variable.
- Parameter:  A parameter is a fixed variable during the measurement of a
  quantity.
- Setting:  This is a constant parameter that is never expected to change
  within a given context.

The difference between a `parameter` and a `setting` is that the former can be
both manipulated by the program itself and by the user as well, whereas the
latter will only be manipulated by the user.

# Types
It is important to define the types/data structures before proceeding to
actually programming for two reasons:
1. Any program can be decomposed into an interplay of types of information and
   an operation based on what type of information you have and the actual
   information itself.
2. Cleanly typed programs are often cleaner, compact and improvable.

Below are some guiding principles when defining types here:
1. Result kind of classes should be immutable.
2. Any operation on a result should be performed on a copy of the result.
3. Contradictory to Rule 1 above, it is usually the case that we would like to
   have an updatable data model that is similar to a result.
4. Value-like objects such as `Quantity` and `Parameter` can be of any primitive
   type, however they must contain a field named `value` and `name` at least.


## MeasurementModel
A `MeasurementModel` should contain information on:
1. Measured quantities.
2. Units of measured quantities.
3. Fixed parameters of the measurement.
4. Units of the fixed parameters.
5. Comments.
6. Measurement name: This is the name of the measurement type.  Thus, different
   measurements of for example SParam1PModel should have the same measurement
   name.
7. Measurement reference: (Optional) This is somewhat a unique reference to the
   measurement.  Can be a GUID or just an incremental number represented as
   string.

It is important that all the quantities in a measurement have the same
dimensionality.

A `MeasurementModel` should have the following methods at least:
1. A way to save the measurement in simple csv format. 
2. A way to serialize data in binary format (something like HDF5 compatible).

From these, the required ones are 1,2,3, and 4.  The 3rd one is relaxed in the
sense that one may provide only a small subset of all the fixed parameters of a
measurement, since measurements are usually complex.  Environment of a
measurement can be provided as a comment in human-readable format.  Units must
be provided for every provided parameter and quantity.

A measurement must contain quantities of same shape.


## ExperimentModel (NOT DONE YET)
It is often the case that we repeat a measurement by changing one of the
parameters.  In this case, we need a way to refer to each MeasurementResult
uniquely.  The simplest way to do so is to assign a number like a measurement
number and increase this per added measurement result.  This can be achieved in
the context of another class of measurement which is called an
`ExperimentModel`s.  Since essentially an `Experiment` is a collection of
`Measurement`s, an `ExperimentModel` is a collection of `MeasurementModel`s.
In addition to the default addressing (indexing), one can also introduce
additional indexes.

An `ExperimentModel` should have the following attributes:
1. A collection of `MeasurementModel` objects.
2. A way to address these `MeasurementModel` objects.
3. Fixed parameters of the experiment.
4. At least one way to serialize the data to save on a disk.

The subclasses of `ExperimentModel` will be specific to the experiment type.


## Controller
### What does a Controller do?
- Controls the instruments that are assigned to it.
### What's the difference between InstrumentManager and a Controller then?
- InstrumentManager can initiate instruments dynamically, whereas
  several Controller's are able to access the ones that are statically assigned
  to them.
- Another important difference is that Controller's can alter the
  state of the instruments whereas InstrumentManager is only responsible for
  keeping track of communications.
### Why not make a generic InstrumentManager rather than a Controller then?
- Restricting functionality oftentimes improves utility.
### What is controller good for?
- Encapsulating complex control sequences that produces an atom of measurement.
  For example, ramping up the current of a current source and then measuring
  the resonance frequency of a parametric amplifier is an atom of measurement
  that requires at least 2 instruments.
### It seems like a Controller can be used as an Instrument as well, is that true?
- This is true since in the context of this library, Instruments are defined by
  their control interfaces to the physical instrument.
  Thus, Instruments are Controllers rather than vice versa.  However, making a
  flat hierarchy between Controller and Instrument objects requires good
  devotion to testing corner-cases, therefore harder to implement.
  Nevertheless, you can declare controllers within a controller, allowing a
  tree-like structure.


## Measurement (NOT IMPLEMENTED)
- Does not utilize the instruments directly.
- Uses one or more controllers.
- Produces MeasurementModel's.


## Experiment (NOT IMPLEMENTED)
- Experiment should be a sequence(or collection?) of measurements.  Probably a
  tree of measurements.
- Experiments have a clear objective whereas controllers don't.
- Experiment is defined by a sequence of measurements.
- Each measurement results in a MeasurementModel instance.
