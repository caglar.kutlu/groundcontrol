"""This module demonstrates a spectrum computation that produces identical
results with a computation done using pyRF3, excluding IQ offset & offset
corrections.

"""

import sys
import os
import time

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import welch
from groundcontrol.util import watt2dbm

from pyrf.devices.thinkrf import WSA
from pyrf.util import compute_spp_ppb


try:
    ip = sys.argv[1]
except IndexError:
    try:
        ip = os.environ['TRF_IP']
    except KeyError:
        ip = '192.168.0.15'

dut: WSA = WSA()

dut.connect(ip)
dut.reset()

RFE_MODE = 'SHN'  # don't change
PASS_BAND_CENTER = dut.properties.PASS_BAND_CENTER['SHN']
FULL_BW = dut.properties.FULL_BW['DEC_SHN']
USABLE_BW = dut.properties.USABLE_BW['DEC_SHN']

dut.pll_reference('EXT')
print(dut.pll_reference())
vcolock = dut.locked('VCO')
clklock = dut.locked('CLKREF')

if any([not vcolock, not clklock]):
    print("Reference locking error.")
    sys.exit()

ATTENUATOR = 0
DECIMATION = 64
FREQ = 5.9e9
FSHIFT = 0
NAVG = 100
TRIGGER_SETTING = {'type': 'NONE',
                   'fstart': (FREQ - 10e6),  # some value
                   'fstop': (FREQ + 10e6),  # some value
                   'amplitude': -140}


# chosen to be something close to ~100
NFFT = 15625  # 1024*20
BINWIDTH = FULL_BW/DECIMATION/NFFT
FSAMPLE = FULL_BW/DECIMATION

print(f"DECIMATION: {DECIMATION}")
print(f"ATTENUATOR: {ATTENUATOR}")
print(f"FREQ: {FREQ}")
print(f"FSHIFT: {FSHIFT}")
print(f"NAVG: {NAVG}")
print(f"BINWIDTH: {BINWIDTH} ({FULL_BW}/({DECIMATION}*{NFFT}))")
print(f"FSAMPLE: {FSAMPLE}")
print(f"NFFT: {NFFT}")

dut.request_read_perm()
dut.iq_output_path('DIGITIZER')
dut.attenuator(ATTENUATOR)
dut.rfe_mode(RFE_MODE)
dut.decimation(DECIMATION)
dut.freq(FREQ)
dut.trigger(TRIGGER_SETTING)

time.sleep(1)
maxppb = dut.properties.MAX_PPB_DECIMATED_OR_FSHIFT
maxspp = dut.properties.MAX_SPP

spp, ppb = compute_spp_ppb(NAVG*NFFT, dut.properties, decimation=DECIMATION)
print(f"Sampling rate is {FSAMPLE} Hz.")
capture_time = spp*ppb/FSAMPLE
print(f"Capturing total of {spp*ppb} samples "
      f"distributed to {ppb} packets. "
      f"Capture time is about {capture_time:.6f} s.")

_MAXITER = 100000


def capture_packets(spp, ppb):
    conpkts = []
    datapkts = []
    dut.capture(spp, ppb)
    for i in range(_MAXITER):
        packet = dut.read()

        if packet.is_context_packet():
            conpkts.append(packet)

        if packet.is_data_packet():
            datapkts.append(packet)

        if len(datapkts) == ppb:
            break
    return conpkts, datapkts


def capture(spp, ppb):
    dut.flush()
    dut.flush_captures()
    _start = time.time()
    conpkts, datapkts = capture_packets(spp, ppb)
    _end = time.time()
    elapsed = _end - _start
    idata, qdata = np.vstack([pkt.data.np_array for pkt in datapkts]).T
    print(f"Took {elapsed:.6f} s to acquire {spp*ppb} samples.")

    context = {}
    for pk in conpkts:
        context.update(pk.fields)

    return idata, qdata, context


bit_size = 14
NFACTOR = 2**(bit_size - 1)  # from documentation


def compwelch(idata, qdata, context):
    ref = context['reflevel'] - dut.properties.REFLEVEL_ERROR
    iq = (idata + 1j*qdata)/NFACTOR

    f, pxx = welch(
            iq,
            fs=context['bandwidth'],
            window='rect',
            nperseg=NFFT, noverlap=0, average='mean', scaling='spectrum',
            detrend=False,
            return_onesided=False)

    f = f + context['rffreq']

    dbm_uncal = watt2dbm(pxx)
    pxxdbm = dbm_uncal + ref
    return f, pxxdbm


idata, qdata, context = capture(spp, ppb)

_start = time.time()
f, pxxdbm = compwelch(idata, qdata, context)
_end = time.time()
elapsed = _end - _start
print(f"It took {elapsed:.6f} s to compute a PSD using Welch's "
      f"with nperseg={NFFT} for a total of {spp*ppb} samples.")

plt.plot(f, pxxdbm)
plt.grid()
plt.show()
