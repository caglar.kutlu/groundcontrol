#!/usr/bin/env python

###############################################################################
## These simple examples connect to a device of IP specified on the command
## line, tunes it to a center frequency of 2.450 MHz, then reads and displays
## one capture of 1024 i, q values.
##
## To run this:
##     <your OS python command> <example_file>.py <device_IP>
##
## © ThinkRF Corporation 2020. All rights reserved.
###############################################################################


import sys
import time
from pyrf.devices.thinkrf import WSA

# define some constants
RFE_MODE = 'ZIF'
CENTER_FREQ = 7000e6
SPP = 1024 # Samples per packet
PPB = 1 # Packets per block
DECIMATION = 1 # Downsample
ATTENUATION = 20
# GAIN = 'HIGH'
TRIGGER_SETTING = {'type': 'NONE',
                'fstart': (CENTER_FREQ - 10e6), # some value
                'fstop': (CENTER_FREQ + 10e6),  # some value
                'amplitude': -100}

# connect to wsa
dut = WSA()
dut.connect(sys.argv[1])

dut.reset()


# setup data capture conditions
dut.request_read_perm()
dut.freq(CENTER_FREQ)
dut.rfe_mode(RFE_MODE)
if (RFE_MODE != 'DD'):
    dut.freq(CENTER_FREQ)
dut.attenuator(ATTENUATION)
# dut.psfm_gain(GAIN)
dut.trigger(TRIGGER_SETTING)
dut.decimation(DECIMATION)

# capture the desired data points
dut.capture(SPP, PPB)

# read until 1 data packet is received
while not dut.eof():
    pkt = dut.read()

    if pkt.is_context_packet():
        print(pkt)

    if pkt.is_data_packet():
        break

print(pkt.valid_data)

# print I/Q data into i and q
# for i, q in pkt.data:
#     print("%d,%d" % (i, q))

dut.disconnect()
