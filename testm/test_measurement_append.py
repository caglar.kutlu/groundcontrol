import sys, shutil
import tempfile
from itertools import chain, repeat, cycle

from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.measurement import MeasurementModel, quantity, parameter
import groundcontrol.units as un
from groundcontrol.logging import set_stdout_loglevel, DEBUG, logger


set_stdout_loglevel(DEBUG)


NROWS = 20
 

class Model(MeasurementModel):
    x: float = quantity(un.nounit, 'x')
    y: float = quantity(un.nounit, 'y')
    a: float = parameter(un.nounit, 'a')


m = Model.fake()


tmpfl = tempfile.mkdtemp()


mio = MIOFileCSV.open(tmpfl, 'w')


MREFS = chain(['FST'], cycle(chain(*[['AA']*1, ['BB']*3, ['CC']*2])))


measurements = []
for i, mref in zip(range(NROWS), MREFS):
    print(mref)
    meas = Model.fake()
    meas.mref = mref
    mio.append(meas)
    measurements.append(meas)


# A single measurement
meas = Model.fake()
meas.mref = "SINGLE"
mio.write(meas)


print(f"Temporary file: {tmpfl}")

todel = input(f"Delete?")

if todel.lower().startswith('n'):
    print("File not deleted.")
    sys.exit(42)
else:
    shutil.rmtree(tmpfl)
    print("File deleted.")
    sys.exit(0)
