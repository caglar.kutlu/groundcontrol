from groundcontrol import InstrumentManager, InstrumentType

RESOURCE = "TCPIP0::10.10.1.150::7777::SOCKET"

im = InstrumentManager.create()

rsc = im.create_visa_instrument(
    'TC', RESOURCE, InstrumentType.SocketLS372TemperatureController)
