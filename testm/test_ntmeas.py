from pathlib import Path

from groundcontrol.analysis.ntmeas import RawDataSet


path = Path("~/Measurements/BF3_JPA/201104/NT_JPA_OFF_R3").expanduser()


rds = RawDataSet.from_path(path)


print(rds)
