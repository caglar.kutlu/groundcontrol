"""Simple testing for analysis/nt module"""
from groundcontrol.analysis.nt import make_generic_model, NTModelParams


modpars = NTModelParams(2.3e9, 0.05, 2, 1, 1, 1, 1, 100)

print(modpars)

model = make_generic_model(modpars, 0.05, 1e7)


params = model.make_params()

for k, v in params.items():
    print(v)
