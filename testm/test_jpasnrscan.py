from pathlib import Path
import os
import argparse

import xarray as xr
import matplotlib.pyplot as plt

from groundcontrol.analysis.jpasnrscan import DataSet, WorkingPointExt
from groundcontrol.analysis.jpasnrscan import _DatasetConverter, NTAnalyzer
from groundcontrol.measurement import read_measurement_csv
from groundcontrol.logging import set_stdout_loglevel, DEBUG


isdebug = True
try:
    _ = os.environ['DEBUG']
except KeyError:
    isdebug = False

if isdebug:
    set_stdout_loglevel(DEBUG)


def load_dataset(path: Path):
    ds = DataSet.from_path(path)
    return ds


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('path', type=Path, help='Input filepath.')
    args = parser.parse_args()

    if args.path.suffix == '.nc':
        dsxr = xr.open_dataset(args.path)
    else:
        ds = load_dataset(args.path)
        dsc = _DatasetConverter.make(ds)
        dsxr = ds.to_xarray()
