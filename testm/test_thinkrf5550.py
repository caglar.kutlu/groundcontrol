import sys

from datargs import parse, arg, argsclass

from groundcontrol.controller import Controller, instrument
from groundcontrol.instruments import SCPIVectorNetworkAnalyzer,\
        VISAInstrument, SCPICommon
from groundcontrol.instruments.spectrumanalyzer.thinkrf import ThinkRFR550SCPI


@argsclass
class Opts:
    ip: str = arg(True)


args = parse(Opts)


class SCPIInstrument(SCPICommon, VISAInstrument):
    pass


class TestController(Controller):
    trf: SCPIInstrument = instrument(f"TCPIP::{args.ip}::hislip0")

    def __del__(self):
        return self.close_all()

    def close_all(self):
        self.trf.close()


tcn = TestController.make()
tcn.initiate()


trf = tcn.trf

trf.query('INP:MODE?')
# Zero IF mode
trf.write('INP:MODE ZIF')
trf.query('OUT:MODE?')
# CONNECTOR mode is analog IQ output mode
trf.write('OUT:MODE CONN')
trf.write('SOUR:REF:PLL EXT')  # select ext ref
trf.write('FREQ:CENTER 5.79GHz')


# AFTER POWER DOWN AND UP
# ALMOST EVERYTHING RESETS EXCEPT FOR LAN STUFF
