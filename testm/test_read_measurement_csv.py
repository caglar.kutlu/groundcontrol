from pathlib import Path

from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.measurement import SParam1PModel, SASpectrumModel
from groundcontrol.measurement import read_measurement_csv
from groundcontrol.measurement import MeasurementModel, quantity, parameter
from groundcontrol import units as un


class SensorResistanceRecord(MeasurementModel):
    timestamp: str = quantity(un.nounit, "Timestamp")
    channel: str = quantity(un.nounit, 'MeasurementChannel')
    resistance: float = quantity(un.ohm, "Resistance")
    resistance_min: float = quantity(un.ohm, "ResistanceMin")
    resistance_max: float = quantity(un.ohm, "ResistanceMax")
    reactance: float = quantity(un.ohm, "Reactance")
    exc_pow: float = quantity(un.watt, "ExcitationPower")
    exc_volt: float = quantity(un.volt, "ExcitationVoltage")
    exc_curr: float = quantity(un.ampere, "ExcitationCurrent")
    read_stat: str = quantity(un.nounit, "ReadingStatus")
    exc_frequency: float = quantity(un.hertz, "ExcitationFrequency")
    dwell: float = parameter(un.second, "DwellPeriod")
    sampling_rate: float = parameter(un.hertz, 'SamplingRate')


thisdir = Path(__file__).absolute().parent

sa = read_measurement_csv(thisdir / "data/SASM_i0j11k1_71.csv")
# print(sa)
assert sa is not None

sparam = read_measurement_csv(thisdir / "data/SPPM_BASELINE_0.csv")
# print(sparam)
assert sparam is not None

tp = read_measurement_csv(thisdir / "data/TP_i0j0k0_2.csv", ignore_units=False)
# print(tp)
assert tp is not None

srr = read_measurement_csv(thisdir / "data/SRR-AP_EXP_0.csv")
# print(srr)
assert srr is not None


