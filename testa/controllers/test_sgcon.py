import unittest
import os

from groundcontrol.resources import get_uris
from groundcontrol.controllers.sgcon import (
        SGController)
from groundcontrol.logging import (
        setup_logger, set_stdout_loglevel,
        INFO, DEBUG)


VERBOSE = (os.environ.get('VERBOSE', None) is not None)


logger = setup_logger('test_sacon')
if VERBOSE:
    set_stdout_loglevel(DEBUG)


class TestSGController(unittest.TestCase):
    def setUp(self):
        try:
            self.uri = os.environ["SG_URI"]
        except KeyError:
            self.uri = get_uris()['sg1']
        sgc = SGController.make(
                resources={'sg': self.uri})
        sgc.initiate()

        self.sgc = sgc
        self.lowpower = -130
        self.testpower = -60
        self.testfreq = 5.85e9
        self.p0 = sgc.query_power()
        self.f0 = sgc.query_frequency()

    def tearDown(self):
        self.sgc.set_power(self.p0)
        self.sgc.set_frequency(self.f0)
        # To prevent opening multiple connections.
        self.sgc.sg.close()

    def test_set_power(self):
        self.sgc.set_power(self.testpower)

        pnew = self.sgc.query_power()
        self.assertEqual(self.testpower, pnew)

    def test_set_frequency(self):
        self.sgc.set_frequency(self.testfreq)

        fnew = self.sgc.query_frequency()
        self.assertEqual(self.testfreq, fnew)

    def test_activate(self):
        self.sgc.set_power(self.lowpower)
        self.sgc.activate()
        self.assertTrue(self.sgc.query_isactive())

    def test_deactivate(self):
        self.sgc.set_power(self.lowpower)
        self.sgc.activate()
        self.assertTrue(self.sgc.query_isactive())
