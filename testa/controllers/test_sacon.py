import os
import unittest
import logging
from functools import wraps

import numpy as np

from contextlib import contextmanager
from groundcontrol.controllers import (
        SAController)
from groundcontrol.controllers.sacon import (
        SASettings,
        PowerSpectrum)
from groundcontrol.resources import get_uris
from groundcontrol.logging import (
        setup_logger, set_stdout_loglevel,
        INFO, DEBUG)


SLOWTEST = (os.environ.get('SLOWTEST', None) is not None)
VERBOSE = (os.environ.get('VERBOSE', None) is not None)


def skipifnotslow(fun):
    @wraps(fun)
    def nop(*args, **kwargs):
        logger.debug("Skipping {fun.__name__} because of slowness")

    if SLOWTEST:
        return fun
    else:
        return nop


logger = setup_logger('test_sacon')
if VERBOSE:
    set_stdout_loglevel(DEBUG)


@contextmanager
def consettings(controller, settings):
    old = controller.query_settings()
    controller.set_settings(settings)
    try:
        yield settings
    finally:
        controller.set_settings(old)


class TestSAController(unittest.TestCase):
    def setUp(self):
        try:
            self.uri = os.environ["SA_URI"]
        except KeyError:
            self.uri = get_uris()['sa']
        self.sac = SAController.make(
                resources={'sa': self.uri})
        self.sac.initiate()

        self.testset = SASettings(
                detector=SASettings.DetectorType.AVERAGE,
                filterbw=SASettings.FilterBWType.NOISE,
                start_frequency=5.7e9,
                stop_frequency=6.1e9,
                npoints=401,
                rbw=1e6,
                vbw=1e3,
                swt_auto=True,
                naverage=1)

    def tearDown(self):
        # To prevent opening multiple connections.
        self.sac.sa.close()

    def test_query_settings_type(self):
        settings = self.sac.query_settings()

        self.assertIsInstance(settings, SASettings)

    def test_set_settings(self):
        """NOTE:
        This test relies on the fact that 3DB filter type RBW definition IS
        used when setting the RBW.  If other filter type is used, the actual
        set RBW and queried will DIFFER.  Also, RBW values are most likely
        discretized, which may also cause this test to fail if RBW other than
        1e6 used.
        """
        _S = SASettings
        old = self.sac.query_settings()
        new = SASettings(
                detector=_S.DetectorType.AVERAGE,
                filterbw=_S.FilterBWType.DB3,
                start_frequency=5.7e9,
                stop_frequency=6.1e9,
                npoints=401,
                rbw=1e6,
                vbw=1e3,
                swt_auto=True,
                naverage=1)
        self.sac.set_settings(new)
        newq = self.sac.query_settings().evolve(
                filtertype=None,
                center_frequency=None,
                span=None,
                swt=None)
        # we don't compare stuff we didn't set.
        self.assertEqual(new, newq)

        # restore
        self.sac.set_settings(old)
        oldq = self.sac.query_settings()
        self.assertEqual(old, oldq)

    def test_read_spectrum_type(self):
        _S = SASettings
        old = self.sac.query_settings()
        new = SASettings(
                detector=_S.DetectorType.AVERAGE,
                filterbw=_S.FilterBWType.NOISE,
                start_frequency=5.7e9,
                stop_frequency=6.1e9,
                npoints=401,
                rbw=1e6,
                vbw=1e3,
                swt_auto=True,
                naverage=1)
        self.sac.set_settings(new)
        spectrum = self.sac.read_spectrum()
        self.assertIsInstance(spectrum, PowerSpectrum)
        self.sac.set_settings(old)

    def test_measure_spectrum(self):
        with consettings(self.sac, self.testset):
            spectrum = self.sac.measure_spectrum()
            self.assertIsInstance(spectrum, PowerSpectrum)


class TestSAControllerFSV(unittest.TestCase):
    def setUp(self):
        try:
            self.uri = os.environ["SA_URI"]
        except KeyError:
            self.uri = get_uris()['sa']
        self.sac = SAController.make(
                resources={'sa': self.uri})
        self.sac.initiate()

        self.narrow_set = SASettings(
                detector=SASettings.DetectorType.AVERAGE,
                filterbw=SASettings.FilterBWType.NOISE,
                start_frequency=5.7e9,
                stop_frequency=6.1e9,
                npoints=401,
                rbw=1e6,
                vbw=1e3,
                swt_auto=True,
                naverage=1)

        self.narrow_set = SASettings(
                detector=SASettings.DetectorType.RMS,
                filterbw=SASettings.FilterBWType.DB3,
                center_frequency=55e6,
                span=500e3,
                npoints=501,
                rbw=1e3,
                vbw=3e3,
                swt_auto=True,
                naverage=1)

    def test_set_span(self):
        spans = [100e3, 50e3, 1e6, 230e3]
        with consettings(self.sac, self.narrow_set):
            for span in spans:
                self.sac.set_span(span)
                qspan = self.sac.query_span()
                self.assertEqual(qspan, span)

    def test_query_data_swt_fast(self):
        """This only tests the querying capability when the swt is changed.
        It's not an exhaustive test of swt functionality in FSV.

        Should take less than 20 seconds to complete.
        """
        swts = (0, 0.1, 0.5, 1)   # ORDERED INCREASING
        reps = 10
        self._test_query_data_swt(swts, reps)

    @skipifnotslow
    def test_query_data_swt_300s(self):
        swt = 300
        self._test_query_data_swt([swt], 1)

    @skipifnotslow
    def test_query_data_swt_slow(self):
        """This tests sweep times 10s, 60s, and 600s, each of them 5 times in
        total.

        Checks the sanity of standard deviations, but no exhausting test for
        the distribution.
        """
        swts = (10, 60, 600)   # ORDERED INCREASING
        reps = 5
        self._test_query_data_swt(swts, reps)

    def _test_query_data_swt(self, swts, reps):
        with consettings(self.sac, self.narrow_set):
            self.sac.set_swt_auto(False)
            stds = []
            for swt in swts:
                logger.info(f"SWT: {swt}")
                _std_l = []
                for rep in range(reps):
                    self.sac.set_swt(swt)
                    sp = self.sac.measure_spectrum()
                    _std_l.append(np.std(sp.power))
                stds.append(np.mean(_std_l))

        # a quick sanity check on std decreasing behavior of swt
        stds_srtd = sorted(stds, reverse=True)  # sort decreasing
        self.assertListEqual(stds, stds_srtd)

    def tearDown(self):
        # To prevent opening multiple connections.
        self.sac.sa.close()
