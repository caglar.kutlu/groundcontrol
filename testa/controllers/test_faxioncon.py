import unittest
import os

import numpy as np
import numpy.fft as npfft
from numpy.testing import assert_allclose

from groundcontrol.controllers.faxioncon import FaxionBasebandGen


try:
    _ = os.environ['DEBUG']
    DEBUG = True
    import matplotlib.pyplot as plt
except KeyError:
    DEBUG = False


class TestFBGSanity(unittest.TestCase):
    def setUp(self):
        fcoffset = 10e3
        f_a = 5.9e9

        self.fbg = FaxionBasebandGen.make(f_a=f_a, fcoffset=fcoffset)

    def generate_psd(self, nsample: int):
        sig = self.fbg.generate(nsample)
        fdsig = npfft.fftshift(npfft.fft(sig))
        fdom = self.fbg.mkfreq(nsample)
        fdsigpsd = np.real(fdsig*np.conjugate(fdsig))
        return fdom, fdsigpsd

    def test_generate(self):
        n = 10000
        fdom, fdsigpsd = self.generate_psd(n)

        fdtruth = self.fbg.eval_fdist(fdom)

        # If both arrays are close index-to-index, assert_allclose returns None.
        if DEBUG:
            plt.plot(fdom, fdsigpsd, label='FDSIG')
            plt.plot(fdom, fdtruth, label='TRUTH')
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Power (a.u.)')
            plt.legend()
            plt.grid()
            plt.show()

        self.assertIsNone(assert_allclose(fdtruth, fdsigpsd, atol=1e-9))

    def test_energy_equals_1(self):
        # energy must be normalized to 1.
        n = int(1e6)
        fdom, fdsigpsd = self.generate_psd(n)
        energy = np.trapz(fdsigpsd, x=fdom)

        self.assertAlmostEqual(energy, 1)


if __name__ == "__main__":
    test = TestFBGSanity()
    test.setUp()
    f, psd = test.generate_psd(10000)
