# Snippet for resonance frequency estimation from JPA phase
# Caglar Kutlu (caglar.kutlu@gmail.com)


from functools import partial
import lmfit
import numpy as np
from scipy.constants import speed_of_light


_ptfe_phase_velocity = 0.695*speed_of_light


def resonator_phase_response_simple(
        f, fr, bw, phi0, elen, vp=_ptfe_phase_velocity,
        unit='deg'):
    """Phase response of an overcoupled resonator that doesn't have any
    coupling effect included.

    Args:
        fr: Resonance frequency.
        bw: Bandwidth.
        phi0: Phase offset.
        elen: Electrical length of the cables coming and out of resonator.
        vp: Phase velocity of the medium carrying signals.  Defaults to
            coaxial cable PTFE phase velocity.

    """
    if unit == 'deg':
        _factor = (360/(2*np.pi))
    else:
        _factor = 1
    a = 2*np.pi*(elen/vp)
    gamma = bw/2
    const = phi0
    lin = -a*f
    res = 2*np.arctan((fr/gamma)*(1-f/fr))
    phi = const + (lin + res)*_factor
    return phi


class ResonatorPhaseSimpleModel(lmfit.Model):
    """Implements the phase response model:
    ``phi = phi0 - (2*pi*L/v_p)*f + 2*atan((2*fr/bw)*(1-f/fr))``
    where:
        phi0:  Phase offset.
        L:  Total length of the transmission lines going in and out of the
            resonator.
        fr:  Resonance frequency.
        bw:  Resonance bandwidth.

    Args:
        unit: Unit of the model.  Can be one of 'deg' or 'rad'.

    Model Parameters:
        fr: Resonance frequency.
        bw: Bandwidth.
        phi0: Phase offset.
        elen: Electrical length of the cables coming and out of resonator.
        vp: Phase velocity of the medium carrying signals.  Defaults to
            coaxial cable PTFE phase velocity.

    """
    def __init__(self, unit='deg', *args, **kwargs):
        fun = partial(resonator_phase_response_simple, unit=unit)
        self.unit = unit
        super().__init__(fun, *args, **kwargs)
        self.set_param_hint('vp', vary=False)

    def guess(self, data, f=None, **kwargs):
        """Returns parameters guessed from the data.

        Args:
            data:  The phase data.
            f: Frequencies.
            **kwargs:  Dictionary with parameter names as keys and values as
                nominal values.
        """
        verbose = kwargs.pop('verbose', False)
        if f is None:
            return

        df = np.diff(f)  # should be always positive
        dfmin = df[df > 0].min()  # minimum step in f

        frmin = f.min()
        frmax = f.max()

        # Assume 10% of the span is bandwidth
        bw0 = kwargs.get('bw', 0.1*(frmax - frmin))

        # Assume sampling can actually resolve bandwidth.
        bwmin = dfmin
        bwmax = frmax - frmin

        # Assume the resonance is somewhere in the middle
        fr0 = kwargs.get('fr', np.median(f))

        # Assume 10m of cable length by default.
        elen0 = kwargs.get('elength', 10)
        elenmin = 0
        elenmax = 50

        vp0 = kwargs.get('vp', _ptfe_phase_velocity)
        vpmin = vpmax = vp0

        if self.unit == 'deg':
            pi = 180
        else:
            pi = np.pi

        phi00 = data[0] + (elen0/vp0)*2*pi*f[0] - pi
        phi0min = data[0] + (elenmin/vpmax)*2*pi*f[0] - pi
        phi0max = data[0] + (elenmax/vpmin)*2*pi*f[0] - pi

        parnames = 'fr bw phi0 elen vp'.split()
        pardict = dict(zip(parnames, (
                        (fr0, frmin, frmax),
                        (bw0, bwmin, bwmax),
                        (phi00, phi0min, phi0max),
                        (elen0, elenmin, elenmax),
                        (vp0, vpmin, vpmax))
                        ))

        if verbose:
            tmpl = "{s}=({tpl[0]}, {tpl[1]}, {tpl[2]})"
            lines = ["Guessing:"]
            for k, tpl in pardict.items():
                lines.append(tmpl.format(s=k, tpl=tpl))
            # logger.info("\n\t".join(lines))

        params = self.make_params()
        # The params may have added prefix.
        param_key_tmpl = f"{self.prefix}" + "{key}"
        for key, (val0, valmin, valmax) in pardict.items():
            pk = param_key_tmpl.format(key=key)
            if params[pk].vary:
                params[pk].set(value=val0, min=valmin, max=valmax)
            else:
                params[pk].set(value=val0)

        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)


phasemodel = ResonatorPhaseSimpleModel()


def estimate_resonance(
        self, frequencies, uphases,
        q_expected=None, fhint=None):
    """
    Note:
        q_expected for FJPA-UOT-1904-2.4G-V1 ~1000
        q_expected for FJPA-UOT-2010-7G-V1 is ~300

        q_expected need not be precise at all.
    """
    kwargs = {}
    if self.q_expected is not None:
        # rough estimate bandwidth using first measurement frequency.
        bw0 = frequencies[0]/self.q_expected
        kwargs['bw'] = bw0

    if fhint is not None:
        kwargs['fr'] = fhint

    pars = phasemodel.guess(uphases, frequencies, **kwargs)
    fit = phasemodel.fit(uphases, f=frequencies, params=pars)
    # logger.debug(fit.fit_report())
    bp = fit.params
    # If fit fails stderr will be None.
    fr, frerr = bp['fr'].value, bp['fr'].stderr
    return fr, frerr
