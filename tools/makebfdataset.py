"""Converts Bluefors LSCI Reader logfiles into a netcdf file."""
import typing as t
from pathlib import Path
from itertools import chain
from datetime import datetime

import xarray as xr
import numpy as np
import pandas as pd
from datargs import parse, argsclass, arg
from loguru import logger

from groundcontrol.csv import read_bf_logfile


@argsclass(description=__doc__)
class Opts:
    path: Path = arg(True)
    out: Path = arg(default=None)
    channels: t.Sequence[str] = arg(nargs='+', default=('6',))


def gendas(
        path: Path,
        ch: str = '6',
        dropdups: bool = True) -> t.Generator:
    if ch not in chain(['A'], map(str, range(1, 17))):
        raise ValueError("Ch can be 1-16 or A")

    for fl in path.rglob(f'CH{ch} T*.log'):
        logger.info(f'Reading {fl.name}.')
        lsd = read_bf_logfile(fl)
        da = lsd.to_xarray()
        if dropdups:
            dim = da.dims[0]
            index = da.get_index(dim)
            norig = len(index)
            dupindexmask = index.duplicated()  # array of bools, dups are True
            ndup = np.sum(dupindexmask)
            if ndup != 0:
                warntxt = f"Removing duplicate indexes: {ndup} out of {norig}"
                logger.warning(warntxt)
                # Select uniques
                da = da.sel({dim: ~dupindexmask})
        yield da


def np2dt(dtnp):
    # assuming <M8[ns] type for np.datetime64
    # dt = datetime.fromtimestamp(dtnp.astype(int)*1e-9)
    dt = pd.Timestamp(dtnp)
    return dt


def get_default_path(ds):
    tmin = np2dt(ds.timestamp.min().values)
    tmax = np2dt(ds.timestamp.max().values)

    tmintxt = tmin.strftime("%y-%m-%d")
    tmaxtxt = tmax.strftime("%y-%m-%d")

    fpath = Path(f"{tmintxt}_{tmaxtxt}.log.nc")
    return fpath


def main(opts: Opts) -> xr.Dataset:
    if len(opts.channels) > 1:
        dss_l = []
        for ch in opts.channels:
            dagen = gendas(opts.path, ch)
            dsch: xr.Dataset = xr.merge(dagen)
            dim = list(dsch.dims.keys())[0]
            logger.info(f"Sorting by {dim}.")
            dssch = dsch.sortby(dim)
            dss_l.append(dssch)
        dss = xr.concat(dss_l, dim='ch')
        dss.coords['ch'] = ('ch', opts.channels)
    else:
        # bw compatibility
        dagen = gendas(opts.path, opts.channels[0])
        ds: xr.Dataset = xr.merge(dagen)
        dim = list(ds.dims.keys())[0]
        logger.info(f"Sorting by {dim}.")
        dss = ds.sortby(dim)

    if opts.out is None:
        out = get_default_path(dss)
    try:
        dss.to_netcdf(out)
    except PermissionError as e:
        logger.warning(e)
        input("Did you forget to close the file? "
              "If yes, close and press Enter.")
        dss.to_netcdf(out)
    return dss


if __name__ == "__main__":
    ds = main(parse(Opts))
