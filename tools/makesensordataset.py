import typing as t
from pathlib import Path
from itertools import chain
from datetime import datetime

import xarray as xr
import pandas as pd
import numpy as np
from datargs import parse, argsclass, arg
from loguru import logger

from groundcontrol.analysis.tempcal import read_as_xarray


@argsclass
class Opts:
    rootdir: Path = arg(True, default=Path('.'))
    bflog: Path = arg(default=None, aliases=['-b'])
    outdir: Path = arg(default=Path('.'), aliases=['-o'])


def _first(s, axis):
    return np.take(s, 0, axis=axis[0])


def coarsen(ds: xr.Dataset):
    attrs = dict(ds.attrs)
    attrs.pop('parameter_info', None)
    logger.info(f"Coarsening dataset ({attrs})")
    logger.info(f"Before: {ds.dims}")
    dwell = ds.attrs['dwell']
    srate = ds.attrs['sampling_rate']
    nperdwell = int(np.floor(dwell*srate))
    resreacds = (ds[['resistance', 'reactance']]
                 .coarsen(
                     timestamp=nperdwell, boundary='trim', keep_attrs=True)
                 .mean(keep_attrs=True))
    maxds = (ds[['resistance_max']]
             .coarsen(timestamp=nperdwell, boundary='trim', keep_attrs=True)
             .max(keep_attrs=True))
    minds = (ds[['resistance_min']]
             .coarsen(timestamp=nperdwell, boundary='trim', keep_attrs=True)
             .min(keep_attrs=True))
    restds = (ds.drop(['resistance', 'reactance',
                       'resistance_min', 'resistance_max',
                       'ref_resistance', 'ref_reactance',
                       'ref_power', 'ref_temperature'])
              .coarsen(timestamp=nperdwell, boundary='trim', keep_attrs=True)
              .reduce(_first, keep_attrs=True))
    refds = (ds[['ref_resistance', 'ref_reactance',
                 'ref_power', 'ref_temperature']]
             .coarsen(
                 timestamp=nperdwell, boundary='trim', keep_attrs=True)
             .mean(keep_attrs=True))
    merged = xr.merge(
            [resreacds, minds, maxds, restds, refds],
            combine_attrs='no_conflicts')
    logger.info(f"After: {merged.dims}")
    return merged


def merge(sensds, refds, fillmethod='nearest'):
    logger.info(f"Aligning reference data (method={fillmethod}).")
    logger.info(f"Before: {refds.dims}")
    rds_re = refds.reindex_like(sensds, method=fillmethod)
    logger.info(f"After: {rds_re.dims}")
    logger.info("Merging reference and sensor data.")
    prefix = 'ref'
    namemap = {
        k: f'{prefix}_{k}'
        for k in refds.variables.keys()
        if k not in refds.dims}
    merged = xr.merge(
        [sensds, rds_re.rename(namemap)],
        combine_attrs='no_conflicts')
    return merged


def makemasks(ds, refds):
    tmin = ds.timestamp.min().values
    tmax = ds.timestamp.max().values
    tminref = refds.timestamp.min().values
    tmaxref = refds.timestamp.max().values

    # minimum covering window
    tminabs = max(tmin, tminref)
    tmaxabs = min(tmax, tmaxref)

    tmintxt = str(tminabs.astype('datetime64[s]')).replace('T', ' ')
    tmaxtxt = str(tmaxabs.astype('datetime64[s]')).replace('T', ' ')
    logger.info(f"Generating mask for values within {tmintxt} and {tmaxtxt}.")

    mask = (ds.timestamp >= tminabs) & (ds.timestamp <= tmaxabs)
    maskref = (refds.timestamp >= tminabs) & (refds.timestamp <= tmaxabs)

    return mask, maskref


def cutmask(ds, mask):
    ntotal = len(ds.timestamp)
    nvalid = np.sum(mask).values
    ndif = ntotal - nvalid
    logger.warning(f"Removing {ndif} rows out of {ntotal}.")
    newds = ds.where(mask).dropna(dim='timestamp', how='all')
    return newds


def proceed_single(ds, refds):
    mask, maskref = makemasks(ds, refds)
    logger.info("Processing sensor data.")
    dsnew = cutmask(ds, mask)
    logger.info("Processing reference data.")
    refdsnew = cutmask(refds, maskref)
    merged = merge(dsnew, refdsnew)
    mergedcs = coarsen(merged)
    return mergedcs


def proceed(path, name, refds, outdir):
    logger.info(f"Proceeding '{name.upper()}' raw set.")
    if isinstance(path, t.Iterable):
        dss = []
        for pth in path:
            logger.info(f"Reading '{pth!s}'.")
            _ds = read_as_xarray(pth)
            pds = proceed_single(_ds, refds)
            dss.append(pds)
        ds = xr.concat(dss, combine_attrs='drop_conflicts', dim='timestamp')
        logger.info(ds.dims)
    else:
        logger.info(f"Reading '{path!s}'.")
        ds = proceed_single(read_as_xarray(path), refds)

    logger.info(f"Saving '{name}.nc'")
    ds.to_netcdf(outdir/f"{name}.nc")
    return ds


def main():
    opts = parse(Opts)

    rootdir = opts.rootdir

    coolto4k = rootdir / "RAW/210310/TEMPCAL_COOLTO4K/"
    condense = map(lambda p: rootdir/p, [
            "RAW/210312/TEMPCAL_4K-0.1K",
            "RAW/210316/TEMPCAL_0.1K-0.03K",
            "RAW/210316/TEMPCAL_BASE",
            "RAW/210316/TEMPCAL_BASE2",
            ])
    stable = map(lambda p: rootdir/p, [
            "RAW/210317/TEMPCAL_11mK-22mK/",
            "RAW/210317/TEMPCAL_20mK/",
            "RAW/210317/TEMPCAL_22mK/",
            "RAW/210317/TEMPCAL_25mK/",
            "RAW/210317/TEMPCAL_27mK/",
            "RAW/210317/TEMPCAL_30mK-100mK/",
            "RAW/210317/TEMPCAL_100mK-620mK/",
            ])
    warmto4k = map(lambda p: rootdir/p, [
            "RAW/210317/TEMPCAL_WARMTO4K/",
            "RAW/210318/TEMPCAL_WARMTO4K_1K-4K/",
            ])
    sanity = rootdir/"RAW/210318/TEMPCAL_SANITY/"

    if opts.bflog is None:
        opts.bflog = next(rootdir.glob('*.log.nc'))

    reference = opts.bflog

    logger.info(f"Reading reference dataset '{reference!s}'.")
    refds = xr.open_dataset(reference)

    paths = [coolto4k, condense, stable, warmto4k, sanity]
    names = ['coolto4k', 'condense', 'stable', 'warmto4k', 'sanity']
    ds_dict = {}
    for path, name in zip(paths, names):
        ds_dict[name] = proceed(path, name, refds, opts.outdir)

    return ds_dict


if __name__ == "__main__":
    ds_dict = main()
