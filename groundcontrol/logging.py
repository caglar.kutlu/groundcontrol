"""Module for providing logging facilities to groundcontrol.

For simplicity there is currently only one logger for whole library, named
`logger`.  There are 2 default handlers:  one for stdout and one for file.
"""

import logging
from pathlib import Path
import typing as t
from datetime import datetime

from logging import DEBUG, INFO, CRITICAL, WARNING, ERROR


LogLevel = int  # type alias


def _default_fname():
    return f"{str(datetime.now())}.log"


def _setup_main_logger():
    logger: logging.Logger = logging.getLogger('main')
    logger.setLevel(logging.DEBUG)

    # STDOUT HANDLER
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    streamformatter = logging.Formatter(
            '[%(asctime)s|%(levelname)s] %(message)s', "%H:%M:%S")
    ch.setFormatter(streamformatter)
    logger.addHandler(ch)
    return logger, ch


logger, _mainch = _setup_main_logger()


def set_stdout_loglevel(level: LogLevel):
    _mainch.setLevel(level)


def setup_logger(name: str, loglevel: LogLevel = DEBUG):
    """Sets up a logger with provided `name`.

    Args:  Name to use when referring to the logger.
    """
    logger = logging.getLogger(f"main.{name}")
    logger.setLevel(loglevel)

    return logger


def create_log(
        fname: t.Optional[t.Union[Path, str]] = None,
        level: LogLevel = logging.DEBUG) -> logging.FileHandler:
    """Creates a new file with the given `fname` for logging.

    If `fname` is `None`, the created log file will have the name with the
        return value of ``str(datetime.datetime.now())``.
    """
    if fname is None:
        fname = _default_fname()
    fh = logging.FileHandler(fname)
    fh.setLevel(level)
    fileformatter = logging.Formatter(
            '[%(asctime)s|%(levelname)s|%(name)s] %(message)s')
    fh.setFormatter(fileformatter)
    logger.addHandler(fh)
    return fh
