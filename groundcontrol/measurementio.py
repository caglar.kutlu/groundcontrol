"""A module defining the measurement saving operations."""
import typing as t
from pathlib import Path
from datetime import datetime

import attr
import pandas as pd
from parse import parse

from groundcontrol.measurement import MeasurementModel, read_measurement_csv
from groundcontrol.measurement import _appending_suffix
from groundcontrol.helper import makecounter
from groundcontrol.logging import logger
from groundcontrol.util import unique, isoformat2datetime


class MeasurementIOError(IOError):
    pass


class MeasurementOperationNotPermitted(IOError):
    pass


class MeasurementExistsError(MeasurementIOError):
    pass


class MeasurementOpenError(MeasurementIOError):
    pass


class MeasurementNotFoundError(MeasurementIOError):
    pass


@attr.s(auto_attribs=True)
class OpenMode:
    readable: bool
    writable: bool
    exclusive: bool
    appending: bool

    @classmethod
    def from_str(cls, s: str):
        """Returns the a mode object from the given string.  Accepted mode
        strings are as follows:
            'r':  Open for reading.
            'w':  Open for writing.
            'w+': Open for reading and writing.
            'x':  Open for exclusive creation, fail if file already exist.
            'a':  Open for writing, appending to the end.
        """
        r, w, x, a = (False, False, False, False)

        if s == 'r':
            r = True
        elif s == 'w':
            w = True
        elif s == 'w+':
            w = True
            r = True
        elif s == 'x':
            x = True
            w = True
        elif s == 'a':
            a = True
            w = True
        else:
            m = "Must have exactly one of ['r', 'w', 'x', 'a' or 'w+']."
            raise ValueError(m)
        return cls(r, w, x, a)


@attr.s(auto_attribs=True)
class MIOBase:
    """A class defining the interface for measurement streams.
    The interface is loosely based on io.IOBase.
    """
    _closed: bool
    _mode: OpenMode

    @property
    def closed(self) -> bool:
        return self._closed

    @property
    def mode(self) -> OpenMode:
        return self._mode

    def open(self):
        raise NotImplementedError

    def close(self):
        self._closed = True

    def check_writable(self):
        if self._closed:
            raise MeasurementIOError("Measurement stream is closed.")

        if not self._mode.writable:
            raise MeasurementOperationNotPermitted("Can not write.")

    def write(self, obj):
        raise NotImplementedError

    def read(self) -> MeasurementModel:
        raise NotImplementedError

    def check_readable(self):
        if self._closed:
            raise MeasurementIOError("Measurement stream is closed.")

        if not self._mode.readable:
            raise MeasurementOperationNotPermitted("Can not read.")

    def __del__(self):
        self.close()

    @staticmethod
    def _preopen(path: t.Union[str, Path], mode: str):
        mode = OpenMode.from_str(mode)
        if mode.appending:
            raise NotImplementedError(
                    "Appending mode is not implemented yet.")

        path = Path(path).expanduser()
        if mode.exclusive and path.exists():
            msg = f"Measurement exists at path: '{str(path)}'"
            raise MeasurementExistsError(msg)

        return mode, path


@attr.s(auto_attribs=True)
class MIOFileCSV(MIOBase):
    """A streaming class for saving measurements as separate csv files.

    Measurements are stored together with the measurements belonging to the
    same class.

    When saving a measurement with an `mref` attribute set to None, the `mref`
    field in the output file name will be changed to NOREF.

    Attributes:
        path:  Must be a valid Path instance
        name_template:  Can be constructed using {abbrev}, {index} and {mref}
            as placeholders.  The valid separators are only "_" and " " for
            now.  Separators at the end and the beginning will be stripped off.

    """
    path: Path

    name_template: str = "{abbrev}_{mref}_{index}.csv"
    indexgen: t.Iterator[int] = attr.Factory(makecounter(0, 1))
    _indexpaths: t.Dict[int, Path] = attr.Factory(dict)
    _indexfpath: Path = attr.Factory(
        lambda self: self.path / "measurements.index", takes_self=True)

    _mref_default: t.ClassVar = "NOREF"
    _appending: t.Dict[str, Path] = attr.Factory(dict)

    def _format_fname(self, abbrev, index, mref, appending: bool = False):
        if mref is None:
            mref = self._mref_default

        if not appending:
            fname = self.name_template.format(
                    abbrev=abbrev, mref=mref, index=index)
        else:
            abbrev = f"{abbrev}-{_appending_suffix}"
            fname = self.name_template.format(
                    abbrev=abbrev, mref=mref, index=index)
        return fname

    def write(self, measurement: MeasurementModel) -> str:
        self.check_writable()
        abbrev = type(measurement).abbrev
        mref = measurement.mref

        # mpath is the directory for the particular measurement class
        mpath = self.path / abbrev

        if mpath.exists():
            if not mpath.is_dir():
                msg = ("Measurement class folder can not be opened.  Check if "
                       "a non-directory folder with the name {str(mpath)} "
                       "exist.")
                raise MeasurementIOError(msg)
        else:
            mpath.mkdir()

        index = next(self.indexgen)

        fname = self._format_fname(
                abbrev=abbrev,
                index=index,
                mref=mref)
        fullpath = mpath / fname
        measurement.to_csv(fullpath)

        # Store the index
        self._indexpaths[index] = fullpath
        self.append_index(measurement, index, fullpath)
        return index

    def append(self, measurement: MeasurementModel):
        """Primitive appending implementation.  Note that this will fail
        horribly if the MeasurementModel is not table-compatible.
        """
        self.check_writable()
        abbrev = type(measurement).abbrev
        mref = measurement.mref

        # mpath is the directory for the particular measurement class
        mpath = self.path / abbrev

        if mpath.exists():
            if not mpath.is_dir():
                msg = ("Measurement class folder can not be opened.  Check if "
                       "a non-directory folder with the name {str(mpath)} "
                       "exist.")
                raise MeasurementIOError(msg)
        else:
            mpath.mkdir()

        index = next(self.indexgen)

        # Search for mref in the store
        try:
            fullpath = self._appending[mref]
            measurement.to_csv(fullpath, index=index, append=True)
        except KeyError:
            fname = self._format_fname(abbrev, index, mref, appending=True)
            fullpath = mpath / fname
            logger.debug(f"Creating appendable measurement at: {fullpath}.")
            measurement.to_csv(fullpath, index=index)
            self._appending[mref] = fullpath

        # Store the index
        self._indexpaths[index] = fullpath
        self.append_index(measurement, index, fullpath)

    def read(self, index: int, ignore_multiple=False) -> MeasurementModel:
        self.check_readable()
        fullpath = self._indexpaths[index]
        return read_measurement_csv(fullpath)

    def read_iter(
            self,
            filter: t.Optional[t.Callable[[t.Dict], bool]] = None,
            ) -> t.Iterator[t.Tuple[datetime, MeasurementModel]]:
        """Yields measurements that satisfy the filter.

        Filter must be a function with signature `dict -> bool`.  The usable
        dictionary keys are the columns of a measurement index:
            {'index', 'timestamp', 'mclass', 'datapath'}

        Returns:
            it:  An iterator of tuples. First element of the tuple is timestamp
                and the second is the measurement object.

        """
        midf = self.read_measurement_index_df()
        for idx, row in midf.iterrows():
            d = row.to_dict()
            if filter is not None:
                pred = filter(d)
            else:
                pred = True
            if pred:
                meas = self.read(idx)
                timestamp = d['timestamp']
                yield (timestamp, meas)
            else:
                continue

    def append_index(
            self,
            measurement: MeasurementModel,
            index, path):
        try:
            timestamp = measurement.timestamp
        except AttributeError:
            timestamp = datetime.now()

        mclassname = type(measurement).measurement_class
        relativepath = path.relative_to(self.path)
        row = ", ".join((
                str(index),
                str(timestamp),
                mclassname,
                str(relativepath)))

        with open(self._indexfpath, mode='a') as fout:
            fout.write(f"{row}\n")

    def _readpath(self, s):
        return self.path / Path(s)

    def parse_path(self, path: Path) -> t.Tuple:
        pdir = path.parents[0]
        fname = path.name
        parsed = parse(self.name_template, fname)
        abbrev, mref, index = tuple(
                parsed[k] for k in ('abbrev', 'mref', 'index'))
        return pdir, abbrev, mref, index

    def _isappendingpathname(self, path: Path):
        _, abbrev, _, _ = self.parse_path(path)
        return abbrev.endswith(f"-{_appending_suffix}")

    def read_measurement_index(self) -> t.Dict[str, t.Any]:
        """Reads the measurement index and returns it as a dict-of-dicts with the
        format {column-label: {index: column-values}.  Column label is one of
        ['timestamp', 'mclass', 'datapath']
        """
        colnames = ['index', 'timestamp', 'mclass', 'datapath']
        converters = dict(
                zip(
                    colnames,
                    [int, isoformat2datetime, str, self._readpath]))
        df = pd.read_csv(
                self._indexfpath,
                index_col=0,
                names=colnames,
                converters=converters,
                skipinitialspace=True
                )

        dct = df.to_dict()
        return dct

    def read_measurement_index_df(self) -> t.Dict[str, t.Any]:
        """Reads the measurement index and returns it as a pandas DataFrame.
        """
        colnames = ['index', 'timestamp', 'mclass', 'datapath']
        converters = dict(
                zip(
                    colnames,
                    [int, isoformat2datetime, str, self._readpath]))
        df = pd.read_csv(
                self._indexfpath,
                index_col=0,
                names=colnames,
                converters=converters,
                skipinitialspace=True
                )

        return df

    @classmethod
    def open(cls, path: t.Union[str, Path], mode: str):
        mode, path = cls._preopen(path, mode)
        if path.exists() and not path.is_dir():
            raise MeasurementOpenError("Given path points to a file.")
        if mode.writable and not path.exists():
            path.mkdir()

        mio = cls(closed=False, mode=mode, path=path)

        if mode.readable:
            mindex_d = mio.read_measurement_index()
            mio._indexpaths = mindex_d['datapath']
            lastindex = max(list(mio._indexpaths.keys()))
            mio.indexgen = makecounter(lastindex, 1)
            paths = mio._indexpaths.values()
            # Appending measurements will have the same path
            uniqpaths = unique(paths)

            # Filling up the appending measurement dict.
            for path in uniqpaths:
                _, abbrev, mref, _ = mio.parse_path(path)
                if abbrev.endswith(f"-{_appending_suffix}"):
                    mio._appending[mref] = path

        return mio
