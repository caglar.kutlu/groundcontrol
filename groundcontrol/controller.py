"""This module provides abstractions for writing controller classes, along with
concrete examples.

Usage example::

    from groundcontrol.controller import Controller, instrument
    from groundcontrol.instruments import SCPISignalGenerator

    RESOURCE = "TCPIP::10.10.1.103::inst0::INSTR"

    class MyController(Controller):
        sg1: SCPISignalGenerator = instrument(RESOURCE, "Signal Generator 1")

        def myoperation(self):
            self.sg1.set_frequency(42)

    c = MyController.make()
    c.myoperation()

MAINLY WORK IN PROGRESS, THINGS MAY CHANGE!

"""
import typing as t
from numbers import Number
import time
import inspect

import attr

from groundcontrol import InstrumentManager
from groundcontrol.instrumentmanager import ResourceNotFoundError
from groundcontrol.instruments import Instrument
from groundcontrol.logging import logger


def within(x: Number, a: Number, b: Number):
    return (x > a) and (x <= b)


class _ControllerMeta(type):
    """Metaclass for making derived classes automagically attrs decorated."""
    def __new__(cls, clsname, supers, attrdict):
        # super() is ensured to return `type` here.
        newcls = super().__new__(cls, clsname, supers, attrdict)
        return attr.s(auto_attribs=True)(newcls)


def instrument(
        resource_addr: t.Optional[str] = None,
        nicename: t.Optional[str] = None,
        bindmap: t.Optional[t.Dict[str, str]] = None,
        required: bool = False):
    """Returns an instrument attribute field to be used in an attrs decorated
    class.

    Args:
        resource_addr:  (optional) A default resource address for the
            instrument.  Usually this will be a VISA resource string.
        nicename:  (optional) A unique nice name for referring to the
            instrument in texts.
        bindmap:  (optional) Binds uri names to member instrument names. 
            {Variable name -> Instrument name}. Intended to be used with
            `Controller` subclasses.  This is experimental API.
        required:  Whether or not the instrument is a must-have.
    """
    # TODO: bindmap should really be a bidict

    metadata = dict(
            metatype='instrument',
            resource_addr=resource_addr,
            nicename=nicename,
            bindmap=bindmap,
            required=required)
    return attr.ib(metadata=metadata, default=None)


def _isinstrumentfield(attribute, value):
    return attribute.metadata['metatype'] == 'instrument'


def instruments(obj) -> t.Dict[str, Instrument]:
    """Given an instance of Controller subtype, returns the instruments as a
    dictionary."""
    return attr.asdict(obj, filter=_isinstrumentfield)


def instrument_fields(cls) -> t.Dict[str, t.Dict[str, t.Any]]:
    """Given a subclass of Controller as `cls`, returns a dictionary with keys
    as instrument names and values as dictionaries containing keys
    ['instrument_type', 'resource_addr', 'nicename']
    """
    fields = attr.fields_dict(cls)

    # Translate attr.s attribute to our dictionary form

    instr_fields = {}
    for name, attribute in fields.items():
        try:
            if attribute.metadata['metatype'] != 'instrument':
                # filtering the instrument fields
                continue
        except KeyError:
            continue
        field = {}
        meta = attribute.metadata
        field['instrument_type'] = attribute.type
        field['resource_addr'] = meta['resource_addr']
        field['nicename'] = meta['nicename']
        field['bindmap'] = meta['bindmap']
        field['required'] = meta['required']
        instr_fields[name] = field

    return instr_fields


def _required_instruments(insflds=None) -> t.List[str]:
    if insflds is None:
        insflds = instrument_fields(self)
    return list(
            filter(lambda k: insflds[k]['required'], insflds.keys()))


class Controller(metaclass=_ControllerMeta):
    """Base class for all controllers.

    The derived classes can use `instrument` function to declare instrument
    fields.

    """
    instrument_manager: InstrumentManager
    allow_not_found_instrument: bool
    _resources: t.Dict[str, str]
    _resource_classes: t.Dict[str, t.Type[Instrument]]

    _initiated: bool = False

    @property
    def initiated(self):
        return self._initiated

    def _initiate_instrument(self, name, props, bindname=None):
        ra = self._resources
        uriname = bindname if bindname is not None else name
        # if _resource_classes is filled, use the class from there
        # use the type from instrument property otherwise.
        instype = self._resource_classes.get(uriname, props['instrument_type'])

        default_addr = props['resource_addr']
        nicename = props['nicename']
        nicename = name if nicename is None else nicename

        instr = getattr(self, name)
        if instr is not None:
            logger.info(f"Skipping '{nicename}'. Already exists.")
            return

        # Handle instrument being a controller
        if inspect.isclass(instype) and issubclass(instype, Controller):
            try:
                instr = instype.make(
                        self.instrument_manager,
                        ra,
                        self.allow_not_found_instrument)
                bindmap = props['bindmap']
                instr.initiate(bindmap)
                setattr(self, name, instr)
            except ResourceNotFoundError as e:
                if props['required']:
                    raise ResourceNotFoundError(
                            f"Controller {name} is missing "
                            "a required resource.") from e
                else:
                    setattr(self, name, None)
            return

        addr = ra.get(uriname, default_addr)
        if addr is None:
            if props['required']:
                raise ResourceNotFoundError(
                        f"'{nicename}' is required but no"
                        " address is provided.")
            else:
                logger.info(
                        f"Skipping instrument '{nicename}'. No address"
                        " provided and it's not required.")
                return

        im = self.instrument_manager
        logger.info(f"Initiating '{nicename}' at '{addr}'")
        try:
            instr = im.create_instrument(name, addr, instype)
        except ResourceNotFoundError as e:
            if self.allow_not_found_instrument:
                logger.info(f"Instrument '{nicename}' was not found.")
            else:
                raise e

        setattr(self, name, instr)

    def initiate(self, bindmap: t.Optional[t.Dict] = None):
        """Makes the connection with the instruments and calls __setup__."""
        instfields = instrument_fields(type(self))
        for name, props in instfields.items():
            if bindmap is not None:
                self._initiate_instrument(name, props, bindname=bindmap[name])
            else:
                self._initiate_instrument(name, props)
        self.__setup__()
        self._initiated = True

    def __setup__(self):
        """This is called right after initiation, override this if needed."""
        pass

    def _check_connection(self, instrument: Instrument):
        """Checks the connectivity of the instrument referred by `name`."""
        idn = instrument.query_idn()
        # raises exception if idn doesn't respond
        logger.info(f"{instrument.name} :: Connection ok! (IDN: {idn})")

    def check_connections(self):
        insts = instruments(self)
        for inst in insts.values():
            self._check_connection(inst)

    def _sleep(self, seconds: float):
        """A blocking pause of `seconds` s."""
        return time.sleep(seconds)

    _ResourceType = t.Union[str, t.Tuple[str, t.Type[Instrument]]]

    @classmethod
    def make(
            cls,
            im: InstrumentManager = None,
            resources: t.Dict[str, _ResourceType] = None,
            allow_not_found_instrument: bool = False):
        """Creates the Controller object.

        Args:
            im: InstrumentManager instance to use when creating the objects.  A
                new instance is created if `None`.

            resources:  A dictionary mapping the instrument names to
                the addresses of their resources.  Optionally can be a
                dictionary mapping to tuples where the tuples first element is
                the resource address and the second element is the particular
                `Instrument` class to be used when constructing the
                instrument.

            allow_not_found_instrument:  Suppresses exceptions related to
                errors when creating the instruments if set to `True`.
                Defaults to `False`.

        """
        im = InstrumentManager.make() if im is None else im
        resources = {} if resources is None else resources
        classes = {}

        for k, v in resources.items():
            try:
                rscaddr, inscls = v
            except ValueError:
                rscaddr = v
                inscls = None
            resources[k] = rscaddr
            if inscls is not None:
                classes[k] = inscls

        return cls(
                im,
                resources=resources, resource_classes=classes,
                allow_not_found_instrument=allow_not_found_instrument)
