"""A module containing measurement data related abstractions.

TODO:
    - Type annotations for arrays must be improved.  Currently, no concrete
      type for the elements is provided.
    - The repr field of parameters and quantities should be renamed to
      `nicename`

"""

import typing as t
import re
from numbers import Number
from itertools import chain
from datetime import datetime
import json

import attr
import numpy as np
import xarray as xr

from .units import UnitDefinition
from . import units as un
from .util import angle_deg, mag_db_volt, unwrap_deg
from .util import dbm2watt, watt2dbm, isoformat2datetime
from .helper import shorten, arbitrary, default_serializer
from groundcontrol.friendly import mprefix_str
from groundcontrol.csv import write_csv, read_csv, read_csv_meta
from groundcontrol.logging import setup_logger
from groundcontrol.helper import Array


ALWAYS_IGNORE_UNITS = True


logger = setup_logger(__file__)


_appending_suffix = "AP"
"""suffix to be used with appending types"""


_mclass_registry = {}
"""A registry for measurement classes"""


def _isiterable(obj):
    return isinstance(obj, t.Iterable)


def register_measurement(cls, clsname: t.Optional[str] = None):
    """Registers the given measurement class into the internal registry using
    `clsname` argument as the key.  If `clsname` is not given or `None`, the
    the `__name__` attribute of the class is used as key.

    WARNING: This function does not perform any checks for existence currently.

    """
    if clsname is None:
        _mclass_registry[cls.__name__] = cls
    else:
        _mclass_registry[clsname] = cls


def deregister_measurement(clsname: str):
    """Deregisters a measurement class.

    WARNING: This function does not perform any checks for existence currently.

    """
    del _mclass_registry[clsname]


def get_measurement_class(mname: str):
    """Returns the measurement class object referred in the registry by
    `mname`

    """
    return _mclass_registry[mname]


def list_measurement_class() -> t.Iterator[str]:
    """Returns a list of names for measurement classes currently registered in
    the _mclass_registry.

    """
    return _mclass_registry.keys()


def list_measurement_class_abbrev() -> t.Iterator[str]:
    """Returns a list of abbreviations for measurement classes currently
    registered in the _mclass_registry.

    """
    return map(lambda mc: mc.abbrev, _mclass_registry.values())


@attr.s(auto_attribs=True, frozen=True)
class Quantity:
    name: str
    value: t.Any
    unit: UnitDefinition
    repr: str
    description: str

    def pretty(self):
        q = self
        if q.unit.shortname.strip() != "":
            unitstr = f"[{q.unit.shortname:s}]"
        else:
            unitstr = ""
        valuestr = f"{q.value}{unitstr}"
        return valuestr

    @property
    def shape(self) -> t.Tuple:
        """Returns the shape of the value object."""
        return np.shape(self.value)

    @property
    def header_repr(self):
        """Returns the string representation for the header."""
        if self.unit != un.nounit:
            return f"{self.name} [{self.unit}]"
        else:
            return f"{self.name}"

    @staticmethod
    def isquantity(obj: attr.Attribute):
        """Checks whether given `Attribute` is a `Quantity`"""
        return obj.metadata.get("metatype") == "quantity"

    @staticmethod
    def iscompatible(obj: attr.Attribute):
        """Checks whether a given attribute is an attribute of this type."""
        return Quantity.isquantity(obj)

    @classmethod
    def make(
        cls,
        name: str,
        value: t.Any,
        unit: UnitDefinition = un.nounit,
        repr: t.Optional[str] = None,
        description: str = "",
    ):
        """Factory method to create an instance of this class."""
        if repr is None:
            repr = name
        return cls(
            name=name, value=value, unit=unit, repr=repr, description=description
        )


@attr.s(auto_attribs=True, frozen=True)
class Parameter:
    name: str
    value: t.Any
    unit: UnitDefinition
    repr: str
    priority: int
    description: str

    def pretty(self):
        p = self
        if p.unit.shortname.strip() != "":
            unitstr = f" {p.unit.shortname:s}"
        else:
            unitstr = ""
        if isinstance(p.value, str):
            valuestr = f"'{p.value}'{unitstr}"
        else:
            valuestr = f"{p.value}{unitstr}"
        return valuestr

    @staticmethod
    def isparameter(obj: attr.Attribute):
        """Checks whether given `Attribute` is a `Parameter`"""
        return obj.metadata.get("metatype") == "parameter"

    @staticmethod
    def iscompatible(obj: attr.Attribute):
        """Checks whether a given attribute is an attribute of this type."""
        return Parameter.isparameter(obj)

    @classmethod
    def make(
        cls,
        name: str,
        value: t.Any,
        unit: UnitDefinition = un.nounit,
        repr: t.Optional[str] = None,
        priority: int = 10,
        description: str = "",
    ):
        """Factory method to create an instance of this class."""
        if repr is None:
            repr = name
        return cls(
            name=name,
            value=value,
            unit=unit,
            repr=repr,
            priority=priority,
            description=description,
        )


def quantity(
    unit: UnitDefinition,
    repr: t.Optional[str] = None,
    isoptional: bool = False,
    default: t.Any = None,
    description: str = "",
):
    """Used to define a quantity attribute in attr.s enabled classes.

    Quantity is a measured entity, whether it's a field or some configuration
    of the system.

    Arguments:
        unit:  Unit as a UnitDefinition object.
        repr:  A string representation for the quantity's name.
        isoptional:  Whether or not quantity can assume default values.
        default:  If `isoptional` is true, the default value is provided using
            this argument.
    """
    meta = dict(metatype="quantity", unit=unit, repr=repr, description=description)
    if isoptional:
        return attr.ib(metadata=meta, default=default)
    else:
        return attr.ib(metadata=meta)


def parameter(
    unit: UnitDefinition,
    repr: t.Optional[str] = None,
    priority: int = 10,
    description: str = "",
    default: t.Optional[t.Any] = None,
    nodefault: bool = False,
):
    """Defines a parameter for the attr.s enabled classes.

    A parameter is different than a quantity in the sense that it is a fixed
    value of the measurement process.  Thus, the relevant configurations of the
    measurement device can be parameters of the measurement, as well as the
    sweep parameter of a sweep kind of measurement.

    The priority is useful when one wants to systematically represent
    measurement parameters in strings.  For example, one can choose whether to
    show a parameter or not depending on the priority value.

    Arguments:
        unit:  Unit as a UnitDefinition object.
        repr:  A string representation for the quantity's name.
        priority:  Determines the importance of a parameter.  It is ordered in
            a way that priority decreases with higher numbers.
        description:  A detailed description about the parameter if necessary.
        default:  A default value for the parameter, defaults to `None`.
        nodefault:  Do not set the default value.
    """
    meta = dict(
        metatype="parameter",
        unit=unit,
        repr=repr,
        priority=priority,
        description=description,
    )
    if nodefault:
        return attr.ib(metadata=meta)
    else:
        return attr.ib(metadata=meta, default=default)


def _metattributes_helper(obj, metaattrtype):
    """Helper function for defining functions that return attributes of a
    particular type such as Quantity or Parameter

    Arguments:
        obj:  An instance of `MeasurementModel`.
        filter:  A function which selects the

    TODO:
        - Type signature of this?

    """
    if not attr.has(obj):
        raise TypeError("Incompatible type for metaattributes.")

    def filter_(attribute, value):
        return metaattrtype.iscompatible(attribute)

    valdct = attr.asdict(obj, filter=filter_)
    cls = obj.__class__
    fielddct = attr.fields_dict(cls)

    out = {}
    for k, v in valdct.items():
        meta = fielddct[k].metadata
        metaattrs = attr.fields_dict(metaattrtype)
        metaattrs.pop("value")
        metaattrs.pop("name")
        metaattrkeys = metaattrs.keys()
        # We collect the key-value in the attr metadata dictionary
        # that correspond to the fields in the metaattrtype.
        q = metaattrtype.make(
            name=k, value=v, **{metak: meta[metak] for metak in metaattrkeys}
        )
        out[k] = q

    return out


def quantities(obj) -> t.Dict[str, Quantity]:
    """Returns the attributes labeled as quantity in an attr style object.

    Arguments:
        obj:  An instance of `MeasurementModel`.

    Returns:
        out:  Dictionary of key - quantity pairs.  Quantity is an object with
            fields 'name', 'value', 'unit' and 'repr', 'description'.
    """

    out = _metattributes_helper(obj, Quantity)
    return out


def parameters(obj) -> t.Dict[str, Parameter]:
    """Returns the attributes labeled as quantity in an attr style object.

    Arguments:
        obj:  An instance of `MeasurementModel`.

    Returns:
        out:  Dictionary of key - parameter pairs.  Parameter is an object with
            fields 'name', 'value', 'unit', 'repr', 'priority' and
            'description'.
    """

    out = _metattributes_helper(obj, Parameter)
    return out


def _fields_helper(
    cls, fieldtype: t.Union[Quantity, Parameter]
) -> t.List[t.Dict[str, t.Any]]:
    """Given a ``MeasurementModel`` compatible class, returns a list of
    dictionaries containing the desired attribute type's static attributes (all
    attributes except value).

    """

    fields = attr.fields(cls)

    if fieldtype == Quantity:
        parnames = ("unit", "repr")
    elif fieldtype == Parameter:
        parnames = ("unit", "repr", "priority", "description")
    else:
        raise TypeError("Inspected field type is unrecognized.")

    lst = []
    for attrfield in fields:
        if not fieldtype.iscompatible(attrfield):
            # Process only relevant fields
            continue
        metadata = attrfield.metadata
        d = {k: metadata[k] for k in parnames}
        d["name"] = attrfield.name
        d["type"] = attrfield.type
        lst.append(d)

    return lst


def quantity_fields(cls) -> t.List[t.Dict[str, t.Any]]:
    """Returns a dictionary containing meta information on the quantity fields
    belonging to the measurement class.  The dictionaries contain all but the
    value attribute.

    Arguments:
        obj:  An instance of `MeasurementModel`.

    Returns:
        out:  List of dictionaries mapping static quantity fields to values.
            The dictionary keys are ('name', 'type', 'unit', 'repr')

    """
    return _fields_helper(cls, Quantity)


def parameter_fields(cls) -> t.List[t.Dict[str, t.Any]]:
    """Returns a dictionary containing meta information on the parameter fields
    belonging to the measurement class.  The dictionaries contain all but the
    value attribute.

    Arguments:
        obj:  An instance of `MeasurementModel`.

    Returns:
        out:  List of dictionaries mapping static parameter fields to values.
            The dictionary keys are ('name', 'type', 'unit', 'repr',
            'priority')

    """
    return _fields_helper(cls, Parameter)


def _shorten_mclass(name: str):
    """Given a string like "MyIncredibleModel", shortens it to MIM
    If there is a conflicting measurement class name, includes the second
    non-capital letter in the shortened name, i.e. MyIM for above example.
    Continues to include more lower-case letters until a unique name is found.

    Raises:
        KeyError:  If a unique name was not found.

    """
    # Defines the max-trials of trying to find a unique shortened name.
    MAXTRIAL = 10

    for trial in range(MAXTRIAL):
        d = trial
        shortened = shorten(name, d)
        if shortened in map(lambda mc: mc.abbrev, _mclass_registry.values()):
            continue
        else:
            return shortened

    raise KeyError(f"Unique abbreviation not possible ({name}).")


def _is_row_compatible(cls):
    """Returns true if the measurement can be represented as a row in a table."""
    qfs = quantity_fields(cls)
    compatible_classes = [Number, str]  # This is
    try:
        return all(
            any(issubclass(qf["type"], cc) for cc in compatible_classes) for qf in qfs
        )
    except TypeError:
        # for qf in qfs:
        #     logger.debug('qf['type'])')
        # This usually happens when we use types from typing module
        return False


class _MeasurementModelMeta(type):
    """This is a metaclass to automatically register every measurement model
    deriving from MeasurementModel to be automatically registered, using the
    class name as the measurement class string.

    There may be a way to do this without resorting to metaclasses.
    """

    def __new__(cls, clsname, supers, attrdict):
        newcls = super().__new__(cls, clsname, supers, attrdict)

        # Decorate with attr.s
        decorated = attr.s(auto_attribs=True)(newcls)

        # Adding abbreviation
        abbrev = _shorten_mclass(clsname)
        decorated._abbrev = abbrev

        # Checking if row compatible
        rowlike = _is_row_compatible(decorated)
        decorated._rowlike = rowlike

        # Register the class
        register_measurement(decorated, clsname)
        return decorated

    @property
    def parameter_fields(cls) -> t.List[t.Dict[str, t.Any]]:
        return parameter_fields(cls)

    @property
    def quantity_fields(cls) -> t.List[t.Dict[str, t.Any]]:
        return quantity_fields(cls)

    @property
    def measurement_class(cls) -> str:
        """This becomes a "class property" returning the valid measurement name
        of the MeasurementModel"""
        return cls.__name__

    @property
    def abbrev(cls) -> str:
        try:
            return cls._abbrev
        except AttributeError:
            cls._abbrev = cls.measurement_class  # no abbreviation.
            return cls.abbrev

    @property
    def rowlike(cls) -> bool:
        return cls._rowlike


class MeasurementModel(metaclass=_MeasurementModelMeta):
    """Base class for measurement models.  Every derived class must have a
    unique measurement class name.  Currently this is not enforced so you must
    make sure you provide this.

    When defining new attributes, you must always annotate the type.  The valid
    types are currently restricted to: all primitive number types and numpy
    arrays.  The class does not perform check for this, but the program may
    behave unexpectedly.

    Attributes:
        mref (str):  (Optional) Reference string.  Derived classes can include
            it as an optional attribute.  Must return `None` if not set.
        abbrev (str):  (Optional)  A readable abbreviation for
            representing the measurement model.  This need not be unique.
    """

    @property
    def mref(self):
        try:
            return self._mref
        except AttributeError:
            return None

    @mref.setter
    def mref(self, value: str):
        self._mref = value

    def pretty(self, showpars: bool = True) -> str:
        """Returns the pretty printed text for the measurement model.  MREF is
        also shown if it is not `None`.
        """
        pars = parameters(self)
        qs = quantities(self)
        mref = self.mref
        header = f"{self.__class__.measurement_class}"
        if mref is not None:
            header = f"{header} ({mref})"
        ll = [header]
        ll.append("::QUANTITIES::")
        for k, v in qs.items():
            txt = f"\t{v.repr}: {v.value} {v.unit}"
            ll.append(txt)
        if showpars:
            ll.append("::PARAMETERS::")
            for k, v in pars.items():
                txt = f"\t{v.repr}: {v.value} {v.unit}"
                ll.append(txt)
        return "\n".join(ll)

    def to_dict(self):
        """Returns the measurement as a dictionary with the fields:
        ["MCLASS", "MREF", "PARAMETERS", "QUANTITIES]"""
        mclass = type(self).measurement_class
        mref = self.mref

        # Getting the params
        params = parameters(self)

        # Getting the quantities
        quants = quantities(self)
        return dict(MCLASS=mclass, MREF=mref, PARAMETERS=params, QUANTITIES=quants)

    def _to_csv_append(self, fname: str, index):
        qs = self.to_dict()["QUANTITIES"]
        rowlike = self._rowlike
        if not rowlike and not isinstance(index, t.Iterable):
            index = [index]

        qval_lst = [q.value for q in qs.values()]
        qval_lst.insert(0, index)

        table = np.vstack(qval_lst).T
        # HACK ALERT
        try:
            write_csv(fname, table, append=True)
        except TypeError:
            # this happens when array has mixed types
            # there are much better ways to handle this
            # this is a very quick&dirty workaround
            write_csv(fname, table, append=True, fmt="%s")

    def to_xarray(self, coords: t.Optional[t.Union[str, t.Sequence[str]]] = "_1"):
        """Converts the `MeasurementModel` to an xarray with the given `coords`.
        Coords '_1' is a special string to select the first quantity field as
        the coordinate.
        """

        d = self.to_dict()
        pars = d["PARAMETERS"]
        quants = d["QUANTITIES"]
        if coords == "_1":
            # pick the first quantity as coordinate
            coords = [next(iter(quants.keys()))]
        elif coords is None:
            coords = []
        elif isinstance(coords, str):
            coords = [coords]

        coordquants = list(quants.pop(coord) for coord in coords)

        _ii = _isiterable
        _coords = {
            cq.name: cq.value if _ii(cq.value) else [cq.value] for cq in coordquants
        }
        ds = xr.Dataset(
            {
                q.name: (
                    tuple(cq.name for cq in coordquants),
                    q.value if _ii(q.value) else [q.value],
                )
                for q in quants.values()
            },
            coords=_coords,
        )

        pardetails = {}
        for par in pars.values():
            if par.name == "timestamp":
                # treat timestamp as quantity
                # coordquants.append(par)    # THIS CAUSES A PROBLEM
                ds.coords["timestamp"] = par.value
                continue
            ds.attrs[par.name] = par.value
            pardetails[par.name] = {
                "nicename": par.repr,
                "value": par.value,
                "unit": par.unit,
                "description": par.description,
            }

        ds.attrs["parameter_info"] = json.dumps(
            pardetails, indent=4, default=default_serializer
        )

        for quant in quants.values():
            attrs = ds[quant.name].attrs
            attrs["units"] = str(quant.unit)
            attrs["description"] = str(quant.description)
            attrs["long_name"] = str(quant.repr)

        for quant in coordquants:
            attrs = ds.coords[quant.name].attrs
            attrs["units"] = str(quant.unit)
            attrs["description"] = str(quant.description)
            attrs["long_name"] = str(quant.repr)
        return ds

    def to_csv(
        self, fname: str, nicenames: bool = True, index=None, append: bool = False
    ):
        """Writes the model data to a csv file.

        Args:
            fname:  Name of the file to be written.
            nicenames:  If `True`, uses `repr` field of the parameters instead
                of attribute names.  Defaults to `True`.
            withindexcol: If `True` includes an index column.
            index: Index values to add.
        """
        if append:
            return self._to_csv_append(fname, index)
        mclass = type(self).measurement_class
        if index is not None:
            # appending suffix from measurementio
            mclass = f"{mclass}-{_appending_suffix}"
        metadata = {"MCLASS": mclass}
        try:
            metadata["MREF"] = self.mref
        except AttributeError:
            # Ignore if doesn't have mref property
            pass

        p: Parameter
        for name, p in parameters(self).items():
            if p.unit.shortname.strip() != "":
                unitstr = f" {p.unit.shortname:s}"
            else:
                unitstr = ""
            key = p.repr if nicenames else p.name
            if isinstance(p.value, str):
                valuestr = f"'{p.value}'{unitstr}"
            else:
                valuestr = f"{p.value}{unitstr}"
            metadata[key] = valuestr

        qs = quantities(self)
        data = []
        colreprs = []
        if index is not None:
            if not isinstance(index, t.Iterable):
                index = [index]
            data.append(index)
            colreprs.append("index")

        for q in qs.values():
            data.append(q.value)
            if q.unit.shortname.strip() != "":
                unitstr = f"[{q.unit.shortname:s}]"
            else:
                unitstr = ""

            colname = q.repr if nicenames else q.name
            colreprs.append(f"{colname:s}{unitstr:s}")

        data = np.vstack(data).T
        header = ", ".join(colreprs)

        # HACK ALERT
        try:
            write_csv(fname, data, delimiter=",", header=header, metadata=metadata)
        except TypeError:
            # this happens when array has mixed types
            # there are much better ways to handle this
            # this is a very quick&dirty workaround
            write_csv(
                fname, data, delimiter=",", header=header, fmt="%s", metadata=metadata
            )

    def evolve(self, **kwargs):
        """Returns a modified copy of this object, changing the attributes
        provided in `kwargs`."""
        return attr.evolve(self, **kwargs)

    def cut(self, indices: np.ndarray, inplace: bool = False):
        """Performs an indexing operation on all the quantities using the
        indices array.  Indices array can be an array of numbers indicating
        the index of the elements to be kept, or it can be a boolean array.
        See numpy indexing rules for details.  For this to be a valid
        operation, all quantities must have the same shape.
        """
        quant: Quantity
        alist = []
        for name, quant in quantities(self).items():
            alist.append((name, quant.value[indices]))

        if inplace:
            for name, qval in alist:
                setattr(self, name, qval)
            return self
        else:
            return self.evolve(**dict(alist))

    @classmethod
    def fake(cls):
        """Returns an instance with arbitrarily generated values for parameters
        and quantities."""
        pfs = parameter_fields(cls)
        qfs = quantity_fields(cls)

        d = {}
        for field in chain(qfs, pfs):
            name = field["name"]
            type = field["type"]
            d[name] = arbitrary(type)

        return cls(**d)


def combine_measurements(measurements: t.List[MeasurementModel]) -> MeasurementModel:
    """Will fail horribly if measurements contain different model types."""
    for i, m in enumerate(measurements):
        if i == 0:
            m0 = m
        qs = quantities(m)
        ps = parameters(m)
        q0v = next(iter(qs.values())).value
        isarray = isinstance(q0v, np.ndarray)
        if i == 0:
            out_tmp_q = dict(**{q.name: [q.value] for name, q in qs.items()})
            out_tmp_p = dict(**{p.name: p.value for name, p in ps.items()})
        else:
            for name, q in qs.items():
                out_tmp_q[name].append(q.value)

    cls = type(m0)

    if isarray:
        out = cls(
            **{k: np.hstack(v) for k, v in out_tmp_q.items()},
            **{k: v for k, v in out_tmp_p.items()},
        )
    else:
        out = cls(
            **{k: np.array(v) for k, v in out_tmp_q.items()},
            **{k: v for k, v in out_tmp_p.items()},
        )

    return out


class ResonanceMap(MeasurementModel):
    current: Array[float] = quantity(un.ampere, "Coil Current")
    frequency: Array[float] = quantity(un.hertz, "Resonance Frequency")
    stepsize: float = parameter(un.ampere, "Current Step")
    completion: float = parameter(un.second, "Completion Time")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )


class NetworkParams1PModel(MeasurementModel):
    """Standard network analyzer measurement class for 1 Port measurements.

    Attributes:
        frequencies: (Quantity) The 1-d array of frequencies for the
            measurement.
        values: (Quantity) The 1-d array of values of measurement.  The unit of
            this will depend on displayformat.
        ifbw: (Parameter) IF bandwidth setting of the network analyzer.
        navg: (Parameter) Number of averages setting of the network analyzer.
        power: (Parameter) Applied power during measurement.
        nparam: (Parameter) Network parameter.  Can be S21, S11, Z21 etc.
        displayformat: (Parameter) This can be MLOGARITHMIC, UPHASE, etc.  See
            SCPIVectorNetworkAnalyzer.DisplayFormat enum.

    """

    frequencies: Array[float] = quantity(un.hertz, "Frequency")
    values: Array[float] = quantity(un.nounit, "SParam")
    ifbw: float = parameter(un.hertz, "IF Bandwidth")
    navg: int = parameter(un.nounit, "Averages")
    power: float = parameter(un.dBm, "Output Power")
    nparam: str = parameter(un.nounit, "Network Parameter")
    displayformat: str = parameter(un.nounit, "Display Format")
    unit: str = parameter(un.nounit, "Unit")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )


class SParam1PModel(MeasurementModel):
    """Similar to NetworkParams1PModel, but the stored quantities are enforced
    to be magnitudes and phases.
    """

    frequencies: Array[float] = quantity(un.hertz, "Frequency")
    mags: Array[float] = quantity(un.dB, "Magnitude")
    phases: Array[float] = quantity(un.degree, "Phase")
    ifbw: float = parameter(un.hertz, "IF Bandwidth")
    navg: int = parameter(un.nounit, "Averages")
    power: float = parameter(un.dBm, "Output Power")
    nparam: str = parameter(un.nounit, "Network Parameter")
    uphases: Array[float] = quantity(un.degree, "UPhase")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )

    def to_hed_xarray(self):
        flds = attr.fields_dict(SParam1PModel)

        ds = xr.Dataset(
            {
                "spar_mag": ('frequency', self.mags),
                "spar_phase": ('frequency', self.phases),
                "nobs": self.navg,
                "ifbw": self.ifbw,
                "power": self.power,
            },
            coords={
                "fbin": ("frequency", self.frequencies),
                "timestamp": self.timestamp,
                },
        )
        ds.spar_mag.attrs['units'] = repr(flds['mags'].metadata['unit'])
        ds.spar_mag.attrs['long_name'] = "|S|"
        ds.spar_mag.attrs['ancillary_variables'] = "nobs"
        ds.spar_mag.attrs['cell_methods'] = "nobs: mean fbean: point"
        ds.spar_phase.attrs['units'] = repr(flds['uphases'].metadata['unit'])
        ds.spar_phase.attrs['long_name'] = "arg(S)"
        ds.spar_phase.attrs['ancillary_variables'] = "nobs"
        ds.spar_phase.attrs['cell_methods'] = "nobs: mean fbean: point"

        ds.power.attrs['units'] = repr(flds['power'].metadata['unit'])
        ds.power.attrs['long_name'] = "Signal Power"

        ds.fbin.attrs['units'] = repr(flds['frequencies'].metadata['unit'])
        ds.fbin.attrs['cell_methods'] = "frequency: point"
        ds.fbin.attrs['long_name'] = "Frequency"

        ds.ifbw.attrs['units'] = repr(flds['ifbw'].metadata['unit'])
        ds.ifbw.attrs['long_name'] = "IF Bandwidth"

        ds.nobs.attrs['long_name'] = "Number of Observations"
        ds.nobs.attrs['standard_name'] = "number_of_observations"

        return ds

    def average(self, *others: t.Iterable["SParam1PModel"]):
        """Averages this measurement with the provided measurements.
        No checks performed, make sure frequencies match.

        Don't expect performance from this.
        """
        magscum = dbm2watt(self.mags)
        phasescum = self.phases
        for n, other in enumerate(others):
            magscum += dbm2watt(other.mags)
            phasescum += other.phases

        # enumerate starts from 0 || we have 1 as self
        ntot = n + 1 + 1
        mags = watt2dbm(magscum / ntot)
        phases = phasescum / ntot
        uphases = unwrap_deg(phases)
        return self.evolve(mags=mags, phases=phases, uphases=uphases)

    @uphases.default
    def _uphase_default(self):
        return unwrap_deg(self.phases)

    @classmethod
    def from_complex(cls, frequencies: np.ndarray, values: np.ndarray, *args, **kwargs):
        """Computes magnitudes and phases in a `SParam1PModel` given an array of
        complex values and other parameters for the model.
        """
        mags = mag_db_volt(values)
        phases = angle_deg(values)
        return cls(frequencies, mags, phases, *args, **kwargs)

    @classmethod
    def from_nppm(cls, nppm: NetworkParams1PModel, **kwargs):
        logger.debug("Units are not checked.")
        f = nppm.frequencies
        default = np.empty_like(f)
        default.fill(np.nan)
        dformat = nppm.displayformat
        if dformat == "MLOGARITHMIC":
            mags = nppm.values
            phases = kwargs.get("phases", default)
            uphases = kwargs.get("uphases", default)
        elif dformat == "UPHASE":
            mags = kwargs.get("mags", default)
            phases = kwargs.get("phases", default)
            uphases = nppm.values
        elif dformat == "PHASE":
            mags = kwargs.get("mags", default)
            phases = nppm.values
            uphases = kwargs.get("uphases", default)
        else:
            msg = f"Unknown 'DisplayFormat' ({dformat})"
            raise TypeError(msg)

        return cls(
            frequencies=f,
            mags=mags,
            phases=phases,
            uphases=uphases,
            ifbw=nppm.ifbw,
            navg=nppm.navg,
            power=nppm.power,
            nparam=nppm.nparam,
            timestamp=nppm.timestamp,
        )


class ScalarMeasurementModel(MeasurementModel):
    """A generic measurement model to be used with any type of quantity.

    Attributes:
        name: Reserved for the name of the measurement.
        value: Reserved for the best estimation of the value, usually mean.
        error: Reserved for the standard error of the estimation, usually
            stdev.
        unit: Units for value and error.

    """

    value: float = quantity(un.nounit, "Value")
    error: float = quantity(un.nounit, "Error")
    unit: str = parameter(un.nounit, "Unit")
    name: str = parameter(un.nounit, "Quantity Name")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )


class SASpectrumModel(MeasurementModel):
    """Standard data model for Spectrum Analyzer measurements.

    Attributes:
        frequencies:  (Quantity) The 1-d array of frequencies for the
            measurement.
        values:  (Quantity) The 1-d array of power measurements performed at
            the center frequencies above.
        rbw:  (Parameter) Resolution bandwidth (3dB).
        vbw:  (Parameter) Video bandwidth (3dB).
        navg:  (Parameter) Average count.
        detector:  (Parameter) Detector type used in the measurement.
        filter_type:  (Parameter) IF band filtering function.  If FFT
            measurement is done, this is determined by the windowing function
            used.

    """

    frequencies: Array[float] = quantity(un.hertz, "Frequency")
    values: Array[float] = quantity(un.dBm, "Power")
    rbw: float = parameter(un.hertz, "RBW")
    vbw: float = parameter(un.hertz, "VBW")
    navg: int = parameter(un.nounit, "Averages")
    detector: str = parameter(un.nounit, "Detector")
    filter_type: str = parameter(un.nounit, "Filter Shape")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )

    def to_hed_xarray(self):
        flds = attr.fields_dict(SASpectrumModel)

        detector = {
            "NORMAL": 3,
            "AVERAGE": 0,
            "POSITIVE": 1,
            "SAMPLE": 5,
            "NEGATIVE": 2,
            "QUASIPEAK": 4,
            "EMIAVERAGE": -1,  # UNKNOWN
            "RMSAVERAGE": 0,
            "RMS": 0,
        }[self.detector]

        ds = xr.Dataset(
            {
                "power": ('frequency', self.values),
                "nobs": self.navg,
                "rbw": self.rbw,
                "vbw": self.vbw,
                "detector": detector,
            },
            coords={
                "fbin": ("frequency", self.frequencies),
                "timestamp": self.timestamp,
                },
        )
        ds.power.attrs['units'] = repr(flds['values'].metadata['unit'])
        ds.power.attrs['long_name'] = "Power"
        ds.power.attrs['ancillary_variables'] = "nobs"
        ds.power.attrs['cell_methods'] = "fbin: sum"

        ds.fbin.attrs['units'] = repr(flds['frequencies'].metadata['unit'])
        ds.fbin.attrs['cell_methods'] = "frequency: point"
        ds.fbin.attrs['long_name'] = "Frequency"

        ds.rbw.attrs['units'] = repr(flds['rbw'].metadata['unit'])
        ds.rbw.attrs['long_name'] = "Resolution Bandwidth"
        ds.rbw.attrs['standard_name'] = "bandwidth_3db"

        ds.nobs.attrs['long_name'] = "Number of Observations"
        ds.nobs.attrs['standard_name'] = "number_of_observations"

        ds.vbw.attrs['units'] = repr(flds['vbw'].metadata['unit'])
        ds.vbw.attrs['units'] = "Video Bandwidth"
        return ds


class NotAMeasurementError(ValueError):
    pass


class MeasurementClassNotFoundError(ValueError):
    pass


class UnitMismatchError(ValueError):
    pass


def read_measurement_csv(
    fname: str,
    ignore_units: bool = False,
    appendlist: bool = False,
) -> MeasurementModel:
    """Reads a csv file into it's object.  The csv file must contain the first
    line as "MCLASS=<measurement-class>".  The appropriate measurement-class is
    picked by parsing the "<measurement-class>" string and searching in the
    registry.

    THIS FUNCTION IS A COMPLETE MESS

    """
    sep = ","  # make this arg

    metadict = read_csv_meta(fname)

    try:
        mclass_str: str = metadict["MCLASS"]
    except KeyError:
        raise NotAMeasurementError("MCLASS directive not found.")

    sp = mclass_str.split("-")
    if len(sp) == 1:
        appending = False
    elif (len(sp) == 2) and sp[1].startswith(_appending_suffix):
        mclass_str = sp[0]
        appending = True

    try:
        mclass = get_measurement_class(mclass_str)
    except KeyError:
        raise MeasurementClassNotFoundError(
            f"Measurement class with the name '{mclass_str}' was not found."
        )

    if appending:
        # appending csv's have an extra column at the beginning
        qfad = {
            qf["name"]: dict(colindex=i + 1, **qf)
            for i, qf in enumerate(quantity_fields(mclass))
        }
    else:
        qfad = {
            qf["name"]: dict(colindex=i, **qf)
            for i, qf in enumerate(quantity_fields(mclass))
        }

    types = {}
    for k, v in qfad.items():
        typ = v["type"]
        if t.get_origin(typ) == Array:
            typ = t.get_args(typ)[0]
        types[v["colindex"]] = typ

    data, _ = read_csv(fname, dtype=types)
    if appending:
        index = data[..., 0]
        data = data[..., 1:]

    mref = metadict.get("MREF", None)
    mref = None if mref.startswith("None") else mref

    # This refers to expected parameters from MCLASS
    parfs = parameter_fields(mclass)

    parsed_params = {}
    for parf in parfs:
        # Ignore non-existing parameters
        # Let the measurement class deal with non-supplied parameters
        # instead.
        try:
            valuestr = metadict[parf["name"]]
        except KeyError:
            try:
                valuestr = metadict[parf["repr"]]
            except KeyError:
                continue

        if parf["type"] == np.ndarray:

            def vparser(s: str):
                lst = re.match(r"\[{0,1}([0-9\s\.\,]+)\]{0,1}", s).groups()[0]
                return np.fromstring(lst, sep=sep)

        elif parf["type"] == str:
            valuestr = valuestr.strip("'")
            vparser = str
        else:
            vparser = parf["type"]

        # this is a very convoluted way of doing things, fix asap
        if parf["type"] == datetime:
            vparser = isoformat2datetime
            # vparser = lambda s: eval(s.split('.', 1)[1])  # HACK FOR BROKEN DATA ONLY
            value = valuestr
            unit = un.nounit
        else:
            value, unit = un.parse(valuestr)

        # TYPE CONVERSION
        value = vparser(value)

        if unit != parf["unit"]:
            if not (ignore_units or ALWAYS_IGNORE_UNITS):
                raise UnitMismatchError(
                    f"Model parameter '{parf['name']}' has a unit mismatch. "
                    f"File: '{unit}', Model: '{parf['unit']}'."
                )

        parsed_params[parf["name"]] = value

    # The ordering of the following is important
    quantfs = quantity_fields(mclass)

    parsed_quantities = {}
    for i, quantf in enumerate(quantfs):
        # i is column in the csv table
        if _is_row_compatible(mclass):
            # This means the schema is originally for all scalars.
            if len(quantfs) == 1:  # if single column data
                # This means there is only single value.
                logger.debug("1 row 1 col")
                parsed_quantities[quantf["name"]] = data.item()
                break
            else:
                # This means there are multiple columns.
                # If _is_row_compatible works, there must be 1 row.
                logger.debug("Expected 1 row, >1 col")

                if len(data.shape) > 1:
                    # This is just a workaround for some internal
                    # inconsistencies in this library.
                    logger.warning(
                        "Got > 1 row.  Silently accepting data not matching schema."
                    )
                    parsed_quantities[quantf["name"]] = data[:, i]
                else:
                    parsed_quantities[quantf["name"]] = data[i]
        else:
            if len(quantfs) == 1:  # if single column data
                parsed_quantities[quantf["name"]] = data[:]
                break
            else:
                ndim = len(data.shape)
                if ndim == 1:
                    dt = data[i]
                else:
                    # If there are less columns in the data
                    # data[:, i] will raise IndexError
                    # This is intended behavior as it shows that the
                    # data and it's schema do not match.  However, sometimes
                    # for few cases where a specific measurementmodel got
                    # updated with an extra quantity column but we still need
                    # to read old data.  That's why i'm catching this error and
                    # letting it go.
                    try:
                        dt = data[:, i]
                    except IndexError:
                        logger.debug(f"File '{fname}' has more columns than the schema")
                        continue
                parsed_quantities[quantf["name"]] = dt

    if appending:
        if appendlist:
            pq = parsed_quantities
            ds = (dict(zip(pq, t)) for t in zip(*pq.values()))
            mm = []
            for d in ds:
                m = mclass(**parsed_params, **d)
                m._mref = mref
                mm.append(m)
        else:
            mm = mclass(**parsed_params, **parsed_quantities)
            mm.index = index
            mm._mref = mref
    else:
        mm = mclass(**parsed_params, **parsed_quantities)
        mm._mref = mref

    return mm


def make_parameter_text(
    mm: MeasurementModel,
    names: t.Optional[t.Collection[str]] = None,
    sigdigits: t.Optional[t.Collection[int]] = None,
):
    """Given a `MeasurementModel` returns a text that can be used with
    matplotlib's annotation functions.

    Args:
        mm:  The measurement model instance.
        names:  The names of the parameters to be used when making the text.
            If `None` all the found parameters are used.  Defaults to `None`.
        sigdigits:  Significant digits to use when representing values.  If
            provided the ordering and length of this object must be the same
            with `names` argument.
    """
    if (names is not None) and (sigdigits is not None):
        sigdigits = dict(zip(names, sigdigits))

    template = "{key}={value}"
    pars = parameters(mm)
    texts = []
    for name, par in pars.items():
        sigdigit = None
        if names is not None:
            if name not in names:
                continue
            else:
                if sigdigits is not None:
                    sigdigit = sigdigits.get(name, None)

        if sigdigit is not None:
            valuestr = mprefix_str(par.value, par.unit, sigdigits=sigdigit)
        else:
            valuestr = mprefix_str(par.value, par.unit)
        text = template.format(key=par.name, value=valuestr)
        texts.append(text)

    return "\n".join(texts)
