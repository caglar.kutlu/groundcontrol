"""This module contains the structure necessary to describe specific devices
like Josephson Parametric Amplifiers.

Currently device hierarchy based on any kind of inheritance is only minimally
implemented.

Device attributes are immutable.
"""
import typing as t
import cattr
from functools import partial

from groundcontrol.declarators import setting, declarative
import groundcontrol.units as un


_settingnop = partial(setting, isoptional=False)


@declarative(frozen=True)
class MWComponent:
    nports: int = _settingnop(nicename="PortCount")


@declarative(frozen=True)
class FluxDrivenJPA(MWComponent):
    device_uid: str = _settingnop(
            nicename="DeviceUID",
            description="Unique ID of the specific sample.")

    resonance_frequency_range: t.Tuple[float, float] = _settingnop(
            un.hertz,
            nicename="ResonanceFrequencyRange",
            description="Range of tunable passive resonance frequencies."
            )

    amplification_frequency_range: t.Tuple[float, float] = _settingnop(
            un.hertz,
            nicename="AmplificationFrequencyRange",
            description="Range of frequencies with observed amplification."
            )

    pump_power_range: t.Tuple[float, float] = _settingnop(
            un.dBm,
            nicename="PumpPowerRange",
            description="Range of pump powers used to achieve amplification.")

    alias: str = _settingnop(
            nicename="Alias",
            description="Short alias for the device.")

    nports: int = setting(nicename="PortCount", default=2)

    @alias.default
    def _alias(self):
        return self.device_uid
