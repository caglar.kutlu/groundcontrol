"""Module defining the common resource points for the module.  Default
configuration dir etc.

The uris are in the VISA Resource format, even when the handler is not VISA
related.  Grammar for the resource address string
    Interface 	        Syntax
    ENET-Serial INSTR 	ASRL[0]::host address::serial port::INSTR
    GPIB INSTR 	        GPIB[board]::primary address[::secondary address][::INSTR]
    GPIB INTFC 	        GPIB[board]::INTFC
    PXI BACKPLANE 	PXI[interface]::chassis number::BACKPLANE
    PXI INSTR 	        PXI[bus]::device[::function][::INSTR]
    PXI INSTR 	        PXI[interface]::bus-device[.function][::INSTR]
    PXI INSTR 	        PXI[interface]::CHASSISchassis number::SLOTslot number[::FUNCfunction][::INSTR]
    PXI MEMACC 	        PXI[interface]::MEMACC
    Remote NI-VISA 	visa://host address[:server port]/remote resource
    Serial INSTR 	ASRLboard[::INSTR]
    TCPIP INSTR 	TCPIP[board]::host address[::LAN device name][::INSTR]
    TCPIP SOCKET 	TCPIP[board]::host address::port::SOCKET
    USB INSTR 	        USB[board]::manufacturer ID::model code::serial number[::USB interface number][::INSTR]
    USB RAW 	        USB[board]::manufacturer ID::model code::serial number[::USB interface number]::RAW
    VXI BACKPLANE 	VXI[board][::VXI logical address]::BACKPLANE
    VXI INSTR 	        VXI[board]::VXI logical address[::INSTR]
    VXI MEMACC 	        VXI[board]::MEMACC
    VXI SERVANT 	

    Example resource for attocube controller:
    USB0::0x16c0::0x055b::L010035::RAW

"""

from pathlib import Path
import typing as t
import os

configdir = Path('~/.config/groundcontrol').expanduser()


logdir = configdir / 'logs'


for dir in (configdir, logdir):
    if not dir.exists():
        dir.mkdir()


def get_culdaq_db_secrets() -> t.Tuple[str, str]:
    """Reads the db secrets that the rack computer has access to"""
    # This is a small attempt at small bit of security.
    # Not that storing a password in an environment variable is in any
    # way secure
    try:
        culdaqdbuser = os.environ['CULDAQDBUSER']
        culdaqdbpass = os.environ['CULDAQDBPASS']
        return culdaqdbuser, culdaqdbpass
    except KeyError:
        raise KeyError("CULDAQ DB secrets do not exist on the host.")


def get_rack_instance() -> str:
    """Reads the rack instance environment variable and returns it as string if
    exists.

    Returns:
        rack_instance:  String reference to the rack.

    """
    try:
        rack_instance = os.environ['RACK']
        return rack_instance
    except KeyError:
        raise ValueError("RACK environment variable is not set.")


# This is done as a quick workaround for some circular dependency issue
uris = {}
def register_uris():
    from groundcontrol.instruments import AgilentN5232A, RohdeSchwarzFSV
    from groundcontrol.instruments import (
            KeysightE8257D, RohdeSchwarzZND, YokogawaGS200, KeysightE8267D)
    from groundcontrol.instruments import AttocubeANC350
    from groundcontrol.instruments.adapters import GS200Adapter

    if uris != {}:
        return False

    uris_bf4_left = {
            'vna': 'TCPIP::10.10.1.100::inst0::INSTR',
            'sg1': ('TCPIP0::10.10.1.102::inst0::INSTR', KeysightE8257D),
            'sa': 'TCPIP::10.10.1.101::inst0::INSTR',
            'tc': 'TCPIP0::10.10.1.150::7777::SOCKET',
            'cs': 'TCPIP0::10.10.1.201::5025::SOCKET',
    }

    uris_bf4_right = {
            'vna': ('TCPIP::10.10.1.103::inst0::INSTR', AgilentN5232A),
            'sg1': ('TCPIP0::10.10.1.105::inst0::INSTR', KeysightE8257D),
            'sa': 'TCPIP::10.10.1.104::inst0::INSTR',
            'tc': 'TCPIP0::10.10.1.150::7777::SOCKET',
            'cs': 'TCPIP0::10.10.1.200::5025::SOCKET',
    }
    
    uris_bf4 = {
            'vna': 'TCPIP::10.10.1.100::inst0::INSTR',
            'sg1': ('TCPIP0::10.10.1.102::inst0::INSTR', KeysightE8257D),
            'sa': 'TCPIP::10.10.1.101::inst0::INSTR',
            'cs': 'TCPIP0::10.10.1.200::5025::SOCKET',
            'vna2': ('TCPIP::10.10.1.103::inst0::INSTR', AgilentN5232A),
            'sg2': 'TCPIP0::10.10.1.105::inst0::INSTR',
            'sa2': 'TCPIP::10.10.1.104::inst0::INSTR',
            'cs2': 'TCPIP0::10.10.1.201::5025::SOCKET',
            'tc': 'TCPIP0::10.10.1.150::7777::SOCKET'
    }
    
    uris_bf5 = {
            'vna': ('TCPIP::10.10.0.106::hislip0::INSTR', AgilentN5232A),
            'sg1': 'TCPIP0::10.10.0.202::inst0::INSTR',
            'sa': ('TCPIP::10.10.0.199::inst0::INSTR', RohdeSchwarzFSV),
            'tc': 'TCPIP0::10.10.0.150::7777::SOCKET',
            # 'cs': 'TCPIP0::10.10.0.201::5025::SOCKET',
            'cs': 'GPIB0::26::INSTR',
    }


    uris_bf6 = {
            'vna': ('TCPIP::192.168.0.12::hislip0::INSTR', RohdeSchwarzZND),
            'sg1': 'TCPIP0::192.168.0.10::inst0::INSTR',
            'sg2': 'TCPIP0::192.168.0.11::inst0::INSTR',
            'sa': 'GPIB0::10::INSTR',
            'sa2': ('TCPIP0::192.168.0.100::hislip0::INSTR', RohdeSchwarzFSV),
            'tc': 'TCPIP0::192.168.0.22::7777::SOCKET',
            'cs': ('GPIB0::5::INSTR', GS200Adapter),
            'pc': ('USB0::0x16c0::0x055b::L010035::RAW', AttocubeANC350),
            'trf': 'TCPIP::192.168.0.15::hislip0::INSTR'
    }


    uris_bf3 = {
            # 'vna': ('TCPIP::10.10.2.100::hislip0::INSTR', AgilentN5232A),
            'vna': ('GPIB0::20::INSTR', AgilentN5232A),
            'sg1': 'TCPIP0::10.10.2.102::inst0::INSTR',
            'sa': ('TCPIP::10.10.2.101::inst0::INSTR'),
            'tc': 'TCPIP0::10.10.2.150::7777::SOCKET',
            'cs': 'TCPIP0::10.10.2.200::5025::SOCKET',
    }

    uris_cktest = {
            'sgtest': ('TCPIP::192.168.0.33', KeysightE8267D),
            'sg1': ('TCPIP::192.168.0.33', KeysightE8267D),
            'trf': 'TCPIP::192.168.0.15::hislip0::INSTR',
    }

    uris.update({
            'BF3': uris_bf3,
            'BF4_R': uris_bf4_right,
            'BF4_L': uris_bf4_left,
            'BF4' : uris_bf4,
            'BF5': uris_bf5,
            'BF6': uris_bf6,
            'CKTEST': uris_cktest,
        })
    return True


def get_uris():
    """Returns a dictionary of instrument uris for the system at which this
    function was called in. 

    The dictionary follows the format in the following example:
        { 'inst_name1': '<visa uri for the instrument>',
          'inst_name2': ('<visa uri for the instrument2>', AgilentN5232A) }

    As can be seen from the example above, values can be just uri string or a
    tuple with the first element as uri string and the second element as the
    instrument class to be used when instantiating the instrument.
    
    Returns:  Dictionary of instrument uris.

    """
    register_uris()
    ri = get_rack_instance()
    return uris[ri]


libdir = Path("~/.local/lib")
