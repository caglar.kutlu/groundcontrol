"""A module for functions converting values to human friendly formats.

Work in progress.
"""
import typing as t

import numpy as np
from dateutil.relativedelta import relativedelta

from groundcontrol import USE_UNICODE


_MAX_EXPONENT = 15
_MIN_EXPONENT = -15


_EXPONENTS = range(_MIN_EXPONENT, _MAX_EXPONENT + 1, 3)
if USE_UNICODE:
    _PREFIXES = ('f', 'p', 'n', 'μ', 'm', '', 'k', 'M', 'G', 'T', 'P')
else:
    _PREFIXES = ('f', 'p', 'n', 'u', 'm', '', 'k', 'M', 'G', 'T', 'P')


PREFIXES = dict(zip(_EXPONENTS, _PREFIXES))
"""Dictionary mapping base 10 exponents to multiplier prefixes."""


def mprefix(value: float) -> t.Tuple[float, str]:
    """Given a value, returns a tuple containing a multiplier prefix string and
    the value after division by the multiplication factor.

    The minimum(maximum) prefix is 'f'emto('P'eta).

    If no appropriate prefix was found, an empty string is returned.

    Example:
        >>> mprefix(1000.0)
        >>> (1.0, 'k')

    """
    trunc = np.floor(np.log10(abs(value)))

    # Base 10 exponents
    exponents = _EXPONENTS

    exponent = 0
    for a, b in zip(exponents, exponents[1:]):
        if a == _MIN_EXPONENT and trunc < b:
            exponent = a
            break
        elif (a <= trunc) and (trunc < b):
            exponent = a
            break
        elif b == _MAX_EXPONENT and b <= trunc:
            exponent = b
            break

    prefix = PREFIXES[exponent]

    new_value = value/(10**exponent)
    return new_value, prefix


def mprefix_str(value: float, unit: str, sigdigits=4):
    """Given a value returns a easily readable form of string
    representation of the value using the given unit.
    """
    trunc, prefix = mprefix(value)
    return f"{trunc:.{sigdigits-1}f} {prefix}{unit}"


def timedelta(seconds: float):
    """Returns a string formatting a time delta in seconds in a human readable
    format.

    Example:
        >>> timedelta(1622)
        >>> '27 minutes 2 seconds'

    """
    rd = relativedelta(seconds=seconds)

    fields = ('days', 'hours', 'minutes', 'seconds')
    formats = ('.0f', '.0f', '.0f', '.3f')

    periods = []
    for field, fmt in zip(fields, formats):
        val = getattr(rd, field)
        if val != 0:
            periods.append(f"{val:{fmt}} {field}")

    return " ".join(periods)
