"""Simple module for providing some unit objects."""
from groundcontrol import USE_UNICODE

import re
import typing as t

import attr
# import pint


_unit_registry = {}


_value_parser = re.compile(r'(.*[0-9]*)\s*(\D+$)')
_nounit_value_parser = re.compile(r'(.*[0-9]+)')
# Better regexp possible...


class UnrecognizedUnit(ValueError):
    pass


@attr.s(auto_attribs=True, repr=False, frozen=True)
class UnitDefinition:
    typename: str
    shortname: str
    longname: str = attr.ib(attr.Factory(
        lambda self: self.typename, takes_self=True))

    def __attrs_post_init__(self):
        # Register the unit
        _unit_registry[self.typename] = self

    def __repr__(self):
        return self.shortname

    def __eq__(self, other):
        return self.typename == other.typename


# Units are usually case sensitive so here they are too.
watt = UnitDefinition('Watt', 'W')
hertz = UnitDefinition('Hertz', 'Hz')
dBW = UnitDefinition('dBW', 'dBW')
dBm = UnitDefinition('dBm', 'dBm')
nounit = UnitDefinition('None', '', '')
# dB is not an actual unit, but it is convenient to store it like one.
dB = UnitDefinition('dB', 'dB')
ratio_vv = UnitDefinition('ratio_vv', 'V/V')
ratio_ww = UnitDefinition('ratio_ww', 'W/W')
ampere = UnitDefinition('ampere', 'A', 'Ampere')
second = UnitDefinition('second', 's')
degree = UnitDefinition('degree', 'deg')
kelvin = UnitDefinition('kelvin', 'K')
dB_d_s = UnitDefinition('dB_d_s', 'dB/s')
dBm_d_hz = UnitDefinition('dBm_d_hz', 'dBm/Hz')
dBm0hz = dBm_d_hz
ampere_d_s = UnitDefinition('ampere_d_s', 'A/s', 'Ampere/Second')
volt = UnitDefinition('volt', 'V')
if USE_UNICODE:
    ohm = UnitDefinition('ohm', "Ω")
else:
    ohm = UnitDefinition('ohm', "ohm")
qflux = UnitDefinition('fluxquanta', "phi/phi0")


T = t.TypeVar('T')


def parse(
        s: str) -> (T, UnitDefinition):
    """Parses the string into value and unit.
    Arguments:
        s:  String to be parsed.

    Acceptable string examples:
        "1 Hz", "'Abcd' Ampere" etc.
    """
    # reverse and strip off spaces
    reverstr: str = s[::-1].lstrip()
    splitted = reverstr.split(' ', 1)
    try:
        revunit, revvalue = splitted
    except ValueError:  # too many(not enough) values to unpack
        value = s
        unit = nounit
        return value, unit

    unit = None
    for un in _unit_registry.values():
        if un == nounit:
            # this breaks the algorithm, workaround
            continue

        if (revunit.startswith(un.shortname[::-1])
                or
                revunit.startswith(un.longname[::-1])):
            unit = un
            value = revvalue[::-1]
            break

    if unit is None:
        # If no unit found, value is the whole string
        unit = un.nounit
        value = s

    return value, unit
