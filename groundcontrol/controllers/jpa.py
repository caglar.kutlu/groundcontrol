"""An example controller for the instruments operating a JPA.

Tentative interface for controllers:
    * Methods that do a measurement and encapsulate the information in a class
    are named with `measure_`, whereas the other measurement related methods
    are simply named via `query_`.

"""
import typing as t
import time
from datetime import datetime
from functools import wraps

import numpy as np

from groundcontrol.controller import Controller, instrument
from groundcontrol.logging import setup_logger
from groundcontrol.instruments import \
    SCPISignalGenerator, SCPISpectrumAnalyzer, SCPIVectorNetworkAnalyzer,\
    TSPCurrentSource, LS372TemperatureController, YokogawaGS200
from groundcontrol.instruments import NotACommand
from groundcontrol.settings import SASettings, VNASettings, CSSettings
from .errors import UserAbort
from groundcontrol.measurement import SASpectrumModel, NetworkParams1PModel,\
        ScalarMeasurementModel, SParam1PModel
import groundcontrol.units as un
from groundcontrol.units import UnitDefinition
from groundcontrol.util import within
from groundcontrol import friendly
from groundcontrol.helper import domaybe
from groundcontrol.declarators import declarative


logger = setup_logger("jpa")


def _stopwatch(fun):
    """Decorator for measuring how long a function takes to return."""
    @wraps(fun)
    def decorated(*args, **kwargs):
        start = time.time()
        logger.debug(f"Calling function '{fun.__name__}'")
        out = fun(*args, **kwargs)
        passed = time.time() - start
        logger.debug(f"Took {friendly.timedelta(passed)} to return.")
        return out
    return decorated


class JPAController(Controller):
    """An example controller for a JPA measurement setup.

    Notes:
        * Instruments are generally operated in "trigger on demand" mode rather
        than continuously being triggered all the time.

        * This class aims to be very generic and straightforward.  An
        alternative and better implementation should break the structures into
        smaller pieces.

    Instruments:
        sg1:  First signal generator.
        sa:  Spectrum analyzer.
        vna:  Network analyzer.
        tc:  Temperature controller.
        cs:  Current source.

    Attributes:
        sa_preset:  Initial settings for the SA.  Settings set to `None` are
            ignored.
        vna_preset:  Initial settings for the VNA.  Settings set to `None`
            are ignored.
        cs_preset:  Initial settings for the CurrentSource.  Settings set to
        `None` are ignored.
        current_max_step:  Maximum allowed step size for flux current
            increments.
        noise_source_control_channel:  Temperature channel of the noise source
            temperature sensor.
        temperature_setpoint_max:  Maximum temperature setpoint for noise
            source heater.
        nparam:  The network parameter to measure (with respect to the VNA
            outputs, not the JPA).
    """
    sg1: SCPISignalGenerator = instrument()
    sa: SCPISpectrumAnalyzer = instrument()
    vna: SCPIVectorNetworkAnalyzer = instrument()
    tc: LS372TemperatureController = instrument()
    cs: TSPCurrentSource = instrument()

    # Initial Settings for some instruments
    sa_preset: SASettings = SASettings()
    vna_preset: VNASettings = VNASettings()
    cs_preset: CSSettings = CSSettings(range_i=1e-4)
    baseline_settings: VNASettings = VNASettings()

    clean_vna: bool = False

    q_expected: float = 300  # Q-factor for the JPA resonance

    current_step: float = 1e-7  # A  # NEED VALIDATOR FOR MAXIMUM VALUE and POSITIVITY !  # noqa: E501
    current_ramp_rate: float = 1e-6  # A/s
    pump_ramp_rate: float = 5  # dB/s
    pump_step: float = 0.1  # dB
    noise_source_control_channel: str = "02"
    temperature_setpoint_max: float = 0.6  # don't set above 600 mK
    nparam: str = "S21"

    timeout_guard_factor: float = 10
    timeout_guard_time_ms: float = 500

    _last_navg: int = None
    current_template: ScalarMeasurementModel = ScalarMeasurementModel(
            name="Current",
            value=float('nan'),
            error=float('nan'),
            unit=un.ampere.shortname)

    verbose: bool = True
    _temp_report_every_s: float = 10
    _max_flux_current = 500e-6
    _vna_default_channel: int = 1
    _bf6_compat: bool = False

    def _prompt(
            self,
            message: str,
            options: t.List[str],
            default_optind: t.Optional[int] = None) -> t.Optional[str]:

        optsfmt = []
        for i, opt in enumerate(options):
            if i == default_optind:
                optsfmt.append(f'[{opt}]')
            else:
                optsfmt.append(opt)
        options_text = '/'.join(optsfmt)

        s = f"{message} ({options_text}) "
        resp = input(s)
        opt: str
        for opt in options:
            if opt.startswith(resp):
                return opt
        return None

    def __setup__(self):
        if self.sa is not None:
            self.setup_sa()
        if self.vna is not None:
            self.setup_vna(self.clean_vna)
        if self.cs is not None:
            self.setup_cs()

    def setup_sa(self):
        """Sets up spectrum analyzer"""
        sa = self.sa
        saset = self.sa_preset

        # Hardcoded settings

        # Single trigger mode
        sa.set_initiate_continuous(sa.State.OFF)

        # HARD SETTING: Averaging Powers == RMS detector
        sa.set_sense_average_type(sa.AverageType.POWER)

        # Supplied initial settings
        self.do_sa_preset(saset)

    def do_sa_preset(self, preset: SASettings):
        sa = self.sa
        domaybe(sa.set_format_byte_order, preset.byteorder)
        domaybe(sa.set_format_data, preset.dataformat)
        domaybe(sa.set_sense_detector, preset.detector)
        domaybe(sa.set_sense_frequency_start, preset.start_frequency)
        domaybe(sa.set_sense_frequency_stop, preset.stop_frequency)
        domaybe(sa.set_sense_frequency_center, preset.center_frequency)
        domaybe(sa.set_sense_frequency_span, preset.span)
        domaybe(sa.set_sense_bandwidth_resolution, preset.rbw)
        domaybe(sa.set_sense_bandwidth_video, preset.vbw)
        domaybe(sa.set_sense_sweep_points, preset.npoints)
        swt_auto = SCPISpectrumAnalyzer.State.from_bool(preset.swt_auto)
        domaybe(sa.set_sense_sweep_time_auto, swt_auto)
        domaybe(sa.set_sense_sweep_time, preset.swt)
        domaybe(self.set_spectrum_average, preset.naverage)

    def setup_vna(self, clean: bool = False):
        """Sets up vector network analyzer.
        Args:
            clean: If `True`, cleans up windows set on the VNA first.
        """
        vna = self.vna
        vnaset = self.vna_preset

        # Hardcoded settings
        if self._bf6_compat:
            logger.warning("BF6 WORKAROUND: no SourcePowerMode AUTO")
        else:
            vna.set_source_power_mode(vna.SourcePowerMode.AUTO)

        mname = "REFLECTION"
        param = self.nparam

        if clean:
            logger.debug("Cleaning measurements on VNA.")
            vna.do_calculate_parameter_delete_all()
            vna.do_calculate_parameter_define_ext(mname=mname, param=param)
            vna.set_calculate_parameter_mname_select(mname)
            logger.debug(f"New measurement with name {mname} is created "
                         f"for {param} measurement.")
        else:
            existing_measurements = vna.query_calculate_parameter_catalog_ext()
            logger.debug("Existing measurements on VNA:"
                         f"{existing_measurements}")
            exparam = filter(
                    lambda tpl: tpl[1] == param, existing_measurements)
            try:
                # If there is an existing `param` measurement, use it.
                name, param = next(exparam)  # pick the first match
                vna.set_calculate_parameter_mname_select(name)
                mname = name  # use the existing name.
                logger.debug(f"Measurement with name '{name}' found for "
                             f"'{param}' measurement. Using it.")
            except StopIteration:  # next(exparam) throws this
                # If not, create one
                vna.do_calculate_parameter_define_ext(mname=mname, param=param)
                vna.set_calculate_parameter_mname_select(mname)
                logger.debug(f"New measurement with name {mname} is created "
                             f"for {param} measurement.")

        # Display the trace only if it's not already displayed.
        if self._bf6_compat:
            logger.warning("BF6 WORKAROUND: 'tnumber'")
            tnum = vna.query_configure_trace_name_id(mname)
        else:
            tnum = vna.query_calculate_parameter_tnumber()

        tnums = vna.query_display_window_catalog()
        if tnum not in tnums:
            try:
                tnum = max(tnums) + 1  # just pick a trace number
            except ValueError:  # if empty
                tnum = 1
            vna.do_display_window_trace_feed(mname)

        # vna.set_calculate_format(vna.DisplayFormat.MLOGARITHMIC)

        # Single trigger mode
        vna.set_initiate_continuous(vna.State.OFF)

        # Supplied initial settings
        vna.set_format_byte_order(vnaset.byteorder)
        vna.set_format_data(vna.FormatDataType.REAL64)

        domaybe(vna.set_sense_frequency_start, vnaset.start_frequency)
        domaybe(vna.set_sense_frequency_stop, vnaset.stop_frequency)
        domaybe(vna.set_sense_sweep_step, vnaset.sweep_step)
        domaybe(vna.set_sense_bandwidth, vnaset.if_bandwidth)
        domaybe(self.set_vna_average, vnaset.naverage)

    def activate_vna_idler_mode(self):
        """Activates the idler measurement mode in VNA."""
        param = "B,1"  # assuming port 2 is input.
        vna = self.vna
        vna.do_calculate_parameter_modify_ext(param=param)
        multiplier = -1
        foffset = self.query_pump_frequency()
        # f_idler = f_pump - f_signal
        # f_new = foffset + multiplier*f_signal
        rname = vna.FOMRangeName.RECEIVERS
        rnum = vna.query_sense_fom_rnum(rname)
        vna.set_sense_fom_range_coupled(rnum, vna.State.ON)
        vna.set_sense_fom_range_frequency_multiplier(rnum, multiplier)
        vna.set_sense_fom_range_frequency_offset(rnum, foffset)
        vna.set_sense_fom_state(vna.State.ON)

    def activate_vna_s21_mode(self):
        param = self.nparam
        vna = self.vna
        vna.do_calculate_parameter_modify_ext(param=param)
        vna.set_sense_fom_state(vna.State.OFF)

    def activate_vna_source_power_mode(self):
        param = 'R1,1'
        vna = self.vna
        vna.do_calculate_parameter_modify_ext(param=param)
        vna.set_sense_fom_state(vna.State.OFF)

    def setup_cs(self):
        """Sets up current source."""
        cs = self.cs
        csset = self.cs_preset
        sf: cs.SourceFunction = cs.query_source_function()

        if sf == cs.SourceFunction.VOLTAGE:
            self._setup_current_source_function()
        else:
            self._setup_current_source_range(csset.range_i)

        if cs.query_output_state() is cs.State.OFF:
            q = ("The DC flux source seems to be OFF.  It can be turned on "
                 "with the current set to 0A, would you like to turn it "
                 "on?")
            opts = ["Yes", "No"]
            resp = self._prompt(q, opts, default_optind=1)
            if resp == opts[0]:
                self._setup_current_source_turnon()

    def set_spectrum_average(self, navg: int):
        """Sets the spectrum average number for SA."""
        sa = self.sa
        if navg <= 1:
            sa.set_sense_average_state(sa.State.OFF)
            sa.set_trace_type(sa.TraceType.CLEARWRITE)
        else:
            sa.set_sense_average_state(sa.State.ON)
            sa.set_trace_type(sa.TraceType.AVERAGE)
        sa.set_sense_average_count(navg)

    def set_vna_average(self, navg: int):
        """Sets the spectrum average number for VNA."""
        vna = self.vna
        vna.set_sense_average_count(navg)
        if navg <= 1:
            vna.set_sense_average_state(vna.State.OFF)
            # vna.set_trigger_source(vna.TriggerSource.MANUAL)
        else:
            vna.set_sense_average_state(vna.State.ON)
            # Single trigger causes a group of sweeps to be measured
            # Group count is made equal to the number of averages.
            vna.set_sense_sweep_groups_count(navg)

    def set_vna_npoints(self, npoints: int):
        """Sets the number of points."""
        vna = self.vna
        vna.set_sense_sweep_points(npoints)

    def query_vna_points(self, npoints: int):
        vna = self.vna
        return vna.query_sense_sweep_points()

    def do_vna_autoscale(self):
        """Auto scales the y-axis in the default window and trace."""
        self.vna.do_display_window_trace_y_scale_auto()

    def do_vna_preset(self, vnaset: VNASettings):
        vna = self.vna
        vna.set_format_byte_order(vnaset.byteorder)
        vna.set_format_data(vna.FormatDataType.REAL64)

        if vnaset.sweep_step == 0:
            # when this holds, always span=0 is meant.
            vnaset.sweep_step = None
            vnaset.span = 0

        domaybe(vna.set_sense_frequency_start, vnaset.start_frequency)
        domaybe(vna.set_sense_frequency_stop, vnaset.stop_frequency)
        domaybe(vna.set_sense_frequency_center, vnaset.center_frequency)
        domaybe(vna.set_sense_frequency_span, vnaset.span)
        domaybe(vna.set_source_power_level, vnaset.power)
        domaybe(vna.set_sense_sweep_step, vnaset.sweep_step)
        domaybe(vna.set_sense_bandwidth, vnaset.if_bandwidth)
        domaybe(self.set_vna_average, vnaset.naverage)
        domaybe(self.set_vna_npoints, vnaset.npoints)

    def query_vna_settings(self) -> VNASettings:
        v = self.vna
        settings = VNASettings(
                byteorder=v.query_format_byte_order(),
                dataformat=v.query_format_data(),
                start_frequency=v.query_sense_frequency_start(),
                stop_frequency=v.query_sense_frequency_stop(),
                center_frequency=v.query_sense_frequency_center(),
                span=v.query_sense_frequency_span(),
                sweep_step=v.query_sense_sweep_step(),
                if_bandwidth=v.query_sense_bandwidth(),
                naverage=v.query_sense_average_count(),
                power=v.query_source_power_level())
        return settings

    def query_sa_settings(self) -> SASettings:
        sa = self.sa
        settings = SASettings(
                byteorder=sa.query_format_byte_order(),
                dataformat=sa.query_format_data(),
                detector=sa.query_sense_detector(),
                start_frequency=sa.query_sense_frequency_start(),
                stop_frequency=sa.query_sense_frequency_stop(),
                center_frequency=sa.query_sense_frequency_center(),
                span=sa.query_sense_frequency_span(),
                rbw=sa.query_sense_bandwidth_resolution(),
                vbw=sa.query_sense_bandwidth_video(),
                swt=sa.query_sense_sweep_time(),
                swt_auto=sa.query_sense_sweep_time_auto(),
                npoints=sa.query_sense_sweep_points(),
                naverage=sa.query_sense_average_count())
        return settings

    def read_vna_settings(self) -> VNASettings:
        logger.warning("`read_vna_settings` will be changed to "
                       "`query_vna_settings`.  Use that one.")  # noqa:  E501
        return self.query_vna_settings()

    def set_signal_power(self, dbm: float):
        """Sets the signal power for the VNA source."""
        self.vna.set_source_power_level(dbm)

    def set_pump_power(self, dbm: float):
        self.sg1.set_power(dbm)

    def query_pump_power(self):
        return self.sg1.query_power()

    def set_pump_frequency(self, f_hz: float):
        self.sg1.set_frequency(f_hz)

    def query_pump_frequency(self):
        return self.sg1.query_frequency()

    def activate_signal(self):
        """Activates the signal (VNA) output power."""
        self.vna.set_output(self.vna.State.ON)

    def deactivate_signal(self):
        """Deactivates the signal (VNA) output power."""
        self.vna.set_output(self.vna.State.OFF)

    def activate_pump(self):
        self.sg1.set_output_state(self.sg1.State.ON)

    def deactivate_pump(self):
        self.sg1.set_output_state(self.sg1.State.OFF)

    def _setup_current_source_turnon(self):
        cs = self.cs
        logger.info("Setting initial current to 0 A")
        cs.set_level_i(0)
        logger.info("Setting the output state to ON.")
        cs.set_output_state(cs.State.ON)

    def _setup_current_source_range(self, value: float):
        """Sets up the range.
        Todo:
            * Prompt user for abrupt range changes (e.g. from 1 A to 1e-4 A)
        """
        cs = self.cs
        frval, prefix = friendly.mprefix(value)
        logger.info(f"Setting current source range to {frval:.1f}{prefix}A.")
        cs.set_range_i(value)

    def _setup_current_source_function(self):
        cs = self.cs
        q = ("The flux source seems to be set to VOLTAGE sourcing mode."
             "You can directly change to CURRENT mode, ramp down to 0 V"
             " and change, or abort.")
        opts = ["Change", "RampChange", "Abort"]

        resp = self._prompt(q, opts, default_optind=2)
        if resp is opts[0]:
            logger.warning("Changing flux source mode to CURRENT.")
            cs.set_source_function(cs.SourceFunction.CURRENT)
        elif resp is opts[1]:
            raise NotImplementedError("Ramping not tested yet.")
        else:
            raise UserAbort("Source mode set to VOLTAGE.")

    def measure_current(self, navg: int = 1) -> ScalarMeasurementModel:
        """Performs `navg` measurements successively and returns the mean and
        stdev of them."""
        vals = np.zeros(navg)

        MAXRETRY = 10
        # the current source comm has issue, may return non-float
        # it rises ValueError in that case
        retry = 0
        i = 0
        while (retry < MAXRETRY) and (i < navg):
            try:
                vals[i] = self.cs.query_measure_i()
            except ValueError as e:
                retry += 1
                if retry >= MAXRETRY:
                    raise ValueError("issue with current source") from e
                else:
                    continue
            i = i + 1

        tstamp = datetime.now()
        meas = self.current_template.evolve(
                timestamp=tstamp,
                value=np.mean(vals),
                error=np.std(vals))
        return meas

    def step_current(self, step: float) -> float:
        """Increments the flux coil current by the given current step.

        Args:
            step:  Current step in Amperes.

        Returns:
            new_current:  The current after setting.
        """
        cs = self.cs

        last_current = cs.query_level_i()

        new_current = last_current + step
        cs.set_level_i(new_current)

        return new_current

    def increment_current(self):
        """Increments the current using the ``current_step`` attribute."""
        return self.step_current(self.current_step)

    def decrement_current(self):
        """Decrements the current using the ``current_step`` attribute."""
        return self.step_current(-self.current_step)

    def step_pump(self, step_db: float) -> float:
        """Increments the pump power by the given step.

        Args:
            step_db:  Pump power step in dB.

        Returns:
            new_power:  The pump power after setting.
        """
        sg = self.sg1

        last_power = sg.query_power()

        new_power = last_power + step_db
        sg.set_power(new_power)

        return new_power

    def increment_pump(self):
        """Increments the pump power using the ``pump_step`` attribute."""
        return self.step_pump(self.pump_step)

    def decrement_pump(self):
        """Decrements the pump power using the ``pump_step`` attribute."""
        return self.step_pump(-self.pump_step)

    def query_current_setpoint(self):
        """Queries the current setpoint on the current sourcing unit."""
        cs = self.cs
        return cs.query_level_i()

    @_stopwatch
    def ramp_current(
            self, target: float):
        """Ramps the current to the `target` value using `current_step`
        property.

        Args:
            target:  Target current value to ramp to, in Amperes.

        """
        if abs(target) >= self._max_flux_current:
            logger.error(f"Max coil current: {self._max_flux_current} A."
                         f"({target})")
            return None
        cs = self.cs
        step = self.current_step
        rate = self.current_ramp_rate

        cs.do_cls()

        i_now = cs.query_level_i()
        _target, _tprefix = friendly.mprefix(target)
        _i_now, _iprefix = friendly.mprefix(i_now)
        _rate, _rprefix = friendly.mprefix(rate)
        logger.info(
            "Preparing to ramp flux current to "
            f"{_target:.3f} {_tprefix}A from {_i_now:.3f} {_iprefix}A with "
            f"a rate of {_rate:.3f} {_rprefix}A/s")
        di = target - i_now

        if di < 0:
            step = -step

        levels = np.arange(i_now, target, step)

        if len(levels) != 0:
            t_tot = abs(di)/rate
            dwell_time = t_tot/len(levels)

        # Set until the last point
        for i, level in enumerate(levels[:-1]):
            if i == 50:
                cs.do_dataqueue_clear()
            _l, _p = friendly.mprefix(level)
            if self.verbose:
                logger.info(f"Current set to {_l:.3f} {_p}A.")
            cs.set_level_i(level)
            i_now = level
            self._sleep(dwell_time)

        # Set the target value after getting close enough.
        # This usually holds because of precision
        cs.set_level_i(target)
        _l, _p = friendly.mprefix(target)
        logger.info(f"Current set to {_l:.3f} {_p}A.")

    @_stopwatch
    def ramp_pump_power(
            self, target: float):
        """Ramps the pump power to the desired value.

        Args:
            target:  Target current value to ramp to, in dBm.

        """
        sg = self.sg1
        step = self.pump_step
        rate = self.pump_ramp_rate

        p_now = sg.query_power()
        logger.info(
            "Preparing to ramp pump power to "
            f"{target:.2f} dBm from {p_now:.2f} dBm with a rate of "
            f"{rate:.2f} dB/s.")
        di = target - p_now

        if di < 0:
            step = -step

        levels = np.arange(p_now, target, step)

        if len(levels) != 0:
            t_tot = abs(di)/rate
            dwell_time = t_tot/len(levels)

        for level in levels[:-1]:
            if self.verbose:
                logger.info(f"Pump power set to {level:.2f} dBm.")
            sg.set_power(level)
            p_now = level
            self._sleep(dwell_time)

        # Set the target value after getting close enough.
        # This usually holds because of precision
        sg.set_power(target)
        logger.info(f"Pump power set to {target:.2f} dBm.")

    @_stopwatch
    def trigger_spectrum_analyzer(self):
        """Triggers and blocks the spectrum analyzer until the measurement is complete.
        """
        sa = self.sa
        last_swt = sa.query_sense_sweep_time()
        last_navg = sa.query_sense_average_count()

        # We need to set the timeout to larger than total measurement time
        swt_ms = last_swt*1000

        # The measurement is expected to complete around this time
        expected_completion_time_ms = last_navg*swt_ms

        # Saving last timeout setting
        oldtimeout = sa.timeout
        # Setting timeout greater than expected time
        # Measurement should be guaranteed to complete before this
        guard_factor = self.timeout_guard_factor
        sa.timeout = expected_completion_time_ms * guard_factor\
            + self.timeout_guard_time_ms

        logger.info("Triggering SA measurement.")
        sa.do_initiate_immediate()
        completion_str = friendly.timedelta(expected_completion_time_ms/1000)
        logger.info(f"Expected completion time: {completion_str}")
        # This blocks
        sa.query_opc()

        # Restoring timeout
        sa.timeout = oldtimeout

    def query_spectrum_y(self, assume_preset: bool = True) -> np.ndarray:
        """Queries the spectrum data as an array.

        Args:
            assume_preset:  Assumes preset values for data format and byte
            order parameters of the instrument.
        """
        sa = self.sa
        if assume_preset:
            byteorder = self.sa_preset.byteorder
            datafmt = self.sa_preset.dataformat
        else:
            byteorder = sa.query_format_byte_order()
            datafmt = sa.query_format_data()

        data = sa.query_trace_data(datafmt, byteorder, traceno=1)
        return data

    def query_spectrum_x(self):
        """Queries the frequency bin centers of the spectrum."""
        sa = self.sa
        fstart = sa.query_sense_frequency_start()
        fstop = sa.query_sense_frequency_stop()
        npoints = sa.query_sense_sweep_points()

        f = np.linspace(fstart, fstop, npoints)
        return f

    def read_spectrum(self) -> SASpectrumModel:
        """Reads the spectrum from the analyzer and returns it as a
        measurement.
        """
        sa = self.sa

        # ASSUMPTION: Measurement is on trace 1
        s = self.query_spectrum_y()

        f = self.query_spectrum_x()

        # ASSUMPTION:  SA has units set to dBm
        rbw = sa.query_sense_bandwidth_resolution()
        vbw = sa.query_sense_bandwidth_video()
        navg = sa.query_sense_average_count()

        # Saving the enums with their names rather than values, since the names
        # are more descriptive.  This is because of the way the enums are used
        # within instrument classes, they actually encode the return strings
        # rather than solely providing enumeration/tokenization.
        detector = sa.query_sense_detector().name
        filter_type = sa.query_sense_bandwidth_shape().name

        measurement = SASpectrumModel(
                frequencies=f,
                values=s,
                rbw=rbw,
                vbw=vbw,
                navg=navg,
                detector=detector,
                filter_type=filter_type)

        return measurement

    def measure_spectrum(self) -> SASpectrumModel:
        """Triggers the spectrum analyzer and reads off the measurement."""
        self.trigger_spectrum_analyzer()
        return self.read_spectrum()

    def measure_transmission(self) -> SParam1PModel:
        """Measures the transmission via VNA."""
        self.trigger_network_analyzer()
        return self.read_s21()

    def measure_baseline(
            self,
            power: t.Optional[float] = None,
            bias_current: t.Optional[float] = None,
            preservesettings: bool = False) -> SParam1PModel:
        """Measures baseline with the baseline_settings for VNA applied.
        If `bias_current` is provided, the coil is ramped to that current
        before baseline is measured.

        Args:
            power:  Overrides the power setting in baseline_settings.
        """
        if preservesettings:
            vnasettings = self.query_vna_settings()

        if power is not None:
            settings = self.baseline_settings.evolve(power=power)
        else:
            settings = self.baseline_settings

        if bias_current is not None:
            logger.info("Setting current for baseline measurement.")
            current = self.query_current_setpoint()
            self.ramp_current(bias_current)

        self.do_vna_preset(settings)
        s21 = self.measure_transmission()

        if bias_current is not None:
            logger.info("Ramping current back to initial value.")
            self.ramp_current(current)

        if preservesettings:
            self.do_vna_preset(vnasettings)
        return s21

    def set_vna_window(
            self,
            center: t.Optional[float] = None,
            span: t.Optional[float] = None,
            sweep_step: t.Optional[float] = None):
        """Centers the vna on `center`, sets the span to `span` and sets the
        frequency steps of the sweep to `sweep step`. If any of the arguments
        is not provided, the setting of that argument is not touched.
        """
        vna = self.vna
        msgs = []
        for k, v in zip(('Center', 'Span', 'Sweep Step'),
                        (center, span, sweep_step)):
            if v is not None:
                msgs.append(f"Setting {k} <- {v}")
        if msgs:  # if list isn't empty
            logger.info("\n".join(msgs))
        domaybe(vna.set_sense_frequency_center, center)
        domaybe(vna.set_sense_frequency_span, span)
        domaybe(vna.set_sense_sweep_step, sweep_step)

    @_stopwatch
    def trigger_network_analyzer(self):
        """Triggers the network analyzer and blocks until the measurement is
        completed.
        """
        vna = self.vna
        # We need to set the timeout to larger than total measurement time
        navg = vna.query_sense_average_count()
        self._last_navg = navg

        # IMPORTANT: This may include the averaging in case of point type
        # average!  Then the reported completion time will be too long.
        swt_ms = vna.query_sense_sweep_time()*1000

        # in milliseconds
        expected_completion_time_ms = navg*swt_ms

        # Saving last timeout setting
        oldtimeout = vna.timeout

        # Setting timeout greater than expected time
        # Measurement should be guaranteed to complete before this
        guard_factor = self.timeout_guard_factor

        # Empirically found that this guard factor isn't enough when you have
        # very small sweep time.
        guard_time_ms = self.timeout_guard_time_ms  # ms
        vna.timeout = expected_completion_time_ms\
            * guard_factor + guard_time_ms

        logger.info("Triggering VNA measurement.")
        if navg > 1:
            # Set trigger source to internal
            vna.do_sense_average_clear()
            vna.set_trigger_source(vna.TriggerSource.IMMEDIATE)
            if self._bf6_compat:
                logger.warning("BF6 Workaround: sense_sweep_mode")
            else:
                vna.set_sense_sweep_mode(vna.SweepMode.GROUPS)
        else:
            if self._bf6_compat:
                vna.set_trigger_source(vna.TriggerSource.IMMEDIATE)
                vna.do_initiate()
            else:
                vna.set_trigger_source(vna.TriggerSource.MANUAL)
                vna.do_initiate()

        completion_str = friendly.timedelta(expected_completion_time_ms/1000)
        logger.info(f"Expected completion time: {completion_str}")
        # This blocks
        vna.query_opc()

        # Restoring timeout
        vna.timeout = oldtimeout

    def query_peak_frequency(self):
        """Queries the pump signal generator's frequency and returns the
        corresponding JPA peak frequency."""
        fpump = self.sg1.query_frequency()
        fpeak = fpump/2
        return fpeak

    def _read_network_param(
            self, dispfmt: SCPIVectorNetworkAnalyzer.DisplayFormat,
            unit: UnitDefinition
            ):
        """Reads the data from the network analyzer."""
        vna = self.vna

        # Measurement format
        vna.set_calculate_format(dispfmt)

        dformat = vna.query_format_data()
        byteorder = vna.query_format_byte_order()

        # For some reason timeout may occur here when this function
        # is called for the first time.  subsequent calls do not fail.

        x_arr = vna.query_calculate_x_values(dformat, byteorder)
        y_arr = vna.query_calculate_data(
                vna.OutDataType.FDATA, dformat, byteorder)

        ifbw = vna.query_sense_bandwidth()
        lnavg = self._last_navg
        if lnavg is None:
            navg = vna.query_sense_average_count()
        else:
            navg = lnavg
        power = vna.query_source_power_level()
        nparam = self.nparam
        displayformat = dispfmt

        measurement = NetworkParams1PModel(
                frequencies=x_arr,
                values=y_arr,
                ifbw=ifbw,
                navg=navg,
                power=power,
                nparam=nparam,
                displayformat=displayformat.name,
                unit=unit)
        return measurement

    def read_s21(self) -> SParam1PModel:
        """Reads the network parameters as a complex value and returns it in a
        `SParam1PModel`.
        """
        vna = self.vna

        dformat = vna.query_format_data()
        byteorder = vna.query_format_byte_order()

        x_arr = vna.query_calculate_x_values(dformat, byteorder)
        y_packed = vna.query_calculate_data(
                vna.OutDataType.SDATA, dformat, byteorder)
        n = len(x_arr)
        # The returning arrays have values in the following format:
        #    [x0_real, x0_imaginary, x1_real, x1_imaginary, ...]
        y_complex_pairs = y_packed.reshape((n, 2))
        # The array of complex values
        y_arr = np.apply_along_axis(
                lambda pair: np.complex(*pair),
                1,  # applied to columns at each row.
                y_complex_pairs)

        ifbw = vna.query_sense_bandwidth()
        lnavg = self._last_navg
        if lnavg is None:
            navg = vna.query_sense_average_count()
        else:
            navg = lnavg
        power = vna.query_source_power_level()
        nparam = self.nparam

        measurement = SParam1PModel.from_complex(
                frequencies=x_arr,
                values=y_arr,
                ifbw=ifbw,
                navg=navg,
                power=power,
                nparam=nparam)
        return measurement

    def read_vna(self) -> SParam1PModel:
        """Reads the active measurement."""
        # this is temporary implementation
        nparam = self.nparam
        self.nparam = self.query_vna_active_param()
        meas = self.read_s21()
        self.nparam = nparam
        return meas

    def query_vna_active_param(self):
        """Queries the selected measurement's measurement parameter."""
        vna = self.vna
        # i don't know if there is a simpler way.
        mname = vna.query_calculate_parameter_mname_select()
        catd = dict(vna.query_calculate_parameter_catalog_ext())
        return catd[mname]

    def read_network_mag(self) -> NetworkParams1PModel:
        """Reads the data of the active measurement as magnitudes in log
        scale."""
        fmt = SCPIVectorNetworkAnalyzer.DisplayFormat.MLOGARITHMIC
        unit = 'dB'
        return self._read_network_param(fmt, unit)

    def read_network_phase(self) -> NetworkParams1PModel:
        """Reads the data of the active measurement as phase (UNWRAPPED)."""
        fmt = SCPIVectorNetworkAnalyzer.DisplayFormat.UPHASE
        unit = 'deg'
        return self._read_network_param(fmt, unit)

    def turn_on_vna_calibration(
            self, calname: t.Optional[str] = None,
            change_stimulus: bool = True):
        """Turns on the VNA calibration.  If calname is a valid string, the
        current calset is changed to the one with a matching name on the device.
        If the string is "", 'CH1_CALREG' is selected."""
        vna = self.vna
        calname = "CH1_CALREG" if calname == "" else calname
        if calname is not None:
            st = {True: vna.State.ON,
                  False: vna.State.OFF}[change_stimulus]
            vna.set_sense_correction_cset_activate(
                    calname,
                    apply_stimulus_settings=st)
        vna.set_sense_correction_state(vna.State.ON)

    def turn_off_vna_calibration(self):
        vna = self.vna
        vna.set_sense_correction_state(vna.State.OFF)

    def query_noise_source_temperature(self) -> float:
        """Queries the temperature of the noise source."""
        channel = self.noise_source_control_channel
        return self.tc.query_kelvin_reading(channel)

    def set_noise_source_temperature(self, setpoint: float):
        """Sends command to the temperature controller to set the temperature
        of the noise source."""
        tc = self.tc
        self._tc_adjust_excitation(setpoint)

        if setpoint > self.temperature_setpoint_max:
            logger.info(
                "Temperature setting exceeded absolute maximum "
                f"permitted {self.temperature_setpoint_max}.  Aborting.")
            self.abort(42)
        tc.set_temperature_setpoint(setpoint)

    def abort(self):
        pass

    def ramp_noise_source_temperature(
            self,
            setpoint: float,
            settle_delay: float,
            timeout: float = 600,
            measurement_period: float = 1,
            measurement_window: int = 50,
            max_fluctuation: float = 1e-2,
            max_error: float = 1e-3):
        """Sends command to the temperature controller to set the temperature
        of the noise source and waits for the stabilization of the temperature.

        Args:
            setpoint:  Setpoint of the source temperature in K.
            settle_delay:  A delay value to be used as an extra wait time after
                stabilization.
            timeout:  Maximum time for trying to ramping up in seconds.
            measurement_period:  Measurement period to be used when
                checking the stabilization of temperature in seconds.
            measurement_window:  Number of samples to collect when calculating
                fluctuations and errors.
            max_fluctuation:  Absolute value for maximum fluctuation.
                Fluctuation is defined as the fractional uncertainty given as
                `Sigma[T]/Mean[T]`.
            max_error:  Maximum value for the fractional error.  Fractional
                error is defined as `Sigma[T-Tset]/Tset`.

        Returns:
            temperature_window:  A 1-d array of last `measurement_window`
                amount of temperature measurements.
            issettled:  A boolean for checking if ramping was successfully
                completed.
        """
        logger.info("Noise source temperature ramping to: "
                    f"{setpoint*1e3:.1f} mK")
        self.set_noise_source_temperature(setpoint)
        tc = self.tc

        # Enough buffer to hold all the data until timeout.
        nbuffer = max(int(timeout/measurement_period)*2, 1)
        logger.debug(f"Creating buffer of size {nbuffer} for "
                     "temperature ramping.")
        tempbuff = np.zeros(nbuffer)

        start = time.time()
        timepassed = 0
        timepassed_last = 0

        settled = False
        rampingdone = False
        equilibrium = False

        reportevery_s = self._temp_report_every_s
        sample_count = 0
        while timepassed <= timeout:
            tempbuff[sample_count] = self.query_noise_source_temperature()

            _tpl, _tp = map(
                    lambda t: int(t) % int(reportevery_s),
                    (timepassed, timepassed_last))

            if self.verbose or (_tpl > _tp):
                logger.info(f"Temperature reading: {tempbuff[sample_count]}")

            sample_count += 1

            window_begin = sample_count - measurement_window
            window = tempbuff[max(window_begin, 0):sample_count]

            if not rampingdone:
                if tc.query_ramp_status() == tc.RampStatus.NORAMPING:
                    logger.info("Setpoint ramping finished.")
                    logger.info("Waiting for equilibrium.")
                    rampingdone = True
            elif not equilibrium and (sample_count >= measurement_window):
                fluctuation = window.std()/window.mean()
                error = np.abs(window.mean() - setpoint)/setpoint

                if (fluctuation <= max_fluctuation and error <= max_error):
                    logger.info("Fractional temperature fluctuation is: "
                                f"{fluctuation} <= {max_fluctuation}.")
                    logger.info("Fractional temperature error is: "
                                f"{error} <= {max_error}.")
                    logger.info("Equilibrium reached. Waiting for a "
                                f"guard period of {settle_delay} s.")
                    equilibrium = True
                    equilibrium_start = time.time()
            elif equilibrium:  # Waiting for `settle_delay` after equilibrium
                since_equilibrium = time.time() - equilibrium_start
                if since_equilibrium > settle_delay:
                    logger.info("Settling finished.")
                    settled = True
                    break
            else:
                # waiting
                pass

            self._sleep(measurement_period)
            timepassed_last = timepassed
            timepassed = time.time() - start

        if not settled:
            logger.warning("Equilibrium was not reached.")

        return window, settled

    def _tc_adjust_excitation(self, setpoint):
        """Adjusts the excitation used for voltage measurement.

        Things here are currently hardcoded unfortunately.
        """
        tc = self.tc
        ch = self.noise_source_control_channel
        mode, excitation, autorange, resrange, csshunt, units = tc.query_input_setup(ch)  # noqa: E501
        exc_old = excitation
        if mode == tc.CurrentExcitation:
            raise NotImplementedError("Current excitation is not defined yet.")
            return  # dont do anything if not set to voltage excitation
        if within(setpoint, 0.01, 0.05):
            excitation = tc.VoltageExcitation.V6u32
        elif within(setpoint, 0.05, 0.2):
            excitation = tc.VoltageExcitation.V20u0
        elif within(setpoint, 0.2, 0.7):
            excitation = tc.VoltageExcitation.V63u2
        elif within(setpoint, 0.7, 1):
            excitation = tc.VoltageExcitation.V200u

        # issue a change only if new excitation is to be different
        if exc_old != excitation:
            logger.info(f"Changing excitation from {exc_old.name} "
                        f"to {excitation.name}.")
            tc.set_input_setup(
                    ch, mode, excitation, autorange, resrange, csshunt, units)
