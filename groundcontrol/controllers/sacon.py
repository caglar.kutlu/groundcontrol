"""Spectrum Analyzer Controller module.

This module implements a spectrum analyzer controller module that is intended
to be a layer between bare instruments and the applications.

Most of the time instruments provide similar functionality described under
different terms.  This class's responsibility is to provide a uniform interface
to the application, as much as possible.

"""

import typing as t
from functools import wraps, partial
import time

import cattr
import numpy as np

from groundcontrol.controller import Controller, instrument
from groundcontrol.measurement import (
        MeasurementModel, quantity, parameter)
from groundcontrol.instruments import RohdeSchwarzFSV, KeysightXSeries
from groundcontrol.logging import setup_logger, INFO
from groundcontrol.settings import InstrumentSettings, setting
from groundcontrol.helper import StrEnum, domaybe
import groundcontrol.units as _un


_settingo = partial(setting, default=None)
logger = setup_logger('sacon.py')

XSeries = KeysightXSeries


class PowerSpectrum(MeasurementModel):
    """A lightweight data class for spectrum measurements."""
    frequency: float = quantity(_un.hertz, "Frequency")
    power: float = quantity(_un.dBm, "Power")
    rbw: float = parameter(_un.hertz, '3dB Bandwidth')
    nbw: float = parameter(_un.hertz, 'Noise Bandwidth')

    @power.validator
    def _checklen(self, attribute, value):
        nfreq = len(self.frequency)
        if len(value) != nfreq:
            msg = "Length mismatch between'{attribute.name}' and 'frequency'."
            raise ValueError(msg)


def loggledeedoo(before=None, after=None, level=INFO):
    # i'm tired of naming stuff (:

    def decoradora(fun):
        @wraps(fun)
        def wrapped(*args, **kwargs):
            if before is not None:
                logger.log(level, before)
            ret = fun(*args, **kwargs)
            if after is not None:
                logger.log(level, after)
            return ret
        return wrapped
    return decoradora


class SASettings(InstrumentSettings):
    """Data class for common SA settings."""

    class DetectorType(StrEnum):
        NORMAL = "NORM"
        AVERAGE = "AVER"
        POSITIVE = "POS"
        SAMPLE = "SAMP"
        NEGATIVE = "NEG"
        QUASIPEAK = "QPE"
        EMIAVERAGE = "EAV"
        RMSAVERAGE = "RAV"
        RMS = "RMS"

    class FilterBWType(StrEnum):
        DB3 = "DB3"
        DB6 = "DB6"
        IMPULSE = "IMP"
        NOISE = "NOIS"

    class FilterType(StrEnum):
        GAUSSIAN = "GAUS"
        FLATTOP = "FLAT"
        BH92 = "BH92"
        RECTANGULAR = "RECT"

    detector: t.Optional[DetectorType] = _settingo(
            _un.nounit, "Detector Type", converter=DetectorType)
    filterbw: t.Optional[FilterBWType] = _settingo(
            _un.nounit, "Filter BW Type", converter=FilterBWType)
    filtertype: t.Optional[FilterType] = _settingo(
            _un.nounit, "Filter Type", converter=FilterType)
    start_frequency: t.Optional[float] = _settingo(
            _un.hertz, "Start Frequency")
    stop_frequency: t.Optional[float] = _settingo(_un.hertz, "Stop Frequency")
    center_frequency: t.Optional[float] = _settingo(
            _un.hertz, "Center Frequency")
    span: t.Optional[float] = _settingo(_un.hertz, "Span")
    rbw: t.Optional[float] = _settingo(_un.hertz, "Resolution Bandwidth")
    vbw: t.Optional[float] = _settingo(_un.hertz, "Video Bandwidth")
    swt: t.Optional[float] = _settingo(_un.second, "Sweep Time")
    swt_auto: t.Optional[bool] = _settingo(_un.nounit, "Auto Sweep Time")
    npoints: t.Optional[int] = _settingo(_un.nounit, "Sweep Points")
    naverage: t.Optional[int] = _settingo(_un.nounit, "Average Count")

    @classmethod
    def from_dict(cls, d: t.Dict):
        return cattr.structure_attrs_fromdict(d, cls)


class SAController(Controller):
    """Controller for concrete SA devices.

    Note that different instruments may have different definitions for the
    following parameters (and probably more):
      - Span
      - Start and stop frequencies
      - Sweep time
    This class tries its best to provide a common definition interface to
    varying devices.  Currently, it uses the following definitions:

        SPAN:  Frequency of the last bin minus frequency of the first bin.
        STARTFREQ:  Frequency of the first bin OR CENTERFREQ - SPAN/2
        STOPFREQ:  Frequency of the last bin OR CENTERFREQ + SPAN/2

    For the record, Keysight X-Series SA uses this same definition whereas R&S
    FSV series devices follow a different definition:
        SPAN:  Frequency of the last bin minus frequency of the first bin +
            width of one bin.
        STARTFREQ:  CENTERFREQ - SPAN/2
        STOPFREQ:  CENTERFREQ + SPAN/2

    """
    sa: t.Union[XSeries, RohdeSchwarzFSV] = instrument()

    timeout_guard_factor: float = 10
    timeout_guard_time_ms: float = 3000
    timeout_default_ms: float = 3000

    _isrohde: bool = False
    _byteorder: XSeries.ByteOrderFormat = XSeries.ByteOrderFormat.LITTLE_ENDIAN
    _dataformat: XSeries.FormatDataType = XSeries.FormatDataType.REAL32

    def __setup__(self):
        # Some settings are hardcoded for the sake of sanity.
        sa = self.sa

        # default timeout
        sa.timeout = self.timeout_default_ms

        # Single trigger mode
        sa.set_initiate_continuous(sa.State.OFF)

        # Averaging Powers == RMS detector
        sa.set_sense_average_type(sa.AverageType.POWER)

        sa.set_format_byte_order(self._byteorder)
        sa.set_format_data(self._dataformat)

        self._isrohde = isinstance(sa, RohdeSchwarzFSV)

    def query_bw_info(self):
        """Special function to query 3dB bandwidth and noise bandwidth information.
        This temporarily changes the `filterbw` setting.

        Returns:
            rbw3db:  RBW using 3dB definition.
            rbwnbw:  RBW using NOISE definition, i.e. noise bandwidth.

        """
        if self._isrohde:
            rbw3db = self.query_rbw()  # careful
            nrbw = 1.8962
            nenbw = 2.0044
            nbw = (rbw3db/nrbw)*nenbw
            return rbw3db, nbw
        else:
            fbw = self.query_filter_bw_type()
            self.set_filter_bw_type(SASettings.FilterBWType.DB3)
            rbw3db = self.query_rbw()
            self.set_filter_bw_type(SASettings.FilterBWType.NOISE)
            rbwnbw = self.query_rbw()
            self.set_filter_bw_type(fbw)
        return rbw3db, rbwnbw

    def set_settings(self, settings: SASettings):
        if settings.swt_auto:
            swt = None
        else:
            swt = settings.swt
        domaybe(self.set_detector_type, settings.detector)
        domaybe(self.set_filter_type, settings.filtertype)  # order important
        domaybe(self.set_filter_bw_type, settings.filterbw)  # order important
        domaybe(self.set_npoints, settings.npoints)  # order important
        domaybe(self.set_start_frequency, settings.start_frequency)
        domaybe(self.set_stop_frequency, settings.stop_frequency)
        domaybe(self.set_center_frequency, settings.center_frequency)
        domaybe(self.set_span, settings.span)
        domaybe(self.set_rbw, settings.rbw)
        domaybe(self.set_vbw, settings.vbw)
        domaybe(self.set_swt, swt)
        domaybe(self.set_swt_auto, settings.swt_auto)
        domaybe(self.set_naverage, settings.naverage)

    def query_settings(self) -> SASettings:
        settings = SASettings(
                detector=self.query_detector_type(),
                start_frequency=self.query_start_frequency(),
                stop_frequency=self.query_stop_frequency(),
                center_frequency=self.query_center_frequency(),
                span=self.query_span(),
                rbw=self.query_rbw(),
                vbw=self.query_vbw(),
                npoints=self.query_npoints(),
                swt=self.query_swt(),
                swt_auto=self.query_swt_auto(),
                naverage=self.query_naverage(),
                filterbw=self.query_filter_bw_type(),
                filtertype=self.query_filter_type()
                )
        return settings

    def set_detector_type(self, dettype: SASettings.DetectorType):
        sa = self.sa
        sa.set_sense_detector(dettype)

    def query_detector_type(self) -> SASettings.DetectorType:
        sa = self.sa
        return sa.query_sense_detector()

    def set_filter_bw_type(self, bwtype: SASettings.FilterBWType):
        self.sa.set_sense_bandwidth_type(
                bwtype)

    def query_filter_bw_type(self) -> SASettings.FilterBWType:
        return self.sa.query_sense_bandwidth_type()

    def set_filter_type(self, ftype: SASettings.FilterType):
        self.sa.set_sense_bandwidth_shape(ftype)

    def query_filter_type(self) -> SASettings.FilterType:
        return self.sa.query_sense_bandwidth_shape()

    def query_binwidth(self) -> float:
        span = self.query_span()
        nbins = self.query_npoints()
        return self._calc_binwidth(span, nbins)

    def set_start_frequency(self, value_hz: float):
        if self._isrohde:
            self._set_start_frequency_rohde(value_hz)
        else:
            self.sa.set_sense_frequency_start(value_hz)

    def _set_start_frequency_rohde(self, value_hz: float):
        binwidth = self.query_binwidth()
        self.sa.set_sense_frequency_start(value_hz - binwidth/2)

    def query_start_frequency(self) -> float:
        if self._isrohde:
            return self._query_start_frequency_rohde()
        else:
            return self.sa.query_sense_frequency_start()

    def _query_start_frequency_rohde(self) -> float:
        binwidth = self.query_binwidth()
        rstartf = self.sa.query_sense_frequency_start()
        return rstartf + binwidth/2

    def set_stop_frequency(self, value_hz: float):
        if self._isrohde:
            self._set_stop_frequency_rohde(value_hz)
        else:
            self.sa.set_sense_frequency_stop(value_hz)

    def _set_stop_frequency_rohde(self, value_hz: float):
        binwidth = self.query_binwidth()
        self.sa.set_sense_frequency_stop(value_hz + binwidth/2)

    def query_stop_frequency(self) -> float:
        if self._isrohde:
            return self._query_stop_frequency_rohde()
        else:
            return self.sa.query_sense_frequency_stop()

    def _query_stop_frequency_rohde(self) -> float:
        binwidth = self.query_binwidth()
        rstopf = self.sa.query_sense_frequency_stop()
        return rstopf - binwidth/2

    def set_center_frequency(self, value_hz: float):
        self.sa.set_sense_frequency_center(value_hz)

    def query_center_frequency(self) -> float:
        return self.sa.query_sense_frequency_center()

    def set_span(self, value_hz: float):
        if self._isrohde:
            self._set_span_rohde(value_hz)
        else:
            self.sa.set_sense_frequency_span(value_hz)

    def _set_span_rohde(self, value_hz: float):
        tspan = value_hz
        npoints = self.query_npoints()
        binwidth = self._calc_binwidth(tspan, npoints, isrohde=False)

        rohdespan = self._calc_span(binwidth, npoints, isrohde=True)
        self.sa.set_sense_frequency_span(rohdespan)

    def query_span(self) -> float:
        if self._isrohde:
            return self._query_span_rohde()
        else:
            return self.sa.query_sense_frequency_span()

    def _query_span_rohde(self) -> float:
        npoints = self.query_npoints()
        span = self.sa.query_sense_frequency_span()
        binwidth = self._calc_binwidth(span, npoints, isrohde=True)
        return self._calc_span(binwidth, npoints, isrohde=False)

    def set_rbw(self, value_hz: float):
        self.sa.set_sense_bandwidth_resolution(value_hz)

    def query_rbw(self) -> float:
        return self.sa.query_sense_bandwidth_resolution()

    def set_vbw(self, value_hz: float):
        self.sa.set_sense_bandwidth_video(value_hz)

    def query_vbw(self) -> float:
        return self.sa.query_sense_bandwidth_video()

    def set_npoints(self, value: int):
        self.sa.set_sense_sweep_points(value)

    def query_npoints(self) -> int:
        return self.sa.query_sense_sweep_points()

    def set_swt(self, seconds) -> float:
        self.sa.set_sense_sweep_time(seconds)

    def query_swt(self) -> float:
        return self.sa.query_sense_sweep_time()

    def set_swt_auto(self, isauto: bool) -> float:
        ON = XSeries.State.ON
        OFF = XSeries.State.OFF
        state = ON if isauto else OFF
        self.sa.set_sense_sweep_time_auto(state)

    def query_swt_auto(self) -> bool:
        state = self.sa.query_sense_sweep_time_auto()
        return state == XSeries.State.ON

    def set_naverage(self, value: int):
        sa = self.sa
        if value > 1:
            sa.set_sense_average_count(value)
            sa.set_sense_average_state(sa.State.ON)
        else:
            sa.set_sense_average_count(1)
            sa.set_sense_average_state(sa.State.OFF)

    def query_naverage(self) -> int:
        return self.sa.query_sense_average_count()

    def trigger(self):
        """Returns:
            expectedtime:  Expected completion time in ms.
        """
        last_swt = self.query_swt()
        last_navg = self.query_naverage()

        # We need to set the timeout to larger than total measurement time
        swt_ms = last_swt*1000

        # The measurement is expected to complete around this time
        expected_completion_time_ms = last_navg*swt_ms
        self.sa.do_initiate_immediate()

        return expected_completion_time_ms

    def trigger_blocking(self):
        """Triggers and blocks until the measurement is
        complete.
        """
        sa = self.sa

        _start = time.time()
        expected_completion_time_ms = self.trigger()

        # Saving last timeout setting
        oldtimeout = sa.timeout
        # Setting timeout greater than expected time
        # Measurement should be guaranteed to complete before this
        guard_factor = self.timeout_guard_factor

        # *OPC? style synchronization sometimes fails for FSV SA, when the SWT
        # > 200s.  Here is a workaround for that.
        _wathreshold = 200*1e3
        if expected_completion_time_ms < _wathreshold:
            sa.timeout = expected_completion_time_ms * guard_factor\
                + self.timeout_guard_time_ms
        else:
            q, r = divmod(expected_completion_time_ms, _wathreshold)
            sleeptime_ms = q*_wathreshold
            self._sleep(sleeptime_ms/1e3)
            oldtimeout = sa.timeout
            guard_factor = self.timeout_guard_factor
            sa.timeout = (
                    expected_completion_time_ms * guard_factor
                    + self.timeout_guard_time_ms - sleeptime_ms + r)

        sa.query_opc()
        _end = time.time()
        logger.info(f"It took {_end-_start:.1f} s.")

        # Restoring timeout
        sa.timeout = oldtimeout

    def _sleep(self, secs: float):
        time.sleep(secs)

    def pause(self):
        self.sa.do_pause()

    def resume(self):
        self.sa.do_resume()

    def query_trace_x(self, fake=True):
        """Queries the frequency bin centers of the spectrum."""
        if fake:
            fstart = self.query_start_frequency()
            fstop = self.query_stop_frequency()
            npoints = self.query_npoints()

            f = np.linspace(fstart, fstop, npoints)
        else:
            raise NotImplementedError
        return f

    def query_trace_y(self, traceno: int) -> np.ndarray:
        """Queries the trace y values.

        Args:
            assume_preset:  Assumes preset values for data format and byte
            order parameters of the instrument.
        """
        sa = self.sa

        data = sa.query_trace_data(
                self._dataformat,
                self._byteorder, traceno=traceno)
        return data

    def read_spectrum(self, traceno: int = 1) -> PowerSpectrum:
        # unit check may go here
        # or maybe read_trace instead of spectrum?

        s = np.array(self.query_trace_y(traceno))
        f = np.array(self.query_trace_x())

        rbw, nbw = self.query_bw_info()

        measurement = PowerSpectrum(
                frequency=f,
                power=s,
                rbw=rbw,
                nbw=nbw)
        return measurement

    def measure_spectrum(self) -> PowerSpectrum:
        """Blocking measurement"""
        self.trigger_blocking()
        return self.read_spectrum()

    @loggledeedoo("SA alignment all initiated.")
    def align_all_blocking(self) -> bool:
        """Succes: True, Fail: False"""
        sa = self.sa
        tmout = sa.timeout
        sa.timeout = 50000
        result = sa.Result.SUCCESS == sa.do_query_calibration()
        sa.timeout = tmout
        return result

    @loggledeedoo("SA alignment all initiated.")
    def align_all(self):
        """Succes: True, Fail: False"""
        sa = self.sa
        sa.do_calibration()

    def align_rf(self) -> bool:
        """Succes: True, Fail: False"""
        return self.sa.Result.SUCCESS == self.sa.do_query_calibration_rf()

    def align_nonrf(self) -> bool:
        """Succes: True, Fail: False"""
        return self.sa.Result.SUCCESS == self.sa.do_query_calibration_nrf()

    def user_preset(self):
        self.sa.do_system_preset_user()

    def center_preselector(self):
        """Centers the preselector."""
        # we may add a routine here, e.g. center at
        # this or that frequency.
        self.sa.do_sense_power_rf_pcenter()

    def set_preselector_center(self, value_hz: float):
        self.sa.set_sense_power_rf_padjust(value_hz)

    def query_preselector_center(self) -> float:
        self.sa.query_sense_power_rf_padjust()

    def measure_spectrum_gen(
            self
            ) -> t.Generator[t.Optional[PowerSpectrum], None, None]:
        """Returns a generator for the power spectrum measurement via SA.
        The returned generator will first yield expected completion time right
        after triggering.
        The second and final yield is done after checking and waiting-if
        necessary- for the SA measurement to complete.  Final yield is a
        'PowerSpectrum'.
        """
        exptime_ms = self.trigger()
        trigtime = time.time()
        yield exptime_ms
        now = time.time()
        oldtimeout = self.sa.timeout
        tmout = (
                (exptime_ms*self.timeout_guard_factor
                    + self.timeout_guard_time_ms)
                - (now - trigtime)*1000)
        tmout = max(tmout, self.timeout_guard_time_ms)
        self.sa.timeout = tmout
        self.sa.query_opc()

        self.sa.timeout = oldtimeout
        spectrum = self.read_spectrum()
        yield spectrum

    @staticmethod
    def _calc_span(binwidth, nbins, isrohde=False):
        if isrohde:
            span = nbins * binwidth
        else:
            span = (nbins - 1) * binwidth
        return span

    @staticmethod
    def _calc_binwidth(span, nbins, isrohde=False):
        """Span here is as responded by the instrument."""
        if isrohde:
            return span/nbins
        else:
            return span/(nbins-1)
