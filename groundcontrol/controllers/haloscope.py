"""THIS IS GOING TO BE DEPRECATED
"""
import typing as t
from functools import wraps
import time

import numpy as np

from groundcontrol import friendly
from groundcontrol.controller import Controller, instrument
from groundcontrol.instruments import AttocubeANC350, SCPISignalGenerator,\
    SCPISpectrumAnalyzer, LS372TemperatureController, TSPCurrentSource,\
    SCPIVectorNetworkAnalyzer
from groundcontrol.measurement import SParam1PModel
from groundcontrol.logging import setup_logger
from groundcontrol.helper import domaybe
from groundcontrol.settings import SASettings, VNASettings, CSSettings


logger = setup_logger('haloscon')
logger.warning("THIS CONTROLLER WILL BE DEPRECATED")


def _stopwatch(fun):
    """Decorator for measuring how long a function takes to return."""
    @wraps(fun)
    def decorated(*args, **kwargs):
        start = time.time()
        logger.debug(f"Calling function '{fun.__name__}'")
        out = fun(*args, **kwargs)
        passed = time.time() - start
        logger.debug(f"Took {friendly.timedelta(passed)} to return.")
        return out
    return decorated


class HaloscopeController(Controller):
    """Bare controller class for haloscope experiment.  This class is not
    responsible for implementing experimental sequence.

    Most of this is actually copied from JPAController in a hackish way.

    Instruments:
        sg1:  First signal generator.
        sa:  Spectrum analyzer.
        vna:  Network analyzer.
        tc:  Temperature controller.
        cs:  Current source.
        pc: Piezo controller.

    """
    sg1: SCPISignalGenerator = instrument()
    sa: SCPISpectrumAnalyzer = instrument()
    vna: SCPIVectorNetworkAnalyzer = instrument()
    tc: LS372TemperatureController = instrument()
    cs: TSPCurrentSource = instrument()
    pc: AttocubeANC350 = instrument()

    timeout_guard_factor: float = 10
    timeout_guard_time_ms: float = 500

    _nparam: str = "S21"

    _vna_preset: VNASettings = VNASettings()
    _sa_preset: SASettings = SASettings()
    _cs_preset: CSSettings = CSSettings(range_i=1e-3)
    _bf6_compat: bool = True
    _last_navg: int = 1

    def _prompt(
            self,
            message: str,
            options: t.List[str],
            default_optind: t.Optional[int] = None) -> t.Optional[str]:

        optsfmt = []
        for i, opt in enumerate(options):
            if i == default_optind:
                optsfmt.append(f'[{opt}]')
            else:
                optsfmt.append(opt)
        options_text = '/'.join(optsfmt)

        s = f"{message} ({options_text}) "
        resp = input(s)
        opt: str
        for opt in options:
            if opt.startswith(resp):
                return opt
        return None

    def __setup__(self):
        if self.sa is not None:
            self.setup_sa()
        if self.vna is not None:
            self.setup_vna()
        if self.cs is not None:
            self.setup_cs()

    def setup_sa(self):
        """Sets up spectrum analyzer"""
        sa = self.sa
        saset = self._sa_preset

        # Hardcoded settings

        # Single trigger mode
        sa.set_initiate_continuous(sa.State.OFF)

        # HARD SETTING: Averaging Powers == RMS detector
        sa.set_sense_average_type(sa.AverageType.POWER)

        # Supplied initial settings
        self.set_sa_settings(saset)

    def setup_vna(self, clean: bool = False):
        """Sets up vector network analyzer.
        Args:
            clean: If `True`, cleans up windows set on the VNA first.
        """
        vna = self.vna
        vnaset = self._vna_preset

        # Hardcoded settings
        if self._bf6_compat:
            logger.warning("BF6 WORKAROUND: no SourcePowerMode AUTO")
        else:
            vna.set_source_power_mode(vna.SourcePowerMode.AUTO)

        mname = "REFLECTION"
        param = self._nparam

        if clean:
            logger.debug("Cleaning measurements on VNA.")
            vna.do_calculate_parameter_delete_all()
            vna.do_calculate_parameter_define_ext(mname=mname, param=param)
            vna.set_calculate_parameter_mname_select(mname)
            logger.debug(f"New measurement with name {mname} is created "
                         f"for {param} measurement.")
        else:
            existing_measurements = vna.query_calculate_parameter_catalog_ext()
            logger.debug("Existing measurements on VNA:"
                         f"{existing_measurements}")
            exparam = filter(
                    lambda tpl: tpl[1] == param, existing_measurements)
            try:
                # If there is an existing `param` measurement, use it.
                name, param = next(exparam)  # pick the first match
                vna.set_calculate_parameter_mname_select(name)
                mname = name  # use the existing name.
                logger.debug(f"Measurement with name '{name}' found for "
                             f"'{param}' measurement. Using it.")
            except StopIteration:  # next(exparam) throws this
                # If not, create one
                vna.do_calculate_parameter_define_ext(mname=mname, param=param)
                vna.set_calculate_parameter_mname_select(mname)
                logger.debug(f"New measurement with name {mname} is created "
                             f"for {param} measurement.")

        # Display the trace only if it's not already displayed.
        if self._bf6_compat:
            logger.warning("BF6 WORKAROUND: 'tnumber'")
            tnum = vna.query_configure_trace_name_id(mname)
        else:
            tnum = vna.query_calculate_parameter_tnumber()

        tnums = vna.query_display_window_catalog()
        if tnum not in tnums:
            try:
                tnum = max(tnums) + 1  # just pick a trace number
            except ValueError:  # if empty
                tnum = 1
            vna.do_display_window_trace_feed(mname)

        # Single trigger mode
        vna.set_initiate_continuous(vna.State.OFF)

        # Supplied initial settings
        vna.set_format_byte_order(vnaset.byteorder)
        vna.set_format_data(vna.FormatDataType.REAL64)

    def setup_cs(self):
        """Sets up current source."""
        cs = self.cs
        csset = self._cs_preset
        sf: cs.SourceFunction = cs.query_source_function()

        if sf == cs.SourceFunction.VOLTAGE:
            self._setup_current_source_function()
        else:
            self._setup_current_source_range(csset.range_i)

        if cs.query_output_state() is cs.State.OFF:
            q = ("The DC flux source seems to be OFF.  It can be turned on "
                 "with the current set to 0A, would you like to turn it "
                 "on?")
            opts = ["Yes", "No"]
            resp = self._prompt(q, opts, default_optind=1)
            if resp == opts[0]:
                self._setup_current_source_turnon()

    def set_sa_settings(self, preset: SASettings):
        sa = self.sa
        domaybe(sa.set_format_byte_order, preset.byteorder)
        domaybe(sa.set_format_data, preset.dataformat)
        domaybe(sa.set_sense_detector, preset.detector)
        domaybe(sa.set_sense_frequency_start, preset.start_frequency)
        domaybe(sa.set_sense_frequency_stop, preset.stop_frequency)
        domaybe(sa.set_sense_frequency_center, preset.center_frequency)
        domaybe(sa.set_sense_frequency_span, preset.span)
        domaybe(sa.set_sense_bandwidth_resolution, preset.rbw)
        domaybe(sa.set_sense_bandwidth_video, preset.vbw)
        domaybe(sa.set_sense_sweep_points, preset.npoints)
        domaybe(sa.set_sense_sweep_time_auto, preset.swt_auto)
        domaybe(sa.set_sense_sweep_time, preset.swt)
        domaybe(self._set_spectrum_average, preset.naverage)

    def query_sa_settings(self) -> SASettings:
        sa = self.sa
        settings = SASettings(
                byteorder=sa.query_format_byte_order(),
                dataformat=sa.query_format_data(),
                detector=sa.query_sense_detector(),
                start_frequency=sa.query_sense_frequency_start(),
                stop_frequency=sa.query_sense_frequency_stop(),
                center_frequency=sa.query_sense_frequency_center(),
                span=sa.query_sense_frequency_span(),
                rbw=sa.query_sense_bandwidth_resolution(),
                vbw=sa.query_sense_bandwidth_video(),
                swt=sa.query_sense_sweep_time(),
                swt_auto=sa.query_sense_sweep_time_auto(),
                npoints=sa.query_sense_sweep_points(),
                naverage=sa.query_sense_average_count())
        return settings

    def _set_spectrum_average(self, navg: int):
        """Sets the spectrum average number for SA."""
        sa = self.sa
        if navg <= 1:
            sa.set_sense_average_state(sa.State.OFF)
            sa.set_trace_type(sa.TraceType.CLEARWRITE)
        else:
            sa.set_sense_average_state(sa.State.ON)
            sa.set_trace_type(sa.TraceType.AVERAGE)
        sa.set_sense_average_count(navg)

    def set_vna_settings(self, vnaset: VNASettings):
        vna = self.vna
        vna.set_format_byte_order(vnaset.byteorder)
        vna.set_format_data(vna.FormatDataType.REAL64)

        if vnaset.sweep_step == 0:
            # when this holds, always span=0 is meant.
            vnaset.sweep_step = None
            vnaset.span = 0

        domaybe(vna.set_sense_frequency_start, vnaset.start_frequency)
        domaybe(vna.set_sense_frequency_stop, vnaset.stop_frequency)
        domaybe(vna.set_sense_frequency_center, vnaset.center_frequency)
        domaybe(vna.set_sense_frequency_span, vnaset.span)
        domaybe(vna.set_source_power_level, vnaset.power)
        domaybe(vna.set_sense_sweep_step, vnaset.sweep_step)
        domaybe(vna.set_sense_bandwidth, vnaset.if_bandwidth)
        domaybe(self._set_vna_average, vnaset.naverage)
        domaybe(self._set_vna_npoints, vnaset.npoints)

    def query_vna_settings(self) -> VNASettings:
        v = self.vna
        settings = VNASettings(
                byteorder=v.query_format_byte_order(),
                dataformat=v.query_format_data(),
                start_frequency=v.query_sense_frequency_start(),
                stop_frequency=v.query_sense_frequency_stop(),
                center_frequency=v.query_sense_frequency_center(),
                span=v.query_sense_frequency_span(),
                sweep_step=v.query_sense_sweep_step(),
                if_bandwidth=v.query_sense_bandwidth(),
                naverage=v.query_sense_average_count(),
                power=v.query_source_power_level())
        return settings

    def _set_vna_average(self, navg: int):
        """Sets the spectrum average number for VNA."""
        vna = self.vna
        vna.set_sense_average_count(navg)
        if navg <= 1:
            vna.set_sense_average_state(vna.State.OFF)
            # vna.set_trigger_source(vna.TriggerSource.MANUAL)
        else:
            vna.set_sense_average_state(vna.State.ON)
            # Single trigger causes a group of sweeps to be measured
            # Group count is made equal to the number of averages.
            vna.set_sense_sweep_groups_count(navg)

    def _set_vna_npoints(self, npoints: int):
        """Sets the number of points."""
        vna = self.vna
        vna.set_sense_sweep_points(npoints)

    def do_vna_autoscale(self):
        """Auto scales the y-axis in the default window and trace."""
        self.vna.do_display_window_trace_y_scale_auto()

    def activate_vna_output(self):
        """Activates the signal (VNA) output power."""
        self.vna.set_output(self.vna.State.ON)

    def deactivate_vna_output(self):
        """Deactivates the signal (VNA) output power."""
        self.vna.set_output(self.vna.State.OFF)

    def set_vna_window(
            self,
            center: t.Optional[float] = None,
            span: t.Optional[float] = None,
            sweep_step: t.Optional[float] = None):
        """Centers the vna on `center`, sets the span to `span` and sets the
        frequency steps of the sweep to `sweep step`. If any of the arguments
        is not provided, the setting of that argument is not touched.
        """
        vna = self.vna
        msgs = []
        for k, v in zip(('Center', 'Span', 'Sweep Step'),
                        (center, span, sweep_step)):
            if v is not None:
                msgs.append(f"Setting {k} <- {v}")
        if msgs:  # if list isn't empty
            logger.info("\n".join(msgs))
        domaybe(vna.set_sense_frequency_center, center)
        domaybe(vna.set_sense_frequency_span, span)
        domaybe(vna.set_sense_sweep_step, sweep_step)

    @_stopwatch
    def trigger_vna_blocking(self):
        """Triggers the network analyzer and blocks until the measurement is
        completed.
        """
        timeout = self.trigger_vna()
        oldtimeout = vna.timeout
        vna.timeout = timeout
        # This blocks
        vna.query_opc()

        # Restoring timeout
        vna.timeout = oldtimeout

    def trigger_vna(self):
        vna = self.vna
        # We need to set the timeout to larger than total measurement time
        avgstate = vna.query_sense_average_state()
        if avgstate == vna.State.ON:
            navg = vna.query_sense_average_count()
        else:
            navg = 1
        self._last_navg = 1

        # IMPORTANT: This may include the averaging in case of point type
        # average!  Then the reported completion time will be too long.
        swt_ms = vna.query_sense_sweep_time()*1000

        # in milliseconds
        expected_completion_time_ms = navg*swt_ms

        # Saving last timeout setting
        oldtimeout = vna.timeout

        # Setting timeout greater than expected time
        # Measurement should be guaranteed to complete before this
        guard_factor = self.timeout_guard_factor

        # Empirically found that this guard factor isn't enough when you have
        # very small sweep time.
        guard_time_ms = self.timeout_guard_time_ms  # ms
        newtimeout = expected_completion_time_ms\
            * guard_factor + guard_time_ms

        logger.info("Triggering VNA measurement.")
        if navg > 1:
            # Set trigger source to internal
            vna.do_sense_average_clear()
            vna.set_trigger_source(vna.TriggerSource.IMMEDIATE)
            if self._bf6_compat:
                logger.warning("BF6 Workaround: sense_sweep_mode")
            else:
                vna.set_sense_sweep_mode(vna.SweepMode.GROUPS)
        else:
            if self._bf6_compat:
                vna.set_trigger_source(vna.TriggerSource.IMMEDIATE)
                vna.do_initiate()
            else:
                vna.set_trigger_source(vna.TriggerSource.MANUAL)
                vna.do_initiate()

        completion_str = friendly.timedelta(expected_completion_time_ms/1000)
        logger.info(f"Expected completion time: {completion_str}")
        return newtimeout

    def read_s21(self) -> SParam1PModel:
        """Reads the network parameters as a complex value and returns it in a
        `SParam1PModel`.
        """
        vna = self.vna

        dformat = vna.query_format_data()
        byteorder = vna.query_format_byte_order()

        x_arr = vna.query_calculate_x_values(dformat, byteorder)
        y_packed = vna.query_calculate_data(
                vna.OutDataType.SDATA, dformat, byteorder)
        n = len(x_arr)
        # The returning arrays have values in the following format:
        #    [x0_real, x0_imaginary, x1_real, x1_imaginary, ...]
        y_complex_pairs = y_packed.reshape((n, 2))
        # The array of complex values
        y_arr = np.apply_along_axis(
                lambda pair: np.complex(*pair),
                1,  # applied to columns at each row.
                y_complex_pairs)

        ifbw = vna.query_sense_bandwidth()
        lnavg = self._last_navg
        if lnavg is None:
            navg = vna.query_sense_average_count()
        else:
            navg = lnavg
        power = vna.query_source_power_level()
        nparam = self._nparam

        measurement = SParam1PModel.from_complex(
                frequencies=x_arr,
                values=y_arr,
                ifbw=ifbw,
                navg=navg,
                power=power,
                nparam=nparam)
        return measurement

    def measure_s21(self) -> SParam1PModel:
        """Measures the transmission via VNA."""
        self.trigger_vna_blocking()
        return self.read_s21()

    def _setup_current_source_range(self, value: float):
        """Sets up the range.
        Todo:
            * Prompt user for abrupt range changes (e.g. from 1 A to 1e-4 A)
        """
        cs = self.cs
        frval, prefix = friendly.mprefix(value)
        logger.info(f"Setting current source range to {frval:.1f}{prefix}A.")
        cs.set_range_i(value)

    def abort(self):
        pass
