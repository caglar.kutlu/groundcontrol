"""Some common error types for controllers."""


class ControllerException(Exception):
    pass


class ConfigurationError(ControllerException):
    pass


class UserAbort(ControllerException):
    pass
