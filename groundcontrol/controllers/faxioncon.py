"""Fake axion controller using E8267D.
"""
from typing import Callable, ClassVar, Optional, NewType
import attr
import json

import numpy.random as nprnd
import numpy.fft as npfft
import numpy as np
from chapp.physics import get_dist_labframe

try:
    from numpy import typing as nptp

    NDArray = nptp.NDArray
    NDComplex128 = nptp.NDArray[np.complex128]
    NDInt16 = nptp.NDArray[np.int16]
    NDFloat64 = nptp.NDArray[np.float64]
except ImportError:
    NDArray = NewType("NDArray", np.ndarray)
    NDComplex128 = NewType("NDComplex128", np.ndarray)
    NDInt16 = NewType("NDInt16", np.ndarray)
    NDFloat64 = NewType("NDFloat64", np.ndarray)

from groundcontrol.logging import setup_logger
from groundcontrol.controller import Controller, instrument
from groundcontrol.instruments import KeysightE8267D


logger = setup_logger("faxioncon")


@attr.frozen
class FaxionBasebandGen:
    """Generates a fake axion baseband signal with the RMS normalized to 1.

    For now the shape is limited to maxwellian boosted to lab-frame.

    Attributes:
        f_a: Axion frequency.
        fcoffset:  Offset from the carrier frequency.
    """

    f_a: float  # Hz
    fcoffset: float  # Hz
    fsample: float
    _distfun: Callable = attr.ib(eq=False)
    _rg: nprnd.Generator = attr.ib(factory=nprnd.default_rng, eq=False)
    _q_a: ClassVar[float] = 1e6  # axion sharpness, internal usage only
    # assume that lineshape covers at most *bw frequency
    _lineshape_occupancy_factor: ClassVar[int] = 20

    def eval_fdist(self, f):
        """Evaluates the frequency distribution at f and returns."""
        return self._distfun(f)

    def mkfreq(self, nsample: int):
        fdom = npfft.fftshift(npfft.fftfreq(nsample, 1 / self.fsample))
        return fdom

    def generate(self, nsample: int) -> NDComplex128:
        """This generates an analytic signal to be used in IQ modulation scheme.  An
        analytic signal does not have a negative frequency component and hence must be
        complex valued in time domain.
        """
        # Define an analytic signal in frequency domain
        fdom = npfft.fftfreq(nsample, 1 / self.fsample)
        nfreq = len(fdom)
        # Sampling in frequency domain
        psd = self._distfun(fdom)
        dfftmag = np.sqrt(psd)
        dfftphase = self._rg.uniform(0, 2 * np.pi, size=nfreq)
        dfft = dfftmag * np.exp(1j * dfftphase)
        iqsig = npfft.ifft(dfft) * nfreq / np.sqrt(np.sum(psd))
        return iqsig  # (i + j*q)

    @classmethod
    def make(cls, f_a: float, fcoffset: float = 0, fsample: Optional[int] = None):
        """Args:
        f_a: Axion frequency.
        fcoffset: Offset of spectrum from 0 Hz.
        fsample: Sampling rate.  Must be an integer, indicating 1 Hz accuracy.  If
            `None`, set equal to fcoffset+15*(f_a/Q_AXION) rounded to nearest higher
            integer.

        """
        lw = f_a / cls._q_a
        lof = cls._lineshape_occupancy_factor
        maxfreq = fcoffset + lof * lw
        nyqrate = 2 * maxfreq
        if fsample is None:
            fsample = np.ceil(nyqrate)
        elif not isinstance(fsample, int):
            raise ValueError("fsample must be int.")
        else:
            fsample = float(fsample)

        if fsample < nyqrate:
            raise ValueError(
                f"fsample must be greater than 2*(fcoffset+{lof}*{lw}).  ({nyqrate})"
            )
        fun = get_dist_labframe(f_a=f_a, loc=fcoffset)
        return cls(f_a, fcoffset, fsample, fun)

    def to_header_desc(self) -> str:
        meta = dict(
            fa=self.f_a, fco=self.fcoffset
        )
        # Change " to ', strip off dictionary tags {}
        headerdesc = (
                json.dumps(meta, separators=(",", ":"))
                    .replace('"', "'")
                    .lstrip('{').rstrip('}'))
        return headerdesc

    @classmethod
    def from_header_desc(cls, headerdesc: str, fsample: Optional[int] = None):
        replaced = headerdesc.replace("'", '"')
        meta = json.loads(f"{{{replaced}}}")
        fa = meta['fa']
        fcoffset = meta['fco']
        return cls.make(fa, fcoffset, fsample)


class FaxionCon(Controller):
    """Fake axion controller class using a Keysight E8267D.
    [1]: Turner M. - Periodic signatures for the detection of cosmis axions

    Attributes:
        sg1:  Signal generator instance.
        scale:  Scaling of the signal, must be a float within [0,1]

    Read-only:
        f_a:  This is the smallest frequency at which the expected axion signal
            spectral distribution has a non-zero value.

    Apart from location, spectral shape also depends on f_a.  For this reason, a change
    in f_a via `set_axion_frequency` will require reloading of waveform to the
    instrument.  In reality, changes in f_a less than 1% will cause less than 1% change
    in spectrum's values.  This fact may be exploited in the future.

    The generated waveforms are currently restricted to a the model where the dark
    matter particles have a velocity distribution following a maxwellian scheme in
    galaxy frame but represented appropriately in the lab frame with a lorentz
    boost([1]).

    """

    sg1: KeysightE8267D = instrument()
    scale: float = attr.ib(default=0.1)
    _f_a: float = 5.9e9
    _pow_err_db: float = 0
    _bb_freq_offset: float = 750e3
    _fbg: FaxionBasebandGen = attr.ib()
    _datafname: str = "gndcntrl_faxion"
    _nsample: int = int(20e6)

    @property
    def f_a(self):
        return self._f_a

    @property
    def pow_err_db(self):
        return self._pow_err_db

    @property
    def nsample(self):
        return self._nsample

    @scale.validator
    def _checkbound_unitinterval(self, attrib, value):
        if (value < 0) or (value > 1):
            raise ValueError(f"{attrib.name} must be within [0, 1].")

    @_fbg.default
    def _fbgdefault(self):
        # baseband offset is applied via SG functionality.
        return FaxionBasebandGen.make(self._f_a, fcoffset=0)

    def _update_f_a(self, f_a: float):
        self._fbg = FaxionBasebandGen.make(f_a)
        self._f_a = f_a

    def __setup__(self):
        sg = self.sg1

        self.deactivate()
        try:
            desc = sg.query_mmem_header_description(self._datafname)
        except UnicodeDecodeError:
            # This happens when there is no header - no previous faxion data
            fbgloaded = self._fbg
            self.update_waveform()
        else:
            fbgloaded = FaxionBasebandGen.from_header_desc(desc)
            if self._fbg != fbgloaded:
                # only upload basebande data
                # if parameters don't match with the desired values
                self.update_waveform()

        sg.set_radio_arb_waveform(self._datafname)
        sg.set_radio_arb_baseband_frequency_offset(self._bb_freq_offset)
        sg.set_radio_arb_filter_state(sg.State.OFF)
        self.set_axion_frequency(self._f_a)

        sg.set_radio_arb_state(sg.State.ON)
        arbsucc = sg.query_radio_arb_state() == sg.State.ON

        sg.set_output_modulation_state(sg.State.ON)
        modsucc = sg.query_output_modulation_state() == sg.State.ON

        success = arbsucc and modsucc
        return success

    def generate_waveform(self) -> NDInt16:
        wfdata = self._fbg.generate(self._nsample)
        sig = (self.scale * wfdata * (2 ** 15)).view(np.float64).astype(np.int16)
        rms = self.scale  # rms from fbg is 1
        return sig, rms

    def upload_waveform(self, waveform: NDInt16, rms: Optional[float] = None):
        sg = self.sg1
        logger.info("Uploading waveform, this may take a while.")
        sg.set_mmem_data(sg.MSUS.WFM1, self._datafname, waveform, datatype="h")
        sg.set_radio_arb_sclock_rate(self._fbg.fsample)
        sg.set_radio_arb_header_rms(sg.MSUS.WFM1, self._datafname, rms)
        sg.set_mmem_header_description(
                self._datafname, self._fbg.to_header_desc())

    def update_waveform(self):
        sig, rms = self.generate_waveform()
        self.upload_waveform(sig, rms)

    def set_power(self, powdbm: float) -> bool:
        """Sets the signal generator power to `powdbm - self.pow_err_db`."""
        powr = powdbm - self.pow_err_db
        self.sg1.set_power(powr)
        powset = self.sg1.query_power()
        return abs(powr - powset) < 1e-2  # usually sg has 0.01 db precision

    def set_axion_frequency(self, f_a: float, update_waveform: bool = False):
        if update_waveform:
            self._update_f_a(f_a)
            self.update_waveform
        freq = f_a - self._bb_freq_offset
        self.sg1.set_frequency(freq)
        fset = self.sg1.query_frequency()
        return abs(fset - freq) < 1e-3  # mHz resolution is probably typical

    def set_nsample(self, nsample: int, noupdate: bool = False):
        self._nsample = nsample
        if not noupdate:
            self.update_waveform()

    def activate(self) -> bool:
        sg = self.sg1
        sg.set_output_state(self.sg1.State.ON)
        outstate = sg.query_output_state() == sg.State.ON
        return outstate

    def deactivate(self) -> bool:
        sg = self.sg1
        sg.set_output_state(self.sg1.State.OFF)
        return sg.query_output_state() == sg.State.OFF

    def acquire_state(self):
        """This acquires the axion waveform state and applies and mutates self
        accordingly. Following attributes are updated [f_a, nsample, scale]
        """
        raise NotImplementedError
