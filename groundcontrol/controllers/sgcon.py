from groundcontrol.controller import Controller, instrument
from groundcontrol.instruments import SCPISignalGenerator


class SGController(Controller):
    """Controller for a signal generator."""
    sg: SCPISignalGenerator = instrument()
    deactivate_on_init: bool = True

    def __setup__(self):
        if self.deactivate_on_init:
            self.deactivate()

    def activate(self):
        self.sg.set_output_state(self.sg.State.ON)

    def deactivate(self):
        self.sg.set_output_state(self.sg.State.OFF)

    def query_isactive(self) -> bool:
        return self.sg.query_output_state() == self.sg.State.ON

    def set_power(self, dbm: float):
        self.sg.set_power(dbm)

    def query_power(self) -> float:
        return self.sg.query_power()

    def set_frequency(self, hertz: float):
        self.sg.set_frequency(hertz)

    def query_frequency(self) -> float:
        return self.sg.query_frequency()
