import typing as t

import attr
import pyvisa as visa

from groundcontrol.instruments import Instrument, VISAInstrument
from groundcontrol import logger


def _issubclass(cls, other):
    """If cls is a union, picks the first type as the cls and checks for
    subclass."""
    try:
        if cls.__origin__ is t.Union:
            cls = cls.__args__[0]
    except AttributeError:
        pass

    return issubclass(cls, other)


class ResourceNotFoundError(Exception):
    pass


@attr.s(auto_attribs=True)
class InstrumentManager:
    visa_rm: visa.ResourceManager
    instruments: t.Dict[str, Instrument] = attr.Factory(dict)
    resource_names: t.Dict[str, str] = attr.Factory(dict)
    open_timeout: float = 1000  # 1000 ms open timeout
    open_retry_count: int = 5

    def reconnect_instrument(self, name: str):
        rscname = self.resource_names[name]
        instrument = self.instruments[name]
        instype = type(instrument)
        self.delete(name)
        # assuming visa instrumnet
        self.create_visa_instrument(name, rscname, instype)

    def get_instrument(
            self,
            name: str,
            resource_name: t.Optional[str] = None,
            instrument_type: t.Optional[Instrument] = None):
        """Returns the instrument referred by the `name` if it exists, creates
        and returns it otherwise.
        """
        try:
            return self.instruments[name]
        except KeyError:
            return self.create_instrument(name, resource_name, instrument_type)

    def create_instrument(
            self,
            name: str,
            resource_name: str,
            instrument_type: Instrument):
        """Resource names can be of different type.  Currently only VISA type
        resource strings are supported.
        """

        if _issubclass(instrument_type, VISAInstrument):
            return self.create_visa_instrument(
                    name, resource_name, instrument_type)
        else:
            return self.create_nonvisa_instrument(
                    name, resource_name, instrument_type)

    def create_nonvisa_instrument(
            self,
            name: str,
            resource_name: str,
            instrument_type: Instrument):
        if name in self.instruments:
            raise ValueError("Pick a unique name for the instrument.")
        logger.debug(f"Creating NON-VISA instrument {name} "
                     f"with resource {resource_name} as {instrument_type}")

        instr = instrument_type.from_visa_resource_name(resource_name, name)
        self.instruments[name] = instr
        self.resource_names[name] = resource_name
        return instr

    def create_visa_instrument(
            self,
            name: str,
            resource_name: str,
            instrument_type: VISAInstrument):
        """Creates a visa instrument exclusive to this session.
        """
        if name in self.instruments:
            raise ValueError("Pick a unique name for the instrument.")
        logger.debug(f"Creating VISA instrument {name} "
                     f"with resource {resource_name} as {instrument_type}")

        try:
            resource = self.visa_rm.open_resource(
                    resource_name,
                    access_mode=visa.constants.AccessModes.exclusive_lock,
                    open_timeout=self.open_timeout)
        except visa.VisaIOError as e:
            # errcode = e.error_code
            abbreviation = e.abbreviation
            desc = e.description
            if abbreviation == "VI_ERROR_RSRC_NFOUND":
                raise ResourceNotFoundError(desc) from e
            else:
                raise e
        except Exception as e:
            msg = e.args[0]
            if 'error_timeout' in msg:
                raise ResourceNotFoundError(msg) from e
            else:
                raise e
            

        instr = instrument_type.from_resource(resource, name)
        self.instruments[name] = instr
        self.resource_names[name] = resource_name

        return instr

    def delete(self, name: str):
        self.instruments[name].close()
        del self.instruments[name]
        del self.resource_names[name]

    def delete_all(self):
        for name in [self.instruments.keys()]:
            self.delete(name)

    @classmethod
    def make(self):
        rm = visa.ResourceManager()
        return InstrumentManager(rm)
