"""Module providing a common setting names for classes of instruments.

Todo:
    * I am using enums for instrument settings from the instrument classes.
    These probably should be moved elsewhere(here?) and imported from there.
"""
import typing as t

import attr

import groundcontrol.units as un
from groundcontrol.units import UnitDefinition
from groundcontrol.instruments import SCPIVectorNetworkAnalyzer as _VNA
from groundcontrol.instruments import SCPISpectrumAnalyzer as _SA
from groundcontrol.instruments import SCPISignalGenerator as _SG
from groundcontrol.instruments import LS372TemperatureController as _TC
from groundcontrol.instruments import TSPCurrentSource as _CS
from groundcontrol.declarators import setting, settings


class _InstrumentSettingsMeta(type):
    def __new__(cls, clsname, supers, attrdict):
        newcls = super().__new__(cls, clsname, supers, attrdict)
        decorated = attr.s(auto_attribs=True)(newcls)

        return decorated


class InstrumentSettings(metaclass=_InstrumentSettingsMeta):
    """Base class to be used when defining instrument settings."""

    def asdict(self) -> t.Dict[str, t.Any]:
        """Returns the settings as a dictionary."""
        return settings(self)

    def evolve(self, **kwargs):
        """Returns a modified copy of this object, changing the attributes
        provided in `kwargs`."""
        return attr.evolve(self, **kwargs)


class VNASettings(InstrumentSettings):
    """Common settings for Vector Network Analyzers.

    Fields can be `None` since the information about the settings may be
    partial at a certain time.

    The inconsistency of the redundant fields such as a combination of start,
    stop, center frequencies should be handled by the users of this class.

    """
    ByteOrderFormat: t.ClassVar = _VNA.ByteOrderFormat
    FormatDataType: t.ClassVar = _VNA.FormatDataType
    State: t.ClassVar = _VNA.State

    byteorder: ByteOrderFormat = setting(default=ByteOrderFormat.LITTLE_ENDIAN)
    dataformat: FormatDataType = setting(default=FormatDataType.REAL64)
    start_frequency: t.Optional[float] = setting(un.hertz, "Start Frequency")
    stop_frequency: t.Optional[float] = setting(un.hertz, "Stop Frequency")
    center_frequency: t.Optional[float] = setting(un.hertz, "Center Frequency")
    span: t.Optional[float] = setting(un.hertz, "Span")
    sweep_step: t.Optional[float] = setting(un.hertz, "Step Size")
    if_bandwidth: t.Optional[float] = setting(un.hertz, "IF Bandwidth")
    naverage: t.Optional[int] = setting(un.nounit, "Average Count")
    power: t.Optional[float] = setting(un.dBm, "Output Power")
    npoints: t.Optional[int] = setting(un.nounit, "Sweep Points")

    def get_start_frequency(self):
        if self.start_frequency is None:
            return self.center_frequency - self.span/2
        else:
            return self.start_frequency

    def get_stop_frequency(self):
        if self.stop_frequency is None:
            return self.center_frequency + self.span/2
        else:
            return self.stop_frequency


class SASettings(InstrumentSettings):
    """Common settings for Spectrum Analyzers.  By convention the settings set
    to `None` will preserve their previous values on the spectrum analyzer.
    With `setting` declarator, the default values are `None` if not set
    explicitly."""
    ByteOrderFormat: t.ClassVar = _SA.ByteOrderFormat
    FormatDataType: t.ClassVar = _SA.FormatDataType
    DetectorType: t.ClassVar = _SA.DetectorType

    byteorder: ByteOrderFormat = setting(default=ByteOrderFormat.LITTLE_ENDIAN)
    dataformat: FormatDataType = setting(default=FormatDataType.REAL32)
    detector: DetectorType = setting(default=DetectorType.RMS)
    start_frequency: t.Optional[float] = setting(un.hertz, "Start Frequency")
    stop_frequency: t.Optional[float] = setting(un.hertz, "Stop Frequency")
    center_frequency: t.Optional[float] = setting(un.hertz, "Center Frequency")
    span: t.Optional[float] = setting(un.hertz, "Span")
    rbw: t.Optional[float] = setting(un.hertz, "Resolution Bandwidth")
    vbw: t.Optional[float] = setting(un.hertz, "Video Bandwidth")
    swt: t.Optional[float] = setting(un.second, "Sweep Time")
    swt_auto: t.Optional[bool] = setting(un.nounit, "Auto Sweep Time")
    npoints: t.Optional[int] = setting(un.nounit, "Sweep Points")
    naverage: t.Optional[int] = setting(un.nounit, "Average Count")


class CSSettings(InstrumentSettings):
    """Common settings for current sources."""
    SourceFunction: t.ClassVar = _CS.SourceFunction

    sourcefunc: SourceFunction = SourceFunction.CURRENT
    level_i: float = setting(un.ampere, "Current Level")
    range_i: float = setting(un.ampere, "Current Range")


class TCSettings(InstrumentSettings):
    """Settings based on LS372 temperature controller.
    The features are not complete.  Some missing stuff are:
        - Heater output settings.
        - PID control related stuff.
        - Etc.

    """

    class ChannelSettings(InstrumentSettings):
        ExcitationFrequency: t.ClassVar = _TC.ExcitationFrequency
        ExcitationMode: t.ClassVar = _TC.ExcitationMode
        VoltageExcitation: t.ClassVar = _TC.VoltageExcitation
        CurrentExcitation: t.ClassVar = _TC.CurrentExcitation
        AutorangeType: t.ClassVar = _TC.AutorangeType
        ResistanceRange: t.ClassVar = _TC.ResistanceRange
        CSShunt: t.ClassVar = _TC.CSShunt
        Units: t.ClassVar = _TC.Units
        TempCo: t.ClassVar = _TC.TempCo

        excitation_mode: ExcitationMode = setting(un.nounit, 'ExcitationMode')
        excitation: t.Union[VoltageExcitation, CurrentExcitation] = setting(
                un.nounit, 'Excitation')
        autorange: AutorangeType = setting(un.nounit, "AutoRange")
        range: ResistanceRange = setting(un.nounit, "ResistanceRange")
        csshunt: CSShunt = setting(un.nounit, "CSShunt")
        units: Units = setting(un.nounit, 'Units')
        enabled: bool = setting(un.nounit, "InputEnabled")
        dwell: float = setting(un.second, "DwellTime")
        pause: float = setting(un.second, "PauseTime")
        curve_number: int = setting(un.nounit, "CurveNumber")
        tempco: TempCo = setting(un.nounit, "TemperatureCoeff")
        filter_enabled: bool = setting(un.nounit, "FilterEnable")
        settle_time: float = setting(un.second, "FilterSettleTime")
        window: float = setting(un.nounit, "FilterWindow")
        excitation_frequency: ExcitationFrequency = setting(
                un.nounit, "ExcitationFrequency")

    channels: t.Dict[str, ChannelSettings] = setting(
            un.nounit, "Channels")

