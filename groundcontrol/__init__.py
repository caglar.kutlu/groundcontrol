# -*- coding: utf-8 -*-
"""groundcontrol

This is a package for controlling instruments and performing experiments.  It
has a high-level interface to instruments and comes with predefined sets of
instruments.
"""

"""This variable determines whether or not to use unicode in any text."""
USE_UNICODE: bool = False
__version__ = "0.0.1"

from pathlib import Path as _Path
import cattr as _cattr

from groundcontrol.logging import logger
from groundcontrol.instrumentmanager import InstrumentManager
from groundcontrol.jpa_tuner import JPATuner, WorkingPoint, TuningPoint, JPATunerOptimizing


# How to ensure that this is executed upon library loading?
_cattr.register_structure_hook(_Path, lambda s, t: _Path(s))
