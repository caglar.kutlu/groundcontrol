"""Module for JPA tuner class."""

import attr
import typing as t
from itertools import count

from scipy.signal import find_peaks, savgol_filter
import numpy as np
from datetime import datetime

from .controllers.jpa import JPAController
from .declarators import setting, quantity, parameter, declarative
from .logging import setup_logger
from .settings import VNASettings
from .measurement import SParam1PModel, MeasurementModel
from . import units as un
from .util import unwrap_deg, makeodd, db2lin_pow, lin2db_pow, find_nearest_idx
from .estimation import resonance_from_phase2 as estimate_resonance
from .pid import PIDTuner, TunerSettings
from .friendly import mprefix_str
from .helper import domaybe, Array


logger = setup_logger("jpa_tuner")


@attr.s(auto_attribs=True)
class WorkingPoint:
    """This is the parameters required to select a certain tuning point of the
    JPA."""
    ib: float = quantity(un.ampere, "Bias Current", True)
    fp: float = quantity(un.hertz, "Pump Frequency", True)
    pp: float = quantity(un.dBm, "Pump Power", True)

    def evolve(self, **kwargs):
        """Returns a copy of the instance with the member variables changed
        with the ones given in `kwargs` argument.

        Example:
            >>> tp = WorkingPoint(0, 0, 0)
            >>> tp
            >>> WorkingPoint(ie=0, fp=0, pp=0)
            >>> tpnew = tp.evolve(ie=3)
            >>> tpnew
            >>> WorkingPoint(ie=3, fp=0, pp=0)
        """
        return attr.evolve(self, **kwargs)


class TuningPoint(MeasurementModel):
    """This contains all information necessary about the tuning point."""
    gain: Array[float] = quantity(un.dB, "Gain Spectrum (Uncal)")
    frequency: Array[float] = quantity(un.hertz, "Frequency Domain")

    offset_gain: float = parameter(un.dB, "Gain at Offset from Peak")
    resonance: float = parameter(un.hertz, "Passive Resonance Frequency")
    peak_frequency: float = parameter(
            un.hertz, "Amplification Peak Frequency",
            description="Peak frequency is determined by half pump "
                        "frequency usually.")
    bias_current: float = parameter(un.ampere, "DC Flux Biasing Current")
    pump_power: float = parameter(un.dBm, "Pump Power")
    pump_frequency: float = parameter(un.hertz, "Pump Frequency")

    offset_frequency: float = parameter(
            un.hertz, "Offset Gain Frequency",
            description="Frequency at which offset gain is measured as an "
                        "offset from the peak frequency")
    signal_power: float = parameter(
            un.dBm, "Signal Power",
            description="Power of the signal when measuring gain")

    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))

    @property
    def working_point(self):
        return WorkingPoint(
                ib=self.bias_current,
                fp=self.pump_frequency,
                pp=self.pump_power)


@attr.s(auto_attribs=True)
class _ResonanceTuner(PIDTuner):
    """Class that implements the resonance tuning logic.

    The form used for PID control is:
    $$
    u_k = Kp(e_k + (1/Ki) \sum_{k'=0}^k {e_{k'}} + K_d(e_k - e_{k-1})) # noqa:  W605
    b_k = b_{k-1} + u_k
    $$
    where b_k is the bias point at k'th iteration.

    In this sense the first difference relation tells us how to update b_k.

    Determining sensible initial Kp:
    1. Assume tuning will be about 1 MHz.
    2. di/df is approx. 1 uA/1MHz.
    3. Choose Kp to have u_k = 1uA for 1MHz tuning.
    4. Since units are A and Hz, Kp=1e-12 satisfy this.
    5. Find K_i and K_d by trial and error.  Start with Ki = 10, Kd=0.

    """
    setpoint: float = parameter(un.hertz, "Frequency Setpoint")
    bias: float = parameter(un.ampere, "Bias Current")

    # Variables
    error: float = quantity(un.hertz, "Frequency Error", True, 0)
    integral: float = quantity(un.hertz, "Error Integral", True, 0)


@attr.s(auto_attribs=True)
class _GainTuner(PIDTuner):
    """Class that implements the gain tuning logic."""
    setpoint: float = parameter(un.dB, "Gain Setpoint")
    bias: float = parameter(un.dBm, "Bias Current")

    # Variables
    error: float = quantity(un.dB, "Gain Error", True, 0)
    integral: float = quantity(un.dB, "Error Integral", True, 0)


@attr.s(auto_attribs=True)
class JPATuner:

    """This class encapsulates all the tuning functionalities required to tune
    the JPA for:

    1. Selecting the passive resonance point.
    2. Adjusting for the desired gain (and bandwidth inevitably).

    """
    controller: JPAController

    # Tuning parameters
    tup: TuningPoint = quantity(un.nounit, "Tuning Point", True)
    wp: WorkingPoint = quantity(un.nounit, "Working Point", True)

    # Estimations
    Tb: SParam1PModel = quantity(un.nounit, "Baseline", True)
    Fr: float = quantity(un.hertz, "Resonance Frequency", True)
    Ta: SParam1PModel = quantity(
            un.nounit, "Amplification Spectrum", True,
            description="Uncalibrated transmission measurement in the "
                        "amplification band.")
    Go: float = quantity(un.dB, "Offset Gain", True)

    # Program Variables
    Df_sign: int = parameter(
            un.nounit,
            "dF/dI Sign",
            default=1,
            description="Sign of the slope of resonance - bias current at the "
                        "current bias")
    fpeak: float = parameter(
            un.hertz, "Gain Peak",
            description="Frequency of the peak point of amplification")

    ####################
    # Program Constants#
    wt: t.Tuple[float, float] = setting(un.hertz, "Tuning Window")
    dfo: float = setting(
            un.hertz, "Frequency Offset",
            description="Frequency offset for gain measurement",
            default=-5e3)

    gain_bootstrap_step: float = setting(
            un.dBm, "Pump Step",
            description="Pump power step used when bootstrapping the gain.",
            default=1)
    gain_bootstrap_threshold: float = setting(
            un.dB, "Gain Threshold",
            description="Gain threshold when bootstrapping the gain.",
            default=10)
    gain_bootstrap_ifbw: float = setting(
            un.hertz, "Bootstrap IFBW",
            description="IFBW used during bootstrapping. "
                        "Choose higher value to speed up gain bootstrapping.",
            default=100)

    zet_max: WorkingPoint = setting(
            un.nounit, "Maximum Values for Tuning",
            default=WorkingPoint(500e-6, 26.5e9, 25))
    zet_min: WorkingPoint = setting(
            un.nounit, "Minimum Values for Tuning",
            default=WorkingPoint(-500e-6, 0, -120))

    restun: TunerSettings = setting(
            un.nounit, "Resonance Tuner Settings",
            default=TunerSettings(
                kp=0.4e-12,
                ki=4,
                kd=0.001,
                ramp_step=0.1e6))

    gaintun: TunerSettings = setting(
            un.nounit, "Gain Tuner Settings",
            default=TunerSettings(
                kp=0.1,
                ki=6,
                kd=0.01,
                ramp_step=2))

    # VNA Settings
    res_settings: VNASettings = setting(
            un.nounit,
            "Resonance Settings",
            description="VNA Settings to use when doing narrow"
                        "passive resonance measurements.")

    baseline_settings: VNASettings = setting(
            un.nounit,
            "Baseline Estimation Settings",
            description="VNA Settings to use when performing baseline "
                        "estimation resonance measurements.")

    offset_settings: VNASettings = setting(
            un.nounit,
            "Offset Settings",
            description="VNA Settings to use when performing gain "
                        "measurement at the offset",
            default=VNASettings(
                span=10, sweep_step=1, power=-30, if_bandwidth=10))

    gain_settings: VNASettings = setting(
            un.nounit,
            "Gain Settings",
            description="VNA Settings to use when performing gain "
                        "measurement covering the whole amplification band.",
            default=VNASettings(
                span=500e3, sweep_step=10e3, power=-30, if_bandwidth=1))

    peakfinder: str = setting(
            un.nounit,
            "Peak Finder",
            description="Peak finding algorithm to use in resonance estimation."
                        "One of ['prominence', 'argmax'].",
            default='prominence')

    bw_expected: float = setting(
            un.hertz,
            "Expected BW",
            description="Expected passive bandwidth.")

    bootstrapped: bool = False

    @property
    def working_point(self):
        return self.wp

    def initiate(self, bootstrap: bool = False):
        if bootstrap:
            self.bootstrap()
        if self.bw_expected is None:
            self.bw_expected = self.res_settings.span/5

    def measure_transmission(self) -> SParam1PModel:
        c = self.controller
        c.activate_signal()
        c.trigger_network_analyzer()
        s21 = c.read_s21()
        return s21

    def measure_baseline(
            self,
            power: t.Optional[float] = None,
            bias_current: t.Optional[float] = None,
            preservesettings: bool = False) -> SParam1PModel:
        """Measures baseline with the baseline_settings for VNA applied.
        If `bias_current` is provided, the coil is ramped to that current
        before baseline is measured.

        Args:
            power:  Overrides the power setting in baseline_settings.
        """
        c = self.controller
        if preservesettings:
            vnasettings = c.query_vna_settings()
        if power is not None:
            settings = self.baseline_settings.evolve(power=power)
        else:
            settings = self.baseline_settings

        if bias_current is not None:
            logger.info("Setting current for baseline measurement.")
            current = c.cs.query_level_i()
            c.ramp_current(bias_current)

        c.do_vna_preset(settings)
        s21 = self.measure_transmission()

        if bias_current is not None:
            logger.info("Ramping current back to initial value.")
            c.ramp_current(current)

        if preservesettings:
            c.do_vna_preset(vnasettings)
        return s21

    def measure_resonance(
            self,
            fhint: t.Optional[float] = None,
            smoothing_band: t.Optional[float] = None) -> float:
        """Measures the resonance applying res_settings to the VNA before.  If
        no frequency hint provided, previous resonance measurement will be
        used.

        Args:
            fhint:  Frequency to center the VNA on.
            smoothing_band:  This determines the window length of the 3rd order
                savitzky-golay filter used for resonance estimation.  If not
                provided, the `self.res_settings.span/5` will be used.

        """
        c = self.controller
        fhint = fhint if fhint is not None else self.Fr
        c.do_vna_preset(self.res_settings.evolve(center_frequency=fhint))
        # Setting display
        c.vna.set_calculate_format(c.vna.DisplayFormat.UPHASE)
        trans = self.measure_transmission()

        if smoothing_band is None:
            smoothing_band = self.res_settings.span/5

        fres = self.estimate_resonance(trans, smoothing_band)
        return fres

    def measure_resonance_spectrum(
            self, fhint: t.Optional[float] = None) -> SParam1PModel:
        """Measures the resonance spectrum applying res_settings to the VNA
        before.  If no frequency hint provided, previous resonance measurement
        will be used.
        """
        c = self.controller
        fhint = fhint if fhint is not None else self.Fr
        c.do_vna_preset(self.res_settings.evolve(center_frequency=fhint))
        # Setting display
        c.vna.set_calculate_format(c.vna.DisplayFormat.UPHASE)
        trans = self.measure_transmission()
        return trans

    def estimate_resonance(
            self,
            measurement: SParam1PModel,
            bw_expected: t.Optional[float] = None
            ) -> t.Optional[float]:
        """Given transmission measurement, estimates the resonance.

        Args:
            measurement:  SParam1PModel instance.
            bw_expected:  Expected bandwidth from the measurement.  If not
                provided, the `res_settings.span/5` was used as reference.

        Returns:
            fres:  Resonance frequency if found, else `None`.
        """

        if bw_expected is None:
            bw_expected = self.bw_expected

        fres = estimate_resonance(measurement, bw_expected)
        return fres

    def estimate_resonance_DEPRECATE(
            self,
            measurement: SParam1PModel,
            smoothing_band: t.Optional[float] = None,
            peakfinder: t.Optional[str] = None) -> float:
        """Estimates the resonance using the phase information.
        Args:
            measurement:  The S-parameter measurement to use when estimating
                resonance.
            smoothing_band:  This determines the window length of the 3rd order
                savitzky-golay filter used for resonance estimation.
            peakfinder:  Can be one of ['prominence', 'argmax'].  'argmax' is
                the simplest one, peakfinder uses scipy's peak finding algorithm.
                Currently 'prominence' is a bit unstable.  Defaults to `None`,
                uses the self.peakfinder member.

        """
        if smoothing_band is None:
            smoothing_band = self.res_settings.span/5

        # unwrapped phases to see the change clearly
        phases = unwrap_deg(measurement.phases)
        f = measurement.frequencies

        df = f[1] - f[0]

        # Interpolation Window as Band
        window_length = makeodd(smoothing_band/df)

        # Prefilter for improvement!
        logger.debug("Magic numbers in resonance estimation!")
        order = 3
        try:
            filtered = savgol_filter(
                    phases, window_length, order, mode='nearest')
        except ValueError as e:
            msg = ("SG filter error: "
                   f"savgol_filter(phases, {window_length}, {order}, "
                   "'nearest'). "
                   f"'{e.args[0]}'")
            logger.error(msg)
            return None

        try:
            dph = -np.diff(filtered)/df
            dph_filtered = savgol_filter(
                    dph, window_length, order,
                    mode='mirror')
        except ValueError as e:
            logger.error(
                    "SG filter error: "
                    f"savgol_filter(dph, {window_length}, {order}, 'mirror'). "
                    f"'{e.args[0]}'")
            return None

        if self.peakfinder == 'argmax':
            peakind = np.argmax(dph_filtered)
            f_res = f[peakind]
        else:
            peaks, props = find_peaks(dph_filtered, prominence=1e-4)

            if len(peaks) != 1:
                return None

            peakind = peaks[0]
            # this can also be used as resonance
            fpeak = f[peakind]
            prominence = props['prominences'][0]
            leftbaseidx = props['left_bases'][0]
            rightbaseidx = props['right_bases'][0]

            # The region where the peak is
            peakregion = dph_filtered[leftbaseidx:rightbaseidx+1]

            # Finding the kind-of-fwhm indices
            band = np.where(peakregion - prominence/2 > 0)[0]
            leftidx, rightidx = band[0], band[-1]

            f_left, f_right = f[leftbaseidx + leftidx], f[leftbaseidx + rightidx]

            # estimating the resonance as the arithmetic mean
            f_res = (f_right + f_left)/2

        return f_res

    def tune_resonance(
            self,
            setpoint: float,
            maxerror: float = 100e3,
            maxiter: int = 1000) -> bool:
        """Tunes the passive resonance point.

        Args:
            setpoint:  Target frequency in hertz.
            maxerror:  Maximum error needed to be reached for the tuner to
                settle.
                Error is reported in Hertz.
            maxiter:  Maximum iteration allowed to reach the setpoint.
        """
        c = self.controller
        c.deactivate_pump()
        c.activate_signal()

        if setpoint < self.wt[0] or setpoint > self.wt[1]:
            logger.warning("Resonance setpoint out of range.")
            return None

        ibmax = self.zet_max.ib
        ibmin = self.zet_min.ib

        ib = c.cs.query_level_i()
        rt = _ResonanceTuner.from_settings(
                self.restun.evolve(
                    kp=self.restun.kp*self.Df_sign,
                    setpoint=setpoint))

        c.do_vna_preset(self.res_settings.evolve(center_frequency=self.Fr))
        c.vna.set_calculate_format(c.vna.DisplayFormat.UPHASE)
        success = False

        for niter in count():
            if niter >= maxiter:
                logger.info(
                        "Maximum iteration count reached without "
                        "reaching to setpoint.")
                return None
            tr = self.measure_transmission()
            c.do_vna_autoscale()
            resonance = self.estimate_resonance(tr)
            if resonance is None:
                # Try again if resonance estimation is inconclusive
                continue

            logger.info(f"Current resonance: {resonance/1e9:.6f} GHz")
            self.controller.do_vna_preset(
                    self.res_settings.evolve(center_frequency=resonance))

            # SUCCESS
            error = setpoint - resonance
            logger.info(f"Current error: {error/1e6:.3f} MHz")
            if np.abs(error) <= maxerror:
                success = True
                break

            dib = rt.step(resonance)
            ibnew = ib + dib
            if (ibnew >= ibmax) or (ibnew <= ibmin):
                _s1, _s2, _s3 = map(
                        lambda v: mprefix_str(v, un.ampere),
                        (ibnew, ibmin, ibmax))
                logger.info(f"For suggested bias current({_s1}) tuning is "
                            f"out of range ({_s2}, {_s3}), aborting.")
                break
            _s = mprefix_str(dib, un.ampere)
            logger.info(f"Applying bias current change: {_s} A")
            c.ramp_current(ibnew)

        if success:
            self.Fr = resonance
            self._update_tuning_point()
        return success

    def tune_gain(
            self,
            setpoint: float,
            fpeak: t.Optional[float] = None,
            maxerror: float = 0.1,
            maxiter: int = 100,
            skipfinal: bool = False):
        """Tunes the gain at the offset point to the desired level.

        Args:
            setpoint:  Target offset gain in dB.
            fpeak:  Peak frequency of amplification.
            maxerror:  Maximum error needed to be reached for the tuner to
                settle.  Error is reported in dB.
            maxiter:  Maximum iteration allowed to reach the setpoint.
            skipfinal:  Skips the last gain measurement performed in a wider
                band.
        """
        c = self.controller
        c.activate_pump()
        c.activate_signal()

        # If gain peak is not given
        if fpeak is None:
            # if previous value exist, use it, else use the resonance.
            fpeak = self.Fr if self.fpeak is None else self.fpeak

        self.fpeak = fpeak

        ppmax = self.zet_max.pp
        ppmin = self.zet_min.pp

        pp = c.sg1.query_power()

        gt = _GainTuner.from_settings(
                self.gaintun.evolve(setpoint=setpoint))

        logger.info("Gain tuner started with settings: {gt}")

        success = False

        for niter in count():
            pp = c.sg1.query_power()

            if niter >= maxiter:
                logger.info(
                        "Maximum iteration count reached without "
                        "reaching to setpoint.")
                return False
            gain = self.measure_offset_gain()

            logger.info(f"Current gain: {gain:.2f} dB")
            self.Go = gain

            error = setpoint - gain
            logger.info(f"Current error: {error:.2f} dB")
            if np.abs(error) <= maxerror:
                success = True
                break

            dpp = gt.step(gain)
            ppnew = pp + dpp
            if (ppnew >= ppmax) or (ppnew <= ppmin):
                logger.info(f"Suggested pump power ({ppnew} dB) tuning is "
                            f"out of range ({ppmin},{ppmax} dB), aborting.")
                break
            logger.info(f"Applying pump power change: {dpp:.2f} dB")
            c.ramp_pump_power(ppnew)

        if not skipfinal:
            self.Ta = self.measure_gain()
        else:
            self.Ta = None

        if success:
            self._update_working_point()
            self._update_tuning_point()
        return success

    def tune_absolute(
            self,
            gain: float,
            fres: t.Optional[float] = None,
            fpeak_offset: float = 0,
            res_maxerr: float = 50e3,
            gain_maxerr: float = 0.1,
            maxiter: int = 1000,
            skipfinal: bool = False,
            policy="resonance") -> bool:
        """Tunes to the given parameters without trying to be clever about it.

        Important:
            One important thing to consider is that the constant gain curve in
            pump power, resonance frequency phase space will have decreasing
            pump powers with decreasing frequency.  Since we measure gain at a
            small band, higher peak gain may reduce the offset gain simply
            because gain*bandwidth remains somewhat constant.  Thus,
            bootstrapping gain to a lower pump powers is used to mitigate this
            issue in a straightforward manner.

        Args:
            gain:  Target gain (AT OFFSET FROM FPEAK) in dBm.
            fres:  Target resonance frequency in Hz.  If `None`, it's not tuned
                again.
            fpeak_offset:  Amplification peak frequency as offset from the
                resonance frequency in Hz.
            res_maxerr:  Maximum allowed absolute error for resonance tuning.
            gain_maxerr:  Maximum allowed absolute error for gain tuning.
            maxiter:  Maximum iteration for both tuners.
            skipfinal:  Skips the final gain measurement.
            policy:  Defines the policy of the tuning.  If set to "resonance",
                than the resonance found that is within tlerance used to define
                center from which fpeak_offset was added to decide on gain peak
                frequency.  If set to "fpeak" than the gain is set to `fres`
                directly after finding resonance within the given error.
        """

        if fres is not None:
            ressuccess = self.tune_resonance(
                    fres, maxerror=res_maxerr, maxiter=maxiter)

            if policy == 'fpeak':
                fpeak = fres + fpeak_offset
            else:
                fpeak = self.Fr + fpeak_offset
        else:
            ressuccess = True
            fpeak = self.Fr + fpeak_offset

        if not ressuccess:
            logger.info("Resonance tuning failed, aborting.")
            return False

        # This is to avoid stucking, can be done in a more clever way.
        bgsuccess = self.bootstrap_gain(
                fpeak=fpeak,
                threshold_db=self.gain_bootstrap_threshold,
                step_db=self.gain_bootstrap_step)

        if not bgsuccess:
            logger.info("Gain bootstrapping failed, aborting.")
            return False

        # Setting peak frequency
        gainsuccess = self.tune_gain(
                gain, fpeak=fpeak, maxerror=gain_maxerr, maxiter=maxiter,
                skipfinal=skipfinal)
        if not gainsuccess:
            logger.info("Gain tuning failed, aborting.")
            return False

        return True

    def set_peak_frequency(self, fpeak: float) -> float:
        """Sets the gain peak using the half pump frequency convention.  This
        method changes the `fpeak` member variable.

        Returns:
            fpeak:  Peak frequency setting.
        """
        logger.info(f"Setting peak frequency to: {fpeak/1e9:.6f} GHz")
        fpump = fpeak*2
        c = self.controller
        c.set_pump_frequency(fpump)
        self.fpeak = fpeak
        return fpeak

    def bootstrap_resonance(
            self,
            usebaseline: bool = False,
            assumeresonance: bool = False,
            fhint: t.Optional[float] = None) -> bool:
        """Used to bootstrap the resonance point either by measuring wide range.

        At the end of a successful resonance bootstrapping, the following
        member variables should be set:
            - Tb:  (T)ransmission measurement of (b)aseline.
            - Fr:  (F)requency of (r)esonance

        Args:
            usebaseline:  Uses the last baseline measurement.
            assumeresonance:  Assumes the VNA is already centered on the
                resonance.

        Returns:
            issuccess:  True if bootstrap procedure is successful.

        """
        self.controller.deactivate_pump()
        self.controller.activate_signal()
        success = False

        if assumeresonance:
            # Storing settings to use later.
            settings = self.controller.read_vna_settings()
            centerf = settings.center_frequency
            fhint = centerf
        elif fhint is None:
            if not usebaseline:
                # We need to use resonance settings in order for the JPA to be
                # in linear regime.
                self.Tb = self.measure_baseline(
                        self.res_settings.power)

            # rough estimation from wideband measurement
            fhint = self.estimate_resonance(self.Tb)

        f0 = self.measure_resonance(fhint)
        if f0 is None:
            logger.info("Could not find resonance.")
        else:
            success = True
            logger.info(f"Resonance found at: {f0/1e9:.6f} GHz")
        self.Fr = f0

        return success

    def bootstrap_gain(
            self,
            threshold_db: t.Optional[float] = None,
            step_db: t.Optional[float] = None,
            ppinit_dbm: t.Optional[float] = None,
            fpeak: t.Optional[float] = None,
            itermax: int = 10000
            ) -> bool:
        """This function sets the pump to twice the passive resonance, and
        increases gain until there is at least `threshold` amount of gain.

        At the end of a successful gain bootstrapping, the following
        member variables should be set:
            - Go:  Gain at offset.
            - fpeak:  Peak frequency for the gain.

        Args:
            threshold_db:  Threshold of gain at which bootstraps will be
                claimed successful.
            step_db:  Steps to increment pump power in dB.  If `None` the
                controller's preferred step is used.
            ppinit_dbm:  Starts ramping up the pump at this power level.  If
                not provided or `None` the bootstrapping will start at minimum
                tuning pump power.
            fpeak:  Peak frequency for the gain.  This determines the pump
                frequency.  If not set, passive resonance frequency is used.
            itermax:  Maximum iteration during bootstrapping.  Just to prevent
                blocking forever.

        Returns:
            issuccess: Returns `True` if successfully bootstraps.

        """
        logger.info("Gain bootstrapping began.")

        c = self.controller
        c.activate_pump()
        c.activate_signal()

        # Changing IFBW of vna to make faster measurements
        oldoffsettings = self.offset_settings
        self.offset_settings = oldoffsettings.evolve(
                if_bandwidth=self.gain_bootstrap_ifbw)

        if fpeak is None:
            fpeak = self.Fr

        if threshold_db is None:
            threshold_db = self.gain_bootstrap_threshold

        if self.Tb is None:
            logger.info("Baseline data was not found. "
                        "Doing a baseline measurement.")
            self.Tb = self.measure_baseline()

        # Set the peak frequency to resonance, i.e. pump to twice resonance.
        self.set_peak_frequency(fpeak)

        # storing this for reapplying later
        cstep = c.pump_step
        step = self.gain_bootstrap_step if step_db is None else step_db
        c.pump_step = step

        # Setting display
        c.vna.set_calculate_format(c.vna.DisplayFormat.MLOGARITHMIC)

        ppmax = self.zet_max.pp
        ppmin = self.zet_min.pp

        pp = ppmin if ppinit_dbm is None else ppinit_dbm
        logger.info(f"Initial pump ramping to {ppmin:.2f} dBm")
        c.ramp_pump_power(pp)

        gain = self.measure_offset_gain()
        c.do_vna_autoscale()

        logger.info("Bootstrapping parameters:\n"
                    f"\tthreshold={threshold_db:.2f}dB\n"
                    f"\tstep={step:.2f}dB\n"
                    f"\tinitial={pp:.2f}dBm")
        logger.info(f"Gain is measured at offset: {self.dfo:.0f} Hz")

        success = False

        msg = ""
        counter = count()
        while next(counter) < itermax:
            if gain >= threshold_db:
                success = True
                msg = f"Gain bootstrap successful: Offset Gain: {gain:.2f} dB"
                self.Go = gain
                break
            ppump = c.increment_pump()
            if ppump == pp:
                # The instrument can't increase pump further
                msg = ("Pump power went outside the instrument range, "
                       "gain bootstrap failed.")
                success = False
                break
            if ppump > ppmax:
                msg = ("Pump power went outside the tuning range, "
                       "gain bootstrap failed.")
                success = False
                break
            pp = ppump
            gain = self.measure_offset_gain()
            logger.info(f"Pump: {ppump:.2f} dBm, Offset Gain: {gain:.1f} dB")
            c.do_vna_autoscale()

        if not success and msg == "":
            msg = ("Bootstrap failed, iteration overflow.")

        logger.info(msg)

        # Restoring original controller settings.
        c.pump_step = cstep
        self.offset_settings = oldoffsettings

        return success

    def bootstrap(
            self,
            usebaseline: bool = False,
            assumeresonance: bool = False,
            threshold_db: t.Optional[float] = None,
            step_db: t.Optional[float] = None,
            fpeak: t.Optional[float] = None,
            ppinit_dbm: t.Optional[float] = None,
            itermax: int = 10000,
            skipgain: bool = False) -> bool:
        """Bootstraps the initial tuning point.

        Args:
            usebaseline:  Uses the last baseline measurement.
            assumeresonance:  Assumes the VNA is already centered on the
                resonance.
            threshold_db:  Threshold of gain at which bootstraps will be
                claimed successful.
            step_db:  Steps to increment pump power in dB.  If `None` the
                controller's preferred step is used.
            fpeak:  Peak frequency for the gain.  This determines the pump
                frequency.  If not set, resonance frequency is used.
            ppinit_dbm:  Starts ramping up the pump at this power level.  If
                not provided or `None` the bootstrapping will start at minimum
                tuning pump power.
            itermax:  Maximum iteration during bootstrapping.  Just to prevent
                blocking forever.

        Returns:
            issuccess:  True if bootstrap procedure is successful.
        """
        self._update_working_point()

        resboot_success = self.bootstrap_resonance(
                usebaseline, assumeresonance)
        if not resboot_success:
            logger.info("Resonance bootstrapping failed, aborting.")
            return False

        gainboot_success = self.bootstrap_gain(
                threshold_db=threshold_db,
                step_db=step_db,
                fpeak=fpeak,
                ppinit_dbm=ppinit_dbm,
                itermax=itermax)

        if not gainboot_success:
            logger.info("Gain bootstrapping failed, aborting.")
            return False

        self._update_tuning_point()
        self._update_working_point()
        return True

    def _get_working_point(self) -> WorkingPoint:
        """Gathers the working point information and returns it."""
        c = self.controller
        ib = c.cs.query_level_i()
        pp = c.sg1.query_power()
        fp = c.sg1.query_frequency()
        wp = WorkingPoint(ib, fp, pp)
        return wp
    
    def _update_working_point(self):
        self.wp = self.query_working_point()

    def query_working_point(self) -> WorkingPoint:
        return self._get_working_point()

    def _get_tuning_point(self) -> TuningPoint:
        """Gathers the tuning point information and returns it."""
        wp = self._get_working_point()
        ampmeas = self.Ta

        if self.fpeak is None:
            fpeak = self.controller.query_peak_frequency()
        else:
            fpeak = self.fpeak

        if self.Fr is None:
            fr = fpeak
        else:
            fr = self.Fr

        if self.Go is None:
            Go = self.measure_offset_gain(fpeak)
        else:
            Go = self.Go

        if ampmeas is None:
            # If no amp. band gain measurement, use offset gain
            frequency = fpeak + self.dfo

            gain = Go
            signal_power = self.offset_settings.power
            if signal_power is None:
                # This may not correspond to the last gain measurement
                # However, good for last resort.
                self.controller.vna.query_source_power_level()
        else:
            frequency = ampmeas.frequencies
            gain = ampmeas.mags
            signal_power = ampmeas.power

        tp = TuningPoint(
                gain=gain,
                frequency=frequency,
                offset_gain=Go,
                resonance=fr,
                peak_frequency=fpeak,
                bias_current=wp.ib,
                pump_power=wp.pp,
                pump_frequency=wp.fp,
                offset_frequency=self.dfo,
                signal_power=signal_power)
        return tp

    def _update_tuning_point(self):
        # Update tuning point information
        tup = self._get_tuning_point()
        self.Go = tup.offset_gain
        self.Fr = tup.resonance
        self.fpeak = tup.peak_frequency

        self.tup = tup

    def update_tuning_point(self):
        """Queries instruments to compile the current tuning point information
        and updates internal state of this object."""
        self._update_tuning_point()

    def measure_offset_gain(self, fpeak: t.Optional[float] = None) -> float:
        """Measures the gain at the designated offset from half-pump
        frequency.  The gain is estimated in dB substracting the baseline
        stored in the `Tb` variable."""
        c = self.controller
        c.activate_pump()

        fpeak = self.fpeak if fpeak is None else fpeak
        foff = fpeak + self.dfo
        c.do_vna_preset(self.offset_settings.evolve(
            center_frequency=foff))
        s21 = self.measure_transmission()
        s21mean = lin2db_pow(db2lin_pow(s21.mags).mean())
        base = self.Tb.mags[find_nearest_idx(self.Tb.frequencies, foff)]
        gain = s21mean - base
        return gain

    def measure_gain(
            self,
            calibrate: bool = False) -> SParam1PModel:
        """Measures the gain spectrum in the amplification band.

        Args:
            calibrate:  Applies calibration using last baseline measurement if
                it exists.
            power:  Signal power to use when measuring gain.
        """
        c = self.controller
        if calibrate:
            raise NotImplementedError

        c.do_vna_preset(
                self.gain_settings.evolve(
                    center_frequency=self.fpeak))
        c.vna.set_calculate_format(c.vna.DisplayFormat.MLOGARITHMIC)
        tr = self.measure_transmission()
        c.do_vna_autoscale()
        return tr

    def set_wp(self, wp: WorkingPoint):
        """Sets the working point."""
        ib, pp, fp = wp.ib, wp.pp, wp.fp
        c = self.controller

        domaybe(c.set_pump_frequency, fp)
        domaybe(c.ramp_pump_power, pp)
        domaybe(c.ramp_current, ib)

    set_working_point = set_wp

    def move_to(
            self,
            tup: TuningPoint):
        """Moves to tuning point, remeasures gain."""
        wp = tup.working_point
        self.set_wp(wp)
        self.Fr = tup.resonance
        self.set_peak_frequency(tup.peak_frequency)
        self.Go = self.measure_offset_gain()
        self.Ta = self.measure_gain()
        self._update_working_point()
        self._update_tuning_point()
        return self.tup

    @classmethod
    def make(
            cls,
            controller: JPAController,
            **kwargs):
        """Creates a JPATuner instance.

        Args:
            controller:  Instance of a JPAController to use for control.
            **kwargs:  key-value pairs for setting additional attributes of the
                class.
        """
        c = controller
        this = cls(c, **kwargs)
        if this.wt is None:
            fstart = this.baseline_settings.start_frequency
            fend = this.baseline_settings.stop_frequency
        else:
            fstart, fend = this.wt

        this.zet_min.fp = fstart
        this.zet_max.fp = fend

        return this


@declarative
class JPATunerOptimizing(JPATuner):
    """This is a less absolute tuner than the original one."""

    scurrent_max: float = setting(
            un.ampere, "CurrentDelta",
            "Max current delta to apply when searching for gain.")
    scurrent_step: float = setting(
            un.ampere, "CurrentStep",
            "Current step when searching gain.")

    def bootstrap_gain(
            self,
            threshold_db: t.Optional[float] = None,
            step_db: t.Optional[float] = None,
            ppinit_dbm: t.Optional[float] = None,
            fpeak: t.Optional[float] = None,
            itermax: int = 10000,
            ) -> bool:
        logger.info("Gain bootstrapping began.")

        c = self.controller
        c.activate_pump()
        c.activate_signal()

        c = self.controller
        ibinit = self.working_point.ib
        ibmin = ibinit - self.scurrent_max
        ibmax = ibinit + self.scurrent_max
        ibstep = self.scurrent_step
        ibs = np.union1d(
                np.arange(ibinit, ibmin, -ibstep),
                np.arange(ibinit, ibmax, ibstep))

        success = False
        for ib in ibs:
            c.ramp_current(ib)
            boottostrappo = super().bootstrap_gain(
                    threshold_db=threshold_db,
                    step_db=step_db,
                    itermax=100)
            if boottostrappo:
                success = True
                ib1 = ib
                break

        if not success:
            c.ramp_current(ibinit)
            return False

        # reposition cursor
        ibmin = ib1 - self.scurrent_max
        ibmax = ib1 + self.scurrent_max
        ibstep = self.scurrent_step
        ibs = np.union1d(
                np.arange(ib1, ibmin, -ibstep),
                np.arange(ib1, ibmax, ibstep))

        gs = []
        for ib in ibs:
            c.ramp_current(ib)
            gs.append(self.measure_offset_gain(fpeak))

        indmax = np.argmax(gs)
        iboptimum = ibs[indmax]
        c.ramp_current(ib)
        logger.info(f"Optimum bias delta: {iboptimum-ibinit:.3e} A")
        self.Go = gs[indmax]
        self.update_tuning_point()

        return success
