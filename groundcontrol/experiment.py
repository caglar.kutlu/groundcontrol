"""Module defining the experiment class.

An ExperimentModel is a sequence of MeasurementModel's.


DO NOT USE THIS!

"""
import typing as t

import attr

from .measurement import MeasurementModel, parameter
from .logging import logger


logger.warning("DO NOT USE THIS MODULE!")


IndexType = t.Hashable


@attr.s(auto_attribs=True)
class ExperimentModel:
    """ExperimentModel provides a base class to model the data of an experiment.

    This model is mutable and intended to be filled as an experiment
    progresses.

    An `ExperimentModel` instance can contain attributes created with
    `parameter` and `quantity` functions just like `MeasurementModel`.

    Attributes:
        indexprovider:  Must be a generator that returns same-type index
            objects when iterated.
        params:  An object holding the experiment parameters.

    """
    _measurements: t.Mapping[IndexType, MeasurementModel]
    indexprovider: t.Generator[IndexType, None, None]

    @property
    def index(self):
        """Return an iterable of indices."""
        return self._measurements.keys()

    def add(self, measurement: MeasurementModel):
        ind = next(self.indexprovider)
        self._measurements[ind] = measurement

    def at(self, index):
        return self._measurements[index]

    def save(self):
        pass
