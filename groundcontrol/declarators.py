"""This module is for exposing various declarator type functions as an API.

The term `declarator` within the context of this library is a function which
sets up a metadata for an attribute of a class.  Referring to the `attrs`
library.

A simple example is the default attr.ib() declarator:
    >>> import attr
    >>>
    >>> @attr.s
    >>> class Foo:
    >>>     x = attr.ib(type=int)

Here `Foo` is an `attrs` decorated class, and `attr.ib` is a declarator that
declares the type of variable.  In `groundcontrol` currently there are
declarators for:
    - Settings
    - Measurement quantity
    - Measurement parameter
    - Instruments

Essentially, the only purpose of a declarator is to seamlessly declare static
properties of a member variable without resorting to writing different types.
This hopefully leads to a more managable codebase when one wants to add extra
functionality.

"""
import typing as t
from functools import partial
import json
from pathlib import Path
from io import StringIO

import cattr
import attr

from groundcontrol.units import UnitDefinition
from groundcontrol.helper import default_serializer
from groundcontrol import units as un
from groundcontrol.measurement import parameter, quantity, \
        parameter_fields, quantity_fields, parameters, quantities
from groundcontrol.controller import instrument, instruments, instrument_fields
from groundcontrol.helper import default_serializer


declarative = partial(attr.s, auto_attribs=True)
"""Decorator to use for making classes declarator compatible."""


__all__ = ['setting', 'parameter', 'quantity', 'instrument']


field = attr.ib


def setting(
        unit: UnitDefinition = un.nounit,
        nicename: t.Optional[str] = None,
        description: t.Optional[str] = None,
        default: t.Optional[t.Any] = None,
        isoptional: bool = True,
        converter: t.Optional[t.Callable] = None,
        validator: t.Optional[t.Callable] = None):
    """Returns a setting attribute field to be used in InstrumentSettings
    derived classes.

    Args:
        unit:  Unit of the setting.
        nicename: A unique nice name for referring to the
            setting in texts.
        description: A detailed description of the setting.
        default: The default value for the setting.
        isoptional: If this is `False`, the value given in `default` will be
            ignored and the value must be provided when instantiating the class
            that has this descriptor.  Defaults to `True`.
    """

    metadata = dict(
            metatype='setting',
            unit=unit,
            nicename=nicename,
            description=description,
            converter=converter,
            validator=validator
            )
    if isoptional:
        return attr.ib(metadata=metadata, default=default)
    else:
        return attr.ib(metadata=metadata)


def _issettingfield(attribute, value):
    return attribute.metadata['metatype'] == 'setting'


def settings(obj) -> t.Dict[str, t.Any]:
    """Given an instance of Controller subtype, returns the settings as a
    dictionary."""
    return attr.asdict(obj, filter=_issettingfield)


def metas(obj) -> t.Dict[str, t.Dict[str, t.Any]]:
    attrdct = attr.fields_dict(type(obj))
    out = {}
    for k, v in attrdct.items():
        metadata = v.metadata
        metadata['type'] = v.type
        out[k] = metadata
    return out


def to_json(obj, fp=None, pretty=True):
    """Converts `obj` to json and returns it as string.
    Args:
        obj:  Object to be converted.
        fp:  File pointer or a Path object.
        pretty:  If `True` pretty prints the json with indentation of 4 spaces.

    """
    if pretty:
        indent = 4
    else:
        indent = None

    unsdict = cattr.unstructure(obj)
    # hack
    unsdict = {k: default_serializer(v, False) for k, v in unsdict.items()}

    if isinstance(fp, Path) or isinstance(fp, str):
        with open(fp, 'w') as fl:
            json.dump(unsdict, fl, indent=indent)
    elif fp is None:
        return json.dumps(unsdict, indent=indent)
    else:
        json.dump(unsdict, fp, indent=indent)


def from_json(fp, typ):
    if isinstance(fp, Path) or isinstance(fp, str):
        with open(fp, 'r') as fl:
            unsd = json.load(fl)
    else:
        unsd = json.load(fp)

    return cattr.structure(unsd, typ)
