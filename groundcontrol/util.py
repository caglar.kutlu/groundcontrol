"""Some utilitary mathematical functions.

Notes
-----
- The dB scales produced from functions here are consistent whether they are
  computed from linear (voltage) or quadratic (power) ratios.
"""

import typing as t
import sys
from numbers import Number
from functools import partial as _partial
from functools import update_wrapper
from datetime import datetime, timedelta, timezone

import numpy as np


_Array = np.ndarray

UNumeric = t.Union[Number, _Array]
""" type:  An alias for the types that ufuncs can operate on.

Technical Gotcha
----------------
The typing attempted in this module is not robust.  The return types actually
depend on the argument types so that genericity should be achieved using
TypeVar's rather than aliases.

"""


# TAKEN FROM https://github.com/python/cpython/blob/3.8/Lib/datetime.py
def _parse_isoformat_date(dtstr):
    # It is assumed that this function will only be called with a
    # string of length exactly 10, and (though this is not used) ASCII-only
    year = int(dtstr[0:4])
    if dtstr[4] != '-':
        raise ValueError('Invalid date separator: %s' % dtstr[4])

    month = int(dtstr[5:7])

    if dtstr[7] != '-':
        raise ValueError('Invalid date separator')

    day = int(dtstr[8:10])

    return [year, month, day]


def _parse_hh_mm_ss_ff(tstr):
    # Parses things of the form HH[:MM[:SS[.fff[fff]]]]
    len_str = len(tstr)

    time_comps = [0, 0, 0, 0]
    pos = 0
    for comp in range(0, 3):
        if (len_str - pos) < 2:
            raise ValueError('Incomplete time component')

        time_comps[comp] = int(tstr[pos:pos+2])

        pos += 2
        next_char = tstr[pos:pos+1]

        if not next_char or comp >= 2:
            break

        if next_char != ':':
            raise ValueError('Invalid time separator: %c' % next_char)

        pos += 1

    if pos < len_str:
        if tstr[pos] != '.':
            raise ValueError('Invalid microsecond component')
        else:
            pos += 1

            len_remainder = len_str - pos
            if len_remainder not in (3, 6):
                raise ValueError('Invalid microsecond component')

            time_comps[3] = int(tstr[pos:])
            if len_remainder == 3:
                time_comps[3] *= 1000

    return time_comps


def _parse_isoformat_time(tstr):
    # Format supported is HH[:MM[:SS[.fff[fff]]]][+HH:MM[:SS[.ffffff]]]
    len_str = len(tstr)
    if len_str < 2:
        raise ValueError('Isoformat time too short')

    # This is equivalent to re.search('[+-]', tstr), but faster
    tz_pos = (tstr.find('-') + 1 or tstr.find('+') + 1)
    timestr = tstr[:tz_pos-1] if tz_pos > 0 else tstr

    time_comps = _parse_hh_mm_ss_ff(timestr)

    tzi = None
    if tz_pos > 0:
        tzstr = tstr[tz_pos:]

        # Valid time zone strings are:
        # HH:MM               len: 5
        # HH:MM:SS            len: 8
        # HH:MM:SS.ffffff     len: 15

        if len(tzstr) not in (5, 8, 15):
            raise ValueError('Malformed time zone string')

        tz_comps = _parse_hh_mm_ss_ff(tzstr)
        if all(x == 0 for x in tz_comps):
            tzi = timezone.utc
        else:
            tzsign = -1 if tstr[tz_pos - 1] == '-' else 1

            td = timedelta(hours=tz_comps[0], minutes=tz_comps[1],
                           seconds=tz_comps[2], microseconds=tz_comps[3])

            tzi = timezone(tzsign * td)

    time_comps.append(tzi)

    return time_comps


def _isoformat2datetime(date_string):
    """Construct a datetime from the output of datetime.isoformat()."""
    if not isinstance(date_string, str):
        raise TypeError('fromisoformat: argument must be str')

    # Split this at the separator
    dstr = date_string[0:10]
    tstr = date_string[11:]

    try:
        date_components = _parse_isoformat_date(dstr)
    except ValueError:
        raise ValueError(f'Invalid isoformat string: {date_string!r}')

    if tstr:
        try:
            time_components = _parse_isoformat_time(tstr)
        except ValueError:
            raise ValueError(f'Invalid isoformat string: {date_string!r}')
    else:
        time_components = [0, 0, 0, 0, None]

    return datetime(*(date_components + time_components))


def isoformat2datetime(date_string, desperate: bool = True):
    if sys.version_info < (3, 7):
        # datetime doesn't have fromisoformat for <3.6
        fun = _isoformat2datetime
    else:
        fun = datetime.fromisoformat

    try:
        result = fun(date_string)
    except TypeError as e:
        if isinstance(date_string, bytes):
            npdt = np.datetime64(date_string)
            # assuming ns type datetime64, convert to utc timestamp
            result = datetime.fromtimestamp(npdt.astype(int)*1e-9)
        else:
            raise e
    return result


def yslice(yval, xarr, yarr):
    """Returns the pairs (x, y) that are solutions to y=yval.  Original data
    points are returned."""
    ycomp = yarr > yval
    idx = np.where(ycomp[0:-2] != ycomp[1:-1])
    xx = xarr[idx]
    yy = yarr[idx]
    return xx, yy


def resize(arr: np.ndarray, shape: t.Sequence):
    """Similar to numpy's resize, but fills with nan's instead of 0s."""
    n = len(arr)
    totsize = np.prod(shape)
    newarr = np.empty(totsize)
    newarr[:n] = arr
    newarr[n:] = np.nan
    shaped = np.reshape(newarr, shape)
    return shaped


def unique(seq: t.Iterable) -> t.Tuple:
    """Given a sequence, returns the unique values in an ordered manner."""
    return tuple(dict.fromkeys(seq).keys())


def partial(func, *args, **kwargs):
    """Freezes an argument of a function.  See `functools.partial` for usage
    details.  The only difference is that this propagates names and docstrings
    from the original function by default.

    """
    pfunc = _partial(func, *args, **kwargs)
    update_wrapper(pfunc, func)
    return pfunc


def makeodd(d: int) -> int:
    """If even returns d+1, returns d otherwise."""
    return d + 1 if d % 2 == 0 else d


def within(
        x: Number, a: Number, b: Number,
        linclusive: bool = False, rinclusive: bool = True):
    """Returns whether x lies within the interval {a, b}.  The exact boundary
    is determined by the `rinclusive`, `linclusive` arguments.  Defaults to the
    interval (a, b].

    Args:
        x:  The value to evaluate the existence within boundary.
        a:  Lower bound
        b:  Upper bound
        rinclusive:  Whether upper bound is included or not.
        linclusive:  Whether lower bound is included or not.

    """
    if linclusive:
        left = a <= x
    else:
        left = a < x

    if rinclusive:
        right = x <= b
    else:
        right = x < b
    return left and right


def fullrange(start, stop, step):
    return np.arange(start, stop+step/2, step)


def find_nearest_idx(array: np.ndarray, value: Number):
    """Simply returns the indice of the element that has the closest value to
    given `value` argument.
    """
    idx = np.searchsorted(array, value, side="left")
    if (idx > 0) and (idx == len(array) or np.abs(value - array[idx-1]) < np.abs(value - array[idx])):
        return idx-1
    else:
        return idx


def watt2dbw(watt: UNumeric) -> UNumeric:
    return 10*np.log10(watt)


def watt2dbm(watt: UNumeric) -> UNumeric:
    return watt2dbw(watt) + 30


def dbw2watt(dbw: UNumeric) -> UNumeric:
    return 10**(dbw/10)


def dbm2watt(dbm: UNumeric) -> UNumeric:
    return dbw2watt(dbm-30)


def mag(lin: UNumeric) -> UNumeric:
    return np.abs(lin)


def mag_db_volt(lin: UNumeric) -> UNumeric:
    """Returns the magnitude in dB assuming the given value is in volt
    scale."""
    return lin2db_volt(mag(lin))


def lin2db_volt(lin: UNumeric) -> UNumeric:
    """Converts linear to logarithmic dB scale, assuming voltage ratio."""
    return 20*np.log10(lin)


def db2lin_volt(db: UNumeric) -> UNumeric:
    """Converts dB to linear, voltage-ratio, scale."""
    return 10**(db/20)


def lin2db_pow(lin: UNumeric) -> UNumeric:
    """Converts linear to logarithmic dB scale, assuming power ratio."""
    return 10*np.log10(lin)


def db2lin_pow(db: UNumeric) -> UNumeric:
    """Converts dB to linear, power-ratio, scale."""
    return 10**(db/10)


def angle_rad(num: UNumeric):
    """Returns the polar angle of a complex number in radians."""
    return np.angle(num)


def angle_deg(num: UNumeric):
    """Returns the polar angle of a complex number in degrees."""
    return np.rad2deg(angle_rad(num))


def unwrap_deg(degrees: _Array, fromrad: bool = True):
    """Unwraps degree phase by changing absolute jumps between values greater
    than 180 degrees by taking 360 degree complement.

    Args:
        degrees:  Array-like object that holds degree values to be unwrapped.
        fromrad:  If this is `True` the input array is first converted to
            radians.  Note that this apparently redundant step actually
            improves numerical stability due to some other factors.
    """
    # IMPORTANT
    if fromrad:
        return np.rad2deg(np.unwrap(np.deg2rad(degrees)))
    else:
        return np.unwrap(degrees, 180)


def unwrap_rad(rads: _Array):
    """Unwraps degree phase by changing absolute jumps between values greater
    than `pi` radians by taking the 2`pi` complement.

    Args:
        rads:  Array-like object that holds degree values to be unwrapped.
    """
    return np.unwrap(rads)


def angle_pi(num: UNumeric):
    """Returns the polar angle of a complex number in units of pi."""
    return angle_rad(num)/np.pi
