"""Estimators for resonance and other parameters.
"""

import typing as t
from numbers import Number

import scipy.constants as cnst
from scipy.constants import speed_of_light
import numpy as np
from scipy.signal import find_peaks, savgol_filter
import lmfit
import xarray as xr

from .measurement import SParam1PModel, MeasurementModel, NetworkParams1PModel
from .measurement import quantity
from . import units as un
from .util import unwrap_deg, makeodd, partial, yslice
from .logging import logger
from groundcontrol.friendly import mprefix_str as _mstr


_Array = np.ndarray

UNumeric = t.Union[Number, _Array]
""" type:  An alias for the types that ufuncs can operate on."""


_ptfe_phase_velocity = 0.695*speed_of_light


def estimate_flux_current(ibarr, farr, r=0.8):
    fpiq = farr.min() + r*(farr.max() - farr.min())
    ii, _ = yslice(fpiq, ibarr, farr)
    try:
        return np.abs(ii[2] - ii[0])
    except IndexError:
        return None


def find_zero_flux_current(
        ibarr: _Array,
        farr: _Array,
        ibflux: float,
        refc: float = 0):
    """Given arrays for current (ibarr) and resonance frequencies (farr),
    estimates the current at which flux is zero at the SQUID loop.  Returns a
    tuple of (current, frequency) corresponding to that point.  This algorithm
    relies on the fact that the flux corresponding to the reference current
    (refc) is less than phi0/2.

    It works as follows:
        1. Divide the current values into two regions of length `ibflux`
            centered at `refc`.
        2. Find the current that gives the maximum resonance frequency for both
            of these regions.
        3. Choose the current with the minimum absolute value.

    This is rather generic algorithm that would work for any periodic data.
    """
    k = 1
    idxr = np.where((ibarr <= refc+ibflux*k)*(ibarr >= refc))
    idxl = np.where((ibarr >= refc-ibflux*k)*(ibarr < refc))

    rmaxidx = np.argmax(farr[idxr])
    lmaxidx = np.argmax(farr[idxl])

    ibleft = ibarr[idxl][lmaxidx]
    fleft = farr[idxl][lmaxidx]
    ibright = ibarr[idxr][rmaxidx]
    fright = farr[idxr][rmaxidx]

    return min(
        [(ibleft, fleft), (ibright, fright)],
        key=lambda tpl: np.abs(tpl[0] - refc))


def make_resonance_map_dataarray(
        ibarr, farr, refc: t.Optional[float] = None,
        mode: t.Optional[str] = 'interpolated',
        ibunit: str = 'A', funit: str = 'Hz',
        ibflux: t.Optional[float] = None,
        **kwargs):
    """Convenience function to prepare a xarray.DataArray with ib and flux as
    coordinates properly scaled.  If `didphi` is `None`, the flux is calibrated
    using the periodicity in the resonance frequency array `farr`.

    Args:
        ibarr:  Current array.
        farr:  Resonance frequency array.
        refc:  Current around which to look for the zero flux point.  Estimated
            as the median of `ibarr` if `None`.
        mode:  One of {'simple', 'interpolated'}.  If 'simple', data is used as
            is when finding the 0 flux point.  If it is 'interpolated', 
            the ib array is divided in steps of `dib/1000` where dib is the
            current difference corresponding to one flux quantum.  The 0 flux
            current is found using the frequency array interpolated on this new
            set of points.
        ibunit: Unit of currents.
        funit: Unit of frequencies.
        ibflux: Flux-current slope (dib/dphi) in units of 'ibunit'/phi0.  Flux
            is calibrated using this if provided.
        kwargs['r']:  r value to be used when estimating "flux" period in current.

    Returns:
        da:  An `xarray.DataArray` with the coordinates `ib` and `flux`.

    """
    RINTERP = 1000

    ib0 = np.nan
    if refc is None:
        refc = np.median(ibarr)

    if ibflux is not None:
        ib0, fr0 = find_zero_flux_current(ibarr, farr, ibflux, refc)
        fluxarr = (ibarr - ib0)/ibflux
    else:
        fcr = kwargs.get('r', 0.8)
        logger.info(f"Using r={fcr:.1f} when estimating flux period.")
        ibflux = estimate_flux_current(ibarr, farr, r=fcr)
        if ibflux is None:
            logger.warning('No periodicity in data.  Flux is not estimated.')
            fluxarr = np.empty_like(ibarr)
            fluxarr[:] = np.nan
        elif mode == 'simple':
            ib0, fr0 = find_zero_flux_current(ibarr, farr, ibflux, refc)
            fluxarr = (ibarr - ib0)/ibflux
        elif mode == 'interpolated':
            ibdom = np.arange(
                    ibarr.min(), ibarr.max(), ibflux/RINTERP)
            
            arrsort = sorted(zip(ibarr, farr))
            ibarrsort, farrsort = np.array(arrsort).T
            finterp = np.interp(ibdom, ibarrsort, farrsort)
            # better estimate of ibflux
            ibflux = estimate_flux_current(ibdom, finterp)
            ib0, fr0 = find_zero_flux_current(
                    ibdom, finterp, ibflux, refc=refc)
            fluxarr = (ibarr - ib0)/ibflux
        else:
            raise ValueError("mode can be one of ('simple', 'interpolated')")

    logger.info(f"Flux period: {_mstr(ibflux, 'A')}")
    logger.info(f"Trapped flux: {ib0/ibflux:.2f}")

    dims = 'ib'

    da = xr.DataArray(
            farr,
            coords={'flux': (dims, fluxarr), 'ib': (dims, ibarr)},
            dims=dims, name='fr',
            attrs={'units': funit, 'long_name': '$ f_r $'}
            )

    da.coords['flux'].attrs['units'] = "$ \Phi_0 $"
    da.coords['flux'].attrs['long_name'] = "Flux"
    da.coords['ib'].attrs['units'] = ibunit
    da.coords['ib'].attrs['long_name'] = "$ i_b $"

    da.coords['ibflux'] = ibflux
    ibflattrs = da.coords['ibflux'].attrs
    ibflattrs['long_name'] = "$ di_b/d\Phi $"
    ibflattrs['units'] = f"{ibunit}/phi0"
    da.coords['ib0phi'] = ib0
    ib0attrs = da.coords['ib0phi'].attrs
    ib0attrs['long_name'] = "$ i_b(\Phi = 0) $"
    ib0attrs['units'] = ibunit

    return da


def resonator_phase_response_simple(
        f, fr, bw, phi0, elen, vp=_ptfe_phase_velocity,
        unit='deg'):
    """Phase response of an overcoupled resonator that doesn't have any
    coupling effect included.

    Args:
        fr: Resonance frequency.
        bw: Bandwidth.
        phi0: Phase offset.
        elen: Electrical length of the cables coming and out of resonator.
        vp: Phase velocity of the medium carrying signals.  Defaults to
            coaxial cable PTFE phase velocity.

    """
    if unit == 'deg':
        _factor = (360/(2*np.pi))
    else:
        _factor = 1
    a = 2*np.pi*(elen/vp)
    gamma = bw/2
    const = phi0
    lin = -a*f
    res = 2*np.arctan((fr/gamma)*(1-f/fr))
    phi = const + (lin + res)*_factor
    return phi


class ResonatorPhaseSimpleModel(lmfit.Model):
    """Implements the phase response model:
    ``phi = phi0 - (2*pi*L/v_p)*f + 2*atan((2*fr/bw)*(1-f/fr))``
    where:
        phi0:  Phase offset.
        L:  Total length of the transmission lines going in and out of the
            resonator.
        fr:  Resonance frequency.
        bw:  Resonance bandwidth.

    Args:
        unit: Unit of the model.  Can be one of 'deg' or 'rad'.

    Model Parameters:
        fr: Resonance frequency.
        bw: Bandwidth.
        phi0: Phase offset.
        elen: Electrical length of the cables coming and out of resonator.
        vp: Phase velocity of the medium carrying signals.  Defaults to
            coaxial cable PTFE phase velocity.

    """
    def __init__(self, unit='deg', *args, **kwargs):
        fun = partial(resonator_phase_response_simple, unit=unit)
        self.unit = unit
        super().__init__(fun, *args, **kwargs)
        self.set_param_hint('vp', vary=False)

    def guess(self, data, f=None, **kwargs):
        """Returns parameters guessed from the data.

        Args:
            data:  The phase data.
            f: Frequencies.
            **kwargs:  Dictionary with parameter names as keys and values as
                nominal values.
        """
        verbose = kwargs.pop('verbose', False)
        if f is None:
            return

        df = np.diff(f)  # should be always positive
        dfmin = df[df > 0].min()  # minimum step in f

        frmin = f.min()
        frmax = f.max()

        # Assume 10% of the span is bandwidth
        bw0 = kwargs.get('bw', 0.1*(frmax - frmin))

        # Assume sampling can actually resolve bandwidth.
        bwmin = dfmin
        bwmax = frmax - frmin

        # Assume the resonance is somewhere in the middle
        fr0 = kwargs.get('fr', np.median(f))

        # Assume 10m of cable length by default.
        elen0 = kwargs.get('elength', 10)
        elenmin = 0
        elenmax = 50

        vp0 = kwargs.get('vp', _ptfe_phase_velocity)
        vpmin = vpmax = vp0

        if self.unit == 'deg':
            pi = 180
        else:
            pi = np.pi

        phi00 = data[0] + (elen0/vp0)*2*pi*f[0] - pi
        phi0min = data[0] + (elenmin/vpmax)*2*pi*f[0] - pi
        phi0max = data[0] + (elenmax/vpmin)*2*pi*f[0] - pi

        parnames = 'fr bw phi0 elen vp'.split()
        pardict = dict(zip(parnames, (
                        (fr0, frmin, frmax),
                        (bw0, bwmin, bwmax),
                        (phi00, phi0min, phi0max),
                        (elen0, elenmin, elenmax),
                        (vp0, vpmin, vpmax))
                        ))

        if verbose:
            tmpl = "{s}=({tpl[0]}, {tpl[1]}, {tpl[2]})"
            lines = ["Guessing:"]
            for k, tpl in pardict.items():
                lines.append(tmpl.format(s=k, tpl=tpl))
            logger.info("\n\t".join(lines))

        params = self.make_params()
        # The params may have added prefix.
        param_key_tmpl = f"{self.prefix}" + "{key}"
        for key, (val0, valmin, valmax) in pardict.items():
            pk = param_key_tmpl.format(key=key)
            if params[pk].vary:
                params[pk].set(value=val0, min=valmin, max=valmax)
            else:
                params[pk].set(value=val0)

        return lmfit.models.update_param_vals(params, self.prefix, **kwargs)


phasemodel = ResonatorPhaseSimpleModel()


def resonance_from_phase_simple(
        measurement: SParam1PModel,
        smoothing_band: float = 1000e3) -> t.Optional[float]:
    """Estimates the resonance using the phase information.
    Args:
        measurement:  The S-parameter measurement to use when estimating
            resonance.
        smoothing_band:  This determines the window length of the 3rd order
            savitzky-golay filter used for resonance estimation.

    Returns:
        resonance:  Returns the resonance frequency as `float`, returns `None`
            if no conclusive estimation was made.
    """
    # unwrapped phases to see the change clearly
    phases = unwrap_deg(measurement.phases)
    f = measurement.frequencies

    df = f[1] - f[0]

    # Interpolation Window as Band
    window_length = makeodd(smoothing_band/df)

    # Prefilter for improvement!
    order = 3
    filtered = savgol_filter(phases, window_length, order, mode='nearest')

    dph = -np.diff(filtered)/df

    dph_filtered = savgol_filter(dph, window_length, order, mode='mirror')

    peaks, props = find_peaks(dph_filtered, prominence=1e-4)

    if len(peaks) != 1:
        return None

    prominence = props['prominences'][0]
    leftbaseidx = props['left_bases'][0]
    rightbaseidx = props['right_bases'][0]

    # The region where the peak is
    peakregion = dph_filtered[leftbaseidx:rightbaseidx+1]

    # Finding the kind-of-fwhm indices
    band = np.where(peakregion - prominence/2 > 0)[0]
    leftidx, rightidx = band[0], band[-1]

    f_left, f_right = f[leftbaseidx + leftidx], f[leftbaseidx + rightidx]

    # estimating the resonance as the arithmetic mean
    f_res = (f_right + f_left)/2

    return f_res


def resonance_from_phase(
        measurement: SParam1PModel,
        smoothing_band: float,
        peakfinder: str = 'argmax') -> float:
    """Estimates the resonance using the phase information.
    Args:
        measurement:  The S-parameter measurement to use when estimating
            resonance.
        smoothing_band:  This determines the window length of the 3rd order
            savitzky-golay filter used for resonance estimation.
        peakfinder:  Can be one of ['prominence', 'argmax'].  'argmax' is
            the simplest one, peakfinder uses scipy's peak finding algorithm.
            Currently 'prominence' is a bit unstable.  Defaults to 'argmax'.
    """
    # unwrapped phases to see the change clearly
    phases = unwrap_deg(measurement.phases)
    f = measurement.frequencies

    df = f[1] - f[0]

    # Interpolation Window as Band
    window_length = makeodd(smoothing_band/df)

    # Prefilter for improvement!
    order = 3
    try:
        filtered = savgol_filter(
                phases, window_length, order, mode='nearest')
    except ValueError as e:
        msg = ("SG filter error: "
               f"savgol_filter(phases, {window_length}, {order}, "
               "'nearest'). "
               f"'{e.args[0]}'")
        logger.debug(msg)
        return None

    try:
        dph = -np.diff(filtered)/df
        dph_filtered = savgol_filter(
                dph, window_length, order,
                mode='mirror')
    except ValueError as e:
        logger.debug(
                "SG filter error: "
                f"savgol_filter(dph, {window_length}, {order}, 'mirror'). "
                f"'{e.args[0]}'")
        return None

    if peakfinder == 'argmax':
        peakind = np.argmax(dph_filtered)
        f_res = f[peakind]
    else:
        peaks, props = find_peaks(dph_filtered, prominence=1e-4)

        if len(peaks) != 1:
            return None

        peakind = peaks[0]
        # this can also be used as resonance
        prominence = props['prominences'][0]
        leftbaseidx = props['left_bases'][0]
        rightbaseidx = props['right_bases'][0]

        # The region where the peak is
        peakregion = dph_filtered[leftbaseidx:rightbaseidx+1]

        # Finding the kind-of-fwhm indices
        band = np.where(peakregion - prominence/2 > 0)[0]
        leftidx, rightidx = band[0], band[-1]

        f_left, f_right = f[leftbaseidx + leftidx], f[leftbaseidx + rightidx]

        # estimating the resonance as the arithmetic mean
        f_res = (f_right + f_left)/2

    return f_res


class DelayMeasurement(MeasurementModel):
    delays: np.ndarray = quantity(un.second, 'Group Delay')
    frequencies: np.ndarray = quantity(un.hertz, 'Frequency')

    @classmethod
    def from_sparam_phase(
            cls,
            s: SParam1PModel):
        fs = s.frequencies
        df = fs[1] - fs[0]
        # assuming degree phases
        delay = -(1/360)*np.diff(s.uphases)/df
        return cls(delay, fs[1:])


s2delay = DelayMeasurement.from_sparam_phase


def sgsmooth(x, y, xwindow, porder: int = 3, mode='mirror'):
    """Thin wrapper around `scipy.signal`s savgol filter implementation.
    Assumes x is regularly spaced and sorted in increasing order."""
    df = x[1] - x[0]
    wl = makeodd(int(xwindow // df))
    try:
        yvals_sg = savgol_filter(
                y, window_length=wl,
                polyorder=porder, mode=mode)
    except ValueError as e:
        if e.args[0].startswith('polyorder'):
            logger.debug(
                    f"DF: {df}, WL: {wl}, PO: {porder}, XWINDOW: {xwindow}")
        raise e

    return yvals_sg


def smoothify(
        meas: MeasurementModel,
        band: float,
        xname: str = 'frequencies',
        yname: t.Union[str, t.Iterable] = 'uphases',
        porder: int = 3,
        drop_band_ratio: float = 0,
        mode='mirror'):
    """Applies a smoothing algorithm to the unwrapped phase data of the given
    SParam1PModel object.  band:  The running frequency band at which the
    smoothing is applied.  porder:  Polynomial order for the SG filter
    smoothing.  drop_band_ratio:  If 0.1, 0.05 times the `band` is cut from
    each side of the spectrum from the end result.
    """
    try:
        # yname is a str
        yvals_l = [getattr(meas, yname)]
        yname = [yname]
    except TypeError:
        # yname is a list
        yvals_l = [getattr(meas, yn) for yn in yname]
    xvals = getattr(meas, xname)
    df = xvals[1] - xvals[0]
    wl = makeodd(int(band // df))

    try:
        yvals_sg_l = []
        for yvals in yvals_l:
            yvals_sg = savgol_filter(
                    yvals, window_length=wl,
                    polyorder=porder, mode=mode)
            yvals_sg_l.append(yvals_sg)
    except ValueError as e:
        if e.args[0].startswith('polyorder'):
            logger.debug(f"DF: {df}, WL: {wl}, PO: {porder}, BAND: {band}")
        raise e
    dropband = drop_band_ratio * band
    xvals_max = xvals.max() - dropband/2
    xvals_min = xvals.min() + dropband/2
    inds = np.where((xvals >= xvals_min)*(xvals <= xvals_max))[0]
    sparnew = meas.evolve(**dict(zip(yname, yvals_sg_l)))
    new = sparnew.cut(inds)
    return new


def resonance_from_phase2_nppm(
        nppm: NetworkParams1PModel, bw_expected: float, upsample: int = 20,
        return_aux: bool = False):
    if nppm.displayformat != 'UPHASE':
        raise TypeError("NetworkParams1PModel data must be in 'UPHASE' format")
    spar = SParam1PModel.from_nppm(nppm)
    return resonance_from_phase2(spar, bw_expected, upsample, return_aux)


def resonance_from_phase2(
        spar: SParam1PModel, bw_expected: float, upsample: int = 20,
        return_aux: bool = False):
    logger.debug(f"BW: {bw_expected}")
    smoothbandfactor = 1
    smband = bw_expected*smoothbandfactor
    smoothed_phase = smoothify(spar, smband, drop_band_ratio=0.5)
    delay = s2delay(smoothed_phase)
    smoothed_delay = smoothify(delay, smband, yname='delays', porder=3)
    delays = smoothed_delay.delays
    fs = smoothed_delay.frequencies
    fs_min, fs_max, n = fs.min(), fs.max(), len(fs)
    fsnew = np.linspace(fs_min, fs_max, upsample*n)
    interped = np.interp(fsnew, fs, delays)

    df = fsnew[1] - fsnew[0]
    width = bw_expected // df
    halfmax = 1/(np.pi*bw_expected)
    height_min = halfmax/2
    width_min = width/2
    peakinds, props = find_peaks(interped, width=width_min, height=height_min)
    logger.debug(f"height,width,max: {height_min, width_min, interped.max()}")
    npeak = len(peakinds)
    logger.debug(f"resonance_from_phase2 npeak: {npeak:d}")
    if npeak == 0:
        fres = None
    elif npeak == 1:
        fres = fsnew[peakinds[0]]
    elif npeak <= 5:
        # If multipeak, return highest one.
        _indmax = props['width_heights'].argmax()
        fres = fsnew[peakinds[_indmax]]
    else:
        # If TOO MANY peak, no peak found.
        fres = None
    if not return_aux:
        return fres
    else:
        return fres, smoothed_phase, delay, smoothed_delay, fsnew, interped


_sppmtmp = SParam1PModel.fake()


def _resonance_from_phase(
        f: np.ndarray, ph: np.ndarray,
        bw_expected: float, upsample: int = 20,
        return_aux=False):
    """Phases must be unwrapped."""
    sppm = _sppmtmp.evolve(frequencies=f, uphases=ph)
    return resonance_from_phase2(sppm, bw_expected, upsample, return_aux)
