import attr
import typing as t

from groundcontrol.declarators import quantity, setting, parameter
from groundcontrol.settings import InstrumentSettings
from groundcontrol.logging import setup_logger
import groundcontrol.units as un


logger = setup_logger("pidtuner")


class TunerSettings(InstrumentSettings):
    kp: float = setting(un.nounit)
    ki: float = setting(un.nounit)
    kd: float = setting(un.nounit)

    ramp_step: t.Optional[float] = setting(un.nounit)
    setpoint: t.Optional[float] = setting(un.nounit)


@attr.s(auto_attribs=True)
class PIDTuner:
    """Class that implements PID tuning logic for change in actuator
    parameter.

    The form used for PID control is:
    $$
    u_k = Kp(e_k + (1/Ki) \sum_{k'=0}^k {e_{k'}} + K_d(e_k - e_{k-1})) # noqa:  W605
    b_k = b_{k-1} + u_k
    $$
    where b_k is the bias point at k'th iteration.  This PID only returns u_k.

    In this sense the first difference relation tells us how to update b_k.

    """
    setpoint: float = parameter(un.nounit, "Setpoint")
    kp: float = parameter(un.nounit, "P value")
    ki: float = parameter(un.nounit, "I value")
    kd: float = parameter(un.nounit, "D value")

    ramping: bool = setting(un.nounit, "Setpoint Ramping", default=False)
    ramp_step: float = parameter(un.nounit, "Ramp Step", default=None)

    # Variables
    error: float = quantity(un.nounit, "Error", True, 0)
    integral: float = quantity(un.nounit, "Integral", True, 0)

    _active_setpoint: float = quantity(un.nounit, "Last Setpoint", True)

    @property
    def active_setpoint(self):
        return self._active_setpoint

    def _get_active_setpoint(
            self, measurement: float):
        if self.ramping and (self.ramp_step is not None):
            if self.active_setpoint is None:
                lastsp = measurement
            else:
                lastsp = self.active_setpoint
            # this is somewhat convoluted way
            # better way to just generate ramping points
            # beforehand and traversing those.
            if lastsp > self.setpoint:
                nextsp = lastsp - self.ramp_step
                if nextsp < self.setpoint:
                    nextsp = self.setpoint
            else:
                nextsp = lastsp + self.ramp_step
                if nextsp > self.setpoint:
                    nextsp = self.setpoint
        else:
            nextsp = self.setpoint

        self._active_setpoint = nextsp
        return nextsp

    def calculate_integral(self, error: float):
        return (self.kp/self.ki)*error + self.integral

    def calculate_difference(self, error: float):
        return (self.kp*self.kd)*(error - self.error)

    def calculate_proportion(self, error: float):
        return error * self.kp

    def step(self, measurement: float):
        """Advances the tuner one step using the measurement.

        Returns:
            step:  This is the change that should be applied to the actuator.
        """
        active_sp = self._get_active_setpoint(measurement)
        error = active_sp - measurement
        integral = self.calculate_integral(error)
        difference = self.calculate_difference(error)
        proportion = self.calculate_proportion(error)
        # new error
        step = proportion + integral + difference
        self.error = error
        self.integral = integral
        return step

    @classmethod
    def make(
            cls,
            setpoint: float,
            kp: float,
            ki: float,
            kd: float,
            ramp_step: t.Optional[float] = None):
        ramping = True if ramp_step is not None else False
        return cls(
                setpoint=setpoint,
                kp=kp,
                ki=ki,
                kd=kd,
                ramping=ramping,
                ramp_step=ramp_step)

    @classmethod
    def from_settings(
            cls,
            settings: TunerSettings):
        return cls.make(
                settings.setpoint,
                settings.kp,
                settings.ki,
                settings.kd,
                settings.ramp_step)
