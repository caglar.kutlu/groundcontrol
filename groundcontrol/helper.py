"""Several helper objects.

TODO:
    - StrEnum's from_str classmethod is super-inefficient.  Generate the
      `reverse_map` upon creation of the class internally and refer to that
      when converting.
"""
import typing as t
from pathlib import Path
from itertools import count, groupby, zip_longest, tee, chain
from functools import partial, wraps
from enum import Enum
from numbers import Number
import string
from logging import Logger
from datetime import datetime, date
import sys

import numpy as np
from random import choices
from numpy import random, ndarray
from attr import NOTHING
import cattr


_pyver = sys.version_info[0]*10 + sys.version_info[1]
is_oldpy = _pyver < 37


def _get_origin_compat(typ):
    try:
        return typ.__origin__
    except AttributeError as e:
        return None


def _get_args_compat(typ):
    try:
        return typ.__args__
    except AttributeError as e:
        return ()


if is_oldpy:
    t.get_origin = _get_origin_compat
    t.get_args = _get_args_compat


# How to ensure that this is executed upon library loading?
cattr.register_structure_hook(Path, lambda s, t: Path(s))


DateTimeLike = t.Union[datetime, np.datetime64]

T = t.TypeVar('T', float, int, str, DateTimeLike)


class Array(t.Generic[T]):
    pass


def default_serializer(obj, returnrepr=True):
    """Mainly used to prepare string representation for objects that are not by
    default JSON serializable.
    """

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    elif isinstance(obj, Path):
        return obj.as_posix()
    else:
        if returnrepr:
            return repr(obj)
        else:
            return obj

    raise TypeError(f"Type {type(obj)!s} is not serializable.")


# Some defaults for the arbitrary function, chosen totally arbitrarily as well.
ARBINT_MIN = 0
ARBINT_MAX = 10000
ARBFLOAT_MIN = -1e16
ARBFLOAT_MAX = 1e16


def abortable(
        fun: t.Callable,
        logger: t.Optional[Logger] = None):
    if logger is None:
        printfun = print
    else:
        printfun = logger.info

    @wraps(fun)
    def _abortable(*args, **kwargs):
        try:
            return fun(*args, **kwargs)
        except KeyboardInterrupt:
            printfun("Aborted.")
    return _abortable


def pairwise(iterable: t.Iterable, firstelem=NOTHING):
    """s -> (s0, s1), (s1, s2), ...  If `firstelem` is not `NOTHING`, s ->
    (firstelem, s0), (s0, s1), (s1, s2), ..."""
    if not (firstelem is NOTHING):
        iterable = chain([firstelem], iterable)
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def makecounter(start: int = 0, step: int = 1):
    """Returns a function that takes no arguments and returns a count
    generator.
    """
    return partial(count, start, step)


class EnumNotFoundError(KeyError):
    pass


def _strenum_structure_hook(s, cls):
    try:
        return cls.__members__[s]
    except KeyError:
        # this is for backwards compatibility, should be removed in the future.
        return cls.from_str(s)


class StrEnum(str, Enum):
    """An enum derivative that is compatible with string literals.

    If multiple enumerations have the same value, the first one is considered
    master and the other ones are aliases.

    Multiple values can correspond to the same enum too.  To declare such
    cases, alternative patterns can be declared using the syntax:
        _ENUMNAME_n = 'MYENUMVALUE'

    where n can be any string, typically number.

    Any derivative of this class registers structure and unstructure hooks for
    cattr to use.  Enums are unstructured by name rather than value.
    """
    def __new__(cls, *args, **kwargs):
        cattr.register_structure_hook(cls, _strenum_structure_hook)
        cattr.register_unstructure_hook(cls, lambda v: v.name)
        return super().__new__(cls, *args, **kwargs)

    @classmethod
    def from_str(cls, s: str):
        """Converts a string into a type of `cls` type enum.

        Raises:
            EnumNotFoundError:  When the string doesn't have a corresponding
                token.
        """

        members_map = cls.__members__

        for k, v in members_map.items():
            if s.lower().startswith(v.lower()):
                if k.startswith('_'):
                    kk = k.split('_')[1]
                    return members_map[kk]
                else:
                    return v

        raise EnumNotFoundError(
                f"Enum corresponding to '{s}' was not found in {cls}")


def _str_join_ignore_none(iterable):
    """Joins strings, ignoring None's"""
    m = map(lambda s: "" if s is None else str(s), iterable)
    return "".join(m)


def shorten(s: str, d: int = 0):
    """Shortens the string using principally the capital letters and a maximal
    `d` number of lowercase letters distributed near uniformly next to the
    capital letters.

    Example:
        >>> s = "MyIncredibleModel"
        >>> shorten(s, 5)
        >>> 'MyIncMod'
        >>> shorten(s)
        >>> 'MIM'

    """
    if s == "":
        return ""

    if s[0].isupper():
        startswithcap = True
    else:
        startswithcap = False

    groups = groupby(s, lambda c: c.isupper())

    uppers = []
    lowers = []

    for iscapital, group in groups:
        if iscapital:
            uppers.append(list(group))
        else:
            lowers.append(list(group))

    lowers_trunc = {i: [] for i in range(len(lowers))}
    while d > 0:
        for i, s in enumerate(lowers):
            try:
                lowers_trunc[i].append(s.pop(0))
                d -= 1
                if d <= 0:
                    break
            except IndexError:  # pop from empty list
                if i == (len(lowers) - 1):
                    break
                else:
                    continue
    pickedlows = map("".join, lowers_trunc.values())
    upperstrings = map("".join, uppers)

    if startswithcap:
        zipd = zip_longest(upperstrings, pickedlows)
    else:
        zipd = zip_longest(pickedlows, upperstrings)
    return "".join(map(_str_join_ignore_none, zipd))


# Experimenting with typing, don't mind me :)
T = t.TypeVar('T')
OpT = t.Optional[T]
R = t.TypeVar('R')


def domaybe(fun: t.Callable[[T], R], arg: OpT) -> t.Optional[R]:
    """Returns `None` if arg is `None`, returns fun(arg) otherwise."""
    out = None if arg is None else fun(arg)
    return out


def arbitrary_int(
        min: t.Optional[int] = None,
        max: t.Optional[int] = None,
        size=None):
    min = ARBINT_MIN if min is None else min
    max = ARBINT_MAX if max is None else max
    return random.randint(low=min, high=max, size=size)


def arbitrary_float(
        min: t.Optional[float] = None,
        max: t.Optional[float] = None,
        size=None):
    min = ARBFLOAT_MIN if min is None else min
    max = ARBFLOAT_MAX if max is None else max
    sample = random.random_sample(size)
    return (max - min) * sample + min


def arbitrary_array(
        size: t.Union[int, t.Tuple[int]] = 10,
        dtype=float,
        min: t.Optional[Number] = None,
        max: t.Optional[Number] = None):
    dispatcher = {
        int: arbitrary_int,
        float: arbitrary_float}
    return dispatcher[dtype](min, max, size)


def arbitrary_str(size: int = 10):
    return "".join(choices(string.ascii_letters, k=size))


def arbitrary(type, **boundaries):
    """Given a type and boundaries for random data generation, returns a random
    value for the type.  This is a simple function for random data generation.
    Currently supported types and their valid boundary arguments are as
    follows:

        int, min=ARBINT_MIN, max=ARBINT_MAX:  Returns an integer in
            interval (min, max).

        float, min=ARBFLOAT_MIN, max=ARBFLOAT_MAX:  Returns an
            integer in interval (min, max).

        np.ndarray, size=10, dtype=float,
            min=ARBFLOAT_MIN, max=ARBFLOAT_MAX:  Returns an array
            of `size` values of type `dtype` within the interval (min,max).
    """
    if t.get_origin(type) == Array:
        valtyp = t.get_args(type)[0]
        return arbitrary_array(dtype=valtyp, **boundaries)

    dispatcher = {
            datetime: datetime.now,
            int: arbitrary_int,
            float: arbitrary_float,
            ndarray: arbitrary_array,
            str: arbitrary_str}
    return dispatcher[type](**boundaries)
