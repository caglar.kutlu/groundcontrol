import typing as t

import attr

from groundcontrol.instruments import VISAInstrument, SCPICommon
from groundcontrol.helper import StrEnum
from enum import IntEnum
from groundcontrol.instruments._common import optarg


@attr.s(auto_attribs=True)
class YokogawaGS200(SCPICommon, VISAInstrument):
    """Class for Yokogawa GS200 SCPI messaging protocol.
    Implementation is most likely incomplete."""
    State = SCPICommon.State

    class SourceFunction(StrEnum):
        VOLTAGE = "VOLT"
        CURRENT = "CURR"

    class Incremental(StrEnum):
        UP = "UP"
        DOWN = "DOWN"

    class Marginal(StrEnum):
        MINIMUM = "MIN"
        MAXIMUM = "MAX"

    class ProgTrig(StrEnum):
        NORMAL = "NORM"
        MEND = "MEND"

    class SensTrig(StrEnum):
        READY = "READ"
        TIMER = "TIMER"
        COMMUNICATE = "COMM"
        IMMEDIATE = "IMM"

    class WiringSystem(StrEnum):
        W2 = "OFF"
        W4 = "ON"
        _W2_1 = "0"
        _W4_1 = "1"

    def set_output_state(self, st: State):
        return self.write(f"OUTP {st}")

    def query_output_state(self) -> 'State':
        q = self.query("OUTP?")
        return self.State.from_str(q)

    def set_source_function(self, sfunc: SourceFunction):
        return self.write(f"SOUR:FUNC {sfunc}")

    def query_source_function(self):
        q = self.query("SOUR:FUNC?")
        return self.SourceFunction.from_str(q)

    def set_source_range(
            self, sourcerange: t.Union[float, Marginal, Incremental]):
        """Sets the source range.
        If sourcerange float, it specifies the smallest range that includes the
        selected voltage or current level.  Else:
            Marginal.MINIMUM:  Sets the range to the minimum range.
            Marginal.MAXIMUM:  Sets the range to the maximum range.
            Incremental.UP:  Increase the range by one level.
            Incremental.DOWN:  Decrease the range by one level.

        """
        return self.write(f"SOUR:RANG {sourcerange}")

    def query_source_range(self, rangemargin: t.Optional[Marginal] = None):
        """Queries the source range.  Queried absolute is returned if
        `rangemargin` is provided."""
        q = self.query_float(f'SOUR:RANG?{optarg(rangemargin, False)}')
        return q

    def set_source_level(self, level: t.Union[float, Marginal]):
        """Sets the source level in terms of the active range.
        If level is `float`, it specifies the level.  Else:
            Marginal.MINIMUM:  Sets the level to the minimum allowed by
                range.
            Marginal.MAXIMUM:  Sets the level to the maximum allowed by
                range.
        """
        return self.write(f"SOUR:LEV {level}")

    def query_source_level(self, levmargin: t.Optional[Marginal] = None):
        """Queries the source range.  Queried absolute is returned if `levmargin`
        is provided."""
        q = self.query_float(f'SOUR:LEV?{optarg(levmargin, False)}')
        return q

    def set_source_level_auto(self, level: t.Union[float, Marginal]):
        """Sets the source level in terms of the most appropriate range.
        If level is `float`, it specifies the level and the range is set to the
        smallest source range that includes the specified level. Else:
            Marginal.MINIMUM:  Sets the range to the maximum range and level
                to the minimum value.
            Marginal.MAXIMUM:  Sets the level to the maximum range and the
                level to the maximum value.
        """
        return self.write(f"SOUR:LEV:AUTO {level}")

    def query_source_level_auto(self, levmargin: t.Optional[Marginal] = None):
        """Queries the source level auto.  Queried absolute is returned if
        levmargin is provided."""
        q = self.query_float(f'SOUR:LEV:AUTO?{optarg(levmargin, False)}')
        return q

    def set_source_protection_voltage(
            self, pvolt: t.Union[float, Marginal]):
        """Sets voltage limiter level.

        if pvolt is
            Marginal.MINIMUM:  Sets to +-1V.
            Marginal.MAXIMUM:  Sets to +-30V.
        """
        return self.write(f"SOUR:PROT:VOLT {pvolt}")

    def query_source_protection_voltage(
            self, voltmargin: t.Optional[Marginal] = None):
        """Queries the voltage limiter level.  Queried absolute is returned if
        voltmargin is provided."""
        q = self.query_float(f'SOUR:PROT:VOLT?{optarg(voltmargin, False)}')
        return q

    def set_source_protection_current(
            self, pcurrent: t.Union[float, Marginal]):
        """Sets current limiter level.

        if pcurrent is
            Marginal.MINIMUM:  Sets to +-1V.
            Marginal.MAXIMUM:  Sets to +-30V.
        """
        return self.write(f"SOUR:CURR:VOLT {pcurrent}")

    def query_source_protection_current(
            self, currmargin: t.Optional[Marginal] = None):
        """Queries the current limiter level.  Queried absolute is returned if
        `currmargin` is provided."""
        q = self.query_float(f'SOUR:PROT:CURR? {optarg(currmargin, False)}')
        return q

    def set_program_repeat_state(self, state: State):
        return self.write(f"PROG:REP {state}")

    def query_program_repeat_state(self):
        return self.State.from_str(self.query("PROG:REP?"))

    def set_program_interval(self, interval: t.Union[float, Marginal]):
        return self.write(f"PROG:INT {interval}")

    def query_program_interval(self, intmargin: t.Optional[Marginal]):
        return self.query_float(f"PROG:INT?{optarg(intmargin, False)}")

    def set_program_slope(self, slope: t.Union[float, Marginal]):
        return self.write(f"PROG:INT {slope}")

    def query_program_slope(self, slopemargin: t.Optional[Marginal]):
        return self.query_float(f"PROG:INT?{optarg(slopemargin, False)}")

    def set_program_trigger(self, trig: ProgTrig):
        """Sets the program trigger.
            ProgTrig.NORMAL: Sets the trigger normal.
            ProgTrig.MEND: Sets the trigger to measurement end.
        """
        return self.write(f"PROG:TRIG {trig}")

    def query_program_trigger(self):
        q = self.query("PROG:TRIG?")
        return self.ProgTrig.from_str(q)

    def set_sense_state(self, stat: State):
        return self.write(f"SENS {stat}")

    def query_sense_state(self):
        q = self.query("SENS?")
        return self.State.from_str(q)

    def set_sense_nplc(
            self, timeplc: t.Union[int, Marginal]):
        return self.write(f"SENS:NPLC {timeplc}")

    def query_sense_nplc(
            self, timeplcmargin: t.Optional[Marginal] = None):
        return self.query_int(f"SENS:NPLC?{optarg(timeplcmargin, False)}")

    def set_sense_delay(
            self, delay: t.Union[float, Marginal]):
        return self.write(f"SENS:DEL {delay}")

    def query_sense_delay(
            self, delaymargin: t.Optional[Marginal] = None):
        return self.query_float(f"SENS:DEL?{optarg(delaymargin, False)}")

    def set_sense_trigger(self, senstrig: SensTrig):
        return self.write(f"SENS:TRIG {senstrig}")

    def query_sense_trigger(self):
        q = self.query("SENS:TRIG?")
        return self.SensTrig.from_str(q)

    def set_sense_interval(self, seconds: t.Union[float, Marginal]):
        return self.write(f"SENS:INT {seconds}")

    def query_sense_interval(
            self, secondsmargin: t.Optional[Marginal] = None):
        return self.write(f"SENS:INT?{optarg(secondsmargin, False)}")

    def set_sense_null_state(
            self, state: State):
        return self.write(f"SENS:NULL {state}")

    def query_sense_null_state(self):
        return self.State.from_str(self.query("SENS:NULL?"))

    def do_sense_null_execute(self):
        return self.write(f"SENS:ZERO:EXEC")

    def set_sense_remote(self, wsys: WiringSystem):
        return self.write(f"SENS:REM {wsys}")

    def query_sense_remote(self):
        q = self.query("SENS:REM?")
        return self.WiringSystem.from_str(q)

    def set_sense_guard(self, state: State):
        return self.write(f"SENS:GUAR {state}")

    def query_sense_guard(self) -> 'State':
        return self.State.from_str(self.query("SENS:GUAR?"))

    def do_initiate(self):
        """Clears the measured value and prepares the GS200 to perform the next
        measurement."""
        return self.write("INIT")

    def query_fetch(self):
        """Queries the measured result."""
        return self.query_float("FETC?")

    def query_read(self):
        """Clears the measured value, prepares the GS200 to perform the next
        measurement, and queries the measured result."""
        return self.query_float("READ?")

    def query_measure(self):
        """Clears the measured value, prepares the GS200 to perform the next
        measurement, generates a measurement trigger, and queries the measured
        result.

        Same with :INIT;*TRG;:FETC?
        """
        return self.query_float("MEAS?")

    def set_system_display_text(self, message: str):
        return self.write(f'SYST:DISP:TEXT "{message}"')

    def query_system_display_text(self) -> str:
        return self.query_str(f"SYST:DISP:TEXT?")

    def do_system_display_text_clear(self):
        return self.write(f"SYST:DISP:TEXT:CLE")

    def do_system_display_normal(self):
        return self.write(f"SYST:DISP:NORM")

    def query_system_error(self):
        return self.query(f"SYST:ERR?")

    def do_system_local(self):
        return self.write(f"SYST:LOC")

    def do_system_remote(self):
        return self.write(f"SYST:REM")
