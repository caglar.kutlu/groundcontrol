"""Spectrum analyzers."""
import attr

from groundcontrol.instruments import VISAInstrument
from groundcontrol.helper import StrEnum
from groundcontrol.logging import setup_logger


logger = setup_logger(__name__)


def _deprecated(func):
    logger.warning("This function is going to be deprecated.")
    return func


@attr.s(auto_attribs=True)
class SCPISpectrumAnalyzer(VISAInstrument):
    """Class to hold SCPI commands for generic spectrum analyzer.
    This class should serve as an interface to implement for derived classes.

    """

    class State(StrEnum):
        ON = '1'
        OFF = '0'
        
        @classmethod
        def from_bool(cls, b: bool):
            if b:
                return cls.ON
            else:
                return cls.OFF

    class Result(StrEnum):
        SUCCESS = '0'
        FAIL = '1'

    class DetectorType(StrEnum):
        NORMAL = "NORM"
        AVERAGE = "AVER"
        POSITIVE = "POS"
        SAMPLE = "SAMP"
        NEGATIVE = "NEG"
        QUASIPEAK = "QPE"
        EMIAVERAGE = "EAV"
        RMSAVERAGE = "RAV"
        RMS = "RMS"

    class InputCoupling(StrEnum):
        AC = "AC"
        DC = "DC"

    class Unit(StrEnum):
        DBM = "DBM"
        DBMV = "DBMV"
        DBMA = "DBMA"
        VOLT = "V"
        WATT = "W"
        AMPERE = "A"
        DBUV = "DBUV"
        DBUA = "DBUA"
        DBPW = "DBPW"
        DBUVM = "DBUVM"
        DBUAM = "DBUAM"
        DBPT = "DBPT"
        DBG = "DBG"

    class SweepType(StrEnum):
        SWEEP = "SWE"
        AUTO = "AUTO"
        FFT = "FFT"

    class SweepTypeRule(StrEnum):
        SPEED = "SPE"
        DRANGE = "DRAN"

    class AverageType(StrEnum):
        LOG = "LOG"
        LINEAR = "LIN"
        POWER = "POW"

    class FormatDataType(StrEnum):
        ASCII = "ASCII"
        REAL32 = "REAL,32"
        REAL64 = "REAL,64"
        INT32 = "INT,32"

    class ByteOrderFormat(StrEnum):
        BIG_ENDIAN = "NORM"
        LITTLE_ENDIAN = "SWAP"
        NORMAL = "NORM"  # BIG ENDIAN
        SWAPPED = "SWAP"  # LITTLE ENDIAN

    class ScalingType(StrEnum):
        LOG = "LOG"
        PERCENT = "LIN"  # Percentage
        LINEAR = "LDB"  # Linear scaling, manual confuses

    class FilterType(StrEnum):
        GAUSSIAN = "GAUS"
        FLATTOP = "FLAT"

    class FilterBWType(StrEnum):
        DB3 = "DB3"
        DB6 = "DB6"
        IMPULSE = "IMP"
        NOISE = "NOIS"

    class SweepTimeRule(StrEnum):
        NORMAL = "NORM"
        ACCURACY = "ACC"
        SRESPONSE = "SRES"

    class TraceType(StrEnum):
        CLEARWRITE = "WRIT"
        AVERAGE = "AVER"
        MAXHOLD = "MAXH"
        MINHOLD = "MINH"

    class ScaleType(StrEnum):
        LIN = "LIN"
        LOG = "LOG"

    class NoiseType(StrEnum):
        NORMAL = "NORM"
        SNS = "SNS"

    class InstrumentMode(StrEnum):
        """There are many more than what's listed here."""
        SA = "SA"
        EMI = "EMI"
        NFIGURE = "NFIG"

    class MarkerMode(StrEnum):
        POSITION = "POS"
        DELTA = "DELT"
        FIXED = "FIX"
        OFF = "OFF"

    class CalAuto(StrEnum):
        ON = "ON"
        LIGHT = "LIGH"
        PARTIAL = "PART"
        OFF = "OFF"
        ALERT = "PART"

    class CalAutoMode(StrEnum):
        ALL = "ALL"
        NRF = "NRF"

    _dispatcher = dict()

    def _query_enum(self, s: str, enumtype: StrEnum):
        """Shorthand for conversion of received strings to enumerations."""
        q = self.query(s)
        return enumtype.from_str(q)

    def _query_state(self, s: str):
        return self._query_enum(s, self.State)

    def query_idn(self):
        return self.query("*IDN?")

    def query_opc(self):
        resp = self.query("*OPC?")
        return resp

    def set_format_byte_order(self, border: ByteOrderFormat):
        """Sets the byte order. NORM is big endian, SWAP is little endian."""
        return self.write(f"FORM:BORD {border}")

    def query_format_byte_order(self) -> "ByteOrderFormat":
        """Queries the byte order, choose from ['NORMal, 'SWAPped'].
        NORM is big endian, SWAP is little endian."""
        q = self.query("FORM:BORD?")
        return self.ByteOrderFormat.from_str(q)

    def set_input_coupling(self, coupling: InputCoupling):
        return self.write("INP:COUP {cpl}".format(cpl=coupling))

    def query_input_coupling(self) -> "InputCoupling":
        q = self.query("INP:COUP?")
        return self.InputCoupling.from_str(q)

    def set_input_gain_state(self, state: State):
        """Sets the preamp state"""
        return self.write(f"INP:GAIN:STAT {state}")

    def query_input_gain_state(self, state: State):
        """Queries the preamp state"""
        q = self.query("INP:GAIN:STAT?")
        return self.State.from_str(q)

    def set_sense_frequency_span(self, span: float):
        """Set your unit with 1e6 1e3 etc."""
        ret = self.write("SENS:FREQ:SPAN {span} Hz".format(span=span))
        return ret

    def query_sense_frequency_span(self) -> float:
        return self.query_float("SENS:FREQ:SPAN?")

    def set_sense_frequency_center(self, center: float):
        ret = self.write("SENS:FREQ:CENT {center} Hz".format(center=center))
        return ret

    def query_sense_frequency_center(self) -> float:
        return self.query_float("SENS:FREQ:CENT?")

    def set_sense_frequency_start(self, start: float):
        return self.write("SENS:FREQ:START {start} Hz".format(start=start))

    def query_sense_frequency_start(self) -> float:
        return self.query_float("SENS:FREQ:START?")

    def set_sense_frequency_stop(self, stop):
        return self.write("SENS:FREQ:STOP {stop} Hz".format(stop=stop))

    def query_sense_frequency_stop(self) -> float:
        return self.query_float("SENS:FREQ:STOP?")

    def set_sense_bandwidth_resolution(self, rbw: float):
        """Sets the resolution bandwidth.  Nearest available bandwidth is selected.
        """
        return self.write("SENS:BAND:RES {rbw:.3f} Hz".format(rbw=rbw))

    def query_sense_bandwidth_resolution(self) -> float:
        return self.query_float("SENS:BAND:RES?")

    def set_sense_bandwidth_resolution_auto(self, state: State):
        """Sets the state of automatic coupling of rbw to span.  If `ON`, RBW
        decrease when SPAN decreases.
        """
        return self.write(f"SENS:BAND:RES:AUTO {state}")

    def query_sense_bandwidth_resolution_auto(self) -> "State":
        return self._query_state("SENS:BAND:RES:AUTO?")

    def set_sense_bandwidth_video(self, vbw: float):
        return self.write(f"SENS:BAND:VID {vbw:.3f} Hz")

    def query_sense_bandwidth_video(self) -> float:
        return self.query_float("SENS:BAND:VID?")

    def set_sense_bandwidth_video_auto(self, state: State):
        """Sets the state of automatic coupling of VBW to RBW.  If `ON`, VBW
        decrease when RBW decreases.
        """
        return self.write(f"SENS:BAND:VID:AUTO {state}")

    def query_sense_bandwidth_video_auto(self) -> "State":
        return self._query_state("SENS:BAND:VID:AUTO?")

    def set_sense_bandwidth_video_ratio(self, ratio: float):
        """Sets the ratio VBW/RBW(3dB)"""
        return self.write(f"SENS:BAND:VID:RAT {ratio:.5f}")

    def query_sense_bandwidth_video_ratio(self):
        """Queries the ratio VBW/RBW(3dB)"""
        return self.query_float("SENS:BAND:VID:RAT?")

    def set_sense_bandwidth_video_ratio_auto(self, state: State):
        """Sets the state of the automatic selection of VBW/RBW(3dB) ratio."""
        return self.write(f"SENS:BAND:VID:RAT:AUTO {state}")

    def query_sense_bandwidth_video_ratio_auto(self) -> "State":
        return self._query_state("SENS:BAND:VID:RAT:AUTO?")

    def set_sense_frequency_span_bandwidth_ratio(self, ratio: int):
        """Sets the ratio SPAN/RBW(3dB)"""
        return self.write(f"SENS:FREQ:SPAN:BAND:RES:RAT {ratio}")

    def query_sense_frequency_span_bandwidth_ratio(self):
        """Queries the ratio SPAN/RBW(3dB)"""
        return self.query_int("SENS:FREQ:SPAN:BAND:RES:RAT?")

    def set_sense_frequency_span_bandwidth_ratio_auto(self, state: State):
        """Sets the state of the automatic selection of SPAN/RBW(3dB) ratio."""
        return self.write(f"SENS:FREQ:SPAN:BAND:RES:RAT:AUTO {state}")

    def query_sense_frequency_span_bandwidth_ratio_auto(self) -> "State":
        return self._query_state("SENS:FREQ:SPAN:BAND:RES:RAT:AUTO?")

    def set_sense_bandwidth_shape(self, filtertype: FilterType):
        return self.write(f"SENS:BAND:SHAP {filtertype}")

    def query_sense_bandwidth_shape(self) -> "FilterType":
        q = self.query("SENS:BAND:SHAP?")
        return self.FilterType.from_str(q)

    def set_sense_bandwidth_type(self, bwtype: FilterBWType):
        return self.write(f"SENS:BAND:TYPE {bwtype}")

    def query_sense_bandwidth_type(self) -> "FilterBWType":
        q = self.query("SENS:BAND:TYPE?")
        return self.FilterBWType.from_str(q)

    def set_sense_average_count(self, count: int, wnum: int = 1):
        """Sets the average `count` for measurements in window `wnum`."""
        return self.write(f"SENS:AVER{wnum:d}:COUN {count}")

    def query_sense_average_count(self, wnum: int = 1) -> int:
        return self.query_int(f"SENS:AVER{wnum:d}:COUN?")

    def do_sense_average_clear(self):
        """Resets the average/hold count and begins another set of sweeps when
        trigger conditions are satisfied.  It is only effective if active trace
        is in Average or Hold type.
        """
        return self.write("SENS:AVER:CLE")

    def set_sense_average_type(self, avgtype: AverageType, wnum: int = 1):
        return self.write(f"SENS:AVER{wnum:d}:TYPE {avgtype}")

    def query_sense_average_type(self, wnum: int = 1) -> "AverageType":
        q = self.query(f"SENS:AVER{wnum:d}:TYPE?")
        return self.AverageType.from_str(q)

    def set_sense_average_type_auto(self, state: State, wnum: int = 1):
        """Sets whether the average type selected automatically or not."""
        return self.write(f"SENS:AVER{wnum:d}:TYPE:AUTO {state}")

    def query_sense_average_type_auto(self, wnum: int = 1) -> "State":
        """Sets whether the average type selected automatically or not."""
        return self._query_state(f"SENS:AVER{wnum:d}:TYPE:AUTO?")

    def set_sense_average_state(self, state: State, wnum: int = 1):
        """Sets the global averaging state.
        Notes:
            - Modern analyzers provide averaging state setting also per trace
              basis.
        """
        self.write(f"SENS:AVER{wnum:d}:STAT {state}")

    def query_sense_average_state(self, wnum: int = 1) -> "State":
        """Queries the global averaging state."""
        return self._query_state(f"SENS:AVER{wnum:d}:STAT?")

    def set_sense_detector(self, dettype: DetectorType, traceno: int = 1):
        self.write(f"SENS:DET:TRAC{traceno:d} {dettype}")

    def query_sense_detector(self, traceno: int = 1) -> "DetectorType":
        q = self.query(f"SENS:DET:TRAC{traceno:d}?")
        return self.DetectorType.from_str(q)

    def set_unit_power(self, unit: Unit):
        self.write(f"UNIT:POW {unit}")

    def query_unit_power(self) -> "Unit":
        q = self.query(f"UNIT:POW?")
        return self.Unit.from_str(q)

    def set_trace_type(self, ttype: TraceType, traceno: int = 1):
        return self.write(f"TRAC{traceno:d}:TYPE {ttype}")

    def query_trace_type(self, traceno: int = 1) -> "TraceType":
        return self.TraceType.from_str(self.query(f"TRAC{traceno:d}:TYPE?"))

    def set_trace_update_state(self, state: State, traceno: int = 1):
        return self.write(f"TRAC{traceno:d}:UPD:STAT {state}")

    def query_trace_update_state(self, traceno: int = 1) -> "State":
        return self._query_state(f"TRAC{traceno:d}:UPD:STAT?")

    def set_trace_display_state(self, state: State, traceno: int = 1):
        return self.write(f"TRAC{traceno:d}:DISP:STAT {state}")

    def query_trace_display_state(self, traceno: int = 1) -> "State":
        return self._query_state(f"TRAC{traceno:d}:DISP:STAT?")

    def set_sense_sweep_points(self, points: int):
        return self.write("SWE:POIN {pnts:d}".format(pnts=points))

    def query_sense_sweep_points(self) -> int:
        return self.query_int("SWE:POIN?")

    def set_sense_sweep_type(self, swetype: SweepType):
        return self.write("SWE:TYPE {styp}".format(styp=swetype))

    def query_sweep_type(self) -> "SweepType":
        return self.SweepType.from_str(self.query("SWE:TYPE?"))

    def set_sense_sweep_type_auto(self, state: State):
        return self.write(f"SWE:TYPE:AUTO {state}")

    def query_sense_sweep_type_auto(self) -> "State":
        return self._query_state(f"SWE:TYPE:AUTO?")

    def set_sense_sweep_type_auto_rules(self, rule: SweepTypeRule):
        return self.write(f"SWE:TYPE:AUTO:RUL {rule}")

    def query_sense_sweep_type_auto_rules(self) -> "SweepTypeRule":
        q = self.query(f"SWE:TYPE:AUTO:RUL?")
        return self.SweepTypeRule.from_str(q)

    def set_sense_sweep_type_auto_rules_auto(self, state: State):
        self.write(f"SWE:TYPE:AUTO:RUL:AUTO {state}")

    def query_sense_sweep_type_auto_rules_auto(self) -> "State":
        self._query_state(f"SWE:TYPE:AUTO:RUL:AUTO?")

    def set_sense_sweep_fft_width(self, fftwidth):
        """Sets the maximum FFT width to be used in Hz."""
        self.write(f"SWE:FFT:WIDT {fftwidth}")

    def query_sense_sweep_fft_width(self):
        """Queries the maximum FFT width to be used in Hz."""
        self.query("SWE:FFT:WIDT?")

    def set_sense_sweep_time(self, swptime: float):
        return self.write("SWE:TIME {}".format(swptime))

    def query_sense_sweep_time(self) -> float:
        return self.query_float("SWE:TIME?")

    def set_sense_sweep_time_auto(self, state: State):
        """ON or OFF"""
        return self.write("SWE:TIME:AUTO {state}".format(state=state))

    def query_sense_sweep_time_auto(self) -> "State":
        """ON or OFF or TIME"""
        return self._query_state("SWE:TIME:AUTO?")

    # FOR BACKWARD COMPATIBILITY
    query_sweep_time_auto = query_sense_sweep_time_auto
    set_sweep_time_auto = set_sense_sweep_time_auto

    def set_sense_sweep_time_auto_rules(self, rule: SweepTimeRule):
        self.write(f"SWE:TIME:AUTO:RUL {rule}")

    def query_sense_sweep_time_auto_rules(self) -> "SweepTimeRule":
        q = self.query(f"SWE:TIME:AUTO:RUL?")
        return self.SweepTimeRule.from_str(q)

    def set_sense_sweep_time_auto_rules_auto(self, state: State):
        return self.write(f"SWE:TIME:AUTO:RUL:AUTO {state}")

    def query_sense_sweep_time_auto_rules_auto(self) -> "State":
        return self._query_state("SWE:TIME:AUTO:RUL:AUTO?")

    def set_initiate_continuous(self, state: State):
        """ON: Continuous, OFF: Single sweep"""
        return self.write(f"INIT:CONT {state}")

    def query_initiate_continuous(self):
        """ON: Continuous, OFF: Single sweep"""
        return self.query(f"INIT:CONT?")

    def do_initiate_immediate(self):
        """Restarts the current sweep, or measurement, or a set of
        averaged/held sweeps or measurements.  If you are Paused, pressing
        Restart does a Resume."""
        return self.write(f"INIT:IMM")

    def do_pause(self):
        return self.write(f"INIT:PAUS")

    def do_resume(self):
        return self.write(f"INIT:RES")

    def query_format_data(self) -> "FormatDataType":
        return self.FormatDataType.from_str(self.query("FORM:DATA?"))

    def set_format_data(self, fmt: FormatDataType):
        return self.write("FORM:DATA {fmt}".format(fmt=fmt))

    def do_wait(self):
        return self.write("*WAI")

    def query_trace_data(
            self,
            dataformat: FormatDataType,
            byteorder: ByteOrderFormat = ByteOrderFormat.BIG_ENDIAN,
            traceno: int = 1):
        qstr = f"TRAC:DATA? TRACE{traceno}"
        if dataformat == self.FormatDataType.ASCII:
            return self.query_ascii_values(qstr)

        if dataformat == self.FormatDataType.REAL64:
            datatype = 'd'
        else:
            datatype = 'f'

        is_big_endian = byteorder == self.ByteOrderFormat.BIG_ENDIAN
        return self.query_binary_values(
                qstr, datatype, is_big_endian)

    def set_display_window_trace_y_rlevel(self, reflevel: float):
        """Sets the level of the topmost graticule."""
        return self.write(f"DISP:WIND:TRAC:Y:RLEV {reflevel}")

    def query_display_window_trace_y_rlevel(self):
        """Queries the level of the topmost graticule."""
        return self.query_float(f"DISP:WIND:TRAC:Y:RLEV?")

    def set_display_window_trace_y_rlevel_offset(self, offset: float):
        """Sets the level of the topmost graticule."""
        return self.write(f"DISP:WIND:TRAC:Y:RLEV:OFFS {offset}")

    def query_display_window_trace_y_rlevel_offset(self) -> float:
        """Sets the level of the topmost graticule."""
        return self.query_float(f"DISP:WIND:TRAC:Y:RLEV:OFFS?")

    def set_sense_power_attenuation(self, lossdb: float):
        return self.write(f"SENS:POW:ATT {lossdb:.1f}")

    def query_sense_power_attenuation(self) -> float:
        return self.query_float(f"SENS:POW:ATT?")

    def set_sense_power_attenuation_auto(self, state: State):
        return self.write(f"SENS:POW:ATT:AUTO {state}")

    def query_sense_power_attenuation_auto(self):
        return self._query_state(f"SENS:POW:ATT:AUTO?")

    def set_display_window_trace_y_pdivision(self, scaledb: float):
        return self.write(f"DISP:WIND:TRAC:Y:SCAL:PDIV {scaledb}")

    def query_display_window_trace_y_pdivision(self) -> float:
        return self.query_float(f"DISP:WIND:TRAC:Y:SCAL:PDIV?")

    def set_display_window_trace_y_spacing(self, spacing: ScaleType):
        return self.write(f"DISP:WIND:TRAC:Y:SCAL:SPAC {spacing}")

    def query_display_window_trace_y_spacing(self):
        q = self.ScaleType.from_str(self.query(f"DISP:WIND:TRAC:Y:SCAL:SPAC?"))
        return q

    def set_source_noise_type(self, type: NoiseType):
        return self.write(f"SOUR:NOIS:TYPE {type}")

    def query_source_noise_type(self) -> "NoiseType":
        return self._query_enum(f"SOUR:NOIS:TYPE?", self.NoiseType)

    def set_source_noise_state(self, state: State):
        return self.write(f"SOUR:NOIS {state}")

    def query_source_noise_state(self) -> "State":
        return self._query_state("SOUR:NOIS?")

    def set_calculate_marker_mode(self, mode: MarkerMode, marker: int = 1):
        return self.write(f"CALC:MARK{marker:d}:MODE {mode}")

    def query_calculate_marker_mode(self, marker: int = 1) -> "MarkerMode":
        return self._query_enum(f"CALC:MARK{marker:d}:MODE?", self.MarkerMode)

    def set_calculate_marker_x(self, xvalue: float, marker: int = 1):
        return self.write(f"CALC:MARK{marker:d}:X {xvalue}")

    def query_calculate_marker_x(self, marker: int = 1) -> float:
        return self.query_float(f"CALC:MARK{marker:d}:X?")

    def set_calculate_marker_x_position(self, xbucket: int, marker: int = 1):
        """Sets the marker in terms of bucket number rather than absolute
        X-value."""
        return self.write(f"CALC:MARK{marker:d}:X:POS {xbucket}")

    def query_calculate_marker_x_position(self, marker: int = 1) -> int:
        """Queries the marker in terms of bucket number rather than absolute
        X-value."""
        return int(self.query_float(f"CALC:MARK{marker:d}:X:POS?"))

    def set_calculate_marker_y(self, yvalue: float, marker: int = 1):
        return self.write(f"CALC:MARK{marker:d}:Y {yvalue}")

    def query_calculate_marker_y(self, marker: int = 1) -> float:
        return self.query_float(f"CALC:MARK{marker:d}:Y?")

    def do_calculate_marker_alloff(self):
        return self.write("CALC:MARK:AOFF")

    def set_marker_x(self, markernum: int, xvalue: float):
        """Sets the marker using the fundamental units of the x axis scale."""
        self.write(f"CALC:MARK{markernum:d}:X {xvalue:.1f}")

    def issue_shutdown(self):
        self.write("SYSTEM:SHUTDOWN")

    def set_instrument_mode(self, mode: InstrumentMode):
        """Sets the mode of the device"""
        return self.write(f"INST:SEL {mode}")

    def query_instrument_mode(self, mode: InstrumentMode):
        """Sets the mode of the device"""
        return self._query_enum("INST:SEL?", self.InstrumentMode)

    def set_calibration_auto(self, auto: CalAuto):
        return self.write(f"CAL:AUTO {auto}")

    def query_calibration_auto(self) -> CalAuto:
        return self.CalAuto.from_str(
                self.query("CAL:AUTO?"))

    def set_calibration_auto_mode(self, automode: CalAutoMode):
        return self.write(f"CAL:AUTO:MODE {automode}")

    def query_calibration_auto_mode(self) -> CalAutoMode:
        return self.CalAutoMode.from_str(
                self.query("CAL:AUTO:MODE"))

    def do_calibration(self):
        return self.write("CAL")

    def do_query_calibration(self) -> Result:
        q = self.query("CAL?")
        return self.Result.from_str(q)

    def do_calibration_npending(self):
        return self.write("CAL:NPEN")

    def do_calibration_nrf(self):
        return self.write("CAL:NRF")

    def do_query_calibration_nrf(self) -> Result:
        return self.Result.from_str(self.query("CAL:NRF?"))

    def do_calibration_rf(self):
        return self.write("CAL:RF")

    def do_query_calibration_rf(self) -> Result:
        return self.Result.from_str(self.query("CAL:RF?"))

    def do_calibration_ytf(self):
        return self.write("CAL:YTF")

    def do_query_calibration_ytf(self) -> Result:
        return self.Result.from_str(self.query("CAL:YTF?"))

    def do_calibration_ytf_npending(self):
        return self.write("CAL:YTF:NPEN")

    def do_system_preset_user(self):
        return self.write("SYST:PRES:USER")

    def do_sense_power_rf_pcenter(self):
        """Presel Center

        This state is not saved in instrument state.  It does not survive a
        power cycle.

        """
        return self.write("SENS:POW:PCEN")

    def set_sense_power_rf_padjust(self, freq: float):
        return self.write(f"SENS:POW:PADJ {freq}")

    def query_sense_power_rf_padjust(self) -> float:
        return self.query_float(f"SENS:POW:PADJ?")

