import typing as t
from enum import IntEnum

from groundcontrol.instruments import VISAInstrument, SCPICommon
from groundcontrol.instruments._common import SCPIError
from groundcontrol.helper import StrEnum


def _optarg(arg: t.Optional[t.Any] = None, hascomma=True):
    retval = ""
    if arg is not None:
        if hascomma:
            retval = f", {arg}"
        else:
            retval = f" {arg}"
    return retval


class R5550SCPI(SCPICommon, VISAInstrument):
    """Instrument class for ThinkRF's model R5550 Real-time spectrum analyzer.
    Implements the SCPI commands."""

    class CaptureMode(StrEnum):
        BLOCK = "BLOCK"
        STREAMING = "STREAMING"
        SWEEPING = "SWEEPING"

    class LanConfigMode(StrEnum):
        DHCP = "DHCP"
        STATIC = "STATIC"

    class Marginal(StrEnum):
        MINIMUM = "MIN"
        MAXIMUM = "MAX"

    class ErrorCode(IntEnum):
        NOERROR = 0
        COMMANDERROR = -100
        CHARTOOLONG = -144
        INVALIDEXPR = -171
        EXECERROR = -200
        TRIGERR = -210
        NOMODULEMATCH = -220
        DATAOUTOFRANGE = -222
        TOOMUCHDATA = -223
        ILLEGALPARAM = -224
        DATACORRUPT = 230
        HWERROR = -240
        HWMISSING = -241
        SYSTEMERROR = -310
        OUTOFMEM = -321
        SELFTESTFAIL = -330
        CALFAIL = -340
        QUERYOVFLW = -350
        QUERYINTRP = -410
        NODATA = -901
        NEEDFWUP = -911
        INVALOPTLICENSE = -912

    class RefClock(StrEnum):
        INTERNAL = "INT"
        EXTERNAL = "EXT"

    class InputMode(StrEnum):
        ZIF = "ZIF"
        DD = "DD"
        HDR = "HDR"
        SH = "SH"
        SHN = "SHN"

    class LockState(StrEnum):
        LOCKED = "1"
        NOTLOCKED = "0"

    class OutputMode(StrEnum):
        CONNECTOR = "CONN"
        DIGITIZER = "DIG"
        HIF = "HIF"

    class Attenuator(IntEnum):
        DB0 = 0
        DB10 = 10
        DB20 = 20
        DB30 = 30

    def do_system_abort(self):
        """This command will cause the R55x0 to stop the data capturing,
        whether in the manual trace block capture, triggering or sweeping mode.
        The R55x0 will be put into the manual mode; in other words, process
        such as streaming, trigger and sweep will be stopped.  The capturing
        process does not wait until the end of a packet to stop, it will stop
        immediately upon receiving the command."""
        return self.write("SYST:ABOR")

    def query_system_capture_mode(self) -> "CaptureMode":
        """This command returns what the current RTSA data capture mode is
        (i.e. sweeping, streaming or block mode)."""
        return self.CaptureMode.from_str(
                self.query(
                    "SYST:CAPT:MODE?"))

    def query_system_communicate_hislip_session(self):
        """When connected over HiSLIP, this command returns the HiSLIP Session
        ID, which is used to establish the associated data connection."""
        return self.query("SYST:COMM:HISL:SESS?")

    def do_system_communicate_lan_apply(self):
        """This command will apply the changes to the LAN settings and the
        embedded system will automatically reconfigure the Ethernet to put in
        effect the new LAN setting. Once the LAN settings are applied, they are
        not affected by power-on, :STATus:PRESET, or *RST.

        Caution: When changing from DHCP to STATIC mode, this command should to
        be sent only when all the required LAN settings have been set using the
        appropriate :SYSTem:COMMunicate:LAN commands.
        """
        return self.write("SYST:COMM:LAN:APPLY")

    def set_system_communicate_lan_configure(self, lanconfig: LanConfigMode):
        """The set command will store the new LAN configuration type to be
        applied to the RTSA.  This command does not take effect until
        :SYSTem:COMMunicate:LAN:APPLy is sent (please refer to the Caution note
        of the :APPLy command). Once the option is applied, it is not affected
        by power-on, :STATus:PRESET, or *RST.  """
        return self.write(f"SYST:COMM:LAN:CONF {lanconfig}")

    def query_system_communicate_lan_configure(self) -> 'LanConfigMode':
        """The query will return the option set or that of the actual current
        LAN configuration if one is not set. The CURRENT query will return what
        is currently and actually used by the RTSA's LAN interface."""
        return self.LanConfigMode.from_str(
                self.query("SYST:COMM:LAN:CONF? CURRENT"))

    def do_system_communicate_lan_default(self):
        """This command will put the RTSA into DHCP mode. Any DHCP associated
        LAN configuration changes set prior to this command will be applied as
        well. These associated LAN commands are:
            :SYSTem:COMMunicate:LAN:DNS
            :SYSTem:COMMunicate:LAN:GATEway
            :SYSTem:COMMunicate:LAN:IP
            :SYSTem:COMMunicate:LAN:MTU
            :SYSTem:COMMunicate:LAN:NETMask
            :SYSTem:COMMunicate:NTP

        Caution: This command takes effect immediately. If already in DHCP
        mode, the IP might change.
        """
        return self.write("SYST:COMM:LAN:DEF")

    def set_system_communicate_lan_dns(self, dns: str):
        """The set command will store the new LAN DNS server address(es) to be
        applied to the RTSA. This command does not take effect until
        :SYSTem:COMMunicate:LAN:APPLy is sent (please refer to the Caution note
        of the :APPLy command). Once the setting is applied, it is not affected
        by power-on, :STATus:PRESET, or *RST. """
        return self.write(f"SYST:COMM:LAN:DNS {dns}")

    def query_system_communicate_lan_dns(self):
        """The query will return the LAN DNS address(es) set or that of the
        actual current configuration if one is not issued. The CURRENT query
        will return what is currently and actually used by the RTSA's LAN
        interface.
        """
        return self.query("SYST:COMM:LAN:DNS?")

    def set_system_communicate_lan_gateway(self, gateway: str):
        """The set command will store the new LAN gateway to be applied to the
        RTSA. This command does not take effect until
        :SYSTem:COMMunicate:LAN:APPLy is sent (please refer to the Caution note
        of the :APPLy command). Once the setting is applied, it is not affected
        by power-on, :STATus:PRESET, or *RST.
        """
        return self.write(f"SYST:COMM:LAN:GATE {gateway}")

    def query_system_communicate_lan_gateway(self):
        """The query will return the gateway address set or that of the actual
        current configuration if one is not issued. The CURRENT query will
        return what is currently and actually used by the RTSA's LAN interface.
        """
        return self.query("SYST:COMM:LAN:GATE?")

    def set_system_communicate_lan_ip(self, ip: str):
        """The set command will store the new LAN IP to be applied to the RTSA.
        This command does not take effect until :SYSTem:COMMunicate:LAN:APPLy
        is sent (please refer to the Caution note of the :APPLy command). Once
        the setting is applied, it is not affected by power-on, :STATus:PRESET,
        or *RST.
        """
        return self.write(f"SYST:COMM:LAN:IP {ip}")

    def query_system_communicate_lan_ip(self):
        """The query will return the IP address set or that of the actual
        current configuration if one is not issued. The CURRENT query will
        return what is currently and actually used by the RTSA's LAN interface.
        """
        return self.query("SYST:COMM:LAN:IP?")

    def set_system_communicate_lan_mtu(self, mtu: int):
        """The set command will store the new LAN MTU (maximum transfer unit,
        in bytes) to be applied to the RTSA. This command does not take effect
        until :SYSTem:COMMunicate:LAN:APPLy is sent (please refer to the
        Caution note of the :APPLy command). Once the setting is applied, it is
        not affected by power-on, :STATus:PRESET, or *RST. The factory default
        value is 1500 bytes.

        Note: Use this command to change the maximum network packet or frame
        size in a transaction; however, changing the MTU value could affect the
        transfer rate or causing lost of network packets if the packets have an
        MTU value larger than that permitted by your network. Check with your
        network administrator before changing this value.

        """
        return self.write(f"SYST:COMM:LAN:MTU {mtu:d}")

    def query_system_communicate_lan_mtu(self):
        """The query will return the MTU set or that of the actual current
        configuration if one is not issued. The CURRENT query will return what
        is currently and actually used by the RTSA's LAN interface.
        """
        return self.query("SYST:COMM:LAN:MTU?")

    def set_system_communicate_ntp(
            self, address1: str,
            address2: t.Optional[str] = None,
            address3: t.Optional[str] = None,):
        """This command sets or queries the NTP configuration server
        address(es) of up to three IP addresses. When set, this command will
        accept server IP address(es) in the form of D.D.D.D only, comma
        separated."""
        args = f"{address1}{_optarg(address2)}{_optarg(address3)}"
        return self.write(f"SYST:COMM:NTP {args}")

    def query_system_communicate_ntp(self) -> t.List[str]:
        """The query response will return the NTP IP address(es) set through
        this command or domain/IP address(es) set through R55x0's web
        administrative page.
        """
        return list(
                map(lambda s: s.strip(),
                    self.query("SYST:COMM:NTP?").split(',')))

    def query_system_error_next(self) -> SCPIError:
        """This query returns the oldest uncleared error code and message from
        the SCPI error/event queue. When there are no error messages, the query
        returns 0,"No error".  *RST does not affect the error queue.

        Note: It is recommended to do this query command after each non-query
        command is sent to ensure that the non-query command is executed
        without error. Since each error message is queued into a buffer, if
        multiple commands have been sent follow by only one
        :SYSTem:ERRor[:NEXT]? command, it would be uncleared which command has
        resulted in which error.
        """

        q = self.query("SYST:ERROR?")
        return SCPIError.from_str(q, enumfun=self.ErrorCode)

    def query_system_error_all(self) -> t.List[SCPIError]:
        """This query is similar to :SYSTem:ERRor[:NEXT]? but returns only the
        error code from the SCPI error/event queue. When there are no errors,
        the query returns 0. *RST does not affect the error queue.

        Note: Similarly, it is recommended to do this query command (or
        :SYSTem:ERRor[:NEXT]?) after each non-query command is sent to ensure
        that the non-query command is executed without error.  """
        q = self.query("SYST:ERR:ALL?")
        _itrs = [iter(q.split(','))]*2
        errtpls = zip(*_itrs)

        return [SCPIError(
            code=self.ErrorCode(int(tpl[0])),
            message=tpl[1].strip('"')) for tpl in errtpls]

    def query_system_error_code_next(self) -> 'ErrorCode':
        """This query is similar to :SYSTem:ERRor[:NEXT]? but returns only the
        error code from the SCPI error/event queue. When there are no errors,
        the query returns 0. *RST does not affect the error queue.

        Note: Similarly, it is recommended to do this query command (or
        :SYSTem:ERRor[:NEXT]?) after each non-query command is sent to ensure
        that the non-query command is executed without error.
        """
        return self.ErrorCode(self.query_int("SYST:ERR:CODE?"))

    def query_system_error_code_all(self) -> t.List['ErrorCode']:
        """This query is similar to :SYSTem:ERRor:ALL? but returns error codes
        only from the SCPI error/event queue. If there are no errors, the query
        returns 0. *RST does not affect the error queue.
        """
        q = self.query("SYST:ERR:CODE:ALL?")
        lst = q.split(',')
        errcds = map(lambda s: self.ErrorCode(int(s)), lst)
        return list(errcds)

    def query_system_error_count(self) -> int:
        """This query returns the number of errors/events in the error/event
        queue."""
        return self.query_int("SYST:ERR:COUN?")

    def do_system_flush(self):
        """This command clears the R55x0's internal data storage buffer of any
        data that is waiting to be sent.

        IMPORTANT Note: It is highly recommended that the flush command (after
        issuing :SYSTem:ABORt) should be used before start of any data capture
        or when switching between different capture modes to clear up the
        remnants of data in the RTSA.  Caution: Issuing :SYSTem:FLUSh any time
        during streaming or sweeping mode will cause the stream or sweep
        capture to stop (abort) and switch automatically to block capture mode.

        Note: Flush command only handles the RTSA's internal buffer storage.
        The host application should ensure that the socket buffer is also
        cleared up of any potential data in the socket buffer. This can be done
        by calling the receive socket (non-blocking) until no data is returned.
        With Streaming or Sweeping, the start ID in a VRT extension packet
        marks the beginning of packets belonging to the new stream or sweep.
        This helps to distinct old packets from new packets.
        """
        return self.write("SYST:FLUS")

    def set_input_attenuator(self, att: Attenuator):
        return self.write(f"INP:ATT {att:d}")

    def query_input_attenuator(self) -> Attenuator:
        return self.Attenuator(self.query_int("INP:ATT?"))

    def set_input_mode(self, mode: InputMode):
        return self.write(f"INP:MODE {mode}")

    def query_input_mode(self):
        return self.InputMode.from_str(
                self.query("INP:MODE?"))

    def set_source_reference_pll(self, refclk: RefClock):
        return self.write(f"SOUR:REF:PLL {refclk}")

    def query_source_reference_pll(self):
        return self.RefClock.from_str(self.query("SOUR:REF:PLL?"))

    def set_sense_frequency_center(self, freq: float):
        return self.write(f"FREQ:CENT {freq}")

    def query_sense_frequency_center(self):
        return self.query_float("FREQ:CENT?")

    def set_sense_frequency_shift(self, hz):
        return self.write(f"FREQ:SHIFT {hz}")

    def query_sense_frequency_shift(
            self, margin: t.Optional[Marginal] = None):
        cmd = f"FREQ:SHIFT?{_optarg(margin, False)}"
        return self.query_float(cmd)

    def query_sense_lock_reference(self) -> LockState:
        return self.LockState.from_str(
                self.query('SENS:LOCK:REF?'))

    def query_sense_lock_rf(self) -> LockState:
        return self.LockState.from_str(
                self.query('SENS:LOCK:RF?'))

    def set_output_mode(self, mode: OutputMode):
        return self.write(f"OUT:MODE {mode}")

    def query_output_mode(self) -> OutputMode:
        return self.OutputMode.from_str(self.query("OUT:MODE?"))


class R5550SCPI_WORKAROUND(R5550SCPI):
    """Whenever a setting type SCPI command is issued, the device stops
    responding to queries over the hislip connection.  However, if a query was
    sent alongside the set command, this does not happen. This class implements
    this as a hack-ish workaround.

    This means every set operation is followed by the query ":STAT:OPER?".

    E.g.:
        INP:ATT 10;:STAT:OPER?
        > 0

    """
    def write(self, cmd: str):
        self.resource.query(f"{cmd};:STAT:OPER?")

    def query(self, cmd):
        return self.resource.query(cmd)
