"""Instruments to communicate with ThinkRF devices via `speqan` software interface."""
from typing import Any

from groundcontrol.instruments import Instrument
from speqan import connect_speqan_sync, SpeqanSyncClient

import attr


@attr.define
class SpeqanClient(SpeqanSyncClient, Instrument):
    name: str
    _client: Any

    @classmethod
    def from_visa_resource_name(cls, resource_name: str, name: str):
        speqcli = connect_speqan_sync(uri=resource_name)
        return cls(name=name, client=speqcli._client)
