"""Rohde & Schwarz FSV series instruments."""

from .spectrumanalyzer import SCPISpectrumAnalyzer as _SA
from groundcontrol.helper import StrEnum


class RohdeSchwarzFSV(_SA):
    ByteOrderFormat = _SA.ByteOrderFormat

    class DetectorType(StrEnum):
        NORMAL = "APE"
        AVERAGE = "AVER"
        RMS = "RMS"
        POSITIVE = "POS"
        SAMPLE = "SAMP"
        NEGATIVE = "NEG"
        QUASIPEAK = "QPE"
        RMSAVERAGE = "RAV"
        APEAK = "APE"  # alias

    class FilterType(StrEnum):
        BH92 = "BH92"

    def set_sense_detector(self, dettype: DetectorType, traceno: int = 1):
        self.write(f"SENS:DET{traceno:d} {dettype}")

    def query_sense_detector(self, traceno: int = 1) -> DetectorType:
        return self._query_enum(f"SENS:DET{traceno:d}?", self.DetectorType)

    def set_calculate_marker_state(
            self, state: _SA.State, marker: int = 1, wnum: int = 1):
        """Sets marker on or off."""
        return self.write(f"CALC{wnum:d}:MARK{marker:d} {state}")

    def query_calculate_marker_state(
            self, marker: int = 1, wnum: int = 1) -> _SA.State:
        """Sets marker on or off."""
        return self._query_state(f"CALC{wnum:d}:MARK{marker:d}?")

    def set_calculate_marker_function_noise_state(
            self, state: _SA.State, marker: int, wnum: int = 1):
        """Turns the noise measurement for the marker"""
        return self.write(f"CALC{wnum:d}:MARK{marker:d}:FUNC:NOIS {state}")

    def query_calculate_marker_function_noise_state(
            self, marker: int, wnum: int = 1) -> _SA.State:
        """Turns the noise measurement for the marker"""
        return self._query_state(f"CALC{wnum:d}:MARK{marker:d}:FUNC:NOIS?")

    def query_calculate_marker_function_noise_result(
            self, marker: int, wnum: int = 1) -> float:
        """Turns the noise measurement result for the marker"""
        return self.query_float(f"CALC{wnum:d}:MARK{marker:d}:FUNC:NOIS:RES?")

    def query_sense_bandwidth_shape(self):
        """FSV series has only Blackman-Harris window built-in by default (in
        spectrum mode). """
        return self.FilterType.BH92

    def query_sense_bandwidth_type(self):
        """The RBW defined is 3dB bandwidth, and it's hardcoded."""
        return self.FilterBWType.DB3

    def set_format_byte_order(self, border: ByteOrderFormat):
        """FSV doesn't have this function.  It's probaby always LITTLE ENDIAN"""
        pass

    def query_format_byte_order(self) -> "ByteOrderFormat":
        """Always returns `ByteOrderFormat.LITTLE_ENDIAN`"""
        return self.ByteOrderFormat.LITTLE_ENDIAN


