from functools import wraps
import time

from groundcontrol.declarators import declarative
from groundcontrol.logging import setup_logger
from .dcsupply import YokogawaGS200
from .sourcemeter import TSPCurrentSource


logger = setup_logger('adapters')


def _nopfun(fun):
    @wraps(fun)
    def wrapped(*args, **kwargs):
        logger.warning(f'NOP for {fun.__name__}.')
    return wrapped


@declarative
class GS200Adapter(YokogawaGS200, TSPCurrentSource):
    """Class adapting GS200 to TSPCurrentSource as accepted by
    JPAController."""
    _gs = YokogawaGS200
    State = _gs.State

    write = _gs.write
    query = _gs.query

    def query(self, s: str):
        time.sleep(self.query_delay)
        return self._gs.query(self, s)

    def do_waitcomplete(self):
        return self.write('*WAI')

    @_nopfun
    def do_dataqueue_clear(self):
        pass

    @_nopfun
    def send_script(self, *args):
        pass

    def do_reset(self):
        return self.do_rst()

    def do_current_source_preset(
            self,
            rangei: float,
            leveli: float,
            state: State):
        self.set_source_function(self.SourceFunction.CURRENT)
        self.set_source_range(rangei)
        self.set_source_level(leveli)
        return self.set_output_state(state)

    def query_measure_i(self):
        return self.query_source_level()

    def set_level_i(self, leveli: float):
        return self.set_source_level(leveli)

    def query_level_i(self):
        return self.query_source_level()

    def set_range_i(self, rangei: float):
        return self.set_source_range(rangei)

    def query_range_i(self):
        return self.query_source_range()
