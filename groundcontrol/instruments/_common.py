import typing as t
from enum import IntEnum, Enum

import attr

class NotACommand(NotImplementedError):
    """Raised when the instrument itself does not implement the particular
    function."""


def optarg(arg: t.Optional[t.Any] = None, hascomma=True):
    retval = ""
    if arg is not None:
        if hascomma:
            retval = f", {arg}"
        else:
            retval = f" {arg}"
    return retval


def optargs(args: t.Sequence[t.Optional[t.Any]]):
    replaced = (
            ("" if arg is None else f"{arg}")
            for arg in args
            )
    joined = ",".join(replaced)
    if joined == "":
        return ""
    else:
        return f" {joined}"


@attr.s(auto_attribs=True)
class SCPIError:
    code: int
    message: str
    description: t.Optional[str] = None

    @classmethod
    def from_str(
            self, s, desc=None,
            enumfun: t.Optional[t.Callable[[int], IntEnum]] = None):
        codestr, msgstr = s.split(',')
        if enumfun is None:
            code = int(codestr.strip())
        else:
            code = enumfun(int(codestr.strip()))
        msg = msgstr.strip().strip('"')
        return SCPIError(code, msg, desc)
