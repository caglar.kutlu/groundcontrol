"""This module introduces the instrument baseclasses to be further derived
from.


Exceptions:
    ConfigurationDoesNotExist:  Raised when an unavailable instrument setting
        is tried to be adjusted via `configure` or `configure` setting methods.

    CommunicationError:  Raised when an instrument related communication error
        happens.

TODO:
    - In VISAInstrument, the query binary and query ascii values should return
      the same type.

"""

import typing as t

import attr
import pyvisa as visa
import numpy as np
from functools import wraps

from groundcontrol.helper import StrEnum
from groundcontrol.logging import setup_logger


# Low-level logging for debugging
logger = setup_logger('instrument')


class ConfigurationDoesNotExist(Exception):
    pass


class Instrument:
    """An instrument as defined here is the building block for
    **communications** with physical instruments.  In that sense, the name
    `Instrument` is a misnomer.

    Every derived class is responsible for fitting it's instrument
    communications API into 3 classes of methods:
        1. Set methods:  These methods must start with the name `set_` and must
           be used for commands that alter the internal state of the
           instrument.
           Any Set method must have an accompanying Query style method.
        2. Query methods:  These methods start with the name `query_` and
           most of the time have a Set method counterpart.  Usually Query
           methods can be called with no arguments (i.e. keyword arguments
           should have defaults.)
        3. Do methods:  These methods initiate an operation in the instrument.
            These usually correspond to a non-blocking, long-running
            measurement sequences performed by the instrument.
    """
    def configure_setting(self, name, val):
        """Sets single setting of the instrument."""
        sname = "set_{nm}".format(nm=name)
        f = getattr(self, sname, None)
        if f is None:
            raise ConfigurationDoesNotExist(
                    f"No configuration with name {name}")
        else:
            if isinstance(val, dict):
                a = f(**val)
            elif isinstance(val, tuple):
                a = f(*val)
            else:
                a = f(val)
        return a

    def configure(self, config_dict):
        """Alternative way of configuration with a dictionary."""
        for k, v in config_dict.items():
            self.configure_setting(k, v)


class CommunicationError(Exception):
    pass


def _wrap_visa_io_error(fun):
    @wraps(fun)
    def decorated_fun(self, *args, **kwargs):
        try:
            return fun(self, *args, **kwargs)
        except visa.VisaIOError as e:
            # errcode = e.error_code
            abbreviation = e.abbreviation
            desc = e.description
            if abbreviation == "VI_ERROR_TMO":
                raise CommunicationError(desc) from e
            else:
                raise e
    return decorated_fun


@attr.s(auto_attribs=True)
class VISAInstrument(Instrument):
    """A base class for the instruments to derive from.

    Attributes:
        resource:  A visa.Resource instance.
        name:  Particular name for the instrument.

    """
    _resource: visa.Resource
    _name: str
    _string_quote_char: t.ClassVar[str] = '"'

    def _read_str(self, s: str):
        """This converts a received string to an internal python str.  The
        importance of this is that the strings returned from the instrument may
        have trailing quotes '"', so this removes them.
        """
        return s.strip(self._string_quote_char)

    @property
    def resource(self):
        return self._resource

    @property
    def name(self):
        return self._name

    def close(self):
        self.resource.close()

    def write(self, s: str):
        logger.debug(f"Write command: '{s}'")
        return self.resource.write(s)

    @_wrap_visa_io_error
    def read(self, *args, **kwargs):
        return self.resource.read(*args, **kwargs)

    @_wrap_visa_io_error
    def query(self, s: str) -> str:
        """Sends a command and reads the response as a string.  Strips off
        whitespaces and newlines.

        """
        logger.debug(f"Query command: '{s}'")
        return self.resource.query(s).strip()

    def query_float(self, s: str) -> float:
        """Queries a numeric value and returns as a float."""
        return float(self.query(s))

    def query_int(self, s: str) -> int:
        """Queries a numeric value and returns as a float."""
        return int(self.query(s))

    def query_str(self, s: str) -> int:
        """Queries a string value and returns it as a python string.

        This is important because instruments may enclose strings with
        quotation marks or other marks.  So this function ensures the removal
        of those.
        """
        q = self.query(s)
        return self._read_str(q)

    @_wrap_visa_io_error
    def query_binary_values(
            self,
            s,
            datatype='f',
            is_big_endian=False) -> np.ndarray:
        """Queries and reads binary values respecting the given arguments.
        Returns a numpy array with the datatype f: float32 or d:float64."""
        logger.debug(f"Query values command('{datatype}',{is_big_endian}): '{s}'")
        result = self.resource.query_binary_values(s, datatype, is_big_endian)
        return np.array(result)

    @_wrap_visa_io_error
    def query_ascii_values(
            self,
            s,
            converter='f',
            container=np.array,
            separator=',') -> np.ndarray:
        """Queries and reads the command output as ascii array, returns as a
        string.

        Args:
            s:  The command string.
            converter:  A valid Python string formatting code or a callable
                that takes a single argument.
            container:  A function that takes an iterable and returns a
                container object of desired type.
            separator:  The separator of values in the ascii data.
        """
        logger.debug(f"Query values command('{datatype}',{is_big_endian}): '{s}'")
        values = self.resource.query_ascii_values(
                s, converter=converter,
                container=container, separator=separator)
        return values

    def write_binary_values(
            self, message: str,
            values: t.Iterable,
            datatype: str,
            is_big_endian: bool = False):
        self.resource.write_binary_values(message, values, datatype, is_big_endian)

    def write_ascii_values(
            self, message: str,
            values: t.Iterable):
        self.resource.write_ascii_values(message, values)

    @property
    def timeout(self):
        """Returns timeout value in ms"""
        return self.resource.timeout

    @timeout.setter
    def timeout(self, value_ms: float):
        self.resource.timeout = value_ms

    @classmethod
    def create(cls, resource: visa.Resource, name: str):
        """Factory method to create instance.  This should be overriden by
        subclasses.
        """
        return cls(resource, name)

    @classmethod
    def from_resource(
            cls, resource: visa.Resource,
            name=None):
        """Creates instances using the given resource manager instance."""
        if name is None:
            name = resource._resource_name
        return cls.create(resource, name)


class SCPICommon:
    class State(StrEnum):
        ON = '1'
        OFF = '0'
        _ON_1 = "ON"
        _OFF_1 = "OFF"

    def do_cls(self):
        return self.write('*CLS')

    def query_idn(self) -> str:
        return self.query('*IDN?')

    def query_opt(self):
        return self.query('*OPT')

    def do_rst(self):
        return self.write('*RST')

    def do_trg(self):
        return self.write('*TRG')

    def query_stb(self):
        return self.query('*STB?')

    def query_opc(self) -> 'State':
        return self.State.from_str(self.query("*OPC?"))

    def do_opc(self):
        return self.write('*OPC')


@attr.s(auto_attribs=True)
class FakeVISAInstrument(Instrument):
    """A fake instrument class for debugging.  Not a complete solution, can be
    used for debugging if needed."""
    _resource: visa.Resource
    _name: str

    @property
    def resource(self):
        return self._resource

    @property
    def name(self):
        return self._name

    def close(self):
        self.resource.close()

    def write(self, s: str):
        return print(s)

    def read(self, *args, **kwargs):
        return print(f"Reading from instrument {self.name}.")

    def query(self, s: str):
        return print(s)

    def query_binary_values(
            self,
            s,
            datatype='f',
            is_big_endian=False) -> np.ndarray:
        """Queries and reads binary values respecting the given arguments.
        Returns a numpy array with the datatype f: float32 or d:float64."""
        print(f"Querying binary values:  datatype {datatype}, "
              f"isbigendian {is_big_endian}")
        print(s)

    def query_ascii_values(
            self,
            s,
            converter='f',
            container=np.array,
            separator=','):
        """Queries and reads the command output as ascii array, returns as a
        string.
        Args:
            s:  The command string.
            converter:  A valid Python string formatting code or a callable
                that takes a single argument.
            container:  A function that takes an iterable and returns a
                container object of desired type.
            separator:  The separator of values in the ascii data.
        """
        print(f"Querying ascii values")
        print(s)

    @property
    def timeout(self):
        """Returns timeout value in ms"""
        return self.resource.timeout

    @timeout.setter
    def timeout(self, value_ms: float):
        self.resource.timeout = value_ms

    @classmethod
    def from_resource(
            cls, resource_manager: visa.ResourceManager,
            resource_name, name=""):
        rsc = resource_manager.open_resource(resource_name)
        return cls(resource=rsc, name=name)
