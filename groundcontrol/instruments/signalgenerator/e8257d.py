"""Module for Keysight E8257D Signal Generator"""
import typing as t

import attr

from groundcontrol.helper import StrEnum
from . import SCPISignalGenerator


__all__ = ['KeysightE8257D']


@attr.s(auto_attribs=True)
class KeysightE8257D(SCPISignalGenerator):
    """Keysight E8257D Signal Generator"""
    pass
