"""Module for signal generators."""
import typing as t
from numbers import Number

import attr

from groundcontrol.helper import StrEnum
from groundcontrol.instruments import VISAInstrument


StrNum = t.Union[str, Number]


@attr.s(auto_attribs=True)
class SCPISignalGenerator(VISAInstrument):
    """Generic SCPI based signal generator class. """

    class State(StrEnum):
        ON = "1"
        OFF = "0"

    class ROscillatorSource(StrEnum):
        INTERNAL = "INT"
        EXTERNAL = "EXT"
        BBG = "BBG"

    def _query_enum(self, s: str, enumtype: StrEnum):
        """Shorthand for conversion of received strings to enumerations."""
        q = self.query(s)
        return enumtype.from_str(q)

    def _query_state(self, s: str):
        return self._query_enum(s, self.State)

    def query_idn(self):
        return self.query("*IDN?")

    def query_opc(self):
        return self.query("*OPC?")

    def do_rst(self):
        return self.write("*RST")

    def set_frequency(self, frequency: float):
        """Set frequency in Hz."""
        return self.write(f"FREQ {frequency}")

    def query_frequency(self) -> float:
        """Query frequency in Hz."""
        return self.query_float("FREQ?")

    def set_output_state(self, state: State):
        return self.write(f"OUTP:STAT {state}")

    def query_output_state(self) -> State:
        return self._query_state("OUTP:STAT?")

    def set_power(self, power: float):
        return self.write(f"POW {power}")

    def query_power(self) -> float:
        return self.query_float("POW?")

    def set_roscillator_source_auto(self, state: State):
        return self.write(f"ROSC:SOUR:AUTO {state}")

    def query_roscillator_source_auto(self) -> State:
        return self._query_state("ROSC:SOUR:AUTO?")

    def set_roscillator_source(self, roscsrc: ROscillatorSource):
        return self.write(f"ROSC:SOUR:AUTO {roscsrc}")

    def query_roscillator_source(self) -> ROscillatorSource:
        return self._query_enum("ROSC:SOUR?", self.ROscillatorSource)

