"""Module for Keysight E8267D Signal Generator"""
import typing as t

import attr

from groundcontrol.helper import StrEnum
from . import SCPISignalGenerator


__all__ = ["KeysightE8267D"]


State = SCPISignalGenerator.State


class MSUS(StrEnum):  # MassStorageUnitSpecifier
    WFM1 = "WFM1"
    MKR1 = "MKR1"
    HDR1 = "HDR1"
    NVWFM = "NVWFM"
    NVMKR = "NVMKR"
    NVHDR = "NVHDR"
    BIN = "BIN"
    BIT = "BIT"
    DMOD = "DMOD"
    FIR = "FIR"
    FSK = "FSK"
    IQ = "I/Q"
    LIST = "LIST"
    MDMOD = "MDMOD"
    MTONE = "MTONE"
    SEQ = "SEQ"
    SHAPE = "SHAPE"
    STATE = "STATE"
    USERFLAT = "USERFLAT"


@attr.frozen
class FileListing:
    filename: str
    filetype: MSUS
    filesize: int


@attr.s(auto_attribs=True)
class KeysightE8267D(SCPISignalGenerator):
    """Keysight E8267D Signal Generator"""

    MSUS = MSUS

    class ArbTrigType(StrEnum):
        CONTINUOUS = "CONT"
        SINGLE = "SING"
        GATE = "GATE"
        SADVANCE = "SADV"

    class ContTrigType(StrEnum):
        FREE = "FREE"
        TRIGGER = "TRIGGER"
        RESET = "RESET"

    def set_mmem_data(
        self,
        msus: MSUS,
        filename: str,
        data: t.Iterable,
        datatype: str = "h",
    ):
        return self.write_binary_values(
            f'MMEM:DATA "{msus}:{filename}",',
            data,
            datatype=datatype,
            is_big_endian=True,
        )

    def do_mmem_delete(self, msus: MSUS, filename: str):
        return self.write(f'MMEM:DEL "{filename}",":{msus}"')

    def set_mmem_header_description(self, filename: str, description: str):
        return self.write(f'MMEM:HEAD:DESC "{filename}","{description}"')

    def query_mmem_header_description(self, filename: str):
        return self.query_str(f'MMEM:HEAD:DESC? "{filename}"')

    def query_mmem_catalog(
        self, msus: MSUS
    ) -> t.Tuple[int, int, t.Tuple[FileListing, ...]]:
        q = self.query(f'MMEM:CAT? "{msus}"')
        flds = q.split(",", maxsplit=2)
        used, free = (int(s) for s in flds[:2])
        try:
            flistings_ = self._read_str(flds[2]).split('","')
        except IndexError:
            return used, free, ()
        flistings = tuple(
            (
                FileListing(fname, MSUS(ftyp), int(fsize))
                for fname, ftyp, fsize in map(lambda s: s.split(","), flistings_)
            )
        )
        return used, free, flistings

    def set_output_modulation_state(self, state: State):
        return self.write(f"OUTP:MOD {state}")

    def query_output_modulation_state(self) -> State:
        return self._query_state("OUTP:MOD?")

    def set_radio_arb_baseband_frequency_offset(self, value: float):
        return self.write(f"RAD:ARB:BAS:FREQ:OFFS {value}")

    def query_radio_arb_baseband_frequency_offset(self) -> float:
        return self.query_float("RAD:ARB:BAS:FREQ:OFFS?")

    def set_radio_arb_filter_state(self, state: State):
        return self.write(f"RAD:ARB:FILT {state}")

    def query_radio_arb_filter_state(self) -> State:
        return self._query_state("RAD:ARB:FILT?")

    def do_radio_arb_header_clear(self):
        """Clears the header information related to current modulation format."""
        return self.write("RAD:ARB:HEAD:CLE")

    def set_radio_arb_header_noise_rms(
        self, msus: MSUS, filename: str, value: t.Optional[float]
    ):
        """Value must be between 0 and sqrt(2)."""
        if value is None:
            value = "UNSP"
        self.write(f'RAD:ARB:HEAD:NOIS:RMS "{msus}:{filename}",{value}')

    def query_radio_arb_header_noise_rms(self, msus: MSUS, filename: str):
        try:
            value = self.query_float(f'RAD:ARB:HEAD:NOIS:RMS "{msus}:{filename}"?')
        except ValueError:
            # Returns UNSPECIFIED instead of a float value
            value = None
        return value

    def set_radio_arb_header_rms(
        self, msus: MSUS, filename: str, value: t.Optional[float]
    ):
        """Value must be between 0 and sqrt(2)."""
        if value is None:
            value = "UNSP"
        self.write(f'RAD:ARB:HEAD:RMS "{msus}:{filename}",{value}')

    def query_radio_arb_header_rms(self, msus: MSUS, filename: str):
        try:
            value = self.query_float(f'RAD:ARB:HEAD:RMS "{msus}:{filename}"?')
        except ValueError:
            # Returns UNSPECIFIED instead of a float value
            value = None
        return value

    def do_radio_arb_header_save(self):
        """Save the header information to the header file used by this modulation
        format.
        """
        return self.write("RAD:ARB:HEAD:SAVE")

    def set_radio_arb_rscaling(self, val: float):
        """Sets runtime scaling as a percentage.  val is in between 1 and 100."""
        return self.write(f"RAD:ARB:RSC {val}")

    def query_radio_arb_rscaling(self) -> float:
        return self.query_float("RAD:ARB:RSC?")

    def set_radio_arb_sclock_rate(self, value_hz: float):
        return self.write(f"RAD:ARB:SCL:RATE {value_hz}")

    def query_radio_arb_sclock_rate(self) -> float:
        return self.query_float("RAD:ARB:SCL:RATE?")

    def set_radio_arb_trigger_type(self, trigtype: ArbTrigType):
        return self.write(f"RAD:ARB:TRIG:TYPE {trigtype}")

    def query_radio_arb_trigger_type(self) -> ArbTrigType:
        return self._query_enum("RAD:ARB:TRIG:TYPE?", self.ArbTrigType)

    def set_radio_arb_trigger_type_continuous(self, conttype: ContTrigType):
        return self.write(f"RAD:ARB:TRIG:TYPE:CONT {conttype}")

    def query_radio_arb_trigger_type_continuous(self):
        return self._query_enum("RAD:ARB:TRIG:TYPE:CONT?", self.ContTrigType)

    def set_radio_arb_waveform(self, filename: str, issequence: bool = False):
        if issequence:
            msus = MSUS.SEQ
        else:
            msus = MSUS.WFM1
        return self.write(f'RAD:ARB:WAV "{msus}:{filename}"')

    def query_radio_arb_waveform(self) -> t.Tuple[str, bool]:
        """Returns a tuple of the form (filename, issequence)."""
        msusstr, fname = self.query_str("RAD:ARB:WAV?").split(":")
        msus = MSUS(msusstr)
        return (fname, msus is MSUS.SEQ)

    def set_radio_arb_state(self, state: State):
        return self.write(f"RAD:ARB {state}")

    def query_radio_arb_state(self) -> State:
        return self._query_state("RAD:ARB?")
