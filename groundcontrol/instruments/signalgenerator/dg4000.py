"""Module for Rigol DG4000 signal generators."""

import typing as t
from numbers import Number
from enum import IntFlag

import attr
import numpy as np

from groundcontrol.helper import StrEnum
from groundcontrol.instruments import VISAInstrument, SCPICommon
from groundcontrol.instruments._common import optarg, optargs


State = SCPICommon.State


class Waveform(StrEnum):
    CUSTOM = "CUST"
    HARMONIC = "HARM"
    NOISE = "NOIS"
    PULSE = "PULS"
    RAMP = "RAMP"
    SINUSOID = "SIN"
    SQUARE = "SQU"
    USER = "USER"


@attr.s(auto_attribs=True, frozen=True)
class ApplyQuery:
    waveform: Waveform
    freq: t.Optional[float]
    amp: t.Optional[float]
    offset: t.Optional[float]
    phase: t.Optional[float]
    delay: t.Optional[float]

    @staticmethod
    def _noneorfloat(s: str):
        if s == 'DEF':
            return None
        else:
            return float(s)

    @classmethod
    def from_str(cls, s: str):
        converters = [
                Waveform.from_str,
                cls._noneorfloat,
                cls._noneorfloat,
                cls._noneorfloat,
                cls._noneorfloat,
                ]
        wf, freq, amp, offs, phordel = (
                convf(arg)
                for convf, arg in
                zip(converters, s.split(','))
                )
        if wf is Waveform.PULSE:
            return cls(wf, freq, amp, offs, phase=None, delay=phordel)
        else:
            return cls(wf, freq, amp, offs, phase=phordel, delay=None)


class RigolDG4000(SCPICommon, VISAInstrument):
    class VoltageUnit(StrEnum):
        VPP = "VPP"
        VRMS = "VRMS"
        DBM = "DBM"

    class Marginal(StrEnum):
        MINIMUM = "MIN"
        MAXIMUM = "MAX"

    class DataFlag(StrEnum):
        CON = "CON"
        END = "END"

    class Interpolate(StrEnum):
        LINEAR = "LINEAR"
        OFF = "OFF"

    def _query_enum(self, s: str, enumtype: StrEnum):
        """Shorthand for conversion of received strings to enumerations."""
        q = self.query(s)
        return enumtype.from_str(q)

    def _query_state(self, s: str):
        return self._query_enum(s, self.State)

    def set_output_state(self, state: State, cnum: int = 1):
        return self.write(f"OUTP{cnum:d} {state}")

    def query_output_state(self, cnum: int = 1) -> State:
        return self._query_state(f"OUTP{cnum:d}:STAT?")

    def _optcnum(
            self,
            cmd: str,
            cnum: t.Optional[int],
            base: str):
        if cnum is not None:
            cmd = f"{base}{cnum:d}:{cmd}"
        return cmd

    def _optsrccnum(
            self, cmd: str, cnum: t.Optional[int]):
        return self._optcnum(cmd, cnum, base='SOUR')

    def set_voltage_unit(
            self,
            unit: VoltageUnit,
            cnum: t.Optional[int] = None):
        cmd = self._optsrccnum(f"VOLT:UNIT {unit}", cnum)
        return self.write(cmd)

    def query_voltage_unit(
            self,
            cnum: t.Optional[int] = None) -> VoltageUnit:
        cmd = self._optsrccnum("VOLT:UNIT?", cnum)
        return self._query_enum(cmd, self.VoltageUnit)

    def set_frequency(
            self,
            freq: t.Union[float, Marginal],
            cnum: t.Optional[int] = None):
        cmd = self._optsrccnum(f"FREQ {freq}", cnum)
        return self.write(cmd)

    def query_frequency(
            self,
            marginal: t.Optional[Marginal] = None,
            cnum: t.Optional[int] = None) -> float:
        cmd = self._optsrccnum(f"FREQ?{optarg(marginal, False)}")
        return self.query_float(cmd)

    def set_apply_noise(
            self,
            amp: t.Optional[float] = None,
            offset: t.Optional[float] = None,
            cnum: t.Optional[int] = None):
        _cmd = f"APPL:NOIS{optargs([amp, offset])}"
        cmd = self._optsrccnum(_cmd, cnum)
        return self.write(cmd)

    def set_apply_sinusoid(
            self,
            freq: t.Optional[float] = None,
            amp: t.Optional[float] = None,
            offset: t.Optional[float] = None,
            phase: t.Optional[float] = None,
            cnum: t.Optional[int] = None):
        _cmd = f"APPL:SIN{optargs([freq, amp, offset, phase])}"
        cmd = self._optsrccnum(_cmd, cnum)
        return self.write(cmd)

    def query_apply(self, cnum: t.Optional[int] = None) -> ApplyQuery:
        cmd = self._optsrccnum("APPL?", cnum)
        return ApplyQuery.from_str(self.query_str(cmd))

    def set_trace_data_dac16(self, flag: DataFlag, arr: np.ndarray):
        """Data must be representable as a 2-byte integer."""
        return self.write_binary_values(f":DATA:DAC16 VOLATILE,{flag},", arr, datatype='h',)

    def set_trace_data_dac(self, arr: np.ndarray):
        """Data must be representable as a 2-byte integer."""
        return self.write_binary_values(":DATA:DAC VOLATILE,", arr, datatype='h')

    def set_trace_data(self, arr: np.ndarray):
        """Data must be a float between -1 to 1."""
        return self.write_ascii_values(":DATA VOLATILE,", arr)

    def set_data_points_interpolate(self, interp: Interpolate):
        return self.write(f"DATA:POIN:INT {interp}")

    def query_data_points_interpolate(self) -> Interpolate:
        return self._query_enum("DATA:POIN:INT?", self.Interpolate)

    def set_data_points(self, value: t.Union[int, Marginal]):
        return self.write(f"DATA:POIN VOLATILE,{value}")

    def query_data_points(self, marginal: t.Optional[Marginal]) -> int:
        cmd = self._optsrccnum(f"DATA:POIN? VOLATILE,{optarg(marginal, False)}")
        return self.query_int(cmd)

    def query_data_value(self, point: int) -> int:
        return self.query_int(f"DATA:VAL? VOLATILE,{point}")

    def set_data_value(self, point: int, data: int):
        return self.write(f"DATA:VAL VOLATILE,{point},{data}")

    def query_data_load(self, num: t.Optional[int] = None):
        if num is None:
            return self.query("DATA:LOAD? VOLATILE")
        else:
            return self.query(f"DATA:LOAD? {num}")

