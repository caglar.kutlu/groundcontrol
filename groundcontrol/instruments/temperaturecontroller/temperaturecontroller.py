"""Module for temperature controllers.  Currently only has LakeShore specific
functionality.

Note:
    - Appart from Channel, I used EnumStr to make it easier to use the
      enumeration in strings without having to call `obj.value`.  However, it's
      better to use Enum and somehow change the representation instead, due to
      type safety considerations.
"""
import typing as t
from numbers import Number
from enum import IntFlag


from groundcontrol.helper import StrEnum
from groundcontrol.instruments import VISAInstrument


def _optargs(*args: str) -> str:
    return ",".join(filter(lambda arg: arg is not None, args))


def _maybefun(func, arg: t.Optional[t.Any]):
    val = func(arg) if arg is not None else arg
    return val


class LS372TemperatureController(VISAInstrument):
    class Channel(StrEnum):
        # todo: use just Enum and integer literals for channels
        CA = 'A'
        C1 = '1'
        C2 = '2'
        C3 = '3'
        C4 = '4'
        C5 = '5'
        C6 = '6'
        C7 = '7'
        C8 = '8'
        C9 = '9'
        C10 = '10'
        C11 = '11'
        C12 = '12'
        C13 = '13'
        C14 = '14'
        C15 = '15'
        C16 = '16'

        @staticmethod
        def parse(value):
            if isinstance(value, Number):
                return f"{value:.0f}"
            else:
                return value

    class HeaterType(StrEnum):
        SAMPLE = '0'
        WARMUP = '1'
        STILL = '2'

    class HeaterOutDisplayType(StrEnum):
        CURRENT = '1'
        POWER = '2'

    class State(StrEnum):
        """State enum"""
        ON = '1'
        OFF = '0'

        @classmethod
        def from_bool(cls, val: bool):
            ret = cls.ON if val else cls.OFF
            return ret

        def asbool(self):
            return self == self.ON

    class MaxWarmupCurrent(StrEnum):
        CUSER = '0'
        C0A45 = '1'
        C0A63 = '2'

    class ExcitationFrequency(StrEnum):
        F9H8 = '1'  # 9.8 Hz
        F13H7 = '2'  # 13.7 Hz
        F16H2 = '3'  # 16.2 Hz
        F11H6 = '4'  # 11.6 Hz
        F18H2 = '5'  # 18.2 Hz

        def asfloat(self):
            return self._d[self.name]

    ExcitationFrequency._d = {
                'F9H8': 9.8,
                'F13H7': 13.7,
                'F16H2': 16.2,
                'F11H6': 11.6,
                'F18H2': 18.2
                }

    class WarmupResistance(StrEnum):
        R25 = '1'
        R50 = '2'

    class HeaterStatus(StrEnum):
        NOERROR = '0'
        HEATEROPEN = '1'
        HEATERSHORT = '2'
        VOLTCOMPL = '3'  # Voltage compliance

    class TempCo(StrEnum):
        NEGATIVE = '1'
        POSITIVE = '2'

    class ExcitationMode(StrEnum):
        VOLTAGE = '0'
        CURRENT = '1'

    class CurrentExcitation(StrEnum):
        C1p00 = '01'
        C3p16 = '02'
        C10p0 = '03'
        C31p6 = '04'
        C100p = '05'
        C316p = '06'
        C1n00 = '07'
        C3n16 = '08'
        C10n0 = '09'
        C31n6 = '10'
        C100n = '11'
        C316n = '12'
        C1u00 = '13'
        C3u16 = '14'
        C10u0 = '15'
        C31u6 = '16'
        C100u = '17'
        C316u = '18'
        C1m00 = '19'
        C3m16 = '20'
        C10m0 = '21'
        C31m6 = '22'

        def asfloat(self):
            return self._d[self.name]

    CurrentExcitation._d = {
            'C1p00': 1e-12,
            'C3p16': 3.16e-12,
            'C10p0': 10e-12,
            'C31p6': 31.6e-12,
            'C100p': 100e-12,
            'C316p': 316e-12,
            'C1n00': 1e-9,
            'C3n16': 3.16e-9,
            'C10n0': 10e-9,
            'C31n6': 31.6e-9,
            'C100n': 100e-9,
            'C316n': 316e-9,
            'C1u00': 1e-6,
            'C3u16': 3.16e-6,
            'C10u0': 10e-6,
            'C31u6': 31.6e-6,
            'C100u': 100e-6,
            'C316u': 316e-6,
            'C1m00': 1e-3,
            'C3m16': 3.16e-3,
            'C10m0': 10e-3,
            'C31m6': 31.6e-3
            }

    class VoltageExcitation(StrEnum):
        V2u00 = '01'
        V6u32 = '02'
        V20u0 = '03'
        V63u2 = '04'
        V200u = '05'
        V632u = '06'
        V2m0 = '07'
        V6m32 = '08'
        V20m0 = '09'
        V63m2 = '10'
        V200m = '11'
        V632m = '12'

        def asfloat(self):
            return self._d[self.name]

    VoltageExcitation._d = {
                'V2u00': 2e-6,
                'V6u32': 6.32e-6,
                'V20u0': 20e-6,
                'V63u2': 63.2e-6,
                'V200u': 200e-6,
                'V632u': 632e-6,
                'V2m0': 2e-3,
                'V6m32': 6.32e-3,
                'V20m0': 20e-3,
                'V63m2': 63.2e-3,
                'V200m': 200e-3,
                'V632m': 632e-3
                }

    class ResistanceRange(StrEnum):
        R2m0 = '01'
        R6m32 = '02'
        R20m0 = '03'
        R63m2 = '04'
        R200m = '05'
        R632m = '06'
        R2R00 = '07'
        R6R32 = '08'
        R20R0 = '09'
        R63R2 = '10'
        R200R = '11'
        R632R = '12'
        R2k00 = '13'
        R6k32 = '14'
        R20k0 = '15'
        R63k2 = '16'
        R200k = '17'
        R632k = '18'
        R2M00 = '19'
        R6M32 = '20'
        R20M0 = '21'
        R63M2 = '22'

    class CSShunt(StrEnum):
        SHUNTED = '1'  # Excitation off
        NOTSHUNTED = '0'  # Excitation on

    class AutorangeType(StrEnum):
        OFF = '0'
        CURRENT = '1'
        ROX102B = '2'

    class Units(StrEnum):
        KELVIN = '1'
        OHMS = '2'

    class OutputMode(StrEnum):
        OFF = 'O'  # the documentation has O not 0
        MONITOR = '1'
        OPENLOOP = '2'
        ZONE = '3'
        STILL = '4'
        CLOSEDLOOP = '5'
        WARMUP = '6'

    class OutputPolarity(StrEnum):
        UNIPOLAR = '0'
        BIPOLAR = '1'

    class RampStatus(StrEnum):
        NORAMPING = '0'
        RAMPING = '1'

    class ReadingSettle(StrEnum):
        VALID = '0'
        FW_FILT_SETTLE = '1'  # Firmware filter settling
        HW_SETTLE = '2'  # Hardware settling

    class HeaterRange(StrEnum):
        OFF = '0'
        C31u6 = '1'
        C100u = '2'
        C316u = '3'
        C1m00 = '4'
        C3m16 = '5'
        C10m0 = '6'
        C31m6 = '7'
        C100m = '8'

        def asfloat(self):
            """Returns the current in ampere."""
            d = {
                    self.OFF: 0,
                    self.C31u6: 31.6e-6,
                    self.C100u: 100e-6,
                    self.C316u: 316e-6,
                    self.C1m00: 1e-3,
                    self.C3m16: 3.16e-3,
                    self.C10m0: 10e-3,
                    self.C31m6: 31.6e-3,
                    self.C100m: 100e-3
                    }
            return d[self]

    class ReadingStatus(IntFlag):
        VALID = 0
        CS_OVL = 1
        VCM_OVL = 2
        VMIX_OVL = 4
        VDIF_OVL = 8
        R_OVER = 16
        R_UNDER = 32
        T_OVER = 64
        T_UNDER = 128

    def query_idn(self):
        return self.query("*IDN?")

    def query_opc(self):
        return self.query("*OPC?")

    def set_filter(
            self,
            channel: Channel,
            state: State,
            settle_time: t.Optional[int] = None,
            window: t.Optional[int] = None):
        """Specifies filtering for a channel."""
        if settle_time is None and window is not None:
            # ignores if you supply window when settle_time is not supplied.
            window = None
        optargs = _optargs(_maybefun(str, settle_time), _maybefun(str, window))
        comma = "," if optargs != "" else ""
        args = [f"{channel:s},{state:s}{comma}{optargs}"]
        return self.write(f"FILTER {args}")

    def query_filter(self, channel: Channel):
        """Returns:
            state:
            settle: Specifies filter settle time: 1 to 200 s.
            window: Specifies what percent of full scale reading limits the
            filtering function.  Reading changes greater than this percentage
            reset the filter: 1 to 80%.
        """
        retval = self.query(f"FILTER? {channel}")
        state, settle, window = retval.strip().split(',')
        state = self.State(state)

        return state, float(settle), float(window)

    def set_excitation_frequency(
            self, channel: Channel, freq: ExcitationFrequency):
        self.write(f"FREQ {channel:s},{freq:s}")

    def query_excitation_frequency(self, channel: Channel):
        cls = self.ExcitationFrequency
        return cls(self.query(f"FREQ? {channel:s}").strip())

    def query_heater_output(self):
        """Returns the heater output as percentage of current or power. """
        return float(self.query("HTR?").strip())

    def set_heater_setup(
            self, which: HeaterType,
            resistance: t.Union[float, WarmupResistance],
            max_warmup_current: MaxWarmupCurrent = MaxWarmupCurrent.CUSER,
            max_warmup_current_user: float = 0,
            out_display: HeaterOutDisplayType = HeaterOutDisplayType.CURRENT):
        self.write(f"HTRSET{which},{resistance},"
                   f"{max_warmup_current},{max_warmup_current_user},"
                   f"{out_display}")

    def query_heater_setup(self, which: HeaterType):
        retval = self.query(f"HTRSET?{which}")
        res, maxcur, maxusercur, dispout = retval.strip().split(',')
        if which in (self.HeaterType.SAMPLE, self.HeaterType.STILL):
            res = float(res)
            maxcur = float(maxcur)
        else:
            res = self.WarmupResistance(res)
            maxcur = self.MaxWarmupCurrent(maxcur)

        maxusercur = float(maxusercur)
        dispout = self.HeaterOutDisplayType(dispout)
        return res, maxcur, maxusercur, dispout

    def query_heater_status(self, which: HeaterType):
        return self.HeaterStatus(self.query(f"HTRST? {which}").strip())

    def set_input_parameter(
            self, channel: Channel, state: State,
            dwell: t.Optional[int] = None,
            pause: t.Optional[int] = None,
            curve_number: t.Optional[int] = None,
            tempco: t.Optional[TempCo] = None):
        optargs = _optargs(
                _maybefun(str, dwell),
                _maybefun(str, pause),
                _maybefun(str, curve_number),
                tempco)
        comma = "," if optargs != "" else ""
        self.write(f"INSET {channel},{state}{comma}{optargs}")

    def query_input_parameter(self, channel: Channel):
        """Returns:
            state: Input activation status.
            dwell: Dwell time in seconds.
            pause: Change pause time in seconds.
            cno: Curve number.
            tempco: Temperature coefficient.
        """
        q = self.query(f"INSET? {channel:s}").strip()
        state, dwell, pause, cno, tempco = q.split(',')
        state = self.State(state)
        dwell = int(dwell)
        pause = int(pause)
        cno = int(cno)
        tempco = self.TempCo(tempco)
        return state, dwell, pause, cno, tempco

    def set_input_setup(
            self, channel: Channel, mode: ExcitationMode,
            excitation: t.Union[VoltageExcitation, CurrentExcitation], 
            autorange: AutorangeType, range: ResistanceRange, csshunt: CSShunt,
            units: Units):
        return self.write(f"INTYPE {channel},{mode},"
                          f"{excitation},{autorange},{range},"
                          f"{csshunt},{units}")

    def query_input_setup(
            self, channel: Channel):
        """Returns:
            excitation_mode
            excitation
            autorange
            range
            csshunt
            units
        """
            
        q = self.query(f"INTYPE? {channel}").strip()
        mode, excitation, autorange, range, csshunt, units = q.split(',')
        mode = self.ExcitationMode(mode)
        if mode is self.ExcitationMode.VOLTAGE:
            excitationcls = self.VoltageExcitation
        else:
            excitationcls = self.CurrentExcitation

        excitation = excitationcls(excitation)
        autorange = self.AutorangeType(autorange)
        range = self.ResistanceRange(range)
        csshunt = self.CSShunt(csshunt)
        units = self.Units(units)
        return mode, excitation, autorange, range, csshunt, units

    def query_kelvin_reading(self, channel: Channel) -> float:
        return float(self.query(f"KRDG?{channel}").strip())

    def set_manual_heater_out(self, out: HeaterType, value: float):
        self.write(f"MOUT {out},{value:3.2e}")

    def query_manual_heater_out(self, out: HeaterType = HeaterType.SAMPLE):
        return float(self.query(f"MOUT? {out}"))

    def set_output_mode(
            self,
            out: HeaterType,
            mode: OutputMode,
            channel: Channel,
            powerup_en: State,
            polarity: OutputPolarity,
            filter: State,
            delay: int):
        self.write(f"OUTMODE {out},{mode},{channel},{powerup_en},{polarity}"
                   f",{filter},{delay:d}")

    def query_output_mode(self, out: HeaterType):
        q = self.query(f"OUTMODE? {out}").strip()
        mode, channel, powerup_en, polarity, filter, delay = q.split(',')
        mode = self.OutputMode(mode)
        channel = self.Channel(channel)
        powerup_en = self.State(powerup_en)
        polarity = self.OutputPolarity(polarity)
        filter = int(filter)
        delay = int(delay)
        return mode, channel, powerup_en, polarity, filter, delay

    def set_pid_parameters(
            self, out: HeaterType, p: float, i: float, d: float):
        self.write(f"PID {out},{p},{i},{d}")

    def query_pid_parameters(self, out: HeaterType):
        q = self.query(f"PID? {out}").strip()
        p, i, d = map(float, q.split(','))
        return p, i, d

    def set_ramp(self, out: HeaterType, state: State, rate: float):
        self.write(f"RAMP {out},{state},{rate}")

    def query_ramp(self, out: HeaterType):
        q = self.query(f"RAMP? {out}").strip()
        state, rate = q.split(',')
        state = self.State(state)
        rate = float(rate)
        return state, rate

    def query_ramp_status(self, out: HeaterType = HeaterType.SAMPLE):
        q = self.query(f"RAMPST? {out}").strip()
        return self.RampStatus(q)

    def set_heater_range(self, out: HeaterType, current_range: HeaterRange):
        self.write(f"RANGE {out},{current_range}")

    def query_heater_range(self, out: HeaterType):
        q = self.query(f"RANGE? {out}").strip()
        return self.HeaterRange(q)

    def query_excitation_power(self, ch: Channel):
        return self.query_float(f'RDGPWR? {ch}')

    def query_resistance_reading(self, channel: Channel):
        return float(self.query(f"RDGR?{channel}").strip())

    def query_quadrature_reading(self, channel: Channel):
        return float(self.query(f"QRDG?{channel}").strip())

    def query_input_reading_status(self, channel: Channel) -> "ReadingStatus":
        qi = self.query_int(f"RDGST? {channel}")
        rse = self.ReadingStatus(qi)
        return rse

    def query_reading_settle(self):
        q = self.query("RDGSTL?").strip()
        control, inputch = q.split(',')
        return self.ReadingSettle(control), self.ReadingSettle(inputch)

    def set_scanner_parameter(
            self, channel: Channel,
            autoscan: State = State.OFF):
        self.write(f"SCAN{channel},{autoscan}")

    def query_scanner_parameter(self):
        q = self.query("SCAN?").strip()
        chstr, autoscan = q.split(',')
        ch = self.Channel(chstr.lstrip('0'))
        return ch, self.State(autoscan)

    def set_temperature_setpoint(
            self, value: float, out: HeaterType = HeaterType.SAMPLE):
        self.write(f"SETP {out},{value:+3.3E}")

    def query_temperature_setpoint(
            self, out: HeaterType = HeaterType.SAMPLE):
        return float(self.query(f"SETP? {out}").strip())

    def set_control_zone_parameters(
            self,
            out: HeaterType,
            zone: int,
            upper_bound: float,
            pvalue: float,
            ivalue: float,
            dvalue: float,
            manual_out: float,
            heater_range: HeaterRange,
            ramp_rate: float,
            relay1: State,
            relay2: State):
        command = ("ZONE "
            f"{out:s},"
            f"{zone:d},"
            f"{upper_bound:+3.3E},"
            f"{pvalue:f},"
            f"{ivalue:f},"
            f"{dvalue:f},"
            f"{manual_out:f},"
            f"{heater_range:s},"
            f"{ramp_rate:f},"
            f"{relay1:s},"
            f"{relay2:s}")
        self.write(command)

    def query_control_zone_parameters(
            self,
            out: HeaterType,
            zone: int):
        q = self.query(f"ZONE? {out:s},{zone:d}").strip()
        upper_bound, pvalue, ivalue, dvalue, mout, hrange, ramprate, relay1, relay2 = q.split(',')
        upper_bound = float(upper_bound)
        pvalue, ivalue, dvalue = map(float, (pvalue, ivalue, dvalue))
        mout = float(mout)
        hrange = self.HeaterRange(hrange)
        ramprate = float(ramprate)
        relay1 = self.State(relay1)
        relay2 = self.State(relay2)
        return upper_bound, pvalue, ivalue, dvalue, mout, hrange, ramprate, relay1, relay2

    def query_min_max(self, ch: Channel) -> t.Tuple[float, float]:
        """Returns:
            min:  Minimum value.
            max:  Maximum value.
        """
        q = self.query(f"MDAT? {ch}")
        dmin, dmax = map(float, q.split(','))
        return dmin, dmax

    def do_reset_min_max(self):
        """Resets the minimum and maximum data for all channels. Active scan
        channel will reset immediately, while all other channels will reset
        with the first valid reading they receive.
        """
        return self.write("MNMXRST")

    @classmethod
    def create(cls, resource, name):
        if resource.resource_class == 'SOCKET':
            resource.write_termination = '\n'
            resource.read_termination = '\r\n'
        return cls(resource, name)
