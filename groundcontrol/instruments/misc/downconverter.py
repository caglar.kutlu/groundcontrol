from groundcontrol.instruments.spectrumanalyzer.thinkrf import (
        R5550SCPI_WORKAROUND)
from groundcontrol.controller import Controller, instrument
from groundcontrol.instruments.signalgenerator import SCPISignalGenerator
from groundcontrol.declarators import declarative


class ThinkRFAsDownconverterController(Controller):
    """Controller for downconverters.

    This is actually an interface.  Only implementation is via ThinkRF as
    downconverter, so the implementation is the interface.
    """
    trf: R5550SCPI_WORKAROUND = instrument()
    _ref_ext: bool = True

    @property
    def ref_ext(self):
        return self._ref_ext

    @property
    def attenuations(self):
        return tuple(map(int, self.trf.Attenuator.__members__.values()))

    def __setup__(self):
        trf = self.trf
        trf.set_input_mode(trf.InputMode.ZIF)
        trf.set_output_mode(trf.OutputMode.CONNECTOR)
        refclk = (trf.RefClock.EXTERNAL
                  if self.ref_ext
                  else trf.RefClock.INTERNAL)
        trf.set_source_reference_pll(refclk)

    def query_is_reference_locked(self):
        trf = self.trf
        return trf.query_sense_lock_reference() == trf.LockState.LOCKED

    def query_is_rf_locked(self):
        return self.trf.query_sense_lock_rf() == self.trf.LockState.LOCKED

    def set_attenuator(self, db: float):
        # db = int(db)
        allowed = self.attenuations
        if db not in allowed:
            raise ValueError(
                    f"Can't set {db} dB.  Allowed values are {allowed}.")
        self.trf.set_input_attenuator(int(db))

    def query_attenuator(self) -> float:
        val = self.trf.query_input_attenuator().value
        return float(val)

    def set_center_frequency(self, hz):
        self.trf.set_sense_frequency_center(hz)

    def query_center_frequency(self):
        return self.trf.query_sense_frequency_center()


@declarative
class SGDownconverterController:
    """Controller for downconversion using a single signal-generator and a mixer.

    """
    sg: SCPISignalGenerator = instrument()
    _ref_ext: bool = True

    @property
    def ref_ext(self):
        raise NotImplementedError

    @property
    def attenuations(self):
        raise NotImplementedError

    def __setup__(self):
        pass

    def initiate(self):
        pass

    def query_is_reference_locked(self):
        raise NotImplementedError

    def query_is_rf_locked(self):
        raise NotImplementedError

    def set_attenuator(self, db: float):
        raise NotImplementedError

    def query_attenuator(self) -> float:
        raise NotImplementedError

    def set_center_frequency(self, hz):
        """This is the LO frequency."""
        self.sg.set_frequency(hz)

    def query_center_frequency(self):
        return self.sg.query_frequency()
