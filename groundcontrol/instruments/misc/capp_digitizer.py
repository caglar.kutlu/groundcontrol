import signal
import socket
import fcntl
import struct
import importlib
from enum import Enum
from contextlib import contextmanager
from pathlib import Path
import typing as t

import numpy as np
from numpy.polynomial import Polynomial
import attr

from groundcontrol.logging import logger
from groundcontrol.util import watt2dbm, dbm2watt


# configuration for the digitizer in bf6
_conf_bf6 = {
    "interface": "enp5s0",
    "mac_address": "01:23:45:67:89:ab",
    "n_bins": 5001,
    "binwidth": 100,
    "fcenter": 10.7e6,
}


@attr.s(auto_attribs=True)
class MJCal:
    """A structure to hold simple polynomial coefficients provided by MJLee's
    calibration txt files.

    If a polynomial p(x) is defined as:
        p(x) = a0 + a1*x * a2*x^2 + ...

    The ordered set of coefficients [a0, a1, a2, ...] is said to be in the
    order of increasing degree.

    The original MJ file lists the polynomials in this order, which is also
    compatible with numpy.polynomial module.

    Note:
        domain variable is not related to numpy.Polynomial's domain argument.

    """
    _coeffs: t.Dict[int, t.Tuple[float]]
    domain: t.Tuple[float, float]

    def coeffs(self, polyorder: int, increasing=True):
        if increasing:
            return self._coeffs[polyorder]
        else:
            return self._coeffs[polyorder][::-1]

    def aspolynomial(
            self, polyorder: int) -> Polynomial:
        """This returns a polynomial that takes log10 scaled FFT outputs and
        transforms them into power values in dBm.

        """
        return Polynomial(
                self.coeffs(polyorder))

    def polyeval(self, digraw, order=5):
        # This is ~1.5 times faster than Polynomial.__call__
        # ~132 us on average for 5001 length data
        p = self.aspolynomial(5).coef[::-1]
        y = np.zeros(digraw.shape, dtype=float)
        for i, v in enumerate(p):
            y *= digraw
            y += v
        return y

    def to_linear_polynomial(
            self, polyorder: int,
            linpolyorder: int,
            npoints: int = 1000):
        """This generates linear polynomial coefficients using the logarithmic
        coefficients and the `self.domain`.  The returning polynomial provides
        a mapping from linear scale FFT values to a power values in units of
        WATT.

        The domain is sampled equidistantly with `npoints` and a polynomial fit
        is performed with the degree `linpolyorder`.

        DISCLAIMER:
            - This does not work with large logarithmic domain like 4, 14.

        Args:
            polyorder:  Calibration order to use.
            linpolyorder:  Desired polynomial order of the linear fit.
            npoints:  Number of points to sample the domain with.

        Returns:
            poly:  `numpy.polynomial.Polynomial` of degree `linpolyorder`.
        """

        l10dom = np.linspace(*self.domain, npoints)
        dom = 10**l10dom

        l10poly = self.aspolynomial(polyorder)
        ydbm = l10poly(l10dom)

        yw = dbm2watt(ydbm)

        poly = Polynomial.fit(x=dom, y=yw, deg=linpolyorder)
        return poly

    @classmethod
    def from_path(
            cls, path: t.Union[str, Path],
            log10fftdomain: t.Tuple[float, float] = (4, 15)):
        """Creates instance reading from path.

        Args:
            path:  Path to the mjlee provided txt file.
            log10fftdomain:  The calibration is done on log10(FFT)->power_dBm.
                This can be thought of as the "calibration domain".

        """
        path = Path(path)
        with path.open('r') as fl:
            coeffs = {}
            for line in fl.readlines():
                vals = line.split(" ")
                order = int(vals[0])
                coeff_l = tuple(map(float, vals[1:]))
                coeffs[order] = coeff_l

        return cls(coeffs, domain=log10fftdomain)

    @classmethod
    def default_bf6(cls):
        """Data from calibration on 20210915"""
        caldict = {
                5: (
                    -218.615499, 54.084774,
                    -9.938426, 1.062241,
                    -0.054542, 0.001085),
                1: (-144.309670, 9.837448)
                }
        return cls(caldict, (4, 15))


class CAPPDigitizer():
    class RunStatus(Enum):
        STOPPED = 0
        WAITFORSTART = 1
        RUNNING = 2

    calibration_factors = [9.60732e+00, -1.42145e+02]
    control_payload_header = '\xca'
    control_payload_tail = '\x00\x00\x00\x00\x00\x00'
    control_modes = {
        'testledon': '\x01',
        'testledoff': '\x02',
        'configdata': '\x10',
        'statictest': '\x11',
        'rawadc': '\x12',
        'rawfft': '\x13',
        'powerspec': '\x14',
        'voltspec': '\x15',
        'standby': '\x16',
        'autoadcavgon': '\x30',
        'autoadcavgoff': '\x31',
        'usingampon': '\x32',
        'usingampoff': '\x33',
        'auxinput': '\x32',
        'orginput': '\x33',
        'ignoresinon': '\x34',
        'ignoresinoff': '\x35'
    }
    digitizer_control_mac_address = '\x08\x62\x66\x52\x30\xaa'

    def __init__(
            self,
            mac_address=_conf_bf6['mac_address'],
            name='CAPP Digitizer',
            calfpath: t.Optional[str] = None,
            **kwargs):
        self.name = name
        self.mac_address = [0,0,0,0,0,0]  # placeholder
        self.convert_mac_address(mac_address)

        self._h_default_sigint = signal.getsignal(signal.SIGINT)
        self._h_default_sigusr1 = signal.getsignal(signal.SIGUSR1)
        self._h_default_sigusr2 = signal.getsignal(signal.SIGUSR2)

        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGUSR1, self.signal_handler)
        signal.signal(signal.SIGUSR2, self.signal_handler)

        self.status = self.RunStatus.WAITFORSTART
        self.is_connected = False
        self.is_started = False
        self.timestamp_pid_0 = 0
        self.last_pid = 31

        self.flags = {
            'bin_output': kwargs.get('flag_bin_output', False),
            'root_output': kwargs.get('flag_root_output', False),
            'signal_control': kwargs.get('flag_signal_control', False),
            'print': kwargs.get('flag_print', False),
            'verbose': kwargs.get('flag_verbose', False)
        }

        if calfpath is None:
            self.mjcal = MJCal.default_bf6()
        else:
            self.mjcal = MJCal.from_path(calfpath)

        self.logger = logger
        self.nbins = _conf_bf6['n_bins']
        self.binwidth = _conf_bf6['binwidth']
        self.fcenter = _conf_bf6['fcenter']

    def connect(self):
        self.if_request = struct.pack('16sH14s', _conf_bf6['interface'].encode('utf-8'), socket.AF_PACKET, ('\x00' * 14).encode('utf-8'))

        try:
            self.server = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0003))
        except PermissionError:
            self.logger.warning(f'[{self.name}] Digitizer should run with a root permission. Try it with sudo.')
            return False
        else:
            self.logger.info(f'[{self.name}] Socket {self.server} created.')

        self.if_request = fcntl.ioctl(self.server, 0x00008933, self.if_request)
        self.if_index = struct.unpack('16sH2x4s8x', self.if_request)[1]
        self.logger.debug(f'[{self.name}] Network interface index: {self.if_index}')

        self.server.bind((_conf_bf6['interface'], self.if_index))
        self.logger.debug(f'[{self.name}] Socket {self.server} bound.')
        self.logger.info(f'[{self.name}] Digitizer server ready.')

        self.is_connected = True

        return True

    def disconnect(self):
        # restore original signal handlers, maybe they are doing something
        # important.

        signal.signal(signal.SIGINT, self._h_default_sigint)
        signal.signal(signal.SIGUSR1, self._h_default_sigusr1)
        signal.signal(signal.SIGUSR2, self._h_default_sigusr2)

        if self.is_connected:
            self.server.close()
            self.is_connected = False

    @contextmanager
    def connect_(self):
        try:
            self.connect()
            yield self
        finally:
            self.disconnect()

    def set_digitizer_mode(self, mode='powerspec'):
        if not self.is_connected:
            self.logger.error(f'[{self.name}] Digitizer is not connected. Call connect() first.')
            return False

        if mode not in self.control_modes:
            self.logger.error(f'[{self.name}] No digitizer mode {mode} supported.')
            return False
        else:
            payload = self.control_payload_header + self.control_modes[mode] + self.control_payload_tail
            des_addr = '\xff\xff\xff\xff\xff\xff'
            check_sum = '\x1a\x2b\x3c\x4d'
            ethernet_type = '\x00\x20'

            packet = des_addr + self.digitizer_control_mac_address + ethernet_type + payload + check_sum

            self.server.send(packet)

    def signal_handler(self, signum, flag):
        if signum == signal.SIGINT:
            # ending_process ()
            pass
        elif signum == signal.SIGUSR1:
            if flag_signal_control:
                if status == RunStatus.STOPPED:
                    if not flag_file_open:
                        if not open_file():
                            self.logger.error(f'[{self.name}] Cannot start DAQ.')
                            # ending_process ()
                status = RunStatus.WAITFORSTART
        elif signum == signal.SIGUSR2:
            if flag_signal_control:
                if status == RunStatus.WAITFORSTART or status == RunStatus.RUNNING:
                    status = RunStatus.STOPPED
                    close_file()

    def bin2uint(self, data, endian='big'):
        return_value = 0
        if len(data) <= 0:
            return return_value

        return_value = int.from_bytes(data, endian)

        return return_value

    def check_mac_address(self, address):
        if len(address) != 6:
            self.logger.error(f'[{self.name}] MAC address length is not right. ({len(addr)} elements input)')
            return False

        flag = True
        for i in range(6):
            flag = flag and (hex(address[i]) == hex(self.mac_address[i]))

        return flag

    def convert_mac_address(self, address):
        token = address.split(':')
        if len(token) != 6:
            self.logger.error(f'[{self.name}] Wrong MAC address.')
            return False

        for i in range(6):
            self.mac_address[i] = int(token[i], 16)

        return True

    def build_mac_address(self, address):
        if len(address) != 6:
            self.logger.error(f'[{self.name}] MAC address length is not right. ({len(addr)} elements input)')
            return None
        else:
            return "%02x:%02x:%02x:%02x:%02x:%02x" % (address[0], address[1], address[2], address[3], address[4], address[5])

    def decode_data(self, data):
        eid = self.bin2uint(data[0:0 + 3])
        pid = self.bin2uint(data[3:3 + 1])
        timestamp = self.bin2uint(data[4:4 + 4], 'big')
        data_packet = data[8:8 + 1280]
        trailer = data[1288:1288 + 5]
        mode = self.bin2uint(data[1293:1293 + 1])
        n_bytes = self.bin2uint(data[1294:1294 + 2])

        ret_data = {}

        self.logger.debug(f'[{self.name}] event id = {eid}, packet id = {pid}, timestamp = {timestamp}, trailer = {trailer}, mode = {mode}, bytes = {n_bytes}')

        if mode == 5:
            if not self.is_started:
                if pid != 0:
                    self.logger.info(f'[{self.name}] Skipping the first event with packet id {pid}')
                    return
                else:
                    self.is_started = True

            if pid == 0:
                if self.timestamp_pid_0 == 0:
                    self.timestamp_pid_0 = timestamp
                else:
                    time_gap = timestamp - self.timestamp_pid_0
                    self.timestamp_pid_0 = timestamp

            if pid == self.last_pid:
                ret_data = self.process_power_spectrum(data_packet[:n_bytes])

        return ret_data

    def unpack_data_struct(self, barr: bytearray) -> np.ndarray:
        val = struct.unpack('>5001q', barr[:5001 * 8])
        return val

    def unpack_data_numpy(self, barr: bytearray) -> np.ndarray:
        # this is ~10x faster than struct.unpack
        val = np.ndarray(5001, buffer=barr, dtype='>i8')
        return val

    def process_get_data_result(
            self, data: t.Dict,
            unpackmethod: str = 'numpy',
            avgmethod: str = 'calpower'):
        """Processes the resulting dictionary from `get_data`.

        Note that no matter the `avgmethod` the returned value is always dBm.

        """
        if avgmethod != 'calpower':
            raise NotImplementedError

        if unpackmethod == 'numpy':
            unpackfun = self.unpack_data_numpy
        elif unpackmethod == 'struct':
            unpackfun = self.unpack_data_struct
        else:
            raise ValueError(
                    f"Unknown method for unpacking.({unpackmethod})")

        arrsum = np.zeros(5001, dtype='float')
        i = 1
        for k, v in data.items():
            if k == 'status':
                continue
            arr = unpackfun(v)
            l10arr = np.log10(arr)
            # this stuff can be parallelized.
            arrcaldbm = self.mjcal.polyeval(l10arr)
            arrsum += dbm2watt(arrcaldbm)
            i += 1

        return watt2dbm(arrsum/i)

    def get_averaged_spectrum(
            self, navg: int,
            unpackmethod: str = 'numpy',
            avgmethod: str = 'calpower'):
        """Collects `navg` spectra and returns a spectrum averaged using
        `avgmethod`."""
        with self.connect_():
            data = self.get_data(navg)
        return self.process_get_data_result(
                data, unpackmethod, avgmethod)

    def get_data(self, n_events):
        daq_total_bytes = 0
        count = 0

        ret_data = {'status': False}
        flush_event_data = True
        events_taken = 0

        while True:
            if self.status == self.RunStatus.STOPPED:
                return ret_data

            data, address = self.server.recvfrom(65536)

            if len(data) == 0:
                self.logger.error(f'[{self.name}] Error in reading data from the digitizer.')
                return ret_data

            if self.check_mac_address(data[6:12]):
                if flush_event_data:
                    event_data = {
                        'events_taken': events_taken,
                        'event_no': self.bin2uint(data[14:17]),
                        'packet_no': self.bin2uint(data[17:18]),
                        'data': bytearray(),
                        'mode': self.bin2uint(data[1307:1308]),
                        'size': 0
                    }
                    flush_event_data = False

                event_data['data'].extend(data[22:22 + 1280])

                if self.status == self.RunStatus.WAITFORSTART:
                    event_data['size'] = len(data)

                    daq_total_bytes = len(data)
                    self.logger.debug(f'[{self.name}] MAC from {self.build_mac_address(data[6:12])} to {self.build_mac_address(data[0:6])}.')
                    self.logger.debug(f"[{self.name}] Starting with an event of event id {event_data['event_no']} in digitizer for {n_events} events.")
                    self.status = self.RunStatus.RUNNING
                elif self.status == self.RunStatus.RUNNING:
                    event_data['packet_no'] = self.bin2uint(data[17:18])
                    event_data['size'] += len(data)

                    daq_total_bytes += len(data)

                    if count % 1000 == 0 and self.flags['verbose']:
                        self.logger.info(f"[{self.name}] Event #{event_data['event_no']}, {len(data)} bytes, {daq_total_bytes / 1000} kB received in total.")
                        self.logger.info(f'[{self.name}] Printing out first 10 data received since verbose flag is on.')
                        for i in range(10):
                            self.logger.info("   %02x%02x%02x%02x%02x%02x%02x%02x" % (data[i + 14], data[i + 15], data[i + 16], data[i + 17], data[i + 18], data[i + 19], data[i + 20], data[i + 21]))

                    if event_data['packet_no'] == 31:
                        if len(event_data['data']) != 1280 * 32:
                            self.logger.warning(f"[{self.name}] (event# {events_taken}) Wrong data length, discarding the event. (probably packet loss?)")

                            flush_event_data = True
                            continue
                        if events_taken == 0:
                            self.logger.warning(f"[{self.name}] (event# {events_taken}) Ignoring the first event.")
                            events_taken += 1
                            flush_event_data = True
                            continue

                        ret_data[event_data['events_taken']] = event_data['data']
                        if events_taken % 100 == 0:
                            self.logger.info(f"[{self.name}] ({events_taken} stored) Event #{event_data['event_no']} received, data size = {event_data['size'] / 1.e3:.3} kB.")
                        flush_event_data = True
                        events_taken += 1

                    if events_taken > n_events:
                        self.logger.info(f"[{self.name}] Event #{event_data['event_no']}, {len(data)} bytes, {daq_total_bytes / 1000} kB received in total.")
                        if self.flags['signal_control']:
                            self.signal_handler(signal.SIGUSR2)
                        else:
                            ret_data['status'] = True
                            return ret_data
            count += 1
