"""ANC350 Piezo Controller instrument classes.

Adapted from CULDAQ2 backend module.  Originally written by Soohyung Lee.
"""

import typing as t
from pathlib import Path
from ctypes import cdll as cdll_factory
from ctypes import CDLL, c_int32, Structure, byref, c_bool, c_int
from ctypes import c_uint32, c_char_p
from enum import IntEnum
import math

import attr

from groundcontrol.helper import StrEnum
from groundcontrol.instruments import Instrument, CommunicationError
from groundcontrol.resources import libdir
from groundcontrol.logging import setup_logger


logger = setup_logger('attocube')


_driverpath = (libdir / "libanc350v2.so").expanduser()


def _magicmult(num: t.Union[c_int32]) -> float:
    # there is a magic multiplier, 1e-3, appearing couple of times.
    # I will put the meaning of it after i find it out, here.
    return float(num.value)*1e-3


@attr.s(auto_attribs=True)
class AttocubeANC350(Instrument):
    device_no: int
    is_connected: bool
    driver: CDLL
    sensor_voltage_range: float = (0, 2)
    actuator_voltage_limit: float = 50
    actuator_frequency_limit: float = 2000

    _name: str = ""
    _handle: c_int32 = c_int32(-1)
    _lock: c_bool = c_bool(0)

    class ActuatorType(StrEnum):
        LINEAR = "LINEAR"
        GONIOMETER = "GONIOMETER"
        ROTATOR = "ROTATOR"

    class DeviceType(StrEnum):
        RES = "ANC350RES"
        NUM = "ANC350NUM"
        FPS = "ANC350FPS"
        NONE = "NONE"  # what does this mean?

    class InterfaceType(StrEnum):
        INVALID = "INVALID"
        USB = "USB"
        TCPIP = "TCP/IP"
        ALL = "ALL"

    class MovementStatus(StrEnum):
        ATREST = "ATREST"
        MOTINPROG = "MOVING"
        BLOCKED = "BLOCKED"

    class AxisNo(IntEnum):
        A1 = 0
        A2 = 1
        A3 = 2

    class Status(StrEnum):
        ON = "1"
        OFF = "0"
        WARNING = "WARNING"
        _ON_1 = "ON"
        _OFF_1 = "OFF"

    class Direction(IntEnum):
        FORWARD = 0
        BACKWARD = 1

    class PositionerInfo(Structure):
        # ctypes generates the descriptors
        _fields_ = [('id', c_int), ('locked', c_bool)]

    def connect(self) -> bool:
        dev_info = self.PositionerInfo(self.device_no, self._lock)

        code = self.driver.PositionerCheck(byref(dev_info))

        if not code:
            return False

        try:
            code = self.driver.PositionerConnect(
                    c_int32(0), byref(self._handle))  # what is 0...
        except TypeError as e:
            raise e
        except Exception as e:
            raise CommunicationError(e)
            # raise e

        self.is_connected = True
        return True

    def disconnect(self):
        self.driver.PositionerClose(self._handle)

    def load_profile(
            self,
            axis: AxisNo, filename: t.Union[str, Path]):
        if isinstance(filename, Path):
            filename = filename.resolve()

        try:
            self.driver.PositionerLoad(
                    self._handle, c_uint32(axis), c_char_p(filename))
        except Exception as e:
            raise CommunicationError(e)
        else:
            return True

    def query_status(self, axis: AxisNo = 0):
        status = c_int32()
        self.driver.PositionerGetStatus(
                self._handle, c_uint32(axis), byref(status))
        return status

    def query_position(self, axis: AxisNo = 0) -> float:
        # TODO: units.
        position = c_int32()
        self.driver.PositionerGetPosition(self._handle, axis, byref(position))

        return _magicmult(position)

    def query_rotation_count(self, axis: AxisNo = 0) -> int:
        count = c_int32()
        self.driver.PositionerGetRotCount(
                self._handle, c_int32(axis), byref(count))

        return count.value

    def query_speed(self, axis: AxisNo = 0) -> float:
        speed = c_int32()
        self.driver.PositionerGetSpeed(
                self._handle, c_int32(axis), byref(speed))

        return _magicmult(speed)

    def query_step_width(self, axis: AxisNo = 0) -> float:
        step_width = c_int32()
        self.driver.PositionerGetStepwidth(
                self._handle, c_int32(axis), byref(step_width))

        return _magicmult(step_width)

    def query_amplitude(self, axis: AxisNo = 0) -> float:
        amplitude = c_int32(-1)
        self.driver.PositionerGetAmplitude(
                self._handle, c_int32(axis), byref(amplitude))

        return _magicmult(amplitude)

    def query_frequency(self, axis: AxisNo = 0) -> int:
        freq = c_int32()
        self.driver.PositionerGetFrequency(
                self._handle, c_int32(axis), byref(freq))

        return freq.value

    def query_capacitance(self, axis: AxisNo = 0) -> int:
        capacitance = c_int32()
        self.driver.PositionerCapMeasure(
                self._handle, c_int32(axis), byref(capacitance))

        return capacitance.value

    def set_output(self, axis: AxisNo, flag: Status):
        if flag in ["1", "ON"]:
            flag = c_bool(1)
        else:
            flag = c_bool(0)

        self.driver.PositionerSetOutput(self._handle, c_int32(axis), flag)

    def set_sensor_voltage(self, volt: float) -> bool:
        """Sets the sensor voltage.  Truncated to 1 mV accuracy."""
        vmin, vmax = self.sensor_voltage_range[0], self.sensor_voltage_range[1]

        if (volt < vmin) or (volt > vmax):
            return False
        else:
            millivolt = math.trunc(volt*1e3)
            self.driver.PositionerStaticAmplitude(
                    self._handle, c_int32(millivolt))
            return True

    def set_amplitude(self, axis: AxisNo, volt: float):
        """Sets the actuator amplitude for axis `axis`.  Truncated to 1 mV
        accuracy."""
        if (volt > self.actuator_voltage_limit) or (volt < 0):
            return False
        else:
            millivolt = math.trunc(volt*1e3)
            self.driver.PositionerAmplitude(
                    self._handle, c_int32(axis), c_int32(millivolt))
            return True

    def set_frequency(self, axis: AxisNo, frequency: float):
        if (frequency > self.actuator_frequency_limit) or (frequency < 0):
            return False
        else:
            self.driver.PositionerFrequency(
                    self._handle, c_int32(axis), c_int32(frequency))
            return True

    def do_move_single_step(self, axis: AxisNo, direction: Direction):
        self.driver.PositionerMoveSingleStep(
                self._handle, c_uint32(axis), c_uint32(direction))
        return True

    def do_move_continuous(self, axis: AxisNo, direction: Direction):
        self.driver.PositionerMoveContinuous(
                self._handle, c_uint32(axis), c_uint32(direction))
        return True

    def do_stop(self, axis: AxisNo):
        self.driver.PositionerStopMoving(self._handle, c_int32(axis))

    @classmethod
    def create(
            cls,
            driver: CDLL,
            device_no: int = 0,
            handle: int = -1,
            name: str = ""):
        iscon = False
        return cls(device_no, iscon, driver, handle=handle, name=name)

    close = disconnect

    @classmethod
    def from_path(
            cls,
            dllpath: t.Union[str, Path],
            device_no: int = 0,
            handle: int = -1,
            name: str = ""):
        handle = c_int32(handle)
        driver = cdll_factory.LoadLibrary(dllpath)
        return cls.create(driver, device_no, handle, name)

    @classmethod
    def from_visa_resource_name(cls, resource_name: str, name: str):
        logger.debug("The resource name for attocube is not parsed."
                     "Initializing with magic numbers inside the module.")
        obj = cls.from_path(_driverpath, name=name)
        obj.connect()
        return obj


if __name__ == "__main__":
    piezo = AttocubeANC350.from_path(_driverpath)
    piezo.connect()
    piezo.set_sensor_voltage(0.1)

    for i in [0]:
        print("-- Axis #%d" % i)
        print("   Capacitance : %f F" % piezo.query_capacitance(i))
        print("   Voltage     : %f V" % piezo.query_amplitude(i))
        print("   Frequency   : %d Hz" % piezo.query_frequency(i))
        print("   Position    : %f um" % piezo.query_position(i))


    piezo.disconnect()
