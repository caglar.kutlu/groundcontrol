"""Keysight(Agilent) N5232A model network analyzer

"""
import typing as t
from numbers import Number
from itertools import islice

import numpy as np

from .networkanalyzer import SCPIVectorNetworkAnalyzer
from groundcontrol.helper import StrEnum, EnumNotFoundError


__all__ = ['AgilentN5232A', 'KeysightN5232A']


class AgilentN5232A(SCPIVectorNetworkAnalyzer):
    """Class implementing difference from the generic one."""
    def set_sense_sweep_step(self, step: float, cnum: int = 1):
        span = self.query_sense_frequency_span()
        npoints = int(np.ceil(span/step)) + 1
        self.set_sense_sweep_points(npoints)

    def query_sense_sweep_step(self, cnum: int = 1):
        npoints = self.query_sense_sweep_points()
        span = self.query_sense_frequency_span()
        sweep_step = span/(npoints - 1)
        return sweep_step


# An alias, just in case.
KeysightN5232A = AgilentN5232A

