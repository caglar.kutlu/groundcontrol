""" Rohde & Schwarz ZND series network analyzer.  """
import typing as t
from itertools import islice
from numbers import Number

import numpy as np

from .networkanalyzer import SCPIVectorNetworkAnalyzer
from groundcontrol.instruments._common import NotACommand
from groundcontrol.instruments._common import optarg as _optarg
from groundcontrol.helper import StrEnum


__all__ = ['RohdeSchwarzZND']


StrInt = t.Union[str, int]
StrNum = t.Union[str, Number]


class RohdeSchwarzZND(SCPIVectorNetworkAnalyzer):
    _super = SCPIVectorNetworkAnalyzer
    State = _super.State
    SweepType = _super.SweepType
    FormatDataType = _super.FormatDataType
    MarkerType = _super.MarkerType
    ByteOrderFormat = _super.ByteOrderFormat
    _string_quote_char: t.ClassVar[str] = "'"

    class SweepType(StrEnum):
        LINEAR = "LIN"
        LOGARITHMIC = "LOG"
        POWER = "POW"
        CW = "CW"
        SEGMENT = "SEGM"
        POINT = "POIN"

    class AverageMode(StrEnum):
        """Averaging Type.
        Choose from:
            AUTO: Automatic selection between REDuce and FLATten mode,
            depending on the trace format.
            FLATten: Cumulative moving averages of the (linear) magnitude and
            phase values, provides the most effective noise suppression for
            the "dB Mag", "Phase", "Unwr Phase", and "Lin Mag" formats.
            REDuce: Cumulative moving averages of the real and imaginary parts
            of each measurement result, provides the most effective noise sup-
            pression for the "Real" and "Imag" formats and for complex trace
            formats.
            MOVing: Simple moving averages of the real and imaginary parts of
            each measurement result; similar to REDuce, but with finite
            history.
        """
        AUTO = "AUTO"
        FLATTEN = "FLAT"
        REDUCE = "RED"
        MOVING = "MOV"

    class SweepType(StrEnum):
        LINEAR = "LIN"
        LOGARITHMIC = "LOG"
        POWER = "POW"
        CW = "CW"
        SEGMENT = "SEGM"
        POINT = "POIN"

    class TriggerSource(StrEnum):
        EXTERNAL = "EXT"
        IMMEDIATE = "IMM"
        MANUAL = "MAN"
        MULTIPLE = "MULT"

    class DisplayFormat(StrEnum):
        """See ZND manual page 621"""
        MLINEAR = "MLIN"
        MLOGARITHMIC = "MLOG"
        PHASE = "PHAS"
        UPHASE = "UPH"
        POLAR = "POL"
        SMITH = "SMIT"
        ISMITH = "ISM"
        GDELAY = "GDEL"
        REAL = "REAL"
        IMAGINARY = "IMAG"
        SWR = "SWR"
        COMPLEX = "COMPLEX"
        MAGNITUDE = "MAGN"
        LOGARITHMIC = "LOG"

    class MeasSequence:
        """ Triggered measurement sequence, string variable.
        'SWEep' – trigger event starts an entire sweep.
        'SEGMent' – trigger event starts a sweep segment, if segmented
        frequency sweep is active (see example below). If another
        sweep type is active, the trigger event starts an entire sweep.
        'POINt' – trigger event starts measurement at the next sweep
        point.
        'PPOint' – trigger event starts the next partial measurement at
        the current or at the next sweep point.
        """
        SWEEP = "SWE"
        SEGMENT = "SEGM"
        POINT = "POIN"
        PPOINT = "PPO"

    class MarkerFormat(StrEnum):
        MLINEAR = "MLIN"
        MLOGARITHMIC = "MLOG"
        PHASE = "PHAS"
        POLAR = "POL"
        GDELAY = "GDEL"
        REAL = "REAL"
        IMAGINARY = "IMAG"
        SWR = "SWR"
        LINPHASE = "LINP"
        LOGPHASE = "LOGP"
        IMPEDANCE = "IMP"
        ADMITTANCE = "ADM"
        DEFAULT = "DEF"
        COMPLEX = "COMP"
        MDB = "MDB"
        MLPHASE = "MLPH"
        MDPHASE = "MDPH"
        MIMPEDANCE = "MIMP"
        PIMPEDANCE = "PIMP"
        PADMITTANCE = "PADM"
        MADMITTANCE = "MADM"
        MPIMPEDANCE = "MPIM"
        INDX = "INDX"

    class SearchMode(StrEnum):
        MAXIMUM = "MAX"
        MINIMUM = "MIN"
        RPEAK = "RPE"
        LPEAK = "LPE"
        NPEAK = "NPE"
        TARGET = "TARG"
        LTARGET = "LTAR"
        RTARGET = "RTAR"
        BFILTER = "BFILT"
        MMAXIMUM = "MMAX"
        MMINIMUM = "MMIN"
        SPROGRESS = "SPR"

    class BandfilterType(StrEnum):
        BPASS = "BPAS"
        BSTOP = "BST"
        BPRMARKER = "BPRM"
        BSRMARKER = "BSRM"
        BPABSOLUTE = "BPAB"
        BSABSOLUTE = "BSAB"
        NONE = "NONE"

    class MarkerMode(StrEnum):
        CONTINUOUS = "CONT"
        DISCRETE = "DISC"

    class OutDataType(StrEnum):
        """Enumerates the data type for transferring data.

        Below information is taken from ZND Help document.

        FDATA
            - Formatted trace data, according to the selected trace format
              (CALCulate<Chn>:FORMat).
            - One value per trace point for Cartesian diagrams, two values for
              polar diagrams.
            - [Data access point 6]

        SDATA
            - Unformatted trace data: real and imaginary part of each
              measurement point.
            - Two values per trace point irrespective of the selected trace
              format.
            - The trace mathematics is not taken into account.
            - [Data access point 4]

        MDATA
            - Unformatted trace data (see SDATa) after evaluation of trace
              mathematics.
            - [Data access point 5]

        NCDATA
            - Factory calibrated trace data: the values are obtained right
              after applying the factory calibration but before applying a
              user-defined calibration (if any).
            - [Data access point 1]
        UCDATA
            - Uncalibrated trace data.
            - [Data access point 0]
            Note:
                * the respective trace must represent a wave quantity or ratio
                * driving and receiving port must not be on a switch matrix
                Otherwise an error occurs.

        SCORrxx:  NOT IMPLEMENTED

        RDATA
            - Mapped to NCDATA in this API for Keysight behavior emulation.

        """
        FDATA = "FDAT"
        SDATA = "SDAT"
        MDATA = "MDAT"
        NCDATA = "NCD"
        UCDATA = "UCD"
        RDATA = "NCD"  # KEYSIGHT EMULATION!!

    class RefOscSource(StrEnum):
        INTERNAL = "INT"
        EXTERNAL = "EXT"

    def query_source_catalog(self) -> t.List[str]:
        raise NotACommand

    def set_source_power_mode(
            self, state: str, port: int = 1, cnum: int = 1,
            src: t.Optional[str] = None):
        raise NotACommand

    def query_source_power_mode(
            self, port: int = 1, cnum: int = 1,
            src: t.Optional[str] = None):
        raise NotACommand

    def set_source_frequency_fixed(self, freq: float, cnum: int = 1):
        return self.write(f"SOUR{cnum}:FREQ:FIX {freq}")

    def query_source_frequency_fixed(self, cnum: int = 1):
        return self.query_float(f"SOUR{cnum}:FREQ:FIX?")

    def query_sense_correction_cset_activate(
            self, opt: str = "NAME", cnum: int = 1) -> str:
        raise NotACommand

    def set_sense_correction_cset_activate(
            self,
            nameorguid: str,
            apply_stimulus_settings: State = State.OFF,
            cnum: int = 1) -> str:
        raise NotACommand

    def set_sense_fom_state(self, state: State, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_state(self, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_catalog(self, cnum: int = 1) -> str:
        raise NotACommand

    def query_sense_fom_count(self, cnum: int = 1) -> int:
        raise NotACommand

    def set_sense_fom_display_select(self, *args):
        raise NotACommand

    def query_sense_fom_display_select(
            self, cnum: int = 1) -> str:
        raise NotACommand

    def query_sense_fom_rnum(
            self, *args) -> int:
        raise NotACommand

    def set_sense_fom_range_coupled(
            self, rnum: int, state: State, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_coupled(
            self, rnum: int, cnum: int = 1) -> "State":
        raise NotACommand

    def set_sense_fom_range_frequency_cw(
            self, rnum: int, freq: float, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_frequency_cw(
            self, rnum: int, cnum: int = 1) -> float:
        raise NotACommand

    def set_sense_fom_range_frequency_divisor(
            self, rnum: int, divisor: int, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_frequency_divisor(
            self, rnum: int, cnum: int = 1) -> int:
        raise NotACommand

    def set_sense_fom_range_frequency_multiplier(
            self, rnum: int, multiplier: int, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_frequency_multiplier(
            self, rnum: int, cnum: int = 1) -> int:
        raise NotACommand

    def set_sense_fom_range_frequency_offset(
            self, rnum: int, offset: float, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_frequency_offset(
            self, rnum: int, cnum: int = 1) -> float:
        raise NotACommand

    def set_sense_fom_range_frequency_start(
            self, rnum: int, start: float, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_frequency_start(
            self, rnum: int, cnum: int = 1) -> float:
        raise NotACommand

    def set_sense_fom_range_frequency_stop(
            self, rnum: int, stop: float, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_frequency_stop(
            self, rnum: int, cnum: int = 1) -> float:
        raise NotACommand

    def query_sense_fom_range_name(
            self, rnum: int, cnum: int = 1) -> str:
        raise NotACommand

    def set_sense_fom_range_sweep_type(
            self, rnum: int, sweep_type: SweepType, cnum: int = 1):
        raise NotACommand

    def query_sense_fom_range_sweep_type(
            self, rnum: int, cnum: int = 1) -> "SweepType":
        raise NotACommand

    def query_cset_catalog(self) -> t.List[str]:
        raise NotACommand

    def do_cset_copy(self, cset1: str, cset2: str):
        raise NotACommand

    def query_cset_date(self, cset: str) -> str:
        raise NotACommand

    def query_cset_exists(self, cset: str) -> 'State':
        raise NotACommand

    def do_cset_fixture_cascade(
            self, s2p1: str, s2p2: str, s2p_result: str, format: str):
        raise NotACommand

    def do_cset_fixture_characterize(
            self, cs1: str, cs2: str, port: int, s2p: str, format: str,
            pivot: t.Optional[float] = None):
        raise NotACommand

    def do_cset_fixture_deembed(
            self, cs1: str, cs2: str, s2p: str, port: int, comp_pwr: bool,
            extrap: t.Optional[bool] = None):
        raise NotACommand

    def do_cset_fixture_embed(
            self, cs1: str, cs2: str, s2p: str, port: int, comp_pwr: bool,
            extrap: t.Optional[bool] = None):
        raise NotACommand

    def query_sense_sweep_dwell_auto(self, cnum: int = 1) -> 'State':
        raise NotACommand

    def set_sense_sweep_dwell_delay(self, *args):
        raise NotACommand

    def set_sense_sweep_dwell_auto(self, state: State, cnum: int = 1):
        raise NotACommand

    def set_sense_sweep_generation(
            self, *args):
        raise NotACommand

    def query_sense_sweep_generation(self, cnum: int = 1):
        raise NotACommand

    def set_sense_sweep_generation_pointsweep(
            self, state: State, cnum: int = 1):
        raise NotACommand

    def query_sense_sweep_generation_pointsweep(
            self, cnum: int = 1) -> 'State':
        raise NotACommand

    def set_sense_sweep_groups_count(self, count: int, cnum: int = 1):
        """EMULATED COMMAND FOR KEYSIGHT N9010A COMPATIBILITY"""
        return self.set_sense_sweep_count(count, cnum)

    def query_sense_sweep_groups_count(self, cnum: int = 1) -> int:
        """EMULATED COMMAND FOR KEYSIGHT N9010A COMPATIBILITY"""
        return self.query_sense_sweep_count(cnum)

    def set_sense_sweep_count(self, count: int, cnum: int = 1):
        """Defines the number of sweeps to be measured in single sweep mode
        (INITiate<Ch>:CONTinuous OFF) and in channel no. <Ch>. Use [SENSe:
            ]SWEep:COUNt:ALL to define the sweep count for all channels."""
        return self.write(f"SENS{cnum:d}:SWE:COUN {count}")

    def query_sense_sweep_count(self, cnum: int = 1) -> int:
        """Returns the number of sweeps to be measured in single sweep mode
        """
        return self.query_int(f"SENS{cnum:d}:SWE:GRO:COUN?")

    def set_sense_sweep_type(self, sweep_type: SweepType, cnum: int = 1):
        """Sets the sweep type for the given channel `cnum`"""
        return self.write(f"SENS{cnum:d}:SWE:TYPE {sweep_type}")

    def query_sense_sweep_type(self, cnum: int = 1) -> 'SweepType':
        """Sets the sweep type for the given channel `cnum`"""
        q = self.query(f"SENS{cnum:d}:SWE:TYPE?")
        return self.SweepType.from_str(q)

    def set_sense_sweep_mode(self, *args):
        raise NotACommand

    def query_sense_sweep_mode(self, *args):
        raise NotACommand

    def set_sense_sweep_speed(self, *args):
        raise NotACommand

    def query_sense_sweep_speed(self, *args):
        raise NotACommand

    def set_sense_sweep_trigger_mode(self, *args):
        raise NotACommand

    def set_initiate_continuous(self, state: State, cnum: int = 1):
        """Sets the continuous triggering mode for the specified channel."""
        return self.write(f"INIT{cnum:d}:CONT {state}")

    def query_initiate_continuous(self, cnum: int = 1) -> "State":
        """Returns the `State` of the continuous triggering mode."""
        return self.State.from_str(self.query(f"INIT{cnum:d}:CONT?"))

    def set_initiate_continuous_all(self, state: State):
        """Sets continuous for all channels"""
        return self.write(f"INIT:CONT:ALL {state}")

    def query_initiate_continuous_all(self) -> "State":
        """Returns the `State` of the continuous triggering mode."""
        return self.State.from_str(self.query("INIT:CONT:ALL?"))

    def set_trigger_scope(self, *args):
        raise NotACommand

    def query_trigger_scope(self, *args):
        raise NotACommand

    def set_trigger_sequence_link(self, seq: MeasSequence, cnum: int = 1):
        """Selects the triggered measurement sequence."""
        return self.write(f"TRIG{cnum:d}:LINK {seq}")

    def query_trigger_sequence_link(self, cnum: int = 1):
        """Queries the triggered measurement sequence."""
        return self.write(f"TRIG{cnum:d}:LINK?")

    def do_abort(self):
        raise NotACommand

    def query_calculate_parameter_catalog(
            self, cnum: int = 1) -> t.List[t.Tuple[str, str]]:
        """Returns the trace names and measurement parameters of all traces
        assigned to a particular channel.
        Returns:
            out(str):
                '<measurement_name>,<parameter>,[<measurement_name>,<parameter>...]'
        """
        q = self.query_str(f"CALC{cnum:d}:PAR:CAT?")
        ql = q.split(',')
        pairs = list(zip(*(islice(ql, i, None, 2) for i in range(2))))
        return pairs

    def query_calculate_parameter_catalog_ext(
            self, cnum: int = 1) -> t.List[t.Tuple[str, str]]:
        """EMULATED COMMAND FOR KEYSIGHT N9010A COMPATIBILITY
        Returns the trace names and measurement parameters of all traces
        assigned to a particular channel.
        Returns:
            out(str):
                "<measurement_name>,<parameter>,[<measurement_name>,<parameter>...]"
        """
        return self.query_calculate_parameter_catalog(cnum)

    def query_calculater_parameter_catalog_sended(
            self, cnum: int = 1) -> t.List[t.Tuple[str, str]]:
        """Returns the trace names and measurement parameters of all traces
        assigned to a particular channel."""
        q = self.query(f"CALC{cnum:d}:PAR:CAT:SEND?").strip('"')
        ql = q.split(',')
        pairs = list(zip(*(islice(ql, i, None, 2) for i in range(2))))
        return pairs

    def do_calculate_parameter_define_sgroup(
            self, log_port1: int, log_port2: int, cnum: int = 1):
        """Creates the traces for all S-parameters associated with a group of
        logical ports (S-parameter group)."""
        return self.write(f"CALC{cnum}:PAR:DEF:SGR {log_port1},{log_port2}")

    def do_calculate_parameter_delete_call(
            self, cnum: int = 1):
        """Deletes all traces in the given channel."""
        return self.write(f"CALC{cnum:d}:PAR:DEL:CALL")

    def do_calculate_parameter_delete_cmemory(
            self, cnum: int = 1):
        """Deletes all memory traces in channel `cnum`."""
        return self.write(f"CALC{cnum:d}:PAR:DEL:CMEM")

    def do_calculate_parameter_delete_sgroup(
            self, cnum: int = 1):
        """Deletes a group of logical port measurements."""
        return self.write(f"CALC{cnum}:PAR:DEL:SGR")

    def set_calculate_parameter_measure(
            self, tracename: str, result: str, cnum: int = 1):
        """Assigns a measurement result to an existing trace. The query returns
        the result assigned to the specified trace (no second parameter; see
        example)."""
        return self.write(f"CALC{cnum:d}:PAR:MEAS '{tracename}', '{result}'")

    def do_calculate_parameter_sdefine(
            self, tracename: str, result: str, cnum: int = 1):
        """Creates a trace and assigns a channel number, a name and a
        measurement parameter to it. The trace becomes the active trace in the
        channel but is not displayed.

        See page 676 on the ZND User Manual.
        """
        return self.write(f"CALC{cnum:d}:PAR:SDEF '{tracename}','{result}'")

    def do_calculate_parameter_define_ext(
            self, mname: str, param: str, cnum: int = 1):
        """EMULATED COMMAND FOR KEYSIGHT N9010A COMPATIBILITY
        Creates a measurement with the given name (mname) for the desired
        parameter (param).
        Args:
            mname:  Measurement name to create.  Must be unique.
            param: Measurement Parameter to create. Case sensitive.
                For S-parameters:
                    Any S-parameter available in the PNA
                    Single-digit port numbers CAN be separated by "_"
                        (underscore). For example: "S21" or "S2_1"
                    Double-digit port numbers MUST be separated by underscore.
                        For example: "S10_1"
                For ratioed measurements:
                    Any two PNA physical receivers separated by forward slash
                        '/' followed by comma and source port.
                    For example: "A/R1, 3"
                For non-ratioed measurements:
                    Any PNA physical receiver followed by comma and source port
                    For example: "A, 4"
        """
        return self.do_calculate_parameter_sdefine(
                mname, param, cnum)

    def set_calculate_parameter_select(
            self, tracename: str, cnum: int = 1):
        """Selects the measurement based on the tracename name."""
        return self.write(f"CALC{cnum:d}:PAR:SEL '{tracename}'")

    set_calculate_parameter_mname_select = set_calculate_parameter_select

    def query_calculate_parameter_select(self, cnum: int = 1) -> str:
        return self.query_str(f"CALC{cnum:d}:PAR:SEL?")

    query_calculate_parameter_mname_select = query_calculate_parameter_select

    def query_calculate_parameter_tag_next(self, cnum: int = 1) -> str:
        raise NotACommand

    def query_calculate_parameter_tnumber(self, cnum: int = 1) -> int:
        raise NotACommand

    def query_calculate_parameter_wnumber(self, cnum: int = 1) -> int:
        raise NotACommand

    def query_calculate_x_values(
            self,
            format: FormatDataType,
            byteorder: str = 'SWAP',
            cnum: int = 1) -> np.ndarray:
        """EMULATED COMMAND FOR KEYSIGHT N9010A COMPATIBILITY
        Returns the stimulus values for the selected measurement in the
        current units."""
        return self.query_calculate_data_stimulus(format, byteorder, cnum)

    def query_calculate_data_stimulus(
            self,
            format: FormatDataType,
            byteorder: str = 'SWAP',
            cnum: int = 1) -> np.ndarray:
        """Returns the stimulus values for the selected measurement in the
        current units."""
        s = f"CALC{cnum:d}:DATA:STIM?"
        if format == self.FormatDataType.ASCII:
            return self.query_ascii_values(s)
        elif format == self.FormatDataType.REAL32:
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'f', isbigendian)
        elif format == self.FormatDataType.REAL64:
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'd', isbigendian)

    def set_calculate_marker_bucket(self, *args, **kwargs):
        raise NotACommand

    def query_calculate_marker_bucket(self, *args):
        raise NotACommand

    def set_calculate_marker_excursion(
            self, excursion: StrNum, markernum: int = 1, cnum: int = 1):
        return self.write(
                f"CALC{cnum:d}:MARK{markernum:d}:EXC {excursion}")

    def query_calculate_marker_excursion(
            self, markernum: int = 1, cnum: int = 1) -> float:
        return self.query_float(
                f"CALC{cnum:d}:MARK{markernum:d}:EXC?")

    def set_calculate_marker_excursion_state(
            self, state: State,  markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:EXC:STAT {state}")

    def query_calculate_marker_excursion_state(
            self, markernum: int = 1, cnum: int = 1):
        return self._query_state(f"CALC{cnum:d}:MARK{markernum:d}:EXC:STAT?")

    def set_calculate_marker_threshold(
            self, threshold: StrNum, markernum: int = 1, cnum: int = 1):
        return self.write(
                f"CALC{cnum:d}:MARK{markernum:d}:THR {threshold}")

    def query_calculate_marker_threshold(
            self, markernum: int = 1, cnum: int = 1) -> float:
        return self.query_float(
                f"CALC{cnum:d}:MARK{markernum:d}:THR?")

    def do_calculate_marker_function_execute(
            self, func: SearchMode, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:FUNC:EXEC {func}")

    def set_calculate_marker_type(
            self, mtype: MarkerType, markernum: int = 1, cnum: int = 1):
        # TODO
        raise NotImplementedError

    def set_calculate_marker_function_bwidth_mode(
            self, bftype: BandfilterType,
            markernum: int = 1, cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:BWID:MODE {bftype}"
        return self.write(c)

    def query_calculate_marker_function_bwidth_mode(
            self, markernum: int = 1,
            cnum: int = 1) -> "BandfilterType":
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:BWID:MODE?"
        return self.BandfilterType.from_str(self.query_str(c))

    def do_calculate_marker_function_center(
            self,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:CENT"
        return self.write(c)

    def query_calculate_marker_function_result(
            self,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:RES?"
        return self.query(c)

    def do_calculate_marker_function_span(
            self,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:SPAN"
        return self.write(c)

    def do_calculate_marker_function_start(
            self,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:START"
        return self.write(c)

    def do_calculate_marker_function_stop(
            self,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:FUNC:STOP"
        return self.write(c)

    def set_calculate_marker_mode(
            self,
            mmode: MarkerMode,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:MODE {mmode}"
        return self.write(c)

    def query_calculate_marker_mode(
            self,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:MODE?"
        return self.MarkerMode.from_str(self.query_str(c))

    def set_calculate_marker_name(
            self,
            name: str,
            markernum: int = 1,
            cnum: int = 1):
        c = f"CALC{cnum:d}:MARK{markernum:d}:NAME {name}"
        return self.write(c)

    def query_calculate_marker_name(
            self,
            markernum: int = 1,
            cnum: int = 1) -> str:
        c = f"CALC{cnum:d}:MARK{markernum:d}:NAME?"
        return self.query_str(c)

    def query_calculate_data(
            self,
            datatype: OutDataType,
            format: FormatDataType,
            byteorder: ByteOrderFormat = 'SWAP',
            cnum: int = 1) -> np.ndarray:
        """Queries the data and returns it as an array.  The meaning of values
        in the resulting array depends on the `datatype` argument.

        Returning array if datatype is;
        FDATA
            - Formatted trace data, according to the selected trace format
              (CALCulate<Chn>:FORMat).
            - One value per trace point for Cartesian diagrams, two values for
              polar diagrams.
            - [Data access point 6]

        SDATA
            - Unformatted trace data: real and imaginary part of each
              measurement point.
            - Two values per trace point irrespective of the selected trace
              format.
            - The trace mathematics is not taken into account.
            - [Data access point 4]

        MDATA
            - Unformatted trace data (see SDATa) after evaluation of trace
              mathematics.
            - [Data access point 5]

        NCDATA
            - Factory calibrated trace data: the values are obtained right
              after applying the factory calibration but before applying a
              user-defined calibration (if any).
            - [Data access point 1]

        UCDATA
            - Uncalibrated trace data.
            - [Data access point 0]
            Note:
                * the respective trace must represent a wave quantity or ratio
                * driving and receiving port must not be on a switch matrix
                Otherwise an error occurs.

        """
        s = f"CALC{cnum:d}:DATA? {datatype}"
        if format == self.FormatDataType.ASCII:
            return self.query_ascii_values(s)
        elif format == self.FormatDataType.REAL32:
            # Use logger!
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'f', isbigendian)
        elif format == self.FormatDataType.REAL64:
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'd', isbigendian)

    def do_display_window_trace_y_scale_auto(
            self, wnum: int = 1, tnum: int = 1,
            tracename: t.Optional[str] = None):
        args = f"ONCE{_optarg(tracename)}"
        return self.write(f"DISP:WIND{wnum:d}:TRAC{tnum:d}:Y:AUTO {args}")

    def set_calculate_equation_text(self, equation: str, cnum: int = 1):
        raise NotACommand

    def query_calculate_equation_text(self, cnum: int = 1) -> str:
        raise NotACommand

    def set_calculate_equation_state(self, state: State, cnum: int = 1):
        raise NotACommand

    def query_calculate_equation_state(self, cnum: int = 1) -> 'State':
        raise NotACommand

    def set_system_klock(self, state: State):
        return self.write(f"SYST:KLOC {state}")

    def query_system_klock(self):
        return self.query(f"SYST:KLOC?")

    def query_configure_channel_catalog(self) -> t.List[str]:
        return list(self.query_str("CONF:CHAN:CAT?").strip().split(','))

    def query_configure_trace_name_id(
            self, tracename: str) -> int:
        return self.query_int(f"CONF:TRAC:NAME:ID? '{tracename}'")

    def query_system_active_channel(self) -> int:
        raise NotACommand

    def query_system_active_measurement(self) -> str:
        """Returns the name of the active measurement."""
        raise NotACommand

    def set_sense_roscillator_source(self, refsource: RefOscSource):
        return self.write(f"ROSC {refsource}")

    def query_sense_roscillator_source(self) -> "RefOscSource":
        q = self.query("ROSC?")
        return self.RefOscSource.from_str(q)

