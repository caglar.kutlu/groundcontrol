"""Network analyzers.

The `SCPIVectorNetworkAnalyzer` is a generic class that can be further derived
from.

Currently the generic class is based on PNA network analyzers from Keysight.

`StrEnum` as Command Enumeration:
    - A somewhat incoherent design choice was made when StrEnum`s were used for
    command enumerations.  A better approach would be to use proper Command
    class which may be a derivative of StrEnum or not.
    - When implementing command enumerations using StrEnum, use the shortest
      form of the command as the response.  This ensures that the received
      values are parsed correctly.


"""
import typing as t
from numbers import Number
from itertools import islice

import numpy as np

from groundcontrol.instruments import VISAInstrument, SCPICommon
from groundcontrol.helper import StrEnum, EnumNotFoundError
from groundcontrol.logging import logger
from groundcontrol.instruments._common import optarg as _optarg


StrInt = t.Union[str, int]
StrNum = t.Union[str, Number]


class SCPIVectorNetworkAnalyzer(SCPICommon, VISAInstrument):
    """Instrument class for network analyzers.

    There are 3 main types of commands:
        set:  Sets a certain parameter.
        do:  Does a certain thing, such as starting the measurement.
        query:  Queries a value.

    This class should be a thin wrapper around SCPI commands.  Therefore, no
    type checking, and minimal conversion should be performed.
    """

    class State(StrEnum):
        ON = '1'
        OFF = '0'

        @classmethod
        def from_str(cls, s: str):
            try:
                return super().from_str(s)
            except EnumNotFoundError:
                # kind of stupid.
                return super().from_str(str(int(float(s))))

    class FormatDataType(StrEnum):
        REAL32 = "REAL,+32"
        REAL64 = "REAL,+64"
        ASCII = "ASCii,0"
        _REAL32_1 = "REAL,32"
        _REAL64_2 = "REAL,64"
        _ASCII_1 = "ASC,0"

    class ByteOrderFormat(StrEnum):
        BIG_ENDIAN = "NORM"
        LITTLE_ENDIAN = "SWAP"
        NORMAL = "NORM"  # alias
        SWAPPED = "SWAP"  # alias

    class Unit(StrEnum):
        DBM = 'DBM'
        DBMV = 'DBMV'
        DBMA = 'DBMA'
        W = 'W'
        V = 'V'
        A = 'A'

    class SourcePowerMode(StrEnum):
        AUTO = "AUTO"
        ON = "ON"
        OFF = "OFF"
        NOCTL = "NOCTL"

    class TriggerSource(StrEnum):
        EXTERNAL = "EXT"
        IMMEDIATE = "IMM"
        MANUAL = "MAN"

    class TriggerScope(StrEnum):
        ALL = "ALL"  # All channels triggered.
        CURRENT = "CURR"  # Only active channel triggered.

    class SweepGeneration(StrEnum):
        STEPPED = "STEP"
        ANALOG = "ANAL"

    class SweepType(StrEnum):
        LINEAR = "LIN"
        LOGARITHMIC = "LOG"
        POWER = "POW"
        CW = "CW"
        SEGMENT = "SEGM"
        PHASE = "PHAS"

    class SweepMode(StrEnum):
        HOLD = "HOLD"  # Channel will not trigger.
        CONTINUOUS = "CONT"  # Channel triggers indefinitely.
        GROUPS = "GRO"  # Channel accepts the number of triggers.
        SINGLE = "SING"  # CHannel accepts ONE trigger and then goes to HOLD.

    class SweepSpeed(StrEnum):
        FAST = "FAST"
        NORMAL = "NORMAL"

    class SweepTriggerMode(StrEnum):
        CHANNEL = "CHAN"  # Each trigger signal causes all traces in that channel to be swept  # noqa: E501
        SWEEP = "SWE"  # Each man or ext trigger all traces sharing same source to be swept  # noqa: E501
        POINT = "POIN"  # Each trigger causes one data point to be measured  # noqa: E501
        TRACE = "TRAC"  # Each trigger causes two identical measurements to be triggered separately.  # noqa: E501

    class MarkerFormat(StrEnum):
        DEFAULT = "DEF"
        MLINEAR = "MLIN"
        MLOGARITHMIC = "MLOG"
        IMPEDANCE = "IMP"
        ADMITTANCE = "ADM"
        PHASE = "PHAS"
        IMAGINARY = "IMAG"
        REAL = "REAL"
        POLAR = "POL"
        GDELAY = "GDEL"
        LINPHASE = "LINP"
        LOGPHASE = "LOGP"
        KELVIN = "KELV"
        FAHRENHEIT = "FAHR"
        CELSIUS = "CELSIUS"
        NOISE = "NOIS"

    class MarkerFunction(StrEnum):
        MAXIMUM = "MAX"
        MINIMUM = "MIN"
        RPEAK = "RPE"
        LPEAK = "LPE"
        NPEAK = "NPE"
        TARGET = "TARG"
        LTARGET = "LTAR"
        RTARGET = "RTAR"
        COMPRESSION = "COMP"

    class MarkerSet(StrEnum):
        CENTER = "CENT"
        SA = "SA"
        SPAN = "SPAN"
        START = "STAR"
        STOP = "STOP"
        RLEVEL = "RLEV"
        DELAY = "DEL"
        CWFREQ = "CWFR"

    class MarkerType(StrEnum):
        NORMAL = "NORM"
        FIXED = "FIX"

    class OutDataType(StrEnum):
        """Enumerates the data type for transferring data.

        Below information is taken from NI PNA Help document.
        FDATA
            - Formatted measurement data to or from Data Access Map location
              Display (access point 2).
            - Note: When querying FDATA, data is received in degrees. When
              setting phase using FDATA, the command expects the data in
              radians.
            - Corrected data is returned when correction is ON.
            - Uncorrected data is returned when correction is OFF.
            - Returns TWO numbers per data point for Polar and Smith Chart
              format.
            - Returns one number per data point for all other formats.
            - Format of the read data is same as the displayed format.

        RDATA
            - Complex measurement data.
            - Writes data to Data Access Map location Raw Measurement (access
              point 0).
            - When writing corrected data, and correction is ON, it will be
              corrected again, resulting in meaningless data (Same behavior as
              SDATA).
            - Reads data from Data Access Map location Raw Measurement (access
              point 0).
            - Returns TWO numbers per data point.
            - Returned numbers are uncorrected (regardless of correction state)

        SDATA
            - Complex measurement data.
            - Writes data to Data Access Map location Raw Measurement (access
              point 0).
            - When writing corrected data, and correction is ON, it will be
              corrected again, resulting in meaningless data.
            - Reads data from Apply Error Terms (access point 1).
            - Returns TWO numbers per data point.
            - Corrected data is returned when correction is ON.
            - Uncorrected data is returned when correction is OFF.

        FMEM
            - Formatted memory data to or from Data Access Map location Memory
              result  (access point 4).
            - Returns TWO numbers per data point for Polar and Smith Chart
              format.
            - Returns one number per data point for all other formats.
            - Format of the read data is same as the displayed format.
            - Returned data reflects the correction level (On|OFF) when the
              data was stored into memory.

        SMEM
            - Complex measurement data to or from Data Access Map location
              Memory  (access point 3).
            - Returns  TWO numbers per data point.
            - Returned data reflects the correction level (On|OFF) when the
              data was stored into memory.
            - Returned data reflects the correction level (On|OFF) when the
              data was stored into memory.

        SDIV
            - Complex data from Data Access Map location Normalization (5).
            - Returns TWO numbers per data point.
            - If normalization interpolation is ON and the number of points
              changes after the initial normalization, the divisor data will
              then be interpolated.
            - When querying the normalization divisor, you must first store a
              divisor trace using CALC:NORMalize[:IMMediate].

        """
        FDATA = "FDATA"
        RDATA = "RDATA"
        SDATA = "SDATA"
        FMEM = "FMEM"
        SMEM = "SMEM"
        SDIV = "SDIV"

    class DisplayFormat(StrEnum):
        MLINEAR = "MLIN"
        MLOGARITHMIC = "MLOG"
        IMPEDANCE = "IMP"
        ADMITTANCE = "ADM"
        PHASE = "PHAS"
        UPHASE = "UPH"
        IMAGINARY = "IMAG"
        REAL = "REAL"
        POLAR = "POL"
        GDELAY = "GDEL"
        LINPHASE = "LINP"
        LOGPHASE = "LOGP"
        KELVIN = "KELV"
        FAHRENHEIT = "FAHR"
        CELSIUS = "CELSIUS"

    class AverageMode(StrEnum):
        """Averaging Type.
        Choose from:
            POINT: Averaging measurements are made on each data point before
                stepping to the next data point.
            SWEEP: Averaging measurements are made on subsequent sweeps until
                the required number of averaging sweeps are performed.
        """
        POINT = "POIN"
        SWEEP = "SWEEP"

    class FOMRangeName(StrEnum):
        PRIMARY = "Primary"
        SOURCE = "Source"
        RECEIVERS = "Receivers"
        SOURCE2 = "Source2"

    def _query_state(self, s: str) -> State:
        q = self.query(s)
        return self.State.from_str(q)

    def query_idn(self) -> str:
        return self.query("*IDN?")

    def query_opc(self) -> State:
        return self.State.from_str(self.query("*OPC?"))

    def set_output(self, state: StrInt):
        return self.write(f"OUTP {state}")

    def query_output_state(self) -> State:
        """Queries the status of output."""
        q = self.query("OUTP?")
        return self.State.from_str(q)

    def query_source_catalog(self) -> t.List[str]:
        """Returns a list of valid port names that can be controlled."""
        q = self.query_str("SOUR:CAT?")
        return q.split(',')

    def set_source_power_mode(
            self, state: str, port: int = 1, cnum: int = 1,
            src: t.Optional[str] = None):
        """Sets the state of the power for the given source.

        Args:
            cnum: Channel number, defaults to 1.
            port: Source port number of the PNA.
            state: State must be one from ["AUTO", "ON", "OFF", "NOCTL"].
                Choose "AUTO" if unsure.
            src: Some VNA have sources that are not simple enumeration of
                ports.  If one of the main VNA ports are referenced here, it
                takes precedence over the `port` argument.
        """
        cmd = f"SOUR{cnum:d}:POW{port:d}:MODE {state}{_optarg(src)}"
        return self.write(cmd)

    def query_source_power_mode(
            self, port: int = 1, cnum: int = 1,
            src: t.Optional[str] = None) -> SourcePowerMode:
        cmd = f"SOUR{cnum:d}:POW{port:d}:MODE?{_optarg(src, False)}"
        q = self.query(cmd)
        return self.SourcePowerMode.from_str(q)

    def set_source_power_level(
            self, num_dbm: float, port: int = 1, cnum: int = 1,
            src: t.Optional[str] = None):
        cmd = f"SOUR{cnum:d}:POW{port:d}:LEV {num_dbm: f}{_optarg(src)}"
        return self.write(cmd)

    def query_source_power_level(
            self, port: int = 1, cnum: int = 1,
            src: t.Optional[str] = None) -> float:
        """TODO: Add MAX, MIN querying."""
        cmd = f"SOUR{cnum:d}:POW{port:d}:LEV?{_optarg(src)}"
        return self.query_float(cmd)

    def set_sense_frequency_center(
            self, num: t.Union[float, str], cnum: int = 1):
        """Sets the center frequency given in Hz units, or one of ['MIN',
        'MAX']
        """
        return self.write(f"SENS{cnum:d}:FREQ:CENT {num: f}")

    def query_sense_frequency_center(
            self, cnum: int = 1) -> float:
        """Queries the center frequency given in Hz units."""
        return self.query_float(f"SENS{cnum:d}:FREQ:CENT?")

    def set_sense_frequency_span(
            self, num: t.Union[float, str], cnum: int = 1):
        """Sets the span given in Hz units, or one of ['MIN',
        'MAX']
        """
        return self.write(f"SENS{cnum:d}:FREQ:SPAN {num: f}")

    def query_sense_frequency_span(
            self, cnum: int = 1) -> float:
        """Sets the span given in Hz units"""
        return self.query_float(f"SENS{cnum:d}:FREQ:SPAN?")

    def set_sense_frequency_start(
            self, num: float, cnum: int = 1):
        """Sets the start frequency given in Hz units."""
        return self.write(f"SENS{cnum:d}:FREQ:STAR {num:f}")

    def query_sense_frequency_start(
            self, cnum: int = 1) -> float:
        """Queries the start frequency given in Hz units."""
        return self.query_float(f"SENS{cnum:d}:FREQ:STAR?")

    def set_sense_frequency_stop(
            self, num: float, cnum: int = 1):
        """Sets the stop frequency given in Hz units."""
        return self.write(f"SENS{cnum:d}:FREQ:STOP {num: f}")

    def query_sense_frequency_stop(
            self, cnum: int = 1) -> float:
        """Queries the start frequency given in Hz units."""
        return self.query_float(f"SENS{cnum:d}:FREQ:STOP?")

    def do_sense_average_clear(self, cnum: int = 1):
        """Clears and restarts averaging of the measurement data."""
        self.write(f"SENS{cnum}:AVER:CLE")

    def set_sense_average_count(self, navg: int, cnum: int = 1):
        """Sets the number of measurements to combine for an average."""
        self.write(f"SENS{cnum}:AVER:COUN {navg}")

    def query_sense_average_count(self, cnum: int = 1) -> int:
        """Queries the number of measurements to combine for an average."""
        return self.query_int(f"SENS{cnum}:AVER:COUN?")

    def set_sense_average_mode(
            self, mode: AverageMode, cnum: int = 1):
        """Sets the averaging mode to point or sweep."""
        self.write(f"SENS{cnum}:AVER:MODE {mode}")

    def query_sense_average_mode(
            self, mode: AverageMode, cnum: int = 1) -> AverageMode:
        """Queries the averaging mode."""
        q = self.query(f"SENS{cnum}:AVER:MODE?")
        return self.AverageMode.from_str(q)

    def set_sense_average_state(self, state: State, cnum: int = 1):
        """Turns trace averaging ON or OFF."""
        self.write(f"SENS{cnum}:AVER:STAT {state}")

    def query_sense_average_state(self, cnum: int = 1) -> State:
        """Queries the state of trace averaging."""
        self._query_state(f"SENS{cnum}:AVER:STAT?")

    def set_sense_correction_state(self, state: State, cnum: int = 1):
        """Turns the calibration correction ON or OFF."""
        self.write(f"SENS{cnum}:CORR {state}")

    def query_sense_correction_state(self, cnum: int = 1) -> "State":
        """Queries the state of calibration."""
        return self._query_state(f"SENS{cnum}:CORR?")

    def query_sense_correction_cset_activate(
            self, opt: str = "NAME", cnum: int = 1) -> str:
        """Queries and returns the calset name (opt="NAME") or calset guid
        (opt='GUID')"""
        return self.query_str(f"SENS{cnum}:CORR:CSET:ACT? {opt}")

    def set_sense_correction_cset_activate(
            self,
            nameorguid: str,
            apply_stimulus_settings: State = State.OFF,
            cnum: int = 1) -> str:
        """Queries and returns the calset name (opt="NAME") or calset guid
        (opt='GUID')"""
        ass = apply_stimulus_settings
        return self.write(f'SENS{cnum}:CORR:CSET:ACT "{nameorguid}",{ass}')

    def set_sense_fom_state(self, state: State, cnum: int = 1):
        return self.write(f"SENS{cnum}:FOM {state}")

    def query_sense_fom_state(self, cnum: int = 1):
        return self._query_state(f"SENS{cnum}:FOM?")

    def query_sense_fom_catalog(self, cnum: int = 1) -> str:
        return self.query_str(f"SENS{cnum}:FOM:CAT?")

    def query_sense_fom_count(self, cnum: int = 1) -> int:
        return self.query_int(f"SENS{cnum}:FOM:COUN?")

    def set_sense_fom_display_select(
            self, range_name: t.Union[str, FOMRangeName], cnum: int = 1):
        """Selects the range to be displayed on x-axis.  All traces in the
        channel have this same x-axis scaling.
        """
        return self.write(f'SENS{cnum}:FOM:DISP:SEL "{range_name}"')

    def query_sense_fom_display_select(
            self, cnum: int = 1) -> str:
        """Returns the range displayed on x-axis.  All traces in the
        channel have this same x-axis scaling.
        """
        return self.query_str(f"SENS{cnum}:FOM:DISP:SEL?")

    def query_sense_fom_rnum(
            self, range_name: t.Union[str, FOMRangeName],
            cnum: int = 1) -> int:
        """Returns the number of a specified range name."""
        return self.query_int(f'SENS{cnum}:FOM:RNUM? "{range_name}"')

    def set_sense_fom_range_coupled(
            self, rnum: int, state: State, cnum: int = 1):
        """Sets the coupling state of the range specified by `rnum` to the
        Primary range"""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum} {state}")

    def query_sense_fom_range_coupled(
            self, rnum: int, cnum: int = 1) -> "State":
        """Queries the coupling state of the range specified by `rnum` to the
        Primary range"""
        return self._query_state(f"SENS{cnum}:FOM:RANG{rnum}?")

    def set_sense_fom_range_frequency_cw(
            self, rnum: int, freq: float, cnum: int = 1):
        """Sets the CW frequency."""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:CW {freq}")

    def query_sense_fom_range_frequency_cw(
            self, rnum: int, cnum: int = 1) -> float:
        """Queries the CW frequency."""
        return self.query_float(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:CW?")

    def set_sense_fom_range_frequency_divisor(
            self, rnum: int, divisor: int, cnum: int = 1):
        """Sets the divisor for FOM."""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:DIV {divisor}")

    def query_sense_fom_range_frequency_divisor(
            self, rnum: int, cnum: int = 1) -> int:
        """Queries the divisor for FOM."""
        return self.query_int(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:DIV?")

    def set_sense_fom_range_frequency_multiplier(
            self, rnum: int, multiplier: int, cnum: int = 1):
        """Sets the multiplier for FOM."""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:MULT {multiplier}")

    def query_sense_fom_range_frequency_multiplier(
            self, rnum: int, cnum: int = 1) -> int:
        """Queries the multiplier for FOM."""
        return self.query_int(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:MULT?")

    def set_sense_fom_range_frequency_offset(
            self, rnum: int, offset: float, cnum: int = 1):
        """Sets the offset for FOM."""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:OFFS {offset}")

    def query_sense_fom_range_frequency_offset(
            self, rnum: int, cnum: int = 1) -> float:
        """Queries the offset for FOM."""
        return self.query_float(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:OFFS?")

    def set_sense_fom_range_frequency_start(
            self, rnum: int, start: float, cnum: int = 1):
        """Sets the start for FOM."""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:STAR {start}")

    def query_sense_fom_range_frequency_start(
            self, rnum: int, cnum: int = 1) -> float:
        """Queries the start for FOM."""
        return self.query_float(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:STAR?")

    def set_sense_fom_range_frequency_stop(
            self, rnum: int, stop: float, cnum: int = 1):
        """Sets the stop for FOM."""
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:STOP {stop}")

    def query_sense_fom_range_frequency_stop(
            self, rnum: int, cnum: int = 1) -> float:
        """Queries the stop for FOM."""
        return self.query_float(f"SENS{cnum}:FOM:RANG{rnum}:FREQ:STOP?")

    def query_sense_fom_range_name(
            self, rnum: int, cnum: int = 1) -> str:
        return self.query_str(f"SENS{cnum}:FOM:RANG{rnum}:NAME?")

    def set_sense_fom_range_sweep_type(
            self, rnum: int, sweep_type: SweepType, cnum: int = 1):
        styp = sweep_type
        return self.write(f"SENS{cnum}:FOM:RANG{rnum}:SWE:TYPE {styp}")

    def query_sense_fom_range_sweep_type(
            self, rnum: int, cnum: int = 1) -> "SweepType":
        q = self.query(f"SENS{cnum}:FOM:RANG{rnum}:SWE:TYPE?")
        return self.SweepType.from_str(q)

    def query_cset_catalog(self) -> t.List[str]:
        """Queries the list of available CalSets"""
        q = self.query_str("CSET:CAT?")
        return q.split(',')

    def do_cset_copy(self, cset1: str, cset2: str):
        """Creates a new Cal Set and copies the current Cal Set data into
        it."""
        return self.write(f"CSET:COPY '{cset1}' '{cset2}'")

    def query_cset_date(self, cset: str) -> str:
        """Returns the (year, month, day) that the specified Cal Set was last
        saved."""
        return self.query(f'CSET:DATE? "{cset}"')

    def query_cset_exists(self, cset: str) -> State:
        """Returns whether or not the Cal Set exists on the PNA."""
        return self._query_state(f'CSET:EXIST? "{cset}"')

    def do_cset_fixture_cascade(
            self, s2p1: str, s2p2: str, s2p_result: str, format: str):
        """Combines the losses and phase shift of two S2P into a single S2P
        file.

        Args:
            s2p1: Path and filename of one of the s2p files to be combined.
            s2p2: Path and filename of the other s2p file.
            s2p_result:  Path and filename for the resulting s2p file.
            format:  One of ['REIM', 'LOG', 'LIN'] where the first is real,
                imaginary data pairs, second is log magnitude, phase and the
                last one is linear magnitude and phase.

        """
        args = f'"{s2p1}", "{s2p2}", "{s2p2}", "{s2p_result}", {format}'
        return self.write(f"CSET:FIXT:CASC {args}")

    def do_cset_fixture_characterize(
            self, cs1: str, cs2: str, port: int, s2p: str, format: str,
            pivot: t.Optional[float] = None):
        """Characterize a fixture based on two Cal Sets

        Args:
            cs1: Name of cal set 1
            cs2: Name of cal set 2
            port: Port number described in the Cal Sets
            s2p: Name of the s2p file containing the adapter/fixture
                characterization.
            format:  One of ['REIM', 'LOG', 'LIN'] where the first is real,
                imaginary data pairs, second is log magnitude, phase and the
                last one is linear magnitude and phase.

        """
        optarg = "" if pivot is None else f", {pivot}"
        args = f'"{cs1}", "{cs2}", {port}, "{s2p}", {format}{optarg}'
        return self.write(f"CSET:FIXT:CHAR {args}")

    def do_cset_fixture_deembed(
            self, cs1: str, cs2: str, s2p: str, port: int, comp_pwr: bool,
            extrap: t.Optional[bool] = None):
        """De-embeds a fixture from an existing Cal Set based on an S2P file.
        The new Cal Set is created with the effects of the fixture removed.
        Args:
            cs1: Name of an existing Cal Set residing on the PNA.
            cs2: Name of the new Cal Set.
            s2p: Name of th s2p file characterizing the adapter/fixture.
            port: Port number from which the fixture will be deembedded.
            comp_pwr: Decides whether or not to apply power corrections in the
            case that the cal set contains power calibrations.
        """
        optarg = "" if extrap is None else f", {extrap:d}"
        args = f'"{cs1}", "{cs2}", "{s2p}", {port}, {comp_pwr:d}{optarg}'
        return self.write(f"CSET:FIXT:DEEM {args}")

    def do_cset_fixture_embed(
            self, cs1: str, cs2: str, s2p: str, port: int, comp_pwr: bool,
            extrap: t.Optional[bool] = None):
        """Embeds a fixture from an existing Cal Set based on an S2P file.
        The new Cal Set is created with the effects of the fixture added.
        Args:
            cs1: Name of an existing Cal Set residing on the PNA.
            cs2: Name of the new Cal Set.
            s2p: Name of th s2p file characterizing the adapter/fixture.
            port: Port number from which the fixture will be embedded.
            comp_pwr: Decides whether or not to apply power corrections in the
            case that the cal set contains power calibrations.
        """
        optarg = "" if extrap is None else f", {extrap:d}"
        args = f'"{cs1}", "{cs2}", "{s2p}", {port}, {comp_pwr:d}{optarg}'
        return self.write(f"CSET:FIXT:EMB {args}")

    def set_format_byte_order(self, border: ByteOrderFormat):
        """Sets the byte order, choose from ['NORMal, 'SWAPped'].
        NORM is big endian, SWAP is little endian."""
        return self.write(f"FORM:BORD {border}")

    def query_format_byte_order(self) -> ByteOrderFormat:
        """Queries the byte order, choose from ['NORMal, 'SWAPped'].
        NORM is big endian, SWAP is little endian."""
        q = self.query(f"FORM:BORD?")
        return self.ByteOrderFormat.from_str(q)

    def set_format_data(self, dataformat: FormatDataType):
        """Always use REAL,64 or ASCII for frequency data! The power data is
        stored as REAL,32, so you may use that for it."""
        return self.write(f"FORM {dataformat}")

    def query_format_data(self) -> FormatDataType:
        q = self.query("FORM?")
        return self.FormatDataType.from_str(q)

    def set_sense_sweep_dwell(self, dtime: StrNum, cnum: int = 1):
        """Sets dwell time in seconds"""
        return self.write(f"SENS{cnum:d}:SWE:DWEL {dtime}")

    def query_sense_sweep_dwell(self, cnum: int = 1) -> float:
        return self.query_float(f"SENS{cnum:d}:SWE:DWEL?")

    def set_sense_sweep_dwell_auto(self, state: State, cnum: int = 1):
        """Sets dwell time automatic setting status."""
        return self.write(f"SENS{cnum:d}:SWE:DWEL:AUTO {state}")

    def query_sense_sweep_dwell_auto(self, cnum: int = 1) -> State:
        """Queries the dwell time auto setting."""
        q = self.query(f"SENS{cnum:d}:SWE:DWEL:AUTO?")
        return self.State.from_str(q)

    def set_sense_sweep_dwell_delay(self, dltime: StrNum, cnum: int = 1):
        """Sets the additional delay in seconds before acquisition begins for
        each sweep.  Addition to dwell time."""
        return self.write(f"SENS{cnum:d}:SWE:DWEL:SDEL {dltime}")

    def query_sense_sweep_dwell_delay(self, cnum: int = 1) -> float:
        return self.query_float(f"SENS{cnum:d}:SWE:DWEL:SDEL?")

    def set_sense_sweep_generation(
            self, swgen: SweepGeneration, cnum: int = 1):
        """Sets sweep as Stepped or Analog."""
        return self.write(f"SENS{cnum:d}:SWE:GEN {swgen}")

    def query_sense_sweep_generation(self, cnum: int = 1) -> SweepGeneration:
        q = self.query(f"SENS{cnum:d}:SWE:GEN?")
        return self.SweepGeneration.from_str(q)

    def set_sense_sweep_generation_pointsweep(
            self, state: State, cnum: int = 1):
        """Sets the state of point sweep mode."""
        return self.write(f"SENS{cnum:d}:SWE:GEN:POIN {state}")

    def query_sense_sweep_generation_pointsweep(self, cnum: int = 1) -> State:
        q = self.query(f"SENS{cnum:d}:SWE:GEN:POIN?")
        return self.State.from_str(q)

    def set_sense_sweep_groups_count(self, count: int, cnum: int = 1):
        """Sets the trigger count (groups) for the specified channel.  Set
        trigger mode to group after setting this."""
        return self.write(f"SENS{cnum:d}:SWE:GRO:COUN {count}")

    def query_sense_sweep_groups_count(self, cnum: int = 1) -> int:
        return self.query_int(f"SENS{cnum:d}:SWE:GRO:COUN?")

    def set_sense_sweep_type(self, sweep_type: SweepType, cnum: int = 1):
        """Sets the sweep type for the given channel `cnum`"""
        return self.write(f"SENS{cnum:d}:SWE:TYPE {sweep_type}")

    def query_sense_sweep_type(self, cnum: int = 1) -> SweepType:
        """Sets the sweep type for the given channel `cnum`"""
        q = self.query(f"SENS{cnum:d}:SWE:TYPE?")
        return self.SweepType.from_str(q)

    def set_sense_sweep_time(self, num: StrNum, cnum: int = 1):
        """Sets the time the analyzer takes to complete one sweep.
        Args:
            num: Sweep time in seconds.  Accepts time unit along with the
                number as well, e.g. "1ms".  Also "MIN", "MAX" is possible.
            cnum: Channel number
        """
        return self.write(f"SENS{cnum:d}:SWE:TIME {num}")

    def query_sense_sweep_time(self, cnum: int = 1) -> float:
        return self.query_float(f"SENS{cnum:d}:SWE:TIME?")

    def set_sense_sweep_time_auto(
            self, state: State, cnum: int = 1):
        return self.write(f"SENS{cnum:d}:SWE:TIME:AUTO {state}")

    def query_sense_sweep_time_auto(self, cnum: int = 1) -> State:
        """Returns the `State` of the auto sweep-time setting."""
        q = self.query(f"SENS{cnum:d}:SWE:TIME:AUTO?")
        return self.State.from_str(q)

    def set_sense_sweep_points(self, num: StrInt, cnum: int = 1):
        """Sets the sweep points.

        Args:
            num: Number of points.  Any number between 1 and maximum number
                allowed by the PNA.  Also accepts "MIN" and "MAX".
        """
        return self.write(f"SENS{cnum:d}:SWE:POIN {num}")

    def query_sense_sweep_points(self, cnum: int = 1) -> int:
        """Queries the sweep points."""
        return self.query_int(f"SENS{cnum:d}:SWE:POIN?")

    def set_sense_sweep_mode(self, mode: SweepMode, cnum: int = 1):
        """Sets the number of trigger signals the specified channel will
        accept.
        If SINGLE is selected and the trigger source is internal, then
        a trigger is issued.
        """
        return self.write(f"SENS{cnum:d}:SWE:MODE {mode}")

    def query_sense_sweep_mode(self, cnum: int = 1) -> SweepMode:
        q = self.query(f"SENS{cnum:d}:SWE:MODE?")
        return self.SweepMode.from_str(q)

    def set_sense_sweep_speed(self, speed: SweepSpeed, cnum: int = 1):
        """Sets the sweep speed to `speed`."""
        return self.write(f"SENS{cnum:d}:SWE:SPE {speed}")

    def query_sense_sweep_speed(self, cnum: int = 1) -> SweepSpeed:
        """Returns the `SweepSpeed` of the Fast Sweep mode."""
        q = self.query(f"SENS{cnum:d}:SWE:SPE?")
        return self.SweepSpeed.from_str(q)

    def set_sense_sweep_step(self, step: StrNum, cnum: int = 1):
        """Sets the frequency step in Hz. Available only when SweepType ==
        Linear."""
        return self.write(f"SENS{cnum:d}:SWE:STEP {step}")

    def query_sense_sweep_step(self, cnum: int = 1) -> float:
        return self.query_float(f"SENS{cnum:d}:SWE:STEP?")

    def set_sense_sweep_trigger_mode(
            self, mode: SweepTriggerMode, cnum: int = 1):
        return self.write(f"SENS{cnum:d}:SWE:TRIG:MODE {mode}")

    def set_sense_bandwidth(self, bandwidth: StrNum, cnum: int = 1):
        return self.write(f"SENS{cnum:d}:BWID {bandwidth}")

    def query_sense_bandwidth(self, cnum: int = 1) -> float:
        """Returns the IF bandwidth setting."""
        return self.query_float(f"SENS{cnum:d}:BWID?")

    def set_initiate_continuous(self, state: State):
        """Specifies whether the PNA trigger source is internal or Manual."""
        return self.write(f"INIT:CONT {state}")

    def query_initiate_continuous(self) -> "State":
        """Returns the `State` of the continuous triggering mode."""
        return self.State.from_str(self.query(f"INIT:CONT?"))

    def do_initiate(self, cnum: int = 1):
        """Sends an immediate trigger signal.  Requires trigger source to be
        set to manual."""
        return self.write(f"INIT{cnum}")

    def set_trigger_source(self, source: TriggerSource):
        """Sets the trigger source."""
        return self.write(f"TRIG:SOUR {source}")

    def query_trigger_source(self) -> "TriggerSource":
        """Queries the trigger source."""
        q = self.query("TRIG:SOUR?")
        return self.TriggerSource.from_str(q)

    def set_trigger_scope(self, scope: TriggerScope):
        return self.write(f"TRIG:SCOP {scope}")

    def query_trigger_scope(self) -> "TriggerScope":
        q = self.query("TRIG:SCOP?")
        return self.TriggerScope.from_str(q)

    def do_abort(self):
        """Stops all sweeps."""
        return self.write("ABOR")

    def set_calculate_smoothing(self, state: State, cnum: int = 1):
        """Turns smoothing ON or OFF."""
        return self.write(f"CALC{cnum:d}:SMO {state}")

    def query_calculate_smoothing(self, cnum: int = 1) -> 'State':
        """Turns smoothing ON or OFF."""
        return self._query_state(f"CALC{cnum:d}:SMO?")

    def query_calculate_parameter_catalog_ext(
            self, cnum: int = 1) -> t.List[t.Tuple[str, str]]:
        """Returns the names and parameters of existing measurements for the
        specified channel.
        Returns:
            out(str):
                "<measurement_name>,<parameter>,[<measurement_name>,<parameter>...]"
        """
        q = self.query(f"CALC{cnum:d}:PAR:CAT:EXT?").strip('"')
        ql = q.split(',')
        pairs = list(zip(*(islice(ql, i, None, 2) for i in range(2))))
        return pairs

    def do_calculate_parameter_define_ext(
            self, mname: str, param: str, cnum: int = 1):
        """Creates a measurement with the given name (mname) for the desired
        parameter (param).
        Args:
            mname:  Measurement name to create.  Must be unique.
            param: Measurement Parameter to create. Case sensitive.
                For S-parameters:
                    Any S-parameter available in the PNA
                    Single-digit port numbers CAN be separated by "_"
                        (underscore). For example: "S21" or "S2_1"
                    Double-digit port numbers MUST be separated by underscore.
                        For example: "S10_1"
                For ratioed measurements:
                    Any two PNA physical receivers separated by forward slash
                        '/' followed by comma and source port.
                    For example: "A/R1, 3"
                For non-ratioed measurements:
                    Any PNA physical receiver followed by comma and source port
                    For example: "A, 4"
        """
        return self.write(f"CALC{cnum:d}:PAR:EXT '{mname}','{param}'")

    def do_calculate_parameter_delete(self, mname: str, cnum: int = 1):
        """Deletes the specified measurement"""
        return self.write(f"CALC{cnum:d}:PAR:DEL '{mname}'")

    def do_calculate_parameter_delete_all(self, cnum: int = 1):
        """Deletes all measurement on the PNA."""
        return self.write(f"CALC{cnum:d}:PAR:DEL:ALL")

    def set_calculate_format(self, format: DisplayFormat, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:FORM {format}")

    def query_calculate_format(self, cnum: int = 1) -> DisplayFormat:
        q = self.query(f"CALC{cnum:d}:FORM?")
        return self.DisplayFormat.from_str(q)

    def set_calculate_parameter_mnumber_select(
            self, mnum: int, cnum: int = 1, isfast=False):
        """Selects the measurement based on the number of the trace as shown
        on the VNA screen like Tr1, Tr2"""
        opt = "fast" if isfast else None
        return self.write(f"CALC{cnum:d}:PAR:MNUM {mnum}{_optarg(opt)}")

    def query_calculate_parameter_mnumber_select(self, cnum: int = 1) -> int:
        """Returns which measurement is selected based on the Tr# numbers shown
        on the usual VNA display."""
        return self.query_int(f"CALC{cnum:d}:PAR:MNUM?")

    def do_calculate_parameter_modify_ext(self, param: str, cnum: int = 1):
        """Modifies the selected measurement using the same arguments as
        calc:par:def:ext"""
        return self.write(f"CALC{cnum:d}:PAR:MOD:EXT {param}")

    def set_calculate_parameter_mname_select(
            self, mname: str, cnum: int = 1, isfast=False):
        """Selects the measurement based on the measurement name.  When 'fast'
        VNA display is not updated."""
        opt = "fast" if isfast else None
        return self.write(f"CALC{cnum:d}:PAR:SEL '{mname}'{_optarg(opt)}")

    def query_calculate_parameter_mname_select(self, cnum: int = 1) -> str:
        return self.query(f"CALC{cnum:d}:PAR:SEL?")

    def query_calculate_parameter_tag_next(self, cnum: int = 1) -> str:
        """Returns a string that is guaranteed to be unique and valid for use
        with CALC:PAR:DEF.
        """
        return self.query(f"CALC{cnum:d}:PAR:TAG:NEXT?")

    def query_calculate_parameter_tnumber(self, cnum: int = 1) -> int:
        """Queries the trace number.  Note that trace number is not the one
        appears as Tr# on the screen."""
        return self.query_int(f"CALC{cnum:d}:PAR:TNUM?")

    def query_calculate_parameter_wnumber(self, cnum: int = 1) -> int:
        return self.query_int(f"CALC{cnum:d}:PAR:WNUM?")

    def query_calculate_x_values(
            self,
            format: FormatDataType,
            byteorder: str = 'SWAP',
            cnum: int = 1) -> np.ndarray:
        """Returns the stimulus values for the selected measurement in the
        current units."""
        s = f"CALC{cnum:d}:X:VAL?"
        if format == self.FormatDataType.ASCII:
            return self.query_ascii_values(s)
        elif format == self.FormatDataType.REAL32:
            # Use logger!
            logger.warning("Frequency values may be erroneous!")
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'f', isbigendian)
        elif format == self.FormatDataType.REAL64:
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'd', isbigendian)

    def do_calculate_marker_all_off(self, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK:AOFF")

    def set_calculate_marker_bucket(
            self, bucket: int, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:BUCK {bucket:d}")

    def query_calculate_marker_bucket(
            self, markernum: int = 1, cnum: int = 1) -> int:
        return self.query_int(f"CALC{cnum:d}:MARK{markernum:d}:BUCK?")

    def set_calculate_marker_format(
            self, format: MarkerFormat, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:FORM {format}")

    def query_calculate_marker_format(
            self, markernum: int = 1, cnum: int = 1) -> MarkerFormat:
        q = self.query(f"CALC{cnum:d}:MARK{markernum:d}:FORM?")
        return self.MarkerFormat.from_str(q)

    def set_calculate_marker_function_apeak_excursion(
            self, excursion: StrNum, markernum: int = 1, cnum: int = 1):
        return self.write(
                f"CALC{cnum:d}:MARK{markernum:d}:FUNC:APE:EXC {excursion}")

    def query_calculate_marker_function_apeak_excursion(
            self, markernum: int = 1, cnum: int = 1) -> float:
        return self.query_float(
                f"CALC{cnum:d}:MARK{markernum:d}:FUNC:APE:EXC?")

    def set_calculate_marker_function_apeak_threshold(
            self, threshold: StrNum, markernum: int = 1, cnum: int = 1):
        return self.write(
                f"CALC{cnum:d}:MARK{markernum:d}:FUNC:APE:THR {threshold}")

    def query_calculate_marker_function_apeak_threshold(
            self, markernum: int = 1, cnum: int = 1) -> float:
        return self.query_float(
                f"CALC{cnum:d}:MARK{markernum:d}:FUNC:APE:THR?")

    def do_calculate_marker_function_execute(
            self, func: MarkerFunction, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:FUNC:EXEC {func}")

    def do_calculate_marker_set(
            self, mset: MarkerSet, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:SET {mset}")

    def set_calculate_marker_state(
            self, state: State, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d} {state}")

    def query_calculate_marker_state(
            self, markernum: int = 1, cnum: int = 1) -> State:
        q = self.query(f"CALC{cnum:d}:MARK{markernum:d}?")
        return self.State.from_str(q)

    def set_calculate_marker_type(
            self, mtype: MarkerType, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:TYPE {mtype}")

    def query_calculate_marker_type(
            self, markernum: int = 1, cnum: int = 1) -> MarkerType:
        q = self.query(f"CALC{cnum:d}:MARK{markernum:d}:TYPE?")
        return self.MarkerType.from_str(q)

    def set_calculate_marker_x(
            self, xval: StrNum, markernum: int = 1, cnum: int = 1):
        return self.write(f"CALC{cnum:d}:MARK{markernum:d}:X {xval}")

    def query_calculate_marker_x(
            self, markernum: int = 1, cnum: int = 1) -> float:
        return self.query_float(f"CALC{cnum:d}:MARK{markernum:d}:X?")

    def query_calculate_marker_y(
            self, markernum: int = 1, cnum: int = 1) -> float:
        return self.query_float(f"CALC{cnum:d}:MARK{markernum:d}:Y?")

    def query_calculate_data(
            self,
            datatype: OutDataType,
            format: FormatDataType,
            byteorder: ByteOrderFormat = 'SWAP',
            cnum: int = 1) -> np.ndarray:
        """Queries the data and returns it as an array.  The meaning of values
        in the resulting array depends on the `datatype` argument.

        Returning array if datatype is;
            FDATA:
                - Returns calibrated if correction is on.
                - Returns two value per frequency if display format is
                  polar or smith chart.
            RDATA:
                - Returns complex measurement data, two values per point.
                - Insensitive to the state of correction setting.
            SDATA:
                - Returns complex measurement data, two values per point.
                - Returns calibrated if correction is on.
            FMEM:
                - Like FDATA but returns the data stored in the memory.
                - Correction state is fetched from the setting when the data
                  was first stored in the memory.
            SMEM:
                - SDATA version of FMEM.

        """
        s = f"CALC{cnum:d}:DATA? {datatype}"
        if format == self.FormatDataType.ASCII:
            return self.query_ascii_values(s)
        elif format == self.FormatDataType.REAL32:
            # Use logger!
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'f', isbigendian)
        elif format == self.FormatDataType.REAL64:
            isbigendian = byteorder.lower().startswith('norm')
            return self.query_binary_values(s, 'd', isbigendian)

    def do_display_window_trace_y_scale_auto(
            self, wnum: int = 1, tnum: int = 1):
        return self.write(f"DISP:WIND{wnum:d}:TRAC{tnum:d}:Y:AUTO")

    def do_display_window_trace_feed(
            self, mname: str, tnum: StrInt = "", wnum: int = 1):
        """Creates a new trace in `wnum` with trace reference `tnum` and feeds
        the measurement with `mname` to it.

        `tnum` is NOT the trace number shown as Tr# on the PNA screen.  It's an
        internal number used only by SCPI.
        """
        return self.write(f"DISP:WIND{wnum:d}:TRAC{tnum}:FEED '{mname}'")

    def query_display_window_catalog(
            self, wnum: int = 1) -> t.List[int]:
        """Returns a list of Trace Numbers that are displayed on window
        `wnum`."""
        q = self.query(f"DISP:WIND{wnum}:CAT?").strip('"')
        # This guy returns "EMPTY" when no traces are displayed
        try:
            return list(map(int, q.split(',')))
        except ValueError:
            # mapping int on ["EMPTY"] throws this
            return []

    def set_calculate_equation_text(self, equation: str, cnum: int = 1):
        return self.write(f'CALC{cnum:d}:EQU:TEXT "{equation}"')

    def query_calculate_equation_text(self, cnum: int = 1) -> str:
        return self.query(f"CALC{cnum:d}:EQU:TEXT?")

    def set_calculate_equation_state(self, state: State, cnum: int = 1):
        return self.write(f'CALC{cnum:d}:EQU:STAT {state}')

    def query_calculate_equation_state(self, cnum: int = 1) -> State:
        return self.State.from_str(self.query(f"CALC{cnum:d}:EQU:STAT?"))

    def query_system_active_channel(self) -> int:
        """Returns the number of the channel that contains the active
        measurement.
        """
        return self.query_int("SYST:ACT:CHAN?")

    def query_system_active_measurement(self) -> str:
        """Returns the name of the active measurement."""
        return self.query_str("SYST:ACT:MEAS?")

    def query_system_channels_catalog(self) -> t.List[int]:
        """Returns the channel numbers that are in use."""
        return list(map(
            int,
            self.query_str("SYST:CHAN:CAT?").split(',').strip()
            ))

    def query_system_error(self) -> str:
        """Returns the next error in error queue"""
        return self.query("SYST:ERR?")

    def query_system_error_count(self) -> int:
        """Returns the number of errors in the queue."""
        return self.query_int("SYST:ERR:COUN?")

    def do_system_macro_copy_channel_to(
            self, fromc: int, toc: int):
        return self.write(f"SYST:MACR:COPY:CHAN{fromc} {toc}")

    def do_system_channel_delete(self, cnum: int):
        return self.write(f"SYST:CHAN:DEL {cnum}")
