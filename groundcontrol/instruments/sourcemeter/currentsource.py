import typing as t
from functools import partial
from numbers import Number
import time

import attr

from groundcontrol.instruments import VISAInstrument
from groundcontrol.helper import StrEnum, EnumNotFoundError


_CURRENTSOURCEPRESET = """-- A preset script for current sourcing
smua.source.func = smua.OUTPUT_DCAMPS
format.asciiprecision = 16
smua.source.rangei = {rangei:f}
smua.source.leveli = {leveli:f}
smua.source.output = smua.OUTPUT_{state:s}
"""


@attr.s(auto_attribs=True)
class TSPCurrentSource(VISAInstrument):
    """Keithley brand sourcemeters have TSP scripts that use Lua based commands
    for control"""
    query_delay: float = 0.01  # 10 ms

    class SourceFunction(StrEnum):
        CURRENT = "smua.OUTPUT_DCAMPS"
        VOLTAGE = "smua.OUTPUT_DCVOLTS"

        @classmethod
        def from_str(cls, s: str) -> "TSPCurrentSource.SourceFunction":
            # returns 1.000 or 0.000
            fl = float(s)
            if fl == 1.0:
                return cls.VOLTAGE
            elif fl == 0.0:
                return cls.CURRENT
            else:
                raise EnumNotFoundError(f"Unknown response ({s})")

    class State(StrEnum):
        ON = "ON"
        OFF = "OFF"

    def query_idn(self) -> str:
        return super().query("*IDN?")

    def do_cls(self) -> str:
        return super().write("*CLS")

    def do_waitcomplete(self) -> str:
        return self.write("waitcomplete()")

    def do_dataqueue_clear(self) -> str:
        return self.write("dataqueue.clear()")

    def write(self, s: str):
        # Workaround for syncing problem
        self.resource.clear()
        return super().write(s)

    def query(self, s: str) -> str:
        self.write(f"print({s})")
        time.sleep(self.query_delay)
        return self.read().strip()

    def set_source_function(self, sourcefun: SourceFunction):
        return self.write(f"smua.source.func = {sourcefun}")

    def query_source_function(self) -> "SourceFunction":
        return self.SourceFunction.from_str(self.query("smua.source.func"))

    def send_script(self, script_str: str):
        self.write("loadandrunscript")
        for line in script_str.split('\n'):
            self.write(line)
        return self.write("endscript")

    def do_reset(self):
        return self.write('reset()')

    def do_current_source_preset(
            self,
            rangei: float,
            leveli: float,
            state: State):
        script = _CURRENTSOURCEPRESET.format(
                rangei=rangei,
                leveli=leveli,
                state=state)
        return self.send_script(script)

    def query_measure_i(self) -> float:
        """Queries a current measurement.
        """
        return self.query_float("print(smua.measure.i())")

    def set_output_state(self, state: State):
        return self.write(f"smua.source.output = smua.OUTPUT_{state}")

    def query_output_state(self) -> "State":
        # for some reason the output is in 1.0000000e+00 form
        qf = float(self.query("smua.source.output"))
        if qf == 1.0:
            qstr = "ON"
        else:
            qstr = "OFF"
        return self.State.from_str(qstr)

    def set_level_i(self, leveli: float):
        return self.write(f"smua.source.leveli = {leveli:.16f}")

    def query_level_i(self) -> float:
        return self.query_float(f"smua.source.leveli")

    def set_range_i(self, rangei: float):
        return self.write(f"smua.source.rangei = {rangei:.16f}")

    def query_range_i(self) -> float:
        return self.query_float(f"smua.source.rangei")

    @classmethod
    def create(cls, resource, name):
        if resource.resource_class == 'SOCKET':
            resource.read_termination = '\n'
        return cls(resource, name)
