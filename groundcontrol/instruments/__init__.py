"""Instruments are defined in the folders of their respective instrument
categories.

Note that the instrument classes defined here should only provide a thin
interface for the communication.  Any complicated logic to perform a sequence
of measurements should be left for higher level classes, e.g. `Controller`s.

The enumerations are used as a way to encode the *return strings* from a
particular device.  So their values should match how a string is returned from
the device in question.

The generic SCPISpectrumAnalyzer was based on Keysight N9010A.  Don't expect it
to work with all.  (Caveat) There is a "window number" parameter in most of the
functions, `wnum`.  This is actually not used in any of the methods in N9010A.
This should be removed later from this generic class.

It is important that the derived devices share the names of the attributes for
the same functionalities.

The naming of the attributes follow the convention from SCPI command naming.
This is done for two reasons:
    1. Easily figuring out which particular command does a method refer to.
    2. Not having to come up with creative names makes it easier to implement
       this.
"""
import typing as t

from .instrument import VISAInstrument, Instrument, SCPICommon,\
    CommunicationError
from .networkanalyzer import SCPIVectorNetworkAnalyzer
from .networkanalyzer import KeysightN5232A, AgilentN5232A, RohdeSchwarzZND
from .spectrumanalyzer import SCPISpectrumAnalyzer
from .spectrumanalyzer import RohdeSchwarzFSV
from .dcsupply import YokogawaGS200
from .signalgenerator import KeysightE8257D, SCPISignalGenerator, KeysightE8267D
from .sourcemeter import TSPCurrentSource, TSPSourceMeter
from .temperaturecontroller import LS372TemperatureController
from .misc import AttocubeANC350
from ._common import NotACommand


# SCPISpectrumAnalyzer was based on Keysight's XSeries SCPI commands.
KeysightXSeries = SCPISpectrumAnalyzer


# This is for easy importing of all available instruments at the module top
# level.
__all__ = [
    'SCPIVectorNetworkAnalyzer',
    'SCPISpectrumAnalyzer',
    'SCPISignalGenerator',
    'TSPCurrentSource',
    'LS372TemperatureController']
