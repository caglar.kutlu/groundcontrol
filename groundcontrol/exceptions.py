"""Module to hold error classes."""


class ExperimentError(Exception):
    pass
