"""Module for reading and analysing data produced by jpasnrscan script.

SNRSCAN netcdf dataset description
    Dimensions:
        wp: Working point index.  This is ordered the same with the
            'WPE-AP_PROGRESS*.csv'.
    Coordinates (Set/Manipulated):
        nu (nu):  Offset from the amplification center.
        fs (wp, nu):  Signal frequency during measurement.
        ib (wp): Bias current (coil current).
        fp (wp):  Pump frequency.
        pp (wp):  Pump power.
    Variables (Measured):
        fr0 (wp): Passive resonance frequency.
        bw0 (wp):  Passive linewidth.
        det (wp):  Detuning (det=(2/bw0)*(fp/2 - fr)
        r_on (wp, nu):  SNR measurement when JPA is ON.
        r_off (wp, nu):  SNR measurement when JPA is OFF.
        n_on (wp, nu):  Noise measurement when JPA is ON.
        n_off (wp, nu):  Noise measurement when JPA is OFF.
        gj (wp, nu):  JPA gain estimation using separate gain measurement.
        gj_err (wp):  Estimate of random error for g_j.  Note that this is a
            rough estimate from the gain spectrum.

TODO:
    - move read_settings to the settings class
    - make DataSet as a wrapper around specific classes
    - focus on netcdf conversion

"""
import typing as t
from pathlib import Path
from parse import parse
from datetime import datetime
from collections import defaultdict
from itertools import chain
from enum import Enum
import json

import attr
import numpy as np
from xrscipy.signal import savgol_filter
# this adds the .filt accessor to datasets and dataarrays
import xarray as xr
import pandas as pd
import scipy.constants as cnst
import netCDF4 as nc

from groundcontrol.jpa_tuner import TuningPoint, WorkingPoint
from groundcontrol.measurement import MeasurementModel, SParam1PModel, SASpectrumModel
from groundcontrol.measurement import read_measurement_csv, NotAMeasurementError
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.declarators import quantity, parameter, declarative
from groundcontrol.declarators import from_json, to_json
import groundcontrol.units as un
from groundcontrol.logging import logger
from groundcontrol.estimation import phasemodel, sgsmooth, smoothify
from groundcontrol.analysis.calibrate import remove_baseline_sppm, remove_baseline
from groundcontrol.analysis.nt import noise_psd
from groundcontrol.util import find_nearest_idx, db2lin_pow, dbm2watt
from groundcontrol.helper import Array


class SNRSpectrum(MeasurementModel):
    """Standard data model for Spectrum Analyzer measurements."""

    frequencies: Array[float] = quantity(un.hertz, "Frequency")
    snrs: Array[float] = quantity(un.ratio_ww, "SNR")
    noisepows: Array[float] = quantity(un.dBm, "NoisePower")
    rbw: float = parameter(un.hertz, "RBW")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )


class WorkingPoint(WorkingPoint, MeasurementModel):
    pass


class WorkingPointExt(WorkingPoint, MeasurementModel):
    """Extended working point to include detuning fr0 etc."""

    det: float = quantity(un.nounit, "Detuning", True)
    fr0: float = quantity(un.hertz, "Passive Resonance Frequency", True)
    bw0: float = quantity(un.hertz, "Passive Bandwidth", True)


@declarative
class ProgramSettings:
    path: Path
    isnotune: bool
    repeated: t.Optional[int]
    rollback: bool
    wp_fpath: t.Optional[Path]
    use_detuning: bool
    use_detuning_noest_bw0: bool
    noskip_consecutive_resonance: bool
    offset_current: float
    baseline_current: float
    nuarray: np.ndarray = None
    version: str = "0.1"
    spectra_comparison: bool = False
    fine_tune: bool = False

    @classmethod
    def nosetting(cls):
        return cls(
            None, None, None, None, None, False, False, True, None, None, None, "0.1"
        )

    @classmethod
    def from_path(cls, path: Path):
        if path.is_dir():
            return from_json(path / "settings.json", cls)
        else:
            return from_json(path, cls)


@declarative
class NTAnalyzer:
    """A simple NT estimator class using SNRSCAN measurements."""

    ds: t.Union[xr.Dataset, t.Dict[str, xr.Dataset]]
    t_eff: float
    tn_off: xr.Dataset

    dsprep: t.Optional[xr.Dataset] = None

    @property
    def dataset_type(self):
        try:
            dstype = self.ds.attrs["dataset_type"]
        except KeyError:
            # bw compat
            dstype = "jpasnrscan-snrcomp"
        except AttributeError:
            dstype = "jpasnrscan-speccomp"
        return dstype

    class EstimationMethod(str, Enum):
        SIMPLE = "simple"
        FILTERED = "filtered"

    def estimate_simple(self, inplace=False):
        dstype = self.dataset_type

        if dstype == "jpasnrscan-speccomp":
            return self._estimate_simple_speccomp()
        elif dstype == "jpasnrscan-snrcomp":
            return self._estimate_simple_snrcomp(inplace)
        else:
            raise TypeError("Unsupported dataset. ({self.ds.attrs['dataset_type'])")

    def _add_nu_dim(self, ds, tup):
        dnu = ds.fbin - tup.peak_frequency

        # sanity check
        # nu array must be the same for all wp,
        # if the amplification peak frequency is centered for all.
        assert (dnu.std(dim="wp").data < 1).all()

        ds.coords["dnu"] = dnu.mean(dim="wp")
        return (
            ds.stack(_tmp=["wp", "frequency"])
            .reset_index("_tmp")
            .set_index(_tmp=["wp", "dnu"])
            .unstack()
            .drop("frequency")
        )

    def _remove_half_pump(
        self, da: xr.DataArray, varnames: t.List[str], include_nearest: bool = False
    ):
        binwidth = da.dnu.diff(dim="dnu").mean().item()
        epsilon = 0.01 * binwidth

        if include_nearest:
            da[varnames].loc[
                {"dnu": da.dnu.sel(dnu=slice(-binwidth - epsilon, binwidth + epsilon))}
            ] = np.nan
        else:
            da[varnames].loc[{"dnu": da.dnu.sel(dnu=0, method="nearest")}] = np.nan

        return da

    def _addlin_ps(self, ds):
        ds["power_w"] = dbm2watt(ds.power)
        ds.power_w.attrs.update({"long_name": "Power", "units": "W"})
        return ds

    def _addlin_sp(self, ds):
        ds["spar_mag_l"] = db2lin_pow(ds.spar_mag)
        ds.spar_mag_l.attrs.update({"long_name": "Power", "units": "W"})
        return ds

    def prepare(self):
        if self.dataset_type == "jpasnrscan-snrcomp":
            logger.info("Nothing to prepare.")
            return None

        logger.info("Preparing dataset by aligning indexes etc.")

        tup = self.ds["TuningPoint"]

        def prep_ps(ds, tup):
            return self._remove_half_pump(
                self._addlin_ps(self._add_nu_dim(ds, tup)),
                varnames=["power_w", "power"],
                include_nearest=True,
            )

        def prep_sp(ds, tup):
            return self._remove_half_pump(
                self._addlin_sp(self._add_nu_dim(ds, tup)), varnames=["spar_mag_l"]
            )

        ps_on = prep_ps(self.ds["PowSpect-ON"], tup)
        ps_off = prep_ps(self.ds["PowSpect-OFF"], tup).interp_like(ps_on)
        sp_on = prep_sp(self.ds["SPar1Spect-ON"], tup).interp_like(ps_on)
        sp_off = prep_sp(self.ds["SPar1Spect-OFF"], tup).interp_like(ps_on)

        tn_off_d = {}
        for ntname, newname in zip(
            "tn tn_err gain gain_err".split(), "tn_bl tn_bl_err g_bl g_bl_err".split()
        ):
            var = getattr(self.tn_off, ntname)
            tn_off_d[newname] = var.interp(
                fs=ps_on.fbin, method="linear", kwargs={"fill_value": "extrapolate"}
            )
        tn_off_d["tn_bl"].attrs["ancillary_variables"] = "tn_bl_err"
        tn_off_d["g_bl"].attrs["ancillary_variables"] = "g_bl_err"
        tn_off = xr.Dataset(tn_off_d)

        pow_on = ps_on.power_w.rename("pow_on")
        pow_off = ps_off.power_w.rename("pow_off")
        sp_on = sp_on.spar_mag_l.rename("sp_on")
        sp_off = sp_off.spar_mag_l.rename("sp_off")
        tn_bl = tn_off.tn_bl.rename("tn_bl").drop("fs")
        tn_bl_err = tn_off.tn_bl_err.rename("tn_bl_err").drop("fs")
        g_bl = tn_off.g_bl.rename("g_bl").drop("fs")
        g_bl_err = tn_off.g_bl_err.rename("g_bl_err").drop("fs")
        ds1 = xr.merge(
            [pow_on, pow_off, sp_on, sp_off, tn_bl, tn_bl_err, g_bl, g_bl_err]
        )
        ds1.coords["fcenter"] = tup.peak_frequency
        filtvars = ["pow_on", "pow_off", "sp_on", "sp_off"]
        wsuffix = [f"{filtvar}_filt" for filtvar in filtvars]

        darrs = []
        for filtvar, wsuffix in zip(filtvars, wsuffix):
            darrs.append(
                ds1[filtvar]
                .interpolate_na(dim='dnu')  # important
                .filt.savgol(window_length=500e3, polyorder=3, dim="dnu")
                .rename(wsuffix)
            )
        ds2 = xr.merge(darrs)
        return xr.merge([ds1, ds2])

    def estimate_filtered(self):
        dstype = self.dataset_type

        if dstype == "jpasnrscan-snrcomp":
            raise TypeError("Unsupported dataset. ({self.ds.attrs['dataset_type'])")

        if self.dsprep is None:
            self.dsprep = self.prepare()

        namemapping = {
                'pow_on_filt': 'pow_on',
                'pow_off_filt': 'pow_off',
                'sp_on_filt': 'sp_on',
                'sp_off_filt': 'sp_off',
                'tn_bl': 'tn_bl',
                'tn_bl_err': 'tn_bl_err',
                }

        dsp = self.dsprep[list(namemapping.keys())].rename_vars(namemapping)
        zet = dsp.pow_on / dsp.pow_off
        rcomp = zet / (dsp.sp_on / dsp.sp_off)

        tnf = noise_psd(self.t_eff, dsp.fbin) / cnst.Boltzmann
        tnref = dsp.tn_bl
        tn = rcomp * (tnf + tnref) - tnf
        tn.name = "tn"
        tnds = tn.to_dataset()
        tnds.tn.attrs = dsp.tn_bl.attrs
        tnds.tn.attrs["ancillary_variables"] = "tn_err"
        tnds["tn_err"] = xr.ones_like(tnds.tn)
        tnds.tn_err.attrs = dsp.tn_bl_err.attrs

        # not estimated yet
        tnds.tn_err[:] = np.nan

        return tnds

    def _estimate_simple_speccomp(self):
        if self.dsprep is None:
            self.dsprep = self.prepare()
        dsp = self.dsprep
        zet = dsp.pow_on / dsp.pow_off
        rcomp = zet / (dsp.sp_on / dsp.sp_off)

        tnf = noise_psd(self.t_eff, dsp.fbin) / cnst.Boltzmann
        tnref = dsp.tn_bl
        tn = rcomp * (tnf + tnref) - tnf
        tn.name = "tn"
        tnds = tn.to_dataset()
        tnds.tn.attrs = dsp.tn_bl.attrs
        tnds.tn.attrs["ancillary_variables"] = "tn_err"
        tnds["tn_err"] = xr.ones_like(tnds.tn)
        tnds.tn_err.attrs = dsp.tn_bl_err.attrs

        # not estimated yet
        tnds.tn_err[:] = np.nan

        return tnds

    def _estimate_simple_snrcomp(self, inplace):
        if not inplace:
            ds = self.ds.copy()
        else:
            ds = self.ds

        tn_off_ds = self.tn_off.interp(fs=ds.fs)

        ratio = ds.r_on / ds.r_off
        # Noise temperature at the fridge temperature
        tnf = noise_psd(self.t_eff, ds.fs) / cnst.Boltzmann
        tn = (1 / ratio) * (tnf + tn_off_ds.tn) - tnf
        ds["tn"] = tn
        atrs = ds["tn"].attrs
        atrs["measurement_method"] = "SNR comparison"
        atrs["estimation_method"] = "simple"
        atrs["units"] = "K"
        atrs["long_name"] = "$ T_n $ "

        ds["g_joff"] = tn_off_ds.gain
        ds["tn_joff"] = tn_off_ds.tn
        ds["g_joff_err"] = tn_off_ds.gain_err
        ds["tn_joff_err"] = tn_off_ds.tn_err
        return ds

    def estimate(self, method: "EstimationMethod" = "simple", inplace=False, **kwargs):
        def unknown_method(*args, **kwargs):
            raise NotImplementedError(f"The method '{method}' is not known.")

        dispatcher = defaultdict(
            unknown_method, {
                self.EstimationMethod.SIMPLE: self.estimate_simple,
                self.EstimationMethod.FILTERED: self.estimate_filtered,
                }
        )

        return dispatcher[method](inplace=False, **kwargs)

    @staticmethod
    def read_nt_csv(csv: Path):
        tnoffds = (
            pd.read_csv(
                csv,
                names=["fs", "gain", "gain_err", "tn", "tn_err"],
                comment="#",
                skiprows=1,
            )
            .set_index("fs")
            .to_xarray()
        )
        return tnoffds

    @classmethod
    def read_nt(cls, fin: Path):
        try:
            return cls.read_nt_csv(fin)
        except UnicodeDecodeError:
            # assume its netcdf file.
            # fs is used as a name for frequency within this file only!
            ds = xr.open_dataset(fin).rename(frequencies="fs")[
                ["tn", "tn_err", "gain", "gain_err"]
            ]
            ds.tn[:] = ds.tn.weighted(1 / ds.tn_err ** 2).mean(
                dim="nrep", keep_attrs=True
            )[:]
            ds.tn_err[:] = np.sqrt((ds.tn_err ** 2).mean(dim="nrep", keep_attrs=True))[
                :
            ]
            ds.gain[:] = ds.gain.weighted(1 / ds.gain_err ** 2).mean(
                dim="nrep", keep_attrs=True
            )[:]
            ds.gain_err[:] = np.sqrt(
                (ds.gain_err ** 2).mean(dim="nrep", keep_attrs=True)
            )[:]
            return ds.weighted(1 / ds.tn_err ** 2).mean(dim="nrep")

    @classmethod
    def _from_xarray(cls, ds: xr.Dataset, t_eff, tn_off):
        # TODO: Change the name of this function.
        return cls(ds, t_eff, tn_off)

    @classmethod
    def make(cls, ds: t.Union[t.Dict, xr.Dataset], tn_off: xr.Dataset, t_eff: float):
        """Creates the NT analyzer object.

        The proc_ntmeas.py output may contain a separate dimension called "rep" which
        refers to the repeated NT measurements.  The tn and gain will be averaged using
        weights of their related _err variables along the rep dimension.

        Args:
            ds: The snrscan dataset.  Either an xarray dataset or dict of datasets with
                keys as HED compliant group names in a netCDF file (i.e. PowSpect-ON,
                PowSpect-OFF) etc.
            tn_off: The noise temperature measurement result when JPA is OFF, as
                xarray.Dataset.
            t_eff: Effective (physical) temperature as seen by the JPA.  If every
                relevant component in the path of the JPA is well thermalized to the
                mixing chamber and the source of the signal is a matched termination,
                this should be givin as the mixing chamber temperature.  If this is not
                the case, this is in general a complicated function of component
                temperatures and impedances.

        """
        return cls._from_xarray(ds, t_eff, tn_off)

    @classmethod
    def from_paths(cls, dspath: Path, ntpath: Path, t_eff: float):
        """Creates the NT analyzer object from the provided paths.

        The reference NT measurement (JPA OFF case) is provided via 'ntpath' argument.
        It can either be a csv file with the fields fs, gain, gain_err, tn, tn_err or a
        netCDF file with the relevant fields (as procuded by proc_ntmeas.py).

        Args:
            dspath: Path to the snrscan dataset netCDF file.
            ntpath: Path to the noise temperature measurement result when JPA
                is OFF.
            t_eff: Effective (physical) temperature as seen by the JPA.  If every
                relevant component in the path of the JPA is well thermalized to the
                mixing chamber and the source of the signal is a matched termination,
                this should be givin as the mixing chamber temperature.  If this is not
                the case, this is in general a complicated function of component
                temperatures and impedances.

        """
        tnoff = cls.read_nt(ntpath)
        ds = xr.open_dataset(dspath)
        if ds.attrs["dataset_type"] == "jpasnrscan-snrcomp":
            return cls.make(ds, tnoff, t_eff)
        elif ds.attrs["dataset_type"] == "jpasnrscan-speccomp":
            return cls.make(
                {
                    "PowSpect-ON": xr.open_dataset(dspath, group="PowSpect-ON"),
                    "PowSpect-OFF": xr.open_dataset(dspath, group="PowSpect-OFF"),
                    "SPar1Spect-ON": xr.open_dataset(dspath, group="SPar1Spect-ON"),
                    "SPar1Spect-OFF": xr.open_dataset(dspath, group="SPar1Spect-OFF"),
                    "TuningPoint": xr.open_dataset(dspath, group="TuningPoint"),
                    "WorkingPoint": xr.open_dataset(dspath, group="WorkingPoint"),
                },
                tnoff,
                t_eff,
            )
        else:
            raise ValueError(f"Unknown dataset_type. ({ds.attrs['dataset_type']})")


@declarative
class _SNRCompDataSet:
    path: Path
    snrs_off: t.Dict[int, SNRSpectrum]
    snrs_on: t.Dict[int, SNRSpectrum]
    gains: t.Dict[int, SParam1PModel]
    tups: t.Dict[int, TuningPoint]
    resonances: t.Dict[int, SParam1PModel]
    baseline: SParam1PModel
    settings: ProgramSettings
    wptarget: WorkingPoint
    wpprogress: t.Dict[int, WorkingPointExt]

    @property
    def script_version(self):
        return self.settings.version

    def report(self) -> str:
        vtext = f"Program Info::"
        dheader = "Data counts::"
        dbody = [
            f"ON SNR: {len(self.snrs_on):d}",
            f"OFF SNR: {len(self.snrs_off):d}",
            f"Gain spectra: {len(self.gains):d}",
            f"Tuning points: {len(self.tups):d}",
            f"Resonance spectra: {len(self.resonances):d}",
            "Working points (PROG/TARGET): "
            f"{len(self.wpprogress.ib):d}/{len(self.wptarget.ib):d}",
        ]
        dtext = "\n\t".join([dheader] + dbody)

        return "\n".join([vtext, to_json(self.settings), dtext])

    def show_report(self):
        print(self.report())

    def to_xarray(self, legacy=True):
        if self.settings.version == "0.1" and legacy:
            dsc = _DatasetConverter(self, None, None, None)
            return dsc._makesnrdataset_simple()
        else:
            dsc = _DatasetConverter.make(self)
            dsc: _DatasetConverter
            return dsc.make_snr_dataset()

    def to_netcdf(self, fname: t.Union[Path, str]):
        ds = self.to_xarray(legacy=False)
        ds.attrs["dataset_type"] = "jpasnrscan-snrcomp"
        ds.to_netcdf(fname, mode="w")

    @classmethod
    def from_path(
        cls,
        path: Path,
        midx: t.Optional[t.Union[t.Tuple, int]] = None,
        settings: t.Optional[ProgramSettings] = None,
    ):
        """Creates the object using the path.

        Args:
            path: Path to the measurement folder.

        Returns:
            obj: DataSet object.

        """
        _path = path
        if settings is None:
            settings = ProgramSettings.from_path(_path)

        # TODO:
        # implement reading various parts as staticmethods.
        mio = MIOFileCSV.open(path, "r")
        df = mio.read_measurement_index_df()
        sppmpaths = df.datapath[df.mclass == "SParam1PModel"].values
        snrpaths = df.datapath[df.mclass == "SNRSpectrum"].values
        tuppaths = df.datapath[df.mclass == "TuningPoint"].values

        # This is the targeted working points
        try:
            wptarget = read_measurement_csv(_path / "wp.csv")
        except NotAMeasurementError:
            # old script saves this as simple csv file not as measurementmodel
            _tmp = pd.read_csv(_path / "wp.csv", names=["ib", "fp", "pp"], comment="#")
            wptarget = WorkingPoint(_tmp.ib, _tmp.fp, _tmp.pp)
        try:
            wppath = df.datapath[df.mclass == "WorkingPointExt"].values[0]
            # These are the total working points done, if script executed well,
            # the length of this will be equal to wptarget.  The actual working
            # points may differ due to some program settings.
            wpes = dict(enumerate(read_measurement_csv(wppath, appendlist=True)))
        except IndexError:
            logger.debug("No WPE data. Assuming script version 0.1.")
            wpes = {}
            for i, (ib, fp, pp) in enumerate(
                zip(wptarget.ib, wptarget.fp, wptarget.pp)
            ):
                wpes[i] = WorkingPointExt(ib, fp, pp)

        gains = {}
        resonances = {}

        for path in sppmpaths:
            pdir, abbrev, mref, index = mio.parse_path(path)
            mrefparse = parse("{mtyp}-i{mind:d}", mref)
            try:
                mrefind = mrefparse["mind"]
                mtyp = mrefparse["mtyp"]
            except TypeError:
                # baseline data
                mtyp = mref
                mrefind = 0

            if isinstance(midx, int):
                if mrefind != midx:
                    continue
            elif isinstance(midx, t.Collection):
                if mrefind not in midx:
                    continue

            logger.info(f"Reading {path!s}.")
            sppm = read_measurement_csv(path)
            if mtyp == "BASELINE":
                baseline = sppm
            elif mtyp == "GAIN":
                gains[mrefind] = sppm
            elif mtyp == "RESONANCE":
                resonances[mrefind] = sppm

        snrs_off = {}
        snrs_on = {}
        for path in snrpaths:
            pdir, abbrev, mref, index = mio.parse_path(path)
            mrefparse = parse("{mtyp}-i{mind:d}", mref)
            mrefind = mrefparse["mind"]
            mtyp = mrefparse["mtyp"]

            if isinstance(midx, int):
                if mrefind != midx:
                    continue
            elif isinstance(midx, t.Collection):
                if mrefind not in midx:
                    continue

            logger.info(f"Reading {path!s}.")
            snr = read_measurement_csv(path)
            if mtyp == "OFF":
                snrs_off[mrefind] = snr
            if mtyp == "ON":
                snrs_on[mrefind] = snr

        tups = {}
        for path in tuppaths:
            pdir, abbrev, mref, index = mio.parse_path(path)
            mrefparse = parse("i{mind:d}", mref)
            mrefind = mrefparse["mind"]
            if isinstance(midx, int):
                if mrefind != midx:
                    continue
            elif isinstance(midx, t.Collection):
                if mrefind not in midx:
                    continue

            logger.info(f"Reading {path!s}.")
            tup = read_measurement_csv(path)
            tups[mrefind] = tup

        if settings.nuarray is None:
            # just use one snrspectrum data for estimating nuarray
            dsc = _DatasetConverter
            nus = dsc.extract_nuarray(snrs_on[0], tups[0])
            settings.nuarray = nus

        obj = cls(
            path,
            snrs_off,
            snrs_on,
            gains,
            tups,
            resonances,
            baseline,
            settings,
            wptarget=wptarget,
            wpprogress=wpes,
        )
        return obj


@declarative
class _SpecCompDataSet:
    path: Path
    indexdf: pd.DataFrame
    sasm_off: t.Dict[int, SASpectrumModel]
    sasm_on: t.Dict[int, SASpectrumModel]
    sppm_off: t.Dict[int, SParam1PModel]
    sppm_on: t.Dict[int, SParam1PModel]
    tups: t.Dict[int, TuningPoint]
    resonances: t.Dict[int, SParam1PModel]
    baseline: SParam1PModel
    settings: ProgramSettings
    wptarget: WorkingPoint
    wpprogress: t.Dict[int, WorkingPointExt]

    @property
    def script_version(self):
        return self.settings.version

    def report(self) -> str:
        vtext = "Program Info::"
        dheader = "Data counts::"
        dbody = [
            f"ON SASM: {len(self.sasm_on):d}",
            f"OFF SASM: {len(self.sasm_off):d}",
            f"ON SP1PM: {len(self.sp1pm_on):d}",
            f"OFF SP1PM: {len(self.sp1pm_off):d}",
            f"Tuning points: {len(self.tups):d}",
            f"Resonance spectra: {len(self.resonances):d}",
            "Working points (PROG/TARGET): "
            f"{len(self.wpprogress.ib):d}/{len(self.wptarget.ib):d}",
        ]
        dtext = "\n\t".join([dheader] + dbody)

        return "\n".join([vtext, to_json(self.settings), dtext])

    def show_report(self):
        print(self.report())

    def _wp2xr(self):
        wpds = xr.concat(
            [
                wp.to_xarray().squeeze().reset_coords("ib")
                for wp in self.wpprogress.values()
            ],
            dim="wp",
            combine_attrs="no_conflicts",
        )
        return wpds

    def _tup2xr_single(self, tup: TuningPoint):
        flds = attr.fields_dict(TuningPoint)
        ds = xr.Dataset(
            {
                "gain": ("frequency", tup.gain),
                "offset_gain": tup.offset_gain,
                "resonance": tup.resonance,
                "peak_frequency": tup.peak_frequency,
                "bias_current": tup.bias_current,
                "pump_power": tup.pump_power,
                "pump_frequency": tup.pump_frequency,
                "offset_frequency": tup.offset_frequency,
                "signal_power": tup.signal_power,
            },
            coords={
                "fbin": ("frequency", tup.frequency),
                "timestamp": tup.timestamp,
            },
        )
        ds.gain.attrs["long_name"] = "Gain"
        ds.gain.attrs["units"] = repr(flds["gain"].metadata["unit"])

        ds.fbin.attrs["units"] = repr(flds["frequency"].metadata["unit"])
        ds.fbin.attrs["cell_methods"] = "frequency: point"
        ds.fbin.attrs["long_name"] = "Frequency"

        for fld in [
            "offset_gain",
            "resonance",
            "peak_frequency",
            "bias_current",
            "pump_power",
            "pump_frequency",
            "offset_frequency",
            "signal_power",
        ]:
            dsfld = getattr(ds, fld)
            dsfld.attrs["units"] = repr(flds[fld].metadata["unit"])
            dsfld.attrs["long_name"] = flds[fld].metadata["repr"]
            dsfld.attrs["comment"] = flds[fld].metadata["description"]

        return ds

    def _tup2xr(self):
        dss = []
        for tup in self.tups.values():
            ds = self._tup2xr_single(tup)
            dss.append(ds)

        return xr.concat(dss, dim="wp", data_vars="all", combine_attrs="no_conflicts")

    def to_xarray_dict(self):
        ps_on = xr.concat(
            [sasm.to_hed_xarray() for sasm in self.sasm_on.values()],
            dim="wp",
            data_vars="different",
            compat="identical",
            combine_attrs="no_conflicts",
        )
        ps_on.attrs["mref"] = "ON"

        ps_off = xr.concat(
            [sasm.to_hed_xarray() for sasm in self.sasm_off.values()],
            dim="wp",
            data_vars="different",
            compat="identical",
            combine_attrs="no_conflicts",
        )
        ps_off.attrs["mref"] = "OFF"

        sp_on = xr.concat(
            [sp.to_hed_xarray() for sp in self.sppm_on.values()],
            dim="wp",
            data_vars="different",
            compat="identical",
            combine_attrs="no_conflicts",
        )
        sp_on.attrs["mref"] = "ON"

        sp_off = xr.concat(
            [sp.to_hed_xarray() for sp in self.sppm_off.values()],
            dim="wp",
            data_vars="different",
            compat="identical",
            combine_attrs="no_conflicts",
        )
        sp_off.attrs["mref"] = "OFF"

        sp_bl = self.baseline.to_hed_xarray()
        sp_bl.attrs["mref"] = "BASELINE"
        sp_res = xr.concat(
            [sp.to_hed_xarray() for sp in self.resonances.values()],
            dim="wp",
            data_vars="different",
            compat="identical",
            combine_attrs="no_conflicts",
        )
        sp_res.attrs["mref"] = "RESONANCE"
        tups = self._tup2xr()
        wpds = self._wp2xr()

        xrd = {
            "PowSpect-ON": ps_on,
            "PowSpect-OFF": ps_off,
            "SPar1Spect-ON": sp_on,
            "SPar1Spect-OFF": sp_off,
            "SPar1Spect-BASELINE": sp_bl,
            "SPar1Spect-RESONANCE": sp_res,
            "TuningPoint": tups,
            "WorkingPoint": wpds,
        }
        return xrd

    def to_netcdf(self, fname: t.Union[Path, str]):
        dct = self.to_xarray_dict()
        for k, v in dct.items():
            try:
                v.to_netcdf(fname, group=k, mode="a")
            except FileNotFoundError:
                v.to_netcdf(fname, group=k, mode="w")

        ds_ = nc.Dataset(fname, mode="a")
        ds_.dataset_type = "jpasnrscan-speccomp"
        ds_.close()

    @classmethod
    def from_path(cls, path: Path, settings: t.Optional[ProgramSettings] = None):
        """Creates the object using the path.

        Args:
            path: Path to the measurement folder.

        Returns:
            obj: DataSet object.

        """
        if settings is None:
            settings = ProgramSettings.from_path(path)

        # TODO:
        # implement reading various parts as staticmethods.
        mio = MIOFileCSV.open(path, "r")
        df = mio.read_measurement_index_df()
        sppmpaths = df.datapath[df.mclass == "SParam1PModel"].values
        sasmpaths = df.datapath[df.mclass == "SASpectrumModel"].values
        tuppaths = df.datapath[df.mclass == "TuningPoint"].values

        # This is the targeted working points
        wptarget = read_measurement_csv(path / "wp.csv")

        wppath = df.datapath[df.mclass == "WorkingPointExt"].values[0]
        # These are the total working points done, if script executed well,
        # the length of this will be equal to wptarget.  The actual working
        # points may differ due to some program settings.
        wpes = dict(enumerate(read_measurement_csv(wppath, appendlist=True)))

        sasm_ons = {}
        sasm_offs = {}
        sppm_ons = {}
        sppm_offs = {}
        resonances = {}
        tups = {}
        baseline = None

        for measpath in chain(sppmpaths, sasmpaths, tuppaths):
            pdir, abbrev, mref, index = mio.parse_path(measpath)
            mrefparse = parse("{mtyp}-i{mind:d}", mref)

            if mrefparse is None:
                mrefparse = parse("i{mind:d}", mref)

                if mrefparse is None:
                    # BASELINE doesn't have mind
                    mtyp = mref
                    mrefind = 0
                else:
                    mtyp = ""
                    mrefind = mrefparse["mind"]
            else:
                mtyp = mrefparse["mtyp"]
                mrefind = mrefparse["mind"]

            logger.info(f"Reading {measpath!s}.")
            meas = read_measurement_csv(measpath)

            if (abbrev, mtyp) == ("SPPM", "ON"):
                sppm_ons[mrefind] = meas
            elif (abbrev, mtyp) == ("SPPM", "OFF"):
                sppm_offs[mrefind] = meas
            elif (abbrev, mtyp) == ("SPPM", "RESONANCE"):
                resonances[mrefind] = meas
            elif (abbrev, mtyp) == ("SASM", "ON"):
                sasm_ons[mrefind] = meas
            elif (abbrev, mtyp) == ("SASM", "OFF"):
                sasm_offs[mrefind] = meas
            elif (abbrev, mtyp) == ("SPPM", "BASELINE"):
                baseline = meas
            elif abbrev == "TP":
                tups[mrefind] = meas

        obj = cls(
            path,
            df,
            sasm_offs,
            sasm_ons,
            sppm_offs,
            sppm_ons,
            tups,
            resonances,
            baseline,
            settings,
            wptarget=wptarget,
            wpprogress=wpes,
        )
        return obj


def read_dataset(path: t.Union[str, Path]):
    path = Path(path)
    progset = ProgramSettings.from_path(path)
    if progset.spectra_comparison:
        return _SpecCompDataSet.from_path(path, settings=progset)
    else:
        return _SNRCompDataSet.from_path(path, settings=progset)


@declarative
class _DatasetConverter:
    """Very hackish converter used for the first version of the measurement
    script"""

    ds: _SNRCompDataSet
    gains_cal: t.Dict[int, SParam1PModel]
    resonances_cal: t.Dict[int, SParam1PModel]
    wpds: xr.Dataset

    def make_gain_dataset(self, usefirst: bool = True):
        # Get the subset of wp's that has gain measurements.
        if usefirst:
            join = "override"
        else:
            join = None
        wpsub = self.wpds.isel(wp=list(self.gains_cal.keys()))

        gains = (
            self.sppm2xr(gcal, self.ds.wpprogress[wpind])
            for wpind, gcal in self.gains_cal.items()
        )

        ds = xr.concat(
            (gain.swap_dims({"fs": "nu"}) for gain in gains), dim=wpsub.wp, join=join
        )
        return ds

    def make_snr_dataset(self):
        ds = self.ds
        nuarr = ds.settings.nuarray
        wpsub = self.wpds.isel(wp=list(self.gains_cal.keys()))

        snrs = (
            self.snr2xr(ron, roff).swap_dims({"fs": "nu"}).assign_coords(nu=nuarr)
            for ron, roff in zip(ds.snrs_on.values(), ds.snrs_off.values())
        )

        snrsds = xr.concat(snrs, dim=wpsub.wp)

        gjs = (
            self.gain2xr_forsnrds(gcal, ds.wpprogress[wpind], nuarr)
            for wpind, gcal in self.gains_cal.items()
        )
        gjds = xr.concat(gjs, dim=wpsub.wp)

        return xr.merge((snrsds, gjds, wpsub), compat="override")

    @staticmethod
    def wp2xr(wp):
        xwp = wp.to_xarray().swap_dims({"ib": "wp"}).set_coords(["fp", "pp"])
        return xwp

    @staticmethod
    def snr2xr(snr_on, snr_off):
        snron = snr_on.to_xarray().rename(
            frequencies="fs", snrs="r_on", noisepows="n_on", timestamp="ts_on"
        )
        snron.r_on.attrs["description"] = "SNR measured when JPA is ON."
        snron.r_on.attrs["long_name"] = r"$ R $"
        snron.n_on.attrs["long_name"] = r"$ N $"
        snron.n_on.attrs["description"] = "Noise measured when JPA is ON."
        snron.ts_on.attrs["description"] = "ON measurements finish time."
        snroff = snr_off.to_xarray().rename(
            frequencies="fs", snrs="r_off", noisepows="n_off", timestamp="ts_off"
        )
        snroff.r_off.attrs["description"] = "SNR measured when JPA is OFF."
        snroff.r_off.attrs["long_name"] = r"$ R' $"
        snroff.n_off.attrs["long_name"] = r"$ N' $"
        snroff.n_off.attrs["description"] = "Noise measured when JPA is OFF."
        snroff.ts_off.attrs["description"] = "OFF measurements finish time."
        return xr.merge([snroff, snron])

    @staticmethod
    def gain2xr_forsnrds(gain: SParam1PModel, wp: WorkingPointExt, nuarr: np.ndarray):
        """Converts gain spectrum measuremnt to the final xarray format,
        estimating the gains at nus given by nu_array."""
        _dsc = _DatasetConverter
        gxr = _dsc.remove_degenerate(gain.to_xarray().mags, wp.fp)
        if gxr.attrs["units"] != un.dB.shortname:
            raise TypeError(f'Gain unit must be in {un.dB!s}({gxr.attrs["units"]})')
        lin = db2lin_pow(gxr)
        try:
            slinarr = sgsmooth(gxr.frequencies, lin, xwindow=wp.bw0 / 10)
        except TypeError:
            # old script doesn't provide bw0.  It can be computer from
            # resonances though.
            bw0 = (gxr.frequencies.max() - gxr.frequencies.min()) / 20
            slinarr = sgsmooth(gxr.frequencies, lin, xwindow=bw0)
        slin = lin.copy(data=slinarr)
        # CREATE DATAARRAY LIKE SLIN
        #  assumption:  higher gain >> less uncertainty
        #    This is actually a biased estimator.
        normstd = ((lin - slin) * slin).std()

        fsarr = nuarr + wp.fp / 2
        gj = slin.interp(frequencies=fsarr).rename(
            {"frequencies": "fs", "timestamp": "ts_gain"}
        )
        gj.attrs["units"] = un.ratio_ww.shortname
        gj.attrs["long_name"] = "G_J"
        gj.attrs["description"] = "JPA gain."

        gj_err = gj.copy(data=normstd / gj)
        gj_err.attrs["long_name"] = "std(G_J)"
        gj_err.attrs["description"] = "Random error estimate for JPA gain."

        ds = (
            xr.Dataset({"gj": gj, "gj_err": gj_err})
            .swap_dims({"fs": "nu"})
            .assign_coords(nu=nuarr)
        )
        return ds

    @staticmethod
    def sppm2xr(
        sppm: SParam1PModel,
        wp: t.Optional[WorkingPointExt] = None,
        removedeg: bool = False,
        smooth: t.Union[bool, float] = False,
    ) -> xr.Dataset:
        """Converts the gain spectrum into a xarray dataset.  If wp is not
        `None` nuarray is calculated using the pump frequency and added to the
        output as a coordinate.  If wp is given and `removedeg` is `True`,
        degenerate point is removed and replaced with interpolation of two
        values in adjacent frequency bins.  If `smooth` evaluates to `True`,
        smoothing operation is performed over all variables, using a
        savitzky-golay filter of order 3 with a window band equal to.:
            - smooth, if smooth is a float
            - wp.bw0/10 if wp is provided.
            - span*0.05 else.  Smoothing is performed without any
              transformation on the dataset.

        """
        _dsc = _DatasetConverter

        gxr = sppm.to_xarray().rename({"frequencies": "fs"})

        if (wp is not None) and removedeg:
            gxr = _dsc.remove_degenerate(gxr, wp.fp, fname="fs")

        if smooth:
            if isinstance(smooth, float):
                xwindow = smooth
            elif wp is None:
                logger.warning(
                    "No wp nor smoothing band provided. Using 5% of span for smoothing."
                )
                xwindow = gxr.fs.max() - gxr.fs.min()
            else:
                xwindow = wp.bw0 / 10

            mags = sgsmooth(gxr.fs, gxr.mags, xwindow=xwindow)
            uphases = sgsmooth(gxr.fs, gxr.uphases, xwindow=xwindow)
            phases = sgsmooth(gxr.fs, gxr.phases, xwindow=xwindow)

            out = gxr.copy(data={"mags": mags, "uphases": uphases, "phases": phases})
        else:
            out = gxr

        if wp is not None:
            nu = out.fs - wp.fp / 2
            out.coords["nu"] = ("fs", nu)
        return out

    @staticmethod
    def remove_degenerate(gj, fp, fname="frequencies", interpolate=True):
        fpeak = fp / 2
        fdeg = getattr(gj.sel({fname: fpeak}, method="nearest"), fname).values

        dropped = gj.drop_sel({fname: fdeg})

        if interpolate:
            gjn = dropped.reindex_like(gj).interpolate_na(dim=fname)
        else:
            gjn = dropped

        gjn.attrs = gj.attrs
        return gjn

    @staticmethod
    def nuarray(f: np.ndarray, tup: TuningPoint):
        return f - tup.peak_frequency

    @staticmethod
    def fp2det(fp, fr, bw):
        det = (fp / 2 - fr) * 2 / bw
        return det

    @staticmethod
    def estimate_resparams(res: SParam1PModel):
        pars = phasemodel.guess(res.uphases, res.frequencies)
        fit = phasemodel.fit(res.uphases, f=res.frequencies, params=pars)
        bp = fit.params
        fr, bw = bp["fr"].value, bp["bw"].value
        return fr, bw

    @staticmethod
    def extract_nuarray(s: SNRSpectrum, tup: TuningPoint, deciround: int = 0):
        """All the nuarray """
        nuarray = _DatasetConverter.nuarray(s.frequencies, tup)
        if deciround == 0:
            return np.round(nuarray)
        else:
            return (10 ** (-deciround)) * np.round((10 ** deciround) * nuarray)

    @staticmethod
    def snrs2ds(snrs: SNRSpectrum):
        f = snrs.frequencies
        snr = snrs.snrs
        noise = snrs.noisepows

        return xr.Dataset(
            {"snr": ("frequency", snr), "noise": ("frequency", noise)},
            coords={"frequency": f},
        )

    @classmethod
    def make(cls, ds: _SNRCompDataSet, smoothbaseline=True):

        if smoothbaseline:
            bl = smoothify(ds.baseline, 5e6, yname=["mags", "uphases"], porder=3)
        else:
            bl = ds.baseline

        # calibrating
        logger.info("Calibrating gain measurements using baseline.")
        gainscal = {k: remove_baseline_sppm(gm, bl) for k, gm in ds.gains.items()}

        logger.info("Calibrating resonance measurements using baseline.")
        rescal = {k: remove_baseline_sppm(rm, bl) for k, rm in ds.resonances.items()}

        wpds = xr.concat((cls.wp2xr(wp) for wp in ds.wpprogress.values()), dim="wp")
        wpds.coords["wp"] = wpds.wp

        return cls(ds, gains_cal=gainscal, resonances_cal=rescal, wpds=wpds)

    def _makesnrdataset_simple(self):
        """This is for quick analysis, gonna be deprecated quickly"""
        ds = self.ds
        sons = []
        sonstds = []
        sonnoises = []
        sonnoisestds = []
        soffs = []
        soffstds = []
        soffnoises = []
        soffnoisestds = []
        gains = []
        deltas = []
        pps = []
        fpumps = []
        frs = []
        bw0s = []
        fcenters = []
        for (son, soff, gsp, tup, res) in zip(
            ds.snrs_on.values(),
            ds.snrs_off.values(),
            ds.gains.values(),
            ds.tups.values(),
            ds.resonances.values(),
        ):
            gsp: SParam1PModel
            sonval, sonstd = np.mean(son.snrs), np.std(son.snrs)
            sonnsval, sonnsstd = np.mean(son.noisepows), np.std(son.noisepows)
            soffval, soffstd = np.mean(soff.snrs), np.std(soff.snrs)
            soffnsval, soffnsstd = (np.mean(soff.noisepows), np.std(soff.noisepows))

            # both have the same freq. spectrum
            fcenter = np.mean(son.frequencies)

            # this can be done much better.
            gcal = remove_baseline(
                gsp.frequencies,
                gsp.mags,
                ds.baseline.frequencies,
                ds.baseline.mags,
                scale="log",
            )
            fidx = find_nearest_idx(gsp.frequencies, fcenter)
            gain = gcal[fidx]

            tup: TuningPoint
            fr = tup.resonance
            fp = tup.pump_frequency

            _, bw0 = self.estimate_resparams(res)
            delta = (fp / 2 - fr) / (bw0 / 2)
            pp = tup.pump_power

            sons.append(sonval)
            sonstds.append(sonstd)
            sonnoises.append(sonnsval)
            sonnoisestds.append(sonnsstd)
            soffnoises.append(soffnsval)
            soffnoisestds.append(soffnsstd)
            gains.append(gain)
            soffs.append(soffval)
            soffstds.append(soffstd)
            deltas.append(delta)
            pps.append(pp)
            fpumps.append(fp)
            frs.append(fr)
            bw0s.append(bw0)
            fcenters.append(fcenter)

        nuarr = self.extract_nuarray(son, tup)

        # first make dataframe then convert to xarray
        df = pd.DataFrame(
            {
                "snr_on": sons,
                "snr_on_std": sonstds,
                "snr_off": soffs,
                "snr_off_std": soffstds,
                "snr_on_noise": sonnoises,
                "snr_on_noise_std": sonnoisestds,
                "snr_off_noise": soffnoises,
                "snr_off_noise_std": soffnoisestds,
                "gain": gains,
                "delta": deltas,
                "ppump": pps,
                "fpump": fpumps,
                "fcenter": fcenters,
                "fr": frs,
                "bw0": bw0s,
            }
        )

        mindex = pd.MultiIndex.from_frame(df[["delta", "ppump"]])

        # Since we are using these for index, drop from columns
        baredf = df.drop(["delta", "ppump"], 1)

        # Using the new index
        newdf = baredf.set_index(mindex)

        # Now convert to xarray dataset
        ds = newdf.to_xarray()
        ds.attrs["nuarray"] = nuarr
        ds.attrs["nuarray_description"] = (
            "SNR measurement frequency points relative to the fp/2, i.e.nu=f-fp/2"
        )
        ds.attrs["description"] = (
            "The SNR measurements are done at several nu values."
            "The dataset contains averages and standard deviations of "
            "these measurements.  For example `snr_on` corresponds to "
            "the mean, `snr_on_std` corresponds to the standard "
            "deviation of these."
        )
        return ds
