"""Analysis library for simple NT measurement data."""

from pathlib import Path
import typing as t

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from groundcontrol.measurement import read_measurement_csv
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import declarative

from .nt import NTDataSet
from .types import PSDEstimate


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['figure.figsize'] = (10, 6)
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

SWEEPPARAMS = Path('sweep_params.csv')
MEASINDEX = Path('measuremens.index')


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path

    def _read_helper(
            self,
            indices: t.Tuple[int, int],
            globdir: Path,
            mrefpre: str = ""):
        i, j = indices[0], indices[1]
        mrefijk = f"i{i:d}j{j:d}"
        if mrefpre != "":
            mref = f"{mrefpre}-{mrefijk}"
        else:
            mref = mrefijk
        files = globdir.glob(f"*_{mref}_*.csv")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None

        return read_measurement_csv(file)

    @property
    def nt_dataset_count(self):
        return 1

    def read_nt_dataset(
            self,
            index: int
            ) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation.
        """
        df = self.sweepdf
        j = index
        grouped = self.sweepdf.groupby('go')
        for go, df in grouped:
            # each group must correspond to single j by def.
            ijks = df.ijk.values
            _, jj = ijks[0]
            if jj != j:
                continue

            psds = []
            tups = []
            for inds in ijks:
                psds.append(PSDEstimate.from_sasm(self.read_sasm(inds)))
                tups.append(self.read_tp(inds))
            temps = df.tp.values

            dataset = NTDataSet.make(temps, psds, tups)
            return dataset

        # if we reach the end, it means we didn't find the indices.
        return None

    def iterate_nt_datasets(self) -> t.Iterator[NTDataSet]:
        grouped = self.sweepdf.groupby('go')
        for go, df in grouped:
            # each group must correspond to single j by def.
            ijks = df.ijk.values

            psds = []
            tups = []
            for inds in ijks:
                psds.append(PSDEstimate.from_sasm(self.read_sasm(inds)))
                tups.append(self.read_tp(inds))
            temps = df.tp.values

            dataset = NTDataSet.make(temps, psds, tups)
            yield dataset

    def read_baseline(
            self, ref: str = "BASELINE"):
        files = self._sppmdir.glob("*BASELINE*")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None
        return read_measurement_csv(file)

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, indices) -> t.Optional[TuningPoint]:
        return self._read_helper(indices, self._tpdir)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    def read_resonance_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir, "RESONANCE")

    @classmethod
    def from_path(
            cls,
            rootdir: Path
            ):
        """The sweep parameters csv contains single array of
        temperature values.
        
        IMPORTANT:
            - The csv for sweepparams is malformed, so this function is just a
              workaround, don't read much into unnecessary parts for ijk
              column creation.
        """
        fname = rootdir / SWEEPPARAMS

        colnames = ("tp", "go")

        df = pd.read_csv(
            fname, skipinitialspace=True, delimiter=',',
            names=colnames, comment='#')

        gridarrs = {}
        uniqvals = {}

        for name in colnames:
            series = df[name]
            gridarrs[name] = series
            uniqvals[name] = np.unique(series)

        elemns = list(map(len, uniqvals.values()))
        ijk = np.unravel_index(df.index, elemns)
        df['ijk'] = tuple(zip(*ijk))
        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR)


