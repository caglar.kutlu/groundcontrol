"""This module collects the models and datatypes I commonly use for NT
estimation."""
import typing as t

import numpy as np
from scipy import constants as cnst
import lmfit
import matplotlib as mpl
import matplotlib.pyplot as plt
import attr

from groundcontrol.logging import logger
from groundcontrol.util import db2lin_pow, find_nearest_idx, lin2db_pow
from groundcontrol.declarators import declarative, quantity, parameter
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.measurement import SParam1PModel
from groundcontrol.measurement import MeasurementModel
from groundcontrol.analysis.calibrate import remove_baseline
import groundcontrol.units as un
from .types import PSDEstimate
from .plotting import plot_nt_fit, plot_nt_data, plot_gain_vs_temp
from groundcontrol.helper import Array


def johnson_noise_psd(temperature: float):
    k_B = cnst.Boltzmann
    T = temperature
    return k_B*T


def coth(x):
    return 1/np.tanh(x)


def noise_psd(temperature: float, frequency: float):
    """Power spectral density of the noise waves emitted by a perfectly
    matched resistor, including the quantum noise.
    """
    h = cnst.Planck
    k_B = cnst.Boltzmann
    T = temperature
    f = frequency
    arg = h*f/(2*k_B*T)
    return 0.5*h*f*coth(arg)


def calibrate_tp(
        tp: TuningPoint,
        baseline: SParam1PModel):
    """Deembeds the baseline from gain measurement in tuning point.
    Uses linear interpolation to approximate points.
    """
    ftp = tp.frequency
    fb = baseline.frequencies
    bmags = baseline.mags
    bl_intrp = np.interp(ftp, fb, bmags)
    # with baseline deembedded
    gaincal = tp.gain - bl_intrp
    return tp.evolve(gain=gaincal)


def make_simple_model(f, gain_exp, k_db, t_loss):
    """Creates a simple noise model fixing the frequency and temperature of
    loss parameters.

    Args:
        f:  Frequency at which the noise model will be created.
        k_db: Loss factor between noise source and input of amplifier.
            Positive definite.
        t_loss:  Temperature of the lossy elements after the noise source.
        gain_exp:  Expected gain of the whole chain as a linear W/W ratio.
            Providing this makes the convergence faster.
    """
    assert k_db >= 0

    gain_max = gain_exp*10
    gain_min = gain_exp/10

    k = db2lin_pow(-k_db)

    @lmfit.Model
    def simple_model(t, tn, g):
        """Simple model to estimate noise temperature of an amplifying element
        connected to a perfectly matched noisy resistor acting as variable
        source.

        THIS IS WRONG FOR JPA!

        Args
            t: Temperature of source.
            tn: Noise temperature
            g: Overall gain.
        """
        p_n = johnson_noise_psd(tn)
        psrc = noise_psd(t, f)
        return g*(k*psrc + (1-k)*noise_psd(t_loss, f) + p_n)

    simple_model.set_param_hint(
            'tn', value=0.2, min=0.06, max=3)
    simple_model.set_param_hint(
            'g', value=gain_exp, min=gain_min, max=gain_max)
    return simple_model


def make_noreflection_model(
        f: float, kc: float,
        g_jpa: float, g_r: float, tf: float, tn_rest: float,
        gain_unit='dB'):
    """This is a model for the noise power spectral density measured for our
    usual noise temperature measurement setup.  This model ignores reflections
    from components, and the effect of the loss between the source and the
    circulator (for now).

    Args:
        f:  Center frequency at which the PSD is measured, i.e. bin frequency
            from the measured spectrum.
        kc:  Power gain factor of the circulator.  Definition:
            1. for log scale: P2 = P1 + kc.  Must be negative.
            2. for linear scale:  P2 = kc * P2.  Must be between 0 and 1.
        g_jpa:  Maximum JPA gain within the spectrum.
        g_r:  Power gain of the rest of the measurement chain.
        tf:  Physical temperature of the fridge.  Everything else other than
            the noise source are assumed to be thermalized to this temperature.
        tn_rest:  Noise temperature of the rest of the chain.
        gain_unit:  Unit of the supplied gain parameters kc, g_{jpa,r}.
            'dB' or 'lin'.

    Estimated Parameters:
        g:  Total gain including circulator, jpa and the rest.
        tn:  Noise temperature for a system connected to a 50 Ohm termination.
        tn_int:  (auxiliary) Noise temperature intrinsic to the unknown loss
            mechanisms in the JPA.

    """
    if gain_unit == 'dB':
        # making sure the sign of kc
        kc = db2lin_pow(-np.abs(kc))
        g_jpa = db2lin_pow(g_jpa)
        g_r = db2lin_pow(g_r)

    g_expected = g_r * g_jpa * kc**2
    g_max = g_expected*1000
    g_min = g_expected/1000

    kB = cnst.Boltzmann
    # Noise from fridge temperature
    pfridge = noise_psd(tf, f)
    tfridge = pfridge/kB

    @lmfit.Model
    def noreflection_model(
            t, tn, g, kc=kc, g_jpa=g_jpa, tn_rest=tn_rest, tn_f=tfridge):
        """JPA Noise model ignoring reflections."""
        H = (2 - 1/g_jpa) * g
        Teff = (tn - tfridge*(2 - 1/kc - 1/g_jpa))/(2 - 1/g_jpa)

        p_n = noise_psd(t, f)

        pout = H*(p_n + kB*Teff)
        return pout

    for fixedparam in 'kc g_jpa tn_rest tn_f'.split():
        noreflection_model.set_param_hint(fixedparam,  vary=False)

    noreflection_model.set_param_hint(
            'tn', value=0.2, min=0.01, max=10)
    noreflection_model.set_param_hint(
            'g', value=g_expected, min=g_min, max=g_max)

    # auxiliary parameter describing intrinsic losses
    noreflection_model.set_param_hint(
            'tn_int', expr='kc*tn - (1-1/g_jpa)*tn_f - tn_rest/(g_jpa*kc)')

    return noreflection_model


@declarative
class NTModelParams:
    f: float = parameter(un.hertz, 'f', nodefault=True)
    t_f: float = parameter(un.kelvin, 'T_f', nodefault=True)
    tn_r: float = parameter(un.kelvin, 'Tn_R', nodefault=True)
    g_t: float = parameter(un.dB, 'G_T', nodefault=True)
    g_l: float = parameter(un.dB, 'G_l', nodefault=True)
    g_c1: float = parameter(un.dB, 'G_c1', nodefault=True)
    g_c2: float = parameter(un.dB, 'G_c2', nodefault=True)
    g_j: float = parameter(un.dB, 'G_J', nodefault=True)
    g_i: float = parameter(un.dB, 'G_I', nodefault=True)

    b_c1: float = parameter(un.nounit, 'b_c1', nodefault=True)
    b_c2: float = parameter(un.nounit, 'b_c2', nodefault=True)
    b_l: float = parameter(un.nounit, 'b_l', nodefault=True)

    def _betacal(self, gval: float):
        return 1/gval - 1

    @b_c1.default
    def _bc1(self):
        return self._betacal(self.g_c1)

    @b_c2.default
    def _bc2(self):
        return self._betacal(self.g_c2)

    @b_l.default
    def _bl(self):
        return self._betacal(self.g_l)

    @g_i.default
    def _idlergain(self):
        return self.g_j - 1


def make_ambitious_model(
        fixed_params: NTModelParams,
        tn_expected: t.Union[float, t.Tuple[float, float, float]],
        g_r_expected: t.Union[float, t.Tuple[float, float, float]]
        ):
    _fp = fixed_params
    f = fixed_params.f
    kB = cnst.Boltzmann
    # Noise from fridge temperature
    p_n_f = noise_psd(fixed_params.t_f, f)

    tn_q = cnst.Planck*f/(2*kB)

    if isinstance(tn_expected, tuple):
        tn_mean, tn_min, tn_max = tn_expected
    else:
        tn_mean, tn_min, tn_max = tn_expected, tn_q, 1e3*tn_q

    if isinstance(g_r_expected, tuple):
        g_r_mean, g_r_min, g_r_max = g_r_expected
    else:
        g_r_min = g_r_expected/1e3
        g_r_max = g_r_expected*1e3
        g_r_mean = g_r_expected

    @lmfit.Model
    def nt_ambitious(
            t, tn, g_r,
            f=f, t_f=_fp.t_f, g_t=_fp.g_t, g_l=_fp.g_l,
            g_c1=_fp.g_c1, g_c2=_fp.g_c2, g_j=_fp.g_j, g_i=_fp.g_i,
            tn_r=_fp.tn_r, b_c1=_fp.b_c1, b_c2=_fp.b_c2,
            p_n_f=p_n_f, kB=kB):
        """
        Note: tn_r is not used in the added noise estimate but estimation of
        other auxiliary parameters.
        """
        p_n_s = noise_psd(t, f)
        b_l = fixed_params.b_l
        p_c = g_l*(p_n_s + b_l*p_n_f)

        g_tot = g_c1*(g_c2**2)*g_r*g_j
        g_alpha = g_c1*(g_c2**2)*g_r*(g_i+g_j)

        # The added noise, tn is to be estimated
        p_a = kB*tn

        _bstr = b_c1 + b_c2/g_c1

        _rat = g_tot/g_alpha
        _k_pnf = _bstr*(1-_rat) - _rat*(g_i/g_j)*(1+_bstr)
        p_beta = _k_pnf*p_n_f + _rat*p_a

        p_h = g_alpha*(p_c + p_beta)
        return p_h

    nt_ambitious.set_param_hint(
            'tn', value=tn_mean, min=tn_min, max=tn_max)
    nt_ambitious.set_param_hint(
            'g_r', value=g_r_mean, min=g_r_min, max=g_r_max)
    nt_ambitious.set_param_hint(
            'g', expr='g_c1*(g_c2**2)*g_r*g_j')
    nt_ambitious.set_param_hint(
            'g_tot', expr='g_c1*(g_c2**2)*g_r*g_j')
    nt_ambitious.set_param_hint(
            'p_e', expr=('g_c1*g_c2*('
                            'kB*tn -'
                            '('
                                'b_c1*(1+g_i/g_j)+'
                                'b_c2*(1/g_c1+g_i/g_j/g_c1+1/g_j/g_c1/g_c2)+'
                                'g_i/g_j'
                            ')*p_n_f-kB*tn_r/g_c1/(g_c2**2)/g_j'
                            ')'))  # noqa: E127
    for fixedparam in ('f t_f g_t g_l g_c1 g_c2 g_j g_i '
                       'tn_r b_c1 b_c2 p_n_f kB').split():
        nt_ambitious.set_param_hint(fixedparam,  vary=False)
    return nt_ambitious


def make_generic_model(
        fixed_params: NTModelParams,
        tn_expected: t.Union[float, t.Tuple[float, float, float]],
        g_r_expected: t.Union[float, t.Tuple[float, float, float]]):
    _fp = fixed_params
    kB = cnst.Boltzmann
    # Noise from fridge temperature
    p_n_f = noise_psd(_fp.t_f, _fp.f)
    t_n_f = p_n_f/kB

    def _ga(gr):
        return gr*_fp.g_c1*_fp.g_c2**2*(_fp.g_j+_fp.g_i)

    def _tb(tn):
        return (tn - p_n_f/kB)

    # using tn_beta to avoid working with small power values
    if isinstance(tn_expected, tuple):
        tn_bet, tn_bet_min, tn_bet_max = tuple(map(_tb, tn_expected))
    else:
        tn_bet, tn_bet_min, tn_bet_max = _tb(tn_expected), -t_n_f, 100*t_n_f

    if isinstance(g_r_expected, tuple):
        g_al, g_al_min, g_al_max = tuple(map(_ga, tn_expected))
    else:
        g_al = _ga(g_r_expected)
        g_al_min, g_al_max = g_al/1e3, g_al*1e3

    @lmfit.Model
    def nt_generic(
            t, g_alpha, tn_beta,
            f=_fp.f, t_f=_fp.t_f, g_t=_fp.g_t, g_l=_fp.g_l,
            g_c1=_fp.g_c1, g_c2=_fp.g_c2, g_j=_fp.g_j, g_i=_fp.g_i,
            tn_r=_fp.tn_r, b_c1=_fp.b_c1, b_c2=_fp.b_c2,
            p_n_f=p_n_f, kB=kB):
        p_n_s = noise_psd(t, f)
        b_l = 1/g_l - 1
        p_c = g_l*(p_n_s + b_l*p_n_f)
        p_beta = kB*tn_beta
        return g_alpha*(p_c + p_beta)

    nt_generic.set_param_hint(
            'g_alpha', value=g_al, min=g_al_min, max=g_al_max)
    nt_generic.set_param_hint(
            'tn_beta', value=tn_bet, min=tn_bet_min, max=tn_bet_max)
    nt_generic.set_param_hint(
            'p_beta', expr='kB*tn_beta')
    nt_generic.set_param_hint(
            'g', expr='g_alpha*g_j/(g_j+g_i)')
    nt_generic.set_param_hint(
            'g_r', expr='g/g_j/g_c1/g_c2**2')
    nt_generic.set_param_hint(
            'b_str', expr='b_c1+b_c2/g_c1')
    nt_generic.set_param_hint(
            'p_a', expr='((g_i+g_j)/g_j)*('
                            'p_beta-(b_str+b_c2*g_c2)*p_n_f)+'
                        '(b_str+(g_i/g_j)*(1+b_str)+b_c2*g_c2/g)*p_n_f'
            )  # noqa: E127
    nt_generic.set_param_hint(
            'p_e', expr=('g_c1*g_c2*('
                            'p_a -'
                            '('
                                'b_c1*(1+g_i/g_j)+'
                                'b_c2*(1/g_c1+g_i/g_j/g_c1+1/g_j/g_c1/g_c2)+'
                                'g_i/g_j'
                            ')*p_n_f-kB*tn_r/g_c1/(g_c2**2)/g_j'
                            ')'))  # noqa: E127
    nt_generic.set_param_hint(
            'tn', expr='p_a/kB')
    for fixedparam in ('f t_f g_t g_l g_c1 g_c2 g_j g_i '
                       'tn_r b_c1 b_c2 p_n_f kB').split():
        nt_generic.set_param_hint(fixedparam,  vary=False)
    return nt_generic


@declarative
class NTDataSet:
    """Dataset for noise temperature estimation."""
    temps: np.ndarray
    psds: t.List[PSDEstimate]
    tups: t.Optional[t.List[TuningPoint]]  # uncalibrated gain measurement
    spectra: np.ndarray  # rows -> frequencies, cols -> temps
    frequencies: np.ndarray
    fpeak: float
    gain_spectra: t.Optional[np.ndarray]  # rows -> frequencies, cols -> temps
    gain_frequencies: t.Optional[np.ndarray]
    _iscal: bool = False

    @classmethod
    def make(
            cls,
            temps: np.ndarray,
            psds: t.List[PSDEstimate],
            tups: t.Optional[t.List[TuningPoint]] = None,
            fpeak: t.Optional[float] = None,
            cutwindow: t.Optional[float] = None):
        """Creates a dataset object.

        Args:
            temps:  Noise source temperatures.
            psds:  Collection of PSDEstimate objects.
            tups:  Collection of tuning points.
            fpeak:  Frequency of the peak in the spectrum.  If `None` estimated
                using the first tuning point.  Defaults to `None`.
            cutwindow:  If provided, PSD spectra were cut around the center
                frequency with the given value.

        """
        spectra = np.vstack([psd.s for psd in psds]).T
        f = psds[0].f

        if (tups is None) and (fpeak is None):
            raise ValueError("Provide fpeak if not providing tuning points.")

        if fpeak is None:
            tup0 = tups[0]
            fpeak = tup0.peak_frequency

        if tups is None:
            gainspectra = None
            gfreq = None
        else:
            gains = []
            gfreq = None
            for tup in tups:
                gains.append(tup.gain)
                if gfreq is None:
                    gfreq = tup.frequency
            gainspectra = np.vstack(gains).T

        obj = cls(
                temps, psds, tups, spectra, f, fpeak,
                gainspectra, gfreq)
        if cutwindow is not None:
            obj.frequency_band_cut(cutwindow)
        return obj

    def report_frequency_range(self):
        f = self.frequencies
        start = f[0]/1e9
        stop = f[-1]/1e9
        step = (f[1] - f[0])/1e3
        return (f"Frequencies (start, stop, step): "
                f"{start:.5f} GHz, {stop:.5f} GHz, {step:.3f} kHz")

    def frequency_band_cut(self, band: float):
        """For all PSDs cuts a region of `band` around the peak frequency.
        This method mutates the object!
        """
        for i, psd in enumerate(self.psds):
            f = psd.f
            s = psd.s
            fcenter = self.fpeak
            fdown = fcenter - band/2
            fup = fcenter + band/2
            mask = (f >= fdown) * (f <= fup)
            fnew = f[mask]
            snew = s[mask]
            psd = PSDEstimate(fnew, snew)
            self.psds[i] = psd
        self.frequencies = fnew
        self.spectra = self.spectra[mask, :]

    def apply_baseline(self, baseline: SParam1PModel):
        """Applies baseline to gain measurements to give an estimate on the JPA
        gain only.
        """
        if self._iscal:
            logger.warning("Baseline was already applied, skipping.")

        if self.tups is None:
            # No gain measurement to apply baseline
            raise ValueError("No gain measurement to apply baseline to.")

        for i, tup in enumerate(self.tups):
            tupnew = calibrate_tp(tup, baseline)
            self.tups[i] = tupnew

        gains = []
        gfreq = None
        for tup in self.tups:
            gains.append(tup.gain)
            if gfreq is None:
                gfreq = tup.frequency
        gainspectra = np.vstack(gains).T
        self.gain_spectra = gainspectra
        self.gain_frequencies = gfreq
        self._iscal = True

    def fit_simple(
            self,
            gain,
            k_db=0,
            t_loss=0.04,
            usegainmeasurement: bool = False,
            temprange: t.Tuple[float, float] = (0, 2)
            ) -> "NTFitResult":
        f = self.fpeak
        model = make_simple_model(f, gain, k_db, t_loss)

        spectra = self.spectra
        # different rows -> different frequencies

        temps = self.temps
        tempsmask = (temps >= temprange[0])*(temps <= temprange[1])
        temps = self.temps[tempsmask]

        results = []
        for fpk, column in zip(self.frequencies, spectra):
            if usegainmeasurement:
                _, gainatfpk = self.gain_at(fpk)
                glin = db2lin_pow(gainatfpk)
                # multiply by average to restore the scale
                # divide to get rid of variations
                column = np.mean(glin)*column/glin

            # limiting the fit range
            column = column[tempsmask]

            result = model.fit(column, t=temps)
            results.append(result)

        frequencies = self.psds[0].f
        return NTFitResult(
                model,
                results, frequencies, temps,
                self.fpeak)

    def psd_at(self, f: float):
        """Returns an array of PSD values sliced at the frequency information
        nearest to f.
        """
        freqs = self.frequencies
        idx = find_nearest_idx(freqs, f)
        s = self.spectra[idx]
        return freqs[idx], s

    def gain_at(self, f: float):
        if self.tups is None:
            raise ValueError("No gain measurement.")
        freqs = self.gain_frequencies
        idx = find_nearest_idx(freqs, f)
        s = self.gain_spectra[idx]
        return freqs[idx], s

    def plot_at(
            self, f: float, ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None, **kwargs):
        """Plot data for single frequency."""
        f, s = self.psd_at(f)
        ax = plot_nt_data(self.temps, s, ax, label=label, **kwargs)
        ax.grid(True)
        return ax

    def plot_gain_at(
            self, f: float, ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None, dbscale: bool = True):
        f, s = self.gain_at(f)
        ax = plot_gain_vs_temp(
                self.temps, s, ax, label=label,
                cal=self._iscal, dbscale=dbscale)
        ax.grid(True)
        return ax


@declarative
class NTDataSetAlt:
    """Dataset for noise temperature estimation."""
    temps: np.ndarray
    psds: t.List[PSDEstimate]
    tup: TuningPoint  # uncalibrated gain measurement
    spectra: np.ndarray  # rows -> frequencies, cols -> temps
    frequencies: np.ndarray
    gains: t.List[SParam1PModel]
    baseline: SParam1PModel
    resonances: t.List[SParam1PModel]

    g_r_expected: float = db2lin_pow(70)

    _gains_jpa: t.List[SParam1PModel] = attr.Factory(list)

    @property
    def fpeak(self):
        return self.tup.peak_frequency

    @classmethod
    def make(
            cls, temps, psds, tup, gains, baseline,
            resonances: t.Optional[t.List[SParam1PModel]] = None):
        spectra = np.vstack([psd.s for psd in psds]).T
        f = psds[0].f

        mags = [
                remove_baseline(gain.frequencies, gain.mags,
                                baseline.frequencies, baseline.mags)
                for gain in gains]
        phases = [
                remove_baseline(gain.frequencies, gain.uphases,
                                baseline.frequencies, baseline.mags)
                for gain in gains]

        gains_jpa = [
                gain.evolve(mags=mag, phases=phase)
                for mag, phase, gain in zip(mags, phases, gains)]
        if resonances is None:
            resonances = []
        return cls(
                temps, psds, tup, spectra, f, gains, baseline, resonances,
                gains_jpa=gains_jpa)

    @classmethod
    def make_ntsimple(
            cls, temps, psds, tups, baseline,
            resonances: t.Optional[t.List[SParam1PModel]] = None):
        nans = np.empty_like(tups[0].frequency)
        nans[:] = np.nan

        gaintmpl = SParam1PModel(
                frequencies=nans, mags=nans, phases=nans, uphases=nans)

        gains = [gaintmpl.evolve(frequencies=tup.frequency, mags=tup.gain,
                                 timestamp=tup.timestamp)
                 for tup in tups]

        return cls.make(temps, psds, tups[0], gains, baseline, resonances)

    def report_frequency_range(self):
        f = self.frequencies
        start = f[0]/1e9
        stop = f[-1]/1e9
        step = (f[1] - f[0])/1e3
        return (f"Frequencies (start, stop, step): "
                f"{start:.5f} GHz, {stop:.5f} GHz, {step:.3f} kHz")

    def frequency_band_cut(self, band: float):
        """For all PSDs cuts a region of `band` around the peak frequency.
        This method mutates the object!
        """
        for i, psd in enumerate(self.psds):
            f = psd.f
            s = psd.s
            fcenter = self.fpeak
            fdown = fcenter - band/2
            fup = fcenter + band/2
            mask = (f >= fdown) * (f <= fup)
            fnew = f[mask]
            snew = s[mask]
            psd = PSDEstimate(fnew, snew)
            self.psds[i] = psd
        self.frequencies = fnew
        self.spectra = self.spectra[mask, :]

    def apply_baseline(self, baseline: SParam1PModel):
        tupnew = calibrate_tp(self.tup, baseline)
        self.tup = tupnew

    def fit_simple(self, gain, k_db=0, t_loss=0.04) -> "NTFitResult":
        f = self.tup.peak_frequency
        model = make_simple_model(f, gain, k_db, t_loss)

        spectra = self.spectra
        # different rows -> different frequencies

        results = []
        for column in spectra:
            result = model.fit(
                    column, t=self.temps)
            results.append(result)

        frequencies = self.psds[0].f
        return NTFitResult(
                model,
                results, frequencies, self.temps,
                self.tup.peak_frequency)

    def interpolate_gain(
            self,
            gain: SParam1PModel,
            interp_dp: bool = True):
        g_j = gain.mags
        f_s = self.frequencies
        f_g = gain.frequencies
        g_intrp = np.interp(f_s, f_g, g_j)
        if interp_dp:
            idx = find_nearest_idx(f_g, self.f_peak)
            idx_s = find_nearest_idx(f_s, self.f_peak)
            g_intrp[idx_s] = np.interp(
                    f_s[idx_s],
                    [f_g[idx-1], self.f_g[idx+1]],
                    [g_j[idx-1], g_j[idx+1]])

        return g_intrp

    def fit_noreflection(
            self,
            kc_db: float, g_r_db: float, tf: float, tn_rest: float):
        """Fits using the noreflection model from `groundcontrol.analysis.nt`.

        The center frequency is used for all points since noise power is 
        expected to be relatively flat within 100 kHz scale.

        Args:
            kc:  Power loss factor of the circulator in dB scale. Positive
                defined.
            g_r:  Power gain of the rest of the measurement chain in dB scale.
            tf:  Physical temperature of the fridge.  Everything else other
                than the noise source are assumed to be thermalized to this
                temperature.
            tn_rest:  Noise temperature of the rest of the chain.
        """
        fpeak = self.tup.peak_frequency
        g_jpa0 = self.tup.offset_gain
        model = make_noreflection_model(
                fpeak, kc_db, g_jpa0, g_r_db,
                tf, tn_rest, gain_unit='dB')
        pars = model.make_params()

        spectra = self.spectra
        # different rows -> different frequencies

        # Using JPA gain from the first measurement
        g_jpa_f = self._gains_jpa[0].frequencies

        frequencies = self.psds[0].f

        # Interpolating gain measurements to desired frequencies.
        g_jpa_vals = np.interp(frequencies, g_jpa_f, self._gains_jpa[0].mags)
        results = []
        for g_jpa, column in zip(g_jpa_vals, spectra):
            pars['g_jpa'].value = db2lin_pow(g_jpa)
            result = model.fit(
                    column, t=self.temps, params=pars)
            results.append(result)

        return NTFitResult(
                model,
                results, frequencies, self.temps,
                self.tup.peak_frequency)

    def fit_generic(
            self,
            t_f: float,
            g_r_expected: float,
            g_t: float = 1,
            g_l: float = 1,
            g_c1: float = 1,
            g_c2: float = 1,
            tn_r: float = 2,
            temperature_mask: t.Optional[np.ndarray] = None):

        if temperature_mask is not None:
            temps = self.temps[temperature_mask]
        else:
            temps = self.temps

        spectra = self.spectra
        # different rows -> different frequencies

        # Gain measurement
        f_gain = self._gains_jpa[0].frequencies
        m_gain = db2lin_pow(self._gains_jpa[0].mags)

        # Spectrum measurement frequencies
        f_s = self.psds[0].f

        # Interpolating gain measurements to desired frequencies.
        g_js = np.interp(f_s, f_gain, m_gain)

        mfpars = NTModelParams(
                f_s[0], t_f, tn_r, g_t, g_l, g_c1, g_c2,
                g_js[0])

        results = []
        for f, g_j, psd_col in zip(f_s, g_js, spectra):
            mfpars.f = f
            mfpars.g_j = g_j
            mfpars.g_i = g_j - 1

            tn_min = 0
            tn_exp = noise_psd(t_f, f)/cnst.Boltzmann
            tn_max = 5*tn_r
            model = make_generic_model(
                    mfpars,
                    (tn_exp, tn_min, tn_max),
                    g_r_expected)

            pars = model.make_params()
            if temperature_mask is not None:
                psd_col = psd_col[temperature_mask]
            result = model.fit(psd_col, t=temps, params=pars)
            results.append(result)

        return NTFitResult(
                model,
                results, f_s, self.temps, self.fpeak)

    def gain_at(self, f: float, cal: bool = False):
        if cal:
            gains = self._gains_jpa
        else:
            gains = self.gains
        freqs = gains[0].frequencies
        idx = find_nearest_idx(freqs, f)
        s = np.array([gain.mags[idx] for gain in gains])
        return freqs[idx], s

    def plot_gain_at(
            self, f: float, ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None, cal: bool = False, dbscale: bool = True):
        f, s = self.gain_at(f, cal)
        ax = plot_gain_vs_temp(
                self.temps, s, ax, label=label,
                cal=cal, dbscale=dbscale)
        ax.grid(True)
        return ax

    def plot_at(
            self, f: float,
            ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None,
            factorgain: bool = False):
        """Plot data for single frequency.
        Args:
            ax:  matplotlib.Axes object.
            label:  Label for the plot.
            factorgain:  Whether or not to divide by gain measurement.
        """
        freqs = self.frequencies
        idx = find_nearest_idx(freqs, f)
        s = self.spectra[idx]
        if factorgain:
            gains = []
            for gain in self.gains:
                fs = gain.frequencies
                fidx = find_nearest_idx(fs, f)
                gains.append(gain.mags[fidx])
            gains = np.array(gains)
            s = s/(gains)
        ax = plot_nt_data(self.temps, s, ax, label=label)
        ax.grid(True)
        return ax


@declarative
class NTFitResult:
    model: lmfit.Model
    results: t.List[lmfit.model.ModelResult]
    frequencies: np.ndarray
    temperatures: np.ndarray
    fpeak: float

    def result_at(self, f: float):
        idx = find_nearest_idx(self.frequencies, f)
        return self.frequencies[idx], self.results[idx]

    def plot_at(
            self, f: float,
            ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None,
            inputreferred: bool = True,
            withparams: t.Union[bool, t.Dict] = True,
            withfitlabel: bool = True):
        """Plot result data for single frequency."""
        freal, result = self.result_at(f)
        ax = plot_nt_fit(
                self.temperatures,
                result, ax, label,
                inputreferred=inputreferred,
                withparams=withparams, withfitlabel=withfitlabel)
        return ax

    def estimations(self):
        """Returns a tuple of arrays for t_n, std(t_n), c, std(c)"""
        pars = (result.params for result in self.results)
        tn, tnerr, c, cerr = map(np.array, zip(*((
            p['tn'].value, p['tn'].stderr,
            p['g'].value, p['g'].stderr) for p in pars)))

        return tn, tnerr, c, cerr

    def plot_gain(
            self,
            ax: t.Optional[mpl.axes.Axes] = None,
            errorbar: bool = True,
            label: str = None,
            dbscale: bool = True):
        _, _, cs, cerrs = self.estimations()
        if dbscale:
            unit = "dB"
            cs = lin2db_pow(cs)
            # This is not correct stdev in dB in strict sense.
            cerrs = lin2db_pow(cs + cerrs)
            cerrs = cerrs - cs
        else:
            unit = "W/W"

        if ax is None:
            ax = plt.gca()
        f = self.fpeak - self.frequencies
        if errorbar:
            ax.errorbar(x=f/1e3, y=cs, yerr=cerrs, marker='o', label=label)
        else:
            ax.plot(f/1e3, cs, 'o', label=label)

        ax.set_xlabel("$f_p/2 - f_s$ [kHz]")
        ax.set_ylabel("$\mathrm{G}_\mathrm{tot}$" f" [{unit}]")
        ax.grid(True)
        return ax

    def plot(
            self,
            ax: t.Optional[mpl.axes.Axes] = None,
            errorbar: bool = True,
            label: str = None,
            useoffset: bool = True,
            window: t.Optional[float] = None,
            fmt: str = 'o',
            **kwargs):

        t_n_pars = [result.params['tn'] for result in self.results]
        tns = np.array([par.value for par in t_n_pars])

        fs = self.frequencies
        if window is not None:
            f_min = self.fpeak - window/2
            f_max = self.fpeak + window/2
            finds = np.where((fs <= f_max)*(fs >= f_min))[0]
        else:
            # oxymoron way
            finds = np.where(fs > -1)[0]

        if ax is None:
            ax = plt.gca()

        if useoffset:
            # if offset, use kHz by default
            f = (self.fpeak - fs)/1e3
        else:
            # else, use GHz
            f = self.frequencies/1e9

        f = f[finds]
        tns = tns[finds]
        if errorbar:
            tnerrs = np.array([par.stderr for par in t_n_pars])
            tnerrs = tnerrs[finds]
            ax.errorbar(
                    x=f, y=tns, yerr=tnerrs, fmt=fmt, label=label,
                    **kwargs)
        else:
            ax.plot(f, tns, fmt, label=label, **kwargs)

        if useoffset:
            ax.set_xlabel("$f_p/2 - f_s$ [kHz]")
        else:
            ax.set_xlabel("$f$ [GHz]")
        ax.set_ylabel("$\mathrm{T}_\mathrm{n}$ [K]")
        ax.grid(True)
        return ax

    def get_parameter_estimates(self, name: str) -> t.Tuple[float, float]:
        pars = [result.params[name] for result in self.results]
        parvals = np.array([par.value for par in pars])
        parerrs = np.array([par.stderr for par in pars])
        return parvals, parerrs

    def plot_param(
            self,
            name: str,
            ylabel: str,
            ax: t.Optional[mpl.axes.Axes] = None,
            errorbar: bool = True,
            label: str = None,
            useoffset: bool = True,
            window: t.Optional[float] = None,
            fmt: str = 'o',
            fun: t.Callable = None):
        """If errorbar, fun must accepts vals and errs and return vals and
        errs."""
        pars = [result.params[name] for result in self.results]
        parvals = np.array([par.value for par in pars])

        fs = self.frequencies
        if window is not None:
            f_min = self.fpeak - window/2
            f_max = self.fpeak + window/2
            finds = np.where((fs <= f_max)*(fs >= f_min))[0]
        else:
            # oxymoron way
            finds = np.where(fs > -1)[0]

        if ax is None:
            ax = plt.gca()

        if useoffset:
            # if offset, use kHz by default
            f = (self.fpeak - fs)/1e3
        else:
            # else, use GHz
            f = self.frequencies/1e9

        f = f[finds]
        parvals = parvals[finds]
        if errorbar:
            parerrs = np.array([par.stderr for par in pars])
            parerrs = parerrs[finds]
            if fun is not None:
                parvals, parerrs = fun(parvals, parerrs)
            ax.errorbar(x=f, y=parvals, yerr=parerrs, fmt=fmt, label=label)
        else:
            if fun is not None:
                parvals = fun(parvals)
            ax.plot(f, parvals, fmt, label=label)

        if useoffset:
            ax.set_xlabel("$f_p/2 - f_s$ [kHz]")
        else:
            ax.set_xlabel("$f$ [GHz]")
        ax.set_ylabel(ylabel)
        ax.grid(True)
        return ax

    def as_measurement_data(self) -> 'NTMeasurementData':
        tn, tnerr, g, gerr = self.estimations()
        f = self.frequencies
        return NTMeasurementData(
                f=f, tn=tn, tn_err=tnerr,
                g=g, g_err=gerr)


class NTMeasurementData(MeasurementModel):
    f: Array[float] = quantity(
            un.hertz, "f",
            description="Frequency domain data.")
    tn: Array[float] = quantity(
            un.kelvin, "T_n",
            description="Noise temperature estimations.")
    tn_err: Array[float] = quantity(
            un.kelvin, "std(T_n)",
            description="Noise temperature 90% CL fit error.")
    g: Array[float] = quantity(
            un.ratio_ww, "G_tot",
            description="Estimated gain of the whole chain.")
    g_err: Array[float] = quantity(
            un.ratio_ww, "std(G_tot)",
            description="Gain est. 90% CL fit error.")
