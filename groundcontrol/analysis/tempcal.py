from pathlib import Path

import numpy as np

from groundcontrol.measurement import (
        MeasurementModel, quantity, parameter,
        read_measurement_csv)
import groundcontrol.units as un
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.declarators import declarative


class SensorResistanceRecord(MeasurementModel):
    timestamp: np.datetime64 = quantity(un.nounit, "Timestamp")
    channel: str = quantity(un.nounit, 'MeasurementChannel')
    resistance: float = quantity(un.ohm, "Resistance")
    resistance_min: float = quantity(un.ohm, "ResistanceMin")
    resistance_max: float = quantity(un.ohm, "ResistanceMax")
    reactance: float = quantity(un.ohm, "Reactance")
    exc_pow: float = quantity(un.watt, "ExcitationPower")
    exc_volt: float = quantity(un.volt, "ExcitationVoltage")
    exc_curr: float = quantity(un.ampere, "ExcitationCurrent")
    read_stat: str = quantity(un.nounit, "ReadingStatus")
    exc_frequency: float = quantity(un.hertz, "ExcitationFrequency")
    dwell: float = parameter(un.second, "DwellPeriod")
    sampling_rate: float = parameter(un.hertz, 'SamplingRate')


@declarative
class DataSet:
    path: Path
    srr: SensorResistanceRecord

    def to_xarray(self):
        ds = self.srr.to_xarray()
        # required for saving netcdf
        ds.timestamp.attrs.pop('units')
        return ds

    @classmethod
    def from_path(cls, path: Path):
        mio = MIOFileCSV.open(path, 'r')
        df = mio.read_measurement_index_df()
        dfuniq = df.groupby('datapath').first().reset_index()

        sel = dfuniq.mclass == 'SensorResistanceRecord'
        srrpath = dfuniq.datapath[sel].values
        # assume its appending, pick first
        srr = read_measurement_csv(srrpath[0])
        return cls(path, srr)


def read_as_xarray(path: Path):
    ds = DataSet.from_path(path)
    return ds.to_xarray()
