"""Analysis library for simple NT measurement data."""

from pathlib import Path
import typing as t

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from groundcontrol.measurement import read_measurement_csv
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import declarative
from groundcontrol.util import find_nearest_idx

from .nt import NTDataSet
from .types import PSDEstimate
from .plotting import plot_xy


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['figure.figsize'] = (10, 6)
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

SWEEPPARAMS = Path('sweep_params.csv')
MEASINDEX = Path('measuremens.index')


def calibrate_sppm(
        sppm: SParam1PModel,
        baseline: SParam1PModel):
    """Deembeds the baseline from gain measurement in sppm.
    Uses linear interpolation to approximate points.  Does not do anything to
    phases!
    """
    ftp = sppm.frequencies
    fb = baseline.frequencies
    bmags = baseline.mags
    bl_intrp = np.interp(ftp, fb, bmags)
    # with baseline deembedded
    magscal = sppm.mags - bl_intrp
    return sppm.evolve(mags=magscal)


@declarative
class SaturationDataset:
    smeas: t.List[SParam1PModel]
    spows: np.ndarray
    tup: TuningPoint
    fpeak: float
    go_set: float
    _frequencies: np.ndarray = None
    _iloss_db: float = 0
    _iscal: bool = False

    _spows_prv: np.ndarray = None

    @property
    def iloss_db(self):
        return self._iloss_db

    @iloss_db.setter
    def iloss_db(self, db: float):
        """Positive defined loss"""
        if self._spows_prv is None:
            self._spows_prv = self.spows
        self._iloss_db = db
        self.spows = self._spows_prv - db

    def data_at(self, f: float):
        idx = find_nearest_idx(self._frequencies, f)
        gains = []
        for s in self.smeas:
            gains.append(s.mags[idx])
        return self._frequencies[idx], np.array(gains)

    def plot_at_offset(self, foff: float, ax=None, label=None, marker=None):
        _f, gains = self.data_at(self.fpeak + foff)
        if label is None:
            label = f"f=$f_p$/2-{foff/1e3:.1f}kHz"
        ax = plot_xy(self.spows, gains, label=label, marker=marker)
        ax.set_xlabel("Signal Power [dBm]")
        ax.set_ylabel("Gain [dB]")
        ax.grid(True)
        return ax

    def apply_baseline(self, baseline: SParam1PModel):
        if self._iscal:
            return self
        for i, sppm in enumerate(self.smeas):
            snew = calibrate_sppm(sppm, baseline)
            self.smeas[i] = snew
        self._iscal = True
        return self

    @classmethod
    def make(cls, smeas, spows, tup, go_set):
        fpeak = tup.peak_frequency
        fs = smeas[0].frequencies
        return cls(smeas, spows, tup, fpeak, go_set, fs)


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path

    def read_dataset(
            self,
            gindex: int
            ) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation.
        """
        df = self.sweepdf
        j = gindex
        grouped = self.sweepdf.groupby('go')
        for go, df in grouped:
            # each group must correspond to single j by def.
            ijks = df.ijk.values
            jj, _ = ijks[0]
            if jj != j:
                continue

            sppms = []
            tup = self.read_tp(jj)
            for inds in ijks:
                sppms.append(self.read_sppm(inds))

            dataset = SaturationDataset.make(sppms, df.spow.values, tup, go)
            return dataset

        # if we reach the end, it means we didn't find the indices.
        return None

    def iterate_nt_datasets(self) -> t.Iterator[NTDataSet]:
        grouped = self.sweepdf.groupby('go')
        for go, df in grouped:
            # each group must correspond to single j by def.
            ijks = df.ijk.values

            sppms = []
            tup = self.read_tp(ijks[0][0])
            for inds in ijks:
                sppms.append(self.read_sppm(inds))

            dataset = SaturationDataset.make(sppms, df.spow.values, tup, go)
            yield dataset

    def _read_helper(
            self,
            indices: t.Tuple[int, int],
            globdir: Path,
            mrefpre: str = ""):
        i, j = indices[0], indices[1]
        mrefijk = f"i{i:d}j{j:d}"
        if mrefpre != "":
            mref = f"{mrefpre}-{mrefijk}"
        else:
            mref = mrefijk
        files = globdir.glob(f"*_{mref}_*.csv")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None

        return read_measurement_csv(file)

    def read_baseline(
            self, ref: str = "BASELINE"):
        files = self._sppmdir.glob("*BASELINE*")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None
        return read_measurement_csv(file)

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, gindex) -> t.Optional[TuningPoint]:
        globdir = self._tpdir
        i = gindex
        mrefijk = f"i{i:d}"
        mref = mrefijk
        files = globdir.glob(f"*_{mref}_*.csv")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None

        return read_measurement_csv(file)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    def read_resonance_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir, "RESONANCE")

    @classmethod
    def from_path(
            cls,
            rootdir: Path
            ):
        """The sweep parameters csv contains single array of
        temperature values.
        
        IMPORTANT:
            - The csv for sweepparams is malformed, so this function is just a
              workaround, don't read much into unnecessary parts for ijk
              column creation.
        """
        fname = rootdir / SWEEPPARAMS

        colnames = ("go", "spow")

        df = pd.read_csv(
            fname, skipinitialspace=True, delimiter=',',
            names=colnames, comment='#')

        gridarrs = {}
        uniqvals = {}

        for name in colnames:
            series = df[name]
            gridarrs[name] = series
            uniqvals[name] = np.unique(series)

        elemns = list(map(len, uniqvals.values()))
        ijk = np.unravel_index(df.index, elemns)
        df['ijk'] = tuple(zip(*ijk))
        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR)
