#!/usr/bin/env python
# coding: utf-8

# Analysis library for NT vs Resonance Frequency and Pump Offsets
# This one is for analysis of JPA NT data data taken at 2019.10.22

from pathlib import Path
import typing as t

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy import constants as cnst
import lmfit

from groundcontrol.measurement import read_measurement_csv, MeasurementModel
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import quantity, quantities, parameters
from groundcontrol.friendly import mprefix_str
import groundcontrol.units as un
from groundcontrol.declarators import declarative
from groundcontrol.util import dbm2watt, watt2dbm, db2lin_pow, lin2db_pow
from groundcontrol.util import find_nearest_idx


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['figure.figsize'] = (10, 6)
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

SWEEPPARAMS = Path('sweep_params.csv')
MEASINDEX = Path('measuremens.index')


def make_parameter_text(
        mm: MeasurementModel,
        names: t.Optional[t.Collection[str]] = None,
        sigdigits: t.Optional[t.Collection[int]] = None):
    """Given a `MeasurementModel` returns a text that can be used with
    matplotlib's annotation functions.

    Args:
        mm:  The measurement model instance.
        names:  The names of the parameters to be used when making the text.
            If `None` all the found parameters are used.  Defaults to `None`.
        sigdigits:  Significant digits to use when representing values.  If
            provided the ordering and length of this object must be the same
            with `names` argument.
    """
    if (names is not None) and (sigdigits is not None):
        sigdigits = dict(zip(names, sigdigits))

    template = "{key}={value}"
    pars = parameters(mm)
    texts = []
    for name, par in pars.items():
        sigdigit = None
        if names is not None:
            if name not in names:
                continue
            else:
                if sigdigits is not None:
                    sigdigit = sigdigits.get(name, None)

        if sigdigit is not None:
            valuestr = mprefix_str(par.value, par.unit, sigdigits=sigdigit)
        else:
            valuestr = mprefix_str(par.value, par.unit)
        text = template.format(
                key=par.name,
                value=valuestr)
        texts.append(text)

    return '\n'.join(texts)


@declarative
class PSDEstimate:
    f: np.ndarray = quantity(un.hertz, "Frequency")
    s: np.ndarray = quantity("W/Hz", "PSD")

    @classmethod
    def from_sasm(cls, sasm: SASpectrumModel):
        # if there is nbw use it, otherwise just use rbw.
        try:
            nbw = sasm.nbw
            if nbw is None:
                raise AttributeError
        except AttributeError:
            nbw = sasm.rbw
        f = sasm.frequencies
        s = sasm.values
        # assuming unit is dBm
        sw = dbm2watt(s)

        return cls(f, sw/nbw)


@declarative
class NTDataSet:
    """Dataset for noise temperature estimation."""
    temps: np.ndarray
    psds: t.List[PSDEstimate]
    tups: t.List[TuningPoint]  # uncalibrated gain measurement
    fr: float
    fog: float
    spectra: np.ndarray  # rows -> frequencies, cols -> temps
    frequencies: np.ndarray

    @property
    def fpeak(self):
        return self.tups[0].peak_frequency

    @classmethod
    def make(cls, temps, psds, tups, fr, fog):
        spectra = np.vstack([psd.s for psd in psds]).T
        f = psds[0].f
        return cls(temps, psds, tups, fr, fog, spectra, f)

    def report_frequency_range(self):
        f = self.frequencies
        start = f[0]/1e9
        stop = f[-1]/1e9
        step = (f[1] - f[0])/1e3
        return (f"Frequencies (start, stop, step): "
                f"{start:.5f} GHz, {stop:.5f} GHz, {step:.3f} kHz")

    def frequency_band_cut(self, band: float):
        """For all PSDs cuts a region of `band` around the peak frequency.
        This method mutates the object!
        """
        for i, psd in enumerate(self.psds):
            f = psd.f
            s = psd.s
            fcenter = self.fpeak
            fdown = fcenter - band/2
            fup = fcenter + band/2
            mask = (f >= fdown) * (f <= fup)
            fnew = f[mask]
            snew = s[mask]
            psd = PSDEstimate(fnew, snew)
            self.psds[i] = psd
        self.frequencies = fnew
        self.spectra = self.spectra[mask, :]

    def apply_baseline(self, baseline: SParam1PModel):
        for i, tup in enumerate(self.tups):
            tupnew = calibrate_tp(tup, baseline)
            self.tups[i] = tupnew

    def fit_simple(self, gain, k_db=0, t_loss=0.04) -> "NTFitResult":
        f = self.fr + self.fog
        model = make_simple_model(f, gain, k_db, t_loss)

        spectra = self.spectra
        # different rows -> different frequencies

        results = []
        for column in spectra:
            result = model.fit(column, t=self.temps)
            results.append(result)

        frequencies = self.psds[0].f
        return NTFitResult(
                model,
                results, frequencies, self.temps,
                self.fr, self.fog)

    def plot_at(
            self, f: float, ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None):
        """Plot data for single frequency."""
        freqs = self.frequencies
        idx = find_nearest_idx(freqs, f)
        s = self.spectra[idx]
        ax = plot_nt_data(self.temps, s, ax, label=label)
        ax.grid(True)
        return ax


@declarative
class NTFitResult:
    model: lmfit.Model
    results: t.List[lmfit.model.ModelResult]
    frequencies: np.ndarray
    temperatures: np.ndarray
    fr: float
    fog: float

    @property
    def fpeak(self):
        return self.fr + self.fog

    def result_at(self, f: float):
        idx = find_nearest_idx(self.frequencies, f)
        return self.frequencies[idx], self.results[idx]

    def plot_at(
            self, f: float, ax: t.Optional[mpl.axes.Axes] = None,
            label: str = None):
        """Plot result data for single frequency."""
        ts = self.temperatures
        f, result = self.result_at(f)
        gain = result.best_values['c']
        if ax is None:
            ax = plt.gca()
        ax.plot(ts, result.data/gain, 'ok', label='Data')
        ax.plot(ts, result.best_fit/gain, '-r', label='Best Fit')
        ax.set_xlabel("Frequency [GHz]")
        ax.set_ylabel("PSD [W/Hz]")
        ax.grid(True, which='both')
        ax.legend()
        return ax

    def plot(
            self,
            ax: t.Optional[mpl.axes.Axes] = None,
            errorbar: bool = True):
        t_n_pars = [result.params['t_n'] for result in self.results]
        tns = np.array([par.value for par in t_n_pars])

        if ax is None:
            ax = plt.gca()

        f = self.frequencies - self.fpeak
        if errorbar:
            tnerrs = np.array([par.stderr for par in t_n_pars])
            ax.errorbar(x=f/1e3, y=tns, yerr=tnerrs, marker='o')
        else:
            ax.plot(f/1e3, tns, 'ok')

        ax.set_xlabel("$f-f_p/2$ [kHz]")
        ax.set_ylabel("$\mathrm{T}_\mathrm{n}$ [K]")
        ax.grid(True)
        return ax


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path

    def _read_helper(
            self,
            indices: t.Tuple[int, int, int],
            globdir: Path):
        i, j, k = indices[0], indices[1], indices[2]
        mrefijk = f"i{i}j{j}k{k}"
        files = globdir.glob(f"*_{mrefijk}_*.csv")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None

        return read_measurement_csv(file)

    @property
    def nt_dataset_count(self):
        return len(self.sweepdf.groupby(['fr', 'fog']))

    def read_nt_dataset(
            self,
            indices: t.Tuple[int, int]) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation
        corresponding to the measurement indices of resonance frequency and
        offset frequency.
        """
        j, k = indices
        grouped = self.sweepdf.groupby(['fr', 'fog'])
        for (fr, fog), df in grouped:
            # each group must correspond to single jk by def.
            ijks = df.ijk.values
            _, jj, kk = ijks[0]
            if (jj, kk) != (j, k):
                continue

            psds = []
            tups = []
            for inds in ijks:
                psds.append(PSDEstimate.from_sasm(self.read_sasm(inds)))
                tups.append(self.read_tp(inds))
            temps = df.tp.values

            dataset = NTDataSet.make(temps, psds, tups, fr, fog)
            return dataset

        # if we reach the end, it means we didn't find the indices.
        return None

    def iterate_nt_datasets(self) -> t.Iterator[NTDataSet]:
        grouped = self.sweepdf.groupby(['fr', 'fog'])
        for (fr, fog), df in grouped:
            # each group must correspond to single jk by def.
            ijks = df.ijk.values

            psds = []
            tups = []
            for inds in ijks:
                psds.append(PSDEstimate.from_sasm(self.read_sasm(inds)))
                tups.append(self.read_tp(inds))
            temps = df.tp.values

            dataset = NTDataSet.make(temps, psds, tups, fr, fog)
            yield dataset

    def read_baseline(
            self, ref: str = "BASELINE"):
        files = self._sppmdir.glob("*BASELINE*")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None
        return read_measurement_csv(file)

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, indices) -> t.Optional[TuningPoint]:
        return self._read_helper(indices, self._tpdir)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    @classmethod
    def from_path(
            cls,
            rootdir: Path,
            colnames=("tp", "fr", "fog")):
        """The csv contains data in the form of:
        # Var1, Var2, Var3
        0, 4, 0
        0, 4, 1
        0, 4, 2
        0, 9, 0
        0, 9, 1
        0, 9, 2
        5, 4, 0
        5, 4, 1
        .
        .
        .

        This form is a meshgrid, immediately usable with functions, but not so
        useful when we want to fetch another data corresponding to each row.
        The reason is sweep data is recorded with the complex index (i,j,k)
        rather than a single increasing index.
        """
        fname = rootdir / SWEEPPARAMS
        df = pd.read_csv(
            fname, skipinitialspace=True, delimiter=',',
            names=colnames, comment='#')

        gridarrs = {}
        uniqvals = {}

        for name in colnames:
            series = df[name]
            gridarrs[name] = series
            uniqvals[name] = np.unique(series)

        elemns = list(map(len, uniqvals.values()))
        ijk = np.unravel_index(df.index, elemns)
        df['ijk'] = tuple(zip(*ijk))
        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR)


def plot_nt_data(temperatures, psdvals, ax=None, label=None):
    if ax is None:
        ax = plt.gca()

    ax.plot(temperatures*1e3, psdvals, 'o', label=label)
    ax.set_xlabel("Source Temperature [mK]")
    ax.set_ylabel("PSD [W/Hz]")
    return ax


def plot_sppm_mag(
        sppm: SParam1PModel,
        ax: t.Optional[mpl.axes.Axes] = None):
    if ax is None:
        ax = plt.gca()
    f_ghz = sppm.frequencies/1e9
    ax.plot(f_ghz, sppm.mags)
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel("$|S_{21}|$ [dB]")
    ax.grid()
    return ax


def plot_sasm(
        sasm: SASpectrumModel,
        ax: t.Optional[mpl.axes.Axes] = None):
    if ax is None:
        ax = plt.gca()
    f_ghz = sasm.frequencies/1e9
    ax.plot(f_ghz, sasm.values)

    magunit = quantities(sasm)['values'].unit
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel(f"P$(B_r$={sasm.rbw:.0f}Hz) [{magunit}]")
    ax.grid()
    return ax


def plot_psd(
        psd: PSDEstimate,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: str = None,
        dbmscale: bool = False):
    if ax is None:
        ax = plt.gca()
    f_ghz = psd.f/1e9

    if dbmscale:
        ax.plot(f_ghz, watt2dbm(psd.s), label=label)
        ax.set_ylabel(f"PSD dB[mW/Hz]")
    else:
        ax.plot(f_ghz, psd.s, label=label)
        ax.set_ylabel(f"PSD [W/Hz]")

    ax.set_xlabel("Frequency [GHz]")
    ax.grid()
    return ax


def plot_tp(
        tp: TuningPoint,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: str = None,
        plot_params: bool = False):
    if ax is None:
        ax = plt.gca()
    f_ghz = tp.frequency/1e9
    ax.plot(f_ghz, tp.gain, label=label)
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel("$|S_{21}|$ [dB]")
    ax.grid()

    if plot_params:
        text = make_parameter_text(
                tp,
                ('resonance',
                    'peak_frequency',
                    'pump_power',),
                (9, 9, 3),
                )
        ax.text(
                1, 1, text, ha='right', va='top', transform=ax.transAxes,
                bbox={
                    'facecolor': 'white',
                    'edgecolor': 'black',
                    },
                fontsize=SMALLFONTSIZE)
    return ax


def calibrate_tp(
        tp: TuningPoint,
        baseline: SParam1PModel):
    """Deembeds the baseline from gain measurement in tuning point.
    Uses linear interpolation to approximate points.
    """
    ftp = tp.frequency
    fb = baseline.frequencies
    bmags = baseline.mags
    bl_intrp = np.interp(ftp, fb, bmags)
    # with baseline deembedded
    gaincal = tp.gain - bl_intrp
    return tp.evolve(gain=gaincal)


def johnson_noise_psd(temperature: float):
    k_B = cnst.Boltzmann
    T = temperature
    return k_B*T


def coth(x):
    return 1/np.tanh(x)


def noise_psd(temperature: float, frequency: float):
    """Power spectral density of the noise waves emitted by a perfectly
    matched resistor, including the quantum noise.
    """
    h = cnst.Planck
    k_B = cnst.Boltzmann
    T = temperature
    f = frequency
    arg = h*f/(2*k_B*T)
    return 0.5*h*f*coth(arg)


def make_simple_model(f, gain_exp, k_db, t_loss):
    """Creates a simple noise model fixing the frequency and temperature of
    loss parameters.

    Args:
        f:  Frequency at which the noise model will be created.
        k_db: Loss factor between noise source and input of amplifier.
            Positive definite.
        t_loss:  Temperature of the lossy elements after the noise source.
        gain_exp:  Expected gain of the whole chain as a linear W/W ratio.
            Providing this makes the convergence faster.
    """
    assert k_db >= 0

    gain_max = gain_exp*10
    gain_min = gain_exp/10

    k = db2lin_pow(-k_db)

    @lmfit.Model
    def simple_model(t, t_n, c):
        """Simple model to estimate noise temperature of an amplifying element
        connected to a perfectly matched noisy resistor acting as variable
        source.

        Args
            t: Temperature of source.
            c: Overall gain.
        """
        p_n = johnson_noise_psd(t_n)
        psrc = noise_psd(t, f)
        return c*(k*psrc + (1-k)*noise_psd(t_loss, f) + p_n)

    simple_model.set_param_hint(
            't_n', value=0.2, min=0.06, max=3)
    simple_model.set_param_hint(
            'c', value=gain_exp, min=gain_min, max=gain_max)
    return simple_model
