"""Analysis library for simple NT measurement data."""
from pathlib import Path
import typing as t

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from groundcontrol.measurement import read_measurement_csv
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import declarative

from .nt import NTDataSet, NTDataSetAlt
from .types import PSDEstimate


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['figure.figsize'] = (10, 6)
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

SWEEPPARAMS = Path('sweep_params.csv')
MEASINDEX = Path('measuremens.index')


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path

    isonlyspectra: bool

    def _read_helper(
            self,
            indices: t.Tuple[int],
            globdir: Path,
            mrefpre: str = ""):
        i = indices[0]
        mrefijk = f"i{i}"
        if mrefpre != "":
            mref = f"{mrefpre}-{mrefijk}"
        else:
            mref = mrefijk
        files = globdir.glob(f"*_{mref}_*.csv")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None

        return read_measurement_csv(file)

    @property
    def nt_dataset_count(self):
        return 1

    def read_nt_dataset(
            self,
            fpeak: t.Optional[float] = None,
            alt: bool = False) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation.

        fpeak is ignored if dataset contains gain measurements.
        """
        df = self.sweepdf
        indices = df.ijk.values
        psds = []
        tups = []
        for inds in indices:
            psds.append(PSDEstimate.from_sasm(self.read_sasm(inds)))
            if not self.isonlyspectra:
                tups.append(self.read_tp(inds))
        temps = df.tp.values

        baseline = self.read_baseline()

        if self.isonlyspectra:
            tups = None

        if alt:
            dataset = NTDataSetAlt.make_ntsimple(temps, psds, tups, baseline)
        else:
            dataset = NTDataSet.make(temps, psds, tups, fpeak=fpeak)
        return dataset

    def read_baseline(
            self, ref: str = "BASELINE"):
        files = self._sppmdir.glob("*BASELINE*")
        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            return None
        return read_measurement_csv(file)

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, indices) -> t.Optional[TuningPoint]:
        return self._read_helper(indices, self._tpdir)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    def read_resonance_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir, "RESONANCE")

    @classmethod
    def from_path(
            cls,
            rootdir: Path,
            colnames=("tp",),
            onlyspectra: bool = False):
        """The sweep parameters csv contains single array of
        temperature values.

        Args:
            rootdir:  Root directory of the data.
            colnames:  Column names in sweepparams.
            onlyspectra:  Will ignore any tuning point or gain data even if it
                exists.

        IMPORTANT:
            - The csv for sweepparams is malformed, so this function is just a
              workaround, don't read much into unnecessary parts for ijk
              column creation.
        """
        fname = rootdir / SWEEPPARAMS

        with open(fname, 'r') as fin:
            fin.readline()  # skipping header
            tps = np.array(list(
                    map(lambda s: s.strip(),
                        fin.readline().split(','))
                    ), dtype=float)

        df = pd.DataFrame({colnames[0]: tps})

        gridarrs = {}
        uniqvals = {}

        for name in colnames:
            series = df[name]
            gridarrs[name] = series
            uniqvals[name] = np.unique(series)

        elemns = list(map(len, uniqvals.values()))
        ijk = np.unravel_index(df.index, elemns)
        df['ijk'] = tuple(zip(*ijk))
        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR,
            isonlyspectra=onlyspectra)


