#!/usr/bin/env python
# coding: utf-8

# Analysis library for NT vs Resonance Frequency and Pump Offsets
# This one is for analysis of JPA NT data data taken at 2019.10.22

from pathlib import Path
import typing as t
from functools import partial

import attr
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy import constants as cnst
import lmfit

from groundcontrol.measurement import read_measurement_csv, MeasurementModel
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import quantity, quantities, parameters
from groundcontrol.friendly import mprefix_str
import groundcontrol.units as un
from groundcontrol.declarators import declarative
from groundcontrol.util import dbm2watt, watt2dbm, db2lin_pow
from groundcontrol.util import find_nearest_idx
from groundcontrol.analysis.nt import make_noreflection_model,\
    make_simple_model, NTFitResult, make_generic_model, NTModelParams,\
    noise_psd
from groundcontrol.analysis.nt import NTDataSetAlt as NTDataSet
from groundcontrol.analysis.calibrate import remove_baseline
from groundcontrol.logging import logger


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['figure.figsize'] = (10, 6)
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

SWEEPPARAMS = Path('sweep_params.csv')
MEASINDEX = Path('measuremens.index')


def make_parameter_text(
        mm: MeasurementModel,
        names: t.Optional[t.Collection[str]] = None,
        sigdigits: t.Optional[t.Collection[int]] = None):
    """Given a `MeasurementModel` returns a text that can be used with
    matplotlib's annotation functions.

    Args:
        mm:  The measurement model instance.
        names:  The names of the parameters to be used when making the text.
            If `None` all the found parameters are used.  Defaults to `None`.
        sigdigits:  Significant digits to use when representing values.  If
            provided the ordering and length of this object must be the same
            with `names` argument.
    """
    if (names is not None) and (sigdigits is not None):
        sigdigits = dict(zip(names, sigdigits))

    template = "{key}={value}"
    pars = parameters(mm)
    texts = []
    for name, par in pars.items():
        sigdigit = None
        if names is not None:
            if name not in names:
                continue
            else:
                if sigdigits is not None:
                    sigdigit = sigdigits.get(name, None)

        if sigdigit is not None:
            valuestr = mprefix_str(par.value, par.unit, sigdigits=sigdigit)
        else:
            valuestr = mprefix_str(par.value, par.unit)
        text = template.format(
                key=par.name,
                value=valuestr)
        texts.append(text)

    return '\n'.join(texts)


@declarative
class PSDEstimate:
    f: np.ndarray = quantity(un.hertz, "Frequency")
    s: np.ndarray = quantity("W/Hz", "PSD")

    @classmethod
    def from_sasm(cls, sasm: SASpectrumModel):
        # if there is nbw use it, otherwise just use rbw.
        try:
            nbw = sasm.nbw
            if nbw is None:
                raise AttributeError
        except AttributeError:
            nbw = sasm.rbw
        f = sasm.frequencies
        s = sasm.values
        # assuming unit is dBm
        sw = dbm2watt(s)

        return cls(f, sw/nbw)


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path
    _colinds: t.Tuple[int, int]

    def _read_helper(
            self,
            indices: t.Union[t.Tuple[int, int], int, None],
            globdir: Path,
            refstr: t.Optional[str] = None):

        if refstr is None:
            glob_template = "*{mrefijk}_*.csv"
        else:
            glob_template = "-".join(
                    (f"*{refstr}", "{mrefijk}_*.csv"))

        if indices is None:
            globstr = f"*{refstr}_*.csv"
        elif isinstance(indices, tuple):
            i, j = indices[0], indices[1]
            mrefijk = f"i{i}j{j}"
            globstr = glob_template.format(mrefijk=mrefijk)
        else:  # check for intness
            i = indices
            mrefijk = f"i{i}"
            globstr = glob_template.format(mrefijk=mrefijk)

        files = globdir.glob(globstr)

        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            raise IndexError(
                    f"No measurement corresponding to index {indices}."
                    f"Globber: '{globstr}'")

        return read_measurement_csv(file)

    @property
    def ndcount(self):
        """Number of points in the primary sweep."""
        return len(self.sweepdf.groupby(['i']))

    def read_nt_dataset(
            self,
            primary_index: int) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation
        corresponding to the measurement indices of resonance frequency and
        offset frequency.  If the spectrum data is missing for certain
        temperatures, returns a dataset containing spectra until the first
        missing data point.

        Returns:
            ntds:  The NT dataset if index exists.  `None` otherwise.
        """
        tind, find = self._colinds

        grouped = self.sweepdf.groupby(['i'])
        for i, df in grouped:
            # NOTE: i is the primary sweep index.
            if i != primary_index:
                continue
            # each group must correspond to single jk by def.
            ijks = df.ijk.values

            baseline = self.read_baseline(i)
            psds = []
            gains = []
            resonances = []
            for k, inds in enumerate(ijks):
                try:
                    sasm = self.read_sasm(inds)
                except IndexError:
                    break
                psds.append(PSDEstimate.from_sasm(sasm))
                gains.append(self.read_gain(inds))
                resonances.append(self.read_resonance(inds))
            tup = self.read_tp(i)
            temps = df.tp.values[:k+1]

            dataset = NTDataSet.make(
                    temps, psds, tup, gains, baseline,
                    resonances)
            return dataset

        # if we reach the end, it means we didn't find the indices.
        return None

    def iterate_nt_datasets(self):
        for i, df in self.sweepdf.groupby('i'):
            # The primary sweep is assumed to be on frequencies.
            f_ghz = df.fr.values[0]/1e9
            logger.info(f"Procuring dataset for sweep f: {f_ghz:.6f} GHz")
            ntds = self.read_nt_dataset(i)
            if ntds is None:
                raise StopIteration
            yield ntds

    def read_baseline(
            self, index: t.Optional[int] = None):
        """Returns the baseline measurement.

        Args:
            index:  Index of the baseline measurement.  If `None`, returns the
                general baseline measurement that is probably performed at the
                beginning of a measurement.

        """
        return self._read_helper(index, self._sppmdir, "BASELINE")

    def read_gain(self, indices: t.Tuple[int, int]):
        """Returns the gain measurement.

        Kwargs:
            indices:  Indices of the gain measurement.

        """
        return self._read_helper(indices, self._sppmdir, "GAIN")

    def read_resonance(self, indices: t.Tuple[int, int]):
        """Returns the resonance measurement.

        Kwargs:
            indices:  Index of the resonance measurement.

        """
        return self._read_helper(indices, self._sppmdir, "RESONANCE")

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, indices) -> t.Optional[TuningPoint]:
        return self._read_helper(indices, self._tpdir)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    def fit_all(self, kc, gain_rest, tf, tn_rest):
        fitresults = []
        for ntds in self.iterate_nt_datasets():
            fpeak = ntds.tup.peak_frequency/1e9
            fres = ntds.tup.resonance/1e9
            msg = f"Processing tup (fr|fpeak): {fres:.6f}|{fpeak:.6f} GHz"
            logger.info(msg)
            fitresult = ntds.fit_noreflection(kc, gain_rest, tf, tn_rest)
            fitresults.append(fitresult)
        return fitresults

    @classmethod
    def from_path(
            cls,
            rootdir: Path,
            temperature_col: int = 0,
            frequency_col: int = 1):
        """The csv contains data in the form of:
        # Var1, Var2, Var3
        0, 4, 0
        0, 4, 1
        0, 4, 2
        0, 9, 0
        0, 9, 1
        0, 9, 2
        5, 4, 0
        5, 4, 1
        .
        .
        .

        This form is a meshgrid, immediately usable with functions, but not so
        useful when we want to fetch another data corresponding to each row.
        The reason is sweep data is recorded with the complex index (i,j,k)
        rather than a single increasing index.
        """
        tcol = 'tp'
        fcol = 'fr'
        colnames = (tcol, fcol)
        colnames = (
                colnames[temperature_col],
                colnames[frequency_col])

        fname = rootdir / SWEEPPARAMS
        df = pd.read_csv(
            fname, skipinitialspace=True, delimiter=',',
            names=colnames, comment='#')

        # Temperature setpoints
        t_sp = np.unique(df[tcol])
        n_t_sp = len(t_sp)

        # Frequency setpoint count
        n_f_sp = len(df) // n_t_sp

        sweep_grid_shape = (n_f_sp, n_t_sp)

        i, j = np.unravel_index(range(len(df)), sweep_grid_shape)
        ijk = list(zip(i, j))
        df['ijk'] = ijk
        df['i'] = i
        df['j'] = j
        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR,
            colinds=(temperature_col, frequency_col))


def plot_nt_data(temperatures, psdvals, ax=None, label=None):
    if ax is None:
        ax = plt.gca()

    ax.plot(temperatures*1e3, psdvals, 'o', label=label)
    ax.set_xlabel("Source Temperature [mK]")
    ax.set_ylabel("PSD [W/Hz]")
    return ax


def plot_sppm_mag(
        sppm: SParam1PModel,
        ax: t.Optional[mpl.axes.Axes] = None):
    if ax is None:
        ax = plt.gca()
    f_ghz = sppm.frequencies/1e9
    ax.plot(f_ghz, sppm.mags)
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel("$|S_{21}|$ [dB]")
    ax.grid()
    return ax


def plot_sasm(
        sasm: SASpectrumModel,
        ax: t.Optional[mpl.axes.Axes] = None):
    if ax is None:
        ax = plt.gca()
    f_ghz = sasm.frequencies/1e9
    ax.plot(f_ghz, sasm.values)

    magunit = quantities(sasm)['values'].unit
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel(f"P$(B_r$={sasm.rbw:.0f}Hz) [{magunit}]")
    ax.grid()
    return ax


def plot_psd(
        psd: PSDEstimate,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: str = None,
        dbmscale: bool = False):
    if ax is None:
        ax = plt.gca()
    f_ghz = psd.f/1e9

    if dbmscale:
        ax.plot(f_ghz, watt2dbm(psd.s), label=label)
        ax.set_ylabel(f"PSD dB[mW/Hz]")
    else:
        ax.plot(f_ghz, psd.s, label=label)
        ax.set_ylabel(f"PSD [W/Hz]")

    ax.set_xlabel("Frequency [GHz]")
    ax.grid()
    return ax


def plot_tp(
        tp: TuningPoint,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: str = None,
        plot_params: bool = False):
    if ax is None:
        ax = plt.gca()
    f_ghz = tp.frequency/1e9
    ax.plot(f_ghz, tp.gain, label=label)
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel("$|S_{21}|$ [dB]")
    ax.grid()

    if plot_params:
        text = make_parameter_text(
                tp,
                ('resonance',
                    'peak_frequency',
                    'pump_power',),
                (9, 9, 3),
                )
        ax.text(
                1, 1, text, ha='right', va='top', transform=ax.transAxes,
                bbox={
                    'facecolor': 'white',
                    'edgecolor': 'black',
                    },
                fontsize=SMALLFONTSIZE)
    return ax


def calibrate_tp(
        tp: TuningPoint,
        baseline: SParam1PModel):
    """Deembeds the baseline from gain measurement in tuning point.
    Uses linear interpolation to approximate points.
    """
    ftp = tp.frequency
    fb = baseline.frequencies
    bmags = baseline.mags
    bl_intrp = np.interp(ftp, fb, bmags)
    # with baseline deembedded
    gaincal = tp.gain - bl_intrp
    return tp.evolve(gain=gaincal)

