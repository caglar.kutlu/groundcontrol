"""A script to measure the nonlinear characteristic of the JTWPA.

A CW signal is applied at a given frequency (f_stim) and power spectrum is
measured at the 2nd and 3rd multiples of this frequency.  This procedure is
repeated for varying bias currents and the individual spectra are recorded
along with the peak values.
"""
import typing as t
from argparse import ArgumentParser
from pathlib import Path
import sys
from datetime import datetime

import numpy as np
import attr

from groundcontrol import __version__
from groundcontrol.instruments.instrument import CommunicationError
from groundcontrol.controllers.jpa import JPAController
from groundcontrol.declarators import declarative, quantity, parameter,\
    quantities, parameters
from groundcontrol.measurement import MeasurementModel, SASpectrumModel
from groundcontrol.measurement import read_measurement_csv
import groundcontrol.units as un
from groundcontrol.logging import setup_logger
from groundcontrol.friendly import mprefix_str
from groundcontrol.util import dbm2watt
from groundcontrol.helper import Array


path: Path = Path("~/Measurements/200330/JTWPA_HARM_19dBm_f2.3G__2/")
sasmdir: Path = path/'SASM'
smmdir: Path = path/'SMM'
path.mkdir('plots')
out: Path = path / 'plots'


class HarmonicPowerSet(MeasurementModel):
    p_2: Array[float] = quantity(un.dBm, "p_2")
    f_2: Array[float] = quantity(un.dBm, "f_2")
    p_3: Array[float] = quantity(un.dBm, "p_3")
    f_3: Array[float] = quantity(un.dBm, "f_3")
    i_b: Array[float] = quantity(un.ampere, "i_b")
    timestamp: datetime = quantity(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))
    p_s: Array[float] = parameter(un.dBm, "SignalPower")
    rbw: Array[float] = parameter(un.hertz, "ResolutionBandwidth")


hps = read_measurement_csv(f"{path}")



fig, ax = plt.subplots()
ax.plot(hps.i_b/1e-6, hps.p_2, label='P2')
ax.plot(hps.i_b/1e-6, hps.p_3, label='P3')
ax.set_xlabel("Bias [uA]")
ax.set_ylabel("Power [dBm]")


fig, ax = plt.subplots()
ax.plot(hps.i_b/1e-6, hps.p_2-np.mean(hps.p_2), label='P2')
ax.plot(hps.i_b/1e-6, hps.p_3, hps.p_3-np.mean(hps.p_3), label='P3')
ax.set_xlabel("Bias [uA]")
ax.set_ylabel("Mean Scaled Ratio [dB]")


# sasms = []
# for fl in sasmdir.glob('.csv'):
    # measurement = read_measurement_csv(fl)
    # sasms.append(measurement)
    
# smms = []
# for fl in sasmdir.glob('.csv'):
    # measurement = read_measurement_csv(fl)
    # smms.append(measurement)


# sasms = sorted(sasms, key=lambda m: m.mref)
