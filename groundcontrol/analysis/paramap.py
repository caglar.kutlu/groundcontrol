"""Module for reading paramap data."""
import typing as t
import sys
import time
from datetime import datetime
from pathlib import Path
from parse import parse
import json
from collections import defaultdict
import xarray as xr
from functools import wraps
from numbers import Number

from dask import dataframe as ddataframe
import cattr
import numpy as np
import attr
import xarray as xr
import pandas as pd

from groundcontrol.measurement import MeasurementModel, SParam1PModel
from groundcontrol.measurement import read_measurement_csv
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.declarators import quantity, parameter, declarative, setting
from groundcontrol.declarators import from_json
from groundcontrol.settings import VNASettings
from groundcontrol.logging import logger
import groundcontrol.units as un
from groundcontrol.util import unique, fullrange, resize
from groundcontrol.helper import Array, domaybe, StrEnum


class ParaMapSweepParameters(MeasurementModel):
    fpumps: Array[float] = quantity(un.hertz, "PumpFrequency")
    ppumps: Array[float] = quantity(un.dBm, "PumpPower")
    detunings: Array[float] = quantity(un.nounit, "Detuning")


class ParaMapRecord(MeasurementModel):
    det: float = quantity(un.nounit, "Detuning")
    fp: float = quantity(un.hertz, "PumpFrequency")
    pp: float = quantity(un.dBm, "PumpPower")
    ib: float = quantity(un.ampere, "BiasCurrent")
    fr: float = quantity(un.hertz, "ResonanceFrequency")
    bw: float = quantity(un.hertz, "ResonanceBandwidth")
    fo1: float = quantity(un.hertz, "FrequencyOffset1")
    fo2: float = quantity(un.hertz, "FrequencyOffset2")
    g1: float = quantity(un.dB, "Gain@F1")
    g2: float = quantity(un.dB, "Gain@F2")
    g1m: float = quantity(
        un.dB, "Gain@F1m", description="Gain at mirror frequency of f1"
    )
    g2m: float = quantity(
        un.dB, "Gain@F2m", description="Gain at mirror frequency of f2"
    )
    ps: float = parameter(un.dBm, "SignalPower")
    timestamp: datetime = parameter(
        un.nounit, "Timestamp", default=attr.Factory(datetime.now)
    )


@declarative
class ParaMapSettings:
    version: str = setting(
        nicename="Script Version",
        description="Version of the script used during measurement.",
    )
    ib_set: t.Optional[t.List] = setting(
        nicename="Current Setpoints", description="The bias current in ampere."
    )
    detuning_rng: t.Tuple[float, float, float] = setting(
        nicename="Detuning Range", description="The frequency detunings."
    )
    ppump_rng: t.Tuple[float, float, float] = setting(
        nicename="Pump Power Range", description="Pump powers."
    )
    frequency_offset_1: float = setting(
        nicename="Frequency Offset 1",
        description="First frequency offset to measure gain at. "
        "Offset is defined as `delta=fp/2-fs`.",
    )
    frequency_offset_2: float = setting(
        nicename="Frequency Offset 2",
        description="Second frequency offset to measure gain at.",
    )
    sample_count: int = setting(
        nicename="Sample Count",
        description="Number of samples to collect statistics from.",
    )
    vna_power: float = setting(
        nicename="VNA Power", description="The output power for the VNA."
    )
    vna_ifbw: float = setting(
        nicename="VNA IF Bandwidth", description="The IF bandwidth setting for the VNA."
    )
    vna_navg: int = setting(
        nicename="VNA Average Count",
        description="Average count setting for the VNA.",
        default=1,
    )
    vna_res_power: float = setting(
        nicename="Resonance VNA Power",
        description="VNA power to use when doing resonance measurement.",
    )
    vna_res_ifbw: float = setting(
        nicename="Resonance VNA IF Bandwidth",
        description="VNA ifbw to use when doing resonance measurement.",
    )
    vna_res_span: float = setting(
        nicename="Resonance VNA Span",
        description="VNA span to use when doing resonance measurement.",
    )
    vna_res_sweep_step: float = setting(
        nicename="Resonance VNA Sweep Step",
        description="VNA sweep step to use when doing resonance " "measurement.",
    )
    do_cal: bool = setting(
        nicename="Perform Calibration",
        description="If enabled, the JPA is tuned to the provided "
        "calibration bias and a transmission measurement is "
        "performed to be used as baseline.",
    )
    ib_cal: t.Optional[float] = setting(
        nicename="Calibration Current",
        description="Bias current to be used when calibration "
        "is performed.  If not given, and if calibration is "
        "enabled, bias current is set to 10 uA higher than "
        "it's current value.",
    )
    vna_bl_power: t.Optional[float] = setting(
        nicename="Baseline VNA Power",
        description="VNA power to be used when doing baseline " "measurement.",
    )
    vna_bl_fstart: t.Optional[float] = setting(
        nicename="Baseline VNA Start Frequency",
        description="Start frequency setting for the baseline measurement "
        "using VNA.",
    )
    vna_bl_fstop: t.Optional[float] = setting(
        nicename="Baseline VNA Stop Frequency",
        description="Stop frequency setting for the baseline measurement " "using VNA.",
    )
    skip_mirror: bool = setting(
        nicename="Skip Mirror",
        description="Whether or not to skip the measurements at the "
        "mirror frequencies.",
    )
    sweep_order: str = setting(
        nicename="Sweep Order",
        default="PUMPFIRST",
        description="Order of sweep.  One of ['PUMPFIRST', 'DETFIRST']",
    )
    delay_ppump: t.Optional[float] = setting(
        un.second,
        default=None,
        nicename="PumpPowerChangeDelay",
        description="Delay after changing pump power in seconds.",
    )
    delay_fpump: t.Optional[float] = setting(
        un.second,
        default=None,
        nicename="PumpFreqChangeDelay",
        description="Delay after changing pump frequency in seconds.",
    )
    delay_ib: t.Optional[float] = setting(
        un.second,
        default=None,
        nicename="CoilCurrentChangeDelay",
        description="Delay after changing coil current in seconds.",
    )


def read_settings(fname):
    return from_json(fname, ParaMapSettings)


@declarative
class RawDataSet:
    minds: t.List[int]
    pmrs: t.List[ParaMapRecord]
    ress: t.List[SParam1PModel]
    settings: ParaMapSettings

    @classmethod
    def make(cls, minds, pmrs, ress, settings):
        obj = cls(minds, pmrs, ress, settings)
        obj.detunings = fullrange(*settings.detuning_rng)
        obj.ppumps = fullrange(*settings.ppump_rng)
        return obj

    def to_xarray(self):
        dets = self.detunings
        ndets = len(dets)
        ppumps = self.ppumps
        nppumps = len(ppumps)

        varnames = ("g1", "g2", "g1m", "g2m")
        datarrsdict = defaultdict(list)
        frs = []
        bws = []
        ibs = []
        for pmr in self.pmrs:
            if self.settings.sweep_order == "PUMPFIRST":
                datres = {
                    k: resize(dat, (ndets, nppumps))
                    for k, dat in zip(varnames, (pmr.g1, pmr.g2, pmr.g1m, pmr.g2m))
                }
                for k, dat in datres.items():
                    datarrsdict[k].append(
                        xr.DataArray(
                            dat, coords=(("detuning", dets), ("ppump", ppumps))
                        ).transpose()
                    )

            elif self.settings.sweep_order == "DETFIRST":
                datres = {
                    k: resize(dat, (nppumps, ndets))
                    for k, dat in zip(varnames, (pmr.g1, pmr.g2, pmr.g1m, pmr.g2m))
                }
                for k, dat in datres.items():
                    datarrsdict[k].append(
                        xr.DataArray(
                            dat, coords=(("ppump", ppumps), ("detuning", dets))
                        )
                    )

            frs.append(pmr.fr[0])
            bws.append(pmr.bw[0])
            ibs.append(pmr.ib[0])

        ds = xr.Dataset(
            {
                k: xr.concat(arrs, pd.Index(ibs, name="ib"))
                for k, arrs in datarrsdict.items()
            }
        )
        ds.coords["fr0"] = ("ib", frs)
        ds.coords["bw0"] = ("ib", bws)

        sets = self.settings
        ds.attrs["fo1"] = sets.frequency_offset_1
        ds.attrs["fo2"] = sets.frequency_offset_2
        ds.attrs["ps"] = sets.vna_power
        return ds

    @classmethod
    def from_path(cls, path: Path, midx: t.Optional[t.Union[t.Tuple, int]] = None):
        """Creates the object using the path.

        Args:
            path: Path to the measurement folder.

        Returns:
            obj: DataSet object.

        """
        settings = read_settings(path / "settings.json")
        mio = MIOFileCSV.open(path, "r")
        df = mio.read_measurement_index_df()
        pmrpaths = unique(df.datapath[df.mclass == "ParaMapRecord"].values)
        sppmpaths = df.datapath[df.mclass == "SParam1PModel"].values

        pmrs = []
        ress = []
        minds = []

        # Assuming ordered dataset AND only RES type sppm
        for pmrpath, sppmpath in zip(pmrpaths, sppmpaths):
            pdir, abbrev, mref, index = mio.parse_path(pmrpath)
            mrefind = parse("EXP-i{:d}", mref)[0]

            if isinstance(midx, int):
                if mrefind != midx:
                    continue
            elif isinstance(midx, t.Collection):
                if mrefind not in midx:
                    continue
            # can be none

            logger.info(f"Reading MREF={mref}.")
            pmrs.append(read_measurement_csv(pmrpath))
            # Assume there is no other SPPM
            ress.append(read_measurement_csv(sppmpath))
            minds.append(mrefind)

        return cls.make(minds, pmrs, ress, settings)


def timer(fun):
    @wraps(fun)
    def wrapped(*args, **kwargs):
        t0 = time.time()
        retval = fun(*args, **kwargs)
        t1 = time.time()
        print(f"{fun.__name__}: {t1 - t0}")
        return retval
    return wrapped


# def _dfsort(df):
#     order = numpy.lexsort(
#             [df[col].values for col in reversed(list(df.columns))])
#     for col in list(df.columns):
#             df[col] = df[col].values[order]

@declarative
class ParaSelector:
    """A class to dump selection and cutting algorithms that work on paramap data
    sets.

    THIS CLASS IMPLEMENTATION MAY CHANGE, ONLY RELY ON `generate_lut`.

    Augments the dataset with two coordinates if they don't already exist:
        fp:  Pump frequency, derived from detuning, fr0 and bw0.
        fac: Amplification center frequency.  Corresponds to fp/2 for 3WM
            paramps.

    Exchanges the `ib` coordinate with `fr0` coordinate internally.

    """

    ds: xr.Dataset

    class UpsampleMethod(StrEnum):
        LINEAR = "linear"

    def _linupsample(self, ar, multiplier):
        n = len(ar)
        _x = np.arange(n)
        _xx = np.linspace(0, n - 1, n * multiplier)
        return np.interp(_xx, _x, ar)

    def select_within(self, var_or_coord: str, vmin: float, vmax: float):
        var = self.ds[var_or_coord]
        return self.ds((var >= vmin) & (var <= vmax))

    def gaincut(self, g1min, g1max, gdelta):
        ds = self.ds
        g1mask = (ds.g1 >= g1min) & (ds.g1 <= g1max)
        mask = g1mask & ((ds.g1 - ds.g2) <= gdelta)
        return ds.where(mask)

    @timer
    def upsample(
        self,
        dim: t.Union[str, t.Union[t.Tuple[str], t.List[str]]],
        multiplier: t.Union[int, t.Sequence[int]],
        method: UpsampleMethod = "linear",
    ):
        """Upsamples the given dimensions with to have the samples equal to
        their current sample count multiplied by the `multiplier` argument."""
        ds = self.ds

        if method != ParaSelector.UpsampleMethod.LINEAR:
            raise ValueError("Currently only upsampling method is 'linear'.")

        if isinstance(dim, (list, tuple)):
            if not isinstance(multiplier, (list, tuple)):
                raise ValueError(
                        "If `dim` is list, `multiplier` must also be a list.")

            newvars = {}
            for dm, mult in zip(dim, multiplier):
                val = self._linupsample(ds[dm], mult)
                newvars[dm] = val
            return ds.interp(**newvars)
        else:
            val = self._linupsample(ds[dim].data, multiplier)
            return ds.interp(**{dim: val})

    def generate_lut(
        self,
        gbe: t.Sequence[Number],
        fbe: t.Union[float, t.Sequence[float]],
        n_fr0up: int = 1,
        n_detup: int = 1,
        n_ppump: int = 1,
        gdelta: t.Optional[float] = None,
        nhead: int = 1,
    ) -> pd.DataFrame:
        """This function is used to generate a set of points to be used when
        tuning the paramp to the desired frequency and gain, i.e. a Look-Up
        Table.  The output dataframe is indexed by two parameters: `faccat` and
        `g1cat`.  The `faccat` indexes the amplification frequency and g1cat
        indexes the peak gain.  Note that these two values are just labels for
        the bins, not the actual expected values.

        During paramap measurements not all amplification frequencies are
        thoroughly measured.  It is empirically known that paramaps change
        smoothly over passive resonance frequencies.  We exploit this here to
        generate a denser frequency data by upsampling over 'detuning' and
        'fr0' dimensions.  This, in turn makes `fac` denser.  The minimization
        procedures are then applied on this denser dataset.

        The actions taken when tuning a paramp can be simplified into two
        steps: 1. Tune the amplification to be centered at a certain frequency.
        2. Tune the gain to a desired level.  3. Tune the noise to a minimum
        level.

        While the first 2 is simple enough, evaluating the 3rd action while
        satisfying 1 and 2 becomes tricky.  It is known that if there are two
        working points giving the same gain and frequency response, the one
        requiring less pump power will have lower noise.  This function uses
        this heuristic to generate points with expected minimum in noise
        temperature.

        All binning is done with open left and closed right intervals.

        Args:
            gbe:  Gain bin edges.
            fbe (array-like or float):  Frequency bin edges.  If `float`, bin
                edges are generated using it as width.
            n_fr0up:  Upsample count for fr0.
            n_detup:  Upsample count for detuning.
            gdelta: If supplied, used to perform the selection of data where
                "ds.g1 - ds.g2 <= gdelta" is satisfied.
            nhead:  Number of minimum pump power samples to pick after
                categorizing into gain and frequency bins.
        """
        ds = self.ds

        g1minmask = ds.g1 > gbe[0]
        g1maxmask = ds.g1 <= gbe[-1]
        gdeltamask = (ds.g1 - ds.g2) <= gdelta if gdelta is not None else True

        mask = g1minmask & g1maxmask & gdeltamask
        self.ds = ds.where(mask, drop=True)

        upargs = []
        if n_fr0up > 1:
            upargs.append(("fr0", n_fr0up))
        if n_detup > 1:
            upargs.append(("detuning", n_fr0up))
        if n_ppump > 1:
            upargs.append(("ppump", n_ppump))

        if upargs != []:
            updim, upmult = zip(*upargs)
            ds = self.upsample(updim, upmult)

        if isinstance(fbe, float):
            step = fbe
            fbe = np.arange(ds.fac.min().item(), ds.fac.max().item(), step)

        t0 = time.time()

        df = ds.stack(
                sid=['fr0', 'detuning', 'ppump']
                ).reset_index('sid').to_dataframe()
        t1 = time.time()
        print("To df took ", t1 - t0, "s.")
        t0 = t1

        g1cat = pd.cut(df.g1, gbe, right=True)
        t1 = time.time()
        print("Cut g1 took ", t1 - t0, "s.")
        t0 = t1

        faccat = pd.cut(df.fac, fbe, right=True)
        t1 = time.time()
        print("Cut fac took ", t1 - t0, "s.")
        t0 = t1

        # below is the original algorithm
        # can't work on large dataset when upsampling is high
#         ddf = (
            # df.assign(g1cat=g1cat, faccat=faccat)
            # .sort_values("ppump", ascending=True)
            # .groupby(["faccat", "g1cat"])
            # .head(nhead)
            # .dropna(subset=['faccat', 'g1cat', 'g1', 'g2'], how='any')
            # .reset_index()
            # .set_index(['faccat', 'g1cat'])
            # .sort_index()
        # )
        df['faccat'] = faccat
        df['g1cat'] = g1cat
        ddf = ddataframe.from_pandas(df, chunksize=50000)
        df = ddf.dropna(subset=['g1cat', 'faccat'], how='any').compute()
        t1 = time.time()
        print("Assign & cat cleanup took ", t1 - t0, "s.")
        t0 = t1

        ddf = df

        dfg = ddf.groupby(["faccat", "g1cat"], observed=True)
        t1 = time.time()
        print("Groupby took ", t1 - t0, "s.")
        t0 = t1

        dfns = dfg.apply(lambda df: df.nsmallest(nhead, columns='ppump'))
        t1 = time.time()
        print("nsmallest took ", t1 - t0, "s.")
        t0 = t1

        _dfns = dfns.dropna(
                subset=['faccat', 'g1cat', 'g1', 'g2'], how='any')
        del dfns
        dfns = _dfns
         
        t1 = time.time()
        print("Dropna took ", t1 - t0, "s.")
        t0 = t1

        dfns.set_index(['faccat', 'g1cat'], inplace=True)
        t1 = time.time()
        print("set_index took ", t1 - t0, "s.")
        t0 = t1

        dfns.sort_index(inplace=True)
        t1 = time.time()
        print("sort_index took ", t1 - t0, "s.")
        t0 = t1

        return dfns

    @classmethod
    def from_dataset(cls, ds):
        fp = ((ds.detuning * ds.bw0 / 2) + ds.fr0) * 2
        # Amplification center frequency
        fac = fp / 2
        nds = ds.assign_coords({"fac": fac, "fp": fp})
        nds = nds.swap_dims({"ib": "fr0"})
        return cls(nds)


def _intervalparse(s: str):
    # Parses stuff like "(5.4e9, 5.5e9]"
    return tuple(map(float, s[1:-1].split(', ')))


def lut_csv2df(fname: Path):
    """Reads the LUT csv file into a pandas DataFrame."""
    df = pd.read_csv(
            fname,
            converters={
                'faccat': _intervalparse,
                'g1cat': _intervalparse})

    df.faccat = pd.IntervalIndex.from_tuples(df.faccat)
    df.g1cat = pd.IntervalIndex.from_tuples(df.g1cat)

    return df.set_index(['faccat', 'g1cat'])


@attr.s(auto_attribs=True)
class ParaLUT:
    df: pd.DataFrame

    def lookup_wp(self):
        pass

    @classmethod
    def from_csv(cls, fname: Path):
        return cls(lut_csv2df(fname))
