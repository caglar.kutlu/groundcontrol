"""Some types to use in analysis."""
from datetime import datetime

import numpy as np
import attr

from groundcontrol.measurement import (
        SASpectrumModel, MeasurementModel,
        parameter, quantity)
from groundcontrol.declarators import declarative
from groundcontrol.util import dbm2watt
import groundcontrol.units as un
from groundcontrol.helper import Array


@declarative
class PSDEstimate:
    f: Array[float] = quantity(un.hertz, "Frequency")
    s: Array[float] = quantity("W/Hz", "PSD")

    @classmethod
    def from_sasm(cls, sasm: SASpectrumModel):
        # if there is nbw use it, otherwise just use rbw.
        try:
            nbw = sasm.nbw
            if nbw is None:
                raise AttributeError
        except AttributeError:
            nbw = sasm.rbw
        f = sasm.frequencies
        s = sasm.values
        # assuming unit is dBm
        sw = dbm2watt(s)

        return cls(f, sw/nbw)


class ResonanceParamModel(MeasurementModel):
    frequency: float = quantity(un.hertz, "ResonanceFrequency")
    linewidth: float = quantity(un.hertz, "ResonanceLinewidth")
    frequency_err: float = quantity(un.hertz, "ResonanceFrequencyError")
    linewidth_err: float = quantity(un.hertz, "LinewidthError")
    timestamp: datetime = parameter(
            un.nounit, "Timestamp",
            default=attr.Factory(datetime.now))
