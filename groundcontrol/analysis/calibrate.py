"""Module for describing calibration routines for deembedding microwave
networks from measured S-parameters.

"""
import typing as t
import numpy as np

from groundcontrol.measurement import SParam1PModel
from groundcontrol.util import UNumeric


def remove_baseline(
        f: UNumeric,
        smag: UNumeric,
        f_baseline: UNumeric,
        smag_baseline: UNumeric,
        scale: str = 'log'):
    """This removes the baseline from the given transmission measurement
    interpolating baseline measurements where necessary.  Note that for the
    microwave magnitude measurements, this ignores any effect mismatch, and
    thus very naive.

    Args:
        f:  Frequencies where the transmission measurement was done.
        smag:  Transmission measurement values from which the baseline will be
            removed.
        f_baseline:  Baseline measurement frequencies.
        smag_baseline:  Baseline measurement magnitudes.
        scale:  Scale of transmission measurements.  One of ['log', 'lin'].

    """
    interpolado = np.interp(f, f_baseline, smag_baseline)

    if scale == 'log':
        gaincal = smag - interpolado
    elif scale == 'lin':
        gaincal = smag/interpolado
    else:
        raise ValueError("Scale can only be one of ('log', 'lin')")
    return gaincal


def remove_baseline_sppm(
        sppm: SParam1PModel, bl: SParam1PModel):
    mags = remove_baseline(
            sppm.frequencies, sppm.mags, bl.frequencies, bl.mags,
            scale='log')
    uphases = remove_baseline(
            sppm.frequencies, sppm.uphases, bl.frequencies, bl.uphases,
            scale='log')
    phases = remove_baseline(
            sppm.frequencies, sppm.phases, bl.frequencies, bl.phases,
            scale='log')

    return sppm.evolve(
            mags=mags,
            phases=phases,
            uphases=uphases)


