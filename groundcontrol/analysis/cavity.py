"""This module has the cavity parameter extraction stuff."""

import typing as t

import numpy as np
import lmfit
import attr

from minalyse.util import fwhm_indices


@attr.s(auto_attribs=True)
class CavityParameters:
    resf: float
    qfactor: float
    bw: float
    beta: float

    @classmethod
    def create(cls, resf, qfactor, beta, bw=None):
        if beta is None:
            beta = resf/qfactor
        return cls(resf,qfactor,bw,beta)


def lorentzian(f, centerf, bw):
    """Lorentzian function with shape parameters centerf and bw.

    Args:
        f(float): Frequency.
        centerf(float): Center frequency.
        bw(float): Bandwidth.

    """
    return (1+((f-centerf)/(bw/2))**2)**(-1)


def resonatorfunc(f, centerf, bw):
    """This is the form of the resonator function without approximations.
    
    Args:
        f(float): Frequency.
        centerf(float): Center frequency.
        bw(float): Bandwidth.

    """
    k = (bw/centerf)**2 # approx.
    return (((centerf**2 - f**2)/(np.sqrt(k)*centerf*f))**2 + 1)**(-1)


def fit_s21_linear(freq, s21mag, fitmodel='lorentzian'):
    """Performs a fit for the S_{21} using a model to extract cavity parameters Q
    and bandwidth.  The transmitted power through a cavity has a
    close-to-Lorentzian shape.  This function takes the s-parameter measurement
    in linear scale, so the squareroot of a usual lorentzian(resonator) function
    is used to perform the fit against the s21mag input.  

    Fit Models:
        "lorentzian":  A good approximation.
        "resonator":  A more realistic resonator function.

    Args:
        freq(float): Frequency in Hz.
        s21mag(float): S-paremeter in linear scale.
        fitmodel(str):  Must be one of {'lorentzian', 'resonator'}.
    
    """
    if fitmodel == "resonator":
        powerfunc = resonatorfunc 
    elif fitmodel == 'lorentzian': 
        powerfunc = lorentzian
    else:
        raise ValueError(f"There is no model with the given fitmodel {fitmodel}.")
        
    func = lambda f, centerf, bw, peak: peak*np.sqrt(powerfunc(f,centerf,bw))
    model_s21 = lmfit.Model(func, name="fitmodel")
    model_s21.name = fitmodel
    
    deltaf = freq[1] - freq[0]
    peak0 = s21mag.max()
    inds = fwhm_indices(s21mag**2)
    bw0 = (inds[1] - inds[0])*deltaf
    centerf0 = np.median(freq)
    params0 = model_s21.make_params(peak=peak0, bw=bw0, centerf=centerf0)
    result = model_s21.fit(s21mag, params0, f=freq)
    return result


def calculate_overcoupled_beta(freq, s22mag):
    """Calculates the beta, assuming overcoupling."""
    smin, smax = s22mag.min(), s22mag.max()
    Gamma = smin/smax
    beta = (1 + Gamma)/(1 - Gamma)
    return beta


def calculate_cavity_parameters(f_s21, mag_s21, f_s22, mag_s22,
        fitmodel='lorentzian'):
    s21fitresult = fit_s21_linear(f_s21, mag_s21, fitmodel='lorentzian')
    resf = s21fitresult.best_values['centerf']
    bw = s21fitresult.best_values['bw']
    q = resf/bw
    beta = calculate_overcoupled_beta(f_s22, mag_s22)
    return CavityParameters(resf, q,  bw, beta)

