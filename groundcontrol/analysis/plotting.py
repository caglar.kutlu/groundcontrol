"""Collection of plotting functionalities for the analysis generally."""
import typing as t
from itertools import islice

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.offsetbox import (
    AnchoredOffsetbox, TextArea)
import lmfit
import numpy as np

from groundcontrol.measurement import SASpectrumModel,\
        SParam1PModel, MeasurementModel
from groundcontrol.declarators import quantities, parameters
from groundcontrol.util import watt2dbm, db2lin_pow
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.friendly import mprefix_str
from groundcontrol.util import lin2db_pow
from .types import PSDEstimate


SMALLFONTSIZE = 12
MIDFONTSIZE = 16
BIGFONTSIZE = 24


mpl.rcParams['font.size'] = MIDFONTSIZE
mpl.rcParams['figure.figsize'] = (10, 6)
mpl.rcParams['axes.formatter.use_mathtext'] = True
mpl.rcParams['axes.formatter.useoffset'] = False


class AnchoredText(AnchoredOffsetbox):
    def __init__(self, s, loc, pad=0.4, borderpad=0.5,
                 prop=None, frameon=True):
        self.txt = TextArea(s, minimumdescent=False)
        super().__init__(loc, pad=pad, borderpad=borderpad,
                         child=self.txt, prop=prop, frameon=frameon)


def make_parameter_text(
        mm: MeasurementModel,
        names: t.Optional[t.Collection[str]] = None,
        sigdigits: t.Optional[t.Collection[int]] = None):
    """Given a `MeasurementModel` returns a text that can be used with
    matplotlib's annotation functions.

    Args:
        mm:  The measurement model instance.
        names:  The names of the parameters to be used when making the text.
            If `None` all the found parameters are used.  Defaults to `None`.
        sigdigits:  Significant digits to use when representing values.  If
            provided the ordering and length of this object must be the same
            with `names` argument.
    """
    if (names is not None) and (sigdigits is not None):
        sigdigits = dict(zip(names, sigdigits))

    template = "{key}={value}"
    pars = parameters(mm)
    texts = []
    for name, par in pars.items():
        sigdigit = None
        if names is not None:
            if name not in names:
                continue
            else:
                if sigdigits is not None:
                    sigdigit = sigdigits.get(name, None)

        if sigdigit is not None:
            valuestr = mprefix_str(par.value, par.unit, sigdigits=sigdigit)
        else:
            valuestr = mprefix_str(par.value, par.unit)
        text = template.format(
                key=par.name,
                value=valuestr)
        texts.append(text)

    return '\n'.join(texts)


def draw_text_box(
        ax: mpl.axes.Axes,
        text: str,
        loc: str = 'lower right'):
    """Draw a text-box anchored to the `loc` position the figure.

    Args:
        ax: Axes object.
        text: Text to be put in the box.
        loc: String representing where to put the box.  Acceptable input
            strings are:  ['upper right', 'upper left', 'lower
            left', 'lower right', 'right', 'center left', 'center right',
            'lower center', 'upper center', 'center']
    """
    at = AnchoredText(text, loc=loc, frameon=True)
    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
    ax.add_artist(at)
    return at


def plot_nt_data(
        temperatures, psdvals, ax=None, label=None, **kwargs):
    if ax is None:
        ax = plt.gca()

    ax.plot(temperatures, psdvals, 'o', label=label, **kwargs)
    ax.set_xlabel("Source Temperature [K]")
    ax.set_ylabel("PSD [W/Hz]")
    return ax


def plot_gain_vs_temp(
        temperatures, gains_db, ax=None, label=None,
        cal: bool = False, dbscale: bool = True):
    if ax is None:
        ax = plt.gca()

    if dbscale:
        unit = 'dB'
        gains = gains_db
    else:
        unit = 'W/W'
        gains = db2lin_pow(gains_db)

    ax.plot(temperatures, gains, 'o', label=label)
    ax.set_xlabel("Source Temperature [K]")

    if cal:
        ax.set_ylabel(f"Gain [{unit}]")
    else:
        ax.set_ylabel(f"|$S_{{21}}$ [{unit}]|")
    return ax


def plot_sppm_mag(
        sppm: SParam1PModel,
        ax: t.Optional[mpl.axes.Axes] = None,
        label=None):
    if ax is None:
        ax = plt.gca()
    f_ghz = sppm.frequencies/1e9
    ax.plot(f_ghz, sppm.mags, label=label)
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel("$|S_{21}|$ [dB]")
    ax.grid()
    return ax


def plot_sasm(
        sasm: SASpectrumModel,
        ax: t.Optional[mpl.axes.Axes] = None,
        label=None):
    if ax is None:
        ax = plt.gca()
    f_ghz = sasm.frequencies/1e9
    ax.plot(f_ghz, sasm.values, label=label)

    magunit = quantities(sasm)['values'].unit
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel(f"P$(B_r$={sasm.rbw:.0f}Hz) [{magunit}]")
    ax.grid()
    return ax


def plot_psd(
        psd: PSDEstimate,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: str = None,
        dbmscale: bool = False):
    if ax is None:
        ax = plt.gca()
    f_ghz = psd.f/1e9

    if dbmscale:
        ax.plot(f_ghz, watt2dbm(psd.s), label=label)
        ax.set_ylabel(f"PSD dB[mW/Hz]")
    else:
        ax.plot(f_ghz, psd.s, label=label)
        ax.set_ylabel(f"PSD [W/Hz]")

    ax.set_xlabel("Frequency [GHz]")
    ax.grid()
    return ax


def plot_tp(
        tp: TuningPoint,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: str = None,
        plot_params: bool = False):
    if ax is None:
        ax = plt.gca()
    f_ghz = tp.frequency/1e9
    ax.plot(f_ghz, tp.gain, label=label)
    ax.set_xlabel("Frequency [GHz]")
    ax.set_ylabel("$|S_{21}|$ [dB]")
    ax.grid()

    if plot_params:
        text = make_parameter_text(
                tp,
                ('resonance',
                    'peak_frequency',
                    'pump_power',),
                (9, 9, 3),
                )
        ax.text(
                1, 1, text, ha='right', va='top', transform=ax.transAxes,
                bbox={
                    'facecolor': 'white',
                    'edgecolor': 'black',
                    },
                fontsize=SMALLFONTSIZE)
    return ax


def plot_nt_fit(
        temperatures: np.ndarray,
        result: lmfit.model.ModelResult,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: t.Optional[str] = None,
        inputreferred: bool = True,
        withparams: bool = True,
        paramsloc: str = 'lower right',
        withfitlabel: bool = True,
        ):
    """Plots the NT fit result.
    Args:
        temperatures:  Noise source temperatures in K.
        result:  The `lmfit.ModelResult` instance.
        ax:  The Axes object for plotting.
        label:  The label for the plot.
        inputreferred:  Whether or not plot the input referred powers.  Input
            referred power means that the result PSD is divided by the
            estimated gain from the result object.
        withparams:  Plots the estimated parameters.
        paramsloc:  Location for the parameter box.

    """

    if ax is None:
        ax = plt.gca()
    data = result.data
    fit = result.best_fit
    if inputreferred:
        gain = result.best_values['g']
        data = data/gain
        fit = fit/gain
    label = 'Data' if label is None else label
    ax.plot(temperatures, data, 'o', label='Data')
    if withfitlabel:
        ax.plot(temperatures, fit, '-r', label='Best Fit')
    else:
        ax.plot(temperatures, fit, '-r')
    ax.set_xlabel("Noise Source Temperature [K]")
    ax.set_ylabel("PSD [W/Hz]")
    ax.grid(True, which='both')
    ax.legend()
    if isinstance(withparams, dict):
        texts = []
        for k, par in withparams.items():
            val = result.params[k].value
            fun = par.get('apply', lambda _: _)
            prec = par.get('fmt', '.3f')
            unit = par.get('unit', '')
            symbol = par.get('symbol', k)
            text = f"{symbol}={fun(val):{prec}} {unit}"
            texts.append(text)

        draw_text_box(ax, ' | '.join(texts), loc=paramsloc)
    elif withparams:
        g = result.best_values['g']
        tn = result.best_values['tn']
        g_db = lin2db_pow(g)
        text = (f"$G_{{\mathrm{{tot}}}}$={g_db:.1f} dB\n"
                f"$T_n$={tn*1e3:.0f} mK")
        draw_text_box(ax, text, loc=paramsloc)

    ax.legend(loc='best')
    return ax


def plot_xy(
        x, y,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: t.Optional[str] = None,
        yerr: t.Optional[np.ndarray] = None,
        marker=None):
    """Common entrypoint for plotting."""
    if ax is None:
        ax = plt.gca()

    if yerr is None:
        ax.plot(x, y, label=label, marker=marker)
    else:
        ax.errorbar(x, y, yerr=yerr, label=label, marker=marker)
    return ax


def plot2d(
        mmodel: MeasurementModel,
        names: t.Optional[t.Tuple[str, str]] = None,
        ax: t.Optional[mpl.axes.Axes] = None,
        label: t.Optional[str] = None,
        ):
    """Creates a 2d-plot using the first quantity in a `MeasurementModel` as
    x-axis and the second quantity as y-axis."""
    qs = quantities(mmodel)
    if names is None:
        qx, qy = islice(qs.values(), 0, 2)
    else:
        qx, qy = qs[names[0]], qs[names[1]]
    if ax is None:
        ax = plt.gca()
    ax.plot(qx.value, qy.value, label=label)
    ax.set_xlabel(f"{qx.repr} [{qx.unit}]")
    ax.set_ylabel(f"{qy.repr} [{qy.unit}]")
    ax.grid()
    return ax
