from pathlib import Path
import typing as t

from parse import parse
import pandas as pd

from groundcontrol.measurement import MeasurementModel, quantity, parameter
from groundcontrol.measurement import SParam1PModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.estimation import phasemodel
import groundcontrol.units as un
from groundcontrol.helper import Array


class FluxSweepResult(MeasurementModel):
    ib: Array[float] = quantity(un.ampere, "CoilCurrent")
    fr: Array[float] = quantity(un.hertz, "ResonanceFrequency")
    bw: Array[float] = quantity(un.hertz, "Bandwidth")

    ib_stderr: Array[float] = quantity(un.ampere)
    fr_stderr: Array[float] = quantity(un.hertz)
    bw_stderr: Array[float] = quantity(un.hertz)


class _ResonatorParams(MeasurementModel):
    ib: float = quantity(un.ampere, "CoilCurrent")
    fr: float = quantity(un.hertz, "ResonanceFrequency")
    bw: float = quantity(un.hertz, "Bandwidth")

    ib_stderr: float = quantity(un.ampere)
    fr_stderr: float = quantity(un.hertz)
    bw_stderr: float = quantity(un.hertz)


def fit_phase(sppm: SParam1PModel):
    params = phasemodel.guess(sppm.uphases, f=sppm.frequencies)
    fitresult = phasemodel.fit(sppm.uphases, f=sppm.frequencies, params=params)
    return fitresult


class DataBrowser:
    """The FluxSweep data contains to main MREF types:
        Usual step: MREF is just "i{step:d}"  Both SMM and SPPM have this.
        Debug step: MREF is "DEBUG-i{step:d}"  Only SPPM measurement.

    """
    mio: MIOFileCSV
    mindex: pd.DataFrame

    _debugind_fmt: str = "DEBUG-i{step}"
    _stepind_fmt: str = "i{step}"

    def prepare_records(self):
        mindex = self.mindex

        records = {}
        for mclass, path in zip(mindex.mclass, mindex.datapath):
            pdir, abbrev, mref, index = self.mio.parse_path(path)
            pdebug = parse(self._debugind_fmt, mref)
            pstep = parse(self._stepind_fmt, mref)
            if pdebug is not None:
                records[pdebug['step']] = index
            elif pstep is not None:
                records[pstep['step']] = index
            else:
                continue

    @classmethod
    def make(cls, mio: MIOFileCSV):
        mindex = pd.DataFrame.from_dict(
                mio.read_measurement_index())

        return cls(mio, mindex)

    @classmethod
    def from_path(cls, path: Path):
        mio = MIOFileCSV.open(path, 'r')
        return cls.make(mio)
