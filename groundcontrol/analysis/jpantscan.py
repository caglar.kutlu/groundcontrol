#!/usr/bin/env python
# coding: utf-8

# Analysis library for NT vs Arbitrary Measurement Points

from pathlib import Path
import typing as t
from datetime import datetime

import pandas as pd
from parse import parse
import attr

from groundcontrol.measurement import read_measurement_csv, MeasurementModel
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import parameters, quantity, parameter
from groundcontrol.friendly import mprefix_str
from groundcontrol.declarators import declarative
from groundcontrol.analysis.types import PSDEstimate
from groundcontrol.analysis.nt import NTDataSetAlt as NTDataSet
from groundcontrol.logging import logger
from groundcontrol import units as un


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

MEASINDEX = Path('measuremens.index')


class TemperatureMeasurement(MeasurementModel):
    temperature: float = quantity(un.kelvin, "Temperature")
    stdev: float = quantity(un.kelvin, "Temperature Stdev")
    settle_delay: float = quantity(un.second, "Settle Delay")
    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))


def make_parameter_text(
        mm: MeasurementModel,
        names: t.Optional[t.Collection[str]] = None,
        sigdigits: t.Optional[t.Collection[int]] = None):
    """Given a `MeasurementModel` returns a text that can be used with
    matplotlib's annotation functions.

    Args:
        mm:  The measurement model instance.
        names:  The names of the parameters to be used when making the text.
            If `None` all the found parameters are used.  Defaults to `None`.
        sigdigits:  Significant digits to use when representing values.  If
            provided the ordering and length of this object must be the same
            with `names` argument.
    """
    if (names is not None) and (sigdigits is not None):
        sigdigits = dict(zip(names, sigdigits))

    template = "{key}={value}"
    pars = parameters(mm)
    texts = []
    for name, par in pars.items():
        sigdigit = None
        if names is not None:
            if name not in names:
                continue
            else:
                if sigdigits is not None:
                    sigdigit = sigdigits.get(name, None)

        if sigdigit is not None:
            valuestr = mprefix_str(par.value, par.unit, sigdigits=sigdigit)
        else:
            valuestr = mprefix_str(par.value, par.unit)
        text = template.format(
                key=par.name,
                value=valuestr)
        texts.append(text)

    return '\n'.join(texts)


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path

    def _read_helper(
            self,
            indices: t.Union[t.Tuple[int, int], int, None],
            globdir: Path,
            refstr: t.Optional[str] = None):

        if refstr is None:
            glob_template = "*{mrefijk}_*.csv"
        else:
            glob_template = "-".join(
                    (f"*{refstr}", "{mrefijk}_*.csv"))

        if indices is None:
            globstr = f"*{refstr}_*.csv"
        elif isinstance(indices, tuple):
            i, j = indices[0], indices[1]
            mrefijk = f"i{i}j{j}"
            globstr = glob_template.format(mrefijk=mrefijk)
        else:  # check for intness
            i = indices
            mrefijk = f"i{i}"
            globstr = glob_template.format(mrefijk=mrefijk)

        files = globdir.glob(globstr)

        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            raise IndexError(
                    f"No measurement corresponding to index {indices}."
                    f"Globber: '{globstr}'")

        return read_measurement_csv(file)

    @property
    def ndcount(self):
        """Number of points in the primary sweep."""
        return len(self.sweepdf.groupby(['i']))

    def read_nt_dataset(
            self,
            primary_index: int) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation
        corresponding to the measurement indices of resonance frequency and
        offset frequency.  If the spectrum data is missing for certain
        temperatures, returns a dataset containing spectra until the first
        missing data point.

        Returns:
            ntds:  The NT dataset if index exists.  `None` otherwise.
        """

        grouped = self.sweepdf.groupby(['i'])
        for i, df in grouped:
            # NOTE: i is the primary sweep index.
            if i != primary_index:
                continue
            # each group must correspond to single jk by def.
            ijks = df.ijk.values

            baseline = self.read_baseline(i)
            psds = []
            gains = []
            resonances = []
            for k, inds in enumerate(ijks):
                try:
                    sasm = self.read_sasm(inds)
                except IndexError:
                    break
                psds.append(PSDEstimate.from_sasm(sasm))
                gains.append(self.read_gain(inds))
                resonances.append(self.read_resonance(inds))
            tup = self.read_tp(i)
            temps = df.tp.values[:k+1]

            dataset = NTDataSet.make(
                    temps, psds, tup, gains, baseline,
                    resonances)
            return dataset

        # if we reach the end, it means we didn't find the indices.
        return None

    def iterate_nt_datasets(self):
        for i, df in self.sweepdf.groupby('i'):
            # The primary sweep is assumed to be on frequencies.
            f_ghz = df.fr.values[0]/1e9
            logger.info(f"Procuring dataset for sweep f: {f_ghz:.6f} GHz")
            ntds = self.read_nt_dataset(i)
            if ntds is None:
                raise StopIteration
            yield ntds

    def read_baseline(
            self, index: t.Optional[int] = None):
        """Returns the baseline measurement.

        Args:
            index:  Index of the baseline measurement.  If `None`, returns the
                general baseline measurement that is probably performed at the
                beginning of a measurement.

        """
        return self._read_helper(index, self._sppmdir, "BASELINE")

    def read_gain(self, indices: t.Tuple[int, int]):
        """Returns the gain measurement.

        Kwargs:
            indices:  Indices of the gain measurement.

        """
        return self._read_helper(indices, self._sppmdir, "GAIN")

    def read_resonance(self, indices: t.Tuple[int, int]):
        """Returns the resonance measurement.

        Kwargs:
            indices:  Index of the resonance measurement.

        """
        return self._read_helper(indices, self._sppmdir, "RESONANCE")

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, indices) -> t.Optional[TuningPoint]:
        return self._read_helper(indices, self._tpdir)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    def fit_all(self, kc, gain_rest, tf, tn_rest):
        fitresults = []
        for ntds in self.iterate_nt_datasets():
            fpeak = ntds.tup.peak_frequency/1e9
            fres = ntds.tup.resonance/1e9
            msg = f"Processing tup (fr|fpeak): {fres:.6f}|{fpeak:.6f} GHz"
            logger.info(msg)
            fitresult = ntds.fit_noreflection(kc, gain_rest, tf, tn_rest)
            fitresults.append(fitresult)
        return fitresults

    @classmethod
    def from_path(
            cls,
            rootdir: t.Union[str, Path]):
        """Returns a sweep browser from the given path."""
        indfmt: str = 'i{i:d}j{j:d}'

        if isinstance(rootdir, str):
            rootdir = Path(rootdir)
        # Make a sweep dataframe with index i as main sweep parameter
        # j as the noise source temperature set point
        mio = MIOFileCSV.open(rootdir, 'r')
        indexdf = pd.DataFrame(mio.read_measurement_index())

        # Temperature measurement paths
        tmpaths = indexdf[indexdf.mclass == 'TemperatureMeasurement'].datapath
        tppaths = indexdf[indexdf.mclass == 'TuningPoint'].datapath

        tps = {}
        for tppath in tppaths:
            _, _, mref, _ = mio.parse_path(tppath)
            iind = parse('i{i:d}', mref)['i']
            tps[iind] = read_measurement_csv(tppath)

        records = []
        for tmpath in tmpaths:
            _, _, mref, _ = mio.parse_path(tmpath)
            parsed = parse(indfmt, mref)
            i, j = parsed['i'], parsed['j']
            tp = tps[i]
            tm = read_measurement_csv(tmpath)
            record = dict(
                    fr=tp.resonance,
                    tp=tm.temperature,
                    ib=tp.bias_current,
                    fp=tp.pump_frequency,
                    pp=tp.pump_power,
                    go=tp.offset_gain,
                    fo=tp.offset_frequency,
                    ps=tp.signal_power,
                    tns_std=tm.stdev,
                    i=i,
                    j=j,
                    ijk=(i, j),
                    )
            records.append(record)

        df = pd.DataFrame(records)

        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR
            )
