#!/usr/bin/env python
# coding: utf-8

# Analysis library for NT vs Arbitrary Measurement Points

from pathlib import Path
import typing as t
from datetime import datetime
from functools import partial
from itertools import repeat, takewhile, cycle, count
from collections import defaultdict as ddict

import scipy.constants as cnst
import xarray as xr
import pandas as pd
import lmfit
import numpy as np
from parse import parse
import attr
import cattr
import matplotlib.pyplot as plt

from groundcontrol.measurement import read_measurement_csv, MeasurementModel
from groundcontrol.measurement import make_parameter_text
from groundcontrol.measurement import SASpectrumModel, SParam1PModel
from groundcontrol.measurementio import MIOFileCSV
from groundcontrol.jpa_tuner import TuningPoint
from groundcontrol.declarators import parameters, quantity, parameter, to_json
from groundcontrol.friendly import mprefix_str
from groundcontrol.settings import VNASettings, SASettings
from groundcontrol.declarators import declarative, setting, from_json
from groundcontrol.analysis.types import PSDEstimate
from groundcontrol.analysis.nt import NTDataSetAlt as NTDataSet
from groundcontrol.logging import logger
from groundcontrol.analysis.nt import noise_psd
from groundcontrol import units as un
from groundcontrol.util import dbm2watt


# Data Folder & File Names

# Just the names
SASMDIR = Path('SASM')
SPPMDIR = Path('SPPM')
TMDIR = Path('TM')
TPDIR = Path('TP')

MEASINDEX = Path('measuremens.index')


class TemperatureMeasurement(MeasurementModel):
    temperature: float = quantity(un.kelvin, "Temperature")
    stdev: float = quantity(un.kelvin, "Temperature Stdev")
    settle_delay: float = quantity(un.second, "Settle Delay")
    timestamp: datetime = parameter(
                un.nounit, "Timestamp",
                default=attr.Factory(datetime.now))


@lmfit.Model
def noisemodel(p_in, g, tn):
    """Simple regression model for noise.
    Args:
        p_in:  Input noise power in W/Hz.
        g:  Gain of the component chain.
        tn:  Noise temperature of the component chain.
    """
    return g*(p_in + cnst.Boltzmann*tn)


def make_model(
        frequency: float,
        hints: t.Dict[str, float],
        modelname: str = 'bb+hp'):
    """
    Returns an lmfit.Model type model object for fitting parameters 'tn' and
    'gain'.

    Args:
        hints:  Initial guess values.  Must have keys ['tn', 'gain'].
        modelname:  Type of model to use.  Must be one of ['bb+hp',
            'rayleigh-jeans'].  'bb+hp' corresponds to the usual
            blackbody + quantum noise.
    """
    fun = partial(noise_psd, frequency=frequency)

    @lmfit.Model
    def bbhp(
            tns: float,
            tn: float, gain: float
            ):
        return gain*(fun(tns) + cnst.Boltzmann*tn)

    @lmfit.Model
    def rj(tns: float, tn: float, gain: float):
        return gain*cnst.Boltzmann*(tns + tn)

    if modelname == 'bb+hp':
        ntm = bbhp
    elif modelname == 'rj':
        ntm = rj
    else:
        raise ValueError(f"Model {modelname} unknown.")
    ntm.set_param_hint('tn', min=0, value=hints['tn'])
    ntm.set_param_hint('gain', min=1, value=hints['gain'])
    return ntm


_nopsetting = partial(setting, isoptional=False)


@declarative
class NTMeasSettings:
    temp_setpoints: t.List[float] = _nopsetting()
    repeat: int = _nopsetting()
    measuretrans: bool = _nopsetting()
    vnapreset: VNASettings = _nopsetting()
    sapreset: SASettings = _nopsetting()
    temperature_settle_delay: float = _nopsetting()
    temperature_settle_timeout: float = _nopsetting()
    temperature_meas_window: float = setting(default=30)
    temperature_meas_period: float = setting(default=0.2)
    skip_initial_temp_settle: bool = setting(default=True)
    temperature_max_fluctuation: float = setting(default=1e-2)
    temperature_max_error: float = setting(default=1e-2)
    repeat_measurements: bool = setting(default=False)

    _maxt: float = setting(default=1.2)


def read_settings(fname):
    return from_json(fname, NTMeasSettings)


@declarative
class RawDataSet:
    minds: t.List[int]
    tms: t.List[TemperatureMeasurement]
    sasms: t.List[SASpectrumModel]
    sppms: t.List[SParam1PModel]
    settings: NTMeasSettings
    nbwfactor: float
    _ignoretrans: bool = False

    @classmethod
    def make(cls, minds, tms, sasms, sppms, settings, nbwfactor, ignoretrans=False):
        return cls(minds, tms, sasms, sppms, settings, nbwfactor, ignoretrans=ignoretrans)

    def to_xarray(self):
        """Merges the measurements.  The merged dataset contains timestamps
        from the temperature measurement at the beginning of each source
        step."""
        tmarr, sarr, sparr = self._to_xarray_datasets()
        if sparr is None:
            ds = xr.merge([tmarr, sarr], compat='override')
        else:
            try:
                sspmerge = xr.merge([sarr, sparr], compat='override', join='exact')
            except ValueError:
                logger.info(
                        "Transmission and power spectrum frequency indexes are"
                        " different.  Assuming small difference, aligning "
                        "transmission measurement by interpolation.")
                spal = sparr.interp_like(sarr)
                sspmerge = xr.merge(
                        [sarr, spal], compat='override', join='exact')
            ds = xr.merge([tmarr, sspmerge], compat='override')
        ds.coords['mind'] = self.minds
        sa_attrs = {
                f"sa_{key}": value
                for key, value
                in sarr.attrs.items()}

        ds.attrs.update(sa_attrs)
        if sparr is not None:
            vna_attrs = {
                    f"vna_{key}": value
                    for key, value
                    in sparr.attrs.items()}
            ds.attrs.update(vna_attrs)

        unsdct = cattr.unstructure(self.settings)
        unsdct['sapreset'] = to_json(self.settings.sapreset)
        unsdct['vnapreset'] = to_json(self.settings.vnapreset)
        ds.attrs.update(unsdct)
        ds.attrs['nbwfactor'] = self.nbwfactor

        # Netcdf don't have boolean...
        lut = {False: 0, True: 1}
        dct = {
                k: lut[v] if isinstance(v, bool) else v
                for k, v in ds.attrs.items()}
        ds.attrs.update(dct)

        n = len(ds.mind)
        nbp = len(ds.temp_setpoints)

        # currently temp_setpoints contain the repeated values actually.
        nmissing = nbp - n
        if nmissing > 0:
            logger.info(f"Ignoring {nmissing} missing measurements.")

        tcount = nbp // ds.repeat
        realrepeat = n // tcount

        # this flag changes repetition order
        # i.e. each temperature setpoint will have N measurements rather than
        # repeating temperature setpoint cycle.
        repmeas_f = self.settings.repeat_measurements

        if repmeas_f:
            nreparr = np.tile(np.arange(realrepeat), tcount)
        else:
            nreparr = np.repeat(np.arange(realrepeat), tcount)
        tsetpoints = ds.temp_setpoints[:realrepeat*tcount]
        # counter = count()
        # tsind_vals = list(
        #         takewhile(
        #             lambda _: next(counter) < n,
        #             cycle(range(ds.repeat)))
        #         )
        ds.coords['nrep'] = ('mind', nreparr)
        ds.coords['tsp'] = ('mind', tsetpoints)
        newds = (ds.set_index(mind=['nrep', 'tsp'])
                   .unstack())

        newds['psd'] = dbm2watt(newds.power)/(newds.sa_rbw*self.nbwfactor)
        newds.psd.attrs.update({
                    'units': 'W/Hz',
                    'long_name': 'PSD'})
        return newds

    def _to_xarray_datasets(self):
        tmarr = (xr.concat(
            map(lambda m: (m.to_xarray(coords='temperature')
                            .swap_dims({'temperature': 'mind'})), self.tms),
            dim='mind')
                .drop('settle_delay')
                .rename({'stdev': 'temperature_stdev'})
                .reset_coords('temperature'))
        sarr = xr.concat(map(
            lambda m: m.to_xarray(coords='frequencies'), self.sasms),
            dim='mind').rename({'values': 'power'})
        if self.settings.measuretrans and (not self._ignoretrans):
            sparr = xr.concat(map(
                lambda m: m.to_xarray(coords='frequencies'), self.sppms),
                dim='mind').rename(
                        {'mags': 's21_mag',
                         'uphases': 's21_uphase',
                         'phases': 's21_phase'})
        else:
            sparr = None

        return tmarr, sarr, sparr

    @classmethod
    def from_path(
            cls, path: t.Union[str, Path],
            nbwfactor: float, ignoretrans: bool = False):
        """Creates the object using the path.

        Args:
            path: Path to the measurement folder.
            nbwfactor: NBW/RBW ratio.  If the measurement data is done when
                the SA filter selection was in "Noise" mode, this is 1.
                Otherwise, look at SA specs.  This will be related to the type
                of windowing function used.

        Returns:
            obj: DataSet object.

        """
        path = Path(path)
        settings = read_settings(path / 'settings.json')
        mio = MIOFileCSV.open(path, 'r')
        df = mio.read_measurement_index_df()
        tmpaths = df.datapath[df.mclass == 'TemperatureMeasurement'].values
        sasmpaths = df.datapath[df.mclass == 'SASpectrumModel'].values
        if settings.measuretrans:
            sppmpaths = df.datapath[df.mclass == 'SParam1PModel'].values
        else:
            sppmpaths = cycle([None])

        minds = []
        tms = []
        sasms = []
        sppms = []

        # Assuming ordered dataset
        for tmpath, sasmpath, sppmpath in zip(tmpaths, sasmpaths, sppmpaths):
            # Assume all mrefs are same
            pdir, abbrev, mref, index = mio.parse_path(tmpath)
            mrefind = parse('i{:d}', mref)[0]

            logger.info(f"Reading MREF={mref}.")
            tms.append(read_measurement_csv(tmpath))
            sasms.append(read_measurement_csv(sasmpath))
            # Assume there is no other SPPM
            if settings.measuretrans and (not ignoretrans):
                sppms.append(read_measurement_csv(sppmpath))
            minds.append(mrefind)

        return cls.make(
                minds, tms, sasms, sppms, settings, nbwfactor=nbwfactor, ignoretrans=ignoretrans)


@declarative
class Analyzer:
    ds: xr.Dataset

    @staticmethod
    def _passivetn(gcomp: float, temp: float, frequency: float):
        return (1/gcomp-1) * noise_psd(temp, frequency)/cnst.Boltzmann

    @staticmethod
    def yestimate(pi1, pi2, po1, po2):
        # quick estimate
        y = po2/po1
        tnr = (pi2 - y*pi1)/(cnst.Boltzmann*(y-1))
        gr = (po1-po2)/(pi1-pi2)
        return tnr, gr

    def fitgen_freq(self, ds, modelname: str) -> t.Generator:
        """Performs the fit on each frequency independently, returns a
        generator that yields a result for each bin."""
        tnsmin = ds.temperature.min()
        tnsmax = ds.temperature.max()
        fdim = 'frequencies'
        tminslice: xr.Dataset = ds.isel(tsp=0)
        tmaxslice = ds.isel(tsp=-1)
        psdmin = (tminslice.psd
                           .mean(dim=fdim))
        psdmax = (tmaxslice.psd
                           .mean(dim=fdim))

        tnsmin = tminslice.temperature.item()
        tnsmax = tmaxslice.temperature.item()

        favg = getattr(ds, fdim).median().item()
        pimin, pimax = noise_psd(tnsmin, favg), noise_psd(tnsmax, favg)

        # Just initial guesses from y-factor
        tnguess, gainguess = self.yestimate(pimin, pimax, psdmin, psdmax)

        hints = dict(tn=tnguess, gain=gainguess)

        for f, fds in ds.groupby(fdim):
            ntm = make_model(f, hints, modelname=modelname)
            fresult = ntm.fit(data=fds.psd.values, tns=fds.temperature)

            yield f, fresult

    def fit(self, nrep: t.Optional[None] = None, modelname: str = 'bb+hp'):
        """Performs the fit on the given repetition.
        If nrep is `None`, the whole dataset is used for fitting.
        """
        if nrep is None:
            raise NotImplementedError("Not implemented yet.")

        fdim = 'frequencies'
        if nrep is None:
            ds = self.ds.reset_index('nrep', drop=True)
        else:
            ds = self.ds.sel(nrep=nrep)

        gen = self.fitgen_freq(ds, modelname)

        nreport = getattr(ds, fdim).size//20
        results = ddict(list)
        logger.info(f"Fitting started.")
        for i, (f, result) in enumerate(gen):
            result: lmfit.model.ModelResult
            try:
                if i % nreport == 0:
                    logger.info(f"Processed f={f/1e9:.3f} GHz.")
            except ZeroDivisionError:
                logger.info(f"Processed f={f/1e9:.3f} GHz.")
            tn = result.params['tn']
            gain = result.params['gain']
            results['tn'].append(tn.value)
            results['tn_err'].append(tn.stderr)
            results['gain'].append(gain.value)
            results['gain_err'].append(gain.stderr)
            results['chisqr'].append(result.chisqr)
            results['fit_residual'].append(result.residual)
            results['psd_fit'].append(result.best_fit)

        for k in 'tn tn_err gain gain_err chisqr'.split(' '):
            results[k] = ([fdim], results[k])

        results['fit_residual'] = ([fdim, 'tsp'], results['fit_residual'])
        results['psd_fit'] = ([fdim, 'tsp'], results['psd_fit'])

        fds = ds.assign(results)
        fds.tn.attrs['units'] = fds.temperature.attrs['units']
        fds.tn.attrs['long_name'] = "$ \\tau_n $"
        fds.gain.attrs['units'] = 'W/W'
        fds.gain.attrs['long_name'] = r"$ G_{\mathrm{tot}} $"
        fds.attrs['fitmodel'] = modelname

        return fds

    def fit_all(self, modelname: str = 'bb+hp'):
        fits = []
        for nrep, ds in self.ds.groupby('nrep'):
            logger.info(f"Fitting for repetition: {nrep}")
            fitds = self.fit(nrep, modelname)
            fits.append(fitds)
        return xr.concat(fits, dim='nrep')

    @staticmethod
    def eval_model(
            dsa: xr.Dataset,
            tns: t.Union[xr.Dataset, np.ndarray],
            frequency: t.Optional[float] = None):
        """Evaluates the model used in analysis of the dsa on the domain
        provided by the array tns.  Useful for making fitting figures.
        """
        if not isinstance(tns, xr.DataArray):
            tns = xr.DataArray(
                    tns, coords={'tsp': tns}, dims='tsp', name='tns')
            tns.attrs = dsa.tsp.attrs
        fdim = 'frequencies'

        pars = {'tn': 1, 'gain': 1}  # not used in calculation
        if frequency is not None:
            try:
                ds = dsa.sel({fdim: frequency}, method='nearest')
            except ValueError:
                # assume the error was cuz dsa was already 1 freq
                ds = dsa
            f = getattr(ds, fdim).item()
            model = make_model(f, pars, modelname=dsa.attrs['fitmodel'])
            return model.eval(tns=tns, tn=ds.tn, gain=ds.gain)
        else:
            evals = []
            for f, fds in dsa.groupby(fdim):
                model = make_model(f, pars, modelname=dsa.attrs['fitmodel'])
                evals.append(model.eval(tns=tns, tn=fds.tn, gain=fds.gain))

            return xr.concat(evals, dim=fdim)

    @classmethod
    def make(cls, ds: xr.Dataset, sort: bool = True):
        return cls(ds.sortby(['nrep', 'tsp', 'frequencies']))

    @classmethod
    def from_path(cls, path: Path, sort: bool = True):
        ds = xr.open_dataset(path)
        return cls.make(ds, sort)


def fitds_to_csv(
        fitds: xr.Dataset, fname: t.Union[str, Path],
        nrep: t.Union[str, int] = 'mean'):
    """Saves the result of the fit_all as csv that can be used with snrscan
    measurements."""
    header = ("Frequency[Hz], Gain[W/W], GainErr[W/W], "
              "NoiseTemperature[K], NoiseTemperatureErr[K]")
    ds = fitds[['gain', 'gain_err', 'tn', 'tn_err']]

    if isinstance(nrep, int):
        (ds.isel(nrep=nrep)
           .to_dataframe()
           .to_csv(fname, header=header, sep=','))
    elif nrep == 'mean':
        (ds.to_dataframe().groupby('frequencies').mean()
           .to_csv(fname, header=header, sep=','))
    else:
        raise ValueError(
                f"Unknown nrep {nrep}")


@declarative
class Plotter:
    dsa: xr.Dataset
    tnsdom: np.ndarray
    _fdim: str = 'frequencies'

    def plot_at_f(self, f: float, nrep=0, ax=None):
        if ax is None:
            ax = plt.gca()
        ds = self.dsa.sel(nrep=nrep)
        ds = ds.sel({self._fdim: f}, method='nearest')
        evald = Analyzer.eval_model(ds, self.tnsdom, f)

        ds.psd.plot.line(marker='x', ax=ax, color='black')
        evald.plot.line(color='red')
        return ax

    @classmethod
    def make(cls, dsa, tnsdom: t.Optional[np.ndarray] = None):
        if tnsdom is None:
            tnsdom = np.linspace(1e-3, dsa.tsp.max().item(), 500)
        return cls(dsa, tnsdom)


@declarative
class SweepBrowser:
    sweepdf: pd.DataFrame
    rootdir: Path

    # Private members
    _sasmdir: Path
    _sppmdir: Path
    _tpdir: Path
    _tmdir: Path

    def _read_helper(
            self,
            indices: t.Union[t.Tuple[int, int], int, None],
            globdir: Path,
            refstr: t.Optional[str] = None):

        if refstr is None:
            glob_template = "*{mrefijk}_*.csv"
        else:
            glob_template = "-".join(
                    (f"*{refstr}", "{mrefijk}_*.csv"))

        if indices is None:
            globstr = f"*{refstr}_*.csv"
        elif isinstance(indices, tuple):
            i, j = indices[0], indices[1]
            mrefijk = f"i{i}j{j}"
            globstr = glob_template.format(mrefijk=mrefijk)
        else:  # check for intness
            i = indices
            mrefijk = f"i{i}"
            globstr = glob_template.format(mrefijk=mrefijk)

        files = globdir.glob(globstr)

        # There must be single file matching
        try:
            file = next(files)
        except StopIteration:
            raise IndexError(
                    f"No measurement corresponding to index {indices}."
                    f"Globber: '{globstr}'")

        return read_measurement_csv(file)

    @property
    def ndcount(self):
        """Number of points in the primary sweep."""
        return len(self.sweepdf.groupby(['i']))

    def read_nt_dataset(
            self,
            primary_index: int) -> t.Optional[NTDataSet]:
        """Reads the complete dataset for noise temperature estimation
        corresponding to the measurement indices of resonance frequency and
        offset frequency.  If the spectrum data is missing for certain
        temperatures, returns a dataset containing spectra until the first
        missing data point.

        Returns:
            ntds:  The NT dataset if index exists.  `None` otherwise.
        """

        grouped = self.sweepdf.groupby(['i'])
        for i, df in grouped:
            # NOTE: i is the primary sweep index.
            if i != primary_index:
                continue
            # each group must correspond to single jk by def.
            ijks = df.ijk.values

            baseline = self.read_baseline(i)
            psds = []
            gains = []
            resonances = []
            for k, inds in enumerate(ijks):
                try:
                    sasm = self.read_sasm(inds)
                except IndexError:
                    break
                psds.append(PSDEstimate.from_sasm(sasm))
                gains.append(self.read_gain(inds))
                resonances.append(self.read_resonance(inds))
            tup = self.read_tp(i)
            temps = df.tp.values[:k+1]

            dataset = NTDataSet.make(
                    temps, psds, tup, gains, baseline,
                    resonances)
            return dataset

        # if we reach the end, it means we didn't find the indices.
        return None

    def iterate_nt_datasets(self):
        for i, df in self.sweepdf.groupby('i'):
            # The primary sweep is assumed to be on frequencies.
            f_ghz = df.fr.values[0]/1e9
            logger.info(f"Procuring dataset for sweep f: {f_ghz:.6f} GHz")
            ntds = self.read_nt_dataset(i)
            if ntds is None:
                raise StopIteration
            yield ntds

    def read_baseline(
            self, index: t.Optional[int] = None):
        """Returns the baseline measurement.

        Args:
            index:  Index of the baseline measurement.  If `None`, returns the
                general baseline measurement that is probably performed at the
                beginning of a measurement.

        """
        return self._read_helper(index, self._sppmdir, "BASELINE")

    def read_gain(self, indices: t.Tuple[int, int]):
        """Returns the gain measurement.

        Kwargs:
            indices:  Indices of the gain measurement.

        """
        return self._read_helper(indices, self._sppmdir, "GAIN")

    def read_resonance(self, indices: t.Tuple[int, int]):
        """Returns the resonance measurement.

        Kwargs:
            indices:  Index of the resonance measurement.

        """
        return self._read_helper(indices, self._sppmdir, "RESONANCE")

    def read_sasm(self, indices) -> t.Optional[SASpectrumModel]:
        return self._read_helper(indices, self._sasmdir)

    def read_sppm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._sppmdir)

    def read_tp(self, indices) -> t.Optional[TuningPoint]:
        return self._read_helper(indices, self._tpdir)

    def read_tm(self, indices) -> t.Optional[SParam1PModel]:
        return self._read_helper(indices, self._tmdir)

    def fit_all(self, kc, gain_rest, tf, tn_rest):
        fitresults = []
        for ntds in self.iterate_nt_datasets():
            fpeak = ntds.tup.peak_frequency/1e9
            fres = ntds.tup.resonance/1e9
            msg = f"Processing tup (fr|fpeak): {fres:.6f}|{fpeak:.6f} GHz"
            logger.info(msg)
            fitresult = ntds.fit_noreflection(kc, gain_rest, tf, tn_rest)
            fitresults.append(fitresult)
        return fitresults

    @classmethod
    def from_path(
            cls,
            rootdir: t.Union[str, Path]):
        """Returns a sweep browser from the given path."""
        indfmt: str = 'i{i:d}j{j:d}'

        if isinstance(rootdir, str):
            rootdir = Path(rootdir)
        # Make a sweep dataframe with index i as main sweep parameter
        # j as the noise source temperature set point
        mio = MIOFileCSV.open(rootdir, 'r')
        indexdf = pd.DataFrame(mio.read_measurement_index())

        # Temperature measurement paths
        tmpaths = indexdf[indexdf.mclass == 'TemperatureMeasurement'].datapath
        tppaths = indexdf[indexdf.mclass == 'TuningPoint'].datapath

        tps = {}
        for tppath in tppaths:
            _, _, mref, _ = mio.parse_path(tppath)
            iind = parse('i{i:d}', mref)['i']
            tps[iind] = read_measurement_csv(tppath)

        records = []
        for tmpath in tmpaths:
            _, _, mref, _ = mio.parse_path(tmpath)
            parsed = parse(indfmt, mref)
            i, j = parsed['i'], parsed['j']
            tp = tps[i]
            tm = read_measurement_csv(tmpath)
            record = dict(
                    fr=tp.resonance,
                    tp=tm.temperature,
                    ib=tp.bias_current,
                    fp=tp.pump_frequency,
                    pp=tp.pump_power,
                    go=tp.offset_gain,
                    fo=tp.offset_frequency,
                    ps=tp.signal_power,
                    tns_std=tm.stdev,
                    i=i,
                    j=j,
                    ijk=(i, j),
                    )
            records.append(record)

        df = pd.DataFrame(records)

        return cls(
            df,
            rootdir=rootdir,
            sasmdir=rootdir/SASMDIR,
            sppmdir=rootdir/SPPMDIR,
            tpdir=rootdir/TPDIR,
            tmdir=rootdir/TMDIR
            )
